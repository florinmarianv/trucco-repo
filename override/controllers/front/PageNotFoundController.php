<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */
class PageNotFoundController extends PageNotFoundControllerCore
{
    /*
      * module: custompagenotfound
      * date: 2017-05-31 14:10:37
      * version: 1.0.0
      */
    /*
    * module: custompagenotfound
    * date: 2020-09-16 18:24:00
    * version: 1.0.4
    */
    public function initContent()
    {
        parent::initContent();
        if (Tools::version_compare(_PS_VERSION_, '1.7', '>=')) {
            return;
        }
        if (Module::isEnabled('custompagenotfound') &&
            ($custompagenotfound = Module::getInstanceByName('custompagenotfound')) &&
            Configuration::get('CUSTOMPAGENOTFOUND_ACTIVE')) {
            $this->context->smarty->assign('search', Configuration::get('CUSTOMPAGENOTFOUND_SEARCH'));
            $this->context->smarty->assign('id_template', Configuration::get('CUSTOMPAGENOTFOUND_CHOICE'));
            $page_content = Db::getInstance()->getValue('SELECT `content` FROM `' . _DB_PREFIX_
                . 'custompagenotfound` WHERE `id_lang`="' . (int)$this->context->language->id . '" AND `id_shop`="'
                . (int)$this->context->shop->id . '" AND `id_template`="'
                . Configuration::get('CUSTOMPAGENOTFOUND_CHOICE')
                . '"');
            $page_content = str_replace('src="../img/cms/', 'src="'
                ._PS_BASE_URL_SSL_.__PS_BASE_URI__.'/img/cms/', $page_content);
            $page_content = str_replace('{$form_link}', $this->context->link->getPageLink('search'), $page_content);
            $this->context->smarty->assign('content', $page_content);
            $this->setTemplate($custompagenotfound->getPath() . '/views/templates/front/customtemplate.tpl');
        }
    }
}
