<?php
/**
* Hide Combinations
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2021 idnovate
*  @license   See above
*/
abstract class Db extends DbCore
{
    /*
    * module: hidecombinations
    * date: 2021-06-29 09:25:20
    * version: 1.0.6
    */
    public function executesmodule($sql, $array = true, $use_cache = true)
    {
        if ($sql instanceof DbQuery) {
            $sql = $sql->build();
        }
        $this->result = false;
        $this->last_query = $sql;
        if ($use_cache && $this->is_cache_enabled && $array) {
            $this->last_query_hash = Tools::encryptIV($sql);
            if (($result = Cache::getInstance()->get($this->last_query_hash)) !== false) {
                $this->last_cached = true;
                return $result;
            }
        }
        if (!preg_match('#^\s*\(?\s*(select|show|explain|describe|desc)\s#i', $sql)) {
            if (defined('_PS_MODE_DEV_') && _PS_MODE_DEV_) {
                throw new PrestaShopDatabaseException('Db->executeS() must be used only with select, show, explain or describe queries');
            }
            return parent::execute($sql, $use_cache);
        }
        $this->result = parent::query($sql);
        if (!$this->result) {
            $result = false;
        } else {
            if (!$array) {
                $use_cache = false;
                $result = $this->result;
            } else {
                $result = $this->getAll($this->result);
            }
        }
        $this->last_cached = false;
        if ($use_cache && $this->is_cache_enabled && $array) {
            Cache::getInstance()->setQuery($sql, $result);
        }
        return $result;
    }
    /*
    * module: hidecombinations
    * date: 2021-06-29 09:25:20
    * version: 1.0.6
    */
    public function query($sql)
    {
        $query_advanced = "advancedsearch_cache_product";
        $query_layered = "INSERT INTO `"._DB_PREFIX_."layered_product_attribute`";
        if (strpos($sql, $query_advanced) != false || strpos($sql, $query_layered) != false) {
            return parent::query($sql);
        }
        $context = Context::getContext();
        if (isset($context->controller) && $context->controller->controller_type == 'admin') {
            return parent::query($sql);
        }
        if (preg_match('#^\s*\(?\s*(select|create|show|explain|describe|desc)\s#i', $sql)) {
            if (isset($context->link) && !empty($context->link) && Module::getInstanceByName('hidecombinations')) {
                include_once(_PS_MODULE_DIR_.'hidecombinations/classes/HidecombinationsConfiguration.php');
                $sql = HidecombinationsConfiguration::returnSQLModified($sql);
            }
        }
        return parent::query($sql);
    }
    /*
    * module: hidecombinations
    * date: 2021-06-29 09:25:20
    * version: 1.0.6
    */
    public function executemodule($sql, $use_cache = true)
    {
        if ($sql instanceof DbQuery) {
            $sql = $sql->build();
        }
        $this->result = parent::query($sql);
        if ($use_cache && $this->is_cache_enabled) {
            Cache::getInstance()->deleteQuery($sql);
        }
        return (bool)$this->result;
    }
}
