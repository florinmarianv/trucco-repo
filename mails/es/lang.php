<?php

global $_LANGMAIL;
$_LANGMAIL = array();
$_LANGMAIL['The virtual product that you bought is available for download'] = '';
$_LANGMAIL['Virtual product to download'] = '';
$_LANGMAIL['Package in transit'] = '';
$_LANGMAIL['Your order has been changed'] = '';
$_LANGMAIL['Order confirmation'] = '';
$_LANGMAIL['New voucher regarding your order %s'] = '';
$_LANGMAIL['New voucher for your order %s'] = '';
$_LANGMAIL['Product out of stock'] = '';
$_LANGMAIL['You received a Gift Card'] = '';
$_LANGMAIL['Your Purchased Gift cards'] = '';
$_LANGMAIL['Product available'] = '';
$_LANGMAIL['New order : #%d - %s'] = '';
$_LANGMAIL['Stock coverage'] = '';
$_LANGMAIL['New return from order #%d - %s'] = '';
$_LANGMAIL['Your account has been activated'] = '';
$_LANGMAIL['A new customers has registered'] = '';

?>