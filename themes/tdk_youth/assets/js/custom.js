/**
 * 2007-2020 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */
//TDK:: create option for slick slider of modal popup at product page
var options_modal_product_page = {
    speed: 300,
    dots: false,
    infinite: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    vertical: true,
    verticalSwiping: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
};

//TDK:: create option for slick slider of quickview
var options_quickview = {
	speed: 300,
	dots: false,
	infinite: false,
	slidesToShow: 4,
	slidesToScroll: 1,
	vertical: true,
	verticalSwiping: true,
	responsive: [
	  {
		breakpoint: 1200,
		settings: {
		  slidesToShow: 4,
		  slidesToScroll: 1,
		}
	  },
	  {
		breakpoint: 992,
		settings: {
		  slidesToShow: 3,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 768,
		settings: {
		  slidesToShow: 4,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 576,
		settings: {
		  slidesToShow: 3,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 480,
		settings: {
		  slidesToShow: 2,
		  slidesToScroll: 1
		}
	  }
	]
};

//TDK:: fix f5 status of browser auto scroll
// $(window).on('unload', function() {
   // $(window).scrollTop(0);
// });
if ($('.pro-info-col').length) {
	var e_info_offset_top = $('.pro-info-col').offset().top;
	var e_info_offset_left = $('.pro-info-col').offset().left;
	var e_info_width = $('.pro-info-col').width();
	
}

if ($('.pro-action-col').length) {
	var e_action_offset_top = $('.pro-action-col').offset().top;
	var e_action_offset_left = $('.pro-action-col').offset().left;
	var e_action_width = $('.pro-action-col').width();
}

$( window ).resize(function() {
	//TDK:: fix zoom, only work at product page
	if (prestashop.page.page_name == 'product')
		restartElevateZoom();
		
	//TDK:: fix lost slider of modal when resize
	if ($('#product-modal .product-images').hasClass('slick-initialized') && $('#product-modal .product-images').height() == 0) {		
		//TDK:: setup slide for product thumb (modal)
		$('#product-modal .product-images').slick('unslick');
		$('#product-modal .product-images').hide();
		initSlickProductModal();
	}
	
	//TDK:: active/inactive scroll effect at product page
	if ($(window).width() >= 768) {
		$('.pro-info-col .pro-col-wrapper, .pro-action-col .pro-col-wrapper').removeClass('effect-off').addClass('effect-on');
		setTimeout(function(){
			if ($('.pro-info-col').length) {
				e_info_offset_top = $('.pro-info-col').offset().top;
				e_info_offset_left = $('.pro-info-col').offset().left;
				e_info_width = $('.pro-info-col').width();
				// console.log(e_info_width);
			}

			if ($('.pro-action-col').length) {
				e_action_offset_top = $('.pro-action-col').offset().top;
				e_action_offset_left = $('.pro-action-col').offset().left;
				e_action_width = $('.pro-action-col').width();
			}
		},200);		
		
		//TDK:: add scroll effect for product page
		if ($('.page-content.one-column').length || $('.page-content.two-column').length) {
			actionNavProductImages();
		}
		
	} else {
		$('.pro-info-col .pro-col-wrapper, .pro-action-col .pro-col-wrapper').removeClass('effect-on').addClass('effect-off').css({'width': 'auto'});
	}
});

$(document).ready(function(){
	floatHeader();
	backtotop();
	lawCookie();
	//check page product only
	if(prestashop.page.page_name == 'product'){
		innitSlickandZoom();
	}
	if (prestashop.page.page_name == 'category'){
		setDefaultListGrid();
	}

	if(typeof (products_list_functions) != 'undefined') {
		for (var i = 0; i < products_list_functions.length; i++) {
		products_list_functions[i]();
		}
	}
  
	//TDK:: update for order page - tab adress, when change adress, block adress change class selected
	$('.address-item .radio-block').click(function() {
		if (!$(this).parents('.address-item').hasClass('selected')) {
			$('.address-item.selected').removeClass('selected');
			$(this).parents('.address-item').addClass('selected');
		}
	})
  
	//TDK:: loading quickview
	actionQuickViewLoading();

	prestashop.on('updateProductList', function() {
		actionQuickViewLoading();
	}); 
			
	prestashop.on('updatedProduct', function () {
		if ($('.quickview.modal .product-thumb-images').length) {
			//TDK:: run slick slider for product thumb - quickview
			initSlickProductQuickView();		
		} else if ($('.product-detail .product-thumb-images').length) {
			//TDK:: re-call setup slick when change attribute at product page
			innitSlickandZoom();			
		}
		
		//TDK:: action of navigation product images (product page - type product image galary with one column)
		if ($('.page-content.one-column').length || $('.page-content.two-column').length) {		
			if ($(window).width() >= 768) {
				actionNavProductImages();
				$('.pro-info-col .pro-col-wrapper, .pro-action-col .pro-col-wrapper').removeClass('effect-off').addClass('effect-on');
			} else {
				$('.pro-info-col .pro-col-wrapper, .pro-action-col .pro-col-wrapper').removeClass('effect-on').addClass('effect-off').css({'width': 'auto'});
			}
		}
	});
	
	//TDK:: display modal by config
	if (typeof $("#content").data('templatemodal') != 'undefined') {
		if (!$("#content").data('templatemodal')) {
			$('div[data-target="#product-modal"]').hide();
		}
	}
	
	//TDK:: create demo product detail from megamenu
	$('.demo-product-detail a').click(function(e){
		if (!$(this).hasClass('updated')) {
			e.preventDefault();	
			var current_url = window.location.href;
			if(prestashop.page.page_name == 'product' && current_url.indexOf('.html') >= 0){		
				var link_href = $(this).attr('href');
				var layout_key_index = link_href.indexOf('?layout=');	
				var layout_key_value = link_href.substring(layout_key_index);			
				current_url = current_url.substring(0, current_url.indexOf('.html'));
				var new_url = current_url+'.html'+layout_key_value;
				window.location.href = new_url;
			};
		}
		
	});
	
	//TDK:: action of navigation product images (product page - type product image galary with one column)
	if ($('.page-content.one-column').length || $('.page-content.two-column').length) {	
		if ($(window).width() >= 768) {
			actionNavProductImages();
			$('.pro-info-col .pro-col-wrapper, .pro-action-col .pro-col-wrapper').removeClass('effect-off').addClass('effect-on');
		} else {
			$('.pro-info-col .pro-col-wrapper, .pro-action-col .pro-col-wrapper').removeClass('effect-on').addClass('effect-off').css({'width': 'auto'});
		}
	}
	
	//TDK:: fix slick not work when it is put in hidden tab (product tab)
	$('.product-tabs .nav-link, .products-accordion .collapsed').click(function(){
		if (!$(this).hasClass('active')) {
			var tab_id = $(this).attr('href');
			check_tab_loaded = setInterval(function(){				
				if ($(tab_id).find('.slick-initialized').length && $(tab_id).find('.slick-initialized .slick-track').width() == 0) {
					$(tab_id).find('.slick-initialized').slick('setPosition');
					if ($(tab_id).find('.slick-initialized .slick-track').width() > 0) {					
						clearInterval(check_tab_loaded);
					}
				}				
			}, 300);
			
		}
	});
		
});

function innitSlickandZoom(){
	if($("#main").hasClass('product-image-thumbs')){
		
		//TDK:: setup slide for product thumb (main)
		$('.product-detail .product-thumb-images').imagesLoaded( function(){ 
					
			if (typeof check_loaded_main_product != 'undefined') {
				clearInterval(check_loaded_main_product);
			}
			
			check_loaded_main_product = setInterval(function(){
				if($('.product-detail .product-thumb-images').height() > 0) {	
					
					$('.product-detail .product-thumb-images').fadeIn();
					
					clearInterval(check_loaded_main_product);
					postion = $("#content").data("templateview");
					//TDK:: add config for over 1200 extra large
					numberimage = $("#content").data("numberimage");
					numberimage1200 = $("#content").data("numberimage1200");
					numberimage992  = $("#content").data("numberimage992");
					numberimage768  = $("#content").data("numberimage768");
					numberimage576  = $("#content").data("numberimage576");
					numberimage480  = $("#content").data("numberimage480");
					numberimage360  = $("#content").data("numberimage360");

					if(postion !== 'undefined'){
						initSlickProductThumb(postion, numberimage, numberimage1200, numberimage992, numberimage768, numberimage576, numberimage480, numberimage360);
					}
					
				}
			}, 300);
		});
		
		//TDK:: setup slide for product thumb (modal)
		initSlickProductModal();
		
	}
	//call action zoom
	applyElevateZoom();
}

function restartElevateZoom(){	
    $(".zoomContainer").remove();
    applyElevateZoom();
}

function applyElevateZoom(){
	if($(window).width() <= 991 || $("#content").data("templatezoomtype") == 'none') {		
		//TDK:: remove elevateZoom on mobile
		if($('#main').hasClass('product-image-gallery')) {
			if ($('img.js-thumb').data('elevateZoom')) {
				var ezApi = $('img.js-thumb').data('elevateZoom');
				ezApi.changeState('disable');			
				$('img.js-thumb').unbind("touchmove");
			}
		} else {
			if ($("#zoom_product").data('elevateZoom')) {
				var ezApi = $("#zoom_product").data('elevateZoom');
				ezApi.changeState('disable');			
				$("#zoom_product").unbind("touchmove");
			}
		}
		return false;
	}
	  
  //check if that is gallery, zoom all thumb
	//TDK:: fix zoom, create config
	var zt = $("#content").data('templatezoomtype');
	var zoom_cursor;
	var zoom_type;
	var scroll_zoom = false;
	var	lens_FadeIn = 200;
	var	lens_FadeOut = 200;
	var	zoomWindow_FadeIn = 200;
	var	zoomWindow_FadeOut = 200;
	var zoom_tint = false;
	var zoomWindow_Width = 400;
	var zoomWindow_Height = 400;
	var zoomWindow_Position = 1;
	
	if (zt == 'in') {
		zoom_cursor = 'crosshair';
		zoom_type = 'inner';
		lens_FadeIn = false;
		lens_FadeOut = false;		
	} else {
		zoom_cursor = 'default';
		zoom_type = 'window';
		zoom_tint = true;
		zoomWindow_Width = $("#content").data('zoomwindowwidth');
		zoomWindow_Height = $("#content").data('zoomwindowheight');
		
		if ($("#content").data('zoomposition') == 'right') {			
			//TDK:: update position of zoom window with ar language
			if (prestashop.language.is_rtl == 1) {
				zoomWindow_Position = 11;
			} else {
				zoomWindow_Position = 1;
			}
		}
		
		if ($("#content").data('zoomposition') == 'left') {
			//TDK:: update position of zoom window with ar language
			if (prestashop.language.is_rtl == 1) {
				zoomWindow_Position = 1;
			} else {
				zoomWindow_Position = 11;
			}
		}
		
		if ($("#content").data('zoomposition') == 'top') {
			zoomWindow_Position = 13;
		}
		
		if ($("#content").data('zoomposition') == 'bottom') {
			zoomWindow_Position = 7;
		}
		
		if (zt == 'in_scrooll') {
			//TDK:: scroll to zoom does not work on IE
			var ua = window.navigator.userAgent;
			var old_ie = ua.indexOf('MSIE ');
			var new_ie = ua.indexOf('Trident/');
			if (old_ie > 0 || new_ie > 0) {
				// If Internet Explorer, return version number
				scroll_zoom = false;
			} else {
				// If another browser, return 0
				scroll_zoom = true;
			}
			
		}
	};
	
	if($('#main').hasClass('product-image-gallery')) {
		lens_FadeIn = false;
		lens_FadeOut = false;
		zoomWindow_FadeIn = false;
		zoomWindow_FadeOut = false;
	}
	
	var zoom_config = {
		responsive  : true,
		cursor: zoom_cursor,
		scrollZoom: scroll_zoom,
		scrollZoomIncrement: 0.1,
		zoomLevel: 1,
		zoomType: zoom_type,
		gallery: 'thumb-gallery',
		lensFadeIn: lens_FadeIn,
		lensFadeOut: lens_FadeOut,
		zoomWindowFadeIn: zoomWindow_FadeIn,
		zoomWindowFadeOut: zoomWindow_FadeOut,
		zoomWindowWidth: zoomWindow_Width,
		zoomWindowHeight: zoomWindow_Height,
		borderColour: '#888',
		borderSize: 2,
		zoomWindowOffetx: 0,
		zoomWindowOffety: 0,
		zoomWindowPosition: zoomWindow_Position,
		tint: zoom_tint,
	};
	
	if($('#main').hasClass('product-image-gallery')) {
		$('img.js-thumb').each(function(){
			var parent_e = $(this).parent();
			$(this).attr('src', parent_e.data('image'));
			$(this).data('type-zoom', parent_e.data('zoom-image'));
		});
		
		if($.fn.elevateZoom !== undefined) {
			$('img.js-thumb').elevateZoom(zoom_config);
			//TDK:: fix click a thumb replace all image and add fancybox
			$('img.js-thumb').bind("click", function(e) {  		
				var ez =   $(this).data('elevateZoom'); 
				$.fancybox(ez.getGalleryList());
				return false;
			});
		}
	} else {
		if($.fn.elevateZoom !== undefined) {
			$("#zoom_product").elevateZoom(zoom_config);
			
			//pass the images to Fancybox
			$("#zoom_product").bind("click", function(e) {  
				var ez =   $('#zoom_product').data('elevateZoom'); 
				$.fancybox(ez.getGalleryList());
				return false;
			});
		}
		
	}
}

function initSlickProductThumb(postion, numberimage, numberimage1200, numberimage992, numberimage768, numberimage576, numberimage480 , numberimage360){
	var vertical = true;
	var verticalSwiping = true;
	//TDK:: update for rtl
	var slick_rtl = false;

	if (postion == "bottom") {
		vertical = false;
		verticalSwiping = false;
	} 

	if (postion == 'none') {
		vertical = false;
		verticalSwiping = false;
		numberimage = numberimage1200 = numberimage992 = numberimage768 = numberimage576 = numberimage480 = numberimage360 = 1;
	}
	
	//TDK:: update for rtl
	if (!vertical && prestashop.language.is_rtl == 1) {
		slick_rtl = true;
	}

	var slider = $('#thumb-gallery');

	slider.slick({
		speed: 300,
		dots: false,
		infinite: false,
		slidesToShow: numberimage,
		vertical: vertical,
		verticalSwiping: verticalSwiping,
		slidesToScroll: 1,
		rtl: slick_rtl,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
						slidesToShow: numberimage1200,
						slidesToScroll: 1,
					}
				},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: numberimage992,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: numberimage768,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: numberimage576,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: numberimage480,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 360,
				settings: {
					slidesToShow: numberimage360,
					slidesToScroll: 1
				}
			}
		]
	});
	$("#thumb-gallery").show();

	if(postion == 'none') {
		var slickInstance = slider[0];
		var slides = $(slickInstance.slick.$slides);
		var positionStart = findPosition(slides);
		var slideCount = slickInstance.slick.slideCount;
		
		//TDK:: update slick for case without thubms
		if ((positionStart + 1) == slideCount) {
			$('.arrows-product-fake .slick-next').addClass('slick-disabled');
		} else if (positionStart == 0) {
			$('.arrows-product-fake .slick-prev').addClass('slick-disabled');
		}
		
		// active image first load
		slider.slick('slickGoTo', positionStart);

		$('.arrows-product-fake .slick-next').on( "click", function() {
			if (!$(this).hasClass('slick-disabled')) {
				$('.arrows-product-fake .slick-prev').removeClass('slick-disabled');
				var positionCurrent = findPosition(slides);
				if ((positionCurrent + 1) < slideCount) {
					$(slides[positionCurrent]).removeClass('active');
					$(slides[positionCurrent + 1]).addClass('active');
					$(slides[positionCurrent + 1]).find('img').trigger("click");
					slider.slick('slickNext');
					if((positionCurrent + 1) == (slideCount - 1)){
						$(this).addClass('slick-disabled');
					}
				}
			}
		  
		});

		$('.arrows-product-fake .slick-prev').on( "click", function() {
			if (!$(this).hasClass('slick-disabled')) {
				$('.arrows-product-fake .slick-next').removeClass('slick-disabled');
				var positionCurrent = findPosition(slides);
				if ((positionCurrent) > 0) {
					$(slides[positionCurrent]).removeClass('active');
					$(slides[positionCurrent - 1]).addClass('active');
					$(slides[positionCurrent - 1]).find('img').trigger("click");
					slider.slick('slickPrev');
					if((positionCurrent - 1) == 0){
						$(this).addClass('slick-disabled');
					}
				}
			}
		});
	}
}

function findPosition(slides){
	var position;
	for (var i = 0; i < slides.length; i++) {
		if ($(slides[i]).hasClass('active')) {
			position = $(slides[i]).data('slick-index');
			return position;
		}
	}
}

//TDK:: loading quickview
function actionQuickViewLoading()
{
	$('.quick-view').click(function(){
    if (!$(this).hasClass('active')) {
		$(this).addClass('active');
		$(this).find('.tdk-quickview-bt-loading').css({'display':'block'});
		$(this).find('.tdk-quickview-bt-content').hide();
		if (typeof check_active_quickview != 'undefined') {
			clearInterval(check_active_quickview);
		}
	 
		check_active_quickview = setInterval(function(){
			if($('.quickview.modal').length) {
				$('.quickview.modal').on('hide.bs.modal', function (e) {
					$('.quick-view.active').find('.tdk-quickview-bt-loading').hide();
					$('.quick-view.active').find('.tdk-quickview-bt-content').show();
					$('.quick-view.active').removeClass('active');
				});
				clearInterval(check_active_quickview);
				
				//TDK:: run slick for product thumb - quickview
				initSlickProductQuickView();
			}
        
		}, 300);

    }
  })
}

$(document).on('click', '.tdk_grid', function(e){
	e.preventDefault();
	$('#js-product-list .product_list').removeClass('list');
	$('#js-product-list .product_list').addClass('grid');
  
	$(this).parent().find('.tdk_list').removeClass('selected');
	$(this).addClass('selected');

	var configName = TDK_COOKIE_THEME +'_grid_list';
	$.cookie(configName, 'grid', {expires: 1, path: '/'});
});

$(document).on('click', '.tdk_list', function(e){
	e.preventDefault();
	$('#js-product-list .product_list').removeClass('grid');
	$('#js-product-list .product_list').addClass('list');
  
	$(this).parent().find('.tdk_grid').removeClass('selected');
	$(this).addClass('selected');

	var configName = TDK_COOKIE_THEME +'_grid_list';
	$.cookie(configName, 'list', {expires: 1, path: '/'});
});

function setDefaultListGrid()
{
	if ($.cookie(TDK_COOKIE_THEME +'_grid_list') == 'grid') {
		$('.tdk_grid').trigger('click');
	}
	
	if ($.cookie(TDK_COOKIE_THEME +'_grid_list') == 'list') {
		$('.tdk_list').trigger('click');
	}
}

function processFloatHeader(headerAdd, scroolAction){
	//TDK:: hide search result when enable float header
	if ($('.ac_results').length) {
		$('.ac_results').hide();
	}
	
	if (headerAdd) {
		$("#TdkHeader").addClass( "navbar-fixed-top" );
		var hideheight =  $("#TdkHeader").height()+120;
		$("#TdkMainPage").css( "padding-top", $("#TdkHeader").height() );
		setTimeout(function(){
			$("#TdkMainPage").css( "padding-top", $("#TdkHeader").height() );
		},200);
	} else {
		$("#TdkHeader").removeClass( "navbar-fixed-top" );
		$("#TdkMainPage").css( "padding-top", '');
	}

	var pos = $(window).scrollTop();
	if ( scroolAction && pos >= hideheight ) {
		$(".header-nav").addClass('hide-bar');
		$(".hide-bar").css( "margin-top", - $(".header-nav").height() );
		$("#TdkHeader").addClass("mini-navbar");
	} else {
		$(".header-nav").removeClass('hide-bar');
		$(".header-nav").css( "margin-top", 0 );
		$("#TdkHeader").removeClass("mini-navbar");
	}
}

//Float Menu
function floatHeader(){
	if (!$("body").hasClass("keep-header") || $(window).width() <= 990) {
		return;
	}
  
	$(window).resize(function(){
		if ($(window).width() <= 990) {
			processFloatHeader(0,0);
		} else if ($(window).width() > 990) {
			if ($("body").hasClass("keep-header"))
				processFloatHeader(1,1);
		}
	});
	var headerScrollTimer;

	$(window).scroll(function() {
		if (headerScrollTimer) {
			window.clearTimeout(headerScrollTimer);
		}

		headerScrollTimer = window.setTimeout(function() {
		if (!$("body").hasClass("keep-header")) return;
			if ($(window).width() > 990) {
				processFloatHeader(1,1);
			}
		}, 100);
	});
}

// Back to top
function backtotop(){
	$("#TdkBackTop").hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			$('#TdkBackTop').fadeIn();
		} else {
			$('#TdkBackTop').fadeOut();
		}
	});

	// scroll body to 0px on click
	$('#TdkBackTop a').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
}

//TDK:: build slick slider for quickview
function initSlickProductQuickView(){
	$('.quickview.modal .product-thumb-images').imagesLoaded( function(){ 
		if (typeof check_loaded_thumb_quickview != 'undefined')
		{
			clearInterval(check_loaded_thumb_quickview);
		}
		check_loaded_thumb_quickview = setInterval(function(){
			if($('.quickview.modal .product-thumb-images').height() > 0)
			{	
				$('.quickview.modal .product-thumb-images').fadeIn();
				
				clearInterval(check_loaded_thumb_quickview);
				$('.quickview.modal .product-thumb-images').slick(options_quickview);
			}
		}, 300);
	});
	
}

//TDK:: build slick slider for modal - product page
function initSlickProductModal(){
	$('#product-modal .product-images').imagesLoaded( function(){ 	
		if (typeof check_loaded_thumb_modal != 'undefined') {
			clearInterval(check_loaded_thumb_modal);
		}
		check_loaded_thumb_modal = setInterval(function(){
			if($('#product-modal .product-images').height() > 0) {	
				$('#product-modal .product-images').fadeIn();
				
				clearInterval(check_loaded_thumb_modal);
				$('#product-modal .product-images').slick(options_modal_product_page);
			}
		}, 300);
	});
}

//TDK: fix base prestashop
$(document).ready(function(){
	//TDK: remove css inline of label
	$('.product-flag').removeAttr('style');
	prestashop.on('updateProductList', function() {
		//TDK: remove css inline of label
		$('.product-flag').removeAttr('style');
	});
})
//Fix translate button choose file to upload: change "Choose file" to choosefile_text
//Fix filter (category page) does not work on IE change dataset.searchUrl to getAttribute('data-search-url')

// TDK Cookie Law
function lawCookie() {
	if ($.cookie('popup-cookie-law') != 'accept-cookie' ) {
		var mainCookie = $("#TdkCookieLaw");
		setTimeout(function(){
			$(mainCookie).addClass('TdkShowCookie');
			$(mainCookie).find('.TdkCookiesAccept').click(function(e){
				acceptCookie();
				e.preventDefault();
				return false;
			});
		},1000);
	}
	function acceptCookie() {
		$(mainCookie).removeClass('TdkShowCookie');
		$.cookie('popup-cookie-law', 'accept-cookie', {expires: 100, path: '/'});
	}
  /*function skipCookie(){
    $(mainCookie).removeClass('TdkShowCookie');
    $.cookie('popup-cookie-law', null, {path: '/'});
  }*/
}
// TDK Search Block Top
$(document).ready(function() {
   	$("#search_widget").each(function() {
		$(".TdkSearchIcon").click(function() {
			setTimeout(function() { $('#search_widget').focus() }, 1000);
			$(".TdkOverLayer").addClass("TdkShowForm"), $(".TdkFormSearch").addClass("TdkShowForm"), $("#TdkSearchIconClose").addClass("TdkShowForm")
		}), $("#TdkSearchIconClose").click(function() {
			$(".TdkFormSearch").removeClass("TdkShowForm"), $("#TdkSearchIconClose").removeClass("TdkShowForm"), $(".TdkOverLayer").removeClass("TdkShowForm")
		})
   	})
});
$(document).mouseup(function(e) {
    var container = $(".TdkFormSearch,.TdkSearchSubget");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(".TdkFormSearch").removeClass("TdkShowForm"), $("#TdkSearchIconClose").removeClass("TdkShowForm"), $(".TdkOverLayer").removeClass("TdkShowForm");
    }
});

// TDK Header Show Menu
$(function(){
    $('.TdkShowMenu').click(function(e) {
     	e.stopPropagation();
     	if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
		
        if ($('.groupShowMenu').hasClass('TdkActiveMenu')) {
            $('.groupShowMenu').removeClass('TdkActiveMenu');
        } else {
            $('.groupShowMenu').addClass('TdkActiveMenu');
        }
		
        if ($('.TdkOverLayer').hasClass('TdkShowBgOverlay')) {
            $('.TdkOverLayer').removeClass('TdkShowBgOverlay');
        } else {
            $('.TdkOverLayer').addClass('TdkShowBgOverlay');
        }
    }); 
    $('.closemenu').click(function(e) {
		e.stopPropagation();
        if ($('.groupShowMenu').hasClass('TdkActiveMenu')) {
            $('.groupShowMenu').removeClass('TdkActiveMenu');
        }
        if ($('.TdkOverLayer').hasClass('TdkShowBgOverlay')) {
            $('.TdkOverLayer').removeClass('TdkShowBgOverlay');
        }
    });
	$(document).click(function(event) { 
	  	if (!$(event.target).closest('.groupShowMenu.TdkActiveMenu').length) {
	   		if ($('.groupShowMenu.TdkActiveMenu').is(":visible")) {
			   	$('.TdkShowMenu.active').removeClass('active');
			    $('.groupShowMenu.TdkActiveMenu').removeClass('TdkActiveMenu');
			    $('.TdkOverLayer.TdkShowBgOverlay').removeClass('TdkShowBgOverlay');
	   		}
	  	}        
	});
});
// TDK Show Nesletter Home 4
$(function(){
    $('.btn-close-popup').click(function(e) {
     	e.stopPropagation();
     	if ($(this).parents(".GroupNewsletter").hasClass('not-active')) {
            $(this).parents(".GroupNewsletter").removeClass('not-active');
        } else {
            $(this).parents(".GroupNewsletter").addClass('not-active');
        }
    }); 
});

// TDK Show Size Chart
$(function(){
    $('.GroupSizeChart .size-finder').click(function(e) {
     	e.stopPropagation();
     	if ($(this).parents(".GroupSizeChart").hasClass('is-visible')) {
            $(this).parents(".GroupSizeChart").removeClass('is-visible');
        } else {
            $(this).parents(".GroupSizeChart").addClass('is-visible');
        }
    }); 
    $('.sizeguide-popup .overlay').click(function(e) {
		e.stopPropagation();
        if ($('.GroupSizeChart').hasClass('is-visible')) {
            $('.GroupSizeChart').removeClass('is-visible');
        }
    });
    $('.GroupSizeChart .c-button-close').click(function(e) {
		e.stopPropagation();
        if ($(this).parents(".GroupSizeChart").hasClass('is-visible')) {
            $(this).parents(".GroupSizeChart").removeClass('is-visible');
        }
    });
});

//TDK:: actions of navigation product images
function actionNavProductImages() {
	//TDK:: remove navigation product images if does not active
	if (!$('.page-content.one-column').hasClass('nav-images-enable') || $('.page-content.two-column').length) {
		$('.nav-product-images').remove();
	}
	
	//TDK:: check when f5 browser, offset of scrollbar exist
	if ($(window).scrollTop() > 0) {
		$('.nav-product-images .nav-item.active').removeClass('active');
		$(".product-thumb-images .thumb-container img").imagesLoaded( function(){		
			// here all images loaded, do your stuff
			var scroll_offset = $(window).scrollTop();
			$(".product-thumb-images .thumb-container").each(function(){
			
				if (scroll_offset < $(this).offset().top -150) {
					var continue_index = $(this).index();
					
					if (continue_index != 0) {
						continue_index = continue_index-1;
					}
					
					$('.nav-product-images .nav-item').eq(continue_index).addClass('active');
					return false;
				}
			});
			
			if (scroll_offset > $(".product-thumb-images .thumb-container").first().offset().top) {					
				$('.nav-product-images').addClass('fixed');
			}		
		});	
	}
	
	$('.nav-product-images .nav-item').click(function(){
		if (!$(this).hasClass('active')) {
			$('.nav-product-images .nav-item.active').removeClass('active');
			$(this).addClass('active');
			var index_item = $(this).index();
			
			$('.nav-product-images').addClass('scrolling');
			
			$('body,html').animate({
				scrollTop: $(".product-thumb-images .thumb-container").eq(index_item).offset().top
			}, 500, function(){
				$('.nav-product-images').removeClass('scrolling');
				
				if (index_item != 0) {
					if ($(window).scrollTop() > $(".product-thumb-images .thumb-container").first().offset().top) {
						$('.nav-product-images').addClass('fixed');
					} else {
						$('.nav-product-images').removeClass('fixed');
					}
				} else {
					
					$('.nav-product-images').addClass('fixed');
				}
			});
		}
		return false;
	});
	
	//TDK:: scroll event to keep navigation of product images and blocks
	var lastScrollTop = 0;
	
	var e_pro_info_col = $('.pro-info-col');
	var e_info_wrapper = e_pro_info_col.find('.pro-col-wrapper');
	var e_info_height = e_info_wrapper.height();
	
	var e_pro_action_col = $('.pro-action-col');
	var e_action_wrapper = e_pro_action_col.find('.pro-col-wrapper');
	var e_action_height = e_action_wrapper.height();
	
	var list_thumb_offset = $('.product-thumb-images').offset().top;
	var w_height = $(window).height();
	
	// var list_thumb_height = $('.product-thumb-images').height();
	$(window).scroll(function() {
		if (!$('.nav-product-images').hasClass('scrolling')) {
			var scroll_offset = $(this).scrollTop();			
			var current_index = $('.nav-product-images .nav-item.active').index();
			var max_index = $('.nav-product-images .nav-item').last().index();			
			if (scroll_offset > lastScrollTop) {
				//TDK:: add class for navigation				
				if (scroll_offset > $(".product-thumb-images .thumb-container").first().offset().top) {					
					$('.nav-product-images').addClass('fixed');
				}
				
				//TDK:: scroll down
				if ($('.nav-product-images .nav-item.active').length && current_index+1 <= max_index && $(".product-thumb-images .thumb-container").eq(current_index+1).offset().top < scroll_offset + 150) {					
					$('.nav-product-images .nav-item.active').removeClass('active');
					$('.nav-product-images .nav-item').eq(current_index+1).addClass('active');					
				}
				
				//TDK:: scroll down over list product images
				if ($(".product-thumb-images .thumb-container").last().offset().top + $(".product-thumb-images .thumb-container").last().height() < scroll_offset + $(window).height()) {
					$('.nav-product-images .nav-item.active').removeClass('active');
					$('.nav-product-images').removeClass('fixed');
				}
				
				//TDK:: scroll down and catch list product images
				if (!$('.nav-product-images .nav-item.active').length && $(".product-thumb-images .thumb-container").first().offset().top < scroll_offset + $(window).height() && $(".product-thumb-images .thumb-container").first().offset().top > scroll_offset) {				
					$('.nav-product-images .nav-item').first().addClass('active');
					$('.nav-product-images').addClass('fixed');	
				}						
				
				//TDK:: product info scroll
				// $('.pro-info-col').each(function(){
					// console.log($('.product-thumb-images').height());					
					if ($('.pro-info-col').length) {
						stickyElementScrollDown(e_info_height, e_info_width, e_pro_info_col, e_info_offset_left, e_info_offset_top, scroll_offset, w_height, list_thumb_offset, e_info_wrapper);
					}
					
					if ($('.pro-action-col').length) {
						stickyElementScrollDown(e_action_height, e_action_width, e_pro_action_col, e_action_offset_left, e_action_offset_top, scroll_offset, w_height, list_thumb_offset, e_action_wrapper);
					}
				// });
				
			} else {
				//TDK:: scroll up
				if ($('.nav-product-images .nav-item.active').length && current_index-1 >= 0 && $(".product-thumb-images .thumb-container").eq(current_index-1).height() + $(".product-thumb-images .thumb-container").eq(current_index-1).offset().top > scroll_offset + 150) {					
					// console.log();
					$('.nav-product-images .nav-item.active').removeClass('active');
					$('.nav-product-images .nav-item').eq(current_index-1).addClass('active');				
				}
				
				//TDK:: scroll up over list product images
				if ($(".product-thumb-images .thumb-container").first().offset().top > scroll_offset) {
					$('.nav-product-images .nav-item.active').removeClass('active');
					$('.nav-product-images').removeClass('fixed');
				}
				
				//TDK:: scroll up and catch list product images
				if (!$('.nav-product-images .nav-item.active').length && $(".product-thumb-images .thumb-container").last().offset().top + $(".product-thumb-images .thumb-container").last().height() > scroll_offset + $(window).height() && $(".product-thumb-images .thumb-container").last().offset().top < scroll_offset) {					
					$('.nav-product-images .nav-item').last().addClass('active');	
					$('.nav-product-images').addClass('fixed');					
				}
				
				// console.log(scroll_offset);
				// console.log($(".product-thumb-images .thumb-container").first().offset().top);
				//TDK:: add class for navigation
				if (scroll_offset < $(".product-thumb-images .thumb-container").first().offset().top - 150) {
					
					$('.nav-product-images').removeClass('fixed');
				}
				
				//TDK:: product info scroll
				// $('.pro-info-col').each(function(){
					// console.log($('.product-thumb-images').height());
					if ($('.pro-info-col').length) {
						stickyElementScrollUp(e_info_height, e_info_width, e_pro_info_col, e_info_offset_left, e_info_offset_top, scroll_offset, w_height, list_thumb_offset, e_info_wrapper);
					}
					
					if ($('.pro-action-col').length) {
						stickyElementScrollUp(e_action_height, e_action_width, e_pro_action_col, e_action_offset_left, e_action_offset_top, scroll_offset, w_height, list_thumb_offset, e_action_wrapper);
					}
					
				// });
			}
			lastScrollTop = scroll_offset;
			
		}
	});
	
	//TDK:: create image when hover navigation of product images
	$('.nav-product-images .nav-item').each(function(){
		var current_index = $(this).index();
		var clone_imge_src = $(".product-thumb-images .thumb-container").eq(current_index).find('img.thumb').data('image-medium-src');
		
		$(this).append('<img class="img-preview" src="'+clone_imge_src+'">')
	});
	
	$('.nav-product-images .nav-item').hover(function(){
		$(this).find('.img-preview').stop(true,true).fadeIn();
	}, function(){
		$(this).find('.img-preview').stop(true,true).fadeOut();
	});
}

//TDK:: keep element when scroll down
function stickyElementScrollDown(e_height, e_width, e_pro_col, e_offset_left, e_offset_top, scroll_offset, w_height, list_thumb_offset, e_wrapper) {	
	if ($('.product-thumb-images').height() > e_height*2 && !e_wrapper.hasClass('effect-off')) {
		e_wrapper.css({'width': e_width });
		e_pro_col.height($('.product-thumb-images').height());
		// console.log(scroll_offset);
		// console.log(e_offset_top);
		if (scroll_offset+10 >= e_offset_top) {
			//TDK:: if height block higher height window
			if (e_height > w_height) {					
				// e_pro_col.css({'height': w_height-20});
				// e_pro_col.mCustomScrollbar({
					// theme:"dark",
					// scrollInertia: 200,
					// callbacks:{
						// onInit:function(){
						  
						// }
					// },
					// keyboard:{
						// enable:true,
					// }
				// });
				//TDK:: scroll down when wrapper has class fixed top
				if (e_wrapper.hasClass('fixed-top') || e_wrapper.hasClass('fixed-scrolling')) {
					if (e_wrapper.hasClass('fixed-top')) {
						e_wrapper.css({'top': scroll_offset - e_offset_top});
						e_wrapper.removeClass('fixed-top').addClass('fixed-scrolling');
					}
					
					if (e_wrapper.hasClass('fixed-scrolling')) {
						if (scroll_offset > e_pro_col.find('.fixed-scrolling').offset().top + e_height - w_height) {
							e_wrapper.css({'top': ''});
							e_wrapper.removeClass('fixed-scrolling').addClass('fixed-bottom');
						}
					}
				} else {
					//TDK:: scroll to end block
					if (scroll_offset >= (e_offset_top + e_height - w_height)) {
						// console.log(e_offset_top);
						// console.log(e_height);
						// console.log(w_height);
						// console.log(e_offset_top+e_height-w_height);
						if (scroll_offset <= (e_offset_top+$('.product-thumb-images').height()-w_height)) {
							e_wrapper.removeClass('fixed-top').addClass('fixed-bottom');
							e_wrapper.css({'top': ''});
						} else {
							//TDK:: scroll to end wrapper block
							e_wrapper.removeClass('fixed-top fixed-bottom');					
							e_wrapper.css({'bottom': 0});
						}
					}
				}					
			} else {
				if (scroll_offset < (e_offset_top+$('.product-thumb-images').height()-e_height)) {
					e_wrapper.addClass('fixed-top');
					e_wrapper.css({'top': ''});
				} else {
					//TDK:: scroll to end wrapper block
					e_wrapper.removeClass('fixed-top fixed-bottom');					
					e_wrapper.css({'bottom': 0});
				}
			}
			
			// e_pro_col.addClass('fixed');
					
			// e_pro_info_col.mCustomScrollbar("scrollTo","top");
			
			// if (e_pro_col.hasClass('pro-info-col'))
			// {
				// var current_e_height = $('.pro-info-col').height();
				// if ($('.pro-images-col').length &&  $('.pro-action-col').length)
				// {
					// $('.pro-images-col').css({'margin-left': e_width})
				// }
			// }
			
			// if (e_pro_col.hasClass('pro-action-col'))
			// {			
				// var current_e_height = $('.pro-action-col').height();
			// }
			//TDK:: scroll to end list thumb
			// if (scroll_offset+20 >= (list_thumb_offset+$('.product-thumb-images').height() - current_e_height ))
			// {								
				// e_pro_col.removeClass('fixed');
				// e_pro_col.css({'height': ''});
				// e_pro_col.css({'width': ''});
				// e_pro_col.mCustomScrollbar("destroy");
			// }
		} else {
			e_wrapper.removeClass('fixed-top fixed-bottom');
			e_wrapper.css({'top': ''});
			// e_pro_col.css({'height': ''});
			// e_pro_col.css({'width': ''});
			// e_pro_col.mCustomScrollbar("destroy");
		}
	} else {	
		e_wrapper.removeClass('fixed-top fixed-bottom effect-on').addClass('effect-off');
		e_wrapper.css({'top': '', 'bottom': '', 'width': 'auto'});
		e_pro_col.css({'height': 'auto'});
	}
}

//TDK:: keep element when scroll down
function stickyElementScrollUp(e_height, e_width, e_pro_col, e_offset_left, e_offset_top, scroll_offset, w_height, list_thumb_offset, e_wrapper) {
	if ($('.product-thumb-images').height() > e_height*2 && !e_wrapper.hasClass('effect-off')) {	
		e_wrapper.css({'width': e_width });
		e_pro_col.height($('.product-thumb-images').height());
		// console.log(scroll_offset);
		// console.log(e_offset_top);
		if (scroll_offset+10 >= e_offset_top) {
			//TDK:: if height block higher height window
			if (e_height > w_height) {					
				// e_pro_col.css({'height': w_height-20});
				// e_pro_col.mCustomScrollbar({
					// theme:"dark",
					// scrollInertia: 200,
					// callbacks:{
						// onInit:function(){
						  
						// }
					// },
					// keyboard:{
						// enable:true,
					// }
				// });
				//TDK:: scroll up when wrapper has class fixed bottom
				if (e_wrapper.hasClass('fixed-bottom') || e_wrapper.hasClass('fixed-scrolling')) {
					if (e_wrapper.hasClass('fixed-bottom')) {
						e_wrapper.css({'top': scroll_offset - (e_height - w_height)});
						e_wrapper.removeClass('fixed-bottom').addClass('fixed-scrolling');
					}
					
					if (e_wrapper.hasClass('fixed-scrolling')) {
						if (scroll_offset <= e_pro_col.find('.fixed-scrolling').offset().top) {
							e_wrapper.css({'top': ''});
							e_wrapper.removeClass('fixed-scrolling').addClass('fixed-top');
						}
					}
				} else {
					//TDK:: scroll to end block
					if (scroll_offset < (e_offset_top+$('.product-thumb-images').height())) {
						if (scroll_offset < (e_offset_top+$('.product-thumb-images').height()-e_height)) {						
							e_wrapper.removeClass('fixed-bottom').addClass('fixed-top');
							e_wrapper.css({'bottom': ''});
						}				
					} else {
						//TDK:: scroll to end wrapper block
						e_wrapper.removeClass('fixed-top fixed-bottom');					
						e_wrapper.css({'bottom': 0});
					}
				}
			} else {
				//TDK:: scroll to end block
				if (scroll_offset < (e_offset_top+$('.product-thumb-images').height())) {
					if (scroll_offset < (e_offset_top+$('.product-thumb-images').height()-e_height)) {				
						e_wrapper.addClass('fixed-top');
						e_wrapper.css({'bottom': ''});
					}				
				} else {
					//TDK:: scroll to end wrapper block
					e_wrapper.removeClass('fixed-top fixed-bottom');					
					e_wrapper.css({'bottom': 0});
				}
			}
			
			// if (e_pro_col.hasClass('pro-info-col'))
			// {
				// var current_e_height = $('.pro-info-col').height();
			// }
			
			// if (e_pro_col.hasClass('pro-action-col'))
			// {
				// var current_e_height = $('.pro-action-col').height();
			// }
															
			//TDK:: scroll to end list thumb
			// if (scroll_offset+20 < (list_thumb_offset+$('.product-thumb-images').height() - current_e_height))
			// {								
				// e_pro_col.addClass('fixed');
				// e_pro_info_col.mCustomScrollbar("scrollTo","top");
			// }
		} else {
			// e_pro_col.removeClass('fixed');
			// e_pro_col.css({'height': ''});
			// e_pro_col.css({'width': ''});			
			// e_pro_col.mCustomScrollbar("destroy");
			// if ($('.pro-images-col').length &&  $('.pro-action-col').length)
			// {
				// $('.pro-images-col').css({'margin-left': 0})
			// }
			e_wrapper.removeClass('fixed-top fixed-bottom');
			e_wrapper.css({'top': ''});
		}
	} else {	
		e_wrapper.removeClass('fixed-top fixed-bottom effect-on').addClass('effect-off');
		e_wrapper.css({'top': '', 'bottom': '', 'width': 'auto'});
		e_pro_col.css({'height': 'auto'});
	}
}