{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<!-- TDK Block Info module -->
<div id="tdk_block_top" class="TdkBlockInfo TdkPopupOver dropdown js-dropdown">
    <a href="javascript:void(0)" data-toggle="dropdown" class="TdkPopupTitle" title="{l s='Setting' d='Shop.Theme.Global'}">
	    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 54 54" style="enable-background:new 0 0 54 54;" xml:space="preserve">
			<path d="M51.22,21h-5.052c-0.812,0-1.481-0.447-1.792-1.197s-0.153-1.54,0.42-2.114l3.572-3.571
				c0.525-0.525,0.814-1.224,0.814-1.966c0-0.743-0.289-1.441-0.814-1.967l-4.553-4.553c-1.05-1.05-2.881-1.052-3.933,0l-3.571,3.571
				c-0.574,0.573-1.366,0.733-2.114,0.421C33.447,9.313,33,8.644,33,7.832V2.78C33,1.247,31.753,0,30.22,0H23.78
				C22.247,0,21,1.247,21,2.78v5.052c0,0.812-0.447,1.481-1.197,1.792c-0.748,0.313-1.54,0.152-2.114-0.421l-3.571-3.571
				c-1.052-1.052-2.883-1.05-3.933,0l-4.553,4.553c-0.525,0.525-0.814,1.224-0.814,1.967c0,0.742,0.289,1.44,0.814,1.966l3.572,3.571
				c0.573,0.574,0.73,1.364,0.42,2.114S8.644,21,7.832,21H2.78C1.247,21,0,22.247,0,23.78v6.439C0,31.753,1.247,33,2.78,33h5.052
				c0.812,0,1.481,0.447,1.792,1.197s0.153,1.54-0.42,2.114l-3.572,3.571c-0.525,0.525-0.814,1.224-0.814,1.966
				c0,0.743,0.289,1.441,0.814,1.967l4.553,4.553c1.051,1.051,2.881,1.053,3.933,0l3.571-3.572c0.574-0.573,1.363-0.731,2.114-0.42
				c0.75,0.311,1.197,0.98,1.197,1.792v5.052c0,1.533,1.247,2.78,2.78,2.78h6.439c1.533,0,2.78-1.247,2.78-2.78v-5.052
				c0-0.812,0.447-1.481,1.197-1.792c0.751-0.312,1.54-0.153,2.114,0.42l3.571,3.572c1.052,1.052,2.883,1.05,3.933,0l4.553-4.553
				c0.525-0.525,0.814-1.224,0.814-1.967c0-0.742-0.289-1.44-0.814-1.966l-3.572-3.571c-0.573-0.574-0.73-1.364-0.42-2.114
				S45.356,33,46.168,33h5.052c1.533,0,2.78-1.247,2.78-2.78V23.78C54,22.247,52.753,21,51.22,21z M52,30.22
				C52,30.65,51.65,31,51.22,31h-5.052c-1.624,0-3.019,0.932-3.64,2.432c-0.622,1.5-0.295,3.146,0.854,4.294l3.572,3.571
				c0.305,0.305,0.305,0.8,0,1.104l-4.553,4.553c-0.304,0.304-0.799,0.306-1.104,0l-3.571-3.572c-1.149-1.149-2.794-1.474-4.294-0.854
				c-1.5,0.621-2.432,2.016-2.432,3.64v5.052C31,51.65,30.65,52,30.22,52H23.78C23.35,52,23,51.65,23,51.22v-5.052
				c0-1.624-0.932-3.019-2.432-3.64c-0.503-0.209-1.021-0.311-1.533-0.311c-1.014,0-1.997,0.4-2.761,1.164l-3.571,3.572
				c-0.306,0.306-0.801,0.304-1.104,0l-4.553-4.553c-0.305-0.305-0.305-0.8,0-1.104l3.572-3.571c1.148-1.148,1.476-2.794,0.854-4.294
				C10.851,31.932,9.456,31,7.832,31H2.78C2.35,31,2,30.65,2,30.22V23.78C2,23.35,2.35,23,2.78,23h5.052
				c1.624,0,3.019-0.932,3.64-2.432c0.622-1.5,0.295-3.146-0.854-4.294l-3.572-3.571c-0.305-0.305-0.305-0.8,0-1.104l4.553-4.553
				c0.304-0.305,0.799-0.305,1.104,0l3.571,3.571c1.147,1.147,2.792,1.476,4.294,0.854C22.068,10.851,23,9.456,23,7.832V2.78
				C23,2.35,23.35,2,23.78,2h6.439C30.65,2,31,2.35,31,2.78v5.052c0,1.624,0.932,3.019,2.432,3.64
				c1.502,0.622,3.146,0.294,4.294-0.854l3.571-3.571c0.306-0.305,0.801-0.305,1.104,0l4.553,4.553c0.305,0.305,0.305,0.8,0,1.104
				l-3.572,3.571c-1.148,1.148-1.476,2.794-0.854,4.294c0.621,1.5,2.016,2.432,3.64,2.432h5.052C51.65,23,52,23.35,52,23.78V30.22z"/>
			<path d="M27,18c-4.963,0-9,4.037-9,9s4.037,9,9,9s9-4.037,9-9S31.963,18,27,18z M27,34c-3.859,0-7-3.141-7-7s3.141-7,7-7
				s7,3.141,7,7S30.859,34,27,34z"/>
		</svg>
	</a>    
	<div class="TdkPopupContent dropdown-menu">
		{if $enable_userinfo == 1}
		<div class="row">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 col-sp-12 tdk-leftBlockInfo">
		{/if}
			<div class="tdk-language-selector">
				<span class="title">{l s='Language:' d='Shop.Theme.Global'}</span>
				<ul class="link">
					{foreach from=$languages item=language}
			          	<li {if $language.id_lang == $current_language.id_lang} class="current" {/if}>
			            	<a href="{url entity='language' id=$language.id_lang}" class="dropdown-item">
			            		<img src="{$img_lang_url|escape:'html':'UTF-8'}{$language.id_lang|escape:'html':'UTF-8'}.jpg" alt="{$language.iso_code|escape:'html':'UTF-8'}" width="16" height="11" />
			            	</a>
			          	</li>
			        {/foreach}
				</ul>
			</div>
			<div class="tdk-currency-selector">
				<span class="title">{l s='Currency:' d='Shop.Theme.Global'}</span>
				<ul class="link">
					{foreach from=$currencies item=currency}
			        	<li {if $currency.current} class="current" {/if}>
			          		<a title="{$currency.name}" rel="nofollow" href="{$currency.url}" class="dropdown-item">{$currency.iso_code}</a>
			        	</li>
			      	{/foreach}
				</ul>
			</div>
		{if $enable_userinfo == 1}
			</div>
		{/if}
			
		{if $enable_userinfo == 1}
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 col-sp-12 tdk-rightBlockInfo">
				<span class="title">{l s='My account' d='Shop.Theme.Global'}</span>
					<ul class="user-info">
				      	{if $logged}
					        <li>
					          <a
					            class="account" 
					            href="{$my_account_url}"
					            title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}"
					            rel="nofollow"
					          >
					            <span>{l s='Hello' d='Shop.Theme.Global'} {$customerName}</span>
					          </a>
					        </li>
					        <li>
					          <a
					            class="logout"
					            href="{$logout_url}"
					            rel="nofollow"
					          >
					          	<i class="icons icon-logout"></i>
					            <span>{l s='Sign out' d='Shop.Theme.Actions'}</span>
					          </a>
					        </li>
				      	{else}
					        <li>
					          	<a
					            class="signin"
					            href="{$my_account_url}"
					            title="{l s='Log in to your customer account' d='Shop.Theme.Customeraccount'}"
					            rel="nofollow"
					          	>
					          		<i class="icons icon-login"></i>
					            	<span>{l s='Sign in' d='Shop.Theme.Actions'}</span>
					          	</a>
					        </li>
				      	{/if}
				      	<li>
					        <a
					          class="myacount"
					          href="{$my_account_url}"
					          title="{l s='My account' d='Shop.Theme.Global'}"
					          rel="nofollow"
					        >
					        	<i class="icons icon-user"></i>
					        	<span>{l s='My account' d='Shop.Theme.Global'}</span>
					        </a>
				      	</li>
				      	{if Configuration::get('TDKTOOLKIT_ENABLE_PRODUCTWISHLIST')}
					      	<li>
						        <a
						          class="tdk-btn-wishlist dropdown-item"
						          href="{url entity='module' name='tdktoolkit' controller='mywishlist'}"
						          title="{l s='Wishlist' d='Shop.Theme.Global'}"
						          rel="nofollow"
						        >
						          	<i class="icons icon-heart"></i>
						          	<span>{l s='Wishlist' d='Shop.Theme.Global'}</span>
						  		    <span class="tdk-total-wishlist tdk-total"></span>
						        </a>
					      	</li>
					  	{/if}
					  	{if Configuration::get('TDKTOOLKIT_ENABLE_PRODUCTCOMPARE')}
					  		<li>
						        <a
						          class="tdk-btn-compare dropdown-item"
						          href="{url entity='module' name='tdktoolkit' controller='productscompare'}"
						          title="{l s='Compare' d='Shop.Theme.Global'}"
						          rel="nofollow"
						        >
						          	<i class="icons icon-refresh"></i>
						          	<span>{l s='Compare' d='Shop.Theme.Global'}</span>
						  		    <span class="tdk-total-compare tdk-total"></span>
						        </a>
					      	</li>
					  	{/if}
				      	<li>
					        <a
					          class="checkout"
					          href="{url entity='cart' params=['action' => show]}"
					          title="{l s='Checkout' d='Shop.Theme.Customeraccount'}"
					          rel="nofollow"
					        >
					        	<i class="icons icon-basket"></i>
					        	<span>{l s='Checkout' d='Shop.Theme.Actions'}</span>
					        </a>
				      	</li>
				    </ul>    
				</div>
			{/if}
		{if $enable_userinfo == 1}
			</div>
		{/if}
	</div>
</div>

<!-- TDK Block Info module -->
