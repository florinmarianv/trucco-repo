{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\front\license.tpl -->

<section id="main" class="product-detail detail-1711738015 product-image-gallery" itemscope itemtype="https://schema.org/Product">
  <meta itemprop="url" content="{$product.url}"><div class="row"><div class="pro-info-col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 col-sp-12"><div class="pro-col-wrapper">
{block name='page_header_container'}
	{block name='page_header'}
		<h1 class="h1 product-detail-name" itemprop="name">{block name='page_title'}{$product.name}{/block}</h1>
	{/block}
{/block}
{block name='product_additional_info'}
	{include file='catalog/_partials/product-additional-info.tpl'}
{/block}
{block name='product_prices'}
	{include file='catalog/_partials/product-prices.tpl'}
{/block}

{block name='product_description_short'}
  <div id="product-description-short-{$product.id}" class="description-short" itemprop="description">{$product.description_short nofilter}</div>
{/block}
{block name='hook_display_reassurance'}
  {hook h='displayReassurance'}
{/block}</div></div><div class="pro-images-col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 col-sp-12"><div class="pro-col-wrapper">
{block name='page_content_container'}
  <section class="page-content one-column nav-images-disable" id="content" data-templatecolumn="one" data-templatenav="0" data-templatezoomtype="in" data-zoomposition="right" data-zoomwindowwidth="400" data-zoomwindowheight="400">
  
    {block name='page_content'}
      <div class="images-container">
    {if $product.images|@count > 1}
      <div class="nav-product-images">
        <ul class="nav-list">
          {for $i=1 to $product.images|@count}
            <li class="nav-item{if $i == 1} active{/if}" data-val="{$i|intval}"></li>
          {/for}
        </ul>
      </div>
    {/if}
        {block name='product_cover_thumbnails'}

          {block name='product_cover'}
            <div class="product-cover">
              {block name='product_flags'}
                <ul class="product-flags">
                  {foreach from=$product.flags item=flag}
                    <li class="product-flag {$flag.type}">{$flag.label}</li>
                  {/foreach}
                </ul>
              {/block}
              <img id="zoom_product" data-type-zoom="" class="js-qv-product-cover img-fluid intense" src="{$product.cover.bySize.large_default.url}" alt="{$product.cover.legend}" title="{$product.cover.legend}" itemprop="image">
              <div class="layer hidden-sm-down" data-toggle="modal" data-target="#product-modal">
                <i class="material-icons zoom-in">&#xE8FF;</i>
              </div>
            </div>
          {/block}

          {block name='product_images'}
            <div id="thumb-gallery" class="product-thumb-images">   
              {foreach from=$product.images item=image}
                <div class="thumb-container {if $image.id_image == $product.cover.id_image} active {/if}">
                  <a href="javascript:void(0)" data-image="{$image.bySize.large_default.url}" data-zoom-image="{$image.bySize.large_default.url}"> 
                    <img
                      class="intense thumb js-thumb {if $image.id_image == $product.cover.id_image} selected {/if}"
                      data-image-medium-src="{$image.bySize.medium_default.url}"
                      data-image-large-src="{$image.bySize.large_default.url}"
                      src="{$image.bySize.home_default.url}"
                      alt="{$image.legend}"
                      title="{$image.legend}"
                      itemprop="image"
                    >
                  </a>
                </div>
              {/foreach}
            </div>
            
      {if $product.images|@count > 1}
              <div class="arrows-product-fake slick-arrows">
                <button class="slick-prev slick-arrow" aria-label="Previous" type="button" >{l s='Previous' d='Shop.Theme.Catalog'}</button>
                <button class="slick-next slick-arrow" aria-label="Next" type="button">{l s='Next' d='Shop.Theme.Catalog'}</button>
              </div>
            {/if}
          {/block}

        {/block}
        {hook h='displayAfterProductThumbs'}
      </div>
    {/block}
  </section>
{/block}

{block name='product_images_modal'}
  {include file='catalog/_partials/product-images-modal.tpl'}
{/block}</div></div><div class="pro-action-col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 col-sp-12"><div class="pro-col-wrapper">
{if $product.is_customizable && count($product.customizations.fields)}
	{block name='product_customization'}
	 	{include file="catalog/_partials/product-customization.tpl" customizations=$product.customizations}
	{/block}
{/if}

<div class="product-actions" data-id-product="{$product.id}">

  {block name='product_buy'}

    <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">

      <input type="hidden" name="token" value="{$static_token}">

      <input type="hidden" name="id_product" value="{$product.id}" id="product_page_product_id">

      <input type="hidden" name="id_customization" value="{$product.id_customization}" id="product_customization_id">



      {block name='product_variants'}

		  

		  {hook h='displayTdkCartAttribute' product=$product page='product' display_type=3}

		  {if !$packItems}

        {hook h='displayTdkSC' sc_key=sc1548080343}

      {/if}

      {/block}



      {block name='product_pack'}

        {if $packItems}

          <section class="product-pack">

            <h3 class="h4">{l s='This pack contains' d='Shop.Theme.Catalog'}</h3>

            {foreach from=$packItems item="product_pack"}

              {block name='product_miniature'}

                {include file='catalog/_partials/miniatures/pack-product.tpl' product=$product_pack}

              {/block}

            {/foreach}

        </section>

        {/if}

      {/block}



      {block name='product_discounts'}

        {include file='catalog/_partials/product-discounts.tpl'}

      {/block}

	  

		<div class="tdk-pro-qty">

			<a href="javascript:void(0)" class="tdk-bt-pro-qty tdk-bt-pro-qty-down"><i class="material-icons">&#xE15B;</i></a>

				{hook h='displayTdkCartQuantity' product=$product}

			<a href="javascript:void(0)" class="tdk-bt-pro-qty tdk-bt-pro-qty-up"><i class="material-icons">&#xE145;</i></a>

		</div>

		

      {block name='product_add_to_cart'}

		  

		  {hook h='displayTdkCartButton' product=$product}

      {/block}



		{hook h='displayTdkWishlistButton' product=$product}

		{hook h='displayTdkCompareButton' product=$product}

		

      {block name='product_refresh'}

        <input class="product-refresh ps-hidden-by-js" name="refresh" type="submit" value="{l s='Refresh' d='Shop.Theme.Actions'}">

      {/block}

    </form>

  {/block}

</div></div></div><div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-xs-12 col-sp-12"><div class="pro-col-wrapper">
{include file="sub/product_info/tab.tpl"}
{block name='product_accessories'}
  {if $accessories}
    <section class="product-accessories clearfix">
      <h3 class="h5 products-section-title">{l s='You might also like' d='Shop.Theme.Catalog'}</h3>
      <div class="products">
        <div class="slick-row">
			<div class="timeline-wrapper clearfix prepare"
				data-item="4" 
				data-xl="4" 
				data-lg="3" 
				data-md="3" 
				data-sm="2" 
				data-m="2"
			>
				{for $foo=1 to 4}			
					<div class="timeline-parent">
						<div class="timeline-item">
							<div class="animated-background">					
								<div class="background-masker content-top"></div>							
								<div class="background-masker content-second-line"></div>							
								<div class="background-masker content-third-line"></div>							
								<div class="background-masker content-fourth-line"></div>
							</div>
						</div>
					</div>
				{/for}
			</div>
			<div class="list-product-slick-carousel slick-slider slick-loading {if isset($productClassWidget)}{$productClassWidget|escape:'html':'UTF-8'}{/if}">
				{foreach from=$accessories item="product_accessory" name=products_accessories name=products_accessories}
					<div class="slick-slide{if $smarty.foreach.products_accessories.index == 0} first{/if}{if $smarty.foreach.products_accessories.last} last{/if}">
						<div class="item">
						  {block name='product_miniature'}
							{if isset($productProfileDefault) && $productProfileDefault}
								
								{hook h='displayTdkProfileProduct' product=$product_accessory profile=$productProfileDefault}
							{else}
								{include file='catalog/_partials/miniatures/product.tpl' product=$product_accessory}
							{/if}
						  {/block}
						</div>
					</div>
				{/foreach}
			</div>
        </div>
      </div>
    </section>
	<script type="text/javascript">
	tdk_list_functions.push(function(){
		$('.product-accessories .list-product-slick-carousel').imagesLoaded( function() {
			$('.product-accessories .list-product-slick-carousel').slick(
				{			
					centerMode: false,
					centerPadding: '60px',
					dots: false,
					infinite: false,
					vertical: false,
					autoplay: false,
					pauseonhover: true,
					autoplaySpeed: 2000,
					arrows: true,
					rows: 1,
					slidesToShow: 4,
					slidesToScroll: 1,
					rtl: {if isset($IS_RTL) && $IS_RTL}true{else}false{/if},
					responsive: [
											{
						  breakpoint: 1200,
						  settings: {
							centerMode: false,
							centerPadding: '60px',
							slidesToShow: 4,
							slidesToScroll: 1,
						  }
						},
											{
						  breakpoint: 992,
						  settings: {
							centerMode: false,
							centerPadding: '60px',
							slidesToShow: 3,
							slidesToScroll: 1,
						  }
						},
											{
						  breakpoint: 768,
						  settings: {
							centerMode: false,
							centerPadding: '60px',
							slidesToShow: 3,
							slidesToScroll: 1,
						  }
						},
											{
						  breakpoint: 576,
						  settings: {
							centerMode: false,
							centerPadding: '60px',
							slidesToShow: 3,
							slidesToScroll: 1,
						  }
						},
											{
						  breakpoint: 575,
						  settings: {
							centerMode: false,
							centerPadding: '60px',
							slidesToShow: 2,
							slidesToScroll: 1,
						  }
						},
											{
						  breakpoint: 400,
						  settings: {
							centerMode: false,
							centerPadding: '60px',
							slidesToShow: 1,
							slidesToScroll: 1,
						  }
						},
										]
				}			
			);
			$('.product-accessories .list-product-slick-carousel').removeClass('slick-loading').addClass('slick-loaded').parents('.slick-row').addClass('hide-loading');
		});
	});
	</script>
  {/if}
{/block}
{block name='product_footer'}
	{hook h='displayFooterProduct' product=$product category=$category}
{/block}</div></div></div>
{block name='page_footer_container'}
	  <footer class="page-footer">
	    {block name='page_footer'}
	    	<!-- Footer content -->
	    {/block}
	  </footer>
	{/block}
</section>

