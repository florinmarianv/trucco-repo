{**

*

* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform

*

* @author    TDK Studio

* @copyright 2018-9999 TDK Studio

* @license   This program is not free software and you can not resell and redistribute it

*

* CONTACT WITH DEVELOPER

* Email/Skype: info.tdkstudio@gmail.com

*

**}

<div class="product-actions" data-id-product="{$product.id}">



  {block name='product_buy'}

{if $groups.$id_attribute_group.attributes_quantity.$id_attribute == 0}disabled{/if}

    <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">

      <input type="hidden" name="token" value="{$static_token}">

      <input type="hidden" name="id_product" value="{$product.id}" id="product_page_product_id">

      <input type="hidden" name="id_customization" value="{$product.id_customization}" id="product_customization_id">



      {block name='product_variants'}

		  {*

        {include file='catalog/_partials/product-variants.tpl'}

		  *}

		  {hook h='displayTdkCartAttribute' product=$product page='product' display_type=}

		  {if !$packItems}

        {hook h='displayTdkSC' sc_key=sc1548080343}

      {/if}

      {/block}

  {hook h='displayTdkSC' sc_key=sc2044386808}



      {block name='product_pack'}

        {if $packItems}

          <section class="product-pack">

            <h3 class="h4">{l s='This pack contains' d='Shop.Theme.Catalog'}</h3>

            {foreach from=$packItems item="product_pack"}

              {block name='product_miniature'}

                {include file='catalog/_partials/miniatures/pack-product.tpl' product=$product_pack}

              {/block}

            {/foreach}

        </section>

        {/if}

      {/block}



      {block name='product_discounts'}

        {include file='catalog/_partials/product-discounts.tpl'}

      {/block}

	  

		<div class="tdk-pro-qty">

			<a href="javascript:void(0)" class="tdk-bt-pro-qty tdk-bt-pro-qty-down"><i class="material-icons">&#xE15B;</i></a>

				{hook h='displayTdkCartQuantity' product=$product}

			<a href="javascript:void(0)" class="tdk-bt-pro-qty tdk-bt-pro-qty-up"><i class="material-icons">&#xE145;</i></a>

		</div>

		

      {block name='product_add_to_cart'}

		  {*

        {include file='catalog/_partials/product-add-to-cart.tpl'}

		  *}

		  {hook h='displayTdkCartButton' product=$product}

      {/block}



		{hook h='displayTdkWishlistButton' product=$product}

		{hook h='displayTdkCompareButton' product=$product}

             
      {block name='product_refresh'}

        <input class="product-refresh ps-hidden-by-js" name="refresh" type="submit" value="{l s='Refresh' d='Shop.Theme.Actions'}">

      {/block}

    </form>

  {/block}

</div>