/**
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*/
var check_slider_rtl;
if (prestashop.language.is_rtl == 1) {check_slider_rtl = true} else {check_slider_rtl = false};
$('.slider-nav').slick({
        centerMode: true,
        centerPadding: '365px',
        slidesToShow: 1, 
        autoplay: true,
		autoplaySpeed: 3500,
        focusOnSelect: true,
        rows: 1,
		slidesToShow: 1,
		slidesToScroll: 1,
		rtl: check_slider_rtl,
        responsive: [
	        {
	          breakpoint: 1800,
	          settings: {
	            centerMode: true,
	            centerPadding: '300px',
	            slidesToShow: 1
	          }
	        },
	        {
	          breakpoint: 1440,
	          settings: {
	            centerMode: true,
	            centerPadding: '160px',
	            slidesToShow: 1
	          }
	        },
	        {
	          breakpoint: 1199,
	          settings: {
	            arrows: false,
	            centerMode: true,
	            centerPadding: '0px',
	            slidesToShow: 1
	          }
	        },
	        {
	          breakpoint: 768,
	          settings: {
	            arrows: false,
	            centerMode: true,
	            centerPadding: '0px',
	            slidesToShow: 1
	          }
	        },
	        {
	          breakpoint: 480,
	          settings: {
	            arrows: false,
	            centerMode: true,
	            centerPadding: '0px',
	            slidesToShow: 1
	          }
	        }
      	]
	});