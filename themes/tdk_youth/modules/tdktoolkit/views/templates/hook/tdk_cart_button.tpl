{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<div class="button-container cart">
	<form action="{$link_cart}" method="post">
		<input type="hidden" name="token" value="{$static_token}">
		<input type="hidden" value="{$tdk_cart_product.quantity}" class="quantity_product quantity_product_{$tdk_cart_product.id_product}" name="quantity_product">
		<input type="hidden" value="{if isset($tdk_cart_product.product_attribute_minimal_quantity) && $tdk_cart_product.product_attribute_minimal_quantity>$tdk_cart_product.minimal_quantity}{$tdk_cart_product.product_attribute_minimal_quantity}{else}{$tdk_cart_product.minimal_quantity}{/if}" class="minimal_quantity minimal_quantity_{$tdk_cart_product.id_product}" name="minimal_quantity">
		<input type="hidden" value="{$tdk_cart_product.id_product_attribute}" class="id_product_attribute id_product_attribute_{$tdk_cart_product.id_product}" name="id_product_attribute">
		<input type="hidden" value="{$tdk_cart_product.id_product}" class="id_product" name="id_product">
		<input type="hidden" name="id_customization" value="{if $tdk_cart_product.id_customization}{$tdk_cart_product.id_customization}{/if}" class="product_customization_id">
			
		<input type="hidden" class="input-group form-control qty qty_product qty_product_{$tdk_cart_product.id_product}" name="qty" value="{if isset($tdk_cart_product.wishlist_quantity)}{$tdk_cart_product.wishlist_quantity}{else}{if $tdk_cart_product.product_attribute_minimal_quantity && $tdk_cart_product.product_attribute_minimal_quantity>$tdk_cart_product.minimal_quantity}{$tdk_cart_product.product_attribute_minimal_quantity}{else}{$tdk_cart_product.minimal_quantity}{/if}{/if}" data-min="{if $tdk_cart_product.product_attribute_minimal_quantity && $tdk_cart_product.product_attribute_minimal_quantity>$tdk_cart_product.minimal_quantity}{$tdk_cart_product.product_attribute_minimal_quantity}{else}{$tdk_cart_product.minimal_quantity}{/if}">
		  <button onclick="showDiv();" class="btn btn-primary TdkBtnProduct add-to-cart tdk-bt-cart tdk-bt-cart_{$tdk_cart_product.id_product}{if !$tdk_cart_product.add_to_cart_url} disabled{/if}" data-button-action="add-to-cart" type="submit">
			<span class="tdk-loading cssload-speeding-wheel"></span>
			<span class="tdk-bt-cart-content">
				<span class="TdkIconBtnProduct icon-cart shopping-cart">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 511.997 511.997" style="enable-background:new 0 0 511.997 511.997;" xml:space="preserve">
		            <path d="M405.387,362.612c-35.202,0-63.84,28.639-63.84,63.84s28.639,63.84,63.84,63.84s63.84-28.639,63.84-63.84    S440.588,362.612,405.387,362.612z M405.387,451.988c-14.083,0-25.536-11.453-25.536-25.536s11.453-25.536,25.536-25.536    c14.083,0,25.536,11.453,25.536,25.536S419.47,451.988,405.387,451.988z"></path>
		            <path d="M507.927,115.875c-3.626-4.641-9.187-7.348-15.079-7.348H118.22l-17.237-72.12c-2.062-8.618-9.768-14.702-18.629-14.702    H19.152C8.574,21.704,0,30.278,0,40.856s8.574,19.152,19.152,19.152h48.085l62.244,260.443    c2.062,8.625,9.768,14.702,18.629,14.702h298.135c8.804,0,16.477-6.001,18.59-14.543l46.604-188.329    C512.849,126.562,511.553,120.516,507.927,115.875z M431.261,296.85H163.227l-35.853-150.019h341.003L431.261,296.85z"></path>
		            <path d="M173.646,362.612c-35.202,0-63.84,28.639-63.84,63.84s28.639,63.84,63.84,63.84s63.84-28.639,63.84-63.84    S208.847,362.612,173.646,362.612z M173.646,451.988c-14.083,0-25.536-11.453-25.536-25.536s11.453-25.536,25.536-25.536    s25.536,11.453,25.536,25.536S187.729,451.988,173.646,451.988z"></path>
		          </svg>
				</span>
				<span class="TdkNameBtnProduct">{l s='Add to cart' d='Shop.Theme.Global'}</span>
			</span>
		  </button>
	</form>
</div>

