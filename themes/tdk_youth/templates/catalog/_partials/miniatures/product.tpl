{**
 * 2007-2020 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='product_miniature_item'}
<article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
	<div class="thumbnail-container">
		<div class="product-image">
			{block name='product_thumbnail'}
				{if isset($cfg_product_list_image) && $cfg_product_list_image}
					<div class="tdk-more-info" data-idproduct="{$product.id_product}"></div>
				{/if}
				{if $product.cover}
					<a href="{$product.url}" class="thumbnail product-thumbnail">
						<img
							class="img-fluid"
							src = "{$product.cover.bySize.home_default.url}"
							alt = "{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
							data-full-size-image-url = "{$product.cover.large.url}"
						> 
						{if isset($cfg_product_one_img) && $cfg_product_one_img}
							<span class="product-additional" data-idproduct="{$product.id_product}"></span>
						{/if}
					</a>
				{else}
					<a href="{$product.url}" class="thumbnail product-thumbnail">
			            <img
			              src = "{$urls.no_picture_image.bySize.home_default.url}"
			            />
					    {if isset($cfg_product_one_img) && $cfg_product_one_img}
					    	<span class="product-additional" data-idproduct="{$product.id_product}"></span>
					    {/if}
				    </a>
				{/if}	
			{/block}
			<!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->
			      {block name='product_flags'}
			        <ul class="product-flags">
			          {foreach from=$product.flags item=flag}
			            <li class="product-flag {$flag.type}">{$flag.label}</li>
			          {/foreach}
			        </ul>
			      {/block}
			<div class="functional-buttons clearfix">
				{hook h='displayTdkCartAttribute' product=$product}
				{hook h='displayTdkCartQuantity' product=$product}
				{hook h='displayTdkCartButton' product=$product}
			</div>
		</div>
		<div class="product-meta">
			<div class="TdkProductInfo">
				{block name='product_name'}
		          	{if $page.page_name == 'index'}
		            	<h3 class="h3 product-title" itemprop="name"><a href="{$product.url}">{$product.name|truncate:30:'...'}</a></h3>
		          	{else}
		            	<h2 class="h3 product-title" itemprop="name"><a href="{$product.url}">{$product.name|truncate:30:'...'}</a></h2>
		          	{/if}
				{/block}
				<div class="TdkButtonAction clearfix">
					{hook h='displayTdkCompareButton' product=$product}
					{hook h='displayTdkWishlistButton' product=$product}
				</div>
			</div>
			<div class="product-description">
				{hook h='displayTdkProductListReview' product=$product}
				{block name='product_reviews'}
					{hook h='displayProductListReviews' product=$product}
				{/block}
				{block name='product_price_and_shipping'}
					{if $product.show_price}
						<div class="product-price-and-shipping">
							{if $product.has_discount}
								{hook h='displayProductPriceBlock' product=$product type="old_price"}
								<span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>
								<span class="regular-price">{$product.regular_price}</span>
								{if $product.discount_type === 'percentage'}
									<span class="discount-percentage discount-product">{$product.discount_percentage}</span>
								{elseif $product.discount_type === 'amount'}
									<span class="discount-amount discount-product">{$product.discount_amount_to_display}</span>
								{/if}
							{/if}

							{hook h='displayProductPriceBlock' product=$product type="before_price"}

							<span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>
							<span itemprop="price" class="price">{$product.price}</span>

							{hook h='displayProductPriceBlock' product=$product type='unit_price'}

							{hook h='displayProductPriceBlock' product=$product type='weight'}
						</div>
					{/if}
				{/block}
				
				{block name='product_description_short'}
					<div class="product-description-short" itemprop="description">{$product.description_short|truncate:150:'...' nofilter}{* HTML form , no escape necessary *}</div>
				{/block}

			</div>
			<div class="highlighted-informations{if !$product.main_variants} no-variants{/if} hidden-sm-down">
				{block name='product_variants'}
					{if $product.main_variants}
						{include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}
					{/if}
				{/block}
			</div>
			{block name='quick_view'}
				<div class="quickview{if !$product.main_variants} no-variants{/if} hidden-sm-down">
					<a
					  href="#"
					  class="quick-view TdkBtnProduct btn"
					  data-link-action="quickview"
					  data-source=".thumb-gallery-{$product.id}-{$product.id_product_attribute}"
					  title="{l s='Quick view' d='Shop.Theme.Actions'}"
					>
						<span class="tdk-quickview-bt-loading cssload-speeding-wheel"></span>
						<span class="tdk-quickview-bt-content">
							<span class="TdkIconBtnProduct icon-quick-view">
								<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 451 451" style="enable-background:new 0 0 451 451;" xml:space="preserve">
									<path d="M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3
										s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4
										C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3
										s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z"></path>
								</svg>
							</span>
							<span class="TdkNameBtnProduct">{l s='Quick view' d='Shop.Theme.Actions'}</span>
						</span>
					</a>
				</div>
			{/block}
		</div>
	</div>
</article>
{/block}
