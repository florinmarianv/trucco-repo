{**
 * 2007-2020 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<!doctype html>
<html lang="{$language.iso_code}" {if isset($IS_RTL) && $IS_RTL} dir="rtl"{if isset($TDK_RTL) && $TDK_RTL} class="rtl{if isset($TDK_DEFAULT_SKIN)} {$TDK_DEFAULT_SKIN}{/if}"{/if}
{else} class="{if isset($TDK_DEFAULT_SKIN)}{$TDK_DEFAULT_SKIN}{/if}" {/if}>

  <head>
    {block name='head'}
      {include file='_partials/head.tpl'}
    {/block}
  </head>

  <body id="{$page.page_name}" class="{$page.body_classes|classnames}{if isset($TDK_LAYOUT_MODE)} {$TDK_LAYOUT_MODE}{/if}{if isset($USE_FHEADER) && $USE_FHEADER} keep-header{/if}">
    <div id="TdkMainPage">
      {block name='hook_after_body_opening_tag'}
        {hook h='displayAfterBodyOpeningTag'}
      {/block}

      <header id="TdkHeader">
        <div class="TdkHeaderContainer">
          {block name='header'}
            {include file='checkout/_partials/header.tpl'}
          {/block}
        </div>
      </header>

      {block name='notifications'}
        {include file='_partials/notifications.tpl'}
      {/block}

      <section id="wrapper">
        {hook h="displayWrapperTop"}
        <div class="container">
        {block name='content'}
          <section id="content">
            <div class="row">
              <div class="cart-grid-body col-xs-12 col-lg-8">
                {block name='cart_summary'}
                  {render file='checkout/checkout-process.tpl' ui=$checkout_process}
                {/block}
              </div>
              <div class="cart-grid-right col-xs-12 col-lg-4">
                {block name='cart_summary'}
                  {include file='checkout/_partials/cart-summary.tpl' cart = $cart}
                {/block}
                {hook h='displayReassurance'}
              </div>
            </div>
          </section>
        {/block}
        </div>
        {hook h="displayWrapperBottom"}
      </section>

      <footer id="TdkFooter" class="TdkFooterContainer">
        {block name='footer'}
          {include file='checkout/_partials/footer.tpl'}
        {/block}
      </footer>

      {if isset($TDK_BACKTOP) && $TDK_BACKTOP}
        <div id="TdkBackTop">
          <a href="#">
            <span class="tdk-icon-backtotop">
              <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31.479 31.479" style="enable-background:new 0 0 31.479 31.479;" xml:space="preserve">
                <path d="M26.477,10.274c0.444,0.444,0.444,1.143,0,1.587c-0.429,0.429-1.143,0.429-1.571,0l-8.047-8.047
                v26.555c0,0.619-0.492,1.111-1.111,1.111c-0.619,0-1.127-0.492-1.127-1.111V3.813l-8.031,8.047c-0.444,0.429-1.159,0.429-1.587,0
                c-0.444-0.444-0.444-1.143,0-1.587l9.952-9.952c0.429-0.429,1.143-0.429,1.571,0L26.477,10.274z"/>
              </svg>
            </span>
            <span class="tdk-text-backtotop">{l s='Back to top' d='Shop.Theme.Global'}</span>
          </a>
        </div>
      {/if}
      <div id="TdkCookieLaw">
        {hook h='displayTdkSC' sc_key=sc1537544295}
      </div>
    </div>

    {block name='javascript_bottom'}
      {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
    {/block}

    {block name='hook_before_body_closing_tag'}
      {hook h='displayBeforeBodyClosingTag'}
    {/block}
  </body>
</html>