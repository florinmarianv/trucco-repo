{**
 * 2007-2020 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<!doctype html>
<html lang="{$language.iso_code}" {if isset($IS_RTL) && $IS_RTL} dir="rtl"{if isset($TDK_RTL) && $TDK_RTL} class="rtl{if isset($TDK_DEFAULT_SKIN)} {$TDK_DEFAULT_SKIN}{/if}"{/if}
{else} class="{if isset($TDK_DEFAULT_SKIN)}{$TDK_DEFAULT_SKIN}{/if}" {/if}>
  <head>
    {block name='head'}
      {include file='_partials/head.tpl'}
    {/block}
  </head>
  <body id="{$page.page_name}" data-profile-key="{$TDK_PROFILE_KEY}" class="{$page.body_classes|classnames}{if isset($TDK_LAYOUT_MODE)} {$TDK_LAYOUT_MODE}{/if}{if isset($TDK_CLASS_DETAIL)} {$TDK_CLASS_DETAIL}{/if}{if isset($USE_FHEADER) && $USE_FHEADER} keep-header{/if}">

    {block name='hook_after_body_opening_tag'}
      {hook h='displayAfterBodyOpeningTag'}
    {/block}

    <main id="TdkMainPage">
      {block name='product_activation'}
        {include file='catalog/_partials/product-activation.tpl'}
      {/block}
      <header id="TdkHeader">
        <div class="TdkHeaderContainer">
          {block name='header'}
            {include file='_partials/header.tpl'}
          {/block}
        </div>
      </header>
      {block name='notifications'}
        {include file='_partials/notifications.tpl'}
      {/block}
      <section id="wrapper">
        {hook h="displayWrapperTop"}
        {if $page.page_name == 'cms'}
          <div class="tdk-breadcrumb-cms">

          {block name='page_title'}
            <header class="page-header">{$cms.meta_title}</header>
          {/block}
        {/if}
        {block name='breadcrumb'}
          {include file='_partials/breadcrumb.tpl'}
        {/block}
        {if $page.page_name == 'cms'}
          </div>
        {/if}
        {if isset($fullwidth_hook.displayHome) AND $fullwidth_hook.displayHome == 0}
          <div class="container">
        {/if}
          <div class="row">
            {block name="left_column"}
              <div id="left-column" class="sidebar col-xs-12 col-sm-12 col-md-4 col-lg-3">
                {if $page.page_name == 'product'}
                  {hook h='displayLeftColumnProduct'}
                {else}
                  {hook h="displayLeftColumn"}
                {/if}
              </div>
            {/block}

            {block name="content_wrapper"}
              <div id="content-wrapper" class="left-column right-column col-sm-4 col-md-6">
		            {hook h="displayContentWrapperTop"}
                {block name="content"}
                  <p>Hello world! This is HTML5 Boilerplate.</p>
                {/block}
		            {hook h="displayContentWrapperBottom"}
              </div>
            {/block}

            {block name="right_column"}
              <div id="right-column" class="sidebar col-xs-12 col-sm-12 col-md-4 col-lg-3">
                {if $page.page_name == 'product'}
                  {hook h='displayRightColumnProduct'}
                {else}
                  {hook h="displayRightColumn"}
                {/if}
              </div>
            {/block}
          </div>
        {if isset($fullwidth_hook.displayHome) AND $fullwidth_hook.displayHome == 0}
          </div>
        {/if}
	      {hook h="displayWrapperBottom"}
      </section>

      <footer id="TdkFooter" class="TdkFooterContainer">
        {block name="footer"}
          {include file="_partials/footer.tpl"}
        {/block}
      </footer>
      {if isset($TDK_PANELTOOL) && $TDK_PANELTOOL}
        {include file="$tpl_dir./modules/tdkplatform/views/templates/front/info/paneltool.tpl"}
      {/if}
      {if isset($TDK_BACKTOP) && $TDK_BACKTOP}
        <div id="TdkBackTop">
          <a href="#">
            <span class="tdk-icon-backtotop">
              <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31.479 31.479" style="enable-background:new 0 0 31.479 31.479;" xml:space="preserve">
                <path d="M26.477,10.274c0.444,0.444,0.444,1.143,0,1.587c-0.429,0.429-1.143,0.429-1.571,0l-8.047-8.047
                v26.555c0,0.619-0.492,1.111-1.111,1.111c-0.619,0-1.127-0.492-1.127-1.111V3.813l-8.031,8.047c-0.444,0.429-1.159,0.429-1.587,0
                c-0.444-0.444-0.444-1.143,0-1.587l9.952-9.952c0.429-0.429,1.143-0.429,1.571,0L26.477,10.274z"/>
              </svg>
            </span>
            <span class="tdk-text-backtotop">{l s='Back to top' d='Shop.Theme.Global'}</span>
          </a>
        </div>
      {/if}
      <div id="TdkCookieLaw">
        {hook h='displayTdkSC' sc_key=sc1537544295}
      </div>
    </main>

    {block name='javascript_bottom'}
      {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
    {/block}

    {block name='hook_before_body_closing_tag'}
      {hook h='displayBeforeBodyClosingTag'}
    {/block}
  </body>
</html>