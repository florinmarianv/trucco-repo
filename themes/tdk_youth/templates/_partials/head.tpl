
{block name='head_charset'}
  <meta charset="utf-8">
{/block}
{block name='head_ie_compatibility'}
  <meta http-equiv="x-ua-compatible" content="ie=edge">
{/block}

{block name='head_seo'}
  <title>{block name='head_seo_title'}{$page.meta.title}{/block}</title>
{hook h="displayAfterTitle"}
  <meta name="description" content="{block name='head_seo_description'}{$page.meta.description}{/block}">
  <meta name="keywords" content="{block name='head_seo_keywords'}{$page.meta.keywords}{/block}">
  {if $page.meta.robots !== 'index'}
    <meta name="robots" content="{$page.meta.robots}">
  {/if}
  {if $page.canonical}
    <link rel="canonical" href="{$page.canonical}">
  {/if}
  {block name='head_hreflang'}
      {foreach from=$urls.alternative_langs item=pageUrl key=code}
            <link rel="alternate" href="{$pageUrl}" hreflang="{$code}">
      {/foreach}
  {/block}
{/block}

{block name='head_viewport'}
  <meta name="viewport" content="width=device-width, initial-scale=1">
{/block}

{block name='head_icons'}
  <link rel="icon" type="image/vnd.microsoft.icon" href="{$shop.favicon}?{$shop.favicon_update_time}">
  <link rel="shortcut icon" type="image/x-icon" href="{$shop.favicon}?{$shop.favicon_update_time}">
{/block}
{block name="setting"}
  {include file="layouts/setting.tpl"}
{/block}
{block name='stylesheets'}
  {include file="_partials/stylesheets.tpl" stylesheets=$stylesheets}
{/block}

{* TDK - Load Css With Prestashop Standard *}
{if isset($LOAD_CSS_TYPE) && !$LOAD_CSS_TYPE}
   
    {if isset($TDK_CSS)}
        {foreach from=$TDK_CSS key=css_uri item=media}
          <link rel="stylesheet" href="{$css_uri}" type="text/css" media="{$media}" />
        {/foreach}
    {/if}
    
    {if isset($TDK_SKIN_CSS)}
        {foreach from=$TDK_SKIN_CSS item=linkCss}
            {$linkCss nofilter}{* HTML form , no escape necessary *}
        {/foreach}
    {/if}
	
{/if}
{* TDK LAYOUT *}
{if isset($LAYOUT_WIDTH)}
    {$LAYOUT_WIDTH nofilter}{* HTML form , no escape necessary *}
{/if}

{block name='javascript_head'}
  {include file="_partials/javascript.tpl" javascript=$javascript.head vars=$js_custom_vars}
{/block}

{block name='hook_extra'}

{/block}

{block name='hook_header'}
  {$HOOK_HEADER nofilter}{* HTML form , no escape necessary *}
{/block}

