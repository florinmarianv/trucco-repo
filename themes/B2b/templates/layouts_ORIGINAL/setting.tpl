{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
{assign var="ENABLE_RESPONSIVE" value="1" scope="global"}
{assign var="LISTING_GRID_MODE" value="grid" scope="global"}
{assign var="LISTING_PRODUCT_COLUMN_MODULE" value="4" scope="global"}
{assign var="LISTING_PRODUCT_COLUMN" value="3" scope="global"}
{assign var="LISTING_PRODUCT_LARGEDEVICE" value="2" scope="global"}
{assign var="LISTING_PRODUCT_TABLET" value="3" scope="global"}
{assign var="LISTING_PRODUCT_SMALLDEVICE" value="2" scope="global"}
{assign var="LISTING_PRODUCT_EXTRASMALLDEVICE" value="2" scope="global"}
{assign var="LISTING_PRODUCT_MOBILE" value="1" scope="global"}
