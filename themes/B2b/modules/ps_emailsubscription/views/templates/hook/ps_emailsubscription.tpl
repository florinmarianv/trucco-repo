{**
 * 2007-2020 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

<div class="block_newsletter block">
  <p id="block-newsletter-label" class="title_block">{l s='Newsletter signup' d='Shop.Theme.Global'}</p>
  <form action="{$tdk_base_url}#footer" method="post">
    <div class="group-newsletter">
      {hook h='displayTdkSC' sc_key=sc1546695032}
      <div class="form-group">
        <button
          class="btn btn-primary"
          name="submitNewsletter"
          type="submit"
          value="{l s='Subscribe' d='Shop.Theme.Actions'}"
        >
		      <span class="tdk-icon-newsletter">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
              <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
              C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
              c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/>
            </svg>
          </span>
          <span>{l s='Subscribe' d='Shop.Theme.Actions'}</span>
        </button>
        <div class="input-wrapper">
          <input
            name="email"
            type="email"
            value="{$value}"
            placeholder="{l s='Your email address' d='Shop.Forms.Labels'}"
            aria-labelledby="block-newsletter-label"
          >
        </div>
        <input type="hidden" name="action" value="0">
        <div class="clearfix"></div>
      </div>
      <div class="alert-group">
        <div class="alert-newsletter">
          {if $conditions}
            <p class="conditions-newsletter">{$conditions}</p>
          {/if}
          <div class="conditions-newsletter-popup">{l s='You also receive our exclusive promotions and offer. Cancel anytime, Don.t worry!' d='Shop.Theme.Global'}</div>
          {if $msg}
            <p class="alert {if $nw_error}alert-danger{else}alert-success{/if}">
              {$msg}
            </p>
          {/if}
          {if isset($id_module)}
            {hook h='displayGDPRConsent' id_module=$id_module}
          {/if}
        </div>
      </div>
    </div>
  </form>
</div>