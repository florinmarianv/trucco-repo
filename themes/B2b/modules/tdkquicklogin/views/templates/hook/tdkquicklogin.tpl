{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{if $isLogged}
<div class="quick-login-selector links dropdown js-dropdown TdkPopupOver">
  	<a href="javascript:void(0)" data-toggle="dropdown" class="TdkPopupTitle" title="{l s='My Account' d='Shop.Theme.Global'}">
	    <i class="icons icon-user"></i>
 	</a>
 	<ul class="TdkPopupContent dropdown-menu user-info">
		<li>
			<a class="account" href="{$my_account_url}" title="{l s='My Account' d='Shop.Theme.Global'}" rel="nofollow">
		        <span>{l s='Hello' d='Shop.Theme.Global'} {$customerName}</span>
			</a>
		</li>
		<li>
			<a class="account" href="{$my_account_url}" title="{l s='My Account' d='Shop.Theme.Global'}" rel="nofollow">
				<i class="icons icon-user"></i> <span>{l s='My Account' d='Shop.Theme.Global'}</span>
			</a>
		</li>
		<li>
			<a class="logout" href="{$logout_url}" rel="nofollow" title="{l s='Sign out' d='Shop.Theme.Global'}">
		        <i class="icons icon-logout"></i> {l s='Sign out' d='Shop.Theme.Actions'}
			</a>
		</li>
	</ul>
</div>
{else}
	{if $tdk_type == 'html'}
		{$html_form nofilter}
	{else}
		{if $tdk_type == 'dropdown'}
			<div class="dropdown">
		{/if}
		{if $tdk_type == 'dropup'}
			<div class="dropup">
		{/if}
				<a href="javascript:void(0)" class="tdkql-quicklogin{if $tdk_type == 'dropdown' || $tdk_type == 'dropup'} tdkql-dropdown dropdown-toggle{/if}" data-enable-sociallogin="{if isset($enable_sociallogin)}{$enable_sociallogin}{/if}" data-type="{$tdk_type}" data-layout="{$tdk_layout}"{if $tdk_type == 'dropdown' || $tdk_type == 'dropup'} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"{/if} title="{l s='Quick Login' d='Shop.Theme.Global'}" rel="nofollow">
					<i class="icons icon-user"></i>
				</a>
		{if $tdk_type == 'dropdown' || $tdk_type == 'dropup'}
				<div class="dropdown-menu tdkql-dropdown-wrapper">
					{$html_form nofilter}
				</div>
			</div>
		{/if}
	{/if}
{/if}