{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\BlogItem -->
<div class="blog-container TdkBlogContainer" itemscope itemtype="https://schema.org/Blog">
    <div class="blogPostImage">
        <div class="blogImage">
            <a class="blogLinkImage" href="{$blog.link|escape:'html':'UTF-8'}" title="{$blog.title|escape:'html':'UTF-8'}" itemprop="url">
				{if isset($formAtts.btdkblogs_sima) && $formAtts.btdkblogs_sima}
					<img class="img-fluid" src="{if (isset($blog.preview_thumb_url) && $blog.preview_thumb_url != '')}{$blog.preview_thumb_url}{else}{$blog.preview_url}{/if}{*full url can not escape*}" 
						alt="{if !empty($blog.legend)}{$blog.legend|escape:'html':'UTF-8'}{else}{$blog.title|escape:'html':'UTF-8'}{/if}" 
						title="{if !empty($blog.legend)}{$blog.legend|escape:'html':'UTF-8'}{else}{$blog.title|escape:'html':'UTF-8'}{/if}" 
						{if isset($formAtts.btdkblogs_width)}width="{$formAtts.btdkblogs_width|escape:'html':'UTF-8'}" {/if}
						{if isset($formAtts.btdkblogs_height)} height="{$formAtts.btdkblogs_height|escape:'html':'UTF-8'}"{/if}
						itemprop="image" />
				{/if}
            </a>
        </div>
    </div>
    <div class="blogPostContent">

        <div class="blogMeta">
			{if isset($formAtts.btdkblogs_saut) && $formAtts.btdkblogs_saut}
				<span class="blogAuthor">
					<span class="icon-author">{l s='By' d='Shop.Theme.Global'}</span> {if $blog.author_name != ''}{$blog.author_name|escape:'html':'UTF-8'}{else}{$blog.author|escape:'html':'UTF-8'}{/if}
				</span>
			{/if}		
			{if isset($formAtts.btdkblogs_scat) && $formAtts.btdkblogs_scat}
				<span class="blogCat"> 
					<span class="icon-list">{l s='in' d='Shop.Theme.Global'}</span> 
					<a href="{$blog.category_link}{*full url can not escape*}" title="{$blog.category_title|escape:'html':'UTF-8'}">{$blog.category_title|escape:'html':'UTF-8'}</a>
				</span>
			{/if}
			{if isset($formAtts.btdkblogs_scre) && $formAtts.btdkblogs_scre}
				<span class="blogCalendar">
					<span class="icon-calendar"> {l s='in' d='Shop.Theme.Global'} </span> 
					<time class="date" datetime="{strtotime($blog.date_add)|date_format:"%Y"}{*convert to date time*}">		
						{assign var='blog_month' value=strtotime($blog.date_add)|date_format:"%B"}
						{l s=$blog_month mod='tdkplatform'}
						{assign var='blog_date_add' value=strtotime($blog.date_add)|date_format:"%d"}<!-- day of month -->
						{assign var='blog_day' value=strtotime($blog.date_add)|date_format:"%e"}
						{l s=$blog_day mod='tdkplatform'}{assign var='blog_daycount' value=$formAtts.tdk_blog_helper->string_ordinal($blog_date_add)}{l s=$blog_daycount mod='tdkplatform'},
						{assign var='blog_year' value=strtotime($blog.date_add)|date_format:"%Y"}						
						{l s=$blog_year mod='tdkplatform'}	<!-- year -->
					</time>
				</span>
			{/if}
			{if isset($formAtts.btdkblogs_scoun) && $formAtts.btdkblogs_scoun}
				<span class="blogNbcomment">
					<span class="icon-comment"> {l s='Comment' d='Shop.Theme.Global'}</span> {$blog.comment_count|intval}
				</span>
			{/if}
			
			{if isset($formAtts.btdkblogs_shits) && $formAtts.btdkblogs_shits}
				<span class="blogHits">
					<span class="icon-hits"> {l s='Hits' d='Shop.Theme.Global'}</span> {$blog.hits|intval}
				</span>	
			{/if}
		</div>
        {if isset($formAtts.show_title) && $formAtts.show_title}
        	<h5 class="blogTitle" itemprop="name"><a href="{$blog.link}{*full url can not escape*}" title="{$blog.title|escape:'html':'UTF-8'}">{$blog.title|strip_tags:'UTF-8'|truncate:80:'...'}</a></h5>
        {/if}
		{if isset($formAtts.show_desc) && $formAtts.show_desc}
	        <div class="blogDesc" itemprop="description">
	            {$blog.description|strip_tags:'UTF-8'|truncate:160:'...'}
	        </div>
        {/if}
        <div class="blogReadmore">
            <a href="{$blog.link}{*full url can not escape*}" title="{$blog.title|escape:'html':'UTF-8'}">{l s='Read more' d='Shop.Theme.Global'}</a>
        </div>
    </div>
	
	<div class="hidden-xl-down hidden-xl-up datetime-translate">
		{l s='Sunday' d='Shop.Theme.Global'}
		{l s='Monday' d='Shop.Theme.Global'}
		{l s='Tuesday' d='Shop.Theme.Global'}
		{l s='Wednesday' d='Shop.Theme.Global'}
		{l s='Thursday' d='Shop.Theme.Global'}
		{l s='Friday' d='Shop.Theme.Global'}
		{l s='Saturday' d='Shop.Theme.Global'}
		
		{l s='January' d='Shop.Theme.Global'}
		{l s='February' d='Shop.Theme.Global'}
		{l s='March' d='Shop.Theme.Global'}
		{l s='April' d='Shop.Theme.Global'}
		{l s='May' d='Shop.Theme.Global'}
		{l s='June' d='Shop.Theme.Global'}
		{l s='July' d='Shop.Theme.Global'}
		{l s='August' d='Shop.Theme.Global'}
		{l s='September' d='Shop.Theme.Global'}
		{l s='October' d='Shop.Theme.Global'}
		{l s='November' d='Shop.Theme.Global'}
		{l s='December' d='Shop.Theme.Global'}
		
		{l s='st' d='Shop.Theme.Global'}
		{l s='nd' d='Shop.Theme.Global'}
		{l s='rd' d='Shop.Theme.Global'}
		{l s='th' d='Shop.Theme.Global'}
	</div>
</div>
