{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkCategoryImage -->
{function name=tdkmenu level=0}
<ul class="level{$level|intval} {if $level == 0} ul-{$random|escape:'html':'UTF-8'}{/if}">
{foreach $data as $category}
	{if isset($category.children) && is_array($category.children)}
	<li class="cate_{$category.id_category|intval}" >
		<a href="{$link->getCategoryLink($category.id_category, $category.link_rewrite)|escape:'html':'UTF-8'}">
			<span class="cate_content">
				<span class="cover-img">
					{if isset($category.image)}
					<img class="img-fluid" src='{$category["image"]|escape:'html':'UTF-8'}' alt='{$category["name"]|escape:'html':'UTF-8'}' 
						 {if $formAtts.showicons == 0 || ($level gt 0 && $formAtts.showicons == 2)} style="display:none"{/if}/>
					{/if}
				</span>
				<span class="cate-name">{$category.name|escape:'html':'UTF-8'}</span>
			</span>
		</a>
		{tdkmenu data=$category.children level=$level+1}
	</li>
	{else}
	<li class="cate_{$category.id_category|intval} cate-no-child">
		<a href="{$link->getCategoryLink($category.id_category, $category.link_rewrite)|escape:'html':'UTF-8'}">
			<span class="cate_content">
				<span class="cover-img">
					{if isset($category.image)}
					<img class="img-fluid" src='{$category["image"]|escape:'html':'UTF-8'}' alt='{$category["name"]|escape:'html':'UTF-8'}' 
						 {if $formAtts.showicons == 0 || ($level gt 0 && $formAtts.showicons == 2)} style="display:none"{/if}/>
					{/if}
				</span>
				<span class="cate-name-nochild">{$category.name|escape:'html':'UTF-8'}</span>
			</span>
		</a>
	</li>
	{/if}
{/foreach}
</ul>
{/function}

{if isset($categories)}
<div class="TdkWidgetCategoryImage TdkBlock block {if isset($formAtts.class)}{$formAtts.class|escape:'html':'UTF-8'}{/if}">
	{($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
	{if isset($formAtts.sub_title) && $formAtts.sub_title}
        <div class="TdkGroupTitle">
    {/if}
		{if isset($formAtts.title) && !empty($formAtts.title)}
			<h4 class="TdkTitleBlock">
				{$formAtts.title|escape:'html':'UTF-8'}
			</h4>
		{/if}
	    {if isset($formAtts.sub_title) && $formAtts.sub_title}
	        <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
	    {/if}
	{if isset($formAtts.sub_title) && $formAtts.sub_title}
        </div>
    {/if}
	<div class="TdkBlockContent">
		{foreach from = $categories key=key item =cate}
			{tdkmenu data=$cate}
		{/foreach}
		<div id="view_all_wapper_{$random|escape:'html':'UTF-8'}" class="view_all_wapper hide">
			<a class="btn btn-primary view_all" href="javascript:void(0)">{l s='View all' d='Shop.Theme.Global'}</a>
		</div> 
	</div>
	{($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
</div>
{/if}
<script type="text/javascript">
{literal} 
	tdk_list_functions.push(function(){
		$(".view_all_wapper").hide();
		var limit = {/literal}{$formAtts.limit|intval}{literal};
		var level = {/literal}{$formAtts.cate_depth|intval}{literal} - 1;
		$("ul.ul-{/literal}{$random|escape:'html':'UTF-8'}, ul.ul-{$random|escape:'html':'UTF-8'} ul"{literal}).each(function(){
			var element = $(this).find(">li").length;
			var count = 0;
			$(this).find(">li").each(function(){
				count = count + 1;
				if(count > limit){
					// $(this).remove();
					$(this).hide();
				}
			});
			if(element > limit) {
				view = $(".view_all","#view_all_wapper_{/literal}{$random|escape:'html':'UTF-8'}"){literal}.clone(1);
				// view.appendTo($(this).find("ul.level" + level));
				view.appendTo($(this));
				var href = $(this).closest("li").find('a:first-child').attr('href');
				$(view).attr("href", href);
			}
		})
	});
{/literal}
</script>