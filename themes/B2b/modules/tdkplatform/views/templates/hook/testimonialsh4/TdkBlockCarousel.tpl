{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkBlockCarousel -->
{if isset($formAtts.lib_has_error) && $formAtts.lib_has_error}
    {if isset($formAtts.lib_error) && $formAtts.lib_error}
        <div class="alert alert-warning tdk-lib-error">{$formAtts.lib_error}</div>
    {/if}
{else}
    <div class="block TdkBlock TdkWidgetBlockCarousel block_carousel exclusive {(isset($formAtts.class)) ? $formAtts.class : ''|escape:'html':'UTF-8'}">
        {($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
        <div class="TdkBlockContent">
            {if !empty($formAtts.slides)}
                {if $formAtts.carousel_type == "slickcarousel"}
                    {assign var=tdk_include_file value=$tdk_helper->getTplTemplate('TdkBlockSlickCarouselItem.tpl', $formAtts['override_folder'])}
                    {include file=$tdk_include_file}
                {else}
                    {if $formAtts.carousel_type == 'boostrap'}
                        {assign var=tdk_include_file value=$tdk_helper->getTplTemplate('TdkBlockCarouselItem.tpl', $formAtts['override_folder'])}
                        {include file=$tdk_include_file}
                    {else}
                        {assign var=tdk_include_file value=$tdk_helper->getTplTemplate('TdkBlockOwlCarouselItem.tpl', $formAtts['override_folder'])}
                        {include file=$tdk_include_file}
                    {/if}
                {/if}
            {else}
                <p class="alert alert-info">{l s='No slide at this time.' d='Shop.Theme.Global'}</p>
            {/if}
        </div>
        {($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
    </div>
{/if}