{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\front\license.tpl -->
<article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
  <div class="thumbnail-container">
    <div class="product-image">
<!-- @file modules\tdkplatform\views\templates\front\products\file_tpl -->
{block name='product_thumbnail'}
<a href="{$product.url}" class="thumbnail product-thumbnail">
  <img
    class="img-fluid"
	src = "{$product.cover.bySize.home_default.url}"
	alt = "{$product.cover.legend}"
	data-full-size-image-url = "{$product.cover.large.url}"
  >
  {if isset($cfg_product_one_img) && $cfg_product_one_img}
	<span class="product-additional" data-idproduct="{$product.id_product}"></span>
  {/if}
</a> 
{/block}<div class="functional-buttons clearfix">
<!-- @file modules\tdkplatform\views\templates\front\products\file_tpl -->
{hook h='displayTdkCartButton' product=$product}

<!-- @file modules\tdkplatform\views\templates\front\products\file_tpl -->
<div class="quickview{if !$product.main_variants} no-variants{/if} hidden-sm-down">
<a
  href="#"
  class="quick-view btn TdkBtnProduct"
  data-link-action="quickview"
>
	<span class="tdk-quickview-bt-loading cssload-speeding-wheel"></span>
	<span class="tdk-quickview-bt-content">
		<span class="TdkIconBtnProduct icon-quick-view">
			<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 451 451" style="enable-background:new 0 0 451 451;" xml:space="preserve">
				<path d="M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3
					s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4
					C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3
					s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z"></path>
			</svg>
		</span>
		<span class="TdkNameBtnProduct">{l s='Quick view' d='Shop.Theme.Actions'}</span>
	</span>
</a>
</div>
</div></div>
    <div class="product-meta">
<!-- @file modules\tdkplatform\views\templates\front\products\file_tpl -->
{hook h='displayTdkProductListReview' product=$product}
<div class="TdkProductInfo">
<!-- @file modules\tdkplatform\views\templates\front\products\file_tpl -->
{block name='product_name'}
  <h5 class="h4 product-title" itemprop="name"><a href="{$product.url}">{$product.name|truncate:80:'...'}</a></h5>
{/block}
<div class="TdkButtonAction">
<!-- @file modules\tdkplatform\views\templates\front\products\file_tpl -->
{hook h='displayTdkCompareButton' product=$product}

<!-- @file modules\tdkplatform\views\templates\front\products\file_tpl -->
{hook h='displayTdkWishlistButton' product=$product}
</div></div>
<!-- @file modules\tdkplatform\views\templates\front\products\file_tpl -->
{block name='product_price_and_shipping'}
  {if $product.show_price}
    <div class="product-price-and-shipping {if $product.has_discount}has_discount{/if}">
      {if $product.has_discount}
        {hook h='displayProductPriceBlock' product=$product type="old_price"}
        <span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>
        <span class="regular-price">{$product.regular_price}</span>
        {if $product.discount_type === 'percentage'}
          <span class="discount-percentage">{$product.discount_percentage}</span>
        {elseif $product.discount_type === 'amount'}
          <span class="discount-amount discount-product">{$product.discount_amount_to_display}</span>
        {/if}
      {/if}

      {hook h='displayProductPriceBlock' product=$product type="before_price"}
      
      <span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>
      <span class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
        <span itemprop="priceCurrency" content="{$currency.iso_code}"></span><span itemprop="price" content="{$product.price_amount}">{$product.price}</span>
      </span>

      {hook h='displayProductPriceBlock' product=$product type='unit_price'}

      {hook h='displayProductPriceBlock' product=$product type='weight'}
    </div>
  {/if}
{/block}

{block name='product_description_short'}
  <div class="product-description-short" itemprop="description">{$product.description_short|strip_tags|truncate:350:'...' nofilter}</div>
{/block}</div>
  </div>
</article>
