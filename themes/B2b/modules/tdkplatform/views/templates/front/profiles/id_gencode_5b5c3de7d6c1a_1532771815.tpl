{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="header_logo text-lg-left text-xs-center"><a href="{$urls.base_url}"><img class="logo img-fluid" src="{$shop.logo}" alt="{$shop.name}"></a></div>