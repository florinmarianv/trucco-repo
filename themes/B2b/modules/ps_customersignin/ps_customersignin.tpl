{**
 * 2007-2020 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="TdkHeaderUserInfo dropdown js-dropdown TdkPopupOver">
  <a href="javascript:void(0)" data-toggle="dropdown" class="TdkPopupTitle" title="{l s='Account' d='Shop.Theme.Global'}">
    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 482.9 482.9" style="enable-background:new 0 0 482.9 482.9;" xml:space="preserve">
      <path d="M239.7,260.2c0.5,0,1,0,1.6,0c0.2,0,0.4,0,0.6,0c0.3,0,0.7,0,1,0c29.3-0.5,53-10.8,70.5-30.5
        c38.5-43.4,32.1-117.8,31.4-124.9c-2.5-53.3-27.7-78.8-48.5-90.7C280.8,5.2,262.7,0.4,242.5,0h-0.7c-0.1,0-0.3,0-0.4,0h-0.6
        c-11.1,0-32.9,1.8-53.8,13.7c-21,11.9-46.6,37.4-49.1,91.1c-0.7,7.1-7.1,81.5,31.4,124.9C186.7,249.4,210.4,259.7,239.7,260.2z
         M164.6,107.3c0-0.3,0.1-0.6,0.1-0.8c3.3-71.7,54.2-79.4,76-79.4h0.4c0.2,0,0.5,0,0.8,0c27,0.6,72.9,11.6,76,79.4
        c0,0.3,0,0.6,0.1,0.8c0.1,0.7,7.1,68.7-24.7,104.5c-12.6,14.2-29.4,21.2-51.5,21.4c-0.2,0-0.3,0-0.5,0l0,0c-0.2,0-0.3,0-0.5,0
        c-22-0.2-38.9-7.2-51.4-21.4C157.7,176.2,164.5,107.9,164.6,107.3z"/>
      <path d="M446.8,383.6c0-0.1,0-0.2,0-0.3c0-0.8-0.1-1.6-0.1-2.5c-0.6-19.8-1.9-66.1-45.3-80.9c-0.3-0.1-0.7-0.2-1-0.3
        c-45.1-11.5-82.6-37.5-83-37.8c-6.1-4.3-14.5-2.8-18.8,3.3c-4.3,6.1-2.8,14.5,3.3,18.8c1.7,1.2,41.5,28.9,91.3,41.7
        c23.3,8.3,25.9,33.2,26.6,56c0,0.9,0,1.7,0.1,2.5c0.1,9-0.5,22.9-2.1,30.9c-16.2,9.2-79.7,41-176.3,41
        c-96.2,0-160.1-31.9-176.4-41.1c-1.6-8-2.3-21.9-2.1-30.9c0-0.8,0.1-1.6,0.1-2.5c0.7-22.8,3.3-47.7,26.6-56
        c49.8-12.8,89.6-40.6,91.3-41.7c6.1-4.3,7.6-12.7,3.3-18.8c-4.3-6.1-12.7-7.6-18.8-3.3c-0.4,0.3-37.7,26.3-83,37.8
        c-0.4,0.1-0.7,0.2-1,0.3c-43.4,14.9-44.7,61.2-45.3,80.9c0,0.9,0,1.7-0.1,2.5c0,0.1,0,0.2,0,0.3c-0.1,5.2-0.2,31.9,5.1,45.3
        c1,2.6,2.8,4.8,5.2,6.3c3,2,74.9,47.8,195.2,47.8s192.2-45.9,195.2-47.8c2.3-1.5,4.2-3.7,5.2-6.3
        C447,415.5,446.9,388.8,446.8,383.6z"/>
    </svg>
  </a>
  <ul class="TdkPopupContent dropdown-menu user-info {if $logged}TdkUserInfo{/if}">
    {if $logged}

      <li style="font-weight:500;letter-spacing:4px;color:#1D1D1B;">MI CUENTA</li>
      <li>
        <a
          class="dropdown-item"
          href="{$my_account_url}"
          title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}"
          rel="nofollow"
        >
          <span>{l s='Hello' d='Shop.Theme.Global'} {$customerName}</span>
        </a>
      </li>
      <li>
        <a
          class="dropdown-item"
          href="{$my_account_url}"
          title="{l s='My Account' d='Shop.Theme.Customeraccount'}"
          rel="nofollow"
        >
          <span>{l s='My Account' d='Shop.Theme.Global'}</span>
        </a>
      </li>
      {if Configuration::get('TDKTOOLKIT_ENABLE_PRODUCTWISHLIST')}
        <li>
          <a
            class="tdk-btn-wishlist dropdown-item"
            href="{url entity='module' name='tdktoolkit' controller='mywishlist'}"
            title="{l s='Wishlist' d='Shop.Theme.Global'}"
            rel="nofollow"
          >
            <span>{l s='Wishlist' d='Shop.Theme.Global'}</span>
            <span class="tdk-total-wishlist tdk-total"></span>
          </a>
        </li>
      {/if}
    {else}
      <li class="tdk-create-account">
        <span>
          {l s='Create an account or log in to view your orders, return or adjust your personal information' d='Shop.Theme.Actions'}
        </span>
      </li>
      <li class="tdkql-register">
        <a
          class="signup"
          data-layout="register"
          href="/iniciar-sesion?create_account=1"
          title="{l s='Regsiter' d='Shop.Theme.Customeraccount'}"
          rel="nofollow"
        >
          <span>{l s='Regsiter' d='Shop.Theme.Customeraccount'}</span>
        </a>
        <!--a
          class="signup tdkql-quicklogin"
          data-enable-sociallogin="enable"
          data-type="popup"
          data-layout="register"
          href="javascript:void(0)"
          title="{l s='Regsiter' d='Shop.Theme.Customeraccount'}"
          rel="nofollow"
        >
          <span>{l s='Regsiter' d='Shop.Theme.Customeraccount'}</span>
        </a-->
        <a
          class="login"
          data-layout="login"
          href="/iniciar-sesion?back=my-account"
          title="{l s='Log in to your customer account' d='Shop.Theme.Customeraccount'}"
          rel="nofollow"
        >
          <span>{l s='Login' d='Shop.Theme.Actions'}</span>
        </a>
      </li>
    {/if}
    {if Configuration::get('TDKTOOLKIT_ENABLE_PRODUCTCOMPARE')}
      <li>
        <a
          class="tdk-btn-compare dropdown-item"
          href="{url entity='module' name='tdktoolkit' controller='productscompare'}"
          title="{l s='Compare' d='Shop.Theme.Global'}"
          rel="nofollow"
        >
          <span>{l s='Compare' d='Shop.Theme.Global'}</span>
          <span class="tdk-total-compare tdk-total"></span>
        </a>
      </li>
    {/if}
    <li class="">
      
      <a
        class="dropdown-item"
        style="display:inline-block;width:49%;"
        href="/recuperar-contrase%C3%B1a"
        title="{l s='Recuperar contraseña' d='Shop.Theme.Customeraccount'}"
        rel="nofollow"
      >
        <span>{l s='Recuperar contraseña' d='Shop.Theme.Customeraccount'}</span>
      </a>
      
      <!--a
        class="checkout dropdown-item"
        style="display:inline-block;width:49%;"
        href="{url entity='cart' params=['action' => show]}"
        title="{l s='Track my order' d='Shop.Theme.Customeraccount'}"
        rel="nofollow"
      >
        <span>{l s='Track my order' d='Shop.Theme.Customeraccount'}</span>
      </a-->
      
    </li>
    <li>

      <a
        class=""
        style="display:inline-block;width:49%;"
        href="/content/6-guia-de-compra"
        title="{l s='Need some help?' d='Shop.Theme.Customeraccount'}"
        rel="nofollow"
      >
        <span>{l s='Help' d='Shop.Theme.Customeraccount'}</span>
      </a>
    
    </li>
    {if $logged}
      <li class="tdk-logout">
        <a
          class="logout dropdown-item"
          href="{$logout_url}"
          rel="nofollow"
          title="{l s='Logout' d='Shop.Theme.Customeraccount'}"
        >
          {l s='Logout' d='Shop.Theme.Customeraccount'}
        </a>
      </li>
    {/if}
  </ul>
</div>
