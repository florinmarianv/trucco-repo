{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="wishlist">
	{if isset($wishlists) && count($wishlists) > 1}
		<div class="dropdown tdk-wishlist-button-dropdown">
		  <button class="tdk-wishlist-button dropdown-toggle show-list btn-primary TdkBtnProduct btn{if $added_wishlist} added{/if}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-id-wishlist="{$id_wishlist}" data-id-product="{$tdk_wishlist_id_product}" data-id-product-attribute="{$tdk_wishlist_id_product_attribute}">
			<span class="tdk-wishlist-bt-loading cssload-speeding-wheel"></span>
			<span class="tdk-wishlist-bt-content">
				<span class="TdkIconBtnProduct icon-wishlist">
					<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 492.719 492.719" style="enable-background:new 0 0 492.719 492.719;" xml:space="preserve">
						<g id="Icons_18_">
							<path d="M492.719,166.008c0-73.486-59.573-133.056-133.059-133.056c-47.985,0-89.891,25.484-113.302,63.569
								c-23.408-38.085-65.332-63.569-113.316-63.569C59.556,32.952,0,92.522,0,166.008c0,40.009,17.729,75.803,45.671,100.178
								l188.545,188.553c3.22,3.22,7.587,5.029,12.142,5.029c4.555,0,8.922-1.809,12.142-5.029l188.545-188.553
								C474.988,241.811,492.719,206.017,492.719,166.008z"/>
						</g>
					</svg>
				</span>
				<span class="TdkNameBtnProduct">{l s='Add to Wishlist' d='Shop.Theme.Global'}</span>
			</span>
			
		  </button>
		  <div class="dropdown-menu tdk-list-wishlist tdk-list-wishlist-{$tdk_wishlist_id_product}">
			{foreach from=$wishlists item=wishlists_item}
				<a href="javascript:void(0)" class="dropdown-item list-group-item list-group-item-action wishlist-item{if in_array($wishlists_item.id_wishlist, $wishlists_added)} added {/if}" data-id-wishlist="{$wishlists_item.id_wishlist}" data-id-product="{$tdk_wishlist_id_product}" data-id-product-attribute="{$tdk_wishlist_id_product_attribute}" title="{if in_array($wishlists_item.id_wishlist, $wishlists_added)}{l s='Remove from Wishlist' d='Shop.Theme.Global'}{else}{l s='Add to Wishlist' d='Shop.Theme.Global'}{/if}">{$wishlists_item.name}</a>			
			{/foreach}
		  </div>
		</div>
	{else}
		<a class="tdk-wishlist-button TdkBtnProduct btn-primary btn{if $added_wishlist} added{/if}" href="javascript:void(0)" data-id-wishlist="{$id_wishlist}" data-id-product="{$tdk_wishlist_id_product}" data-id-product-attribute="{$tdk_wishlist_id_product_attribute}" title="{if $added_wishlist}{l s='Remove from Wishlist' d='Shop.Theme.Global'}{else}{l s='Add to Wishlist' d='Shop.Theme.Global'}{/if}">
			<span class="tdk-wishlist-bt-loading cssload-speeding-wheel"></span>
			<span class="tdk-wishlist-bt-content">
				<span class="TdkIconBtnProduct icon-wishlist">
					<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 492.719 492.719" style="enable-background:new 0 0 492.719 492.719;" xml:space="preserve">
						<g id="Icons_18_">
							<path d="M492.719,166.008c0-73.486-59.573-133.056-133.059-133.056c-47.985,0-89.891,25.484-113.302,63.569
								c-23.408-38.085-65.332-63.569-113.316-63.569C59.556,32.952,0,92.522,0,166.008c0,40.009,17.729,75.803,45.671,100.178
								l188.545,188.553c3.22,3.22,7.587,5.029,12.142,5.029c4.555,0,8.922-1.809,12.142-5.029l188.545-188.553
								C474.988,241.811,492.719,206.017,492.719,166.008z"/>
						</g>
					</svg>
				</span>
				<span class="TdkNameBtnProduct">{l s='Add to Wishlist' d='Shop.Theme.Global'}</span>
			</span>
		</a>
	{/if}
</div>