PresTeamShop license agreement

This License Agreement represents a legal agreement between you (be it an individual or an entity) and PresTeamShop, a private company from Colombia (PresTeamShop) for the software product purchased, including the software, media and online support.
BY PURCHASING, INSTALLING, COPYING OR ANY OTHER USE OF THE SOFTWARE, YOU AGREE TO BE BOUND BY ALL THE TERMS AND CONDITIONS OF THE LICENSE AGREEMENT.

GRANT OF LICENSE

As long as you comply with all the terms and conditions of this agreement, PresTeamShop grants you a non-exclusive license that includes the following rights:

You can install and use a copy of the product only in one store or in several stores in the case of having a multi-store implementation (Multi-store), configuration supported in PrestaShop versions 1.5 or higher.
The copy of the software can only be reinstalled in the same store with its respective Multistore in PrestaShop versions 1.5 or higher.
Incorporation of the product on your website, modification of source code, and incorporation of modified source code into the product software may be done within the limits of the quantity of "Licensed Copies" purchased.
You may make backup and archive copies of the Product.
PresTeamShop reserves all rights not expressly granted in this agreement.

RESTRICTIONS

Unless the prior and written consent of PresTeamShop has been given, it is not possible: its reproduction, distribution or transfer of the product or parts of it to third parties.
Sell, rent, lease, assign or sublease the software or part of it.
Grant rights to any other person.

NOTE: PresTeamShop does not support any form of software piracy.

DISCLAIMER OF WARRANTY

The software purchased is guaranteed for a period of six (6) months from the date of purchase. In this period of time, the technical support service will be provided to make the product compatible and functional on your website.

LIMITATIONS OF THE LICENSE BASKET FOR THE PURCHASE OF THE MODULE

Each product license is issued on the basis of the store (or Multi-store in Prestashop versions 1.5 or higher) where it will be used. If you need this product in additional stores, you need to purchase separate licenses.

ADDONS SOFTWARE

Robots, plugins, and extensions are separate software products, and each requires a separate license key.

TECHNICAL SUPPORT

The technical support is valid for six (6) months from the product purchase period. This includes sending emails, chat, calls, direct configuration support in your store (if you wish) and any other means of communication available on both sides.

WARRANTY AND EXTENDED SUPPORT

You can buy an extension of the warranty and support period offered by PresTeamShop, each extension of warranty and support has a validity of six (6) months from the purchase of the purchase and will have a value of fifty percent (50%) of the license value of the product you purchased. You will also have the right to have product updates during the period of time that the warranty and support extension lasts.

RETURNS

If for any reason the software cannot function properly on your website, you can make an agreement with PresTeamShop so that your money will be returned for the same value as the purchase of the license or the purchase of the WARRANTY AND SUPPORT EXTENDED
NOTE: Any type of refund of money will cause the license to be revoked and you will not be able to make use, modification or any other use of the acquired software.

PRODUCT DELIVERY

All PresTeamShop products - documentation and related materials are delivered electronically over the Internet.

INSTALLATION AND CONFIGURATION

The installation and configuration service is available free of charge, as long as it is in the support period or the WARRANTY AND EXTENDED SUPPORT.
PresTeamShop reserves the right to charge for this service in the future.

PRODUCT UPDATES

The customer receives the free product update (Downloads) for 6 (six) months from the date of purchase as part of the product. Product updates (downloads) after that date will be available with the purchase of the WARRANTY AND EXTENDED SUPPORT.
It is not possible to retrieve updates that have already been downloaded.

In the event of any attempt to improperly obtain, distribute or market updates, PresTeamShop reserves the right to discontinue support and deactivate all licenses for products developed by PresTeamShop until the full cost of the update service is amortized. .

PresTeamShop is under no obligation to provide you with any additional details that have not been released for general distribution or as an update. Nothing in this license shall be construed as an obligation for PresTeamShop to provide updates to you under any circumstances.

PRODUCT CHANGE

The customer can exchange the product for the same product for other versions of PrestaShop at no cost in case the update and support service is active. If product update and service support has expired it cannot be changed.

LIFE CYCLE POLICY

PresTeamShop reserves the right to modify the life cycle of each of its products in general or in particular.
PresTeamShop reserves the right to terminate the development and support of its products, prior notice by its newsletter and respecting the Warranty, Support and WARRANTY AND EXTENDED SUPPORT times that are active.
PresTeamShop products would end their distribution, support and warranty in the event that PrestaShop also ends the distribution, development or version support.

EVALUATION / DEMO VERSION

PresTeamShop does not offer a trial version of any of its products and the marketing and / or distribution of product copies as a trial version by third parties is unauthorized and is considered illegal.
The demonstrations of their products can be seen online from the PresTeamShop website (http://www.presteamshop.com) and can be accessed from the description page of each product.

PRIVACY POLICY

On some sites PresTeamShop will ask you to provide us with personal information, such as your e-mail address, name, home or work address, or phone number. We may also collect demographic information, such as your zip code, age, gender, preferences, interests, and favorites. If you decide to make a purchase or sign up for a paid subscription service, we will ask you for additional information, such as your credit card number and billing address that is used to create a PresTeamShop billing account.

In addition, we collect certain standard information that your browser sends to each website you visit, such as your IP address, browser type and language, access times, and website addresses.

Use of your personal information
PresTeamShop collects and uses your personal information to operate and improve its sites and services.
We also use your personal information to communicate with you. We send out certain service communications, make calls to provide better technical support, and update or news announcements.

This does not include any personal information, such as your identity, browsing history, contacts, or information about family and friends. We use this information to improve software, technical support, and to develop a better product that meets your needs.

Except as described in this statement, we will not disclose your personal information outside of PresTeamShop without your consent.

You are free to change your personal information. Please note that your email is used as a unique identifier. You must notify PresTeamShop if you would like them to change it as it may require some additional actions on our side.

Modifications to this Privacy Statement
We will occasionally update this privacy statement to reflect changes to our services and customer feedback. When we post changes to this statement, we will revise the "last updated" date at the top of this statement. If there are material changes to this statement or to how PresTeamShop will use your personal information, we will notify you by either prominently posting a notice of such changes before implementing them or by sending you a notification directly. We recommend that you periodically review this statement to be informed of how PresTeamShop protects your information.

PresTeamShop welcomes your comments on this privacy statement. If you have any questions about this statement or believe that we have not respected it, please contact us through our web form. If you have technical or general support questions, please visit http://www.presteamshop.com/contact-form.php or email us at support@presteamshop.com to learn more about PresTeamShop support resources.

LIMITATION OF LIABILITY

In no event and under no legal theory, tort, contract or otherwise, shall PresTeamShop be liable to you or any other person for indirect, special, incidental or consequential damages of any nature including, without limitation, damages for loss of funds of trade, work stoppage, computer failure, website failure or malfunction, or any other business damage or loss.

CONVENIENCE

The person or company using this product is solely responsible for fully testing to ensure that it works to their satisfaction before purchasing a license and / or incorporating it on their website. By obtaining a license, the owner indicates that the product has met his expectations and works to his satisfaction.

TERMINATION

This agreement terminates, all rights to your license are lost, and you must stop using the product, if you do not comply with any term or condition of this License Agreement.

Translation done by Google Translate.

---------------------------------------------------------------------------------------------------------------------------------------------

Acuerdo de licencia PresTeamShop

Este Acuerdo de Licencia representa un acuerdo legal entre usted (ya sea un individuo o una entidad) y PresTeamShop, una compañía privada de Colombia (PresTeamShop) para el producto de software adquirido, incluido el software, medios de comunicación y el acompañamiento en linea.
CON LA COMPRA, INSTALACIÓN, COPIA O CUALQUIER OTRO USO DEL SOFTWARE , USTED ACEPTA SOMETERSE A TODOS LOS TÉRMINOS Y CONDICIONES DEL CONTRATO DE LICENCIA .

CONCESIÓN DE LICENCIA

Siempre y cuando cumpla con todos los términos y condiciones de este acuerdo, PresTeamShop le concede una licencia no exclusiva que incluye los siguientes derechos:

Puede instalar y utilizar una copia del producto sólo en una tienda o en varias tiendas en el caso de tener una implementación de tiendas múltiples (Multitienda), configuración soportada en versiones de PrestaShop 1.5 o superior.
La copia del software puede ser reinstalada solamente en la misma tienda con su respectiva Multitienda en versiones de PrestaShop 1.5 o superior.
La incorporación del producto en su sitio web, la modificación de código fuente y la incorporación del código fuente modificado en el software del producto se pueden realizar dentro de los límites de la cantidad de " Copias con Licencia" compradas.
Usted puede hacer copias del Producto de respaldo y archivo.
PresTeamShop se reserva todos los derechos no concedidos expresamente en este acuerdo.

RESTRICCIONES

A menos que se hay dado el consentimiento previo y por escrito de PresTeamShop, no es posible: su reproducción, distribución o transferencia del producto o partes del mismo a terceros.
Vender, alquilar, arrendar, ceder o subarrendar el software o parte del mismo.
Otorgar derechos a cualquier otra persona .

NOTA : PresTeamShop no admite ninguna forma de piratería de software.

RENUNCIA DE GARANTÍA

El software adquirido tiene garantía de un período de seis (6) meses a partir de la fecha de compra. En este lapso de tiempo se le brindará el servicio de apoyo técnico para que el producto sea compatible y funcional en su sitio web.

LIMITACIONES DE LA LICENCIA CESTA DE LA COMPRA DEL MÓDULO

Cada licencia de producto se expide sobre la base de la tienda (o Multitienda en versiones Prestashop 1.5 o superior) donde se va a utilizar. Si usted necesita este producto en tiendas adicionales, usted necesita comprar licencias separadas .

ADDONS SOFTWARE

Los robots, plugins y extensiones son productos de software independientes y cada uno de ellos requiere una clave de licencia separada.

APOYO TÉCNICO

El soporte técnico tiene una vigencia de seis (6) meses a partir del periodo de compra del producto. Este incluye envío de correos, chat, llamadas, apoyo en la configuración directa en su tienda (si usted así lo desea) y cualquier otro medio de comunicación disponibles en ambas partes.

GARANTÍA Y SOPORTE EXTENDIDO

Usted podrá comprar una extensión del periodo de garantía y soporte que ofrece PresTeamShop, cada extensión de garantía y soporte tiene una vigencia de seis (6) meses a partir de la compra de la compra y tendrá un valor del cincuenta porciento (50%) del valor de la licencia del producto que compró. Además tendrá derecho a tener actualizaciones del producto durante el periodo de tiempo que dure la extensión de garantía y soporte.

DEVOLUCIONES

Si por cualquier motivo el software no puede funcionar correctamente en su sitio web, usted puede hacer un acuerdo con PresTeamShop para que su dinero sea devuelto por el mismo valor con el que fue realizada la compra de la licencia o la compra de la GARANTÍA Y SOPORTE EXTENDIDO
NOTA: Cualquier tipo de devolución de dinero hará que la licencia quede revocada y no podrá hacer uso, modificación ni cualquier otro uso del software adquirido.

ENTREGA DEL PRODUCTO

Todos los productos PresTeamShop: documentación y materiales relacionados se entregan electrónicamente a través de Internet.

INSTALACIÓN Y CONFIGURACIÓN

El servicio de instalación y configuración está disponible de manera gratuita, siempre y cuando se encuentre en el periodo de soporte o de la GARANTÍA Y SOPORTE EXTENDIDO.
PresTeamShop se reserva derecho de cobrar por este servicio en el futuro.

ACTUALIZACIONES DE PRODUCTOS

El cliente recibe la actualización del producto gratis (Descargas) por 6 (seis) meses a partir de la fecha de compra como parte del producto. Las actualizaciones de productos (descargas) después de esa fecha estarán disponibles por la compra de la GARANTÍA Y SOPORTE EXTENDIDO.
No es posible recuperar actualizaciones que ya han sido descargadas.

En caso de cualquier intento de obtener las actualizaciones de una manera inapropiada, distribuirlas o comercializarlas, PresTeamShop se reserva el derecho de dejar de prestar apoyo y desactivar todas las licencias de los productos desarrollados por PresTeamShop hasta que el costo total del servicio de actualización se amortice.

PresTeamShop no tiene la obligación de proporcionarle cualquier detalle adicional que no haya sido liberado para su distribución general o como actualización. Ninguna disposición de la presente licencia se interpretará como una obligación de PresTeamShop para proporcionar actualizaciones a usted bajo ninguna circunstancia.

CAMBIO DE PRODUCTO

El cliente puede intercambiar el producto por el mismo producto para otras versiones de PrestaShop sin costo alguno en caso de que se tenga el servicio de actualización y soporte activo. Si ha expirado actualización y soporte de servicio de producto no puede ser cambiado.

POLÍTICA DE CICLO DE VIDA

PresTeamShop se reserva el derecho de modificar el ciclo de vida de cada uno de sus productos en general o particularmente.
PresTeamShop se reserva el derecho de finalizar el desarrollo y soporte de sus productos, previo aviso por su boletín de noticias y respetando los tiempos de Garantía, Soporte y GARANTÍA Y SOPORTE EXTENDIDO que se encuentren activos.
Los productos de PresTeamShop finalizarían su distribución, soporte y garantía en el caso que PrestaShop también finalice la distribución, desarrollo o soporte de versiones.

VERSIÓN DE EVALUACIÓN / DEMO

PresTeamShop no ofrece versión de evaluación de ninguno de sus productos y la comercialización y/o distribución de copias de productos como versión de evaluación por parte de terceros están desautorizados y son considerados ilegales.
Las demostraciones de sus productos se pueden ver de forma on-line desde la página web de PresTeamShop (http://www.presteamshop.com) y pueden ser accedidos desde la página de descripción de cada producto.

POLÍTICA DE PRIVACIDAD

En algunos sitios PresTeamShop le solicitará que nos proporcione información personal, como su dirección de e-mail, nombre, dirección de casa o del trabajo o número de teléfono. También podemos recopilar información demográfica, como su código postal, edad, sexo, preferencias, intereses y favoritos. Si decide realizar una compra o inscribirse para un servicio de suscripción de pago, le pediremos información adicional, como su número de tarjeta de crédito y dirección de facturación que se utiliza para crear una cuenta de facturación PresTeamShop.

Además , recopilamos cierta información estándar que su explorador envía a cada sitio web que visita, como su dirección IP, tipo de navegador y el idioma, los tiempos de acceso y direcciones de sitios Web.

Uso de su información personal
PresTeamShop colecciona y utiliza su información personal para operar y mejorar sus sitios y servicios .
También usamos su información personal para comunicarnos con usted. Enviamos ciertas comunicaciones de servicio, realizamos llamadas para ofrecer un mejor soporte técnico y anuncios de actualizaciones o noticias.

Esto no incluye ninguna información personal, tales como su identidad, historial de navegación, contactos o información de familiares y amigos. Utilizamos esta información para mejorar el software, soporte técnico y el desarrollo de un mejor producto que satisfaga sus necesidades .

Excepto por lo descrito en esta declaración, no revelaremos su información personal fuera de PresTeamShop sin su consentimiento.

Usted es libre de cambiar su información personal. Tenga en cuenta, que su correo electrónico se utiliza como identificador único. Debe notificar a PresTeamShop si le gustaría que lo cambien, ya que puede requerir algunas acciones adicionales de nuestro lado.

Modificaciones a esta Declaración de Privacidad
Ocasionalmente actualizaremos esta declaración de privacidad para reflejar los cambios en nuestros servicios y los comentarios de los clientes. Cuando publiquemos cambios en esta declaración, revisaremos la fecha de "última actualización " en la parte superior de esta declaración. Si hay cambios materiales en esta declaración o en cómo PresTeamShop utilizará su información personal, le notificaremos ya sea prominente publicación de un aviso de tales cambios antes de implementarlos o enviándole directamente una notificación. Le recomendamos que revise periódicamente esta declaración para estar informado de cómo PresTeamShop protege su información .

PresTeamShop da la  bienvenida a sus comentarios sobre esta declaración de privacidad. Si tiene alguna pregunta sobre esta declaración o cree que no la hemos respetado, póngase en contacto con nosotros a través de nuestro formulario web. Si usted tiene preguntas de soporte técnico o general, por favor visite http://www.presteamshop.com/contact-form.php o escribanos al correo support@presteamshop.com para aprender más sobre los recursos de apoyo PresTeamShop.

LIMITACIÓN DE LA RESPONSABILIDAD

En ningún caso y bajo ninguna teoría legal, agravio, contrato o de otro modo, deberá PresTeamShop ser responsable ante usted o cualquier otra persona por daños indirectos, especiales, incidentales o consecuentes de cualquier carácter incluyendo, sin limitación, daños por pérdida de fondo de comercio, paro laboral, falla en la computadora, falla en el sitio web o mal funcionamiento, o cualquier otro daño comercial o pérdida.

CONVENIENCIA

La persona o empresa que utiliza este producto es el único responsable de probar completamente para asegurarse de que funciona a su satisfacción antes de comprar una licencia y/o incorporarlo en su sitio web. Con la obtención de una licencia, el titular indica que el producto ha cumplido con sus expectativas y funciona a su satisfacción.

TERMINACIÓN

Este acuerdo termina, se pierden todos los derechos de su licencia, y usted debe dejar de utilizar el producto, si usted no cumple con cualquier término o condición de este Acuerdo de Licencia.