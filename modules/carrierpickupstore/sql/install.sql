CREATE TABLE IF NOT EXISTS `PREFIX_cps_carrierpickupstore` (
    `id_carrierpickupstore` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `id_cart` INT NOT NULL,
    `id_store` INT NOT NULL,
    `id_address` INT NOT NULL
) CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_cps_emails_sent` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `id_order` INT NOT NULL
) CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_cps_store` (
    `id_store` INT NOT NULL PRIMARY KEY
) CHARSET=utf8;