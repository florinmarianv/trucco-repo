/**
 * We offer the best and most useful modules PrestaShop and modifications for your online
 store.
 *
 * We are experts and professionals in PrestaShop
 *
 * @author    PresTeamShop.com <support@presteamshop.com>
 * @copyright 2011-2021 PresTeamShop
 * @license   see file: LICENSE.txt
 * @category  PrestaShop
 * @category  Module
 */

var AppCPS = {
    init: function() {
        $('#btn_update_version_module').on('click', function() {
            $.ajax({
                type: 'POST',
                url: url_call,
                data: {
                    is_ajax: true,
                    action: 'updateVersion',
                    token: token,
                    dataType: 'html'
                },
                beforeSend: function () {
                    $('#btn_update_version_module').attr('disabled', true).addClass('disabled');
                },
                success: function (data) {
                    if (data == 'OK') {
                        location.reload();
                    }
                }
            });
        });
    }
};

window.onload = function(){
    AppCPS.init();
};