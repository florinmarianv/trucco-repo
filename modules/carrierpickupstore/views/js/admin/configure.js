/**
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * We are experts and professionals in PrestaShop
 *
 * @category  PrestaShop
 * @category  Module
 * @author    PresTeamShop.com <support@presteamshop.com>
 * @copyright 2011-2021 PresTeamShop
 * @license   see file: LICENSE.txt
 */

var AppCPS = {
    init: function() {
        $.ptsInitChangeLog();
        $.ptsInitPopOver();
        $.ptsInitTabDrop();

        AppCPS.registerEvents();
    },
    registerEvents: function() {
        $('.switch').ptsToggleDepend();
        $('div[data-depend]').each(function (i, element) {
            var depend_parent = $(element).attr('data-depend');
            $('[id*="-' + depend_parent + '"]').ptsToggleDepend();
        });

        CPSSettings.init();
        CPSStore.init();
        AppCPS.initializeCreditsTraductions();

        $('button#btn-expand-all').on('click', function() {
            $('#content_translations .panel-title .accordion-toggle.collapsed').trigger('click');
        });
        $('button#btn-collapse-all').on('click', function() {
            $('#content_translations .panel-title .accordion-toggle:not(.collapsed)').trigger('click');
        });
    },
    initializeCreditsTraductions: function() {
        $('#helper-credits-developed').html(CarrierPickupStore.Msg.credits_developed);
        $('#helper-credits-spanish').html(CarrierPickupStore.Msg.credits_spanish);
        $('#helper-credits-english').html(CarrierPickupStore.Msg.credits_english);
        $('#helper-credits-development').html(CarrierPickupStore.Msg.credits_development);
        $('#helper-credits-information').html(CarrierPickupStore.Msg.credits_information);
        $('#helper-credits-support').html(CarrierPickupStore.Msg.credits_support);
        $('#helper-credits-website').html(CarrierPickupStore.Msg.credits_website);
        $('#helper-credits-suggestions').html(CarrierPickupStore.Msg.credits_suggestions);
    }
};

var CPSSettings = {
    init: function() {
        AppCPS.tab_settings = $('#tab-settings');
        CPSSettings.createButtonAddIP();
        CPSSettings.buttonModifyAPIKey();

        AppCPS.tab_settings.find('#txt-zoom')
            .onlyNumber()
            .on('input', function() {
                CPSSettings.numberRange(18, 2);
            });

        $(document).on('click', '#enable_input_api_key', CPSSettings.modifyAPIKey);
        $(document).on('click', '.btn-delete-icon', CPSSettings.deleteIcon);

        CPSSettings.addInputFile('shop_selected_icon', CarrierPickupStore.marker_icons.shop_selected_icon);
        CPSSettings.addInputFile('shop_unselected_icon', CarrierPickupStore.marker_icons.shop_unselected_icon);

        AppCPS.tab_settings.find('input[type=file]').filestyle({buttonText: CarrierPickupStore.Msg.input_file_text});
        AppCPS.tab_settings.find('form').attr('enctype', 'multipart/form-data');
    },
    modifyAPIKey: function() {
        var choice = confirm(CarrierPickupStore.Msg.api_modifying+'\n'+CarrierPickupStore.Msg.confirm);
        if(choice) {
            $('#txt-api_key').prop('readonly', false);
            $(this).remove();
        }
    },
    buttonModifyAPIKey: function(e) {
        var data = {
            action: 'modifyAPIKey'
        };
        var _json = {
            data: data,
            success: function(json) {
                if (json.result) {
                    $('#txt-api_key').prop('readonly', true);

                    var button = '<div class="col-xs-12 col-sm-2">';
                    button += '<a id="enable_input_api_key" class="btn btn-primary btn-settings">';
                    button += CarrierPickupStore.Msg.button_api_key;
                    button += '</a></div>';
                    $('#txt-api_key').parent().after(button);
                }
            }
        };
        $.makeRequest(_json);
    },
    createButtonAddIP: function() {
        var $div_container_ip_debug = AppCPS.tab_settings.find('div#container-ip_debug');

        var $sub_div_add_ip = $('<div>')
                .addClass('col-xs-12 col-sm-3 input-group-md pull-right')
                .appendTo($div_container_ip_debug);

        $('<button>')
            .addClass('btn btn-primary btn-sm')
            .html('&nbsp;'+CarrierPickupStore.Msg.add_IP)
            .prepend($('<i>').addClass('fa-pts fa-pts-plus'))
            .attr('type', 'button')
            .on('click', CPSSettings.addNewIP)
            .appendTo($sub_div_add_ip);
    },
    deleteIcon: function(e) {
        if (!confirm(CarrierPickupStore.Msg.confirm_delete_icon)) {
            return false;
        }

        var $button = $(e.currentTarget);
        var icon = $button.data('icon');

        var data = {
            action: 'deleteIcon',
            icon: icon
        };
        var _json = {
            data: data,
            beforeSend: function() {
                $button.prop('disabled', true);
            },
            success: function(json) {
                if (parseInt(json.message_code) === 0) {
                    $button.parents('.container-marker-icon').remove();
                }
            },
            complete: function() {
                $button.prop('disabled', false);
            }
        };
        $.makeRequest(_json);
    },
    addInputFile: function(selector, icon) {
        var $container = $('#container-'+selector);

        if (!$.isEmpty(icon)) {
            $('<div>')
                .addClass('col-xs-12 col-sm-6 col-sm-push-5 input-group-md container-marker-icon')
                .append(
                    $('<div>')
                    .addClass('col-md-2 nopadding')
                    .append(
                        $('<img>').attr('src', icon).css('max-width', '100%'),
                        $('<button>').attr('type', 'button').addClass('btn btn-danger btn-xs btn-delete-icon').data('icon', selector).append($('<i>').addClass('fa-pts fa-pts-trash'))
                    )
                )
                .insertAfter($container.children('.container-depends'));
        }

        $('<div>')
                .addClass('col-xs-12 col-sm-4 nopadding input-group-md')
                .append(
                    $('<input>').attr('type', 'file').addClass('form-control').attr('id', 'fl-'+selector).attr('name', selector).attr('accept', 'image/png')
                )
                .insertAfter($container.children('.container-depends'));

    },
    addNewIP: function() {
        var $txt_ip_debug = AppCPS.tab_settings.find('#txt-ip_debug');
        var ip_debug = $txt_ip_debug.val();

        var default_ip = (ip_debug.length > 0) ? ','+CarrierPickupStore.remote_addr : CarrierPickupStore.remote_addr;
        $txt_ip_debug.val(ip_debug+default_ip);
    },
    numberRange: function(max, min) {
        var input_value = $('#txt-zoom').val();
        input_value = parseInt(input_value);

        if(input_value > max || input_value < min){
            AppCPS.tab_settings.find('.btn-settings').prop('disabled', true);
        } else {
            AppCPS.tab_settings.find('.btn-settings').prop('disabled', false);
        }
    }
};

var CPSStore = {
    init: function() {
        CPSStore.getListTabsContentFn();
    },
    getListTabsContentFn: function() {
        $.getList('table-stores', 'getStoresList', {}, function(data) {
            if (data.content.length === 0) {
                $('#table-stores').find('tbody')
                .append(
                    $('<tr/>')
                    .append(
                        $('<td/>')
                        .attr('colspan', '6')
                        .append(
                            $('<div>')
                            .addClass('alert alert-info text-center')
                            .append(CarrierPickupStore.Msg.no_stores_created)
                            .append(
                                $('<a/>')
                                .attr({href: CarrierPickupStore.url_manage_stores, target: '_blank', style: 'text-decoration: underline; margin-left: 5px;'})
                                .append(CarrierPickupStore.Msg.manage_stores+'&nbsp;')
                                .append(
                                    $('<i>').addClass('fa-pts fa-pts-external-link')
                                )
                            )
                        )
                    )
                );
            }
        });
    },
    editStore: function(e, data) {
        CPSStore.getStoreUrl(e, data['id_store']);
    },
    getStoreUrl: function(e, id_store) {
        var data = {
            action: 'getStoreURL',
            id_store: id_store
        };
        var _json = {
            data: data,
            success: function(json) {
                if (json.message_code) {
                    window.open(json.url, '_blank');
                }
            }
        };
        $.makeRequest(_json);
    },
    toggleVisibility: function(event, params) {
        var _json = {
            data: {
                action: 'updateStoreVisibility',
                id_store: params.id_store,
                active: params.active
            },
            beforeSend: function() {
                _json.e = event;
            },
            success: function(json) {
                if (json.message_code === CarrierPickupStore.SUCCESS_CODE ) {
                    CPSStore.getListTabsContentFn();
                }
            }
        };
        $.makeRequest(_json);
    },
    toggleActive: function(event, params) {
        var _json = {
            data: {
                action: 'updateStoreStatus',
                id_store: params.id_store,
                visible: params.visible
            },
            beforeSend: function() {
                _json.e = event;
            },
            success: function(json) {
                if (json.message_code === CarrierPickupStore.SUCCESS_CODE ) {
                    CPSStore.getListTabsContentFn();
                }
            }
        };
        $.makeRequest(_json);
    }
};

window.onload = function(){
    AppCPS.init();
    globalTabs.ini();
};