/**
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * We are experts and professionals in PrestaShop
 *
 * @author    PresTeamShop.com <support@presteamshop.com>
 * @copyright 2011-2021 PresTeamShop
 * @license   see file: LICENSE.txt
 * @category  PrestaShop
 * @category  Module
 */

var AppCPS = {
    init: function(){
        AppCPS.registerEvents();
    },
    registerEvents: function() {
        $(document).on('change', 'div#stores-container select[name="id_store"]', AppCPS.changeStore);
        $('form#js-delivery').find('button[name="confirmDeliveryOption"]').on('click', AppCPS.validateDeliverySection);

        if (typeof OnePageCheckoutPS !== typeof undefined) {
            AppCPS.setSessionStorage();
            AppCPS.changeCarrierOPC();
            $(document).on('opc-update-carrier:completed', AppCPS.changeCarrierOPC);
        } else {
            $(document).on('change', 'input[name*="delivery_option"]', AppCPS.changeCarrier);
            AppCPS.changeCarrier();

            var id_store = $.totalStorageCPS('id_store' + CarrierPickupStore.id_cart);
            if (id_store > 0) {
                $('#stores-container').find('select[name="id_store"]').val(id_store);
            }
        }
    },
    setSessionStorage: function() {
        if (!OnePageCheckoutPS.IS_LOGGED) {
            var $container = $('#onepagecheckoutps');
            var $form = $container.find('#form_address_delivery');
            var session_data = {};

            $.each($form.find('#delivery_id_country, #delivery_id_state, #delivery_city'), function(i, elem) {
                var field_name = $(elem).data('field-name');
                var required = ($(elem).data('required') || $(elem).data('validation') === 'required') ? 1 : 0;

                if (required === 0 && field_name === 'id_state' && typeof countriesJS !== typeof undefined) {
                    var id_country = $('#onepagecheckoutps_step_one').find('#delivery_id_country').val();
                    if (typeof countriesJS[id_country] !== typeof undefined && countriesJS[id_country].length > 0) {
                        required = true;
                    }
                }

                session_data[field_name] = required;
            });

            sessionStorage.setItem('cps_data', JSON.stringify(session_data));
            AppCPS.toggleRequiredField();
        }
    },
    getIdCarrierSelected: function() {
        var id_carrier = $('input[name*="delivery_option"]:checked').val();
        return parseInt(id_carrier);
    },
    validateDeliverySection: function(event) {
        if (AppCPS.getIdCarrierSelected() === parseInt(CarrierPickupStore.id_carrierpickupstore)) {
            var id_store = $('#stores-container').find('select[name="id_store"]').val();
            if (parseInt(id_store) === 0) {
                alert(CarrierPickupStore.MsgCPS.need_select_pickup_point);
                event.preventDefault();
                event.stopPropagation();
            }
        }
    },
    changeStore: function(e) {
        var id_store = $(e.currentTarget).val();
        if (parseInt(id_store) <= 0) {
            id_store = null;
        }

        var is_set_invoice  = false;
        if (typeof OnePageCheckoutPS !== typeof undefined && AppOPC.$opc_step_one.find('#checkbox_create_invoice_address').length > 0) {
            is_set_invoice = (AppOPC.$opc_step_one.find('#checkbox_create_invoice_address').is(':checked')) ? 1 : 0;
        } else if (!$('div.addresses input#addressesAreEquals').is(':checked')) {
            is_set_invoice = true;
        }

        $.totalStorageCPS('id_store' + CarrierPickupStore.id_cart, id_store);

        var data = {
            url_call: CarrierPickupStore.actions_carrierpickupstore,
            action: 'changeStore',
            token: CarrierPickupStore.pts_static_token,
            dataType: 'json',
            id_store: id_store,
            is_set_invoice: is_set_invoice
        };
        var _json = {
            data: data,
            success: function () {
                if (typeof OnePageCheckoutPS !== typeof undefined) {
                    PaymentOPC.getByCountry();
                }
            }
        };
        $.makeRequest(_json);
    },
    changeCarrier: function() {
        if (AppCPS.getIdCarrierSelected() !== parseInt(CarrierPickupStore.id_carrierpickupstore)) {
            $('#delivery-addresses').children('.address-item').removeClass('cps_disabled');
        } else {
            $('#delivery-addresses').children('.address-item').addClass('cps_disabled');
        }
    },
    changeCarrierOPC: function(e) {
        if (!OnePageCheckoutPS.IS_LOGGED) {
            var $container = $('#form_address_delivery').children('.fields_container');
            var $row = $container.children('.row');

            if (AppCPS.getIdCarrierSelected() !== parseInt(CarrierPickupStore.id_carrierpickupstore)) {
                $row.removeClass('cps_hide');
                $row.children('div').removeClass('cps_hide');
            } else {
                $row.addClass('cps_hide');
                $row.children('div').addClass('cps_hide');

                var $visible_containers = $row.find('#field_delivery_id_country, #field_delivery_id_state, #field_delivery_city');
                $visible_containers.removeClass('cps_hide');
                $visible_containers.parent().removeClass('cps_hide');
            }

            AppCPS.toggleRequiredField();
        } else {
            if (AppCPS.getIdCarrierSelected() !== parseInt(CarrierPickupStore.id_carrierpickupstore)) {
                $('#cps_address_message').addClass('cps_hide');
            } else {
                $('#cps_address_message').removeClass('cps_hide');
            }
        }
    },
    toggleRequiredField: function() {
        var $container = $('#onepagecheckoutps');
        var $form = $container.find('#form_address_delivery');

        var session_data = JSON.parse(sessionStorage.getItem('cps_data'));

        $.each($form.find('#field_delivery_id_country, #field_delivery_id_state, #field_delivery_city'), function(i, elem) {
            var $parent = $(elem);
            var id_parent = $parent.attr('id');
            var selector = id_parent.replace('field_', '');
            var $element = $parent.find('#'+selector);
            if (typeof session_data[$element.data('field-name')] !== typeof undefined) {
                var required = parseInt(session_data[$element.data('field-name')]);
                if (AppCPS.getIdCarrierSelected() === parseInt(CarrierPickupStore.id_carrierpickupstore)) {
                    $parent.children('label').find('sup').html('');
                    $element
                        .removeClass('required')
                        .attr('data-required', 0)
                        .attr('data-validation-has-keyup-event', false)
                        .attr('data-validation-optional', true);
                } else if (required === 1) {
                    $parent.children('label').find('sup').html('*');
                    $element
                        .addClass('required')
                        .attr('data-required', 1)
                        .attr('data-validation-has-keyup-event', true)
                        .attr('data-validation-optional', false);
                }
            }
        });
    }
};

$(function(){
    AppCPS.init();
});