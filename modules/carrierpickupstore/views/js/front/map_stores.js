/**
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * We are experts and professionals in PrestaShop
 *
 * @author    PresTeamShop.com <support@presteamshop.com>
 * @copyright 2011-2021 PresTeamShop
 * @license   see file: LICENSE.txt
 * @category  PrestaShop
 * @category  Module
 */

var AppCPSMap = {
    init: function(){
        AppCPSMap.map = '';
        AppCPSMap.marker = '';
        AppCPSMap.markers = [];
        AppCPSMap.registerEvents();
    },
    registerEvents: function(){
        $(document).on('click', '#stores-container button#see_map', AppCPSMap.showModalMap);

        $(document).delegate('input[name="id_store_map"]', 'change', function(){
            var id = $(this).val();

            $('#stores_list .stores-content.card').removeClass('selected');
            $(this).parents('.card').addClass('selected');
            $(this).parents('.card').find('.header-content-link').trigger('click');

            for (var i = 0, length = AppCPSMap.markers.length; i < length; i++) {
                if(AppCPSMap.markers[i].id_store == id){
                    AppCPSMap.map.panTo(AppCPSMap.markers[i].getPosition());
                    new google.maps.event.trigger( AppCPSMap.markers[i], 'click' );
                }
            }
        });
    },
    manageErrors: function(error){
        switch(error.code) {
            case error.PERMISSION_DENIED:
               alert(CarrierPickupStore.MsgCPS.permission_denied);
                break;
            case error.POSITION_UNAVAILABLE:
               alert(CarrierPickupStore.MsgCPS.position_unavailable);
                break;
            case error.TIMEOUT:
               alert(CarrierPickupStore.MsgCPS.timeout);
                break;
            case error.UNKNOWN_ERROR:
               alert(CarrierPickupStore.MsgCPS.unknown_error);
                break;
        }
    },
    showMap: function(){
        var latitude = ' 40.4165000';
        var longitude = '-3.7025600';
        var zoom = (!$.isEmpty(CarrierPickupStore.configs.CPS_ZOOM)) ? CarrierPickupStore.configs.CPS_ZOOM : 5;

        if (!$.isEmpty(CarrierPickupStore.configs.CPS_COORDINATES)) {
            var coordinates = CarrierPickupStore.configs.CPS_COORDINATES.split(',');
            latitude    = (typeof coordinates[0] !== typeof undefined) ? coordinates[0]: latitude;
            longitude   = (typeof coordinates[1] !== typeof undefined) ? coordinates[1]: longitude;
        }

        AppCPSMap.map = new google.maps.Map(document.getElementById('map'), {
            zoom: parseInt(zoom),
            center: new google.maps.LatLng(latitude, longitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var id_store = $('#stores-container select[name="id_store"]').val();
        var infowindow = new google.maps.InfoWindow();

        $.each( CarrierPickupStore.stores, function( key, value ) {
            var content = AppCPSMap.getContentStores(value);
            AppCPSMap.marker = new google.maps.Marker({
                position: new google.maps.LatLng(value.latitude, value.longitude),
                map: AppCPSMap.map,
                icon: CarrierPickupStore.src_img + 'icon/'+CarrierPickupStore.shop_unselected_icon,
                id_store: value.id
            });

            google.maps.event.addListener(AppCPSMap.marker, 'click', (function(marker, i) {
                return function() {
                    var id_store = marker.get('id_store');
                    $('input#id_store_map_'+id_store).trigger('click');
                    infowindow.setContent(content);
                    infowindow.open(AppCPSMap.map, marker);

                    if (!$.isEmpty(CarrierPickupStore.shop_selected_icon)) {
                        for (var j = 0; j < AppCPSMap.markers.length; j++) {
                            AppCPSMap.markers[j].setIcon(CarrierPickupStore.src_img + 'icon/'+CarrierPickupStore.shop_unselected_icon);
                        }
                        marker.set('icon', CarrierPickupStore.src_img + 'icon/'+CarrierPickupStore.shop_selected_icon);
                    }
                }
            })(AppCPSMap.marker, key));

            if (parseInt(value.id) === parseInt(id_store)) {
                new google.maps.event.trigger(AppCPSMap.marker, 'click');
            }

            AppCPSMap.markers.push(AppCPSMap.marker);
        });

        var marker_user = new google.maps.Marker({
            position: new google.maps.LatLng(latitude, longitude),
            map: AppCPSMap.map
        });

        navigator.geolocation.getCurrentPosition(function(position){
            var latitude_user = position.coords.latitude;
            var longitude_user = position.coords.longitude;

            var latlng = new google.maps.LatLng(latitude_user, longitude_user);
            marker_user.setPosition(latlng);
            AppCPSMap.map.panTo(marker_user.getPosition());
        }, AppCPSMap.manageErrors);

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        AppCPSMap.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        AppCPSMap.map.addListener('bounds_changed', function() {
            searchBox.setBounds(AppCPSMap.map.getBounds());
        });

        var markers = [];
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: AppCPSMap.map,
//                    icon: src_img+'icon/shop.png',
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            AppCPSMap.map.fitBounds(bounds);
        });
    },
    getContentStores: function(value){
        var content =
            "<table class='information_stores'>"+
                "<tr>"+
                    "<th colspan='2'><h4>"+value.name+"</h4></th>"+
                "</tr><tr>";
        content += "</table>";

        return content;
    },
    showModalMap: function(){
        var data = {
            url_call: CarrierPickupStore.actions_carrierpickupstore,
            action: 'showModalMap',
            token: CarrierPickupStore.pts_static_token,
            dataType: 'html'
        };
        var _json = {
            data: data,
            success: function (html)
            {
                $('body').append($(html)).show();

                var callback = function () {
                    var store_selected_carrier_zone = $('#stores-container select[name="id_store"]').val();
                    $('#stores_list').find('#id_store_map_'+store_selected_carrier_zone).trigger('click');

                    AppCPSMap.showMap();
                }

                var callback_close = function(){
                    var store_selected_modal = $('#stores_list .card.selected input[name="id_store_map"]').val();
                    if (typeof store_selected_modal == typeof undefined) {
                        store_selected_modal = 0;
                    }

                    $('#stores-container select[name="id_store"]').val(store_selected_modal).trigger('change');
                    $('body #stores-map-container').remove();
                    $('body .pac-container').remove();
                };

                var container = '#checkout-delivery-step';
                if (CarrierPickupStore.installed_opc) {
                    container = '#onepagecheckoutps';
                }

                $('#checkout-delivery-step').append($('<div>').addClass('pts bootstrap').attr('id', 'pts_modal_cps'));

                AppCPSMap.showModal({
                    name: 'pts_modal_stores',
                    type: 'normal',
                    title: CarrierPickupStore.MsgCPS.pick_up_store,
                    content: $('#stores-map-container'),
                    callback : callback,
                    callback_close : callback_close,
                    button_close: true,
                    container: container
                });

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("TECHNICAL ERROR: unable view comment \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
            }
        };
        $.makeRequest(_json);
    },
    showModal: function(params){
        var param = $.extend({}, {
            name: 'pts_modal_stores',
            type: 'normal',
            title: '',
            title_icon: '',
            message: '',
            content: '',
            close: true,
            button_close: false,
            size: '',
            callback: '',
            callback_close: '',
            container: 'div#onepagecheckoutps'
        }, params);

        $('#'+param.name).remove();

        var parent_content = '';
        if (typeof param.content === 'object'){
            parent_content = param.content.parent();
        }

        var $modal = $('<div/>').attr({id:param.name, 'class':'modal fade', role:'dialog'});
        var $modal_dialog = $('<div/>').attr({'class':'modal-dialog ' + param.size});
        var $modal_header = $('<div/>').attr({'class':'modal-header'});
        var $modal_content = $('<div/>').attr({'class':'modal-content'});
        var $modal_body = $('<div/>').attr({'class':'modal-body'});
        var $modal_footer = $('<div/>').attr({'class':'modal-footer'});
        var $modal_button_close = $('<button/>')
                .attr({type:'button', 'class':'close'})
                .click(function(){
                    $('#'+param.name).modal('hide');

                    if (!$.isEmpty(parent_content))
                        param.content.appendTo(parent_content).addClass('hidden');

                    $('body').removeClass('modal-open');

                    if (typeof param.callback_close !== typeof undefined && typeof param.callback_close === 'function')
                        param.callback_close();
                })
                .append('<i class="fa-pts fa-pts-close"></i>');
        var $modal_button_close_footer = $('<button/>')
            .attr({type:'button', 'class':'btn btn-default'})
            .click(function(){
                $('#'+param.name).modal('hide');

                if (!$.isEmpty(parent_content))
                    param.content.appendTo(parent_content).addClass('hidden');

                $('body').removeClass('modal-open');

                if (typeof param.callback_close !== typeof undefined && typeof param.callback_close === 'function')
                    param.callback_close();
            })
            .append(CarrierPickupStore.MsgCPS.close_modal_btn);
        var $modal_title = '';

        if (typeof param.message === 'array'){
            var message_html = '';
            $.each(param.message, function(i, message){
                message_html += '- ' + message + '<br/>';
            });
            param.message =  message_html;
        }

        if (param.type == 'error'){
            $modal_title = $('<span/>')
                .attr({'class':'panel-title'})
                .append(param.close ? $modal_button_close : '')
                .append('<i class="fa-pts fa-pts-times-circle fa-pts-2x" style="color:red"></i>')
                .append(param.message);
        }else if (param.type == 'warning'){
            $modal_title = $('<span/>')
                .attr({'class':'panel-title'})
                .append(param.close ? $modal_button_close : '')
                .append('<i class="fa-pts fa-pts-warning fa-pts-2x" style="color:orange"></i>')
                .append(param.message);
        }
        else{
            $modal_title = $('<span/>')
                .attr({'class':'panel-title'})
                .append(param.close ? $modal_button_close : '')
                .append('<i class="fa-pts '+param.title_icon+' fa-pts-1x"></i>')
                .append(param.title);
        }

        $modal_header.append($modal_title);
        $modal_content.append($modal_header);

        if (param.type == 'normal'){
            if (typeof param.content === 'object'){
                param.content.removeClass('hidden').appendTo($modal_body);
            }else{
                $modal_body.append(param.content);
            }

            $modal_content.append($modal_body);

            if (param.button_close){
                $modal_footer.append($modal_button_close_footer);
                $modal_content.append($modal_footer);
            }
        }

        $modal_dialog.append($modal_content);
        $modal.append($modal_dialog);

        $modal.on('hide.bs.modal', function(){
            if (!param.close){
                return false;
            } else {
                if (!$.isEmpty(parent_content))
                    param.content.appendTo(parent_content).addClass('hidden');

                if (typeof param.callback_close !== typeof undefined && typeof param.callback_close === 'function')
                    param.callback_close();
            }
        });

        $(param.container).prepend($modal);
        $('#'+param.name)
            .modal('show')


        if (!$('#'+param.name).hasClass('in'))
            $('#'+param.name).addClass('in').css({display : 'block'});

        $modal_dialog.css({
            top: (($(window).height() - $('#'+param.name).find('.modal-dialog').height()) / 2),
        });

        if (param.container === 'div#onepagecheckoutps') {
            $(param.container).find('.loading_big').hide();
        }

        if (typeof param.callback !== typeof undefined && typeof param.callback === 'function')
            param.callback();

        window.scrollTo(0, $(param.container).offset().top);
    }
};
$(function(){
    AppCPSMap.init();
});