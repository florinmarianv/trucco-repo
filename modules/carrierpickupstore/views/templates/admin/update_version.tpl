{*
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* We are experts and professionals in PrestaShop
*
* @author    PresTeamShop.com <support@presteamshop.com>
* @copyright 2011-2021 PresTeamShop
* @license   see file: LICENSE.txt
* @category  PrestaShop
* @category  Module
*}

<div class="bootstrap panel">
    <div class="alert alert-warning">
        {l s='We have detected you uploaded the new version' mod='carrierpickupstore'} <b>{$module_version|escape:'htmlall':'UTF-8'}</b> {l s='of our module' mod='carrierpickupstore'} <b>{$module_name|escape:'htmlall':'UTF-8'}</b>.
        <br/><br/>
        {l s='To proceed with the update, you need to click here' mod='carrierpickupstore'}: <input id="btn_update_version_module" type="button" class="btn btn-primary btn-xs" value="{l s='Update now' mod='carrierpickupstore'}" />
    </div>
</div>