{*
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* We are experts and professionals in PrestaShop
*
* @author    PresTeamShop.com <support@presteamshop.com>
* @copyright 2011-2021 PresTeamShop
* @license   see file: LICENSE.txt
* @category  PrestaShop
* @category  Module
*}

{if empty($paramsBack.id_shop)}
    
    <div class="row mtop10">
        <div role="alert" class="alert alert-info clearfix">
            <div class="col-xs-9 pts-nowrap col-sm-6 col-md-4 nopadding-xs">
                <label class="pts-label-tooltip col-xs-12 nopadding control-label">
                    {l s='Choose a store' mod='carrierpickupstore'}
                </label>
            </div>
            <div class="col-xs-12">
                <p>
                    {l s='To configure the module, a store must be selected.' mod='carrierpickupstore'}
                </p>
            </div>
        </div>
    </div>
    
{else}
    <div id="pts_content" class="configure_carrierpickupstore pts bootstrap row nopadding">
        <div class="col-xs-12 nopadding">
            
                {if $paramsBack.ERRORS}
                    <br class="clearfix">
                    <div class="col-xs-12">
                        {foreach from=$paramsBack.ERRORS item='error'}
                            <div class="alert alert-danger">
                                {$error|escape:'htmlall':'UTF-8'}
                            </div>
                        {/foreach}
                    </div>
                {/if}

                {if $paramsBack.WARNINGS}
                    <br class="clearfix">
                    <div class="col-xs-12">
                        {foreach from=$paramsBack.WARNINGS item='warning'}
                            <div class="alert alert-warning">
                                {$warning|escape:'htmlall':'UTF-8'}
                            </div>
                        {/foreach}
                    </div>
                {/if}

                {if isset($show_saved_message) and $show_saved_message}
                    <br class="clearfix">
                    <div class="col-xs-12">
                        <div class="alert alert-success">
                            <b>{l s='Configuration was saved successful' mod='carrierpickupstore'}</b>
                        </div>
                    </div>
                {/if}
                <div class="col-xs-12">
                    <div class="clear row-fluid clearfix panel {*col-xs-12*} nopadding">
                        <div class="pts-menu-xs visible-xs visible-sm pts-menu">
                            <span class="belt text-center">
                                <i class="fa-pts fa-pts-align-justify fa-pts-3x nohover"></i>
                            </span>
                            <div class="pts-menu-xs-container hidden"></div>
                        </div>
                        <div class="hidden-xs hidden-sm col-sm-3 col-lg-2 pts-menu">
                            <ul class="nav">
                                <li class="pts-menu-title hidden-xs hidden-sm">
                                    <a>
                                        {l s='Menu' mod='carrierpickupstore'}
                                    </a>
                                </li>
                                {foreach from=$paramsBack.HELPER_FORM.tabs item='tab' name='tabs'}
                                    <li class="{if (isset($CURRENT_FORM) && $CURRENT_FORM eq $tab.href) || (not isset($CURRENT_FORM) && $smarty.foreach.tabs.first)}active{/if}">
                                        <a href="#tab-{$tab.href|escape:'htmlall':'UTF-8'}" data-toggle="tab" class="{if isset($tab.sub_tab)}has-sub{/if}">
                                            <i class='fa-pts fa-pts-{if isset($tab.icon)}{$tab.icon|escape:'htmlall':'UTF-8'}{else}cogs{/if} fa-pts-1x'></i>&nbsp;{$tab.label|escape:'htmlall':'UTF-8'}
                                        </a>
                                        {if isset($tab.sub_tab)}
                                            <div class="sub-tabs" data-tab-parent="{$tab.href|escape:'htmlall':'UTF-8'}" style="display: none;overflow: hidden;">
                                                <ul class="nav sub-tab-list">
                                                    {foreach from=$tab.sub_tab item='sub_tab'}
                                                        <li class="{if (isset($CURRENT_FORM) && $CURRENT_FORM eq $sub_tab.href)}active{/if}">
                                                            <a href="#tab-{$sub_tab.href|escape:'htmlall':'UTF-8'}" data-toggle="tab">
                                                                <i class='fa-pts {if isset($sub_tab.icon)}{$sub_tab.icon|escape:'htmlall':'UTF-8'}{else}{$tab.icon|escape:'htmlall':'UTF-8'}{/if} fa-pts-1x'></i>&nbsp;{$sub_tab.label|escape:'htmlall':'UTF-8'}
                                                            </a>
                                                        </li>
                                                    {/foreach}
                                                </ul>
                                            </div>
                                        {/if}
                                    </li>
                                {/foreach}
                            </ul>
                        </div>
                        <div class="col-xs-12 col-md-10 pts-content">
                            <div class="panel pts-panel nopadding">
                                <div class="panel-heading main-head">
                                    <span class="pull-right bold">{l s='Version' mod='carrierpickupstore'}&nbsp;{$paramsBack.VERSION|escape:'htmlall':'UTF-8'}</span>
                                    <span class="pts-content-current-tab">&nbsp;</span>
                                </div>
                                <div class="panel-body">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        {if isset($ANOTHER_MODULES) and file_exists($paramsBack.MODULE_TPL|cat:'views/templates/admin/helper/another_modules.tpl')}
                                            <div class="tab-pane{if (isset($CURRENT_FORM) && $CURRENT_FORM eq 'another_modules')} active{/if}" id="tab-another_modules">
                                                {include file=$paramsBack.MODULE_TPL|cat:'views/templates/admin/helper/another_modules.tpl' modules=$ANOTHER_MODULES}
                                            </div>
                                        {/if}
                                        {if isset($ADDONS) and file_exists($paramsBack.MODULE_TPL|cat:'views/templates/admin/helper/another_modules.tpl')}
                                            <div class="tab-pane{if (isset($CURRENT_FORM) && $CURRENT_FORM eq 'addons')} active{/if}" id="tab-addons">
                                                {include file=$paramsBack.MODULE_TPL|cat:'views/templates/admin/helper/another_modules.tpl' modules=$ADDONS}
                                            </div>
                                        {/if}
                                        {if isset($paramsBack.HELPER_FORM)}
                                            {if isset($paramsBack.HELPER_FORM.forms) and is_array($paramsBack.HELPER_FORM.forms) and count($paramsBack.HELPER_FORM.forms)}
                                                {foreach from=$paramsBack.HELPER_FORM.forms key='key' item='form' name='forms'}
                                                    {if isset($form.modal) and $form.modal}{assign var='modal' value=1}{else}{assign var='modal' value=0}{/if}
                                                    <div class="tab-pane {if (isset($CURRENT_FORM) && $CURRENT_FORM eq $form.tab) || (not isset($CURRENT_FORM) && $smarty.foreach.forms.first)}active{/if}" id="tab-{$form.tab|escape:'htmlall':'UTF-8'}">

                                                        {if $form.tab eq 'stores'}
                                                            <div class="alert alert-info">
                                                                <p>
                                                                    {l s='The rows of the following color indicate that the country of the store is not active and therefore the store will not be displayed in the drop-down list of available pickup stores in FrontOffice:' mod='carrierpickupstore'} <i class="icon-inactive-country fa-pts fa-pts-circle fa-pts-1x"></i>
                                                                </p>
                                                                <p>
                                                                    {l s='You can activate the country(s) from' mod='carrierpickupstore'} <a class="country-admin-url" href="{$paramsBack.url_manage_countries|escape:'htmlall':'UTF-8'}" target="_blank">{l s='here' mod='carrierpickupstore'} <i class="fa-pts fa-pts-external-link"></i></a>
                                                                </p>
                                                            </div>
                                                        {/if}

                                                        <form action="{$paramsBack.ACTION_URL|escape:'htmlall':'UTF-8'}" {if isset($form.method) and $form.method neq 'ajax'}method="{$form.method|escape:'htmlall':'UTF-8'}"{/if}
                                                              class="form form-horizontal clearfix {if isset($form.class)}{$form.class|escape:'htmlall':'UTF-8'}{/if}"
                                                              {if isset($form.id)}id="{$form.id|escape:'htmlall':'UTF-8'}"{/if}
                                                              autocomplete="off">
                                                            {if isset($form.options) and is_array($form.options) and count($form.options)}
                                                                <div class="col-xs-12 {if not $modal}col-md-8{/if} content-form pts-content nopadding-xs">
                                                                    {foreach from=$form.options item='option'}
                                                                        <div class="form-group clearfix clear {if isset($option.hide_on) and $option.hide_on}hidden{/if}"
                                                                             {if isset($option.data_hide)}data-hide="{$option.data_hide|escape:'htmlall':'UTF-8'}"{/if}
                                                                             id="container-{$option.name|escape:'htmlall':'UTF-8'}">
                                                                            {if isset($option.label)}
                                                                                <div class="col-xs-{if $modal}3{else}{if $option.type eq $paramsBack.GLOBALS->type_control->checkbox}9 pts-nowrap{else}12{/if} col-sm-6 col-md-5 nopadding-xs{/if}"
                                                                                     title="{$option.label|escape:'quotes':'UTF-8'}">
                                                                                    <label class="pts-label-tooltip col-xs-12 nopadding control-label">
                                                                                        {$option.label|escape:'quotes':'UTF-8'}
                                                                                        {if isset($option.tooltip)}
                                                                                            {include file='./helper/tooltip.tpl' option=$option}
                                                                                        {/if}
                                                                                    </label>
                                                                                </div>
                                                                            {/if}
                                                                            {include file=$paramsBack.MODULE_TPL|cat:'views/templates/admin/helper/form.tpl' option=$option global=$paramsBack.GLOBALS modal=$modal}
                                                                        </div>
                                                                    {/foreach}
                                                                </div>
                                                            {/if}
                                                            {if isset($form.actions)}
                                                                <div class="col-xs-12 nopadding clear clearfix">
                                                                    <hr />
                                                                    {include file=$paramsBack.MODULE_TPL|cat:'views/templates/admin/helper/action.tpl' form=$form key=$key modal=$modal}
                                                                </div>
                                                            {/if}
                                                        </form>
                                                        {if isset($form.list) and is_array($form.list) and count($form.list)}
                                                            {if isset($form.list.headers) and is_array($form.list.headers) and count($form.list.headers)}
                                                                {if $form.tab eq 'fields_comments'}
                                                                    <div class="clearfix">
                                                                        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-3 nopadding-xs">
                                                                            <div class="pull-left col-xs-12 nopadding">
                                                                                <span id="btn-manage_field_options" class="btn btn-default btn-block">
                                                                                    <i class="fa-pts fa-pts-list nohover"></i>
                                                                                    {l s='Manage field options' mod='carrierpickupstore'}
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                {/if}
                                                                <div class="row">&nbsp;</div>
                                                                <div class="table-responsive">
                                                                    <div class="pts-overlay"></div>
                                                                    <table class="table table-bordered" id="{$form.list.table|escape:'htmlall':'UTF-8'}">
                                                                        <thead>
                                                                            <tr>
                                                                                {foreach from=$form.list.headers item='header_text' key='header'}
                                                                                    <th {if $header eq 'actions'}class="col-sm-2 col-md-1 text_center"{/if}>{$header_text|escape:'htmlall':'UTF-8'}</th>
                                                                                {/foreach}
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody></tbody>
                                                                    </table>
                                                                </div>
                                                            {/if}
                                                        {/if}
                                                    </div>
                                                {/foreach}

                                                {include file=$paramsBack.MODULE_TPL|cat:'views/templates/admin/global_tabs.tpl' tabs=$paramsBack.HELPER_FORM.tabs}
                                            {/if}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </div>
        {include file=$paramsBack.MODULE_TPL|cat:'views/templates/admin/helper/credits.tpl'}
    </div>
{/if}
