{*
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* We are experts and professionals in PrestaShop
*
* @author    PresTeamShop.com <support@presteamshop.com>
* @copyright 2011-2021 PresTeamShop
* @license   see file: LICENSE.txt
* @category  PrestaShop
* @category  Module
*}

<script src="https://maps.googleapis.com/maps/api/js?key={$paramsHook.API_KEY|escape:'htmlall':'UTF-8'}&&libraries=places" async defer></script>