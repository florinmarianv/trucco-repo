{*
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* We are experts and professionals in PrestaShop
*
* @author    PresTeamShop.com <support@presteamshop.com>
* @copyright 2011-2021 PresTeamShop
* @license   see file: LICENSE.txt
* @category  PrestaShop
* @category  Module
*}

<div class="panel" id="cps_admin_order">
    <div class="panel-heading">
        <i class="icon-home"></i> {l s='Store' mod='carrierpickupstore'}
    </div>
    <p>
        {l s='Customer has chosen pick up in' mod='carrierpickupstore'}:&nbsp;<b>{$paramsHook.STORE_NAME|escape:'htmlall':'UTF-8'}&nbsp;({$paramsHook.STORE_ADDRESS|escape:'htmlall':'UTF-8'})</b>
    </p>
</div>