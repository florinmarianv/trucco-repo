{*
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* We are experts and professionals in PrestaShop
*
* @author    PresTeamShop.com <support@presteamshop.com>
* @copyright 2011-2021 PresTeamShop
* @license   see file: LICENSE.txt
* @category  PrestaShop
* @category  Module
*}

{if isset($paramsHook.JS_FILES)}
    {foreach from=$paramsHook.JS_FILES item="file"}
        <script type="text/javascript" src="{$file|escape:'htmlall':'UTF-8'}"></script>
    {/foreach}
{/if}

{if $paramsHook.installed_opc}
    <div> &nbsp; </div>
{/if}

<div id="stores-container" class="row pts bootstrap">
    <div class="error alert alert-danger" id="error-store" style="display: none;">
        <span>{l s='Select a store for shipping' mod='carrierpickupstore'}</span>
    </div>

    <div class="form-horizontal" id="carrierpickupstore_form">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="input-group my-1">
                <select id="id_store_carrier_step" name="id_store" class="opt_id_store not_uniform not_unifrom input-sm {if $paramsHook.installed_opc} form-control {/if}">
                    {if count($paramsHook.STORES) > 1}
                        <option value="0" {if !$paramsHook.store_selected}selected{/if}>
                        -- {l s='Choose a pick up store' mod='carrierpickupstore'} -- </option>
                    {/if}
                    {foreach from=$paramsHook.STORES item='store' name='stores'}
                        <option value="{$store.id|intval}" {if $paramsHook.store_selected and $store.id eq $paramsHook.store_selected}selected{/if}>{$store.name|escape:'htmlall':'UTF-8'}&nbsp;({$store.address1|escape:'htmlall':'UTF-8'})</option>
                    {/foreach}
                </select>
                {if $paramsHook.CONFIGS.CPS_SHOW_GOOGLE_MAPS neq ''}
                    <div class="input-group-btn">
                        <button class="btn btn-primary" id="see_map" type="button">
                            <i class="fa-pts fa-pts-map-marker" aria-hidden="true"></i>
                            {l s='See map' mod='carrierpickupstore'}
                        </button>
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>