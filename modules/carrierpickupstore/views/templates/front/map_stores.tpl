{*
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* We are experts and professionals in PrestaShop
*
* @author    PresTeamShop.com <support@presteamshop.com>
* @copyright 2011-2021 PresTeamShop
* @license   see file: LICENSE.txt
* @category  PrestaShop
* @category  Module
*}

<div id="stores-map-container">
    <div class="clearfix">
        {*<pre>
            {$paramsHook.STORES|print_r}
        </pre>*}
        <div id="stores_list" class="panel-group col-12 col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 clearfix">
            {foreach from=$paramsHook.STORES item='store' name='stores'}
                <div class="stores-content card">
                        <div class="header-content">
                            <label for="id_store_map_{$store.id|escape:'htmlall':'UTF-8'}" class="pts-radio">
                                <input type="radio" class="not_unifrom not_uniform" id="id_store_map_{$store.id|escape:'htmlall':'UTF-8'}" name="id_store_map" value="{$store.id|escape:'htmlall':'UTF-8'}">
                                <span class="radiokmark"></span>
                            </label>
                            <a class="collapsed header-content-link" data-toggle="collapse" href="#collapse_{$store.id|escape:'htmlall':'UTF-8'}">
                                <div class="header-text">
                                    <h5 class="store_name">{$store.name|escape:'htmlall':'UTF-8'}</h5>
                                    <span class="address">{$store.address1|escape:'htmlall':'UTF-8'}</span>
                                    <span class="location">{if not empty($store.postcode)}{$store.postcode|escape:'htmlall':'UTF-8'}{/if}, {if not empty($store.city|escape:'htmlall':'UTF-8')}{$store.city|escape:'htmlall':'UTF-8'}{/if}, {$store.state_name|escape:'htmlall':'UTF-8'}, {$store.country_name|escape:'htmlall':'UTF-8'}</span>
                                </div>
                                <i class="fa-pts fa-pts-chevron-down" aria-hidden="true"></i>
                            </a>
                        </div>
                    <div id="collapse_{$store.id|escape:'htmlall':'UTF-8'}" class="body-content collapse" data-parent="#stores_list">
                        {if not empty($store.email)}
                            <i class="fa-pts fa-pts-envelope" aria-hidden="true"></i>
                            <span class="email">{$store.email|escape:'htmlall':'UTF-8'}</span>
                            <br>
                        {/if}
                        {if not empty($store.phone)}
                            <i class="fa-pts fa-pts-phone" aria-hidden="true"></i>
                            <span class="phone">{$store.phone|escape:'htmlall':'UTF-8'}</span>
                            <br>
                        {/if}
                        {if count($store.hours) gt 0}
                            <i class="fa-pts fa-pts-clock-o" aria-hidden="true"></i>
                            <span class="schedule"> {l s='Schedule:' mod='carrierpickupstore'}</span>
                            <br>
                            <div class="schedule-content">
                                {foreach from=$store.hours item='hour'}
                                    <div class="row">
                                        <div class="day col-12 col-xs-12 col-sm-12 col-md-6 col-lg-5 col-xl-5">
                                            <i class="fa-pts fa-pts-caret-right" aria-hidden="true"></i> {$hour.day|escape:'htmlall':'UTF-8'}
                                        </div>
                                        <div class="hour col-12 col-xs-12 col-sm-12 col-md-6 col-lg-7 col-xl-7">
                                            {if empty($hour.hour)}
                                                {l s='No information' mod='carrierpickupstore'}
                                            {else}
                                                {$hour.hour|escape:'htmlall':'UTF-8'}
                                            {/if}
                                        </div>
                                    </div>
                                {/foreach}
                            </div>
                        {/if}

                    </div>
                </div>
            {/foreach}
        </div>
        <div id="stores_map" class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-8 col-xl-8">
            <input id="pac-input" class="controls not_unifrom not_uniform" type="text" placeholder="Search Box">
            <div id="map"></div>
        </div>
    </div>
</div>

