{*
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* We are experts and professionals in PrestaShop
*
* @author    PresTeamShop.com <support@presteamshop.com>
* @copyright 2011-2021 PresTeamShop
* @license   see file: LICENSE.txt
* @category  PrestaShop
* @category  Module
*}

<tbody>
    {foreach from=$products item=product}
        <tr>
            <td style="border:1px solid #D6D4D4;">
                <table class="table">
                    <tr>
                        <td width="10">&nbsp;</td>
                        <td>
                            <font size="2" face="Open-sans, sans-serif" color="#555454">
                            {$product['product_reference']|escape:'htmlall':'UTF-8'}
                            </font>
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td style="border:1px solid #D6D4D4;">
                <table class="table">
                    <tr>
                        <td width="10">&nbsp;</td>
                        <td>
                            <font size="2" face="Open-sans, sans-serif" color="#555454">
                            <strong>{$product['product_name']|escape:'htmlall':'UTF-8'}</strong>
                            </font>
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td style="border:1px solid #D6D4D4;">
                <table class="table">
                    <tr>
                        <td width="10">&nbsp;</td>
                        <td align="right">
                            <font size="2" face="Open-sans, sans-serif" color="#555454">
                            {$product['unit_price']|escape:'htmlall':'UTF-8'}
                            </font>
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td style="border:1px solid #D6D4D4;">
                <table class="table">
                    <tr>
                        <td width="10">&nbsp;</td>
                        <td align="right">
                            <font size="2" face="Open-sans, sans-serif" color="#555454">
                            {$product['product_quantity']|intval}
                            </font>
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td style="border:1px solid #D6D4D4;">
                <table class="table">
                    <tr>
                        <td width="10">&nbsp;</td>
                        <td align="right">
                            <font size="2" face="Open-sans, sans-serif" color="#555454">
                            {$product['total_price']|escape:'htmlall':'UTF-8'}
                            </font>
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        {if isset($product['customization'])}
            {foreach $product['customization'] as $customization}
                <tr>
                    <td colspan="2" style="border:1px solid #D6D4D4;">
                        <table class="table">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td>
                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                    <strong>{$product['product_name']|escape:'htmlall':'UTF-8'}</strong><br>
                                    {$customization['customization_text']|escape:'htmlall':'UTF-8'}
                                    </font>
                                </td>
                                <td width="10">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td style="border:1px solid #D6D4D4;">
                        <table class="table">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td align="right">
                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                    {$product['unit_price']|escape:'htmlall':'UTF-8'}
                                    </font>
                                </td>
                                <td width="10">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td style="border:1px solid #D6D4D4;">
                        <table class="table">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td align="right">
                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                    {$customization['customization_quantity']|intval}
                                    </font>
                                </td>
                                <td width="10">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td style="border:1px solid #D6D4D4;">
                        <table class="table">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td align="right">
                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                    {$customization['quantity']|intval}
                                    </font>
                                </td>
                                <td width="10">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            {/foreach}
        {/if}
    {/foreach}
</tbody>