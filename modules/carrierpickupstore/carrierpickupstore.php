<?php
/**
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * We are experts and professionals in PrestaShop
 *
 * @author    PresTeamShop.com <support@presteamshop.com>
 * @copyright 2011-2021 PresTeamShop
 * @license   see file: LICENSE.txt
 * @category  PrestaShop
 * @category  Module
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once _PS_MODULE_DIR_.'/carrierpickupstore/classes/CPSCore.php';
require_once _PS_MODULE_DIR_.'/carrierpickupstore/classes/CPSCarrierPickupStore.php';
require_once _PS_MODULE_DIR_.'/carrierpickupstore/classes/commons/CPSStoreFunctions.php';

class CarrierPickupStore extends Module
{
    const VERSION = '4.0.2';

    public $core;
    public $errors = array();
    public $warnings = array();
    public $html = '';
    public $prefix_module;
    public $params_back;
    public $override_css;
    public $override_js;
    public $translations_path;
    public $globals = array();
    public $config_vars = array();
    public $configure_vars = array(
        array('name' => 'CPS_ENABLE_DEBUG', 'default_value' => '0', 'is_html' => false, 'is_bool' => true),
        array('name' => 'CPS_IP_DEBUG', 'default_value' => '', 'is_html' => false, 'is_bool' => false),
        array('name' => 'CPS_VERSION', 'default_value' => self::VERSION, 'is_html' => false, 'is_bool' => false),
        array('name' => 'CPS_OVERRIDE_CSS', 'default_value' => '', 'is_html' => false, 'is_bool' => false),
        array('name' => 'CPS_OVERRIDE_JS', 'default_value' => '', 'is_html' => false, 'is_bool' => false),
        array('name' => 'CPS_SHOW_GOOGLE_MAPS', 'default_value' => '0', 'is_html' => false, 'is_bool' => true),
        array('name' => 'CPS_SHOW_API_KEY', 'default_value' => '0', 'is_html' => false, 'is_bool' => true),
        array('name' => 'CPS_API_KEY', 'default_value' => '', 'is_html' => false, 'is_bool' => false),
        array('name' => 'CPS_ZOOM', 'default_value' => '5', 'is_html' => false, 'is_bool' => false),
        array(
            'name' => 'CPS_COORDINATES',
            'default_value' => '40.4165000, -3.7025600',
            'is_html' => false,
            'is_bool' => false
        ),
        array('name' => 'CPS_SEND_MAIL_TO_STORE', 'default_value' => '0', 'is_html' => false, 'is_bool' => true),
        array('name' => 'CPS_CUSTOM_SHOP_ICONS', 'default_value' => '0', 'is_html' => false, 'is_bool' => true)
    );

    protected $smarty;
    protected $cookie;

    private $installed_opc = false;
    private $max_marker_icon_width = 50;
    private $max_marker_icon_height = 50;

    public function __construct()
    {
        $this->prefix_module = 'CPS';
        $this->name = 'carrierpickupstore';
        $this->tab = 'front_office_features';
        $this->version = '4.0.2';
        $this->author = 'PresTeamShop';
        $this->module_key = 'f118d5627b19bd598e5fdd05bbe349b1';
        $this->bootstrap = true;

        if (property_exists($this, 'controllers')) {
            $this->controllers = array('main');
        }

        parent::__construct();

        $this->errors = array();
        $this->warnings = array();
        $this->params_back = array();
        $this->globals = new stdClass();

        $this->core = new CPSCore($this, $this->context);

        $this->displayName = $this->l('Carrier Pick Store');
        $this->description = $this->l('List the pickup points of your physical stores');
        $this->confirmUninstall = $this->l('Are you sure you want uninstall?');

        if ($onepagecheckoutps = $this->core->isModuleActive('onepagecheckoutps')) {
            if ($onepagecheckoutps->core->isVisible()) {
                $this->installed_opc = true;
            }
        }
        $this->core->checkModulePTS();
    }

    public function install()
    {
        if (!parent::install()
            || !$this->core->install()
            || !$this->registerHook('displayAdminOrder')
            || !$this->registerHook('displayCarrierExtraContent')
            || !$this->registerHook('header')
            || !$this->registerHook('actionCreateAddressCPS')
            || !$this->registerHook('actionObjectStoreUpdateAfter')
            || !$this->registerHook('actionObjectStoreDeleteAfter')
            || !$this->registerHook('actionValidateOrder')
            || !$this->registerHook('actionOrderStatusUpdate')
            || !$this->registerHook('actionObjectOrderAddBefore')
        ) {
            return false;
        }

        if ($this->getCarrier() === false || !$this->createCustomer()) {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() || !$this->core->uninstall()) {
            return false;
        }

        return true;
    }

    public function getContent()
    {
        $registered_version = Configuration::get($this->prefix_module.'_VERSION');

        if ($registered_version == $this->version) {
            $forms = $this->getHelperForm();
            
            if (Tools::isSubmit('form-settings')) {
                $this->uploadShopIcon('shop_unselected_icon');
                $this->uploadShopIcon('shop_selected_icon');
            }
            
            if (is_array($forms)
                && count($forms)
                && isset($forms['forms'])
                && is_array($forms['forms'])
                && count($forms['forms'])
            ) {
                foreach ($forms['forms'] as $key => $form) {
                    if (Tools::isSubmit('form-'.$key)) {
                        $this->smarty->assign('CURRENT_FORM', $key);
                        $this->core->saveFormData($form);

                        $this->smarty->assign('show_saved_message', true);
                        break;
                    }
                }
            }
        }
        $this->displayForm();

        return $this->html;
    }

    protected function displayForm()
    {
        $content = $this->core->getContentUpgrade();
        if ($content !== true) {
            $this->html = $content;
            return false;
        }

        $this->context->controller->addJS(
            $this->path.'views/js/lib/bootstrap/plugins/bootstrap-filestyle.min.js?v='.$this->version
        );

        $this->getTemplateVarsBackOffice($this->params_back);

        Media::addJsDef(array(
            'url_contact_addons'    => '',
            'url_opinions_addons'   => 'http://addons.prestashop.com/ratings.php',
        ));

        $this->core->displayForm();

        $this->html .= $this->display(__FILE__, 'views/templates/admin/configure.tpl');
    }

    private function getTemplateVarsBackOffice(&$template_vars)
    {
        $helper_form = $this->getHelperForm();
        $this->getExtraTabs($helper_form);

        $template_vars = array(
            'HELPER_FORM' => $helper_form,
            'LANGUAGES' => Language::getLanguages(false),
            'id_lang'  => $this->context->employee->id_lang,
            'MODULE_PREFIX' => $this->prefix_module,
            'STATIC_TOKEN' => Tools::getAdminTokenLite('AdminModules'),
            'remote_addr' => Tools::getRemoteAddr(),
            'id_shop' => Shop::getContextShopID(),
            'marker_icons' => $this->getMarkerIcons(),
            'url_manage_stores' => $this->context->link->getAdminLink('AdminStores'),
            'url_manage_countries' => $this->context->link->getAdminLink('AdminCountries'),
            'Msg' => array(
                'add_IP' => $this->l('Add IP'),
                'button_api_key' => $this->l('Modify API'),
                'api_modifying' => $this->l('If the Api key is modified, it will also be modified in the One Page Checkout PS module'),
                'confirm' => $this->l('Are you sure to modify the API KEY?'),
                'credits_developed'=> $this->l('Developed by=> PresTeamShop'),
                'credits_spanish'=> $this->l('User guide module (Spanish)'),
                'credits_english'=> $this->l('User guide module (English)'),
                'credits_development'=> $this->l('Development of templates, modules and custom modifications.'),
                'credits_information'=> $this->l('Information'),
                'credits_support'=> $this->l('Support'),
                'credits_website'=> $this->l('Web site & Store'),
                'credits_suggestions'=> $this->l('Suggestions, bugs or problems to report here!'),
                'remove_dependencies'=> $this->l('Are you sure that you want to delete all dependencies and options?'),
                'confirm_delete_icon' => $this->l('Are you sure to delete the icon?'),
                'input_file_text' => $this->l('Choose a file'),
                'no_stores_created' => $this->l('You do not have stores created, remember to create them from here:'),
                'manage_stores' => $this->l('Manage stores')
            )
        );
    }

    public function getHelperForm()
    {
        $tabs = $this->getHelperTabs();
        $settings = $this->getSettingsForm();
        $stores = $this->getStoresForm();

        $form = array(
            'tabs' => $tabs,
            'forms' => array(
                'settings' => $settings,
                'stores' => $stores,
            )
        );

        return $form;
    }

    private function getHelperTabs()
    {
        $tabs = array(
            'settings' => array(
                'label' => $this->l('Settings'),
                'href' => 'settings',
                'icon' => 'cog'
            ),
            'stores' => array(
                'label' => $this->l('Stores'),
                'href' => 'stores',
                'icon' => 'shopping-cart'
            ),
        );

        return $tabs;
    }

    private function uploadShopIcon($file)
    {
        if (empty($_FILES[$file]['name'])) {
            return true;
        }

        $image_size = getimagesize($_FILES[$file]['tmp_name']);
        $width = $image_size[0];
        $height = $image_size[1];

        if ($width > $this->max_marker_icon_width || $height > $this->max_marker_icon_height) {
            $this->errors[] = sprintf(
                $this->l('Image size exceeds allowed size: %s'),
                $this->max_marker_icon_width.'x'.$this->max_marker_icon_height.'px'
            );
            return false;
        }

        $path_upload   = _PS_MODULE_DIR_.$this->name.'/views/img/icon/';
        if ($_FILES[$file]['error'] == 0) {
            $extension  = pathinfo($_FILES[$file]['name'], PATHINFO_EXTENSION);
            if ($extension !== 'png') {
                $this->errors[] = $this->l('Only images with the extension .png are allowed');
                return false;
            }

            $icon_name = $file.'_'.$this->context->shop->id.'.png';
            if (move_uploaded_file($_FILES[$file]['tmp_name'], $path_upload.$icon_name)) {
                return true;
            }
        }

        $this->errors[] = $this->l('Could not upload icon');
        return false;
    }

    public function saveCustomConfigValue($option, &$config_var_value)
    {
        switch ($option['name']) {
            case 'api_key':
                if ($this->installed_opc) {
                    Configuration::updateValue('OPC_GOOGLE_API_KEY', $config_var_value);
                }
                break;
        }
    }

    public function getSettingsForm()
    {
        $api_key = array(
            'api_key' => array(
                'name' => 'api_key',
                'prefix' => 'txt',
                'label' => $this->l('Api key'),
                'type' => $this->globals->type_control->textbox,
                'value' => $this->config_vars['CPS_API_KEY'],
                'hidden_on' => false,
            ),
        );

        if ($this->installed_opc) {
            $opc_autocomplete_address = Configuration::get('OPC_AUTOCOMPLETE_GOOGLE_ADDRESS');
            $opc_google_api_key = Configuration::get('OPC_GOOGLE_API_KEY');
            if ($opc_autocomplete_address) {
                $this->config_vars['CPS_API_KEY'] = $opc_google_api_key;
                $api_key = [
                    'show_api_key' => array(
                        'name' => 'show_api_key',
                        'prefix' => 'chk',
                        'label' => $this->l('Modify Api Key'),
                        'type' => $this->globals->type_control->checkbox,
                        'check_on' => $this->config_vars['CPS_SHOW_API_KEY'],
                        'label_on' => $this->l('YES'),
                        'label_off' => $this->l('NO'),
                        'hidden_on' => false,
                        'tooltip' => array(
                            'information' => array(
                                'title'   => $this->l('Info'),
                                'content' => $this->l('Default api key is gotten from One Page Checkout PS Module. If the api key value is modified, It will be reflected in One Page Checkout PS too.'),
                            ),

                        ),
                        'depends' => $api_key,
                    ),
                ];
            }
        }

        $zoom_values = array();
        for ($i = 2; $i <= 18; $i++) {
            $zoom_values[] = array('value' => $i, 'text' => $i);
        }

        $depends_maps = $api_key;
        $depends_maps['coordinates'] = array(
            'name' => 'coordinates',
            'prefix' => 'txt',
            'label' => $this->l('Center the map on'),
            'type' => $this->globals->type_control->textbox,
            'value' => $this->config_vars['CPS_COORDINATES'],
            'hidden_on' => false,
            'tooltip' => array(
                'information' => array(
                    'title'   => $this->l('Info'),
                    'content' => $this->l('Enter latitude and longitude separated by comma. Example: 40.4165000, -3.7025600'),
                )
            )
        );
        $depends_maps['zoom'] = array(
            'name' => 'zoom',
            'prefix' => 'lst',
            'label' => $this->l('Zoom'),
            'type' => $this->globals->type_control->select,
            'data'   => $zoom_values,
            'hidden_on' => false,
            'default_option' => $this->config_vars['CPS_ZOOM'],
            'option_value' => 'value',
            'option_text' => 'text',
            'tooltip' => array(
                'information' => array(
                    'title'   => $this->l('Info'),
                    'content' => $this->l('Allowed values between 2 to 18.'),
                )
            )
        );

        $depends_maps['custom_shop_icons'] = array(
            'name' => 'custom_shop_icons',
            'prefix' => 'chk',
            'label' => $this->l('Set custom icons for map markers'),
            'type' => $this->globals->type_control->checkbox,
            'check_on' => $this->config_vars['CPS_CUSTOM_SHOP_ICONS'],
            'label_on' => $this->l('YES'),
            'label_off' => $this->l('NO'),
            'hidden_on' => false,
            'tooltip' => array(
                'information' => array(
                    'title'   => $this->l('Info'),
                    'content' => sprintf(
                        $this->l('Only images with .png extension are allowed, the maximum size of the image to upload is: %s'),
                        $this->max_marker_icon_width.'x'.$this->max_marker_icon_height.'px'
                    )
                )
            ),
            'depends' => array(
                'shop_selected_icon' => array(
                    'name' => 'shop_selected_icon',
                    'prefix' => 'fl',
                    'label' => $this->l('Icon for selected shop marker'),
                    'type' => '',
                    'value' => '',
                    'accept' => 'image/png',
                    'hidden_on' => false,
                ),
                'shop_unselected_icon' => array(
                    'name' => 'shop_unselected_icon',
                    'prefix' => 'fl',
                    'label' => $this->l('Icon for not selected shop marker'),
                    'type' => '',
                    'value' => '',
                    'accept' => 'image/png',
                    'hidden_on' => false,
                )
            )
        );

        $options = array(
            'enable_debug' => array(
                'name' => 'enable_debug',
                'prefix' => 'chk',
                'label' => $this->l('Test mode'),
                'type' => $this->globals->type_control->checkbox,
                'check_on' => $this->config_vars['CPS_ENABLE_DEBUG'],
                'label_on' => $this->l('YES'),
                'label_off' => $this->l('NO'),
                'depends' => array(
                    'ip_debug' => array(
                        'name' => 'ip_debug',
                        'prefix' => 'txt',
                        'label' => $this->l('Test IP'),
                        'type' => $this->globals->type_control->textbox,
                        'value' => $this->config_vars['CPS_IP_DEBUG'],
                        'hidden_on' => false
                    )
                ),
                'tooltip' => array(
                    'warning' => array(
                        'title'   => $this->l('Warning'),
                        'content' => $this->l('It is recommended that you enable this option to test the module in your store before enabling it for clients.'),
                    )
                )
            ),
            'show_google_maps' => array(
                'name' => 'show_google_maps',
                'prefix' => 'chk',
                'label' => $this->l('Show Google Maps'),
                'type' => $this->globals->type_control->checkbox,
                'check_on' => $this->config_vars['CPS_SHOW_GOOGLE_MAPS'],
                'label_on' => $this->l('YES'),
                'label_off' => $this->l('NO'),
                'depends' => $depends_maps,
            ),
            'send_mail_to_store' => array(
                'name' => 'send_mail_to_store',
                'prefix' => 'chk',
                'label' => $this->l('Send email to the selected store'),
                'type' => $this->globals->type_control->checkbox,
                'check_on' => $this->config_vars['CPS_SEND_MAIL_TO_STORE'],
                'label_on' => $this->l('YES'),
                'label_off' => $this->l('NO'),
                'tooltip' => array(
                    'warning' => array(
                        'title'   => $this->l('Info'),
                        'content' => $this->l('Send notification email to the selected store when a new order is created'),
                    )
                )
            ),
        );

        $form = array(
            'tab' => 'settings',
            'method' => 'post',
            'actions' => array(
                'save' => array(
                    'label' => $this->l('Save'),
                    'class' => 'settings',
                    'icon' => 'save'
                )
            ),
            'options' => $options
        );

        return $form;
    }

    public function getStoresForm()
    {
        $list = $this->getStoresList();

        $form = array(
            'id' => 'form_stores',
            'tab' => 'stores',
            'method' => 'post',
            'list'  => $list,
            'options' => array()
        );

        return $form;
    }

    public function getStoresList()
    {
        $stores = CPSStoreFunctions::getStores();
        if (is_array($stores) && count($stores) > 0) {
            foreach ($stores as &$store) {
                $country = new Country($store['id_country']);
                $store['active_country'] = (int) $country->active;
            }
        }

        $actions = array(
            'editStore' => array(
                'action_class' => 'CPSStore',
                'class' => 'has-action nohover',
                'icon' => 'edit',
                'title' => $this->l('Edit'),
                'tooltip' => $this->l('Edit')
            )
        );

        $headers  = array(
            'id' => $this->l('ID'),
            'name' => $this->l('Name'),
            'country_name' => $this->l('Country'),
            'state_name' => $this->l('State'),
            'address1' => $this->l('Address'),
            'active' => $this->l('Active'),
            'visible' => $this->l('Visibility'),
            'actions' => $this->l('Actions')
        );

        $truncate = array(
            'description' => 30,
            'label' => 30
        );

        $status = array(
            'visible' => array(
                'action_class' => 'CPSStore',
                'action' => 'toggleVisibility',
                'class' => 'has-action'
            ),
            'active' => array(
                'action_class' => 'CPSStore',
                'action' => 'toggleActive',
                'class' => 'has-action'
            )
        );

        $color = array(
            array(
                'by' => 'active_country',
                'colors' => array(
                    '0' => 'inactive-country'
                )
            )
        );

        return array(
            'message_code' => $this->core->CODE_SUCCESS,
            'content' => $stores,
            'table' => 'table-stores',
            'headers' => $headers,
            'actions' => $actions,
            'truncate' => $truncate,
            'status' => $status,
            'color' => $color,
            'prefix_row' => 'store',
        );
    }

    public function getStoreURL()
    {
        $id_store = Tools::getValue('id_store');

        $params = array(
            'id_store' => $id_store,
            'updatestore' => 1
        );

        $url = $this->context->link->getAdminLink('AdminStores', true, array(), $params);
        if (!empty($url)) {
            return array('message_code' => 1, 'url' => $url);
        } else {
            return array(
                'message_code' => $this->core->CODE_ERROR,
                'message' => $this->l('There was an error on getting url.')
            );
        }
    }

    public function modifyAPIKey()
    {
        $opc_autocomplete_address = Configuration::get('OPC_AUTOCOMPLETE_GOOGLE_ADDRESS');
        if ($this->installed_opc && $opc_autocomplete_address) {
            return array('message_code' => $this->core->CODE_SUCCESS, 'result' => 1);
        } else {
            return array(
                'message_code' => $this->core->CODE_ERROR,
                'result' => 0
            );
        }
    }

    /**
     * Establece la visibilidad de las tiendas de recogida en el FrontOffice
     */
    public function updateStoreVisibility()
    {
        $id_store = (int) Tools::getValue('id_store');
        $active = Tools::getValue('active');
        $visibility = CPSStoreFunctions::checkVisibility($id_store);

        if (CPSStoreFunctions::toggleVisibility($id_store, $visibility)) {
            if (($visibility && $active) || (!$visibility && !$active)) {
                CPSStoreFunctions::toggleActive($id_store);
            }
            
            return array(
                'message_code' => $this->core->CODE_SUCCESS,
                'message' => $this->l('Visibility updated successful.'),
            );
        } else {
            return array(
                'message_code' => $this->core->CODE_ERROR,
                'message' => $this->l('There was an error on updating the visibility.')
            );
        }
    }

    public function updateStoreStatus()
    {
        $id_store = Tools::getValue('id_store');
        $visible = Tools::getValue('visible');
        
        $store = new Store($id_store);
        $store->active = (!$store->active);

        if ($store->update()) {
            if (!$store->active && $visible) {
                CPSStoreFunctions::toggleVisibility($id_store, $visible);
            }

            return array(
                'message_code' => $this->core->CODE_SUCCESS,
                'message' => $this->l('Visibility updated successful.'),
            );
        } else {
            return array(
                'message_code' => $this->core->CODE_ERROR,
                'message' => $this->l('There was an error on updating the visibility.')
            );
        }
    }

    /**
     * Extra tabs for PresTeamShop
     * @param type $helper_form
     */
    private function getExtraTabs(&$helper_form)
    {
        $helper_form['tabs']['translate'] = array(
            'label' => $this->l('Translate'),
            'href' => 'translate',
            'icon' => 'globe'
        );

        $helper_form['tabs']['code_editors'] = array(
            'label' => $this->l('Code Editors'),
            'href' => 'code_editors',
            'icon' => 'code'
        );

        $helper_form['tabs']['another_modules'] = array(
            'label' => $this->l('Another modules'),
            'href' => 'another_modules',
            'icon' => 'cubes',
        );

        $helper_form['tabs']['suggestions']    = array(
            'label' => $this->l('Suggestions'),
            'href' => 'suggestions',
            'icon' => 'pencil'
        );
    }

//    public function hookDisplayAdminOrderMain($params)
//    {
//        if (!$this->core->isVisible()
//            || !$this->core->checkModulePTS()
//            || version_compare(_PS_VERSION_, '1.7.7.0', '<')
//        ) {
//            return false;
//        }
//
//        $this->context->controller->addCSS($this->_path.'views/css/admin/configure.css', 'all');
//
//        $cart = Cart::getCartByOrderId($params['id_order']);
//        return $this->seeOrderAdminDetail($cart);
//    }
//
//    private function seeOrderAdminDetail(Cart $cart)
//    {
//        if (Validate::isLoadedObject($cart)) {
//            if ((int) $cart->id_carrier === (int) $this->getCarrier()) {
//                $carrier_pickup_store = CPSCarrierPickupStore::getByIdCart($cart->id);
//                if (!$carrier_pickup_store) {
//                    return false;
//                }
//                $store = $this->getStoreById($carrier_pickup_store->id_store);
//                $params_hook = array(
//                    'STORE_NAME'    => $store['name'],
//                    'STORE_ADDRESS' => $store['address1'],
//                    'main' => version_compare(_PS_VERSION_, '1.7.7.0', '>=') ? true : false
//                );
//
//                $this->smarty->assign('paramsHook', $params_hook);
//
//                return $this->display(__FILE__, 'views/templates/hook/admin_order.tpl');
//            }
//        }
//
//        return false;
//    }

    public function hookDisplayAdminOrder($params)
    {
        if (!$this->core->isVisible() || !$this->core->checkModulePTS()) {
            return false;
        }

        $this->context->controller->addCSS($this->_path.'views/css/admin/configure.css', 'all');

        $cart = (is_null($params['cart'])) ? Cart::getCartByOrderId($params['id_order']) : $params['cart'];

        if ((int) $cart->id_carrier === (int) $this->getCarrier()) {
            $carrier_pickup_store   = CPSCarrierPickupStore::getByIdCart($cart->id);
            if (!$carrier_pickup_store) {
                return false;
            }

            $store = $this->getStoreById($carrier_pickup_store->id_store);
            $params_hook = array(
                'STORE_NAME'    => $store['name'],
                'STORE_ADDRESS' => $store['address1']
            );

            $this->smarty->assign('paramsHook', $params_hook);

            return $this->display(__FILE__, 'views/templates/hook/admin_order.tpl');
        }

        return false;
    }

    public function hookHeader()
    {
        $stores = CPSStoreFunctions::getStores();

        $controller = Tools::getValue('controller');
        if (!$this->core->isVisible() || !$this->core->checkModulePTS() || $controller !== 'order' || empty($stores)) {
            return false;
        }

        $this->context->controller->addJS(
            $this->_path.'views/js/lib/jquery/plugins/total-storage/jquery.total-storage.min.js'
        );
        $this->context->controller->addJS($this->_path.'views/js/lib/pts/tools.js');
        $this->context->controller->addJS($this->_path.'views/js/front/map_stores.js');
        $this->context->controller->addJS($this->_path.'views/js/front/carrierpickupstore.js');
        $this->context->controller->addJS($this->_path.'views/js/front/override.js');

        $this->context->controller->addCSS($this->_path.'views/css/front/override.css', 'all');

        $this->context->controller->addCSS($this->_path.'views/css/lib/font-awesome/font-awesome.css');
        $this->context->controller->addCSS($this->_path.'views/css/lib/bootstrap/pts/pts-bootstrap.css');
        
        $this->context->controller->addCSS($this->_path.'views/css/front/extra_carrier.css', 'all');
        $this->context->controller->addCSS($this->_path.'views/css/front/map_stores.css', 'all');

        $id_carrier = $this->getCarrier();
        if ($id_carrier === false) {
            return false;
        }

        $load_script_google_maps = true;
        if ($this->installed_opc) {
            $opc_autocomplete_address = Configuration::get('OPC_AUTOCOMPLETE_GOOGLE_ADDRESS');
            $opc_google_api_key = Configuration::get('OPC_GOOGLE_API_KEY');

            if ((bool) $opc_autocomplete_address && !empty($opc_google_api_key)) {
                $load_script_google_maps = false;
            }
        }

        $marker_icons = $this->getMarkerIcons(false);
        $shop_selected_icon = $marker_icons['shop_selected_icon'];
        $shop_unselected_icon = !empty($marker_icons['shop_unselected_icon']) ? $marker_icons['shop_unselected_icon'] : 'shop.png';

        Media::addJsDef(array(
            'CarrierPickupStore' => array(
                'pts_static_token' => Tools::encrypt('carrierpickupstore/index'),
                'installed_opc' => $this->installed_opc,
                'src_img' => $this->_path.'views/img/',
                'stores' => $stores,
                'configs' => $this->config_vars,
                'id_cart' => isset($this->context->cart->id) ? $this->context->cart->id : 0,
                'actions_carrierpickupstore' => $this->context->link->getModuleLink($this->name, 'actions'),
                'id_carrierpickupstore' => (int) $id_carrier,
                'shop_selected_icon' => $shop_selected_icon,
                'shop_unselected_icon' => $shop_unselected_icon,
                'MsgCPS'  => array(
                    'need_select_pickup_point' => $this->l('You need to select on shipping a pickup point to continue with the purchase.'),
                    'pick_up_store'  => $this->l('Pick up store'),
                    'msg_save'       => $this->l('Save'),
                    'msg_latitude'   => $this->l('Latitude'),
                    'msg_longitude'  => $this->l('Longitude'),
                    'msg_address'    => $this->l('Address'),
                    'msg_postcode'   => $this->l('Postcode'),
                    'msg_city'   => $this->l('City'),
                    'msg_state'  => $this->l('State'),
                    'msg_email'  => $this->l('Email'),
                    'msg_phone'  => $this->l('Phone'),
                    'msg_fax'    => $this->l('Fax'),
                    'msg_day'    => $this->l('Day'),
                    'msg_hour'   => $this->l('Hour'),
                    'permission_denied'      => $this->l('User denied the request for Geolocation'),
                    'position_unavailable'   => $this->l('Location information is unavailable'),
                    'timeout'        => $this->l('The request to get user location timed out'),
                    'unknown_error'  => $this->l('An unknown error occurred'),
                    'close_modal_btn'  => $this->l('Close'),
                ),
            )
        ));

        if ($load_script_google_maps && $this->config_vars['CPS_SHOW_GOOGLE_MAPS']) {
            $this->smarty->assign('paramsHook', array(
                'API_KEY' => $this->config_vars['CPS_API_KEY']
            ));

            return $this->display(__FILE__, 'views/templates/hook/header.tpl');
        }
    }

    public function hookDisplayCarrierExtraContent($params)
    {
        if (!$this->core->isVisible() || !$this->core->checkModulePTS()) {
            return false;
        }

        $id_carrier = (int) $this->getCarrier();
        $stores = CPSStoreFunctions::getStores(1);
        if (!$id_carrier || $id_carrier !== $params['carrier']['id'] || empty($stores)) {
            return false;
        }

        $this->smarty->assign('paramsHook', array(
            'STORES' => $stores,
            'installed_opc' => $this->installed_opc,
            'CONFIGS'       => $this->config_vars,
            'store_selected' => empty($this->context->cookie->cps_id_store) ? false : $this->context->cookie->cps_id_store
        ));

        return $this->display(__FILE__, 'views/templates/hook/extra_carrier.tpl');
    }

    public function hookActionObjectOrderAddBefore($params)
    {
        $log_sql = fopen(_PS_MODULE_DIR_.'carrierpickupstore/log/log_'.date('Y-m-d').'.log', 'a');

        $data_d = '';
        $data_d .= "'isVisible': ".$this->core->isVisible()."\n";
        $data_d .= "'checkModulePTS': ".$this->core->checkModulePTS()."\n";
        $data_d .= "'isset cps_id_store': ".$params['cookie']->cps_id_store."\n";
        $data_d .= "'params': ".json_encode($params)."\n";
        $data_d .= "'cookie': ".json_encode(Context::getContext()->cookie)."\n";

        fwrite($log_sql, $data_d);
        fclose($log_sql);

        if (!$this->core->isVisible()
            || !$this->core->checkModulePTS()
            || !isset($this->context->cookie->cps_id_store)
        ) {
            return true;
        }

        $this->hookActionCreateAddressCPS(array(
            'order' => $params['object'],
            'id_store' => $this->context->cookie->cps_id_store,
            'is_set_invoice' => $this->context->cookie->cps_is_set_invoice
        ));

        unset($this->context->cookie->cps_id_store);
        unset($this->context->cookie->cps_is_set_invoice);
    }

    public function changeStore()
    {
        $this->context->cookie->__set('cps_id_store', Tools::getvalue('id_store', null));
        $this->context->cookie->__set('cps_is_set_invoice', Tools::getValue('is_set_invoice', false));

        return array('message_code' => $this->core->CODE_SUCCESS);
    }
    
    public function hookActionObjectStoreUpdateAfter($params)
    {
        $store   = $params['object'];
        $id_address = CPSCarrierPickupStore::getIdAddressByIdStore($store->id);

        if (!$id_address) {
            return false;
        }

        $id_lang = Configuration::get('PS_LANG_DEFAULT');
        $address = new Address($id_address);
        $required_fields = $this->getRequiredAddressFields($this->context->shop->id);

        foreach ($required_fields as $field) {
            if (property_exists($store, $field['name'])) {
                $name_field = $field['name'];
                $address->{$name_field} = !empty($store->{$name_field}) ? $store->{$name_field} : $field['default_value'];
            }
        }

        $address->alias = $store->name[$id_lang];
        $address->id_country = $store->id_country;
        $address->id_state = $store->id_state;

        $address1 = $store->address1;
        if (is_array($store->address1) && count($store->address1) > 0) {
            $address1 = $store->address1[$id_lang];
        }

        $address->address1 = $address1;
        $address->city = $store->city;

        if (!empty($store->address2)) {
            $address2 = $store->address2;
            if (is_array($store->address2) && count($store->address2) > 0) {
                $address2 = $store->address2[$id_lang];
            }

            $address->address2 = $address2;
        }

        if (!empty($store->postcode)) {
            $address->postcode = ltrim($store->postcode, ' ');
        }

        if (!$address->save()) {
            return false;
        }
    }

    public function hookActionObjectStoreDeleteAfter($params)
    {
        $store = $params['object'];
        $id_address = CPSCarrierPickupStore::getIdAddressByIdStore($store->id);

        if ((int) $id_address > 0) {
            $address = new Address($id_address);
            $address->deleted = 1;
            $address->update();
        }

        $stores = CPSStoreFunctions::getStores();
        if (empty($stores)) {
            $id_carrier = $this->getCarrier();
            if ((int)$id_carrier > 0) {
                $carrier = new Carrier($id_carrier);
                $carrier->need_range = 0;
                $carrier->update();
            }
        }
    }

    /**
     * Envía un correo electrónico de notificación a la tienda de recogida cuando hay un pedido válido nuevo o un pedido pasa a estado válido
     */
    private function sendEmailToStore($store, $order, $customer, $order_state)
    {
        if (Validate::isLoadedObject($store) && !empty($store->email)) {
            $products = $order->getProducts();
            $customized_datas = Product::getAllCustomizedDatas((int) $order->id_cart);
            $currency = new Currency($order->id_currency);
            if (is_array($products) && count($products) > 0) {
                foreach ($products as &$product) {
                    $product['unit_price'] = Tools::displayPrice($product['unit_price_tax_incl'], $currency, false);
                    $product['total_price'] = Tools::displayPrice($product['total_price_tax_incl'], $currency, false);

                    if (isset($customized_datas[$product['product_id']][$product['product_attribute_id']])) {
                        $product['customization'] = array();
                        foreach ($customized_datas[$product['product_id']][$product['product_attribute_id']][$order->id_address_delivery] as $customization) {
                            $customization_text = '';
                            if (isset($customization['datas'][Product::CUSTOMIZE_TEXTFIELD])) {
                                foreach ($customization['datas'][Product::CUSTOMIZE_TEXTFIELD] as $text) {
                                    $customization_text .= $text['name'].': '.$text['value'].'<br />';
                                }
                            }

                            if (isset($customization['datas'][Product::CUSTOMIZE_FILE])) {
                                $customization_text .= sprintf(Tools::displayError('%d image(s)'), count($customization['datas'][Product::CUSTOMIZE_FILE])).'<br />';
                            }

                            $customization_quantity = (int) $customization['quantity'];

                            $product['customization'][] = array(
                                'customization_text' => $customization_text,
                                'customization_quantity' => $customization_quantity,
                                'quantity' => Tools::displayPrice($customization_quantity * $product['unit_price_tax_incl'], $currency, false)
                            );
                        }
                    }
                }
            }

            $this->context->smarty->assign(array(
                'products' => $products
            ));

            $cart_products_detail = $this->context->smarty->fetch(_PS_MODULE_DIR_.$this->name.'/views/templates/front/cart_products_detail.tpl');

            $invoice_address = new Address($order->id_address_invoice);
            $format_address = AddressFormat::generateAddress(
                $invoice_address,
                array('avoid' => array()),
                '<br />',
                ' '
            );
            $message = Message::getMessageByCartId($order->id_cart);
            $carrier = new Carrier($order->id_carrier, $order->id_lang);

            if (Product::getTaxCalculationMethod($customer->id) == PS_TAX_EXC) {
                $total_products = $order->getTotalProductsWithoutTaxes();
            } else {
                $total_products = $order->getTotalProductsWithTaxes();
            }

            $email_sent = $this->core->sendEmail(
                $store->email,
                Mail::l('New order', (int)$order->id_lang),
                array(
                    '{customer_name}' => $customer->firstname.' '.$customer->lastname,
                    '{customer_email}' => $customer->email,
                    '{invoice_address}' => $format_address,
                    '{order_reference}' => $order->reference,
                    '{order_date}' => $order->date_add,
                    '{order_payment}' => $order->payment,
                    '{order_status}' => $order_state->name,
                    '{order_message}' => $message['message'],
                    '{order_carrier}' => $carrier->name,
                    '{cart_products_detail}' => $cart_products_detail,
                    '{total_paid}' => Tools::displayPrice($order->total_paid, $currency),
                    '{total_products}' => Tools::displayPrice($total_products, $currency),
                    '{total_discounts}' => Tools::displayPrice($order->total_discounts, $currency),
                    '{total_shipping}' => Tools::displayPrice($order->total_shipping, $currency),
                    '{total_tax_paid}' => Tools::displayPrice(
                        ($order->total_products_wt - $order->total_products) + ($order->total_shipping_tax_incl - $order->total_shipping_tax_excl),
                        $currency,
                        false
                    ),
                    '{total_wrapping}' => Tools::displayPrice($order->total_wrapping, $currency),
                ),
                'new_order',
                null,
                null,
                $this->context->language->id
            );

            if ($email_sent) {
                Db::getInstance()->insert('cps_emails_sent', array('id_order' => $order->id));
            }

            return $email_sent;
        }
    }

    public function hookActionOrderStatusUpdate($params)
    {
        $order_state = $params['newOrderStatus'];
        $id_carrier = $this->getCarrier();
        $order = new Order($params['id_order']);
        if (!$this->config_vars['CPS_SEND_MAIL_TO_STORE']
            || !$order_state->paid
            || (int) $id_carrier !== (int) $order->id_carrier
        ) {
            return false;
        }

        $carrierpickupstore = CPSCarrierPickupStore::getByIdCart($order->id_cart);
        if (Validate::isLoadedObject($carrierpickupstore)) {
            $sql = new Dbquery();
            $sql->select('id');
            $sql->from('cps_emails_sent');
            $sql->where('id_order = '.(int) $order->id);
            $email_sent = Db::getInstance()->getValue($sql);

            if (!$email_sent) {
                $customer = new Customer($order->id_customer);
                $store = new Store($carrierpickupstore->id_store);
                $this->sendEmailToStore($store, $order, $customer, $order_state);
            }
        }
    }

    public function hookActionValidateOrder($params)
    {
        $order = $params['order'];
        $id_carrier = $this->getCarrier();
        if (!$this->config_vars['CPS_SEND_MAIL_TO_STORE']
            || !$order->valid
            || (int) $id_carrier !== (int) $order->id_carrier
        ) {
            return false;
        }

        $carrierpickupstore = CPSCarrierPickupStore::getByIdCart($order->id_cart);
        if (Validate::isLoadedObject($carrierpickupstore)) {
            $customer = $params['customer'];
            $order_state = $params['orderStatus'];

            $store = new Store($carrierpickupstore->id_store);
            $this->sendEmailToStore($store, $order, $customer, $order_state);
        }
    }

    public function getRequiredAddressFields($id_shop)
    {
        if ($this->core->isModuleActive('onepagecheckoutps')) {
            $query = new DbQuery();
            $query->select('opc.name, opcs.default_value');
            $query->from('opc_field', 'opc');
            $query->innerJoin('opc_field_shop', 'opcs', 'opc.id_field = opcs.id_field');
            $query->where('opc.is_custom = 0 AND opcs.required = 1 AND opc.object = "delivery"');
            $query->where('opcs.id_shop = '.(int) $id_shop);

            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        } else {
            $fields = Address::$definition['fields'];
            $result = array();
            $i = 0;

            $default_country = (int) Configuration::get('PS_COUNTRY_DEFAULT');

            foreach ($fields as $field => $data) {
                if (!empty($data['required']) || $field === 'dni') {
                    if ($field === 'id_state') {
                        $result[$i]['default_value'] = 0;

                        if (Country::containsStates($default_country)) {
                            $states = State::getStatesByIdCountry($default_country);
                            if (count($states)) {
                                $result[$i]['default_value'] =  $states[0]['id_state'];
                            }
                        }
                    } elseif ($field === 'id_country') {
                        $result[$i]['default_value'] = $default_country;
                    } elseif ($field === 'postcode') {
                        $country = new Country($default_country);
                        $result[$i]['default_value'] = str_replace(
                            'C',
                            $country->iso_code,
                            str_replace('N', '0', str_replace('L', 'A', $country->zip_code_format))
                        );
                    } elseif (in_array($field, array(
                        'alias',
                        'company',
                        'lastname',
                        'firstname',
                        'address1',
                        'address2',
                        'city',
                        'other'
                    ))) {
                        $result[$i]['default_value'] = '.';
                    } else {
                        $result[$i]['default_value'] = 0;
                    }

                    $result[$i]['name'] = $field;
                    $i++;
                }
            }
        }

        return $result;
    }

    public function createAddress()
    {
        $is_set_invoice = Tools::getValue('is_set_invoice');
        $id_store = Tools::getValue('id_store');

        if ($this->hookActionCreateAddressCPS(array(
            'id_store' => $id_store,
            'is_set_invoice' => $is_set_invoice
        ))) {
            return array(
                'message_code' => $this->core->CODE_SUCCESS,
                'message' => $this->l('The address was saved correctly')
            );
        }
        return array(
            'message_code' => $this->core->CODE_ERROR,
            'message' => $this->l('There was an error saving the address')
        );
    }

    public function getAddressCPS($id_store)
    {
        $id_lang    = Configuration::get('PS_LANG_DEFAULT');
        $store      = $this->getStoreById($id_store, $id_lang);
        
        if (empty($store)) {
            return false;
        }

        $cps_class = CPSCarrierPickupStore::getByIdCart($this->context->cart->id);
        if (!$cps_class) {
            $cps_class = new CPSCarrierPickupStore();
        }

        if (Validate::isLoadedObject($cps_class)) {
            $address = new Address($cps_class->id_address);
        } else {
            $opc = $this->core->isModuleActive('onepagecheckoutps');
            if ($opc && !$this->context->customer->isLogged()) {
                $id_address = $opc->getIdAddressAvailable();
                $address = new Address($id_address);
            } else {
                $address = new Address();
            }
        }


        $required_fields = $this->getRequiredAddressFields($this->context->shop->id);
        foreach ($required_fields as $field) {
            $name_field = $field['name'];
            if (!empty($address->{$name_field}) && $address->{$name_field} !== '.') {
                continue;
            }

            if (in_array($name_field, array('firstname', 'lastname', 'company'))) {
                if (!empty($this->context->customer->{$name_field})) {
                    $address->{$name_field} = $this->context->customer->{$name_field};
                } else {
                    $address->{$name_field} = $address->{$name_field};
                }
            } else {
                $address->{$name_field} = !empty($store[$name_field]) ? $store[$name_field] : $field['default_value'];
            }
        }

        if (empty($address->dni) && Country::isNeedDniByCountryId($store['id_country'])) {
            $address->dni = 0;
        }

        $address->alias = Tools::substr($store['name'], 0, 32);
        $address->deleted = 0;
        $address->id_customer = Configuration::get('CPS_ID_CUSTOMER');
        $address->id_country = $store['id_country'];
        $address->id_state = $store['id_state'];
        $address->address1 = $store['address1'];
        $address->city = $store['city'];

        if (!empty($store['address2'])) {
            $address->address2 = $store['address2'];
        }
        if (!empty($store['postcode'])) {
            $address->postcode = ltrim($store['postcode'], ' ');
        }

        if (!$address->save()) {
            return false;
        }

        $cps_class->id_store = $id_store;
        $cps_class->id_cart = $this->context->cart->id;
        $cps_class->id_address = $address->id;
        $cps_class->save();

        return $address;
    }

    public function hookActionCreateAddressCPS($params)
    {
        $id_cps_carrier = $this->getCarrier();
        $id_carrier = (!empty($params['order'])) ? $params['order']->id_carrier : $this->context->cart->id_carrier;
        
        if ((int) $id_cps_carrier !== (int) $id_carrier) {
            return false;
        }

        if ((int) $params['id_store'] === 0) {
            $params['id_store'] = CPSStoreFunctions::getIDFirstActiveStore();
        }

        $address = $this->getAddressCPS($params['id_store']);
        $this->context->cart->id_address_delivery = $address->id;
        if (!$params['is_set_invoice']) {
            $this->context->cart->id_address_invoice = $address->id;
        }
        
        $this->context->cart->update();
        $this->context->cart->setNoMultishipping();

        $delivery_option = array($address->id => $id_cps_carrier.',');
        $this->context->cart->setDeliveryOption($delivery_option);
        $this->context->cart->update();

        if (method_exists('Cart', 'resetStaticCache')) {
            Cart::resetStaticCache();
        }
        Cache::clean('*');

        if (!empty($params['order'])) {
            $params['order']->id_address_delivery = $address->id;
            if (!$params['is_set_invoice']) {
                $params['order']->id_address_invoice = $address->id;
            }
        }
        return true;
    }

    private function createCarrier()
    {
        $carrier = new Carrier();
        $carrier->name = $this->l('Pick Store');
        $languages = Language::getLanguages(false);
        $carrier->delay = array();
        foreach ($languages as $lang) {
            $carrier->delay[$lang['id_lang']] = utf8_encode($this->l('Pick Store'));
        }

        $carrier->is_free = true;
        $carrier->need_range = true;
        $carrier->is_module = true;
        $carrier->external_module_name = $this->name;

        if ($carrier->save()) {
            $query_groups = new DbQuery();
            $query_groups->select('id_group');
            $query_groups->from('group');

            $groups = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query_groups);

            $sql = 'INSERT INTO '._DB_PREFIX_.'carrier_group (id_carrier, id_group) VALUES ';
            foreach ($groups as $group) {
                $sql .= '('.(int)$carrier->id.', '.(int) $group['id_group'].'),';
            }

            Db::getInstance()->execute(rtrim($sql, ','));

            $zones = Zone::getZones(true);
            if (is_array($zones) && count($zones) > 0) {
                foreach ($zones as $zone) {
                    $carrier->addZone($zone['id_zone']);
                }
            }

            return $carrier->id;
        }

        return false;
    }

    public function getCarrier()
    {
        $sql = new DbQuery();
        $sql->select('id_carrier');
        $sql->from('carrier');
        $sql->where('external_module_name = "'.pSQL($this->name).'"');
        $sql->where('deleted = 0');

        $id_carrier = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        if ($id_carrier !== false) {
            return $id_carrier;
        }

        return $this->createCarrier();
    }

    public function deleteIcon()
    {
        $icon = Tools::getValue('icon');
        $img_url = _PS_MODULE_DIR_.$this->name.'/views/img/icon/'.$icon.'_'.$this->context->shop->id.'.png';
        if (!file_exists($img_url)) {
            return array(
                'message_code' => $this->core->CODE_ERROR,
                'message' => $this->l('The file was not found, it was probably already deleted. Reload the page')
            );
        }

        if (unlink($img_url)) {
            return array(
                'message_code' => $this->core->CODE_SUCCESS,
                'message' => $this->l('The file was deleted successfully')
            );
        }

        return array(
            'message_code' => $this->core->CODE_ERROR,
            'message' => $this->l('The file could not be deleted')
        );
    }

    private function getMarkerIcons($path = true)
    {
        $id_shop = $this->context->shop->id;
        $icons = array('shop_selected_icon' => '', 'shop_unselected_icon' => '');
        if ($this->config_vars['CPS_CUSTOM_SHOP_ICONS'] || !empty($this->context->cookie->id_employee)) {
            foreach (array_keys($icons) as $icon) {
                $img_name = $icon.'_'.$id_shop.'.png';
                if (file_exists(_PS_MODULE_DIR_.$this->name.'/views/img/icon/'.$img_name)) {
                    if ($path) {
                        $icons[$icon] = _MODULE_DIR_.$this->name.'/views/img/icon/'.$img_name;
                    } else {
                        $icons[$icon] = $img_name;
                    }
                } elseif (file_exists(_PS_MODULE_DIR_.$this->name.'/views/img/icon/shop.png')) {
                    $icons[$icon] = 'shop.png';
                }
            }
        }

        return $icons;
    }

    public function showModalMap()
    {
        $stores = CPSStoreFunctions::getStores(1);
        $store_selected = CPSCarrierPickupStore::getByIdCart($this->context->cart->id);

        $paramsHook = array(
            'STORES'            => $stores,
            'STORE_SELECTED'    => $store_selected,
        );

        $this->smarty->assign('paramsHook', $paramsHook);

        return $this->display(__FILE__, 'views/templates/front/map_stores.tpl');
    }

    public function getStoreById($id_store = null, $id_lang = null)
    {
        if (is_null($id_lang)) {
            $id_lang = Context::getContext()->language->id;
        }

        $sql = new DbQuery();

        if (version_compare(_PS_VERSION_, '1.7.3.0') == -1) {
            $sql->select('s.*');
        } else {
            $sql->select('s.*, sl.name, sl.address1, sl.address2, sl.hours, sl.note');
            $sql->innerJoin('store_lang', 'sl', 'sl.id_store = s.id_store AND sl.id_lang = '.(int) $id_lang);
        }

        $sql->from('store', 's');
        $sql->where('s.id_store = '.(int) $id_store);

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
    }

    public function createCustomer()
    {
        $query = new DbQuery();
        $query->select('id_customer');
        $query->from('customer');
        $query->where('email = "'.pSQL('noreply-cps@'.$this->context->shop->domain).'"');

        if ((int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query) > 0) {
            return true;
        }

        $date = date('Y-m-d H:i:s');
        $values = array(
            'firstname' => 'CPS PTS Not Delete',
            'lastname' => 'CPS PTS Not Delete',
            'email' => pSQL('noreply-cps@'.$this->context->shop->domain),
            'passwd' => Tools::encrypt('CPS123456'),
            'id_shop' => (int) Context::getContext()->shop->id,
            'id_shop_group' => (int) Context::getContext()->shop->id_shop_group,
            'id_default_group' => (int) Configuration::get('PS_CUSTOMER_GROUP'),
            'id_lang' => (int) Context::getContext()->language->id,
            'birthday' => '0000-00-00',
            'secure_key' => md5(uniqid(rand(), true)),
            'date_add' => $date,
            'date_upd' => $date,
            'active' => 0,
            'deleted' => 1
        );

        Db::getInstance(_PS_USE_SQL_SLAVE_)->insert('customer', $values);

        $id_customer = (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->Insert_ID();
        if (empty($id_customer)) {
            return false;
        }

        Db::getInstance(_PS_USE_SQL_SLAVE_)->insert('customer_group', array(
            'id_customer' => (int) $id_customer,
            'id_group' => (int) Configuration::get('PS_CUSTOMER_GROUP')
        ));

        Configuration::updateValue('CPS_ID_CUSTOMER', (int)$id_customer);

        return $id_customer;
    }
}
