<?php
/**
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * We are experts and professionals in PrestaShop
 *
 * @author    PresTeamShop.com <support@presteamshop.com>
 * @copyright 2011-2021 PresTeamShop
 * @license   see file: LICENSE.txt
 * @category  PrestaShop
 * @category  Module
 */

class CPSCarrierPickupStore extends ObjectModel
{
    public static $definition =  array(
        'table' => 'cps_carrierpickupstore',
        'primary' => 'id_carrierpickupstore',
        'multilang' => false,
        'multishop' => false,
        'multilang_shop' => false,
        'fields' => array(
            'id_cart' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'id_store' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'id_address' => array('type' => self::TYPE_INT, 'validate' => 'isInt')
        )
    );
    
    public $id;
    public $id_cart;
    public $id_store;
    public $id_address;

    public function getFields()
    {
        $this->validateFields();
        $fields = $this->formatFields(self::FORMAT_COMMON);

        if (!$fields && isset($this->id) && Validate::isUnsignedId($this->id)) {
            $fields[$this->def['primary']] = $this->id;
        }

        return $fields;
    }

    public static function getByIdCart($id_cart)
    {
        $query  = new DbQuery();
        $query->select('id_carrierpickupstore');
        $query->from('cps_carrierpickupstore');
        $query->where('id_cart = '.(int) $id_cart);

        $id_carrierpickupstore = Db::getInstance()->getValue($query);

        if ($id_carrierpickupstore !== false) {
            return new CPSCarrierPickupStore($id_carrierpickupstore);
        }

        return false;
    }

    public static function getIdAddressByIdStore($id_store)
    {
        $sql = new DbQuery();
        $sql->select('id_address');
        $sql->from(self::$definition['table']);
        $sql->where('id_store = '.(int) $id_store);

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
    }
}
