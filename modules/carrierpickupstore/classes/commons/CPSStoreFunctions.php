<?php
/**
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * We are experts and professionals in PrestaShop
 *
 * @author    PresTeamShop.com <support@presteamshop.com>
 * @copyright 2011-2021 PresTeamShop
 * @license   see file: LICENSE.txt
 * @category  PrestaShop
 * @category  Module
 */

class CPSStoreFunctions
{
    public static function getIDFirstActiveStore()
    {
        $id_shop = Context::getContext()->shop->id;

        $sql = new DbQuery();
        $sql->select('s.id_store as `id`');
        $sql->from('store', 's');
        $sql->innerJoin('store_shop', 'ss', '(ss.id_store = s.id_store AND ss.id_shop = '.(int) $id_shop.')');
        $sql->innerJoin('country', 'c', 'c.id_country = s.id_country');
        $sql->leftJoin('cps_store', 'cps', '(cps.id_store = s.id_store)');
        $sql->where('s.active = 1  AND c.active = 1');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
    }

    /**
     * Retorno todos las tiendas de contacto del prestashop
     *
     * @param int $visible = 1 - trae solo las visibles
     * @param int $visible = 0 - trae solo los no visibles
     * @param int $visible = -1 - trae los visibles y no visible (default)
     * @return type
     */
    public static function getStores($visible = -1)
    {
        $context = Context::getContext();
        $id_lang = $context->language->id;

        $sql = new DbQuery();
        $sql->select('s.id_store as `id`');
        $sql->select('s.*');
        $sql->select('sl.*');
        $sql->select('c.id_country');
        $sql->select('IF(cps.id_store IS NULL, \'0\', \'1\') as visible');
        $sql->from('country', 'c');
        $sql->from('store', 's');
        $sql->innerJoin(
            'store_shop',
            'store_shop',
            '(store_shop.id_store = s.id_store AND store_shop.id_shop = '.(int) Context::getContext()->shop->id.')'
        );
        $sql->leftJoin('store_lang', 'sl', '(sl.id_store = s.id_store AND sl.id_lang = '.(int) $id_lang.')');
        if ($visible == -1) {
            $sql->leftJoin('cps_store', 'cps', '(cps.id_store = s.id_store)');
        } elseif ($visible == 0) {
            $sql->rightJoin('cps_store', 'cps', '(cps.id_store = s.id_store)');
        } else {
            $sql->leftJoin('cps_store', 'cps', '(cps.id_store = s.id_store)');
            $sql->where('cps.id_store IS NOT NULL');
        }
        $sql->where('c.id_country = s.id_country');

        if (!Validate::isLoadedObject($context->employee)) {
            $sql->where('c.active = 1');
        }

        $stores = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        $carrierpickupstore = Module::getInstanceByName('carrierpickupstore');

        if (is_array($stores) && count($stores) > 0) {
            foreach ($stores as &$store) {
                $store['country_name'] = Country::getNameById($id_lang, $store['id_country']);
                $store['state_name'] = State::getNameById($store['id_state']);

                $hours = Tools::jsonDecode($store['hours']);
                $store['hours']   = array(
                    array('day' => $carrierpickupstore->l('Monday:'), 'hour' => $hours[0][0]),
                    array('day' => $carrierpickupstore->l('Tuesday:'), 'hour' => $hours[1][0]),
                    array('day' => $carrierpickupstore->l('Wednesday:'), 'hour' => $hours[2][0]),
                    array('day' => $carrierpickupstore->l('Thursday:'), 'hour' => $hours[3][0]),
                    array('day' => $carrierpickupstore->l('Friday:'), 'hour' => $hours[4][0]),
                    array('day' => $carrierpickupstore->l('Saturday:'), 'hour' => $hours[5][0]),
                    array('day' => $carrierpickupstore->l('Sunday:'), 'hour' => $hours[6][0]),
                );
            }
            return $stores;
        } else {
            return array();
        }
    }

    public static function checkVisibility($id_store)
    {
        $query = new DbQuery();
        $query->select('id_store');
        $query->from('cps_store');
        $query->where('id_store = '.(int) $id_store);

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
    }

    public static function toggleVisibility($id_store, $visibility)
    {
        if ($visibility) {
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->delete('cps_store', 'id_store = '.(int) $id_store);
        } else {
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->insert('cps_store', array('id_store' => $id_store));
        }

        return $result;
    }

    public static function toggleActive($id_store)
    {
        $store = new Store($id_store);
        $status = (!$store->active);
        $store->active = $status;
        if ($store->update()) {
            return true;
        }
        return false;
    }
}
