<?php
/**
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * We are experts and professionals in PrestaShop
 *
 * @author    PresTeamShop.com <support@presteamshop.com>
 * @copyright 2011-2021 PresTeamShop
 * @license   see file: LICENSE.txt
 * @category  PrestaShop
 * @category  Module
 */

function upgrade_module_4_0_0($object)
{
    Configuration::updateValue('CPS_IP_DEBUG', '');
    Configuration::updateValue('CPS_SHOW_GOOGLE_MAPS', 0);
    Configuration::updateValue('CPS_COORDINATES', '40.4165000, -3.7025600');
    Configuration::updateValue('CPS_ZOOM', 5);
    Configuration::updateValue('CPS_SEND_MAIL_TO_STORE', '0');
    
    $object->registerHook('actionCreateAddressCPS');
    $object->registerHook('actionObjectStoreUpdateAfter');
    $object->registerHook('actionObjectStoreDeleteAfter');
    $object->registerHook('actionObjectOrderAddBefore');

    $object->registerHook('actionValidateOrder');
    $object->registerHook('actionOrderStatusUpdate');

    Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'cps_carrierpickupstore ADD id_address INT NOT NULL');

    $sql = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'cps_store (`id_store` INT NOT NULL PRIMARY KEY
    ) CHARSET=utf8;';

    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);

    Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'cps_emails_sent` (
            `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `id_order` INT NOT NULL
        ) CHARSET=utf8;
    ');

    return true;
}
