<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class ProductReviewGrade extends ObjectModel
{
    public $id;

    public $id_product_review;
    
    public $id_product_review_criterion;
    
    public $grade;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'tdktoolkit_product_review_grade',
        'primary' => 'id_product_review_grade',
        'fields' => array(
            'id_product_review' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_product_review_criterion' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'grade' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
        )
    );
}
