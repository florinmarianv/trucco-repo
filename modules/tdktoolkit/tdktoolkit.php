<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

require_once(_PS_MODULE_DIR_ . 'tdktoolkit/classes/ProductReviewCriterion.php');
require_once(_PS_MODULE_DIR_ . 'tdktoolkit/classes/ProductReview.php');
require_once(_PS_MODULE_DIR_ . 'tdktoolkit/classes/CompareProduct.php');
require_once(_PS_MODULE_DIR_ . 'tdktoolkit/classes/WishList.php');

//TDK:: use library for get dropdown cart
use PrestaShop\PrestaShop\Adapter\Cart\CartPresenter;

class Tdktoolkit extends Module
{

    protected $config_form = false;
    private $link;
    public $link_cart;
    public $module_path;
    protected $_postErrors = array();
    public $is_gen_rtl;

    public function __construct()
    {
        $this->name = 'tdktoolkit';
        $this->tab = 'front_office_features';
        $this->version = '1.4.0';
        $this->author = 'TDK Studio';
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->need_instance = 0;
        $this->controllers = array('productscompare', 'mywishlist', 'viewwishlist');
        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;
        parent::__construct();

        $this->secure_key = Tools::encrypt($this->name);
        $this->displayName = $this->l('TDK Toolkit');
        $this->description = $this->l('TDK Toolkit for prestashop 1.7: ajax cart, dropdown cart, fly cart, review, compare, wishlist at product list');
        $this->link = $this->context->link;

        $this->module_path = $this->local_path;
        if (file_exists(_PS_THEME_DIR_ . 'modules/' . $this->name . '/views/css/front_rtl.css') || file_exists(_PS_THEME_DIR_ . '/assets/modules/' . $this->name . '/views/css/front_rtl.css')) {
            $this->is_gen_rtl = true;
        } else {
            $this->is_gen_rtl = false;
        }
		// echo '<pre>';
		// print_r(Configuration::get('TDKTOOLKIT_FREESHIPPING_TEXT', 2));die();
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (parent::install() && $this->registerTDKHook()) {
            $this->createConfiguration();
            $res = true;
            /* Creates tables */
            $res &= $this->createTables();
            Configuration::updateValue('TDK_INSTALLED_TDKTOOLKIT', '1');
            $id_parent = Tab::getIdFromClassName('IMPROVE');
            $class = 'Admin' . Tools::ucfirst($this->name) . 'Management';
            $tab1 = new Tab();
            $tab1->class_name = $class;
            $tab1->module = $this->name;
            $tab1->id_parent = $id_parent;
            $langs = Language::getLanguages(false);
            foreach ($langs as $l) {
                # validate module
                $tab1->name[$l['id_lang']] = $this->l('TDK Toolkit Management');
            }
            $tab1->add(true, false);
            # insert icon for tab
            Db::getInstance()->execute(' UPDATE `' . _DB_PREFIX_ . 'tab` SET `icon` = "star" WHERE `id_tab` = "' . (int) $tab1->id . '"');
            $this->installModuleTab('TDK Toolkit Configuration', 'module', 'AdminTdktoolkitManagement');
            $this->installModuleTab('Product Review Management', 'reviews', 'AdminTdktoolkitManagement');

            return (bool) $res;
        }
        return false;
    }

    public function uninstall()
    {
        if (parent::uninstall() && $this->unregisterTDKHook()) {
            $res = true;
            $this->uninstallModuleTab('management');
            $this->uninstallModuleTab('reviews');
            $this->uninstallModuleTab('module');
            $res &= $this->deleteTables();
            $this->deleteConfiguration();
            return (bool) $res;
        }
        return false;
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $output = '';
        //TDK:: correct module
        if (Tools::getValue('correctmodule')) {
            $this->correctModule();
        }

        if (Tools::getValue('success')) {
            switch (Tools::getValue('success')) {
                case 'correct':
                    $output .= $this->displayConfirmation($this->l('Correct Module is successful'));
                    break;
            }
        }

        /**
         * If values have been submitted in the form, process.
         */
        if (((bool) Tools::isSubmit('submitTdktoolkitConfig')) == true) {
            $this->postValidation();
            if (!count($this->_postErrors)) {
				$this->postProcess();
            } else {
                foreach ($this->_postErrors as $err) {
                    $output .= $this->displayError($err);
                }
            }
        }

        $output .= $this->renderGroupConfig();
        return $output;
    }

    public function renderGroupConfig()
    {
        $type_dropdown = array(
            array(
                'id_type' => 'dropdown',
                'name_type' => $this->l('Dropdown'),
            ),
            array(
                'id_type' => 'dropup',
                'name_type' => $this->l('Dropup'),
            ),
            array(
                'id_type' => 'slidebar_left',
                'name_type' => $this->l('Slidebar Left'),
            ),
            array(
                'id_type' => 'slidebar_right',
                'name_type' => $this->l('Slidebar Right'),
            ),
            array(
                'id_type' => 'slidebar_top',
                'name_type' => $this->l('Slidebar Top'),
            ),
            array(
                'id_type' => 'slidebar_bottom',
                'name_type' => $this->l('Slidebar Bottom'),
            ),
        );
        $type_displayattribute = array(
            array(
                'id_type' => 1,
                'name_type' => $this->l('Combination'),
            ),
            array(
                'id_type' => 2,
                'name_type' => $this->l('Dropdown List'),
            ),
            array(
                'id_type' => 3,
                'name_type' => $this->l('Radio Button Without Product Attribute Image (Display All Attributes)'),
            ),
            array(
                'id_type' => 4,
                'name_type' => $this->l('Radio Button Without Product Attribute Image (Only Display Available Attributes'),
            ),
            array(
                'id_type' => 5,
                'name_type' => $this->l('Radio Button With Product Attribute Image (Display All Attributes)'),
            ),
            array(
                'id_type' => 6,
                'name_type' => $this->l('Radio Button With Product Attribute Image (Only Display Available Attributes)'),
            )
        );
        $type_position = array(
            array(
                'id_type' => 'fixed',
                'name_type' => $this->l('Fixed'),
            ),
            array(
                'id_type' => 'absolute',
                'name_type' => $this->l('Absolute'),
            ),
        );
        $type_unit = array(
            array(
                'id_type' => 'percent',
                'name_type' => $this->l('Percent'),
            ),
            array(
                'id_type' => 'pixel',
                'name_type' => $this->l('Pixel'),
            ),
        );
        $type_vertical = array(
            array(
                'id_type' => 'top',
                'name_type' => $this->l('Top'),
            ),
            array(
                'id_type' => 'bottom',
                'name_type' => $this->l('Bottom'),
            ),
        );
        $type_horizontal = array(
            array(
                'id_type' => 'left',
                'name_type' => $this->l('Left'),
            ),
            array(
                'id_type' => 'right',
                'name_type' => $this->l('Right'),
            ),
        );
        $type_effect = array(
            array(
                'id_type' => 'none',
                'name_type' => $this->l('None'),
            ),
            array(
                'id_type' => 'fade',
                'name_type' => $this->l('Fade'),
            ),
            array(
                'id_type' => 'shake',
                'name_type' => $this->l('Shake'),
            ),
        );
        $fields_form = array();
        $fields_form[0]['form'] = array(
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'TDKTOOLKIT_DEFAULT_TAB',
                    'default' => '',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Button Cart At Product List'),
                    'name' => 'TDKTOOLKIT_ENABLE_AJAXCART',
                    'is_bool' => true,
                    // 'desc' => $this->l('Show Button Cart At Product List'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_AJAXCART_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_AJAXCART_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Select Attribute'),
                    'name' => 'TDKTOOLKIT_ENABLE_SELECTATTRIBUTE',
                    'is_bool' => true,
                    // 'desc' => $this->l('Show Select Attribute'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_SELECTATTRIBUTE_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_SELECTATTRIBUTE_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Display Attribute Type'),
                    'name' => 'TDKTOOLKIT_DISPLAYATTRIBUTE_TYPE',
                    'options' => array(
                        'query' => $type_displayattribute,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Input Quantity'),
                    'name' => 'TDKTOOLKIT_ENABLE_INPUTQUANTITY',
                    'is_bool' => true,
                    // 'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_INPUTQUANTITY_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_INPUTQUANTITY_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable Flycart Effect'),
                    'name' => 'TDKTOOLKIT_ENABLE_FLYCART_EFFECT',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_FLYCART_EFFECT_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_FLYCART_EFFECT_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable Notification'),
                    'name' => 'TDKTOOLKIT_ENABLE_NOTIFICATION',
                    'desc' => $this->l('Show notification when add cart successful'),
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_NOTIFICATION_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_NOTIFICATION_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Popup After Add Cart'),
                    'name' => 'TDKTOOLKIT_SHOW_POPUP',
                    'desc' => $this->l('Default is ON. You can turn OFF and turn ON "Notification" instead'),
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_POPUP_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_POPUP_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
				array(
                    'type' => 'switch',
                    'label' => $this->l('Show Sidebar Cart After Add Cart'),
                    'name' => 'TDKTOOLKIT_SHOW_SIDERBARCART',
                    'desc' => $this->l('Only work with Sidebar Type for Default Cart and Fly Cart'),
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_SIDERBARCART_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_SIDERBARCART_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
				array(
                    'type' => 'text',
                    'label' => $this->l('Free Shipping Text'),
                    'name' => 'TDKTOOLKIT_FREESHIPPING_TEXT',
                    'lang' => true,
                    'desc' => $this->l('Leave empty to hide free shipping text')
                ),
				array(
                    'type' => 'text',
                    'label' => $this->l('Link Button "SHOP NOW"'),
                    'name' => 'TDKTOOLKIT_CTA_LINK',
                    'lang' => true,
                    'desc' => $this->l('Leave empty to hide "SHOP NOW" button. Default: index (home page)')
                ),
                array(
                    'type' => 'html',
                    'name' => 'html_data',
                    'html_content' => '<hr>',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Dropdown Cart (Default Cart)'),
                    'name' => 'TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Type Dropdown Cart (Default Cart)'),
                    'name' => 'TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART',
                    'options' => array(
                        'query' => $type_dropdown,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'html',
                    'name' => 'html_data',
                    'html_content' => '<hr>',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Dropdown Cart (Fly Cart)'),
                    'name' => 'TDKTOOLKIT_ENABLE_DROPDOWN_FLYCART',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_DROPDOWN_FLYCART_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_DROPDOWN_FLYCART_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Type Dropdown Cart (Fly Cart)'),
                    'name' => 'TDKTOOLKIT_TYPE_DROPDOWN_FLYCART',
                    'options' => array(
                        'query' => $type_dropdown,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Type Effect (Fly Cart)'),
                    'name' => 'TDKTOOLKIT_TYPE_EFFECT_FLYCART',
                    'desc' => $this->l('Only when "Flycart Effect" ON'),
                    'options' => array(
                        'query' => $type_effect,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Type Position (Fly Cart)'),
                    'name' => 'TDKTOOLKIT_TYPE_POSITION_FLYCART',
                    'options' => array(
                        'query' => $type_position,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Position By Vertical (Fly Cart)'),
                    'hint' => $this->l('Select type of distance between "Fly cart" and the top edge or the bottom edge of window (browser)'),
                    'name' => 'TDKTOOLKIT_POSITION_VERTICAL_FLYCART',
                    'options' => array(
                        'query' => $type_vertical,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Position By Vertical Value (Fly Cart)'),
                    'name' => 'TDKTOOLKIT_POSITION_VERTICAL_VALUE_FLYCART',
                    'lang' => false,
                    'desc' => $this->l('Must has value from 0 to 100 with the unit type below is percent')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Position By Vertical Unit (Fly Cart)'),
                    'name' => 'TDKTOOLKIT_POSITION_VERTICAL_UNIT_FLYCART',
                    'options' => array(
                        'query' => $type_unit,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Position By Horizontal (Fly Cart)'),
                    'hint' => $this->l('Select type of distance between "Fly cart" and the left edge or the right edge of window (browser)'),
                    'name' => 'TDKTOOLKIT_POSITION_HORIZONTAL_FLYCART',
                    'options' => array(
                        'query' => $type_horizontal,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Position By Horizontal Value (Fly Cart)'),
                    'name' => 'TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_FLYCART',
                    'lang' => false,
                    'desc' => $this->l('Must has value from 0 to 100 with the unit type below is percent')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Position By Horizontal Unit (Fly Cart)'),
                    'name' => 'TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_FLYCART',
                    'options' => array(
                        'query' => $type_unit,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'html',
                    'name' => 'html_data',
                    'html_content' => '<hr>',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable Overlay Background (All Cart)'),
                    'name' => 'TDKTOOLKIT_ENABLE_OVERLAYBACKGROUND_ALLCART',
                    'desc' => $this->l('Only for "Slidebar" type. Turn OFF to allow "Add to cart" for Slidebar'),
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_OVERLAYBACKGROUND_ALLCART_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_OVERLAYBACKGROUND_ALLCART_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable Update Quantity (All Cart)'),
                    'name' => 'TDKTOOLKIT_ENABLE_UPDATE_QUANTITY_ALLCART',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_UPDATE_QUANTITY_ALLCART_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_UPDATE_QUANTITY_ALLCART_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable Button Up/Down (All Cart)'),
                    'name' => 'TDKTOOLKIT_ENABLE_BUTTON_QUANTITY_ALLCART',
                    'desc' => $this->l('Only when turn ON update quantity'),
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_BUTTON_QUANTITY_ALLCART_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_BUTTON_QUANTITY_ALLCART_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Product Combination (All Cart)'),
                    'name' => 'TDKTOOLKIT_SHOW_COMBINATION_ALLCART',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_COMBINATION_ALLCART_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_COMBINATION_ALLCART_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Product Customization (All Cart)'),
                    'name' => 'TDKTOOLKIT_SHOW_CUSTOMIZATION_ALLCART',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_CUSTOMIZATION_ALLCART_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_CUSTOMIZATION_ALLCART_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Width Of Cart Item (All Cart)'),
                    'name' => 'TDKTOOLKIT_WIDTH_CARTITEM_ALLCART',
                    'suffix' => $this->l('Pixel'),
                    'lang' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Height Of Cart Item (All Cart)'),
                    'name' => 'TDKTOOLKIT_HEIGHT_CARTITEM_ALLCART',
                    'suffix' => $this->l('Pixel'),
                    'lang' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Number Cart Item To Display (All Cart)'),
                    'name' => 'TDKTOOLKIT_NUMBER_CARTITEM_DISPLAY_ALLCART',
                    'desc' => $this->l('Ony for "Dropup/Dropdown" type. When the total of cart item is greater this config, the scrollbar will be displayed'),
                    'lang' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Width (Notification)'),
                    'name' => 'TDKTOOLKIT_WIDTH_NOTIFICATION',
                    // 'suffix' => $this->l('Pixel'),
                    'lang' => false,
                    'desc' => $this->l('Must has value from 0 to 100 with the unit width below is percent')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Width Unit (Notification)'),
                    'name' => 'TDKTOOLKIT_WIDTH_UNIT_NOTIFICATION',
                    'options' => array(
                        'query' => $type_unit,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Position By Vertical (Notification)'),
                    'hint' => $this->l('Select type of distance between "Notification" and the top edge or the bottom edge of window (browser)'),
                    'name' => 'TDKTOOLKIT_POSITION_VERTICAL_NOTIFICATION',
                    'options' => array(
                        'query' => $type_vertical,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Position By Vertical Value (Notification)'),
                    'name' => 'TDKTOOLKIT_POSITION_VERTICAL_VALUE_NOTIFICATION',
                    'lang' => false,
                    'desc' => $this->l('Must has value from 0 to 100 with the unit type below is percent')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Position By Vertical Unit (Notification)'),
                    'name' => 'TDKTOOLKIT_POSITION_VERTICAL_UNIT_NOTIFICATION',
                    'options' => array(
                        'query' => $type_unit,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Position By Horizontal (Notification)'),
                    'hint' => $this->l('Select type of distance between "Fly cart" and the left edge or the right edge of window (browser)'),
                    'name' => 'TDKTOOLKIT_POSITION_HORIZONTAL_NOTIFICATION',
                    'options' => array(
                        'query' => $type_horizontal,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Position By Horizontal Value (Notification)'),
                    'name' => 'TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_NOTIFICATION',
                    'lang' => false,
                    'desc' => $this->l('Must has value from 0 to 100 with the unit type below is percent')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Position By Horizontal Unit (Notification)'),
                    'name' => 'TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_NOTIFICATION',
                    'options' => array(
                        'query' => $type_unit,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default'
            )
        );
        //TDK:: image setting
        $fields_form[1]['form'] = array(
            'input' => array(
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable product reviews'),
                    'name' => 'TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show product reviews at list product'),
                    'name' => 'TDKTOOLKIT_SHOW_PRODUCT_REVIEWS_LISTPRODUCT',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_PRODUCT_REVIEWS_LISTPRODUCT_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_PRODUCT_REVIEWS_LISTPRODUCT_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show number product reviews at list product'),
                    'name' => 'TDKTOOLKIT_SHOW_NUMBER_PRODUCT_REVIEWS_LISTPRODUCT',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_NUMBER_PRODUCT_REVIEWS_LISTPRODUCT_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_NUMBER_PRODUCT_REVIEWS_LISTPRODUCT_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show zero product reviews at list product'),
                    'name' => 'TDKTOOLKIT_SHOW_ZERO_PRODUCT_REVIEWS_LISTPRODUCT',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_ZERO_PRODUCT_REVIEWS_LISTPRODUCT_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_ZERO_PRODUCT_REVIEWS_LISTPRODUCT_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('All reviews must be validated by an employee'),
                    'name' => 'TDKTOOLKIT_PRODUCT_REVIEWS_MODERATE',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_PRODUCT_REVIEWS_MODERATE_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_PRODUCT_REVIEWS_MODERATE_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Allow usefull button'),
                    'name' => 'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_USEFULL_BUTTON',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_USEFULL_BUTTON_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_USEFULL_BUTTON_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Allow report button'),
                    'name' => 'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_REPORT_BUTTON',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_REPORT_BUTTON_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_REPORT_BUTTON_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Allow guest reviews'),
                    'name' => 'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_GUESTS',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_GUESTS_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_GUESTS_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Minimum time between 2 reviews from the same user'),
                    'name' => 'TDKTOOLKIT_PRODUCT_REVIEWS_MINIMAL_TIME',
                    'lang' => false,
                    'suffix' => $this->l('second(s)'),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default')
        );
        //TDK:: css setting
        $fields_form[2]['form'] = array(
            'input' => array(
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable product compare'),
                    'name' => 'TDKTOOLKIT_ENABLE_PRODUCTCOMPARE',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_PRODUCTCOMPARE_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_PRODUCTCOMPARE_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show product compare at list product'),
                    'name' => 'TDKTOOLKIT_SHOW_PRODUCTCOMPARE_LISTPRODUCT',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_PRODUCTCOMPARE_LISTPRODUCT_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_PRODUCTCOMPARE_LISTPRODUCT_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show product compare at product page'),
                    'name' => 'TDKTOOLKIT_SHOW_PRODUCTCOMPARE_PRODUCTPAGE',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_PRODUCTCOMPARE_PRODUCTPAGE_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_PRODUCTCOMPARE_PRODUCTPAGE_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Number product comparison '),
                    'name' => 'TDKTOOLKIT_COMPARATOR_MAX_ITEM',
                    'lang' => false,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default')
        );

        //TDK:: navigatior and direction
        $fields_form[3]['form'] = array(
            'input' => array(
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable product wishlist'),
                    'name' => 'TDKTOOLKIT_ENABLE_PRODUCTWISHLIST',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_PRODUCTWISHLIST_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_PRODUCTWISHLIST_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show product wishlist at list product'),
                    'name' => 'TDKTOOLKIT_SHOW_PRODUCTWISHLIST_LISTPRODUCT',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_PRODUCTWISHLIST_LISTPRODUCT_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_PRODUCTWISHLIST_LISTPRODUCT_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show product wishlist at product page'),
                    'name' => 'TDKTOOLKIT_SHOW_PRODUCTWISHLIST_PRODUCTPAGE',
                    'is_bool' => true,
                    //'desc' => $this->l('Show Input Quantity'),
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_PRODUCTWISHLIST_PRODUCTPAGE_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_SHOW_PRODUCTWISHLIST_PRODUCTPAGE_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default')
        );
        $type_display = array(
            array(
                'id_type' => 'show',
                'name_type' => $this->l('Show'),
            ),
            array(
                'id_type' => 'hide',
                'name_type' => $this->l('Hide'),
            ),
            array(
                'id_type' => 'fade',
                'name_type' => $this->l('Fade'),
            )
        );
        //TDK:: facebook chat
        $fields_form[4]['form'] = array(
            'input' => array(
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable Facebook Chat'),
                    'name' => 'TDKTOOLKIT_ENABLE_FBCHAT',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_FBCHAT_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'TDKTOOLKIT_ENABLE_FBCHAT_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Facebook Page ID '),
                    'name' => 'TDKTOOLKIT_FBCHAT_PAGEID',
                    'desc' => $this->l('Your facebook page ID. Your facebook page > About > Page ID'),
                    'lang' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Logged In Greeting '),
                    'name' => 'TDKTOOLKIT_FBCHAT_LOGGEDIN_GREETING',
                    'lang' => true,
                    'desc' => $this->l('The greeting text that will be displayed if the user is currently logged in to Facebook. Maximum 80 characters.')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Logged Out Greeting'),
                    'name' => 'TDKTOOLKIT_FBCHAT_LOGGEDOUT_GREETING',
                    'lang' => true,
                    'desc' => $this->l('The greeting text that will be displayed if the user is currently not logged in to Facebook. Maximum 80 characters.')
                ),
                array(
                    'type' => 'color',
                    'label' => $this->l('Theme Color'),
                    'name' => 'TDKTOOLKIT_FBCHAT_THEMECOLOR',
                    'lang' => false,
                    'desc' => $this->l('Specifies the background color of the customer chat plugin icon and the background color of any messages sent by users.')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Greeting Dialog Display'),
                    'name' => 'TDKTOOLKIT_FBCHAT_GREETINGDIALOG_DISPLAY',
                    'options' => array(
                        'query' => $type_display,
                        'id' => 'id_type',
                        'name' => 'name_type'
                    ),
                    'desc' => $this->l('Sets how the greeting dialog will be displayed.')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Greeting Dialog Delay'),
                    'name' => 'TDKTOOLKIT_FBCHAT_GREETINGDIALOG_DELAY',
                    'lang' => false,
                    'suffix' => $this->l('second(s)'),
                    'desc' => $this->l('Sets the number of seconds of delay before the greeting dialog is shown after the plugin is loaded.')
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default')
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->name_controller = 'tdktoolkit';
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitTdktoolkitConfig';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getGroupFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        $globalform = $helper->generateForm($fields_form);

        //TDK::
        $this->context->smarty->assign(array(
            'globalform' => $globalform,
            'url_admin' => $this->context->link->getAdminLink('AdminModules') . '&configure=' . $this->name,
            'default_tab' => Configuration::get('TDKTOOLKIT_DEFAULT_TAB'),
        ));
        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/panel.tpl');
    }

    /**
     * Set values for the inputs.
     */
    protected function getGroupFieldsValues()
    {
        $list_value = array(
            'TDKTOOLKIT_DEFAULT_TAB',
            'TDKTOOLKIT_ENABLE_AJAXCART',
            'TDKTOOLKIT_ENABLE_SELECTATTRIBUTE',
            'TDKTOOLKIT_DISPLAYATTRIBUTE_TYPE',
            'TDKTOOLKIT_ENABLE_UPDATELABEL',
            'TDKTOOLKIT_ENABLE_INPUTQUANTITY',
            'TDKTOOLKIT_ENABLE_FLYCART_EFFECT',
            'TDKTOOLKIT_ENABLE_NOTIFICATION',
            'TDKTOOLKIT_SHOW_POPUP',
            'TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS',
            'TDKTOOLKIT_SHOW_PRODUCT_REVIEWS_LISTPRODUCT',
            'TDKTOOLKIT_SHOW_NUMBER_PRODUCT_REVIEWS_LISTPRODUCT',
            'TDKTOOLKIT_SHOW_ZERO_PRODUCT_REVIEWS_LISTPRODUCT',
            'TDKTOOLKIT_PRODUCT_REVIEWS_MODERATE',
            'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_GUESTS',
            'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_REPORT_BUTTON',
            'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_USEFULL_BUTTON',
            'TDKTOOLKIT_PRODUCT_REVIEWS_MINIMAL_TIME',
            'TDKTOOLKIT_ENABLE_PRODUCTCOMPARE',
            'TDKTOOLKIT_SHOW_PRODUCTCOMPARE_LISTPRODUCT',
            'TDKTOOLKIT_SHOW_PRODUCTCOMPARE_PRODUCTPAGE',
            'TDKTOOLKIT_COMPARATOR_MAX_ITEM',
            'TDKTOOLKIT_ENABLE_PRODUCTWISHLIST',
            'TDKTOOLKIT_SHOW_PRODUCTWISHLIST_PRODUCTPAGE',
            'TDKTOOLKIT_SHOW_PRODUCTWISHLIST_LISTPRODUCT',
            'TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART',
            'TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART',
            // 'TDKTOOLKIT_ENABLE_PUSHEFFECT_DEFAULTCART',
            'TDKTOOLKIT_ENABLE_DROPDOWN_FLYCART',
            'TDKTOOLKIT_TYPE_DROPDOWN_FLYCART',
            // 'TDKTOOLKIT_ENABLE_PUSHEFFECT_FLYCART',
            'TDKTOOLKIT_TYPE_POSITION_FLYCART',
            'TDKTOOLKIT_POSITION_VERTICAL_FLYCART',
            'TDKTOOLKIT_POSITION_VERTICAL_VALUE_FLYCART',
            'TDKTOOLKIT_POSITION_VERTICAL_UNIT_FLYCART',
            'TDKTOOLKIT_POSITION_HORIZONTAL_FLYCART',
            'TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_FLYCART',
            'TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_FLYCART',
            'TDKTOOLKIT_TYPE_EFFECT_FLYCART',
            'TDKTOOLKIT_ENABLE_OVERLAYBACKGROUND_ALLCART',
            'TDKTOOLKIT_ENABLE_UPDATE_QUANTITY_ALLCART',
            'TDKTOOLKIT_ENABLE_BUTTON_QUANTITY_ALLCART',
            'TDKTOOLKIT_SHOW_COMBINATION_ALLCART',
            'TDKTOOLKIT_SHOW_CUSTOMIZATION_ALLCART',
            'TDKTOOLKIT_WIDTH_CARTITEM_ALLCART',
            'TDKTOOLKIT_HEIGHT_CARTITEM_ALLCART',
            'TDKTOOLKIT_NUMBER_CARTITEM_DISPLAY_ALLCART',
            'TDKTOOLKIT_WIDTH_NOTIFICATION',
            'TDKTOOLKIT_WIDTH_UNIT_NOTIFICATION',
            'TDKTOOLKIT_POSITION_VERTICAL_NOTIFICATION',
            'TDKTOOLKIT_POSITION_VERTICAL_VALUE_NOTIFICATION',
            'TDKTOOLKIT_POSITION_VERTICAL_UNIT_NOTIFICATION',
            'TDKTOOLKIT_POSITION_HORIZONTAL_NOTIFICATION',
            'TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_NOTIFICATION',
            'TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_NOTIFICATION',
            'TDKTOOLKIT_ENABLE_FBCHAT',
            'TDKTOOLKIT_FBCHAT_PAGEID',
            // 'TDKTOOLKIT_FBCHAT_LOGGEDIN_GREETING',
            // 'TDKTOOLKIT_FBCHAT_LOGGEDOUT_GREETING',
            'TDKTOOLKIT_FBCHAT_THEMECOLOR',
            'TDKTOOLKIT_FBCHAT_GREETINGDIALOG_DISPLAY',
            'TDKTOOLKIT_FBCHAT_GREETINGDIALOG_DELAY',
			'TDKTOOLKIT_SHOW_SIDERBARCART',
        );

        $return = array();
        foreach ($list_value as $list_value_val) {
            $return[$list_value_val] = Configuration::get($list_value_val);
        }
        $langs = Language::getLanguages(false);
        foreach ($langs as $l) {
            $return['TDKTOOLKIT_FBCHAT_LOGGEDIN_GREETING'][$l['id_lang']] = Configuration::get('TDKTOOLKIT_FBCHAT_LOGGEDIN_GREETING', $l['id_lang']);
            $return['TDKTOOLKIT_FBCHAT_LOGGEDOUT_GREETING'][$l['id_lang']] = Configuration::get('TDKTOOLKIT_FBCHAT_LOGGEDOUT_GREETING', $l['id_lang']);
			$return['TDKTOOLKIT_FREESHIPPING_TEXT'][$l['id_lang']] = Configuration::get('TDKTOOLKIT_FREESHIPPING_TEXT', $l['id_lang']);
            $return['TDKTOOLKIT_CTA_LINK'][$l['id_lang']] = Configuration::get('TDKTOOLKIT_CTA_LINK', $l['id_lang']);
        }
		// echo '<pre>';
		// print_r($return);die();
        return $return;
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getGroupFieldsValues();
        // echo '<pre>';
        // print_r($form_values);die();
        foreach (array_keys($form_values) as $key) {
			// echo '<pre>';
			// print_r($key);
			// echo '<pre>';
			// print_r(Tools::getValue($key));
            Configuration::updateValue($key, Tools::getValue($key));
        }
		// die();
        $langs = Language::getLanguages(false);
        $loggedin_greeting = array();
        $loggedout_greeting = array();
		$free_shipping_text = array();
		$link_cta = array();
        foreach ($langs as $l) {
            $loggedin_greeting[$l['id_lang']] = Tools::getValue('TDKTOOLKIT_FBCHAT_LOGGEDIN_GREETING_'.$l['id_lang']);
            $loggedout_greeting[$l['id_lang']] = Tools::getValue('TDKTOOLKIT_FBCHAT_LOGGEDOUT_GREETING_'.$l['id_lang']);
			$free_shipping_text[$l['id_lang']] = Tools::getValue('TDKTOOLKIT_FREESHIPPING_TEXT_'.$l['id_lang']);
            $link_cta[$l['id_lang']] = Tools::getValue('TDKTOOLKIT_CTA_LINK_'.$l['id_lang']);
        }
		// echo '<pre>';
        // print_r($loggedin_greeting);die();
        Configuration::updateValue('TDKTOOLKIT_FBCHAT_LOGGEDIN_GREETING', $loggedin_greeting);
        Configuration::updateValue('TDKTOOLKIT_FBCHAT_LOGGEDOUT_GREETING', $loggedout_greeting);
		Configuration::updateValue('TDKTOOLKIT_FREESHIPPING_TEXT', $free_shipping_text);
        Configuration::updateValue('TDKTOOLKIT_CTA_LINK', $link_cta);
        Tools::redirectAdmin('index.php?controller=AdminModules&configure=tdktoolkit&token=' . Tools::getAdminTokenLite('AdminModules') . '&conf=4');
		// Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true) . '&conf=4&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name);
		// return $this->displayConfirmation($this->trans('The settings have been updated.', array(), 'Admin.Notifications.Success'));
		// return '';
    }
    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookActionAdminControllerSetMedia()
    {
        $this->context->controller->addJS($this->_path . 'views/js/back.js');
        $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        Media::addJsDef(array(
            'tdktoolkit_module_dir' => $this->_path,
        ));
        $this->autoRestoreSampleData();
        //TDK:: auto update module
        $this->autoUpdateTDKModule();
    }
    /**
    * Add the CSS & JavaScript files you want to be added on the FO.
    */
    public function hookHeader()
    {
        //TDK:: fix correct token when guest checkout
        $page_name = Dispatcher::getInstance()->getController();
        $tdk_token = Tools::getToken(false);
        if ($page_name == 'orderconfirmation') {
            if ($this->context->customer->is_guest) {
                /* If guest we clear the cookie for security reason */
                $tdk_token = Tools::hash(false);
            }
        };
        Media::addJsDef(array(
            'tdk_token' => $tdk_token,
            'add_cart_error' => $this->l('An error occurred while processing your request. Please try again'),
        ));
        if (!Configuration::isCatalogMode()) {
            if (Configuration::get('TDKTOOLKIT_ENABLE_AJAXCART') || Configuration::get('TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART') || Configuration::get('TDKTOOLKIT_ENABLE_DROPDOWN_FLYCART')) {
                $this->context->controller->addJS($this->_path . 'views/js/tdktoolkit_cart.js');
            }

            //TDK:: ajax cart
            // if (Configuration::get('TDKTOOLKIT_ENABLE_AJAXCART'))
            // {
            // if (Configuration::get('TDKTOOLKIT_ENABLE_SELECTATTRIBUTE'))
            // {
            // Media::addJsDef(
            // array(
            // 'enable_product_label' => (int)Configuration::get('TDKTOOLKIT_ENABLE_UPDATELABEL'),
            // )
            // );
            // }
            // }

            $page_name = Dispatcher::getInstance()->getController();

            //TDK:: dropdown cart
            if ((Configuration::get('TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART') || Configuration::get('TDKTOOLKIT_ENABLE_DROPDOWN_FLYCART')) && $page_name != 'order') {
                $this->context->controller->addJS($this->_path . 'views/js/jquery.mousewheel.min.js');
                $this->context->controller->addJS($this->_path . 'views/js/jquery.mCustomScrollbar.js');
                $this->context->controller->addCSS($this->_path . 'views/css/jquery.mCustomScrollbar.css');
                // if (Configuration::get('TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART'))
                // {
                //TDK:: reverse position with rtl
                $type_dropdown_defaultcart = '';
                $type_dropdown_defaultcart = Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART');
                if ($this->context->language->is_rtl && !$this->is_gen_rtl) {
                    if (Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART') == 'slidebar_left') {
                        $type_dropdown_defaultcart = 'slidebar_right';
                    }
                    if (Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART') == 'slidebar_right') {
                        $type_dropdown_defaultcart = 'slidebar_left';
                    }
                }

                Media::addJsDef(array(
                    'enable_dropdown_defaultcart' => (int) Configuration::get('TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART'),
                    'type_dropdown_defaultcart' => $type_dropdown_defaultcart,
                    // 'enable_pusheffect_defaultcart' => (int)Configuration::get('TDKTOOLKIT_ENABLE_PUSHEFFECT_DEFAULTCART'),
                    'number_cartitem_display' => (int) Configuration::get('TDKTOOLKIT_NUMBER_CARTITEM_DISPLAY_ALLCART'),
                    'width_cart_item' => Configuration::get('TDKTOOLKIT_WIDTH_CARTITEM_ALLCART'),
                    'height_cart_item' => Configuration::get('TDKTOOLKIT_HEIGHT_CARTITEM_ALLCART')
                ));
            }
        }

        Media::addJsDef(array(
            'enable_flycart_effect' => (int) Configuration::get('TDKTOOLKIT_ENABLE_FLYCART_EFFECT'),
            'type_flycart_effect' => Configuration::get('TDKTOOLKIT_TYPE_EFFECT_FLYCART'),
            'enable_notification' => (int) Configuration::get('TDKTOOLKIT_ENABLE_NOTIFICATION'),
            'show_popup' => (int) Configuration::get('TDKTOOLKIT_SHOW_POPUP'),
			'show_sidebarcart' => (int) Configuration::get('TDKTOOLKIT_SHOW_SIDERBARCART'),
            'lf_is_gen_rtl' => $this->is_gen_rtl,
        ));

        $this->context->controller->addCSS($this->_path . 'views/css/front.css');
        // $this->static_token = Tools::getToken(false);
        // $this->id_lang = $this->context->language->id;
        // if (isset($this->context->controller->php_self) && ($this->context->controller->php_self == 'product')) {
        if (Configuration::get('TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS')) {
            $this->context->controller->addJS($this->_path . 'views/js/jquery.rating.pack.js');
            $this->context->controller->addJS($this->_path . 'views/js/tdktoolkit_review.js');
            //TDK:: add txt cancel rating to translate
            Media::addJsDef(array(
                'cancel_rating_txt' => $this->l('Cancel Rating'),
            ));
        }
        // }


        if (Configuration::get('TDKTOOLKIT_ENABLE_PRODUCTCOMPARE') && Configuration::get('TDKTOOLKIT_COMPARATOR_MAX_ITEM') > 0) {
            $this->context->controller->addJS($this->_path . 'views/js/tdktoolkit_compare.js');

            //TDK:: add javascript param for compare
            $compared_products = array();
            if (Configuration::get('TDKTOOLKIT_COMPARATOR_MAX_ITEM') && isset($this->context->cookie->id_compare)) {
                $compared_products = CompareProduct::getCompareProducts($this->context->cookie->id_compare);
            }

            $comparator_max_item = (int) Configuration::get('TDKTOOLKIT_COMPARATOR_MAX_ITEM');

            $productcompare_max_item = sprintf($this->l('You cannot add more than %d product(s) to the product comparison'), $comparator_max_item);
            Media::addJsDef(array(
                'productcompare_url' => $this->link->getModuleLink('tdktoolkit', 'productscompare'),
                'productcompare_add' => $this->l('The product has been added to list compare'),
                'productcompare_viewlistcompare' => $this->l('View list compare'),
                'productcompare_remove' => $this->l('The product was successfully removed from list compare'),
                'productcompare_add_error' => $this->l('An error occurred while adding. Please try again'),
                'productcompare_remove_error' => $this->l('An error occurred while removing. Please try again'),
                'comparator_max_item' => $comparator_max_item,
                'compared_products' => (count($compared_products) > 0) ? $compared_products : array(),
                'productcompare_max_item' => $productcompare_max_item,
                'buttoncompare_title_add' => $this->l('Add to Compare'),
                'buttoncompare_title_remove' => $this->l('Remove from Compare'),
            ));
        }

        if (Configuration::get('TDKTOOLKIT_ENABLE_PRODUCTWISHLIST')) {
            $this->context->controller->addJS($this->_path . 'views/js/tdktoolkit_wishlist.js');
            //TDK:: add javascript param for wishlist
            if ($this->context->customer->isLogged()) {
                $isLogged = true;
            } else {
                $isLogged = false;
            }
            Media::addJsDef(array(
                'wishlist_url' => $this->link->getModuleLink('tdktoolkit', 'mywishlist'),
                'wishlist_add' => $this->l('The product was successfully added to your wishlist'),
                'wishlist_viewwishlist' => $this->l('View your wishlist'),
                'wishlist_remove' => $this->l('The product was successfully removed from your wishlist'),
                // 'wishlist_products' => (count($this->array_wishlist_product)>0) ? $this->array_wishlist_product : array(),
                'buttonwishlist_title_add' => $this->l('Add to Wishlist'),
                'buttonwishlist_title_remove' => $this->l('Remove from WishList'),
                'wishlist_loggin_required' => $this->l('You must be logged in to manage your wishlist'),
                'isLogged' => $isLogged,
                'wishlist_cancel_txt' => $this->l('Cancel'),
                'wishlist_ok_txt' => $this->l('Ok'),
                'wishlist_send_txt' => $this->l('Send'),
                'wishlist_reset_txt' => $this->l('Reset'),
                'wishlist_send_wishlist_txt' => $this->l('Send wishlist'),
                'wishlist_email_txt' => $this->l('Email'),
                'wishlist_confirm_del_txt' => $this->l('Delete selected item?'),
                'wishlist_del_default_txt' => $this->l('Cannot delete default wishlist'),
                'wishlist_quantity_required' => $this->l('You must enter a quantity'),
            ));
        }
        if (Configuration::get('TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS')) {
            Media::addJsDef(array(
                'disable_review_form_txt' => $this->l('Not exists a criterion to review for this product or this language'),
                'review_error' => $this->l('An error occurred while processing your request. Please try again'),
            ));
        }
    }

    public function hookdisplayTdkCartButton($params)
    {
        if (!Configuration::isCatalogMode()) {
            if (Configuration::get('TDKTOOLKIT_ENABLE_AJAXCART')) {
                $this->link_cart = $this->context->link->getPageLink('cart', true);
                // $presenter = new ProductPresenter();
                $tdk_cart_product = array();
                $product = $params['product'];
                $id_product = $product['id_product'];
                if ($this->shouldEnableAddToCartButton($product)) {
                    $tdk_cart_product['add_to_cart_url'] = $this->getAddToCartURL($product);
                } else {
                    $tdk_cart_product['add_to_cart_url'] = null;
                }
                if ($product['customizable']) {
                    $customization_datas = $this->context->cart->getProductCustomization($id_product, null, true);
                }
                $tdk_cart_product['id_customization'] = empty($customization_datas) ? null : $customization_datas[0]['id_customization'];
                //TDK:: fix for some case have not
                if (!isset($product['product_attribute_minimal_quantity'])) {
                    $tdk_cart_product['product_attribute_minimal_quantity'] = Attribute::getAttributeMinimalQty($product['id_product_attribute']);
                } else {
                    $tdk_cart_product['product_attribute_minimal_quantity'] = $product['product_attribute_minimal_quantity'];
                }
                if (isset($product['wishlist_quantity'])) {
                    $tdk_cart_product['wishlist_quantity'] = $product['wishlist_quantity'];
                }
                //TDK:: update parameter
                $tdk_cart_product['id_product'] = $id_product;
                $tdk_cart_product['id_product_attribute'] = $product['id_product_attribute'];
                $tdk_cart_product['quantity'] = $product['quantity'];
                $tdk_cart_product['minimal_quantity'] = $product['minimal_quantity'];
                $templateVars = array(
                    'static_token' => Tools::getToken(false),
                    'tdk_cart_product' => $tdk_cart_product,
                    'link_cart' => $this->link_cart,
                        // 'show_input_quantity' => Configuration::get('TDKTOOLKIT_ENABLE_INPUTQUANTITY'),
                );
                // echo '<pre>';
                // print_r($product);die();
                $this->context->smarty->assign($templateVars);
                return $this->display(__FILE__, 'tdk_cart_button.tpl');
            }
        }
    }

    public function hookdisplayTdkCartQuantity($params)
    {
        if (!Configuration::isCatalogMode()) {
            $product = $params['product'];

            if (Configuration::get('TDKTOOLKIT_ENABLE_INPUTQUANTITY') && Configuration::get('TDKTOOLKIT_ENABLE_AJAXCART')) {
//                $id_product = $product['id_product'];

                $templateVars = array(
                    'tdk_cart_product_quantity' => $product,
                );
                // echo '<pre>';
                // print_r($product);die();
                $this->context->smarty->assign($templateVars);
                return $this->display(__FILE__, 'tdk_cart_button_quantity.tpl');
            }
        }
    }

    public function hookdisplayTdkCartAttribute($params)
    {
        if (!Configuration::isCatalogMode()) {
            $product = $params['product'];
            if (Configuration::get('TDKTOOLKIT_ENABLE_SELECTATTRIBUTE') && count($product['attributes']) > 0 && Configuration::get('TDKTOOLKIT_ENABLE_AJAXCART')) {
                $id_product = $product['id_product'];
                $lang_id = $this->context->language->id;
                if (isset($params['display_type']) && $params['display_type'] != '') {
                    $display_type = $params['display_type'];
                } else {
                    $display_type = Configuration::get('TDKTOOLKIT_DISPLAYATTRIBUTE_TYPE');
                }
                if ($display_type == 1) {
                    $attributes = $this->getAttributesResume($id_product, $lang_id);
                    if (!empty($attributes)) {
                        $whitelist = $this->getProductAttributeWhitelist();
                        foreach ($attributes as $k_attributes => $v_attributes) {
                            if ($v_attributes['id_product_attribute'] == $product['id_product_attribute']) {
                                $product['attribute_designation'] = $v_attributes['attribute_designation'];
                            }
                            foreach ($whitelist as $v_whitelist) {
                                if (isset($product[$v_whitelist])) {
                                    $attributes[$k_attributes][$v_whitelist] = $product[$v_whitelist];
                                }
                            }
                            if ($this->shouldEnableAddToCartButton($attributes[$k_attributes])) {
                                $attributes[$k_attributes]['add_to_cart_url'] = $this->getAddToCartURL($attributes[$k_attributes]);
                            } else {
                                $attributes[$k_attributes]['add_to_cart_url'] = null;
                            }
                        }
                        $product['combinations'] = $attributes;
                    }
                    $templateVars = array(
                        'tdk_cart_product_attribute' => $product,
                        'display_type' => $display_type,
                    );
                }
                if ($display_type == 2 || $display_type == 3 || $display_type == 4 || $display_type == 5 || $display_type == 6) {
                    $list_attribute = array();
                    $list_attribute['attributes'] = $product['attributes'];
                    $list_attr_group = $this->getListAttributeGroup($id_product, $lang_id, $list_attribute, $display_type);
                    //TDK:: check page to display product attribute
                    $page = '';
                    if (isset($params['page'])) {
                        $page = $params['page'];
                    }
                    $templateVars = array(
                        'list_attr_group' => $list_attr_group,
                        'display_type' => $display_type,
                        'id_product' => $id_product,
                        'tdk_page' => $page
                    );
                }
                $this->context->smarty->assign($templateVars);
                return $this->display(__FILE__, 'tdk_cart_button_attribute.tpl');
            }
        }
    }
    //TDK:: get list attribute by group (clone from core)
    public function getListAttributeGroup($id_product, $lang_id, $product_for_template = null, $display_type = null)
    {
        $groups = array();
        $product_obj =  new Product($id_product, false, $lang_id);
        // @todo (RM) should only get groups and not all declination ?
        $attributes_groups = $product_obj->getAttributesGroups($lang_id);
        if (is_array($attributes_groups) && $attributes_groups) {
            // $combination_images = $product_obj->getCombinationImages($lang_id);
            // $combination_prices_set = array();
            foreach ($attributes_groups as $k => $row) {
                if (!isset($groups[$row['id_attribute_group']])) {
                    $groups[$row['id_attribute_group']] = array(
                        'group_name' => $row['group_name'],
                        'name' => $row['public_group_name'],
                        'group_type' => $row['group_type'],
                        'default' => -1,
                    );
                }

                //TDK:: add image attribute cover
                $image_attribute_cover = '';
                if (($display_type == 5 || $display_type == 6) && $row['group_type'] == 'color') {
                    $id_product_attribute = $this->getIdProductAttributeByIdAttributes($id_product, $row['id_attribute'], true);
                    $combination_image = Product::getCombinationImageById($id_product_attribute, $lang_id);				
					if ($combination_image['id_image']) {
						$image_attribute_cover = $this->context->link->getImageLink($product_obj->link_rewrite, $combination_image['id_image'], ImageType::getFormattedName('small'));
					}                   			
                }
                $groups[$row['id_attribute_group']]['attributes'][$row['id_attribute']] = array(
                    'name' => $row['attribute_name'],
                    'html_color_code' => $row['attribute_color'],
                    'texture' => (@filemtime(_PS_COL_IMG_DIR_.$row['id_attribute'].'.jpg')) ? _THEME_COL_DIR_.$row['id_attribute'].'.jpg' : '',
                    'selected' => (isset($product_for_template['attributes'][$row['id_attribute_group']]['id_attribute']) && $product_for_template['attributes'][$row['id_attribute_group']]['id_attribute'] == $row['id_attribute']) ? true : false,
                    'image_attribute_cover' => $image_attribute_cover
                );

                //$product.attributes.$id_attribute_group.id_attribute eq $id_attribute
                if ($row['default_on'] && $groups[$row['id_attribute_group']]['default'] == -1) {
                    $groups[$row['id_attribute_group']]['default'] = (int) $row['id_attribute'];
                }
                //TDK:: get selected attribute
                if (isset($product_for_template['attributes'][$row['id_attribute_group']]['id_attribute']) && $product_for_template['attributes'][$row['id_attribute_group']]['id_attribute'] == $row['id_attribute']) {
                    $groups[$row['id_attribute_group']]['selected'] = (int) $row['id_attribute'];
                }
                if (!isset($groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']])) {
                    $groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']] = 0;
                }
                $groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']] += (int) $row['quantity'];
            }
            // wash attributes list depending on available attributes depending on selected preceding attributes
            $current_selected_attributes = array();
            $count = 0;
            foreach ($groups as &$group) {
                $count++;
                if ($count > 1) {
                    //find attributes of current group, having a possible combination with current selected
                    $id_product_attributes = array(0);
                    $query = 'SELECT pac.`id_product_attribute`
                        FROM `'._DB_PREFIX_.'product_attribute_combination` pac
                        INNER JOIN `'._DB_PREFIX_.'product_attribute` pa ON pa.id_product_attribute = pac.id_product_attribute
                        WHERE id_product = '.$product_obj->id.' AND id_attribute IN ('.implode(',', array_map('intval', $current_selected_attributes)).')
                        GROUP BY id_product_attribute
                        HAVING COUNT(id_product) = '.count($current_selected_attributes);
                    if ($results = Db::getInstance()->executeS($query)) {
                        foreach ($results as $row) {
                            $id_product_attributes[] = $row['id_product_attribute'];
                        }
                    }
                    $id_attributes = Db::getInstance()->executeS('SELECT `id_attribute` FROM `'._DB_PREFIX_.'product_attribute_combination` pac2
                        WHERE `id_product_attribute` IN ('.implode(',', array_map('intval', $id_product_attributes)).')
                        AND id_attribute NOT IN ('.implode(',', array_map('intval', $current_selected_attributes)).')');
                    foreach ($id_attributes as $k => $row) {
                        $id_attributes[$k] = (int)$row['id_attribute'];
                    }
                    //TDK:: display disable attribute for type 3
                    if (isset($display_type) && ($display_type == 3 || $display_type == 5)) {
                        foreach ($group['attributes'] as $key => $attribute) {
                            if (!in_array((int)$key, $id_attributes)) {
                                $group['attributes'][$key]['disable'] = 1;
                            }
                        }
                    } else {
                        foreach ($group['attributes'] as $key => $attribute) {
                            if (!in_array((int)$key, $id_attributes)) {
                                unset($group['attributes'][$key]);
                                unset($group['attributes_quantity'][$key]);
                            }
                        }
                    }
                }
                //find selected attribute or first of group
                $index = 0;
                $current_selected_attribute = 0;
                foreach ($group['attributes'] as $key => $attribute) {
                    if ($index === 0) {
                        $current_selected_attribute = $key;
                    }
                    if ($attribute['selected']) {
                        $current_selected_attribute = $key;
                        break;
                    }
                }
                if ($current_selected_attribute > 0) {
                    $current_selected_attributes[] = $current_selected_attribute;
                }
            }
            // wash attributes list (if some attributes are unavailables and if allowed to wash it)
            if (!Product::isAvailableWhenOutOfStock($product_obj->out_of_stock) && Configuration::get('PS_DISP_UNAVAILABLE_ATTR') == 0) {
                foreach ($groups as &$group) {
                    foreach ($group['attributes_quantity'] as $key => &$quantity) {
                        if ($quantity <= 0) {
                            unset($group['attributes'][$key]);
                        }
                    }
                }
            }
            return $groups;
        } else {
            return array();
        }
    }
    //TDK:: get attribute group (clone core)
    // public function getAttributesGroups($id_product, $id_lang)
    // {
        // if (!Combination::isFeatureActive()) {
            // return array();
        // }
        // $sql = 'SELECT ag.`id_attribute_group`, ag.`is_color_group`, agl.`name` AS group_name, agl.`public_name` AS public_group_name,
                    // a.`id_attribute`, al.`name` AS attribute_name, a.`color` AS attribute_color, product_attribute_shop.`id_product_attribute`,
                    // IFNULL(stock.quantity, 0) as quantity, product_attribute_shop.`price`, product_attribute_shop.`ecotax`, product_attribute_shop.`weight`,
                    // product_attribute_shop.`default_on`, pa.`reference`, product_attribute_shop.`unit_price_impact`,
                    // product_attribute_shop.`minimal_quantity`, product_attribute_shop.`available_date`, ag.`group_type`
                // FROM `'._DB_PREFIX_.'product_attribute` pa
                // '.Shop::addSqlAssociation('product_attribute', 'pa').'
                // '.Product::sqlStock('pa', 'pa').'
                // LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
                // LEFT JOIN `'._DB_PREFIX_.'attribute` a ON (a.`id_attribute` = pac.`id_attribute`)
                // LEFT JOIN `'._DB_PREFIX_.'attribute_group` ag ON (ag.`id_attribute_group` = a.`id_attribute_group`)
                // LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute`)
                // LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl ON (ag.`id_attribute_group` = agl.`id_attribute_group`)
                // '.Shop::addSqlAssociation('attribute', 'a').'
                // WHERE pa.`id_product` = '.(int)$id_product.'
                    // AND al.`id_lang` = '.(int)$id_lang.'
                    // AND agl.`id_lang` = '.(int)$id_lang.'
                // GROUP BY id_attribute_group, id_product_attribute
                // ORDER BY ag.`position` ASC, a.`position` ASC, agl.`name` ASC';
        // return Db::getInstance()->executeS($sql);
    // }

    public function hookdisplayTdkProductReviewExtra($params)
    {
        if (Configuration::get('TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS')) {
            $product = $params['product'];
            // echo '<pre>';
            // print_r($product);die();
            $id_product = $product['id_product'];

            $id_guest = (!$id_customer = (int) $this->context->cookie->id_customer) ? (int) $this->context->cookie->id_guest : false;
            // validate module
            unset($id_customer);
            $customerReview = ProductReview::getByCustomer((int) $id_product, (int) $this->context->cookie->id_customer, true, (int) $id_guest);

            $average = ProductReview::getAverageGrade((int) $id_product);

            $this->context->smarty->assign(array(
                // 'secure_key' => $this->secure_key,
                // 'logged' => $this->context->customer->isLogged(true),
                'allow_guests_extra' => (int) Configuration::get('TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_GUESTS'),
                //'criterions' => ProductReviewCriterion::getByProduct((int) $id_product, $this->context->language->id),
                'averageTotal_extra' => round($average['grade']),
                'ratings_extra' => ProductReview::getRatings((int) $id_product),
                'too_early_extra' => ($customerReview && (strtotime($customerReview['date_add']) + Configuration::get('TDKTOOLKIT_PRODUCT_REVIEWS_MINIMAL_TIME')) > time()),
                'nbReviews_product_extra' => (int) (ProductReview::getReviewNumber((int) $id_product)),
                'id_product_review_extra' => $id_product,
                'link_product_review_extra' => $product['link'],
            ));
            // print_r((int) (ProductReview::getReviewNumber((int) $id_product)));die();
            return $this->display(__FILE__, 'tdk_product_review_extra.tpl');
        }
    }

    //TDK:: create review tab at product page
    public function hookdisplayTdkProductTab($params)
    {
        if (Configuration::get('TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS')) {
            // print_r($params['use_ptabs']);die();
            if (Module::isInstalled('tdkplatform') && Module::isEnabled('tdkplatform')) {
                include_once(_PS_MODULE_DIR_ . 'tdkplatform/tdkplatform.php');
                $module = new TdkPlatform();
                $this->context->smarty->assign(array(
                    'USE_PTABS' => $module->getConfig('ENABLE_PTAB'),
                ));
            }

            return $this->display(__FILE__, 'tdk_product_tab.tpl');
        }
    }

    //TDK:: display reviews on review tab at product detail
    public function hookdisplayTdkProductTabContent($params)
    {
        if (Configuration::get('TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS')) {
            $product = $params['product'];
            $id_product = $product['id_product'];

            $id_guest = (!$id_customer = (int) $this->context->cookie->id_customer) ? (int) $this->context->cookie->id_guest : false;
            // validate module
            unset($id_customer);
            $customerReview = ProductReview::getByCustomer((int) $id_product, (int) $this->context->cookie->id_customer, true, (int) $id_guest);

            $this->context->smarty->assign(array(
                'reviews' => ProductReview::getByProduct((int) $id_product, 1, null, $this->context->cookie->id_customer),
                'allow_guests' => (int) Configuration::get('TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_GUESTS'),
                'too_early' => ($customerReview && (strtotime($customerReview['date_add']) + Configuration::get('TDKTOOLKIT_PRODUCT_REVIEWS_MINIMAL_TIME')) > time()),
                'allow_report_button' => (int) Configuration::get('TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_REPORT_BUTTON'),
                'allow_usefull_button' => (int) Configuration::get('TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_USEFULL_BUTTON'),
                'id_product_tab_content' => $id_product,
                'link_product_tab_content' => $product['link'],
            ));
            if (Module::isInstalled('tdkplatform') && Module::isEnabled('tdkplatform')) {
                include_once(_PS_MODULE_DIR_ . 'tdkplatform/tdkplatform.php');
                $module = new TdkPlatform();
                $this->context->smarty->assign(array(
                    'USE_PTABS' => $module->getConfig('ENABLE_PTAB'),
                ));
            }

            return $this->display(__FILE__, 'tdk_product_tab_content.tpl');
        }
    }

    //TDK:: display review of product at product list
    public function hookdisplayTdkProductListReview($params)
    {
        if (Configuration::get('TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS') && Configuration::get('TDKTOOLKIT_SHOW_PRODUCT_REVIEWS_LISTPRODUCT')) {
            $product = $params['product'];
            $id_product = $product['id_product'];
            if (!$this->isCached('tdk_list_product_review.tpl', $this->getCacheId($id_product))) {
                $average = ProductReview::getAverageGrade($id_product);
                $this->smarty->assign(array(
                    // 'product' => $product,
                    'averageTotal' => round($average['grade']),
                    'ratings' => ProductReview::getRatings($id_product),
                    'nbReviews' => (int) ProductReview::getReviewNumber($id_product),
                    'show_number_product_review' => Configuration::get('TDKTOOLKIT_SHOW_NUMBER_PRODUCT_REVIEWS_LISTPRODUCT'),
                    'show_zero_product_review' => Configuration::get('TDKTOOLKIT_SHOW_ZERO_PRODUCT_REVIEWS_LISTPRODUCT'),
                ));
            }
            return $this->display(__FILE__, 'tdk_list_product_review.tpl', $this->getCacheId($id_product));
        }
    }

    //TDK:: display compare button
    public function hookdisplayTdkCompareButton($params)
    {
        if (Configuration::get('TDKTOOLKIT_ENABLE_PRODUCTCOMPARE') && Configuration::get('TDKTOOLKIT_COMPARATOR_MAX_ITEM') > 0) {
            $page_name = Dispatcher::getInstance()->getController();
            if ((Configuration::get('TDKTOOLKIT_SHOW_PRODUCTCOMPARE_LISTPRODUCT') && $page_name != 'product') || (Configuration::get('TDKTOOLKIT_SHOW_PRODUCTCOMPARE_PRODUCTPAGE') && $page_name == 'product')) {
                $id_product = $params['product']['id_product'];
                $compared_products = array();
                if (Configuration::get('TDKTOOLKIT_COMPARATOR_MAX_ITEM') && isset($this->context->cookie->id_compare)) {
                    $compared_products = CompareProduct::getCompareProducts($this->context->cookie->id_compare);
                }
                $added = false;
                if (count($compared_products) > 0 && in_array($id_product, $compared_products)) {
                    $added = true;
                }
                $this->smarty->assign(array(
                    'added' => $added,
                    'tdk_compare_id_product' => $id_product,
                ));
                return $this->display(__FILE__, 'tdk_compare_button.tpl');
            }
        }
    }

    //TDK:: display review at product compare page
    public function hookdisplayTdkProducReviewCompare($params)
    {
        if (Configuration::get('TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS')) {
            // echo '<pre>';
            // print_r($params['list_product']);die();
            $list_grades = array();
            $list_product_grades = array();
            $list_product_average = array();
            $list_product_review = array();

            foreach ($params['list_product'] as $id_product) {
                $id_product = (int) $id_product;
                $grades = ProductReview::getAveragesByProduct($id_product, $this->context->language->id);
                $criterions = ProductReviewCriterion::getByProduct($id_product, $this->context->language->id);
                $grade_total = 0;
                if (count($grades) > 0) {
                    foreach ($criterions as $criterion) {
                        if (isset($grades[$criterion['id_product_review_criterion']])) {
                            $list_product_grades[$criterion['id_product_review_criterion']][$id_product] = $grades[$criterion['id_product_review_criterion']];
                            $grade_total += (float) ($grades[$criterion['id_product_review_criterion']]);
                        } else {
                            $list_product_grades[$criterion['id_product_review_criterion']][$id_product] = 0;
                        }

                        if (!array_key_exists($criterion['id_product_review_criterion'], $list_grades)) {
                            $list_grades[$criterion['id_product_review_criterion']] = $criterion['name'];
                        }
                    }

                    $list_product_average[$id_product] = $grade_total / count($criterions);
                    $list_product_review[$id_product] = ProductReview::getByProduct($id_product, 0, 3);
                }
            }

            if (count($list_grades) < 1) {
                return false;
            }

            $this->context->smarty->assign(array(
                'grades' => $list_grades,
                'product_grades' => $list_product_grades,
                'list_ids_product' => $params['list_product'],
                'list_product_average' => $list_product_average,
                'product_reviews' => $list_product_review,
            ));

            return $this->display(__FILE__, '/tdk_product_review_compare.tpl');
        }
    }

    //TDK:: display wishlist button
    public function hookdisplayTdkWishlistButton($params)
    {
        if (Configuration::get('TDKTOOLKIT_ENABLE_PRODUCTWISHLIST')) {
            $page_name = Dispatcher::getInstance()->getController();
            if ((Configuration::get('TDKTOOLKIT_SHOW_PRODUCTWISHLIST_LISTPRODUCT') && $page_name != 'product') || (Configuration::get('TDKTOOLKIT_SHOW_PRODUCTWISHLIST_PRODUCTPAGE') && $page_name == 'product')) {
                $wishlists = array();
                $wishlists_added = array();
                $id_wishlist = false;
                $added_wishlist = false;
                $id_product = $params['product']['id_product'];
                $id_product_attribute = $params['product']['id_product_attribute'];
                if ($this->context->customer->isLogged()) {
                    $wishlists = Wishlist::getByIdCustomer($this->context->customer->id);
                    if (empty($this->context->cookie->id_wishlist) === true ||
                            WishList::exists($this->context->cookie->id_wishlist, $this->context->customer->id) === false) {
                        if (!count($wishlists)) {
                            $id_wishlist = false;
                        } else {
                            $id_wishlist = (int) $wishlists[0]['id_wishlist'];
                            $this->context->cookie->id_wishlist = (int) $id_wishlist;
                        }
                    } else {
                        $id_wishlist = $this->context->cookie->id_wishlist;
                    }

                    $wishlist_products = ($id_wishlist == false ? array() : WishList::getSimpleProductByIdCustomer($this->context->customer->id, $this->context->shop->id));

                    $check_product_added = array('id_product' => $id_product, 'id_product_attribute' => $id_product_attribute);

                    foreach ($wishlist_products as $key => $wishlist_products_val) {
                        if (in_array($check_product_added, $wishlist_products_val)) {
                            $added_wishlist = true;
                            $wishlists_added[] = $key;
                        }
                    }
                }

                $this->smarty->assign(array(
                    'wishlists_added' => $wishlists_added,
                    'wishlists' => $wishlists,
                    'added_wishlist' => $added_wishlist,
                    'id_wishlist' => $id_wishlist,
                    'tdk_wishlist_id_product' => $id_product,
                    'tdk_wishlist_id_product_attribute' => $id_product_attribute,
                ));

                return $this->display(__FILE__, 'tdk_wishlist_button.tpl');
            }
        }
    }

    //TDK:: add mywishlist link to page my account
    public function hookdisplayCustomerAccount($params)
    {
        if (Configuration::get('TDKTOOLKIT_ENABLE_PRODUCTWISHLIST')) {
            $this->context->smarty->assign(array(
                'wishlist_link' => $this->link->getModuleLink('tdktoolkit', 'mywishlist'),
            ));
            return $this->display(__FILE__, 'tdk_wishlist_link.tpl');
        }
    }

    //TDK:: create fly cart
    public function hookDisplayBeforeBodyClosingTag($params)
    {
        if (!Configuration::isCatalogMode()) {
            // print_r('test');die();
            $output = '';
            $check_create_slidebar = false;
            $page_name = Dispatcher::getInstance()->getController();

            if (Configuration::get('TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART') && (Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART') == 'slidebar_left' || Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART') == 'slidebar_right' || Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART') == 'slidebar_top' || Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART') == 'slidebar_bottom') && $page_name != 'order') {
                //TDK:: reverse position with rtl
                $type_dropdown_defaultcart = '';
                $type_dropdown_defaultcart = Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART');
                if ($this->context->language->is_rtl && !$this->is_gen_rtl) {
                    if (Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART') == 'slidebar_left') {
                        $type_dropdown_defaultcart = 'slidebar_right';
                    }
                    if (Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART') == 'slidebar_right') {
                        $type_dropdown_defaultcart = 'slidebar_left';
                    }
                }
                $output .= $this->buildFlyCartSlideBar($type_dropdown_defaultcart);
                if (Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART') == Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_FLYCART')) {
                    $check_create_slidebar = true;
                }
            }
            if (Configuration::get('TDKTOOLKIT_ENABLE_DROPDOWN_FLYCART') && $page_name != 'order') {
                $output .= $this->buildFlyCart();
                if (!$check_create_slidebar && (Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_FLYCART') == 'slidebar_left' || Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_FLYCART') == 'slidebar_right' || Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_FLYCART') == 'slidebar_top' || Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_FLYCART') == 'slidebar_bottom')) {
                    //TDK:: reverse position with rtl
                    $type_fly_cart = '';
                    $type_fly_cart = Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_FLYCART');
                    if ($this->context->language->is_rtl && !$this->is_gen_rtl) {
                        if (Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_FLYCART') == 'slidebar_left') {
                            $type_fly_cart = 'slidebar_right';
                        }
                        if (Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_FLYCART') == 'slidebar_right') {
                            $type_fly_cart = 'slidebar_left';
                        }
                    }
                    $output .= $this->buildFlyCartSlideBar($type_fly_cart);
                }
            }

            return $output;
        }
    }

    //TDK:: build fly cart
    public function buildFlyCart()
    {
        $vertical_position = '';

        $vertical_position .= Configuration::get('TDKTOOLKIT_POSITION_VERTICAL_FLYCART') . ':';

        if (Configuration::get('TDKTOOLKIT_POSITION_VERTICAL_UNIT_FLYCART') == 'percent') {
            $vertical_position .= Configuration::get('TDKTOOLKIT_POSITION_VERTICAL_VALUE_FLYCART') . '%';
        }
        if (Configuration::get('TDKTOOLKIT_POSITION_VERTICAL_UNIT_FLYCART') == 'pixel') {
            $vertical_position .= Configuration::get('TDKTOOLKIT_POSITION_VERTICAL_VALUE_FLYCART') . 'px';
        }
        $horizontal_position = '';
        //TDK:: reverse position with rtl
        $type_horizontal_position = '';
        $type_horizontal_position = Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_FLYCART');
        if ($this->context->language->is_rtl) {
            if (Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_FLYCART') == 'left') {
                $type_horizontal_position = 'right';
            }
            if (Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_FLYCART') == 'right') {
                $type_horizontal_position = 'left';
            }
        }
        $horizontal_position .= $type_horizontal_position . ':';
        if (Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_FLYCART') == 'percent') {
            $horizontal_position .= Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_FLYCART') . '%';
        }
        if (Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_FLYCART') == 'pixel') {
            $horizontal_position .= Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_FLYCART') . 'px';
        }
        //TDK:: reverse position with rtl
        $type_fly_cart = '';
        $type_fly_cart = Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_FLYCART');
        if ($this->context->language->is_rtl && !$this->is_gen_rtl) {
            if (Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_FLYCART') == 'slidebar_left') {
                $type_fly_cart = 'slidebar_right';
            }
            if (Configuration::get('TDKTOOLKIT_TYPE_DROPDOWN_FLYCART') == 'slidebar_right') {
                $type_fly_cart = 'slidebar_left';
            }
        }

        $templateVars = array(
            'type_fly_cart' => $type_fly_cart,
            // 'pusheffect' => (int)Configuration::get('TDKTOOLKIT_ENABLE_PUSHEFFECT_FLYCART'),
            'type_position' => Configuration::get('TDKTOOLKIT_TYPE_POSITION_FLYCART'),
            'vertical_position' => $vertical_position,
            'horizontal_position' => $horizontal_position,
        );
        $this->smarty->assign($templateVars);

        return $this->fetch('module:tdktoolkit/views/templates/front/fly_cart.tpl');
    }

    //TDK:: build fly cart
    public function buildFlyCartSlideBar($type)
    {
        $templateVars = array(
            'type' => $type,
            'enable_overlay_background' => (int) Configuration::get('TDKTOOLKIT_ENABLE_OVERLAYBACKGROUND_ALLCART'),
        );
        $this->smarty->assign($templateVars);

        return $this->fetch('module:tdktoolkit/views/templates/front/fly_cart_slide_bar.tpl');
    }

    //TDK:: copy function from base
    public function shouldEnableAddToCartButton($product)
    {
        if (($product['customizable'] == 2 || !empty($product['customization_required']))) {
            $shouldShowButton = false;

            if (isset($product['customizations'])) {
                $shouldShowButton = true;
                foreach ($product['customizations']['fields'] as $field) {
                    if ($field['required'] && !$field['is_customized']) {
                        $shouldShowButton = false;
                    }
                }
            }
        } else {
            $shouldShowButton = true;
        }

        $shouldShowButton = $shouldShowButton && $this->shouldShowAddToCartButton($product);

        if ($product['quantity'] <= 0 && !$product['allow_oosp']) {
            $shouldShowButton = false;
        }

        return $shouldShowButton;
    }

    //TDK:: copy function from base
    public function shouldShowAddToCartButton($product)
    {
        return (bool) $product['available_for_order'];
    }

    //TDK:: copy function from base
    public function getAddToCartURL($product)
    {
        // echo '<pre>';
        // print_r($this->link);die();
        return $this->link->getAddToCartURL($product['id_product'], $product['id_product_attribute']);
    }

    //TDK:: render modal cart popup
    public function renderModal()
    {
        $output = $this->fetch('module:tdktoolkit/views/templates/front/modal.tpl');

        return $output;
    }

    //TDK:: render notification
    public function renderNotification()
    {
        if (!Configuration::isCatalogMode()) {
            if ((Configuration::get('TDKTOOLKIT_ENABLE_AJAXCART') && Configuration::get('TDKTOOLKIT_ENABLE_NOTIFICATION')) || Configuration::get('TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART') || Configuration::get('TDKTOOLKIT_ENABLE_DROPDOWN_FLYCART')) {
                $vertical_position = '';

                $vertical_position .= Configuration::get('TDKTOOLKIT_POSITION_VERTICAL_NOTIFICATION') . ':';

                if (Configuration::get('TDKTOOLKIT_POSITION_VERTICAL_UNIT_NOTIFICATION') == 'percent') {
                    $vertical_position .= Configuration::get('TDKTOOLKIT_POSITION_VERTICAL_VALUE_NOTIFICATION') . '%';
                }
                if (Configuration::get('TDKTOOLKIT_POSITION_VERTICAL_UNIT_NOTIFICATION') == 'pixel') {
                    $vertical_position .= Configuration::get('TDKTOOLKIT_POSITION_VERTICAL_VALUE_NOTIFICATION') . 'px';
                }
                $horizontal_position = '';
                //TDK:: reverse position with rtl
                $type_horizontal_position = '';
                $type_horizontal_position = Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_NOTIFICATION');
                if ($this->context->language->is_rtl) {
                    if (Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_NOTIFICATION') == 'left') {
                        $type_horizontal_position = 'right';
                    }
                    if (Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_NOTIFICATION') == 'right') {
                        $type_horizontal_position = 'left';
                    }
                }
                $horizontal_position .= $type_horizontal_position . ':';
                if (Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_NOTIFICATION') == 'percent') {
                    $horizontal_position .= Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_NOTIFICATION') . '%';
                }
                if (Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_NOTIFICATION') == 'pixel') {
                    $horizontal_position .= Configuration::get('TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_NOTIFICATION') . 'px';
                }
                $width_notification = '';
                if (Configuration::get('TDKTOOLKIT_WIDTH_UNIT_NOTIFICATION') == 'percent') {
                    $width_notification .= Configuration::get('TDKTOOLKIT_WIDTH_NOTIFICATION') . '%';
                }
                if (Configuration::get('TDKTOOLKIT_WIDTH_UNIT_NOTIFICATION') == 'pixel') {
                    $width_notification .= Configuration::get('TDKTOOLKIT_WIDTH_NOTIFICATION') . 'px';
                }
                $templateVars = array(
                    'vertical_position' => $vertical_position,
                    'horizontal_position' => $horizontal_position,
                    'width_notification' => $width_notification,
                );
                $this->smarty->assign($templateVars);
                $output = $this->fetch('module:tdktoolkit/views/templates/front/notification.tpl');

                return $output;
            }
        }
    }

    //TDK:: render modal review popup
    public function renderModalReview($id_product, $is_logged)
    {
        $product = new Product((int) $id_product, false, $this->context->language->id, $this->context->shop->id);
        // echo '<pre>';
        // print_r($product);die();
        $image = Product::getCover((int) $id_product);
        $cover_image = $this->context->link->getImageLink($product->link_rewrite, $image['id_image'], ImageType::getFormattedName('medium'));
        // print_r($cover_image);die();
        $this->context->smarty->assign(array(
            'product_modal_review' => $product,
            'criterions' => ProductReviewCriterion::getByProduct((int) $id_product, $this->context->language->id),
            'secure_key' => $this->secure_key,
            'productcomment_cover_image' => $cover_image,
            'allow_guests' => (int) Configuration::get('TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_GUESTS'),
            'is_logged' => (int) $is_logged,
        ));

        $output = $this->fetch('module:tdktoolkit/views/templates/front/modal_review.tpl');

        return $output;
    }

    //TDK:: render modal popup
    public function renderPriceAttribute($product, $check_product_page)
    {
        if ($check_product_page == 1) {
            $lang_id = $this->context->language->id;
            // $shop_id = $this->context->shop->id;
            $product_obj =  new Product($product['id'], true, $lang_id);
            $curr = array();
            $fields = array('name', 'iso_code', 'iso_code_num', 'sign');
            foreach ($fields as $field_name) {
                $curr[$field_name] = $this->context->currency->{$field_name};
            }
            $priceDisplay = Product::getTaxCalculationMethod((int) $this->context->cookie->id_customer);
            // Pack management
            $pack_items = Pack::isPack($product_obj->id) ? Pack::getItemTable($product_obj->id, $lang_id, true) : array();
            $productPrice = 0;
            if (!$priceDisplay || $priceDisplay == 2) {
                $productPrice = $product_obj->getPrice(true, null, 6);
            } elseif ($priceDisplay == 1) {
                $productPrice = $product_obj->getPrice(false, null, 6);
            }
            $configuration = $this->getTemplateVarConfiguration();
            //$configuration['display_taxes_label'] = (Module::isEnabled('ps_legalcompliance') && (bool) Configuration::get('AEUC_LABEL_TAX_INC_EXC')) || $this->context->country->display_tax_label;
            $templateVars = array(
                'product' => $product,
                'currency' => $curr,
                'priceDisplay' => $priceDisplay,
                'displayUnitPrice' => (!empty($product_obj->unity) && $product_obj->unit_price_ratio > 0.000000) ? true : false,
                'displayPackPrice' => ($pack_items && $productPrice < $product_obj->getNoPackPrice()) ? true : false,
                'configuration' => $configuration,
            );
            $this->context->smarty->assign($templateVars);
            $output = $this->fetch('file:catalog/_partials/product-prices.tpl');
        } else {
            $templateVars = array(
                'product_price_attribute' => $product,
            );
            $this->context->smarty->assign($templateVars);
            $output = $this->fetch('module:tdktoolkit/views/templates/front/price_attribute.tpl');
        }
        return $output;
    }
	
	//TDK:: add clone function for 1.7.6
	public function getTemplateVarConfiguration()
    {
        $quantity_discount_price = Configuration::get('PS_DISPLAY_DISCOUNT_PRICE');

        return array(
            'display_taxes_label' => $this->getDisplayTaxesLabel(),
            'display_prices_tax_incl' => (bool) (new TaxConfiguration())->includeTaxes(),
            'taxes_enabled' => (bool) Configuration::get('PS_TAX'),
            'low_quantity_threshold' => (int) Configuration::get('PS_LAST_QTIES'),
            'is_b2b' => (bool) Configuration::get('PS_B2B_ENABLE'),
            'is_catalog' => (bool) Configuration::isCatalogMode(),
            'show_prices' => (bool) Configuration::showPrices(),
            'opt_in' => array(
                'partner' => (bool) Configuration::get('PS_CUSTOMER_OPTIN'),
            ),
            'quantity_discount' => array(
                'type' => ($quantity_discount_price) ? 'price' : 'discount',
                'label' => ($quantity_discount_price)
                    ? $this->getTranslator()->trans('Price', array(), 'Shop.Theme.Catalog')
                    : $this->getTranslator()->trans('Discount', array(), 'Shop.Theme.Catalog'),
            ),
            'voucher_enabled' => (int) CartRule::isFeatureActive(),
            'return_enabled' => (int) Configuration::get('PS_ORDER_RETURN'),
            'number_of_days_for_return' => (int) Configuration::get('PS_ORDER_RETURN_NB_DAYS'),
        );
    }

	//TDK:: add clone function for 1.7.6
    public function getDisplayTaxesLabel()
    {
        return (Module::isEnabled('ps_legalcompliance') && (bool) Configuration::get('AEUC_LABEL_TAX_INC_EXC')) || $this->context->country->display_tax_label;
    }
	
    //TDK:: render modal popup
    public function renderProductCoverThumbnails($product)
    {
        $templateVars = array(
            'product' => $product,
        );
        $this->context->smarty->assign($templateVars);
        $output = $this->fetch('file:catalog/_partials/product-cover-thumbnails.tpl');
        return $output;
    }

    //TDK:: get list attribute of product
    public function getAttributesResume($id_product, $id_lang, $attribute_value_separator = ' - ', $attribute_separator = ', ')
    {
        if (!Combination::isFeatureActive()) {
            return array();
        }

        $combinations = Db::getInstance()->executeS('SELECT pa.*, product_attribute_shop.*
				FROM `' . _DB_PREFIX_ . 'product_attribute` pa
				' . Shop::addSqlAssociation('product_attribute', 'pa') . '
				WHERE pa.`id_product` = ' . (int) $id_product . '
				GROUP BY pa.`id_product_attribute`');

        if (!$combinations) {
            return false;
        }

        $product_attributes = array();
        foreach ($combinations as $combination) {
            $product_attributes[] = (int) $combination['id_product_attribute'];
        }

        $lang = Db::getInstance()->executeS('SELECT pac.id_product_attribute, GROUP_CONCAT(agl.`name`, \'' . pSQL($attribute_value_separator) . '\',al.`name` ORDER BY agl.`id_attribute_group` SEPARATOR \'' . pSQL($attribute_separator) . '\') as attribute_designation
				FROM `' . _DB_PREFIX_ . 'product_attribute_combination` pac
				LEFT JOIN `' . _DB_PREFIX_ . 'attribute` a ON a.`id_attribute` = pac.`id_attribute`
				LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group` ag ON ag.`id_attribute_group` = a.`id_attribute_group`
				LEFT JOIN `' . _DB_PREFIX_ . 'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = ' . (int) $id_lang . ')
				LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group_lang` agl ON (ag.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = ' . (int) $id_lang . ')
				WHERE pac.id_product_attribute IN (' . implode(',', $product_attributes) . ')
				GROUP BY pac.id_product_attribute');

        foreach ($lang as $k => $row) {
            $combinations[$k]['attribute_designation'] = $row['attribute_designation'];
        }

        //Get quantity of each variations
        foreach ($combinations as $key => $row) {
            $cache_key = $row['id_product'] . '_' . $row['id_product_attribute'] . '_quantity';

            if (!Cache::isStored($cache_key)) {
                $result = StockAvailable::getQuantityAvailableByProduct($row['id_product'], $row['id_product_attribute']);
                Cache::store($cache_key, $result);
                $combinations[$key]['quantity'] = $result;
            } else {
                $combinations[$key]['quantity'] = Cache::retrieve($cache_key);
            }
        }

        return $combinations;
    }

    //TDK:: get list attribute of product inherit to attribute of its
    public function getProductAttributeWhitelist()
    {
        return array(
            "customizable",
            "available_for_order",
            "customization_required",
            "customizations",
            "allow_oosp",
        );
    }

    //TDK:: render dopdown cart
    public function renderDropDown($only_total, $only_empty)
    {
        // echo '<pre>';
        // print_r((new CartPresenter)->present($this->context->cart));die();
        $cart = (new CartPresenter)->present($this->context->cart);
        $drop_down_html = '';
        if ($cart['products_count'] > 0) {
            $templateVars = array(
                'only_total' => $only_total,
				'only_empty' => $only_empty,
                'cart' => $cart,
                'cart_url' => $this->context->link->getPageLink('cart', null, $this->context->language->id, array('action' => 'show'), false, null, true),
                'order_url' => $this->context->link->getPageLink('order'),
                'enable_update_quantity' => (int) Configuration::get('TDKTOOLKIT_ENABLE_UPDATE_QUANTITY_ALLCART'),
                'enable_button_quantity' => (int) Configuration::get('TDKTOOLKIT_ENABLE_BUTTON_QUANTITY_ALLCART'),
                'show_combination' => (int) Configuration::get('TDKTOOLKIT_SHOW_COMBINATION_ALLCART'),
                'show_customization' => (int) Configuration::get('TDKTOOLKIT_SHOW_CUSTOMIZATION_ALLCART'),
                // 'cart_count' => $cart['products_count'],
                'width_cart_item' => Configuration::get('TDKTOOLKIT_WIDTH_CARTITEM_ALLCART'),
                'height_cart_item' => Configuration::get('TDKTOOLKIT_HEIGHT_CARTITEM_ALLCART')
            );                    
        } else {
			$templateVars = array(
				'empty_cart' => 1,
				'only_empty' => $only_empty,	
			);
		}
		$lang_id = $this->context->language->id;
		$templateVars['free_shipping_text'] = Configuration::get('TDKTOOLKIT_FREESHIPPING_TEXT', $lang_id);
		$link_cta = '';
		if (Configuration::get('TDKTOOLKIT_CTA_LINK', $lang_id) == 'index') {
			$link_cta = $this->context->link->getPageLink('index', true);
		} else {
			$link_cta = Configuration::get('TDKTOOLKIT_CTA_LINK', $lang_id);
		}
		$templateVars['link_cta'] = $link_cta;
		
		$this->smarty->assign($templateVars);
		$drop_down_html = $this->fetch('module:tdktoolkit/views/templates/front/drop_down.tpl');
        return $drop_down_html;
    }

    /**
     * Common method
     * Resgister all hook for module
     */
    public function registerTDKHook()
    {
        $res = true;
        $res &= $this->registerHook('header');
        $res &= $this->registerHook('displayTdkCartButton');
        $res &= $this->registerHook('displayTdkCartQuantity');
        $res &= $this->registerHook('displayTdkCartAttribute');
        $res &= $this->registerHook('displayBackOfficeHeader');
        $res &= $this->registerHook('displayTdkProductReviewExtra');
        $res &= $this->registerHook('displayTdkProductTab');
        $res &= $this->registerHook('displayTdkProductTabContent');
        $res &= $this->registerHook('displayTdkProductListReview');
        $res &= $this->registerHook('displayTdkCompareButton');
        $res &= $this->registerHook('displayTdkWishlistButton');
        $res &= $this->registerHook('displayTdkProducReviewCompare');
        $res &= $this->registerHook('displayCustomerAccount');
        $res &= $this->registerHook('actionAdminControllerSetMedia');
        $res &= $this->registerHook('displayBeforeBodyClosingTag');
        $res &= $this->registerHook('displayAfterBodyOpeningTag');
        return $res;
    }

    /**
     * Common method
     * Unresgister all hook for module
     */
    public function unregisterTDKHook()
    {
        $res = true;
        $res &= $this->unregisterHook('header');
        $res &= $this->unregisterHook('displayTdkCartButton');
        $res &= $this->unregisterHook('displayTdkCartQuantity');
        $res &= $this->unregisterHook('displayTdkCartAttribute');
        $res &= $this->unregisterHook('displayBackOfficeHeader');
        $res &= $this->unregisterHook('displayTdkProductReviewExtra');
        $res &= $this->unregisterHook('displayTdkProductTab');
        $res &= $this->unregisterHook('displayTdkProductTabContent');
        $res &= $this->unregisterHook('displayTdkProductListReview');
        $res &= $this->unregisterHook('displayTdkCompareButton');
        $res &= $this->unregisterHook('displayTdkWishlistButton');
        $res &= $this->unregisterHook('displayTdkProducReviewCompare');
        $res &= $this->unregisterHook('displayCustomerAccount');
        $res &= $this->unregisterHook('actionAdminControllerSetMedia');
        $res &= $this->unregisterHook('displayBeforeBodyClosingTag');
        $res &= $this->unregisterHook('displayAfterBodyOpeningTag');
        return $res;
    }

    /**
     * Install Module Tabs
     */
    private function installModuleTab($title, $class_sfx = '', $parent = '')
    {
        $class = 'Admin' . Tools::ucfirst($this->name) . Tools::ucfirst($class_sfx);
        // @copy(_PS_MODULE_DIR_.$this->name.'/logo.gif', _PS_IMG_DIR_.'t/'.$class.'.gif');
        if ($parent == '') {
            # validate module
            $position = Tab::getCurrentTabId();
        } else {
            # validate module
            $position = Tab::getIdFromClassName($parent);
        }

        $tab1 = new Tab();
        $tab1->class_name = $class;
        $tab1->module = $this->name;
        $tab1->id_parent = (int) $position;
        $langs = Language::getLanguages(false);
        foreach ($langs as $l) {
            # validate module
            $tab1->name[$l['id_lang']] = $title;
        }
        $tab1->add(true, false);
    }

    /**
     * Uninstall tabs
     */
    private function uninstallModuleTab($class_sfx = '')
    {
        $tab_class = 'Admin' . Tools::ucfirst($this->name) . Tools::ucfirst($class_sfx);

        $id_tab = Tab::getIdFromClassName($tab_class);
        if ($id_tab != 0) {
            $tab = new Tab($id_tab);
            $tab->delete();
            return true;
        }
        return false;
    }

    /**
     * Creates tables
     */
    protected function createTables()
    {
        if ($this->_installTDKDataSample()) {
            return true;
        }
        $res = 1;
        include_once(dirname(__FILE__) . '/install/install.php');
        return $res;
    }

    public function deleteTables()
    {
        return Db::getInstance()->execute('
            DROP TABLE IF EXISTS
            `' . _DB_PREFIX_ . 'tdktoolkit_product_review`,
			`' . _DB_PREFIX_ . 'tdktoolkit_product_review_criterion`,
			`' . _DB_PREFIX_ . 'tdktoolkit_product_review_criterion_product`,
			`' . _DB_PREFIX_ . 'tdktoolkit_product_review_criterion_lang`,
			`' . _DB_PREFIX_ . 'tdktoolkit_product_review_criterion_category`,
			`' . _DB_PREFIX_ . 'tdktoolkit_product_review_grade`,
			`' . _DB_PREFIX_ . 'tdktoolkit_product_review_usefulness`,
			`' . _DB_PREFIX_ . 'tdktoolkit_product_review_report`,
			`' . _DB_PREFIX_ . 'tdktoolkit_compare`,
			`' . _DB_PREFIX_ . 'tdktoolkit_compare_product`,
			`' . _DB_PREFIX_ . 'tdktoolkit_wishlist`,
			`' . _DB_PREFIX_ . 'tdktoolkit_wishlist_product`
		');
    }

    //TDK:: create configs
    public function createConfiguration()
    {
        $langs = Language::getLanguages(false);
        $loggedin_greeting = array();
        $loggedout_greeting = array();
		$free_shipping_text = array();
		$link_cta = array();
        foreach ($langs as $l) {
            $loggedin_greeting[$l['id_lang']] = 'Hi! How can we help you?';
            $loggedout_greeting[$l['id_lang']] = 'Hi! We are here to answer any questions you may have.';
			$free_shipping_text[$l['id_lang']] = 'FREE SHIPPING FROM 100$';
            $link_cta[$l['id_lang']] = 'index';
        }
        $list_config_create = array(
            'TDKTOOLKIT_ENABLE_AJAXCART' => 1,
            'TDKTOOLKIT_ENABLE_SELECTATTRIBUTE' => 0,
            'TDKTOOLKIT_DISPLAYATTRIBUTE_TYPE' => 2,
            'TDKTOOLKIT_ENABLE_UPDATELABEL' => 0,
            'TDKTOOLKIT_ENABLE_INPUTQUANTITY' => 1,
            'TDKTOOLKIT_ENABLE_FLYCART_EFFECT' => 1,
            'TDKTOOLKIT_ENABLE_NOTIFICATION' => 0,
            'TDKTOOLKIT_SHOW_POPUP' => 1,
            'TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS' => 1,
            'TDKTOOLKIT_SHOW_PRODUCT_REVIEWS_LISTPRODUCT' => 1,
            'TDKTOOLKIT_SHOW_NUMBER_PRODUCT_REVIEWS_LISTPRODUCT' => 1,
            'TDKTOOLKIT_SHOW_ZERO_PRODUCT_REVIEWS_LISTPRODUCT' => 1,
            'TDKTOOLKIT_PRODUCT_REVIEWS_MINIMAL_TIME' => 30,
            'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_GUESTS' => 0,
            'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_USEFULL_BUTTON' => 1,
            'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_REPORT_BUTTON' => 1,
            'TDKTOOLKIT_PRODUCT_REVIEWS_MODERATE' => 1,
            'TDKTOOLKIT_ENABLE_PRODUCTCOMPARE' => 1,
            'TDKTOOLKIT_SHOW_PRODUCTCOMPARE_PRODUCTPAGE' => 1,
            'TDKTOOLKIT_SHOW_PRODUCTCOMPARE_LISTPRODUCT' => 1,
            'TDKTOOLKIT_COMPARATOR_MAX_ITEM' => 3,
            'TDKTOOLKIT_ENABLE_PRODUCTWISHLIST' => 1,
            'TDKTOOLKIT_SHOW_PRODUCTWISHLIST_PRODUCTPAGE' => 1,
            'TDKTOOLKIT_SHOW_PRODUCTWISHLIST_LISTPRODUCT' => 1,
            'TDKTOOLKIT_DEFAULT_TAB' => '#fieldset_0',
            'TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART' => 1,
            'TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART' => 'dropdown',
            // 'TDKTOOLKIT_ENABLE_PUSHEFFECT_DEFAULTCART' => 0,
            'TDKTOOLKIT_ENABLE_DROPDOWN_FLYCART' => 0,
            'TDKTOOLKIT_TYPE_DROPDOWN_FLYCART' => 'slidebar_bottom',
            // 'TDKTOOLKIT_ENABLE_PUSHEFFECT_FLYCART' => 0,
            'TDKTOOLKIT_TYPE_POSITION_FLYCART' => 'fixed',
            'TDKTOOLKIT_POSITION_VERTICAL_FLYCART' => 'bottom',
            'TDKTOOLKIT_POSITION_VERTICAL_VALUE_FLYCART' => 20,
            'TDKTOOLKIT_POSITION_VERTICAL_UNIT_FLYCART' => 'pixel',
            'TDKTOOLKIT_POSITION_HORIZONTAL_FLYCART' => 'left',
            'TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_FLYCART' => 20,
            'TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_FLYCART' => 'pixel',
            'TDKTOOLKIT_TYPE_EFFECT_FLYCART' => 'fade',
            'TDKTOOLKIT_ENABLE_OVERLAYBACKGROUND_ALLCART' => 0,
            'TDKTOOLKIT_ENABLE_UPDATE_QUANTITY_ALLCART' => 1,
            'TDKTOOLKIT_ENABLE_BUTTON_QUANTITY_ALLCART' => 1,
            'TDKTOOLKIT_SHOW_COMBINATION_ALLCART' => 1,
            'TDKTOOLKIT_SHOW_CUSTOMIZATION_ALLCART' => 1,
            'TDKTOOLKIT_WIDTH_CARTITEM_ALLCART' => 265,
            'TDKTOOLKIT_HEIGHT_CARTITEM_ALLCART' => 135,
            'TDKTOOLKIT_NUMBER_CARTITEM_DISPLAY_ALLCART' => 3,
            // 'TDKTOOLKIT_WIDTH_NOTIFICATION' => 320,
            'TDKTOOLKIT_WIDTH_NOTIFICATION' => 100,
            'TDKTOOLKIT_WIDTH_UNIT_NOTIFICATION' => 'percent',
            'TDKTOOLKIT_POSITION_VERTICAL_NOTIFICATION' => 'top',
            'TDKTOOLKIT_POSITION_VERTICAL_VALUE_NOTIFICATION' => 0,
            'TDKTOOLKIT_POSITION_VERTICAL_UNIT_NOTIFICATION' => 'pixel',
            'TDKTOOLKIT_POSITION_HORIZONTAL_NOTIFICATION' => 'left',
            'TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_NOTIFICATION' => 0,
            'TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_NOTIFICATION' => 'pixel',
            'TDKTOOLKIT_ENABLE_FBCHAT' => 1,
            'TDKTOOLKIT_FBCHAT_PAGEID' => 409421136509661,
            'TDKTOOLKIT_FBCHAT_LOGGEDIN_GREETING' => $loggedin_greeting,
            'TDKTOOLKIT_FBCHAT_LOGGEDOUT_GREETING' => $loggedout_greeting,
            'TDKTOOLKIT_FBCHAT_THEMECOLOR' => '#0084ff',
            'TDKTOOLKIT_FBCHAT_GREETINGDIALOG_DISPLAY' => 'fade',
            'TDKTOOLKIT_FBCHAT_GREETINGDIALOG_DELAY' => 5,
			'TDKTOOLKIT_FREESHIPPING_TEXT' => $free_shipping_text,
            'TDKTOOLKIT_CTA_LINK' => $link_cta,
			'TDKTOOLKIT_SHOW_SIDERBARCART' => 1
        );
        foreach ($list_config_create as $key => $value) {
            Configuration::updateValue($key, $value);
        }

        return true;
    }

    //TDK:: delete configs
    public function deleteConfiguration()
    {
        $list_config_delete = array(
            'TDKTOOLKIT_ENABLE_AJAXCART',
            'TDKTOOLKIT_ENABLE_SELECTATTRIBUTE',
            'TDKTOOLKIT_DISPLAYATTRIBUTE_TYPE',
            'TDKTOOLKIT_ENABLE_UPDATELABEL',
            'TDKTOOLKIT_ENABLE_INPUTQUANTITY',
            'TDKTOOLKIT_ENABLE_FLYCART_EFFECT',
            'TDKTOOLKIT_ENABLE_NOTIFICATION',
            'TDKTOOLKIT_SHOW_POPUP',
            'TDKTOOLKIT_ENABLE_PRODUCT_REVIEWS',
            'TDKTOOLKIT_SHOW_PRODUCT_REVIEWS_LISTPRODUCT',
            'TDKTOOLKIT_SHOW_NUMBER_PRODUCT_REVIEWS_LISTPRODUCT',
            'TDKTOOLKIT_SHOW_ZERO_PRODUCT_REVIEWS_LISTPRODUCT',
            'TDKTOOLKIT_PRODUCT_REVIEWS_MINIMAL_TIME',
            'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_GUESTS',
            'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_USEFULL_BUTTON',
            'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_REPORT_BUTTON',
            'TDKTOOLKIT_PRODUCT_REVIEWS_MODERATE',
            'TDKTOOLKIT_ENABLE_PRODUCTCOMPARE',
            'TDKTOOLKIT_SHOW_PRODUCTCOMPARE_PRODUCTPAGE',
            'TDKTOOLKIT_SHOW_PRODUCTCOMPARE_LISTPRODUCT',
            'TDKTOOLKIT_COMPARATOR_MAX_ITEM',
            'TDKTOOLKIT_ENABLE_PRODUCTWISHLIST',
            'TDKTOOLKIT_SHOW_PRODUCTWISHLIST_PRODUCTPAGE',
            'TDKTOOLKIT_SHOW_PRODUCTWISHLIST_LISTPRODUCT',
            'TDKTOOLKIT_DEFAULT_TAB',
            'TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART',
            'TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART',
            // 'TDKTOOLKIT_ENABLE_PUSHEFFECT_DEFAULTCART',
            'TDKTOOLKIT_ENABLE_DROPDOWN_FLYCART',
            'TDKTOOLKIT_TYPE_DROPDOWN_FLYCART',
            // 'TDKTOOLKIT_ENABLE_PUSHEFFECT_FLYCART',
            'TDKTOOLKIT_TYPE_POSITION_FLYCART',
            'TDKTOOLKIT_POSITION_VERTICAL_FLYCART',
            'TDKTOOLKIT_POSITION_VERTICAL_VALUE_FLYCART',
            'TDKTOOLKIT_POSITION_VERTICAL_UNIT_FLYCART',
            'TDKTOOLKIT_POSITION_HORIZONTAL_FLYCART',
            'TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_FLYCART',
            'TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_FLYCART',
            'TDKTOOLKIT_TYPE_EFFECT_FLYCART',
            'TDKTOOLKIT_ENABLE_OVERLAYBACKGROUND_ALLCART',
            'TDKTOOLKIT_ENABLE_UPDATE_QUANTITY_ALLCART',
            'TDKTOOLKIT_ENABLE_BUTTON_QUANTITY_ALLCART',
            'TDKTOOLKIT_SHOW_COMBINATION_ALLCART',
            'TDKTOOLKIT_SHOW_CUSTOMIZATION_ALLCART',
            'TDKTOOLKIT_WIDTH_CARTITEM_ALLCART',
            'TDKTOOLKIT_HEIGHT_CARTITEM_ALLCART',
            'TDKTOOLKIT_NUMBER_CARTITEM_DISPLAY_ALLCART',
            'TDKTOOLKIT_WIDTH_NOTIFICATION',
            'TDKTOOLKIT_WIDTH_UNIT_NOTIFICATION',
            'TDKTOOLKIT_POSITION_VERTICAL_NOTIFICATION',
            'TDKTOOLKIT_POSITION_VERTICAL_VALUE_NOTIFICATION',
            'TDKTOOLKIT_POSITION_VERTICAL_UNIT_NOTIFICATION',
            'TDKTOOLKIT_POSITION_HORIZONTAL_NOTIFICATION',
            'TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_NOTIFICATION',
            'TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_NOTIFICATION',
            'TDKTOOLKIT_ENABLE_FBCHAT',
            'TDKTOOLKIT_FBCHAT_PAGEID',
            'TDKTOOLKIT_FBCHAT_LOGGEDIN_GREETING',
            'TDKTOOLKIT_FBCHAT_LOGGEDOUT_GREETING',
            'TDKTOOLKIT_FBCHAT_THEMECOLOR',
            'TDKTOOLKIT_FBCHAT_GREETINGDIALOG_DISPLAY',
            'TDKTOOLKIT_FBCHAT_GREETINGDIALOG_DELAY',
			'TDKTOOLKIT_FREESHIPPING_TEXT',
			'TDKTOOLKIT_CTA_LINK',
			'TDKTOOLKIT_SHOW_SIDERBARCART',
        );
        foreach ($list_config_delete as $list_config_delete_val) {
            Configuration::deleteByName($list_config_delete_val);
        }
        return true;
    }

    private function _installTDKDataSample()
    {
        if (!file_exists(_PS_MODULE_DIR_ . 'tdkplatform/libs/TDKDataSample.php')) {
            return false;
        }
        require_once(_PS_MODULE_DIR_ . 'tdkplatform/libs/TDKDataSample.php');

        $sample = new TDKDataSample(1);
        if ($sample->processImport($this->name)) {
			//TDK:: fix for case can not install sample when install theme (can not get theme name to find directory folder)
			Configuration::updateValue('TDK_INSTALLED_SAMPLE_TDKTOOLKIT', 1);
			return true;
		} else {
			return false;
		}
    }

    public function _clearCache($template, $cache_id = null, $compile_id = null)
    {
        // validate module
        unset($cache_id);
        unset($compile_id);
        parent::_clearCache($template);
    }

    public function correctModule()
    {

        if (Configuration::hasKey('TDKTOOLKIT_ENABLE_PUSHEFFECT_DEFAULTCART')) {
            Configuration::updateValue('TDKTOOLKIT_ENABLE_PUSHEFFECT_DEFAULTCART', 0);
        }
        if (Configuration::hasKey('TDKTOOLKIT_ENABLE_PUSHEFFECT_FLYCART')) {
            Configuration::updateValue('TDKTOOLKIT_ENABLE_PUSHEFFECT_FLYCART', 0);
        }
        $list_config_check = array(
            'TDKTOOLKIT_WIDTH_CARTITEM_ALLCART' => 265,
            'TDKTOOLKIT_HEIGHT_CARTITEM_ALLCART' => 135,
            'TDKTOOLKIT_NUMBER_CARTITEM_DISPLAY_ALLCART' => 3,
            'TDKTOOLKIT_ENABLE_UPDATE_QUANTITY_ALLCART' => 1,
            'TDKTOOLKIT_ENABLE_BUTTON_QUANTITY_ALLCART' => 1,
            'TDKTOOLKIT_SHOW_COMBINATION_ALLCART' => 1,
            'TDKTOOLKIT_SHOW_CUSTOMIZATION_ALLCART' => 1,
            'TDKTOOLKIT_WIDTH_NOTIFICATION' => 320,
            'TDKTOOLKIT_WIDTH_UNIT_NOTIFICATION' => 'pixel',
            'TDKTOOLKIT_POSITION_VERTICAL_NOTIFICATION' => 'top',
            'TDKTOOLKIT_POSITION_VERTICAL_VALUE_NOTIFICATION' => 50,
            'TDKTOOLKIT_POSITION_VERTICAL_UNIT_NOTIFICATION' => 'pixel',
            'TDKTOOLKIT_POSITION_HORIZONTAL_NOTIFICATION' => 'right',
            'TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_NOTIFICATION' => 20,
            'TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_NOTIFICATION' => 'pixel',
            'TDKTOOLKIT_SHOW_POPUP' => 1,
            'TDKTOOLKIT_ENABLE_NOTIFICATION' => 0,
            'TDKTOOLKIT_ENABLE_OVERLAYBACKGROUND_ALLCART' => 0,
            'TDKTOOLKIT_TYPE_POSITION_FLYCART' => 'fixed',
            'TDKTOOLKIT_POSITION_VERTICAL_FLYCART' => 'bottom',
            'TDKTOOLKIT_POSITION_VERTICAL_VALUE_FLYCART' => 20,
            'TDKTOOLKIT_POSITION_VERTICAL_UNIT_FLYCART' => 'pixel',
            'TDKTOOLKIT_POSITION_HORIZONTAL_FLYCART' => 'left',
            'TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_FLYCART' => 20,
            'TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_FLYCART' => 'pixel',
            // 'TDKTOOLKIT_ENABLE_PUSHEFFECT_FLYCART' => 0,
            'TDKTOOLKIT_TYPE_DROPDOWN_FLYCART' => 'slidebar_bottom',
            'TDKTOOLKIT_ENABLE_DROPDOWN_FLYCART' => 0,
            // 'TDKTOOLKIT_ENABLE_PUSHEFFECT_DEFAULTCART' => 0,
            'TDKTOOLKIT_TYPE_DROPDOWN_DEFAULTCART' => 'dropdown',
            'TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART' => 1,
            'TDKTOOLKIT_SHOW_ZERO_PRODUCT_REVIEWS_LISTPRODUCT' => 1,
            'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_USEFULL_BUTTON' => 1,
            'TDKTOOLKIT_PRODUCT_REVIEWS_ALLOW_REPORT_BUTTON' => 1,
            'TDKTOOLKIT_ENABLE_UPDATELABEL' => 0,
            'TDKTOOLKIT_ENABLE_FLYCART_EFFECT' => 1,
            'TDKTOOLKIT_TYPE_EFFECT_FLYCART' => 'fade',
        );
        foreach ($list_config_check as $key => $value) {
            if (!Configuration::hasKey($key)) {
                Configuration::updateValue($key, $value);
            }
        };

        //TDK:: add hook for attribute of product
        $this->registerHook('displayTdkCartAttribute');

        //TDK:: add hook for quantity of product
        $this->registerHook('displayTdkCartQuantity');

        //TDK:: add id field to product_review_grade table
        $sql = 'SHOW FIELDS FROM `' . _DB_PREFIX_ . 'tdktoolkit_product_review_grade` LIKE "id_product_review_grade"';
        $column = Db::getInstance()->executeS($sql);

        if (empty($column)) {
            Db::getInstance()->execute('ALTER TABLE ' . _DB_PREFIX_ . 'tdktoolkit_product_review_grade DROP PRIMARY KEY');
            Db::getInstance()->execute('ALTER TABLE ' . _DB_PREFIX_ . 'tdktoolkit_product_review_grade ADD id_product_review_grade INT AUTO_INCREMENT PRIMARY KEY FIRST');
        }

        //TDK:: add hook for fly cart
        $this->registerHook('displayBeforeBodyClosingTag');
    }
    /**
     * FIX BUG 1.7.3.3 : install theme lose hook displayHome, displayTdkProfileProduct
     * because ajax not run hookActionAdminBefore();
     */
    public function autoRestoreSampleData()
    {
        if (Hook::isModuleRegisteredOnHook($this, 'actionAdminBefore', (int)Context::getContext()->shop->id)) {
            $theme_manager = new stdclass();
            $theme_manager->theme_manager = 'theme_manager';
            $this->hookActionAdminBefore(array(
                'controller' => $theme_manager,
            ));
        }
    }

    /**
     * Run only one when install/change Theme_of_Tdk Studio
     */
    public function hookActionAdminBefore($params)
    {
        $this->unregisterHook('actionAdminBefore');
        if (isset($params) && isset($params['controller']) && isset($params['controller']->theme_manager)) {
            // Validate : call hook from theme_manager
        } else {
            // Other module call this hook -> duplicate data
            return;
        }


        # FIX : update Prestashop by 1-Click module -> NOT NEED RESTORE DATABASE
        $tdk_version = Configuration::get('TDK_CURRENT_VERSION');
        if ($tdk_version != false) {
            $ps_version = Configuration::get('PS_VERSION_DB');
            $versionCompare = version_compare($tdk_version, $ps_version);
            if ($versionCompare != 0) {
                // Just update Prestashop
                Configuration::updateValue('TDK_CURRENT_VERSION', $ps_version);
                return;
            }
        }


        # WHENE INSTALL THEME, INSERT HOOK FROM DATASAMPLE IN THEME
        $hook_from_theme = false;
        if (file_exists(_PS_MODULE_DIR_ . 'tdkplatform/libs/TDKDataSample.php')) {
            require_once(_PS_MODULE_DIR_ . 'tdkplatform/libs/TDKDataSample.php');
            $sample = new TDKDatasample();
            if ($sample->processHook($this->name)) {
                $hook_from_theme = true;
            };
        }

        # INSERT HOOK FROM MODULE_DATASAMPLE
        if ($hook_from_theme == false) {
            $this->registerTDKHook();
        }

        # WHEN INSTALL MODULE, NOT NEED RESTORE DATABASE IN THEME
        $install_module = (int) Configuration::get('TDK_INSTALLED_TDKTOOLKIT', 0);
        if ($install_module) {
            Configuration::updateValue('TDK_INSTALLED_TDKTOOLKIT', '0');    // next : allow restore sample
            //TDK:: fix for case can not install sample when install theme (can not get theme name to find directory folder)
			if ((int)Configuration::get('TDK_INSTALLED_SAMPLE_TDKTOOLKIT', 0)) {				
				return;
			}
        }

        # INSERT DATABASE FROM THEME_DATASAMPLE
        if (file_exists(_PS_MODULE_DIR_ . 'tdkplatform/libs/TDKDataSample.php')) {
            require_once(_PS_MODULE_DIR_ . 'tdkplatform/libs/TDKDataSample.php');
            $sample = new TDKDatasample();
            $sample->processImport($this->name);
        }
    }

    protected function postValidation()
    {
        if (Tools::isSubmit('submitTdktoolkitConfig')) {
            if (!Validate::isUnsignedInt(Tools::getValue('TDKTOOLKIT_COMPARATOR_MAX_ITEM'))) {
                $this->_postErrors[] = $this->l('"Number product comparison" is invalid. Must an integer validity (unsigned).');
            }
            if (!Validate::isUnsignedInt(Tools::getValue('TDKTOOLKIT_PRODUCT_REVIEWS_MINIMAL_TIME'))) {
                $this->_postErrors[] = $this->l('"Minimum time between 2 reviews from the same user" is invalid. Must an integer validity (unsigned).');
            }
            if (!Validate::isUnsignedInt(Tools::getValue('TDKTOOLKIT_POSITION_VERTICAL_VALUE_FLYCART'))) {
                $this->_postErrors[] = $this->l('"Position By Vertical Value (Fly Cart)" is invalid. Must an integer validity (unsigned).');
            } else {
                if (Tools::getValue('TDKTOOLKIT_POSITION_VERTICAL_UNIT_FLYCART') == 'percent' && Tools::getValue('TDKTOOLKIT_POSITION_VERTICAL_VALUE_FLYCART') > 100) {
                    $this->_postErrors[] = $this->l('"Position By Vertical Value (Fly Cart)" is invalid. Must an integer validity (unsigned). Must has value from 0 to 100 with the unit type is percent');
                }
            }
            if (!Validate::isUnsignedInt(Tools::getValue('TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_FLYCART'))) {
                $this->_postErrors[] = $this->l('"Position By Horizontal Value (Fly Cart)" is invalid. Must an integer validity (unsigned).');
            } else {
                if (Tools::getValue('TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_FLYCART') == 'percent' && Tools::getValue('TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_FLYCART') > 100) {
                    $this->_postErrors[] = $this->l('"Position By Horizontal Value (Fly Cart)" is invalid. Must an integer validity (unsigned). Must has value from 0 to 100 with the unit type is percent');
                }
            }
            if (!Validate::isUnsignedInt(Tools::getValue('TDKTOOLKIT_WIDTH_CARTITEM_ALLCART'))) {
                $this->_postErrors[] = $this->l('"Width Of Cart Item (All Cart)" is invalid. Must an integer validity (unsigned).');
            }
            if (!Validate::isUnsignedInt(Tools::getValue('TDKTOOLKIT_HEIGHT_CARTITEM_ALLCART'))) {
                $this->_postErrors[] = $this->l('"Height Of Cart Item (All Cart)" is invalid. Must an integer validity (unsigned).');
            }
            if (!Validate::isUnsignedInt(Tools::getValue('TDKTOOLKIT_NUMBER_CARTITEM_DISPLAY_ALLCART'))) {
                $this->_postErrors[] = $this->l('"Number Cart Item To Display (All Cart)" is invalid. Must an integer validity (unsigned).');
            }
            if (!Validate::isUnsignedInt(Tools::getValue('TDKTOOLKIT_WIDTH_NOTIFICATION'))) {
                $this->_postErrors[] = $this->l('"Width (Notification)" is invalid. Must an integer validity (unsigned).');
            } else {
                if (Tools::getValue('TDKTOOLKIT_WIDTH_UNIT_NOTIFICATION') == 'percent' && Tools::getValue('TDKTOOLKIT_WIDTH_NOTIFICATION') > 100) {
                    $this->_postErrors[] = $this->l('"Width (Notification)" is invalid. Must an integer validity (unsigned). Must has value from 0 to 100 with the unit width is percent');
                }
            }

            if (!Validate::isUnsignedInt(Tools::getValue('TDKTOOLKIT_POSITION_VERTICAL_VALUE_NOTIFICATION'))) {
                $this->_postErrors[] = $this->l('"Position By Vertical Value (Notification)" is invalid. Must an integer validity (unsigned).');
            } else {
                if (Tools::getValue('TDKTOOLKIT_POSITION_VERTICAL_UNIT_NOTIFICATION') == 'percent' && Tools::getValue('TDKTOOLKIT_POSITION_VERTICAL_VALUE_NOTIFICATION') > 100) {
                    $this->_postErrors[] = $this->l('"Position By Vertical Value (Notification)" is invalid. Must an integer validity (unsigned). Must has value from 0 to 100 with the unit type is percent');
                }
            }
            if (!Validate::isUnsignedInt(Tools::getValue('TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_NOTIFICATION'))) {
                $this->_postErrors[] = $this->l('"Position By Horizontal Value (Notification)" is invalid. Must an integer validity (unsigned).');
            } else {
                if (Tools::getValue('TDKTOOLKIT_POSITION_HORIZONTAL_UNIT_NOTIFICATION') == 'percent' && Tools::getValue('TDKTOOLKIT_POSITION_HORIZONTAL_VALUE_NOTIFICATION') > 100) {
                    $this->_postErrors[] = $this->l('"Position By Horizontal Value (Notification)" is invalid. Must an integer validity (unsigned). Must has value from 0 to 100 with the unit type is percent');
                }
            }
        }
    }

    //TDK:: check token
    public function isTokenValid()
    {
        if (!Configuration::get('PS_TOKEN_ENABLE')) {
            return true;
        }
        return strcasecmp(Tools::getToken(false), Tools::getValue('token')) == 0;
    }
    //TDK:: setup for facebook chat
    public function hookDisplayAfterBodyOpeningTag($params)
    {
        if (Configuration::get('TDKTOOLKIT_ENABLE_FBCHAT')) {
			//TDK:: do not load library if fb login enable
			$fb_login_enable = false;
			$lang_locale = '';
			if (Configuration::get('TDKQUICKLOGIN_SOCIALLOGIN_ENABLE') && Configuration::get('TDKQUICKLOGIN_FACEBOOK_ENABLE') && Configuration::get('TDKQUICKLOGIN_FACEBOOK_APPID') != '' && !$this->context->customer->isLogged())
			{
				$fb_login_enable = true;
			}
			else
			{
				$lang_locale = $this->context->language->locale;
				if ($lang_locale != '') {
					if (strpos($lang_locale, 'ar-') !== false) {
						$lang_locale = 'ar_AR';
					} else if (strpos($lang_locale, 'es-') !== false) {
						$lang_locale = 'es_ES';
					} else {
						$lang_locale = str_replace('-', '_', $lang_locale);
					}
				} else {
					$lang_locale = 'en_US';
				}
			}
            $lang_id = $this->context->language->id;
            $this->context->smarty->assign(array(
                'fb_page_id' => Configuration::get('TDKTOOLKIT_FBCHAT_PAGEID'),
                'lang_locale' => $lang_locale,
                'loggedin_greeting' => Configuration::get('TDKTOOLKIT_FBCHAT_LOGGEDIN_GREETING', $lang_id),
                'loggedout_greeting' => Configuration::get('TDKTOOLKIT_FBCHAT_LOGGEDOUT_GREETING', $lang_id),
                'theme_color' => Configuration::get('TDKTOOLKIT_FBCHAT_THEMECOLOR'),
                'greetingdialog_display' => Configuration::get('TDKTOOLKIT_FBCHAT_GREETINGDIALOG_DISPLAY'),
                'greetingdialog_delay' => Configuration::get('TDKTOOLKIT_FBCHAT_GREETINGDIALOG_DELAY'),
				'fb_login_enable' => $fb_login_enable,
            ));
            $output = $this->fetch('module:tdktoolkit/views/templates/front/fbchat.tpl');

            return $output;
        }
    }
    //TDK:: get id_product_attribute by one or more attributes (clone core)
    public function getIdProductAttributeByIdAttributes($idProduct, $idAttributes, $findBest = false)
    {
        $idProduct = (int) $idProduct;

        if (!is_array($idAttributes) && is_numeric($idAttributes)) {
            $idAttributes = array((int) $idAttributes);
        }

        if (!is_array($idAttributes) || empty($idAttributes)) {
            throw new PrestaShopException(sprintf('Invalid parameter $idAttributes with value: "%s"', print_r($idAttributes, true)));
        }
        $idAttributesImploded = implode(',', array_map('intval', $idAttributes));
        $idProductAttribute =  Db::getInstance()->getValue('
            SELECT 
                pac.`id_product_attribute`
            FROM 
                `' . _DB_PREFIX_ . 'product_attribute_combination` pac
                INNER JOIN `' . _DB_PREFIX_ . 'product_attribute` pa ON pa.id_product_attribute = pac.id_product_attribute
            WHERE 
                pa.id_product = ' . $idProduct . '
                AND pac.id_attribute IN (' . $idAttributesImploded . ')
            GROUP BY 
                pac.`id_product_attribute`
            HAVING 
                COUNT(pa.id_product) = ' . count($idAttributes));

        if ($idProductAttribute === false && $findBest) {
            //find the best possible combination
            //first we order $idAttributes by the group position
            $orderred = array();
            $result = Db::getInstance()->executeS('
                SELECT 
                    a.`id_attribute`
                FROM 
                    `'._DB_PREFIX_.'attribute` a
                    INNER JOIN `'._DB_PREFIX_.'attribute_group` g ON a.`id_attribute_group` = g.`id_attribute_group`
                WHERE 
                    a.`id_attribute` IN (' . $idAttributesImploded . ')
                ORDER BY 
                    g.`position` ASC');

            foreach ($result as $row) {
                $orderred[] = $row['id_attribute'];
            }

            while ($idProductAttribute === false && count($orderred) > 0) {
                array_pop($orderred);
                $idProductAttribute =  Db::getInstance()->getValue('
                    SELECT 
                        pac.`id_product_attribute`
                    FROM 
                        `'._DB_PREFIX_.'product_attribute_combination` pac
                        INNER JOIN `'._DB_PREFIX_.'product_attribute` pa ON pa.id_product_attribute = pac.id_product_attribute
                    WHERE 
                        pa.id_product = '.(int)$idProduct.'
                        AND pac.id_attribute IN ('.implode(',', array_map('intval', $orderred)).')
                    GROUP BY 
                        pac.id_product_attribute
                    HAVING 
                        COUNT(pa.id_product) = '.count($orderred));
            }
        }
        return $idProductAttribute;
    }
    //TDK:: get combination by id_product_attribute (clone core)
    // public function getAttributeCombinationsById($id_product, $id_product_attribute, $id_lang, $groupByIdAttributeGroup = true)
    // {
        // if (!Combination::isFeatureActive()) {
             // return array();
        // }
        // $combinations = Db::getInstance()->getRow('SELECT pa.*, product_attribute_shop.*
                // FROM `' . _DB_PREFIX_ . 'product_attribute` pa
                // ' . Shop::addSqlAssociation('product_attribute', 'pa') . '
                // WHERE pa.`id_product` = ' . (int) $id_product . ' AND pa.`id_product_attribute` = ' . (int) $id_product_attribute . '
                // GROUP BY pa.`id_product_attribute`');
        // if (!$combinations) {
            // return false;
        // }
        // //Get quantity of each variations
        // $cache_key = $combinations['id_product'] . '_' . $combinations['id_product_attribute'] . '_quantity';
        // if (!Cache::isStored($cache_key)) {
            // $result = StockAvailable::getQuantityAvailableByProduct($combinations['id_product'], $combinations['id_product_attribute']);
            // Cache::store($cache_key, $result);
            // $combinations['quantity'] = $result;
        // } else {
            // $combinations['quantity'] = Cache::retrieve($cache_key);
        // }
        // return $combinations;
    // }
    //TDK:: render html list attribute after change attribute
    public function renderHtmlListAttribute($list_attribute, $id_product, $display_type, $lang_id, $page = '')
    {
        $list_attr_group = $this->getListAttributeGroup($id_product, $lang_id, $list_attribute, $display_type);
        $templateVars = array(
            'list_attr_group' => $list_attr_group,
            'display_type' => $display_type,
            'id_product' => $id_product,
            'tdk_page' => $page,
        );
        $this->context->smarty->assign($templateVars);
        $output = $this->fetch('module:tdktoolkit/views/templates/hook/tdk_cart_button_attribute.tpl');
        return $output;
    }
    public function autoUpdateTDKModule()
    {
        //TDK:: add fb chat
        if (!Configuration::hasKey('TDKTOOLKIT_CURRENT_VERSION') || (Configuration::hasKey('TDKTOOLKIT_CURRENT_VERSION') && version_compare(Configuration::get('TDKTOOLKIT_CURRENT_VERSION'), '1.1.0', '<'))) {
			// echo '<pre>';
			// print_r(Configuration::get('TDKTOOLKIT_CURRENT_VERSION'));
			// var_dump(version_compare(Configuration::get('TDKTOOLKIT_CURRENT_VERSION'), '1.1.0', '<')); die();
            $this->registerHook('displayAfterBodyOpeningTag');
            $langs = Language::getLanguages(false);
            $loggedin_greeting = array();
            $loggedout_greeting = array();
            foreach ($langs as $l) {
                $loggedin_greeting[$l['id_lang']] = 'Hi! How can we help you?';
                $loggedout_greeting[$l['id_lang']] = 'Hi! We are here to answer any questions you may have.';
            }
            $list_config_check = array(
                'TDKTOOLKIT_ENABLE_FBCHAT' => 1,
                'TDKTOOLKIT_FBCHAT_PAGEID' => 409421136509661,
                'TDKTOOLKIT_FBCHAT_LOGGEDIN_GREETING' => $loggedin_greeting,
                'TDKTOOLKIT_FBCHAT_LOGGEDOUT_GREETING' => $loggedout_greeting,
                'TDKTOOLKIT_FBCHAT_THEMECOLOR' => '#0084ff',
                'TDKTOOLKIT_FBCHAT_GREETINGDIALOG_DISPLAY' => 'fade',
                'TDKTOOLKIT_FBCHAT_GREETINGDIALOG_DELAY' => 5
            );
            foreach ($list_config_check as $key => $value) {
                if (!Configuration::hasKey($key)) {
                    Configuration::updateValue($key, $value);
                }
            };
            Module::upgradeModuleVersion($this->name, '1.1.0');
            Configuration::updateValue('TDKTOOLKIT_CURRENT_VERSION', '1.1.0');
        }
        if (!Configuration::hasKey('TDKTOOLKIT_CURRENT_VERSION') || (Configuration::hasKey('TDKTOOLKIT_CURRENT_VERSION') && version_compare(Configuration::get('TDKTOOLKIT_CURRENT_VERSION'), '1.2.0', '<'))) {
            $list_config_check = array(
                'TDKTOOLKIT_DISPLAYATTRIBUTE_TYPE' => 2
            );
            foreach ($list_config_check as $key => $value) {
                if (!Configuration::hasKey($key)) {
                    Configuration::updateValue($key, $value);
                }
            };
            Module::upgradeModuleVersion($this->name, '1.2.0');
            Configuration::updateValue('TDKTOOLKIT_CURRENT_VERSION', '1.2.0');
        }
		
		//TDK:: update module 1.3.0, add cart empty and update cart
		if (!Configuration::hasKey('TDKTOOLKIT_CURRENT_VERSION') || (Configuration::hasKey('TDKTOOLKIT_CURRENT_VERSION') && version_compare(Configuration::get('TDKTOOLKIT_CURRENT_VERSION'), '1.3.0', '<'))) {
            $langs = Language::getLanguages(false);
            $free_shipping_text = array();
            $link_cta = array();
            foreach ($langs as $l) {
                $free_shipping_text[$l['id_lang']] = 'FREE SHIPPING FROM 100$';
				$link_cta[$l['id_lang']] = 'index';
            }
            $list_config_check = array(
                'TDKTOOLKIT_FREESHIPPING_TEXT' => $free_shipping_text,
                'TDKTOOLKIT_CTA_LINK' => $link_cta,                
            );
            foreach ($list_config_check as $key => $value) {
                if (!Configuration::hasKey($key)) {
                    Configuration::updateValue($key, $value);
                }
            };
            Module::upgradeModuleVersion($this->name, '1.3.0');
            Configuration::updateValue('TDKTOOLKIT_CURRENT_VERSION', '1.3.0');
        }
			
		//TDK:: update module 1.4.0, add config
		if (!Configuration::hasKey('TDKTOOLKIT_CURRENT_VERSION') || (Configuration::hasKey('TDKTOOLKIT_CURRENT_VERSION') && version_compare(Configuration::get('TDKTOOLKIT_CURRENT_VERSION'), '1.4.0', '<'))) {			
            $list_config_check = array(
                'TDKTOOLKIT_SHOW_SIDERBARCART' => 1,                           
            );
            foreach ($list_config_check as $key => $value) {
                if (!Configuration::hasKey($key)) {
                    Configuration::updateValue($key, $value);
                }
            };
            Module::upgradeModuleVersion($this->name, '1.4.0');
            Configuration::updateValue('TDKTOOLKIT_CURRENT_VERSION', '1.4.0');
        }
    }
}
