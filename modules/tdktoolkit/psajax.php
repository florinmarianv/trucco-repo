<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');

include_once(dirname(__FILE__).'/tdktoolkit.php');
include_once(dirname(__FILE__).'/classes/TdktoolkitProduct.php');

$module = new Tdktoolkit();

if ((!$module->isTokenValid() || !Tools::getValue('action')) && Tools::getValue('action') != 'get-new-review') {
    // Ooops! Token is not valid!
    // die('Token is not valid, hack stop');
    $result = '';
    die($result);
}

//TDK:: render modal popup and dropdown cart
if (Tools::getValue('action') == 'render-modal') {
    $context = Context::getContext();
    $modal = '';
    $notification = '';
    $dropdown = '';
    if (Tools::getValue('only_dropdown') == 0) {
        $modal = $module->renderModal();
        $notification = $module->renderNotification();
    }
    if (Configuration::get('TDKTOOLKIT_ENABLE_DROPDOWN_DEFAULTCART') || Configuration::get('TDKTOOLKIT_ENABLE_DROPDOWN_FLYCART')) {
        $only_total = Tools::getValue('only_total');
		$only_empty = Tools::getValue('only_empty');
        $dropdown = $module->renderDropDown($only_total, $only_empty);
    }

    ob_end_clean();
    header('Content-Type: application/json');
    die(Tools::jsonEncode(array(
        'dropdown' => $dropdown,
        'modal'   => $modal,
        'notification' => $notification,
    )));
}

if (Tools::getValue('action') == 'get-attribute-data') {
    $type = Tools::getValue('type');
    $result = array();
    $return = array();
    $context = Context::getContext();
    $lang_id = $context->language->id;
    $id_product = Tools::getValue('id_product');
    $check_product_page = Tools::getValue('check_product_page');
    if ($type == 1) {
        $id_product_attribute = Tools::getValue('id_product_attribute');
    }
    if ($type == 2 || $type == 3 || $type == 4 || $type == 5 || $type == 6) {
        $id_product = Tools::getValue('id_product');
        // $product_ojb = new Product($id_product);
        $groups = Tools::getValue('group');
        $id_product_attribute = (int) $module->getIdProductAttributeByIdAttributes($id_product, $groups);
        if (!$id_product_attribute) {
            $id_attribute = Tools::getValue('id_attribute');
            $id_product_attribute = (int) $module->getIdProductAttributeByIdAttributes($id_product, $id_attribute, true);
        }
        // $combination = $module->getAttributeCombinationsById($id_product, $id_product_attribute, $context->language->id, false);
        // $whitelist = $module->getProductAttributeWhitelist();
        // foreach ($whitelist as $v_whitelist) {
            // if (isset($product_ojb->$v_whitelist)) {
                // $combination[$v_whitelist] = $product_ojb->$v_whitelist;
            // }
        // }
        // if ($module->shouldEnableAddToCartButton($combination)) {
            // $combination['add_to_cart_url'] = $module->getAddToCartURL($combination);
        // } else {
            // $combination['add_to_cart_url'] = null;
        // }
    }
    $attribute_data = new TdktoolkitProduct();
    $result = $attribute_data->getTemplateVarProduct2($id_product, $id_product_attribute);
    if ($type == 2 || $type == 3 || $type == 4 || $type == 5 || $type == 6) {
        $return['combination_quantity'] = $result['quantity'];
        $return['combination_minimal_quantity'] = $result['minimal_quantity'];
        $return['add_to_cart_url'] = $result['add_to_cart_url'];
        $return['id_product_attribute'] = $id_product_attribute;
        $list_attribute = array();
        $list_attribute['attributes'] = $result['attributes'];
        $page = '';
        if ($check_product_page == 1) {
            $page = 'product';
        }
        $return['html_list_attribute'] = $module->renderHtmlListAttribute($list_attribute, $id_product, $type, $lang_id, $page);
    }
    $return['product_cover'] = $result['cover'];
    $return['price_attribute']  = $module->renderPriceAttribute($result, $check_product_page);
    // $return['price_attribute']  = $attribute_data->render('catalog/_partials/product-prices')
    // $return['product_url'] = $context->link->getProductLink($id_product, null, null, null, $context->language->id, null, $id_product_attribute, false, false, true);
    $return['product_url'] = $result['url'];
    if ($check_product_page == 1) {
        $return['product_cover_thumbnails'] = $module->renderProductCoverThumbnails($result);
        // $return['product_cover_thumbnails'] =  $attribute_data->render('catalog/_partials/product-cover-thumbnails'),
    }
    die(Tools::jsonEncode($return));
}

if (Tools::getValue('action') == 'get-new-review') {
    // $result = array();
    if (Configuration::get('TDKTOOLKIT_PRODUCT_REVIEWS_MODERATE')) {
        $reviews = ProductReview::getByValidate(0, false);
    } else {
        $reviews = array();
    }

    die(Tools::jsonEncode(array(
        'number_review' => count($reviews)
    )));
}

if (Tools::getValue('action') == 'check-product-outstock') {
    $id_product = Tools::getValue('id_product');
    $id_product_attribute = Tools::getValue('id_product_attribute');
    $id_customization = Tools::getValue('id_customization');
    $check_product_in_cart = Tools::getValue('check_product_in_cart');
    $quantity = Tools::getValue('quantity');
    $context = Context::getContext();
    $qty_to_check = $quantity;
    // print_r('test111');
    if ($check_product_in_cart == 'true') {
        $cart_products = $context->cart->getProducts();

        if (is_array($cart_products)) {
            foreach ($cart_products as $cart_product) {
                if ((!isset($id_product_attribute) || ($cart_product['id_product_attribute'] == $id_product_attribute && $cart_product['id_customization'] == $id_customization )) && isset($id_product) && $cart_product['id_product'] == $id_product) {
                    $qty_to_check = $cart_product['cart_quantity'];
                    $qty_to_check += $quantity;
                    break;
                }
            }
        }
    }

    $product = new Product($id_product, true, $context->language->id);
    $return = true;
    // Check product quantity availability
    if ($id_product_attribute) {
        if (!Product::isAvailableWhenOutOfStock($product->out_of_stock) && !Attribute::checkAttributeQty($id_product_attribute, $qty_to_check)) {
            $return = false;
        }
    } elseif (!$product->checkQty($qty_to_check)) {
        $return = false;
    }

    die(Tools::jsonEncode(array(
        'success' => $return,
    )));
}
