{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div data-type="{$type_fly_cart}" style="position: {$type_position}; {$vertical_position}; {$horizontal_position}" class="tdk-fly-cart solo type-{$type_position}{if $type_fly_cart == 'dropup' || $type_fly_cart == 'dropdown'} enable-dropdown{/if}{if $type_fly_cart == 'slidebar_top' || $type_fly_cart == 'slidebar_bottom' || $type_fly_cart == 'slidebar_right' || $type_fly_cart == 'slidebar_left'} enable-slidebar{/if}">
	<div class="tdk-fly-cart-icon-wrapper">
		<a href="javascript:void(0)" class="tdk-fly-cart-icon" data-type="{$type_fly_cart}"><i class="material-icons">&#xE8CC;</i></a>
		<span class="tdk-fly-cart-total"></span>
	</div>
	{*
	<div class="tdk-fly-cssload-speeding-wheel"></div>
	*}
	<div class="tdk-fly-cart-cssload-loader"></div>
</div>