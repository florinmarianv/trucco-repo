{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
{if $enable_overlay_background}
	<div class="tdk-fly-cart-mask"></div>
{/if}

<div class="tdk-fly-cart-slidebar {$type}">
	
	<div class="tdk-fly-cart disable-dropdown">
		<div class="tdk-fly-cart-wrapper">
			<div class="tdk-fly-cart-icon-wrapper">
				<a href="javascript:void(0)" class="tdk-fly-cart-icon"><i class="material-icons">&#xE8CC;</i></a>
				
			</div>
			{*
			<div class="tdk-fly-cssload-speeding-wheel"></div>		
			<div class="tdk-fly-cart-cssload-loader"></div>
			*}
		</div>
	</div>
	<div class="tdk-fly-cart-header">
		<div class="tdk-fly-cart-header-numcart">
			<span>{l s='My cart' mod='tdktoolkit'}</span><span class="tdk-fly-cart-total"></span>
		</div>
		<div class="tdk-fly-cart-header-total">
			<span class="tdk-fly-cart-header-total-label"></span><span class="tdk-fly-cart-header-total-value"></span>
		</div>
	</div>
</div>