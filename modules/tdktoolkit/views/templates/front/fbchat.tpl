{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{if $fb_page_id != ''}
	{if !$fb_login_enable}
		{literal}
		<script>
			window.fbAsyncInit = function() {
				FB.init({
					xfbml: true,
					version: 'v3.2'
				});
			};
			// Load the SDK asynchronously	
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "https://connect.facebook.net/{/literal}{$lang_locale}{literal}/sdk/xfbml.customerchat.js";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));

		</script>
		{/literal}
	{/if}
	<div class="fb-customerchat"
		page_id="{$fb_page_id}" 
		theme_color="{$theme_color}" 
		logged_in_greeting="{$loggedin_greeting}" 
		loggedout_greeting="{$loggedout_greeting}" 
		greeting_dialog_display="{$greetingdialog_display}" 
		greeting_dialog_delay="{$greetingdialog_delay}">
	</div>
{/if}

