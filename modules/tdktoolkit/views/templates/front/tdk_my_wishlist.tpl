{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
{extends file=$layout}

{block name='content'}
	<section id="main">
		<div id="mywishlist">
			<h2>{l s='New wishlist' mod='tdktoolkit'}</h2>
			<div class="new-wishlist">
				<div class="form-group">
					<label for="wishlist_name">{l s='Name' mod='tdktoolkit'}</label>
					<input type="text" class="form-control" id="wishlist_name" placeholder="{l s='Enter name of new wishlist' mod='tdktoolkit'}">
				</div>
				<div class="form-group has-success">
					<div class="form-control-feedback"></div>			 
				</div>
				<div class="form-group has-danger">		 
				  <div class="form-control-feedback"></div>		 
				</div>
				<button type="submit" class="btn btn-primary tdk-save-wishlist-bt">
					<span class="tdk-save-wishlist-loading cssload-speeding-wheel"></span>
					<span class="tdk-save-wishlist-bt-text">
						{l s='Save' mod='tdktoolkit'}
					</span>
				</button>
			</div>
			
				<div class="list-wishlist">
					<table class="table table-striped">
					  <thead class="wishlist-table-head">
						<tr>
						  <th>{l s='Name' mod='tdktoolkit'}</th>
						  <th>{l s='Quantity' mod='tdktoolkit'}</th>
						  <th>{l s='Viewed' mod='tdktoolkit'}</th>
						  <th class="wishlist-datecreate-head">{l s='Created' mod='tdktoolkit'}</th>
						  <th>{l s='Direct Link' mod='tdktoolkit'}</th>
						  <th>{l s='Default' mod='tdktoolkit'}</th>
						  <th>{l s='Delete' mod='tdktoolkit'}</th>
						</tr>
					  </thead>
					  <tbody>
						{if $wishlists}
							{foreach from=$wishlists item=wishlists_item name=for_wishlists}
								<tr>					 
									<td><a href="javascript:void(0)" class="view-wishlist-product" data-name-wishlist="{$wishlists_item.name}" data-id-wishlist="{$wishlists_item.id_wishlist}"><i class="material-icons">&#xE8EF;</i>{$wishlists_item.name}</a><div class="tdk-view-wishlist-product-loading tdk-view-wishlist-product-loading-{$wishlists_item.id_wishlist} cssload-speeding-wheel"></div></td>
									<td class="wishlist-numberproduct wishlist-numberproduct-{$wishlists_item.id_wishlist}">{$wishlists_item.number_product|intval}</td>
									<td>{$wishlists_item.counter|intval}</td>
									<td class="wishlist-datecreate">{$wishlists_item.date_add}</td>							
									<td><a class="view-wishlist" data-token="{$wishlists_item.token}" target="_blank" href="{$view_wishlist_url}{if $tdk_is_rewrite_active}?{else}&{/if}token={$wishlists_item.token}" title="{l s='View' mod='tdktoolkit'}">{l s='View' mod='tdktoolkit'}</a></td>
									<td>
										
											<label class="form-check-label">
												<input class="default-wishlist form-check-input" data-id-wishlist="{$wishlists_item.id_wishlist}" type="checkbox" {if $wishlists_item.default == 1}checked="checked"{/if}>
											</label>
									
									</td>
									<td><a class="delete-wishlist" data-id-wishlist="{$wishlists_item.id_wishlist}" href="javascript:void(0)" title="{l s='Delete' mod='tdktoolkit'}"><i class="material-icons">&#xE872;</i></a></td>
								</tr>
							{/foreach}
						{/if}
					  </tbody>
					</table>
				</div>
			<div class="send-wishlist">
				<a class="tdk-send-wishlist-button btn btn-info" href="javascript:void(0)" title="{l s='Send this wishlist' mod='tdktoolkit'}">
					<i class="material-icons">&#xE163;</i>
					{l s='Send this wishlist' mod='tdktoolkit'}
				</a>
			</div>
			<section id="products">
				<div class="tdk-wishlist-product products row">
				
				</div>
			</section>
			<ul class="footer_links">
				<li class="pull-xs-left"><a class="btn btn-outline" href="{$link->getPageLink('my-account', true)|escape:'html'}"><i class="material-icons">&#xE317;</i>{l s='Back to Your Account' mod='tdktoolkit'}</a></li>
				<li class="pull-xs-right"><a class="btn btn-outline" href="{$urls.base_url}"><i class="material-icons">&#xE88A;</i>{l s='Home' mod='tdktoolkit'}</a></li>
			</ul>
		</div>
	</section>
{/block}

