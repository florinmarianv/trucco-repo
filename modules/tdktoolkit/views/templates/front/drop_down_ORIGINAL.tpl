{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
{if $only_empty == 1}
	<div class="tdk-cart-empty">
		<p class="tdk-cart-empty-icon">
			<span>
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 511.997 511.997" style="enable-background:new 0 0 511.997 511.997;" xml:space="preserve">
					<path d="M405.387,362.612c-35.202,0-63.84,28.639-63.84,63.84s28.639,63.84,63.84,63.84s63.84-28.639,63.84-63.84    S440.588,362.612,405.387,362.612z M405.387,451.988c-14.083,0-25.536-11.453-25.536-25.536s11.453-25.536,25.536-25.536    c14.083,0,25.536,11.453,25.536,25.536S419.47,451.988,405.387,451.988z"/>
					<path d="M507.927,115.875c-3.626-4.641-9.187-7.348-15.079-7.348H118.22l-17.237-72.12c-2.062-8.618-9.768-14.702-18.629-14.702    H19.152C8.574,21.704,0,30.278,0,40.856s8.574,19.152,19.152,19.152h48.085l62.244,260.443    c2.062,8.625,9.768,14.702,18.629,14.702h298.135c8.804,0,16.477-6.001,18.59-14.543l46.604-188.329    C512.849,126.562,511.553,120.516,507.927,115.875z M431.261,296.85H163.227l-35.853-150.019h341.003L431.261,296.85z"/>
					<path d="M173.646,362.612c-35.202,0-63.84,28.639-63.84,63.84s28.639,63.84,63.84,63.84s63.84-28.639,63.84-63.84    S208.847,362.612,173.646,362.612z M173.646,451.988c-14.083,0-25.536-11.453-25.536-25.536s11.453-25.536,25.536-25.536    s25.536,11.453,25.536,25.536S187.729,451.988,173.646,451.988z"/>
				</svg>
			</span>
        </p>
		<h2 class="tdk-cart-empty-title">{l s='Nothing was found' mod='tdktoolkit'}</h2>
		<p class="tdk-cart-empty-cta-txt">{l s='It seem like nothing\'s worse than \"sold out\", so act fast before it\'s too late' mod='tdktoolkit'}</p>
		{if $link_cta != ''}<a class="tdk-cart-empty-bt btn btn-primary btn-outline" href="{$link_cta}">{l s='Shopping now' mod='tdktoolkit'}</a>{/if}
		{if $free_shipping_text != ''}<p class="tdk-cart-empty-freeship-txt">{$free_shipping_text}</p>{/if}
	</div>
{/if}
{if !isset($empty_cart)}
	{if $only_total != 1}
		<div class="tdk-dropdown-cart-content clearfix">
			<div class="tdk-dropdown-list-item-warpper">
				<ul class="tdk-dropdown-list-item">{foreach from=$cart.products item=product name="cart_product"}<li data-width-item="{$width_cart_item}" data-height-item="{$height_cart_item}" style="width: {$width_cart_item}px; height: {$height_cart_item}px" class="tdk-dropdown-cart-item clearfix{if ($product.attributes|count && $show_combination) || ($product.customizations|count && $show_customization)} has-view-additional{/if}{if $smarty.foreach.cart_product.first} first{/if}{if $smarty.foreach.cart_product.last} last{/if}">
							<div class="tdk-cart-item-wrapper" style="width: {$width_cart_item}px;">
								<div class="tdk-cart-item-img">
									{if $product.images}
										<a class="label" href="{$product.url}" title="{$product.name}"><img class="img-fluid" src="{$product.images.0.bySize.small_default.url}" alt="{$product.name}" title="{$product.name}"/></a>
									{/if}	
								</div>						
								<div class="tdk-cart-item-info">					
									<div class="product-name"><a class="label" href="{$product.url}" title="{$product.name}">{$product.name|truncate:18:'...'}</a></div>
									{if $enable_update_quantity}
										<div class="product-quantity">												
											{if $enable_button_quantity}
												<a href="javascript:void(0)" class="tdk-bt-product-quantity tdk-bt-product-quantity-down"><i class="material-icons">&#xE15B;</i></a>
											{/if}
											<input
												class="tdk-input-product-quantity input-group"
												data-down-url="{$product.down_quantity_url}"
												data-up-url="{$product.up_quantity_url}"
												data-update-url="{$product.update_quantity_url}"
												data-id-product = "{$product.id_product|escape:'javascript'}"
												data-id-product-attribute = "{$product.id_product_attribute|escape:'javascript'}"
												data-id-customization = "{$product.id_customization|escape:'javascript'}"
												data-min-quantity="{$product.minimal_quantity}"
												data-product-quantity="{$product.quantity}"
												data-quantity-available="{$product.quantity_available}"									
												type="text"
												value="{$product.quantity}"								
												min="{$product.minimal_quantity}"
											  />
											{if $enable_button_quantity}	
												<a href="javascript:void(0)" class="tdk-bt-product-quantity tdk-bt-product-quantity-up"><i class="material-icons">&#xE145;</i></a>
											{/if}
										</div>
									{else}
										<div class="product-quantity"><span class="lablel">{l s='Quantity' mod='tdktoolkit'}</span>: {$product.quantity}</div>
									{/if}
									<div class="product-price">
										{if $product.has_discount}
											<div class="product-discount">
											  <span class="regular-price">{$product.regular_price}</span>
											  
											  {if $product.discount_type === 'percentage'}
												<span class="discount discount-percentage">
													-{$product.discount_percentage_absolute}
												  </span>
											  {else}
												<span class="discount discount-amount">
													-{$product.discount_to_display}
												  </span>
											  {/if}
											  
											</div>
										  {/if}
										  <div class="current-price">
											<span class="price">{$product.price}</span>
										  </div>
											{if $product.unit_price_full}
											  <div class="unit-price-cart">{$product.unit_price_full}</div>
											{/if}
									</div>
									
									<div class="tdk-dropdown-additional">
										{if $product.attributes|count && $show_combination}
											{*
											<div class="view-combination label">
												
											</div>
											*}
											<div class="combinations">
												{foreach from=$product.attributes key="attribute" item="value"}
													  <div class="product-line-info">
														<span class="label">{$attribute}:</span>
														<span class="value">{$value}</span>
													  </div>
												{/foreach}
											</div>
										{/if}
										{if $product.customizations|count && $show_customization}
											{*
											<div class="view-customization label">
												
											</div>
											*}
											<div class="customizations">								
												{foreach from=$product.customizations item='customization'}			
													
													<ul>
														{foreach from=$customization.fields item='field'}
															<li>
																<span class="label">{$field.label}</span>
																{if $field.type == 'text'}
																	: <span class="value">{$field.text nofilter}{* HTML form , no escape necessary *}</span>
																{else if $field.type == 'image'}
																	<img src="{$field.image.small.url}">
																{/if}
															</li>
														{/foreach}
													</ul>								
												{/foreach}								
											</div>
										{/if}
									</div>
																
								</div>
								<a class="tdk-remove-from-cart"					
									href="javascript:void(0)"					
									title="{l s='Remove from cart' mod='tdktoolkit'}" 
									data-link-url="{$product.remove_from_cart_url}"
									data-id-product = "{$product.id_product|escape:'javascript'}"
									data-id-product-attribute = "{$product.id_product_attribute|escape:'javascript'}"
									data-id-customization = "{$product.id_customization|escape:'javascript'}"
								>
									<i class="material-icons">&#xE872;</i>
								</a>
								{if ($product.attributes|count && $show_combination) || ($product.customizations|count && $show_customization)}
									<div class="view-additional">								
										<div class="view-tdk-dropdown-additional">
											<span class="show-info">{l s='Info' mod='tdktoolkit'}</span>
											<span class="hide-info">{l s='Hide' mod='tdktoolkit'}</span>
										</div>
									</div>
								{/if}
								
								
							</div>																														
							<div class="tdk-dropdown-overlay">
								{*
									<div class="tdk-dropdown-cssload-speeding-wheel"></div>
								*}
							</div>
						</li>{/foreach}</ul>
			</div>
			<div class="tdk-dropdown-bottom">
			{/if}
				<div class="tdk-dropdown-total" data-cart-total="{$cart.products_count}" data-cart-total-label="{$cart.totals.total_including_tax.label}" data-cart-total-value="{$cart.totals.total_including_tax.value}">				
					<div class="tdk-dropdown-cart-total clearfix">
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
								<span class="label">{$cart.totals.total_including_tax.label}</span>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
								<span class="value">{$cart.totals.total_including_tax.value}</span>
							</div>
						</div>
					</div>
				</div>
			{if $only_total != 1}
				<div class="tdk-cart-dropdown-action clearfix" style="width: {$width_cart_item}px;">
					<a class="cart-dropdow-button cart-dropdow-viewcart btn btn-primary btn-outline" href="{$cart_url}">{l s='View Cart' mod='tdktoolkit'}</a>
					<a class="cart-dropdow-button cart-dropdow-checkout btn btn-primary btn-outline" href="{$order_url}">{l s='Check Out' mod='tdktoolkit'}</a>
					{if $free_shipping_text != ''}<p class="tdk-cart-empty-freeship-txt">{$free_shipping_text}</p>{/if}
				</div>
			</div>
		</div>
	{/if}
{/if}