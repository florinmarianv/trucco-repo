{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
{if $product_price_attribute.show_price}
	{if $product_price_attribute.has_discount}
		{hook h='displayProductPriceBlock' product=$product_price_attribute type="old_price"}
		<span class="sr-only">{l s='Regular price' mod='tdktoolkit'}</span>
		<span class="regular-price">{$product_price_attribute.regular_price}</span>
		{if $product_price_attribute.discount_type === 'percentage'}
			<span class="discount-percentage discount-product">{$product_price_attribute.discount_percentage}</span>
		{elseif $product_price_attribute.discount_type === 'amount'}
			<span class="discount-amount discount-product">{$product_price_attribute.discount_amount_to_display}</span>
		{/if}
	{/if}

	{hook h='displayProductPriceBlock' product=$product_price_attribute type="before_price"}

	<span class="sr-only">{l s='Price' mod='tdktoolkit'}</span>
	<span itemprop="price" class="price">{$product_price_attribute.price}</span>

	{hook h='displayProductPriceBlock' product=$product_price_attribute type='unit_price'}

	{hook h='displayProductPriceBlock' product=$product_price_attribute type='weight'}
{/if}