{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="tdk-notification" style="width: {$width_notification}; {$vertical_position}; {$horizontal_position}">
</div>
<div class="tdk-temp tdk-temp-success">
	<div class="notification-wrapper">
		<div class="notification notification-success">
			{*
			<span class="notification-title"><i class="material-icons">&#xE876;</i></span> 
			*}
			<strong class="noti product-name"></strong>
			<span class="noti noti-update">{l s='The product has been updated in your shopping cart' mod='tdktoolkit'}</span>
			<span class="noti noti-delete">{l s='The product has been removed from your shopping cart' mod='tdktoolkit'}</span>
			<span class="noti noti-add"><strong class="noti-special"></strong> {l s='Product successfully added to your shopping cart' mod='tdktoolkit'}</span>
			<span class="notification-close">X</span>
			
		</div>
	</div>
</div>
<div class="tdk-temp tdk-temp-error">
	<div class="notification-wrapper">
		<div class="notification notification-error">
			{*
			<span class="notification-title"><i class="material-icons">&#xE611;</i></span>
			*}
			
			<span class="noti noti-update">{l s='Error updating' mod='tdktoolkit'}</span>
			<span class="noti noti-delete">{l s='Error deleting' mod='tdktoolkit'}</span>
			<span class="noti noti-add">{l s='Error adding. Please go to product detail page and try again' mod='tdktoolkit'}</span>
			
			<span class="notification-close">X</span>
			
		</div>
	</div>
</div>
<div class="tdk-temp tdk-temp-warning">
	<div class="notification-wrapper">
		<div class="notification notification-warning">
			{*
			<span class="notification-title"><i class="material-icons">&#xE645;</i></span> 
			*}
			<span class="noti noti-min">{l s='The minimum purchase order quantity for the product is' mod='tdktoolkit'} <strong class="noti-special"></strong></span>
			<span class="noti noti-max">{l s='There are not enough products in stock' mod='tdktoolkit'}</span>
			
			<span class="notification-close">X</span>
			
		</div>
	</div>
</div>
<div class="tdk-temp tdk-temp-normal">
	<div class="notification-wrapper">
		<div class="notification notification-normal">
			{*
			<span class="notification-title"><i class="material-icons">&#xE88F;</i></span>
			*}
			<span class="noti noti-check">{l s='You must enter a quantity' mod='tdktoolkit'}</span>
			
			<span class="notification-close">X</span>
			
		</div>
	</div>
</div>
