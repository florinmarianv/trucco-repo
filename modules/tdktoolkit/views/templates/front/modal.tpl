{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="modal tdk-modal tdk-modal-cart fade" tabindex="-1" role="dialog" aria-hidden="true">
	<!--
	<div class="vertical-alignment-helper">
	-->
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title h6 text-xs-center tdk-warning tdk-alert">
				<i class="material-icons">info_outline</i>				
				{l s='You must enter a quantity' mod='tdktoolkit'}		
			</h4>
			
			<h4 class="modal-title h6 text-xs-center tdk-info tdk-alert">
				<i class="material-icons">info_outline</i>				
				{l s='The minimum purchase order quantity for the product is ' mod='tdktoolkit'}<strong class="alert-min-qty"></strong>		
			</h4>	
			
			<h4 class="modal-title h6 text-xs-center tdk-block tdk-alert">				
				<i class="material-icons">block</i>				
				{l s='There are not enough products in stock' mod='tdktoolkit'}
			</h4>
		  </div>
		  <!--
		  <div class="modal-body">
			...
		  </div>
		  <div class="modal-footer">
			
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary">Save changes</button>
			
		  </div>
		  -->
		</div>
	  </div>
	<!--
	</div>
	-->
</div>