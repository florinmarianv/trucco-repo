{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
{if $products && count($products) >0}
	{foreach from=$products item=product_item name=for_products}
		{assign var='product' value=$product_item.product_info}
		{assign var='wishlist' value=$product_item.wishlist_info}
		<div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-xs-12 product-miniature js-product-miniature tdk-wishlistproduct-item tdk-wishlistproduct-item-{$wishlist.id_wishlist_product} product-{$product.id_product}" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
			<div class="delete-wishlist-product clearfix">
				<a class="tdk-wishlist-button-delete btn" href="javascript:void(0)" title="{l s='Remove from this wishlist' mod='tdktoolkit'}" data-id-wishlist="{$wishlist.id_wishlist}" data-id-wishlist-product="{$wishlist.id_wishlist_product}" data-id-product="{$product.id_product}"><i class="material-icons">&#xE872;</i>
				</a>
			</div>
			<div class="thumbnail-container clearfix">

				{block name='product_thumbnail'}
				  <a href="{$product.url}" class="thumbnail product-thumbnail">
					<img class="img-fluid"
					  src = "{$product.cover.bySize.home_default.url}"
					  alt = "{$product.cover.legend}"
					  data-full-size-image-url = "{$product.cover.large.url}"
					>
				  </a>
				{/block}

				<div class="product-description">							
				  {block name='product_name'}
					<h1 class="h3 product-title" itemprop="name"><a href="{$product.url}">{$product.name|truncate:30:'...'}</a></h1>
				  {/block}
				  
				</div>			
			</div>
			<div class="wishlist-product-info">
				<div class="form-group">
					<label>{l s='Quantity' mod='tdktoolkit'}</label>
					<input class="form-control wishlist-product-quantity wishlist-product-quantity-{$wishlist.id_wishlist_product}" type="number" min=1 value="{$wishlist.quantity}">					
				</div>
				<div class="form-group">
					<label>{l s='Priority' mod='tdktoolkit'}</label>
					<select class="form-control wishlist-product-priority wishlist-product-priority-{$wishlist.id_wishlist_product}">					  
						{for $i=0 to 2}
							<option value="{$i}"{if $i == $wishlist.priority} selected="selected"{/if}>								
								{if $i == 0}{l s='High' mod='tdktoolkit'}{/if}
								{if $i == 1}{l s='Medium' mod='tdktoolkit'}{/if}
								{if $i == 2}{l s='Low' mod='tdktoolkit'}{/if}								
							</option>
						{/for}
					</select>
				  </div>
			</div>		
			<div class="wishlist-product-action">
				<a class="tdk-wishlist-product-save-button btn btn-primary" href="javascript:void(0)" title="{l s='Save' mod='tdktoolkit'}" data-id-wishlist="{$wishlist.id_wishlist}" data-id-wishlist-product="{$wishlist.id_wishlist_product}" data-id-product="{$product.id_product}">{l s='Save' mod='tdktoolkit'}
				</a>
				{if isset($wishlists) && count($wishlists) > 0}					
					<div class="dropdown tdk-wishlist-button-dropdown">					 
					  <button class="tdk-wishlist-button dropdown-toggle btn btn-primary show-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{l s='Move' mod='tdktoolkit'}</button>
					  <div class="dropdown-menu tdk-list-wishlist tdk-list-wishlist-{$product.id_product}">				
						{foreach from=$wishlists item=wishlists_item}							
							<a href="#" class="dropdown-item list-group-item list-group-item-action move-wishlist-item" data-id-wishlist="{$wishlists_item.id_wishlist}" data-id-wishlist-product="{$wishlist.id_wishlist_product}" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" title="{$wishlists_item.name}">{$wishlists_item.name}</a>			
						{/foreach}
					  </div>
					</div>
				{/if}
			</div>
		</div>
	{/foreach}
{else}
	<div class="col-xl-12"><p class="alert alert-warning">{l s='No products' mod='tdktoolkit'}</p></div>
{/if}

