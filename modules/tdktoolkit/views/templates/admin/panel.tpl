{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
{*
<div class="panel form-horizontal">
	<div class="form-group">					
		<div class="col-lg-1">
			<a class="megamenu-correct-module btn btn-success" href="{$url_admin}&success=correct&correctmodule=1">
				<i class="icon-AdminParentPreferences"></i>{l s='Correct module' mod='tdktoolkit'}
			</a>
		</div>
		<label class="control-label col-lg-3">* {l s='Please backup the database before run correct module to safe' mod='tdktoolkit'}</label>							
	</div>
</div>
*}
<div id="tdktoolkit-group">

	<div class="panel panel-default">
		<div class="panel-heading"><i class="icon-cogs"></i> {l s='TDK Toolkit Global Config' mod='tdktoolkit'}</div>
		
		<div class="panel-content" id="tdktoolkit-setting">
			<ul class="nav nav-tabs tdktoolkit-tablist" role="tablist">
				<li class="nav-item{if $default_tab == '#fieldset_0'} active{/if}">
					<a class="nav-link" href="#fieldset_0" role="tab" data-toggle="tab">{l s='Ajax Cart' mod='tdktoolkit'}</a>
				</li>
				<li class="nav-item{if $default_tab == '#fieldset_1_1'} active{/if}">
					<a class="nav-link" href="#fieldset_1_1" role="tab" data-toggle="tab">{l s='Product Review' mod='tdktoolkit'}</a>
				</li>
				<li class="nav-item{if $default_tab == '#fieldset_2_2'} active{/if}">
					<a class="nav-link" href="#fieldset_2_2" role="tab" data-toggle="tab">{l s='Product Compare' mod='tdktoolkit'}</a>
				</li>
				<li class="nav-item{if $default_tab == '#fieldset_3_3'} active{/if}">
					<a class="nav-link" href="#fieldset_3_3" role="tab" data-toggle="tab">{l s='Product Wishlist' mod='tdktoolkit'}</a>
				</li>
				<li class="nav-item{if $default_tab == '#fieldset_4_4'} active{/if}">
					<a class="nav-link" href="#fieldset_4_4" role="tab" data-toggle="tab">{l s='Facebook Chat' mod='tdktoolkit'}</a>
				</li>
			</ul>
			<div class="tab-content">
				{$globalform}{* HTML form , no escape necessary *}
			</div>
		</div>	

	</div>
		
</div>