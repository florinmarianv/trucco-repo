{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="mywishlist-link" href="{$wishlist_link}">
	<span class="link-item">
		<i class="material-icons">&#xE87D;</i>
		{l s='My Wishlist' mod='tdktoolkit'}
	</span>
</a>

