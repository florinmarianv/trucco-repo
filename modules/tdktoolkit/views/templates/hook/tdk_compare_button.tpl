{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="compare">
	<a class="tdk-compare-button btn-primary TdkBtnProduct btn{if $added} added{/if}" href="javascript:void(0)" data-id-product="{$tdk_compare_id_product}" title="{if $added}{l s='Remove from Compare' mod='tdktoolkit'}{else}{l s='Add to Compare' mod='tdktoolkit'}{/if}">
		<span class="tdk-compare-bt-loading cssload-speeding-wheel"></span>
		<span class="tdk-compare-bt-content">
			<i class="TdkIconBtnProduct icon-compare material-icons">&#xE915;</i>
			<span class="TdkNameBtnProduct">{l s='Add to Compare' mod='tdktoolkit'}</span>
		</span>
	</a>
</div>