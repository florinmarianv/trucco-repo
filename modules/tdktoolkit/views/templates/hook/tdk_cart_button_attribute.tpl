{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{if $display_type == 1}
	{if isset($tdk_cart_product_attribute.combinations) && count($tdk_cart_product_attribute.combinations) > 0}		
		<div class="dropdown type-1 tdk-pro-attr-section">
		  <button class="btn btn-secondary dropdown-toggle tdk-bt-select-attr dropdownListAttrButton_{$tdk_cart_product_attribute.id_product}" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			{$tdk_cart_product_attribute.attribute_designation}
		  </button>
		  <div class="dropdown-menu tdk-dropdown-attr">
			{foreach from=$tdk_cart_product_attribute.combinations item=attribute}
				<a class="dropdown-item tdk-select-attr{if $attribute.id_product_attribute == $tdk_cart_product_attribute.id_product_attribute} selected{/if}{if $attribute.add_to_cart_url == ''} disable{/if}" href="javascript:void(0)" data-id-product="{$attribute.id_product}" data-id-attr="{$attribute.id_product_attribute}" data-qty-attr="{$attribute.quantity}" data-min-qty-attr="{$attribute.minimal_quantity}">{$attribute.attribute_designation}</a>
			{/foreach}
			
		  </div>
		</div>
	{/if}
{/if}

{if $display_type == 2}
	{if isset($list_attr_group) && count($list_attr_group) > 0}	
		{foreach from=$list_attr_group key=id_attribute_group item=list_attr_group_item}
			{if !empty($list_attr_group_item.attributes)}
				<div class="dropdown type-{$display_type} tdk-pro-attr-section group-type-{$list_attr_group_item.group_type}{if count($list_attr_group) == 1} full-width{/if}">
					{if isset($tdk_page) && $tdk_page == 'product'}
						<div class="tdk-attr-info">
							<span class="tdk-attr-group-name">{$list_attr_group_item.name}</span>: <span class="tdk-attr-value-name" data-current-attribute-name="{$list_attr_group_item.attributes[$list_attr_group_item.selected].name}">{$list_attr_group_item.attributes[$list_attr_group_item.selected].name}</span>
						</div>
					{/if}
					<div class="btn btn-secondary dropdown-toggle tdk-bt-select-attr dropdownListAttrButton_{$id_product}_{$id_attribute_group}{if count($list_attr_group_item.attributes) <= 1} disable-list{/if}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="attribute-label">{$list_attr_group_item.attributes[$list_attr_group_item.selected].name}</span>
						{if $list_attr_group_item.group_type == 'color'}
							{if $list_attr_group_item.attributes[$list_attr_group_item.selected].texture != ''}
								<img class="attribute-display attribute-texture active" src="{$list_attr_group_item.attributes[$list_attr_group_item.selected].texture}"></img>
							{else}
								<span class="attribute-display attribute-color active" style="background:{$list_attr_group_item.attributes[$list_attr_group_item.selected].html_color_code};"></span>
							{/if}
						{else}
							<span class="attribute-name">{$list_attr_group_item.attributes[$list_attr_group_item.selected].name}</span>
						{/if}
					</div>
					{if count($list_attr_group_item.attributes) > 1}
						<div class="dropdown-menu tdk-dropdown-attr">
							{foreach from=$list_attr_group_item.attributes key=id_attribute item=attribute}
								<a class="dropdown-item tdk-select-attr{if $id_attribute == $list_attr_group_item.selected} selected{/if}" data-display-type="{$display_type}" data-group-type="{$list_attr_group_item.group_type}"{if $list_attr_group_item.group_type == 'color'} data-color-value="{$attribute.html_color_code}"{/if} data-id-attribute-group={$id_attribute_group} data-id-product="{$id_product}" data-id-attribute="{$id_attribute}" data-attribute-name="{$attribute.name}" href="javascript:void(0)">
									<span class="attribute-label">{$attribute.name}</span>
									{if $list_attr_group_item.group_type == 'color'}
										{if $attribute.texture != ''}
											<img class="attribute-texture active" src="{$attribute.texture}"></img>
										{else}
											<span class="attribute-color active" style="background:{$attribute.html_color_code}"></span>
										{/if}
									{else}
										<span>{$attribute.name}</span>
									{/if}
								</a>
							{/foreach}
						
						</div>
					{/if}
				</div>
			{/if}
		{/foreach}
	{/if}
{/if}

{if $display_type == 3 || $display_type == 4 || $display_type == 5 || $display_type == 6}
	{if isset($list_attr_group) && count($list_attr_group) > 0}	
		{foreach from=$list_attr_group key=id_attribute_group item=list_attr_group_item}
			{if !empty($list_attr_group_item.attributes)}
				<div class="type-{$display_type} tdk-pro-attr-section{if ($display_type == 5 || $display_type == 6 ) && $list_attr_group_item.group_type == 'color'} show-img{/if} group-type-{$list_attr_group_item.group_type}{if count($list_attr_group) == 1} full-width{/if}">
					{if isset($tdk_page) && $tdk_page == 'product'}
						<div class="tdk-attr-info">
							<span class="tdk-attr-group-name">{$list_attr_group_item.name}</span>: <span class="tdk-attr-value-name" data-current-attribute-name="{$list_attr_group_item.attributes[$list_attr_group_item.selected].name}">{$list_attr_group_item.attributes[$list_attr_group_item.selected].name}</span>
						</div>
					{/if}
					<div class="tdk-list-attr">
						{foreach from=$list_attr_group_item.attributes key=id_attribute item=attribute}
							<a class="tdk-select-attr{if $id_attribute == $list_attr_group_item.selected} selected{/if}{if isset($attribute.disable)} disable{/if}" data-display-type="{$display_type}" data-group-type="{$list_attr_group_item.group_type}"{if $list_attr_group_item.group_type == 'color'} data-color-value="{$attribute.html_color_code}"{/if} data-id-attribute-group={$id_attribute_group} data-id-product="{$id_product}" data-id-attribute="{$id_attribute}" data-attribute-name="{$attribute.name}"href="javascript:void(0)">
								<span class="attribute-label">{$attribute.name}</span>
								{if $list_attr_group_item.group_type == 'color'}
									{if ($display_type == 5 || $display_type == 6 ) && $attribute.image_attribute_cover != ''}
										<img class="attribute-image active" src="{$attribute.image_attribute_cover}"></img>
									{else}
										{if $attribute.texture != ''}
											<img class="attribute-texture active" src="{$attribute.texture}"></img>
										{else}
											<span class="attribute-color active" style="background:{$attribute.html_color_code};"></span>
										{/if}
									{/if}
								{else}
									<span>{$attribute.name}</span>
								{/if}
							</a>
						{/foreach}
					
					</div>
				</div>
			{/if}
		{/foreach}
	{/if}
{/if}