{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<input type="number" class="input-group form-control tdk_cart_quantity" value="" data-id-product="{$tdk_cart_product_quantity.id_product}" data-min="">
	
