{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<div class="button-container cart">
	<form action="{$link_cart}" method="post">
		<input type="hidden" name="token" value="{$static_token}">
		<input type="hidden" value="{$tdk_cart_product.quantity}" class="quantity_product quantity_product_{$tdk_cart_product.id_product}" name="quantity_product">
		<input type="hidden" value="{if isset($tdk_cart_product.product_attribute_minimal_quantity) && $tdk_cart_product.product_attribute_minimal_quantity>$tdk_cart_product.minimal_quantity}{$tdk_cart_product.product_attribute_minimal_quantity}{else}{$tdk_cart_product.minimal_quantity}{/if}" class="minimal_quantity minimal_quantity_{$tdk_cart_product.id_product}" name="minimal_quantity">
		<input type="hidden" value="{$tdk_cart_product.id_product_attribute}" class="id_product_attribute id_product_attribute_{$tdk_cart_product.id_product}" name="id_product_attribute">
		<input type="hidden" value="{$tdk_cart_product.id_product}" class="id_product" name="id_product">
		<input type="hidden" name="id_customization" value="{if $tdk_cart_product.id_customization}{$tdk_cart_product.id_customization}{/if}" class="product_customization_id">
			
		<input type="hidden" class="input-group form-control qty qty_product qty_product_{$tdk_cart_product.id_product}" name="qty" value="{if isset($tdk_cart_product.wishlist_quantity)}{$tdk_cart_product.wishlist_quantity}{else}{if $tdk_cart_product.product_attribute_minimal_quantity && $tdk_cart_product.product_attribute_minimal_quantity>$tdk_cart_product.minimal_quantity}{$tdk_cart_product.product_attribute_minimal_quantity}{else}{$tdk_cart_product.minimal_quantity}{/if}{/if}" data-min="{if $tdk_cart_product.product_attribute_minimal_quantity && $tdk_cart_product.product_attribute_minimal_quantity>$tdk_cart_product.minimal_quantity}{$tdk_cart_product.product_attribute_minimal_quantity}{else}{$tdk_cart_product.minimal_quantity}{/if}">
		  <button class="btn btn-primary TdkBtnProduct add-to-cart tdk-bt-cart tdk-bt-cart_{$tdk_cart_product.id_product}{if !$tdk_cart_product.add_to_cart_url} disabled{/if}" data-button-action="add-to-cart" type="submit">
			<span class="tdk-loading cssload-speeding-wheel"></span>
			<span class="tdk-bt-cart-content">
				<i class="TdkIconBtnProduct icon-cart material-icons shopping-cart">&#xE547;</i>
				<span class="TdkNameBtnProduct">{l s='Add to cart' mod='tdktoolkit'}</span>
			</span>
		  </button>
	</form>
</div>

