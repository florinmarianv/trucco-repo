{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
{if isset($USE_PTABS) && $USE_PTABS == 'default'}
	<h4 class="title-info-product tdk-product-show-review-title">{l s='Reviews' mod='tdktoolkit'}</h4>
{else if isset($USE_PTABS) && $USE_PTABS == 'accordion'}
	<div class="card-header" role="tab" id="headingtdktoolkitproductreview">
	  <h5 class="h5">
		<a class="collapsed tdk-product-show-review-title tdktoolkit-accordion" data-toggle="collapse" data-parent="#accordion" href="#collapsetdktoolkitproductreview" aria-expanded="false" aria-controls="collapsetdktoolkitproductreview">
		  {l s='Reviews' mod='tdktoolkit'}
		</a>
	 </h5>
  </div>
{else}
	<li class="nav-item">
	  <a class="nav-link tdk-product-show-review-title" data-toggle="tab" href="#tdk-product-show-review-content">{l s='Reviews' mod='tdktoolkit'}</a>
	</li>
{/if}

