{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="wishlist">
	{if isset($wishlists) && count($wishlists) > 1}
		<div class="dropdown tdk-wishlist-button-dropdown">
		  <button class="tdk-wishlist-button dropdown-toggle show-list btn-primary TdkBtnProduct btn{if $added_wishlist} added{/if}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-id-wishlist="{$id_wishlist}" data-id-product="{$tdk_wishlist_id_product}" data-id-product-attribute="{$tdk_wishlist_id_product_attribute}">
			<span class="tdk-wishlist-bt-loading cssload-speeding-wheel"></span>
			<span class="tdk-wishlist-bt-content">
				<i class="TdkIconBtnProduct icon-wishlist material-icons">&#xE87D;</i>
				<span class="TdkNameBtnProduct">{l s='Add to Wishlist' mod='tdktoolkit'}</span>
			</span>
			
		  </button>
		  <div class="dropdown-menu tdk-list-wishlist tdk-list-wishlist-{$tdk_wishlist_id_product}">
			{foreach from=$wishlists item=wishlists_item}
				<a href="javascript:void(0)" class="dropdown-item list-group-item list-group-item-action wishlist-item{if in_array($wishlists_item.id_wishlist, $wishlists_added)} added {/if}" data-id-wishlist="{$wishlists_item.id_wishlist}" data-id-product="{$tdk_wishlist_id_product}" data-id-product-attribute="{$tdk_wishlist_id_product_attribute}" title="{if in_array($wishlists_item.id_wishlist, $wishlists_added)}{l s='Remove from Wishlist' mod='tdktoolkit'}{else}{l s='Add to Wishlist' mod='tdktoolkit'}{/if}">{$wishlists_item.name}</a>			
			{/foreach}
		  </div>
		</div>
	{else}
		<a class="tdk-wishlist-button TdkBtnProduct btn-primary btn{if $added_wishlist} added{/if}" href="javascript:void(0)" data-id-wishlist="{$id_wishlist}" data-id-product="{$tdk_wishlist_id_product}" data-id-product-attribute="{$tdk_wishlist_id_product_attribute}" title="{if $added_wishlist}{l s='Remove from Wishlist' mod='tdktoolkit'}{else}{l s='Add to Wishlist' mod='tdktoolkit'}{/if}">
			<span class="tdk-wishlist-bt-loading cssload-speeding-wheel"></span>
			<span class="tdk-wishlist-bt-content">
				<i class="TdkIconBtnProduct icon-wishlist material-icons">&#xE87D;</i>
				<span class="TdkNameBtnProduct">{l s='Add to Wishlist' mod='tdktoolkit'}</span>
			</span>
		</a>
	{/if}
</div>