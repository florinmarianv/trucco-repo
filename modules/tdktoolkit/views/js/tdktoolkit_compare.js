/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
* 
**/
$(document).ready(function(){
	createTdkCompareModalPopup();
	TdkCompareButtonAction();
	prestashop.on('updateProductList', function() {
		TdkCompareButtonAction();
	});
	//TDK:: recall button action if need when change attribute at product page
	prestashop.on('updatedProduct', function() {  
		TdkCompareButtonAction();
	});
	prestashop.on('clickQuickView', function() {		
		var check_active_compare = setInterval(function(){
			if($('.quickview.modal').length) {	
				$('.quickview.modal').on('shown.bs.modal', function (e) {					
					TdkCompareButtonAction();
				})
				clearInterval(check_active_compare);
			}
			
		}, 300);
		
	});
	activeEventModalCompare();
});

function createTdkCompareModalPopup()
{
	var tdkCompareModalPopup = '';
	tdkCompareModalPopup += '<div class="modal tdk-modal tdk-modal-compare fade" tabindex="-1" role="dialog" aria-hidden="true">';
		tdkCompareModalPopup += '<div class="modal-dialog" role="document">';
			tdkCompareModalPopup += '<div class="modal-content">';
				tdkCompareModalPopup += '<div class="modal-header">';
					tdkCompareModalPopup += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
						tdkCompareModalPopup += '<span aria-hidden="true">&times;</span>';
					tdkCompareModalPopup += '</button>';
					tdkCompareModalPopup += '<h5 class="modal-title text-xs-center">';
					tdkCompareModalPopup += '</h5>';
				tdkCompareModalPopup += '</div>';
			tdkCompareModalPopup += '</div>';
		tdkCompareModalPopup += '</div>';
	tdkCompareModalPopup += '</div>';
	$('body').append(tdkCompareModalPopup);
}
function TdkCompareButtonAction()
{
	$('.tdk-compare-button').click(function(){
		if (!$('.tdk-compare-button.active').length)
		{
			var total_product_compare = compared_products.length;
			var id_product = $(this).data('id-product');
			
			var content_product_compare_mess_remove = productcompare_remove+'. <a href="'+productcompare_url+'" target="_blank"><strong>'+productcompare_viewlistcompare+'.</strong></a>';
			var content_product_compare_mess_add = productcompare_add+'. <a href="'+productcompare_url+'" target="_blank"><strong>'+productcompare_viewlistcompare+'.</strong></a>';
			var content_product_compare_mess_max = productcompare_max_item+'. <a href="'+productcompare_url+'" target="_blank"><strong>'+productcompare_viewlistcompare+'.</strong></a>';
			
			$(this).addClass('active');
			$(this).find('.tdk-compare-bt-loading').css({'display':'block'});
			$(this).find('.tdk-compare-bt-content').hide();
			var object_e = $(this);
			if ($(this).hasClass('added') || $(this).hasClass('delete'))
			{
				//TDK:: remove product form list product compare
				//TDK:: add product to list product compare
				$.ajax({
					type: 'POST',
					headers: {"cache-control": "no-cache"},
					url: productcompare_url+ '?rand=' + new Date().getTime(),
					async: true,
					cache: false,
					data: {
						"ajax": 1,
						"action": "remove",
						"id_product": id_product,
						"token": tdk_token
					},
					success: function (result)
					{
						// console.log(result);
						if (result == 1)
						{
							//TDK: update number product on icon compare
							if ($('.tdk-btn-compare .tdk-total-compare').length)
							{
								var old_num_compare = parseInt($('.tdk-btn-compare .tdk-total-compare').data('compare-total'));
								var new_num_compare = old_num_compare-1;
								$('.tdk-btn-compare .tdk-total-compare').data('compare-total',new_num_compare);
								$('.tdk-btn-compare .tdk-total-compare').text(new_num_compare);
							}
													
							compared_products.splice($.inArray(parseInt(id_product), compared_products), 1);
							if (object_e.hasClass('delete'))
							{
								//TDK:: remove from page product compare
								if ($('.tdk-productscompare-item').length == 1)
								{								
									window.location.replace(productcompare_url);
								}
								else
								{
									$('td.product-'+id_product).fadeOut(function(){
										$(this).remove();
										
									});
								}
							}
							else
							{
								//TDK:: remove from page product list
								$('.tdk-modal-compare .modal-title').html(content_product_compare_mess_remove);
								$('.tdk-modal-compare').modal('show');
								$('.tdk-compare-button[data-id-product='+id_product+']').removeClass('added');
								$('.tdk-compare-button[data-id-product='+id_product+']').attr('title',buttoncompare_title_add);
								// object_e.find('.tdk-compare-bt-loading').hide();
								// object_e.find('.tdk-compare-bt-content').show();
							}
						}
						else
						{
							$('.tdk-modal-compare .modal-title').html(productcompare_remove_error);
							$('.tdk-modal-compare').modal('show');
							
						}
						object_e.find('.tdk-compare-bt-loading').hide();
						object_e.find('.tdk-compare-bt-content').show();
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						alert("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
					}
				});
			}
			else
			{
				if (total_product_compare < comparator_max_item)
				{
					//TDK:: add product to list product compare
					$.ajax({
						type: 'POST',
						headers: {"cache-control": "no-cache"},
						url: productcompare_url+ '?rand=' + new Date().getTime(),
						async: true,
						cache: false,
						data: {
							"ajax": 1,
							"action": "add",
							"id_product": id_product,
							"token": tdk_token,
						},
						success: function (result)
						{
							// console.log(result);
							if (result == 1)
							{
								$('.tdk-modal-compare .modal-title').html(content_product_compare_mess_add);
								$('.tdk-modal-compare').modal('show');
								//TDK: update number product on icon compare
								if ($('.tdk-btn-compare .tdk-total-compare').length)
								{								
									var old_num_compare = parseInt($('.tdk-btn-compare .tdk-total-compare').data('compare-total'));
									var new_num_compare = old_num_compare+1;
									$('.tdk-btn-compare .tdk-total-compare').data('compare-total',new_num_compare);
									$('.tdk-btn-compare .tdk-total-compare').text(new_num_compare);
								}
								
								compared_products.push(id_product);
								$('.tdk-compare-button[data-id-product='+id_product+']').addClass('added');
								$('.tdk-compare-button[data-id-product='+id_product+']').attr('title',buttoncompare_title_remove);
							}
							else
							{
								$('.tdk-modal-compare .modal-title').html(productcompare_add_error);
								$('.tdk-modal-compare').modal('show');
							}
							
							object_e.find('.tdk-compare-bt-loading').hide();
							object_e.find('.tdk-compare-bt-content').show();
										
						},
						error: function (XMLHttpRequest, textStatus, errorThrown) {
							alert("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
						}
					});
					
				}
				else
				{
					//TDK:: list product compare limited
					$('.tdk-modal-compare .modal-title').html(content_product_compare_mess_max);
					$('.tdk-modal-compare').modal('show');
					object_e.find('.tdk-compare-bt-loading').hide();
					object_e.find('.tdk-compare-bt-content').show();
				}
			}
		}
		return false;
	})
}

function activeEventModalCompare()
{
	$('.tdk-modal-compare').on('hide.bs.modal', function (e) {
		// console.log($('.tdk-modal-review-bt').length);
		if ($('.tdk-compare-button.active').length)
		{
			// console.log('aaa');
			$('.tdk-compare-button.active').removeClass('active');
		}
	})
	$('.tdk-modal-compare').on('hidden.bs.modal', function (e) {
		$('body').css('padding-right', '');
	})
	$('.tdk-modal-compare').on('shown.bs.modal', function (e) {
		if ($('.quickview.modal').length)
		{			
			$('.quickview.modal').modal('hide');		
		}
	});
}


