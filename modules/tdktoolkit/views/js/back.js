/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
* 
**/
$(document).ready(function() {
	$('select#id_product_review_criterion_type').change(function() {
		// PS 1.6
		$('#categoryBox').closest('div.form-group').hide();
		$('#ids_product').closest('div.form-group').hide();
		// PS 1.5
		$('#categories-treeview').closest('div.margin-form').hide();
		$('#categories-treeview').closest('div.margin-form').prev().hide();
		$('#ids_product').closest('div.margin-form').hide();
		$('#ids_product').closest('div.margin-form').prev().hide();

		if (this.value == 2)
		{
			$('#categoryBox').closest('div.form-group').show();
			// PS 1.5
			$('#categories-treeview').closest('div.margin-form').show();
			$('#categories-treeview').closest('div.margin-form').prev().show();
		}
		else if (this.value == 3)
		{
			$('#ids_product').closest('div.form-group').show();
			// PS 1.5
			$('#ids_product').closest('div.margin-form').show();
			$('#ids_product').closest('div.margin-form').prev().show();
		}
	});

	$('select#id_product_review_criterion_type').trigger("change");
	
	//TDK:: tab change in group config
	var id_panel = $("#tdktoolkit-setting .tdktoolkit-tablist li.active a").attr("href");
	$(id_panel).addClass('active').show();
	$('.tdktoolkit-tablist li').click(function(){
		if(!$(this).hasClass('active'))
		{
			var default_tab = $(this).find('a').attr("href");			
			$('#TDKTOOLKIT_DEFAULT_TAB').val(default_tab);
		}
	})
	
	// console.log('test');
	if (typeof tdktoolkit_module_dir != 'undefined')
	{
		$.ajax({
			type: 'POST',
			headers: {"cache-control": "no-cache"},
			url: tdktoolkit_module_dir + 'psajax.php?rand=' + new Date().getTime(),
			async: true,
			cache: false,
			data: {
				"action": "get-new-review",		
			},
			success: function (result)
			{
				if(result != '')
				{						
					var obj = $.parseJSON(result);
					if (obj.number_review > 0)
					{
						$('#subtab-AdminTdktoolkitManagement').addClass('has-review');
						// $('#subtab-AdminTdktoolkitReviews').append('<span id="total_notif_number_wrapper" class="notifs_badge"><span id="total_notif_value">'+obj.number_review+'</span></span>');
						$('#subtab-AdminTdktoolkitReviews').append('<div class="notification-container"><span class="notification-counter">'+obj.number_review+'</span></div>');
					}
					
				}
				
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				// alert("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
			}
		});
	}
});