{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="module-content" class="clearfix">

  <div class="col-lg-2">
    <div class="list-group">

      <ul class="nav nav-pills nav-stacked">
        <li class="{if $main_active}active{/if}"><a class="list-group-item " data-toggle="tab" href="#main_info" >{l s='Main Config' mod='status2'}</a></li>
        <!--<li class="{if $status_active}active{/if}"><a class="list-group-item " data-toggle="tab" href="#status" >{l s='Status 2 S.L.' mod='status2'}</a></li>-->
      </ul>            
    </div>
  </div>
  <div class="tab-content col-lg-10">
    <div id="main_info" class="tab-pane panel {if $main_active}active{/if}">            
      <div class="panel" style="overflow:hidden">   
        <div class="panel-heading">
          <i class="icon icon-config"></i> {l s='status2' mod='status2'}
        </div>                               
        <p>
          <strong>{l s='Sincronización' mod='status2'}</strong><br />
          {l s='Puedes sincronizar con Status 2' mod='status2'}<br />
          {l s='Enlaces Disponibles.' mod='status2'}
        </p>
        <p>{l s='Ejecucion manual:'}</p>
        <ul>
          <li>{l s='Preparar Productos Status2:' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_prepare_products_status2.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_prepare_products_status2.php</a></li>
          <li>{l s='Cargar Products Status2:' mod='ecommsync'}<a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_products_full.php"> {$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_products_full.php</a></li>
          <li>{l s='Cargar Stock Status2:' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_stock.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_stock.php</a></li>
          <li>{l s='Pasar Pedido:' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/reimport_pedido.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/reimport_pedido.php</a></li>
          <li>{l s='Desmarcar Pedido:' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/desmarcar_pedido.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/desmarcar_pedido.php</a></li>
          <li>{l s='Pasar Devolucion:' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/reimport_devolucion.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/reimport_devolucion.php</a></li>
          <li>{l s='Cargar SpecialPrices :' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_specific_prices.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_specific_prices.php</a></li>
          <li>{l s='Prepare NewsLetter:' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_newsletter_stmoda.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_newsletter_stmoda.php</a></li>
          <li>{l s='Prepare TPVS:' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_tpvs.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_tpvs.php</a></li>
          <li>{l s='Crear BDs Import Status2:' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_tables_import.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_tables_import.php</a></li>
 		  <li>{l s='Inicializar BD Import Status2:' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_truncate_tables_import.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_truncate_tables_import.php</a></li>
          <li>{l s='Recrear Status2 Customers:' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_generar_table_customers.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_generar_table_customers.php</a></li>
          <li>{l s='Inicializar BD TPVS Status2:' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_truncate_tables_TPVS.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/cron_truncate_tables_TPVS.php</a></li>
          <li>{l s='Test Status2:' mod='ecommsync'} <a href="{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/test.php">{$shop_url|escape:'htmlall':'UTF-8'}/modules/status2/crons/test.php</a></li>
        </ul>

      </div>

      {$main_form}
      {if isset($resultado)}
        <div class="alert alert-success">
          <strong>Success!</strong> {$resultado}
        </div>
      {/if}
      {if isset($resultado_error)}
        <div class="alert alert-danger">
          <strong>Error!</strong> {$resultado_error}
        </div>
      {/if}
    </div>

    <div id="status" class="tab-pane panel {if $status_active}active{/if}">
      <div class="panel" style="overflow:hidden">   
        <div class="panel-heading">
          <i class="icon icon-config"></i> {l s='status2' mod='status2'}
        </div>                               
        <p>
          <strong>{l s='Syncronize your status with your ERP' mod='status2'}</strong><br />
          {l s='Select each status for each required action on your ERP.' mod='status2'}<br />
        </p>
      </div>

      {$status_form}

    </div>
  </div>
</div>        