<?php

/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

require_once dirname(__FILE__) . '/status2ModuleCore.php';

class Status2 extends status2ModuleCore
{

    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'status2';
        $this->tab = 'administration';
        $this->version = '3.0.7';
        $this->author = 'Status2 S.L';
        $this->need_instance = 0;
        $this->aw_key = "status2";
        $this->aw_protocol = '';
        $this->aw_domain = '';
        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Module PrestaShop X Status2');
        $this->description = $this->l('Module PrestaShop X Status2');
        $this->confirmUninstall = $this->l('');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('STATUS2_LIVE_MODE', false);
        Configuration::updateValue('ECOMMANDSYNC_STATUS2_TOKEN', '');
        Configuration::updateValue("STATUS2_SHOPS_STOCK", '');
        Configuration::updateValue('ECOMMANDSYNC_ID_STATUS2', '');
        Configuration::updateValue('ECOMMANDSYNC_STATUS2_PALETA', false);
        Configuration::updateValue('ECOMMANDSYNC_ID_USER', '');
        Configuration::updateValue('ECOMMANDSYNC_PRESUPUESTO', '');
        Configuration::updateValue('ECOMMANDSYNC_PRESUPUESTO2', '');
        Configuration::updateValue('ECOMMANDSYNC_FACTURA', '');
        Configuration::updateValue('ECOMMANDSYNC_PEDIDO', '');
        Configuration::updateValue('ECOMMANDSYNC_STATUS2_MARCA', true);
        Configuration::updateValue('STATUS2_FECHA_CONTROL_PRODUCTS', '');
        Configuration::updateValue('STATUS2_CANTIDAD_DE_REGISTROS_WEBSERVICE', '');
        Configuration::updateValue('STATUS2_INICIALIZAR_PRE_BD', true);
        Configuration::updateValue('STATUS2_TIMEOUT', '');
        Configuration::updateValue('STATUS2_FECHA_CONTROL_STOCKS', '');
        Configuration::updateValue('STATUS2_DESCRIPCION_O_MODELO', true);
        Configuration::updateValue('STATUS2_CANTIDAD_DE_REGISTROS_PRODUCTOS', '');
        Configuration::updateValue('STATUS2_PETICION_TRASPASO', '0');
        Configuration::updateValue('STATUS2_FECHA_CONTROL_REBAJAS', '');
        Configuration::updateValue('STATUS2_TARIFA', '10');
        Configuration::updateValue('STATUS2_TIPOCONSULTA', '1');
        Configuration::updateValue('STATUS2_REGISTROS_ARTICULOS', '0');
        Configuration::updateValue('STATUS2_REGISTROS_STOCKS', '0');
        Configuration::updateValue('STATUS2_REGISTROS_REBAJAS', '0');
        Configuration::updateValue('STATUS2_TIPO_REBAJAS', false);
        Configuration::updateValue('STATUS2_PERMITE_GESTION_POR_TALLA', false);
        Configuration::updateValue('STATUS2_TPVS_STOCK_PREFERIDOS', '');
        Configuration::updateValue('STATUS2_IMPUESTOS', '21');
        Configuration::updateValue('STATUS2_DESCRIPCION_ECOMERCE', false);
        Configuration::updateValue('STATUS2_COLOR_ECOMERCE', false);
        Configuration::updateValue('STATUS2_COLOR_EN_DESCRIPCION', false);
        Configuration::updateValue('STATUS2_CLIENTES_NEWSLETTER', '');
        Configuration::updateValue('STATUS2_MANTENIMIENTO_BD', false);
        Configuration::updateValue('STATUS2_IDIOMAS', '');
        Configuration::updateValue('STATUS2_IDIOMA_DEFECTO', '');
        Configuration::updateValue('STATUS2_DESCRIPCION_CORTA_LARGA', false);
        Configuration::updateValue('STATUS2_ARTICULO_AGRUPADO', false);
        Configuration::updateValue('STATUS2_ARTICULOS_AGRUPADOS_Y_NO_AGRUPADOS', false);
        Configuration::updateValue('STATUS2_VALOR_AGRUPACION', '');
        Configuration::updateValue('STATUS2_NOMBRE_MINUSCULAS', false);
        Configuration::updateValue('STATUS2_CARGAR_TARIFAS_PAIS', false);
        Configuration::updateValue('STATUS2_TIENDA_ACTIVA', false);
        Configuration::updateValue('STATUS2_SOLO_TARIFAS', false);
        Configuration::updateValue('STATUS2_URL_SEO_ERP', false);
        Configuration::updateValue('STATUS2_CATEGORIAS_X_ID', false);
        Configuration::updateValue('STATUS2_PASAR_VENTA', false);
        Configuration::updateValue('STATUS2_CONTROL_ESTADOS', false);
        Configuration::updateValue('STATUS2_ID_ESTADO', '');
        Configuration::updateValue('STATUS2_ID_ESTADO_ERROR', '');
        Configuration::updateValue('STATUS2_IMPUESTOS_VENTA', '21');


        $archivo = array("matches" => array());
        $fp = fopen('../modules/status2/matching.json', 'w');
        fwrite($fp, json_encode($archivo, JSON_UNESCAPED_SLASHES));
        fclose($fp);
        include(dirname(__FILE__) . '/sql/install.php');
        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('actionOrderHistoryAddAfter') &&
            $this->registerHook('actionCategoryAdd') &&
            $this->registerHook('actionCategoryUpdate') &&
            $this->registerHook('actionObjectCategoryUpdateAfter') &&
            $this->registerHook('actionObjectManufacturerAddAfter') &&
            $this->registerHook('actionOrderStatusPostUpdate') &&
            $this->registerHook('actionProductAdd') &&
            $this->registerHook('actionCustomerAccountAdd') &&
            $this->registerHook('actionAdminProductsControllerSaveAfter') &&
            $this->registerHook('actionOrderReturn') &&
            $this->registerHook('actionObjectSupplierAddAfter');
    }

    public function uninstall()
    {
     /*
        Configuration::deleteByName('STATUS2_LIVE_MODE');
        Configuration::deleteByName('ECOMMANDSYNC_STATUS2_TOKEN');
        Configuration::deleteByName('ECOMMANDSYNC_ID_STATUS2');
        Configuration::deleteByName('ECOMMANDSYNC_ID_USER');
        Configuration::deleteByName("STATUS2_SHOPS_STOCK");
        Configuration::deleteByName('ECOMMANDSYNC_PRESUPUESTO');
        Configuration::deleteByName('ECOMMANDSYNC_PRESUPUESTO2');
        Configuration::deleteByName('ECOMMANDSYNC_FACTURA');
        Configuration::deleteByName('ECOMMANDSYNC_PEDIDO');
        Configuration::deleteByName('STATUS2_FECHA_CONTROL_PRODUCTS');
        Configuration::deleteByName('STATUS2_CANTIDAD_DE_REGISTROS_WEBSERVICE');
        Configuration::deleteByName('STATUS2_INICIALIZAR_PRE_BD');
        Configuration::deleteByName('STATUS2_DESCRIPCION_O_MODELO');
        Configuration::deleteByName('STATUS2_CANTIDAD_DE_REGISTROS_PRODUCTOS');
        Configuration::deleteByName('STATUS2_TIMEOUT');
        Configuration::deleteByName('STATUS2_FECHA_CONTROL_STOCKS');
        Configuration::deleteByName('STATUS2_PETICION_TRASPASO');
        Configuration::deleteByName('STATUS2_FECHA_CONTROL_REBAJAS');
        Configuration::deleteByName('STATUS2_TARIFA');
        Configuration::deleteByName('STATUS2_TIPOCONSULTA');
        Configuration::deleteByName('STATUS2_REGISTROS_ARTICULOS');
        Configuration::deleteByName('STATUS2_REGISTROS_STOCKS');
        Configuration::deleteByName('STATUS2_REGISTROS_REBAJAS');
        Configuration::deleteByName('STATUS2_TIPO_REBAJAS');
        Configuration::deleteByName('STATUS2_PERMITE_GESTION_POR_TALLA');
        Configuration::deleteByName('STATUS2_TPVS_STOCK_PREFERIDOS');
        Configuration::deleteByName('STATUS2_IMPUESTOS');
        Configuration::deleteByName('STATUS2_DESCRIPCION_ECOMERCE');
        Configuration::deleteByName('STATUS2_COLOR_ECOMERCE');
        Configuration::deleteByName('STATUS2_COLOR_EN_DESCRIPCION');
        Configuration::deleteByName('STATUS2_CLIENTES_NEWSLETTER');
        Configuration::deleteByName('STATUS2_MANTENIMIENTO_BD');
        Configuration::deleteByName('STATUS2_IDIOMAS');
        Configuration::deleteByName('STATUS2_IDIOMA_DEFECTO');
        Configuration::deleteByName('STATUS2_DESCRIPCION_CORTA_LARGA');
        Configuration::deleteByName('STATUS2_ARTICULO_AGRUPADO');
        Configuration::deleteByName('STATUS2_ARTICULOS_AGRUPADOS_Y_NO_AGRUPADOS');
        Configuration::deleteByName('STATUS2_VALOR_AGRUPACION');
        Configuration::deleteByName('STATUS2_NOMBRE_MINUSCULAS');
        Configuration::deleteByName('STATUS2_CARGAR_TARIFAS_PAIS');
        Configuration::deleteByName('STATUS2_TIENDA_ACTIVA');
        Configuration::deleteByName('STATUS2_SOLO_TARIFAS');
        Configuration::deleteByName('STATUS2_URL_SEO_ERP');
        Configuration::deleteByName('STATUS2_CATEGORIAS_X_ID');
        Configuration::deleteByName('STATUS2_PASAR_VENTA');
*/



        include(dirname(__FILE__) . '/sql/uninstall.php');
        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        return $this->checkCurrency(true, true, true);
    }

    protected function hookDisplayAwCustomView()
    {
        return $this->_displayHomeContent();
    }

    public function _displayHomeContent()
    {
        if (((bool)Tools::isSubmit('submitEcommandsyncModule')) == true || ((bool)Tools::isSubmit('submitEcommandsyncStatus')) == true) {
            $this->postProcess();
        }
        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('main_active', true);
        $this->context->smarty->assign('status_active', false);
        $this->context->smarty->assign('main_form', $this->renderMainForm());
        $this->context->smarty->assign('status_form', $this->renderStatusForm());
        $this->context->smarty->assign('shop_url', (Configuration::get('PS_SSL_ENABLED')) ? Tools::getShopDomainSsl(true) : Tools::getShopDomain(true));
        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/information.tpl');
        //$output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');
        return $output; //$this->output;
    }

    protected function renderStatusForm()
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitEcommandsyncStatus';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        //$config_array = $this->getConfigStatusForm();
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigStatusFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );
        return $helper->generateForm(array($this->getConfigStatusForm()));
    }

    protected function getConfigStatusFormValues()
    {
        return array(
            'ECOMMANDSYNC_PRESUPUESTO' => Configuration::get('ECOMMANDSYNC_PRESUPUESTO'),
            'ECOMMANDSYNC_PRESUPUESTO2' => Configuration::get('ECOMMANDSYNC_PRESUPUESTO2'),
            'ECOMMANDSYNC_FACTURA' => Configuration::get('ECOMMANDSYNC_FACTURA'),
            'ECOMMANDSYNC_PEDIDO' => Configuration::get('ECOMMANDSYNC_PEDIDO')
        );
    }

    protected function renderMainForm()
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitEcommandsyncModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigMainFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );
        //$tpvs = Db::getInstance()->executeS("select * from " . _DB_PREFIX_ . "status2_tpvs");
        $id_shop = (int)Context::getContext()->shop->id;
        $id_shop_group = (int)Shop::getContextShopGroupID();
        $tpvs = Db::getInstance()->executeS("select id_tpv, id_tpv_status2,nombre,selected, concat(id_tpv_status2,' -> ',nombre) as nombre2 from " . _DB_PREFIX_ . "status2_tpvs where id_shop = '".$id_shop."' and id_group = '".$id_shop_group."'");
        return $helper->generateForm(array($this->getConfigMainForm($tpvs)));
    }

    protected function getConfigMainForm($tiendas)
    {

        /**
         * TODO: recoger del webservice de status2 todas las tiendas del cliente
         * TODO: Recorrer esas tiendas recogidas y mirar en la variable de Configuration las que ya tenian asignadas para sincronizar
         */

        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Tienda Activa'),
                        'name' => 'STATUS2_TIENDA_ACTIVA',
                        'desc' => $this->l('Habilitado se realizará carga de producto/stocks/rebajas para esta tienda'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Solo Tarifas'),
                        'name' => 'STATUS2_SOLO_TARIFAS',
                        'desc' => $this->l('Habilitado solo se cargarán tarifas para productos ya creados para esta tienda'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Pasar venta a StModa'),
                        'name' => 'STATUS2_PASAR_VENTA',
                        'desc' => $this->l('Deshabilitado no cargará la venta en StModa'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Actualizar estado pedidos'),
                        'name' => 'STATUS2_CONTROL_ESTADOS',
                        'desc' => $this->l('Permite actualizar el estado del pedido una vez importado en StModa'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('ID de Prestashop para actualizar el estado una vez importado CORRECTAMENTE en StModa '),
                        'name' => 'STATUS2_ID_ESTADO',
                        'label' => $this->l('ID Estado Pedido Correcto'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('ID de Prestashop para actualizar el estado si FALLA la importación en StModa '),
                        'name' => 'STATUS2_ID_ESTADO_ERROR',
                        'label' => $this->l('ID Estado Pedido ERROR'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Codigo Cliente'),
                        'name' => 'ECOMMANDSYNC_STATUS2_KEYCLIENTE',
                        'label' => $this->l('Codigo Cliente Status2'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('URL Status2'),
                        'name' => 'ECOMMANDSYNC_STATUS2_TOKEN',
                        'label' => $this->l('URL WS Status2'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Idiomas ej: CA;EN;FR;ES->Idioma x defecto el último'),
                        'name' => 'STATUS2_IDIOMAS',
                        'label' => $this->l('IDIOMAS '),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Idioma defecto ej:ES'),
                        'name' => 'STATUS2_IDIOMA_DEFECTO',
                        'label' => $this->l('IDIOMA X DEFECTO '),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Fecha Control YYYY-MM-DD HH:mm:ss'),
                        'name' => 'STATUS2_FECHA_CONTROL_PRODUCTS',
                        'label' => $this->l('Fecha Control PRODUCTOS'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Paginación->Número de registros'),
                        'name' => 'STATUS2_REGISTROS_ARTICULOS',
                        'label' => $this->l('Paginación PRODUCTOS'),
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Permite gestionar combinaciones'),
                        'name' => 'STATUS2_PERMITE_GESTION_POR_TALLA',
                        'desc' => $this->l('Habilitado permite activar/desactivar combinaciones del producto desde StModa'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Descripción / Modelo'),
                        'name' => 'STATUS2_DESCRIPCION_O_MODELO',
                        'desc' => $this->l('Habilitado se utilizará la descripción de StModa, si no Modelo'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Descripción ARTICULO ECOMERCE'),
                        'name' => 'STATUS2_DESCRIPCION_ECOMERCE',
                        'desc' => $this->l('Habilitado se utilizará la descripción ECOMERCE del artículo de StModa en Resumen/Descripcion'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Resumen/Descripción'),
                        'name' => 'STATUS2_DESCRIPCION_CORTA_LARGA',
                        'desc' => $this->l('Utilizar Resumen(texto corto) o Descripción(texto largo) de Prestashop para la descripción ECOMERCE de StModa'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Descripción COLOR ECOMERCE'),
                        'name' => 'STATUS2_COLOR_ECOMERCE',
                        'desc' => $this->l('Habilitado se utilizará la descripción ECOMERCE del color de StModa'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Nombre Minúsculas'),
                        'name' => 'STATUS2_NOMBRE_MINUSCULAS',
                        'desc' => $this->l('Habilitado se convertirá el nombre del producto en minúsculas'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Fecha Control YYYY-MM-DD HH:mm:ss'),
                        'name' => 'STATUS2_CLIENTES_NEWSLETTER',
                        'label' => $this->l('Fecha Control eMails NewsLetter'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Fecha Control YYYY-MM-DD HH:mm:ss'),
                        'name' => 'STATUS2_FECHA_CONTROL_STOCKS',
                        'label' => $this->l('Fecha Control STOCKS'),
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Solicitar Stock con DateTimeStamp Actual'),
                        'name' => 'STATUS2_STOCK_CARGA_INICIAL',
                        'desc' => $this->l('Habilitado no se descontarán 6 horas del último DateTimeStamp para la busqueda de stock.(Recomendado desactivado)->Util cuando se marcan articulos nuevos en ecomerce subir su stock'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Fecha Control YYYY-MM-DD HH:mm:ss !!!SI MODIFICA->PAGINACIÓN=1 !!!'),
                        'name' => 'STATUS2_FECHA_CONTROL_REBAJAS',
                        'label' => $this->l('Fecha Control REBAJAS'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Paginación->Número de registros'),
                        'name' => 'STATUS2_REGISTROS_REBAJAS',
                        'label' => $this->l('Paginación REBAJAS'),
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Tipo Rebajas'),
                        'name' => 'STATUS2_TIPO_REBAJAS',
                        'desc' => $this->l('Habilitado % - Deshabilitado Importe'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Realizar mantenimiento BD productos'),
                        'name' => 'STATUS2_MANTENIMIENTO_BD',
                        'desc' => $this->l('Habilitado limpiará los registros resueltos de la BD'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Numero de registros a solicitar al Webservice ej.5000'),
                        'name' => 'STATUS2_CANTIDAD_DE_REGISTROS_WEBSERVICE',
                        'label' => $this->l('Registros a solicitar'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Numero de registros a grabar en Prestashop ej.500'),
                        'name' => 'STATUS2_CANTIDAD_DE_REGISTROS_PRODUCTOS',
                        'label' => $this->l('Registros a grabar'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Numero de Tarifa de los Artículos'),
                        'name' => 'STATUS2_TARIFA',
                        'label' => $this->l('Tarifa'),
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Tarifas X Paises'),
                        'name' => 'STATUS2_CARGAR_TARIFAS_PAIS',
                        'desc' => $this->l('Cargar diferentes Tarifas para Paises'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Impuesto para la VENTA'),
                        'name' => 'STATUS2_IMPUESTOS_VENTA',
                        'label' => $this->l('Impuesto Ventas'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Impuesto para los Artículos'),
                        'name' => 'STATUS2_IMPUESTOS',
                        'label' => $this->l('Impuesto Artículos'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Tipo de consulta (1) Marca Defecto (2) Todos (3) Orden Id Tipo Desc (4) ERP'),
                        'name' => 'STATUS2_TIPOCONSULTA',
                        'label' => $this->l('Tipo de EANS'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Time Out espera de datos ej.7200'),
                        'name' => 'STATUS2_TIMEOUT',
                        'label' => $this->l('Time Out'),
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Inicializar BD preimport'),
                        'name' => 'STATUS2_INICIALIZAR_PRE_BD',
                        'desc' => $this->l('Habilita para borrar la base de datos de pre carga en cada llamada'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('URL Status2 FTP'),
                        'name' => 'ECOMMANDSYNC_STATUS2_URLFTP',
                        'label' => $this->l('URL FTP Status2 (Sin FTP://)'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Cliente FTP'),
                        'name' => 'ECOMMANDSYNC_STATUS2_CLIFTP',
                        'label' => $this->l('usuario de FTP'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Password FTP'),
                        'name' => 'ECOMMANDSYNC_STATUS2_PASSFTP',
                        'label' => $this->l('Password FTP imagenes'),
                    ), array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Ean Transporte'),
                        'name' => 'ECOMMANDSYNC_STATUS2_EANTRANS',
                        'label' => $this->l('Ean para el transporte'),
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Importar features'),
                        'name' => 'ECOMMANDSYNC_STATUS2_FEATURES',
                        'desc' => $this->l('Habilita para importar caracteristicas'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Importar Composición Tejido'),
                        'name' => 'ECOMMANDSYNC_STATUS2_FEATURES_TEJIDO',
                        'desc' => $this->l('Habilita para importar composición del tejido'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Usar Id_Paleta'),
                        'name' => 'ECOMMANDSYNC_STATUS2_PALETA',
                        'desc' => $this->l('Los ids de prestashop seran los mismos que en Id_Paleta (no compatible con Articulos agrupados)'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Artículo agrupado por color'),
                        'name' => 'STATUS2_ARTICULO_AGRUPADO',
                        'desc' => $this->l('ATENCIÓN!! Activado el Id_Paleta no se tendrá en cuenta (no pueden estar los dos activados)'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Activar agrupados y no agrupados'),
                        'name' => 'STATUS2_ARTICULOS_AGRUPADOS_Y_NO_AGRUPADOS',
                        'desc' => $this->l('Se pueden combinar artículos agrupados y no agrupados en el catalogo'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('SI agrupación combinada->se agrupará coincidiendo este valor con el 5 Dígito del Filtro Ad '),
                        'name' => 'STATUS2_VALOR_AGRUPACION',
                        'label' => $this->l('Valor agrupación'),
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Nombre Artículo + Desc. Color'),
                        'name' => 'STATUS2_COLOR_EN_DESCRIPCION',
                        'desc' => $this->l('Añadir en el nombre del artículo la descripción del color'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Activar Familia'),
                        'name' => 'ECOMMANDSYNC_STATUS2_FAMILIA',
                        'desc' => $this->l('Las categorias se importaran de familia/subfamilia en lugar de seccion/tipo'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Activar Prefijo marca'),
                        'name' => 'ECOMMANDSYNC_STATUS2_MARCA',
                        'desc' => $this->l('Las referencias se juntaran por marca-idArticulo'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Categorizar por ID'),
                        'name' => 'STATUS2_CATEGORIAS_X_ID',
                        'desc' => $this->l('Se utilizará el ID para realizar la categorización del artículo'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('SEO URLS desde ERP'),
                        'name' => 'STATUS2_URL_SEO_ERP',
                        'desc' => $this->l('Las SEO URLS se informan desde el ERP'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                         )
                    ),
                    array(
                        'type' => 'switch',
                        'lang' => true,
                        'label' => $this->l('Peticiones de Traspaso'),
                        'name' => 'STATUS2_PETICION_TRASPASO',
                        'desc' => $this->l('Se realizará la busqueda del stock y petición de traspaso a las tiendas'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('ORDEN BUSQUEDA STOCK-TPVS ej:M000101;M000201;M000203'),
                        'name' => 'STATUS2_TPVS_STOCK_PREFERIDOS',
                        'label' => $this->l('Prioridad Tpvs Stock'),
                    ),
                    array(
                        'type' => 'checkbox',
                        'label' => $this->l('Display shops'),
                        'desc' => $this->l('Selecciona las tiendas de las que quieres sincronizar el stock'),
                        'name' => 'STATUS2_SHOPS_STOCK',
                        'values' => array(
                            'query' => $tiendas,
                            'id' => 'id_tpv_status2',
                            'name' => 'nombre2',
                        ),
                        'tab' => 'config',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function getConfigStatusForm()
    {
        $order_states_full = OrderState::getOrderStates($this->context->language->id);
        $order_states = array(array('id' => 0, 'name' => '-- Ninguno'));
        foreach ($order_states_full as $key => $value) {
            array_push($order_states, array('id' => $value['id_order_state'], 'name' => $value['name']));
        }
        $status_actions = array(
            array(
                'id' => 'presupuesto',
                'name' => $this->l('Crear presupuesto')
            ),
            array(
                'id' => 'confirmado',
                'name' => $this->l('Confirmar pedido de venta')
            ),
            array(
                'id' => 'factura',
                'name' => $this->l('Generar factura')
            ),
        );
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ), 'input' => array(
                    array(
                        'col' => 6,
                        'type' => 'select',
                        'name' => 'ECOMMANDSYNC_PRESUPUESTO',
                        'label' => $this->l('Estado para generar el presupuesto'),
                        'options' => array(
                            'query' => $order_states,
                            'id' => 'id',
                            'name' => 'name',
                        ),
                    ), array(
                        'col' => 6,
                        'type' => 'text',
                        'desc' => $this->l('Si necesitas mas de un estado para generar el presupuesto, pon los ids separados por comas (ej: 1,5,6)'),
                        'name' => 'ECOMMANDSYNC_PRESUPUESTO2',
                        //'label' => $this->l('Token STATUS2'),
                    ),
                    array(
                        'col' => 6,
                        'type' => 'select',
                        'name' => 'ECOMMANDSYNC_PEDIDO',
                        'label' => $this->l('Estado para generar el pedido de venta'),
                        'options' => array(
                            'query' => $order_states,
                            'id' => 'id',
                            'name' => 'name',
                        ),
                    ),
                    array(
                        'col' => 6,
                        'type' => 'select',
                        'name' => 'ECOMMANDSYNC_FACTURA',
                        'label' => $this->l('Estado para generar factura'),
                        'options' => array(
                            'query' => $order_states,
                            'id' => 'id',
                            'name' => 'name',
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function getConfigMainFormValues()
    {
        $returner = array(
            'ECOMMANDSYNC_LIVE_MODE' => Configuration::get('ECOMMANDSYNC_LIVE_MODE'),
            'ECOMMANDSYNC_STATUS2_TOKEN' => Configuration::get('ECOMMANDSYNC_STATUS2_TOKEN', ''),
            'STATUS2_FECHA_CONTROL_PRODUCTS' => Configuration::get('STATUS2_FECHA_CONTROL_PRODUCTS', ''),
            'STATUS2_CANTIDAD_DE_REGISTROS_WEBSERVICE' => Configuration::get('STATUS2_CANTIDAD_DE_REGISTROS_WEBSERVICE', ''),
            'STATUS2_INICIALIZAR_PRE_BD' => Configuration::get('STATUS2_INICIALIZAR_PRE_BD', false),
            'STATUS2_TIMEOUT' => Configuration::get('STATUS2_TIMEOUT', ''),
            'STATUS2_FECHA_CONTROL_STOCKS' => Configuration::get('STATUS2_FECHA_CONTROL_STOCKS', ''),
            'STATUS2_DESCRIPCION_O_MODELO' => Configuration::get('STATUS2_DESCRIPCION_O_MODELO', false),
            'STATUS2_CANTIDAD_DE_REGISTROS_PRODUCTOS' => Configuration::get('STATUS2_CANTIDAD_DE_REGISTROS_PRODUCTOS', ''),
            'STATUS2_PETICION_TRASPASO' => Configuration::get('STATUS2_PETICION_TRASPASO', false),
            'STATUS2_FECHA_CONTROL_REBAJAS' => Configuration::get('STATUS2_FECHA_CONTROL_REBAJAS', ''),
            'STATUS2_TARIFA' => Configuration::get('STATUS2_TARIFA', ''),
            'STATUS2_TIPOCONSULTA' => Configuration::get('STATUS2_TIPOCONSULTA', ''),
            'STATUS2_REGISTROS_ARTICULOS' => Configuration::get('STATUS2_REGISTROS_ARTICULOS', ''),
            'STATUS2_REGISTROS_STOCKS' => Configuration::get('STATUS2_REGISTROS_STOCKS', ''),
            'STATUS2_REGISTROS_REBAJAS' => Configuration::get('STATUS2_REGISTROS_REBAJAS', ''),
            'STATUS2_TIPO_REBAJAS' => Configuration::get('STATUS2_TIPO_REBAJAS', false),
            'STATUS2_TPVS_STOCK_PREFERIDOS' => Configuration::get('STATUS2_TPVS_STOCK_PREFERIDOS', ''),
            'STATUS2_SHOPS_STOCK' => Configuration::get('STATUS2_SHOPS_STOCK'),
            'STATUS2_IMPUESTOS' => Configuration::get('STATUS2_IMPUESTOS', ''),
            'STATUS2_DESCRIPCION_ECOMERCE' => Configuration::get('STATUS2_DESCRIPCION_ECOMERCE', false),
            'STATUS2_COLOR_ECOMERCE' => Configuration::get('STATUS2_COLOR_ECOMERCE', false),
            'STATUS2_COLOR_EN_DESCRIPCION' => Configuration::get('STATUS2_COLOR_EN_DESCRIPCION', false),
            'STATUS2_PERMITE_GESTION_POR_TALLA' => Configuration::get('STATUS2_PERMITE_GESTION_POR_TALLA', false),
            'STATUS2_CLIENTES_NEWSLETTER' => Configuration::get('STATUS2_CLIENTES_NEWSLETTER', ''),
            'STATUS2_MANTENIMIENTO_BD' => Configuration::get('STATUS2_MANTENIMIENTO_BD', false),
            'STATUS2_IDIOMAS' => Configuration::get('STATUS2_IDIOMAS', ''),
            'STATUS2_IDIOMA_DEFECTO' => Configuration::get('STATUS2_IDIOMA_DEFECTO', ''),
            'STATUS2_DESCRIPCION_CORTA_LARGA' => Configuration::get('STATUS2_DESCRIPCION_CORTA_LARGA', false),
            'STATUS2_STOCK_CARGA_INICIAL' => Configuration::get("STATUS2_STOCK_CARGA_INICIAL", '0'),
            'STATUS2_ARTICULO_AGRUPADO' => Configuration::get('STATUS2_ARTICULO_AGRUPADO', false),
            'STATUS2_ARTICULOS_AGRUPADOS_Y_NO_AGRUPADOS' => Configuration::get('STATUS2_ARTICULOS_AGRUPADOS_Y_NO_AGRUPADOS', false),
            'STATUS2_VALOR_AGRUPACION' => Configuration::get('STATUS2_VALOR_AGRUPACION', ''),
            'STATUS2_NOMBRE_MINUSCULAS' => Configuration::get('STATUS2_NOMBRE_MINUSCULAS', false),
            'STATUS2_CARGAR_TARIFAS_PAIS' => Configuration::get('STATUS2_CARGAR_TARIFAS_PAIS', false),
            'STATUS2_TIENDA_ACTIVA' => Configuration::get('STATUS2_TIENDA_ACTIVA', false),
            'STATUS2_SOLO_TARIFAS' => Configuration::get('STATUS2_SOLO_TARIFAS', false),
            'STATUS2_CATEGORIAS_X_ID' => Configuration::get('STATUS2_CATEGORIAS_X_ID', false),
            'STATUS2_URL_SEO_ERP' => Configuration::get('STATUS2_URL_SEO_ERP', false),
            'STATUS2_PASAR_VENTA' => Configuration::get('STATUS2_PASAR_VENTA', false),
            'STATUS2_CONTROL_ESTADOS' => Configuration::get('STATUS2_CONTROL_ESTADOS', false),
            'STATUS2_ID_ESTADO' => Configuration::get('STATUS2_ID_ESTADO', ''),
            'STATUS2_ID_ESTADO_ERROR' => Configuration::get('STATUS2_ID_ESTADO_ERROR', ''),
            'STATUS2_IMPUESTOS_VENTA' => Configuration::get('STATUS2_IMPUESTOS_VENTA', ''),
            'ECOMMANDSYNC_STATUS2_FEATURES_TEJIDO' => Configuration::get("ECOMMANDSYNC_STATUS2_FEATURES_TEJIDO", '0'),
            'ECOMMANDSYNC_STATUS2_KEYCLIENTE' => Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE', ''),
            'ECOMMANDSYNC_STATUS2_FEATURES' => Configuration::get("ECOMMANDSYNC_STATUS2_FEATURES", '0'),
            'ECOMMANDSYNC_STATUS2_URLFTP' => Configuration::get('ECOMMANDSYNC_STATUS2_URLFTP', ''),
            'ECOMMANDSYNC_STATUS2_CLIFTP' => Configuration::get('ECOMMANDSYNC_STATUS2_CLIFTP', ''),
            'ECOMMANDSYNC_STATUS2_PASSFTP' => Configuration::get('ECOMMANDSYNC_STATUS2_PASSFTP', ''),
            'ECOMMANDSYNC_STATUS2_PALETA' => Configuration::get('ECOMMANDSYNC_STATUS2_PALETA', false),
            'ECOMMANDSYNC_STATUS2_FAMILIA' => Configuration::get('ECOMMANDSYNC_STATUS2_FAMILIA', false),
            'ECOMMANDSYNC_STATUS2_EANTRANS' => Configuration::get('ECOMMANDSYNC_STATUS2_EANTRANS', ''),
            'ECOMMANDSYNC_STATUS2_MARCA' => Configuration::get('ECOMMANDSYNC_STATUS2_MARCA', true)
        );
        $id_shop = (int)Context::getContext()->shop->id;
        $id_shop_group = (int)Shop::getContextShopGroupID();
        $tpvs = Db::getInstance()->executeS("select * from " . _DB_PREFIX_ . "status2_tpvs where id_shop = '".$id_shop."' and id_group = '".$id_shop_group."'");
        foreach ($tpvs as $tpv) {
            $returner["STATUS2_SHOPS_STOCK_" . $tpv["id_tpv_status2"]] = Configuration::get('STATUS2_SHOPS_STOCK_' . $tpv["id_tpv_status2"],null, $id_shop_group, $id_shop);
        }

        return $returner;
    }

    protected function getPaymentsConfigurationForm()
    {
        $arrayForm = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                    'label' => 'debes introducir el codigo de metodo de pago de SAP correspondiente al metodo de pago de Prestashop'
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
        foreach (Module::getPaymentModules() as $paymentModule) {
            $arrayForm['form']['input'][] = array(
                'type' => 'text',
                'label' => $this->l('Payment ID for ' . $paymentModule['name']),
                'name' => 'MODALIA_PAYMENT_' . strtoupper($paymentModule['name']),
                'desc' => "solo SAP",
                'required' => false,
                'lang' => false,
                'col' => 4,
            );
        }
        return $arrayForm;
    }

    protected function getPaymentsConfigurationFormValues()
    {
        $arrayForm = array();
        foreach (Module::getPaymentModules() as $paymentModule) {
            $arrayForm += array(
                'MODALIA_PAYMENT_' . strtoupper($paymentModule['name']) => Configuration::get('MODALIA_PAYMENT_' . strtoupper($paymentModule['name']), 0),
            );
        }
        return $arrayForm;
    }

    /*protected function renderPaymentsForm() {
      $helper = new HelperForm();
      $helper->show_toolbar = false;
      $helper->table = $this->table;
      $helper->module = $this;
      $helper->default_form_language = $this->context->language->id;
      $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
      $helper->identifier = $this->identifier;
      $helper->submit_action = 'submitEcommandsyncPayments';
      $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
              . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
      $helper->token = Tools::getAdminTokenLite('AdminModules');
      $helper->tpl_vars = array(
          'fields_value' => $this->getPaymentsConfigurationFormValues(),
          'languages' => $this->context->controller->getLanguages(),
          'id_language' => $this->context->language->id,
      );
      return $helper->generateForm(array($this->getPaymentsConfigurationForm()));
    }*/
    /*
      protected function renderForm() {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitStatus2Module';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
                . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );
        return $helper->generateForm(array($this->getConfigForm()));
      }*/

    /**
     * Save form data.
     */
    protected function postProcess()
    {


        $main_active = true;
        $status_active = false;

        if ((bool)Tools::isSubmit('submitEcommandsyncModule') == true) {
            $tpvs = Db::getInstance()->executeS("select * from " . _DB_PREFIX_ . "status2_tpvs");
            $datos = Tools::getAllValues();
            $id_shop = (int)Context::getContext()->shop->id;
            $id_shop_group = (int)Shop::getContextShopGroupID();
            foreach ($tpvs as $tpv) {
                if (isset($datos["STATUS2_SHOPS_STOCK_" . $tpv['id_tpv_status2']])) {
                    Configuration::updateValue("STATUS2_SHOPS_STOCK_" . $tpv['id_tpv_status2'], $datos["STATUS2_SHOPS_STOCK_" . $tpv['id_tpv_status2']]);
                    Db::getInstance()->execute("update " . _DB_PREFIX_ . "status2_tpvs set selected = 1 where id_tpv_status2 = '" . $tpv['id_tpv_status2'] . "' and id_shop = '".$id_shop."' and id_group = '".$id_shop_group."'");
                } else {
                    Db::getInstance()->execute("update " . _DB_PREFIX_ . "status2_tpvs set selected = 0 where id_tpv_status2 = '" . $tpv['id_tpv_status2'] . "' and id_shop = '".$id_shop."' and id_group = '".$id_shop_group."'");
                }
            }
            $form_values = $this->getConfigMainFormValues();
            //$form_values = $this->getConfigFormValues();
            foreach (array_keys($form_values) as $key) {
                Configuration::updateValue($key, Tools::getValue($key));
            }
            Configuration::updateValue('ECOMMANDSYNC_STATUS2_TOKEN', Tools::getValue('ECOMMANDSYNC_STATUS2_TOKEN'));
            Configuration::updateValue('STATUS2_FECHA_CONTROL_PRODUCTS', Tools::getValue('STATUS2_FECHA_CONTROL_PRODUCTS'));
            Configuration::updateValue('STATUS2_CANTIDAD_DE_REGISTROS_WEBSERVICE', Tools::getValue('STATUS2_CANTIDAD_DE_REGISTROS_WEBSERVICE'));
            Configuration::updateValue('STATUS2_INICIALIZAR_PRE_BD', Tools::getValue('STATUS2_INICIALIZAR_PRE_BD'));
            Configuration::updateValue('STATUS2_TIMEOUT', Tools::getValue('STATUS2_TIMEOUT'));
            Configuration::updateValue('STATUS2_FECHA_CONTROL_STOCKS', Tools::getValue('STATUS2_FECHA_CONTROL_STOCKS'));
            Configuration::updateValue('STATUS2_DESCRIPCION_O_MODELO', Tools::getValue('STATUS2_DESCRIPCION_O_MODELO'));
            Configuration::updateValue('STATUS2_CANTIDAD_DE_REGISTROS_PRODUCTOS', Tools::getValue('STATUS2_CANTIDAD_DE_REGISTROS_PRODUCTOS'));
            Configuration::updateValue('STATUS2_PETICION_TRASPASO', Tools::getValue('STATUS2_PETICION_TRASPASO'));
            Configuration::updateValue('STATUS2_FECHA_CONTROL_REBAJAS', Tools::getValue('STATUS2_FECHA_CONTROL_REBAJAS'));
            Configuration::updateValue('STATUS2_TARIFA', Tools::getValue('STATUS2_TARIFA'));
            Configuration::updateValue('STATUS2_TIPOCONSULTA', Tools::getValue('STATUS2_TIPOCONSULTA'));
            Configuration::updateValue('STATUS2_REGISTROS_ARTICULOS', Tools::getValue('STATUS2_REGISTROS_ARTICULOS'));
            Configuration::updateValue('STATUS2_REGISTROS_STOCKS', Tools::getValue('STATUS2_REGISTROS_STOCKS'));
            Configuration::updateValue('STATUS2_REGISTROS_REBAJAS', Tools::getValue('STATUS2_REGISTROS_REBAJAS'));
            Configuration::updateValue('STATUS2_TIPO_REBAJAS', Tools::getValue('STATUS2_TIPO_REBAJAS'));
            Configuration::updateValue('STATUS2_PERMITE_GESTION_POR_TALLA', Tools::getValue('STATUS2_PERMITE_GESTION_POR_TALLA'));
            Configuration::updateValue('STATUS2_TPVS_STOCK_PREFERIDOS', Tools::getValue('STATUS2_TPVS_STOCK_PREFERIDOS'));
            Configuration::updateValue('STATUS2_IMPUESTOS', Tools::getValue('STATUS2_IMPUESTOS'));
            Configuration::updateValue('STATUS2_DESCRIPCION_ECOMERCE', Tools::getValue('STATUS2_DESCRIPCION_ECOMERCE'));
            Configuration::updateValue('STATUS2_COLOR_ECOMERCE', Tools::getValue('STATUS2_COLOR_ECOMERCE'));
            Configuration::updateValue('STATUS2_COLOR_EN_DESCRIPCION', Tools::getValue('STATUS2_COLOR_EN_DESCRIPCION'));
            Configuration::updateValue('STATUS2_CLIENTES_NEWSLETTER', Tools::getValue('STATUS2_CLIENTES_NEWSLETTER'));
            Configuration::updateValue('STATUS2_MANTENIMIENTO_BD', Tools::getValue('STATUS2_MANTENIMIENTO_BD'));
            Configuration::updateValue('STATUS2_IDIOMAS', Tools::getValue('STATUS2_IDIOMAS'));
            Configuration::updateValue('STATUS2_IDIOMA_DEFECTO', Tools::getValue('STATUS2_IDIOMA_DEFECTO'));
            Configuration::updateValue('STATUS2_DESCRIPCION_CORTA_LARGA', Tools::getValue('STATUS2_DESCRIPCION_CORTA_LARGA'));
            Configuration::updateValue('STATUS2_STOCK_CARGA_INICIAL', Tools::getValue('STATUS2_STOCK_CARGA_INICIAL'));
            Configuration::updateValue('STATUS2_ARTICULO_AGRUPADO', Tools::getValue('STATUS2_ARTICULO_AGRUPADO'));
            Configuration::updateValue('STATUS2_ARTICULOS_AGRUPADOS_Y_NO_AGRUPADOS', Tools::getValue('STATUS2_ARTICULOS_AGRUPADOS_Y_NO_AGRUPADOS'));
            Configuration::updateValue('STATUS2_VALOR_AGRUPACION', Tools::getValue('STATUS2_VALOR_AGRUPACION'));
            Configuration::updateValue('STATUS2_NOMBRE_MINUSCULAS', Tools::getValue('STATUS2_NOMBRE_MINUSCULAS'));
            Configuration::updateValue('STATUS2_CARGAR_TARIFAS_PAIS', Tools::getValue('STATUS2_CARGAR_TARIFAS_PAIS'));
            Configuration::updateValue('STATUS2_TIENDA_ACTIVA', Tools::getValue('STATUS2_TIENDA_ACTIVA'));
            Configuration::updateValue('STATUS2_SOLO_TARIFAS', Tools::getValue('STATUS2_SOLO_TARIFAS'));
            Configuration::updateValue('STATUS2_CATEGORIAS_X_ID', Tools::getValue('STATUS2_CATEGORIAS_X_ID'));
            Configuration::updateValue('STATUS2_URL_SEO_ERP', Tools::getValue('STATUS2_URL_SEO_ERP'));
            Configuration::updateValue('STATUS2_PASAR_VENTA', Tools::getValue('STATUS2_PASAR_VENTA'));
            Configuration::updateValue('STATUS2_CONTROL_ESTADOS', Tools::getValue('STATUS2_CONTROL_ESTADOS'));
            Configuration::updateValue('STATUS2_ID_ESTADO', Tools::getValue('STATUS2_ID_ESTADO'));
            Configuration::updateValue('STATUS2_ID_ESTADO_ERROR', Tools::getValue('STATUS2_ID_ESTADO_ERROR'));
            Configuration::updateValue('STATUS2_IMPUESTOS_VENTA', Tools::getValue('STATUS2_IMPUESTOS_VENTA'));
            Configuration::updateValue('ECOMMANDSYNC_STATUS2_FEATURES_TEJIDO', Tools::getValue('ECOMMANDSYNC_STATUS2_FEATURES_TEJIDO'));
            Configuration::updateValue('ECOMMANDSYNC_STATUS2_KEYCLIENTE', Tools::getValue('ECOMMANDSYNC_STATUS2_KEYCLIENTE'));
            Configuration::updateValue('ECOMMANDSYNC_STATUS2_FEATURES', Tools::getValue('ECOMMANDSYNC_STATUS2_FEATURES'));
            Configuration::updateValue('ECOMMANDSYNC_STATUS2_URLFTP', Tools::getValue('ECOMMANDSYNC_STATUS2_URLFTP'));
            Configuration::updateValue('ECOMMANDSYNC_STATUS2_CLIFTP', Tools::getValue('ECOMMANDSYNC_STATUS2_CLIFTP'));
            Configuration::updateValue('ECOMMANDSYNC_STATUS2_PASSFTP', Tools::getValue('ECOMMANDSYNC_STATUS2_PASSFTP'));
            Configuration::updateValue('ECOMMANDSYNC_STATUS2_PALETA', Tools::getValue('ECOMMANDSYNC_STATUS2_PALETA'));
            Configuration::updateValue('ECOMMANDSYNC_STATUS2_FAMILIA', Tools::getValue('ECOMMANDSYNC_STATUS2_FAMILIA'));
            Configuration::updateValue('ECOMMANDSYNC_STATUS2_EANTRANS', Tools::getValue('ECOMMANDSYNC_STATUS2_EANTRANS'));
            Configuration::updateValue('ECOMMANDSYNC_STATUS2_MARCA', Tools::getValue('ECOMMANDSYNC_STATUS2_MARCA'));

        } elseif ((bool)Tools::isSubmit('submitEcommandsyncStatus') == true) {
            Configuration::updateValue('ECOMMANDSYNC_PRESUPUESTO', Tools::getValue('ECOMMANDSYNC_PRESUPUESTO'));
            Configuration::updateValue('ECOMMANDSYNC_PRESUPUESTO2', Tools::getValue('ECOMMANDSYNC_PRESUPUESTO2'));
            Configuration::updateValue('ECOMMANDSYNC_FACTURA', Tools::getValue('ECOMMANDSYNC_FACTURA'));
            Configuration::updateValue('ECOMMANDSYNC_PEDIDO', Tools::getValue('ECOMMANDSYNC_PEDIDO'));
            $main_active = false;
            $status_active = true;
        }
        $this->context->smarty->assign('main_active', $main_active);
        $this->context->smarty->assign('status_active', $status_active);
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }

    public function hookActionCategoryUpdate()
    {
        /* Place your code here. */
    }

    public function hookActionObjectCategoryUpdateAfter()
    {
        /* Place your code here. */
    }

    public function hookActionProductUpdate($params)
    {
        /* Place your code here. */
    }

    public function hookActionAdminProductsControllerSaveAfter($params)
    {
        /* Place your code here. */
    }

    public function hookActionProductDelete($params)
    {
        /* Place your code here. */
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        //PrestaShopLogger::addLog("Status2: VENTA" . (string)date("j, Y, g:i a") , 2);
        //PrestaShopLogger::addLog("Status2: ORDER NEW STATUS: ". $params["newOrderStatus"]->id ." - ". (string)date("j, Y, g:i a"), 2);
        //var_dump($params);
        //die;
        /*

        Estado Pedido:

            PS_OS_CHEQUE	1
            PS_OS_PAYMENT	2
            PS_OS_PREPARATION	3
            PS_OS_SHIPPING	4
            PS_OS_DELIVERED	5
            PS_OS_CANCELED	6
            PS_OS_REFUND	7
            PS_OS_ERROR	8
            PS_OS_OUTOFSTOCK	9
            PS_OS_BANKWIRE	10
            PS_OS_WS_PAYMENT	11
            PS_OS_OUTOFSTOCK_PAID	9
            PS_OS_OUTOFSTOCK_UNPAID	12
            PS_OS_COD_VALIDATION	13
        */
        $id_shop = (int)Context::getContext()->shop->id;
        $id_shop_group = (int)Shop::getContextShopGroupID();
        if (Configuration::get("STATUS2_PASAR_VENTA", null, $id_shop_group, $id_shop) == 1) {
            if ($params["newOrderStatus"]->id != 8 && $params["newOrderStatus"]->id != 6 && $params["newOrderStatus"]->id != 7) {
                $order = new Order($params["id_order"]);

               /*
               if (Configuration::get("STATUS2_OBLIGADO_FACTURA", null, $id_shop_group, $id_shop) == 1) {
                    if (!isset($order->invoice_number) || empty($order->invoice_number)) {
                        PrestaShopLogger::addLog("El estado del pedido ' . $order->id . ' no tiene factura emitida " . (string)date("j, Y, g:i a"), 2);
                        return;
                    }
                }
                */
                if ((int)$order->current_state == 8 || (int)$order->current_state == 6 || (int)$order->current_state == 7) {
                    PrestaShopLogger::addLog("El estado del pedido ' . $order->id . ' no es acorde para ser subido a status2 " . (string)date("j, Y, g:i a"), 2);
                    return;
                }

                $result = Db::getInstance()->getValue("select max(correcto) from " . _DB_PREFIX_ . "status2_pedidos where id_presta = '" . $order->id . "'");

                if ($result == 1) {
                    PrestaShopLogger::addLog("El pedido " . $order->id . " ya ha sido subido a Status2 " . (string)date("j, Y, g:i a"), 2);
                    return;
                }


                $address_dev = new Address($order->id_address_delivery);
                //EN LOLACASADEMUNT NOS PASARAN AQUI EL NUMERO DE TIENDA DONDE SE RECOGE EL PEDIDO
                //var_dump($address_dev->other);
                //die;
                $address_bill = new Address($order->id_address_invoice);
                $customer = new Customer($order->id_customer);
                $cliente = substr(Configuration::get("ECOMMANDSYNC_STATUS2_KEYCLIENTE", null, $id_shop_group, $id_shop), 0, 5);
                $id_status = null;
                if ($cliente == "WB000") {
                    $id_status = Db::getInstance()->getValue("select id_codigocliente_wehbe from " . _DB_PREFIX_ . "customer where id_customer = " . $customer->id);
                }
                if (!$id_status) {
                    $id_status = Db::getInstance()->getValue("select id_status2 from " . _DB_PREFIX_ . "status2_customers where id_presta = " . $customer->id);
                }
                try {
                    $soapClient = new SoapClient(Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN", null, $id_shop_group, $id_shop), array('trace' => true,
                        'connection_timeout' => 5000,
                        'cache_wsdl' => WSDL_CACHE_NONE,
                        'keep_alive' => false,));
                } catch (SoapFault $soapClient) {
                    PrestaShopLogger::addLog("Status2: IMPOSIBLE CONECTAR CON EL SERVIDOR " . time(), 4);
                    goto fin;
                    //die("Imposible conectar");
                }
                $parameters = new stdClass();
                if ($id_status) {
                    $parameters->P_sCodigoCliente = $id_status;
                }
                $parameters->P_sNombre = $customer->firstname;
                $parameters->P_sApellidos = $customer->lastname;
                $parameters->P_sSexo = $customer->id_gender;
                $parameters->P_seMail = $customer->email;
                $parameters->P_sFechaNacimiento = empty($customer->birthday) ? "00000000" : str_replace("-", "", $customer->birthday);
                //direcciones
                $parameters->P_sDireccion = $address_bill->address1;
                $parameters->P_sCodigoPostal = $address_bill->postcode;
                $parameters->P_sLocalidad = $address_bill->city;
                $parameters->P_sProvincia = !empty($address_bill->id_state) ? State::getNameById($address_bill->id_state) : "";
                $parameters->P_sPais = CountryCore::getIsoById(CountryCore::getIdByName(false, $address_dev->country));
                $parameters->P_sTelefono = empty($address_bill->phone) ? "0" : $address_bill->phone;
                $parameters->P_sMovil = empty($address_bill->phone_mobile) ? "0" : $address_bill->phone_mobile;
                $parameters->P_sDNI = empty($address_bill->dni) ? "0" : $address_bill->dni;
                $parameters->P_sDireccion_Envio = $address_dev->address1;
                $parameters->P_sCodigoPostal_Envio = $address_dev->postcode;
                $parameters->P_sLocalidad_Envio = $address_dev->city;
                $parameters->P_sProvincia_Envio = !empty($address_dev->id_state) ? State::getNameById($address_dev->id_state) : "";
                $parameters->P_sPais_Envio = CountryCore::getIsoById(CountryCore::getIdByName(false, $address_dev->country));
                $parameters->P_sTelefono_Envio = $address_dev->phone;
                $parameters->P_sMovil_Envio = $address_dev->phone_mobile;
                $return = $soapClient->WS_APP_Cliente_Fidelizacion_Actualizar($parameters);
                $resultado = substr($return->WS_APP_Cliente_Fidelizacion_ActualizarResult, 6, 4);
                //echo $resultado;
                if (is_numeric($resultado)) {
                    if (!$id_status) {
                        $id_status = $return->WS_APP_Cliente_Fidelizacion_ActualizarResult;
                        Db::getInstance()->execute("insert into " . _DB_PREFIX_ . "status2_customers(id_status2, id_presta) values(" . $id_status . "," . $customer->id . ")");
                    }
                    $lineas = 0;
                    $lineas_vent = array("clsSt_Venta_Lineas" => array());
                    $cart_rules = $order->getCartRules();
                    $total_discount_import = 0;
                    $total_paid = 0;
                    //modG: total ahora es un array de porcentajes
                    $total_carrito_desc = [];
                    $discount_types = [];
                    //modG: Añadimos las cartrules al array de descuentos
                    foreach ($cart_rules as $cart_rule) {
                        //$total_carrito_desc += $cart_rule["value"];
                        $cartRule_obj = new CartRule($cart_rule["id_cart_rule"]);
                        //$total_discount_import += $cart_rule["value"];
                        $groups = $cartRule_obj->getProductRuleGroups();
                        if (is_array($groups) && !empty($groups)) {
                            foreach ($groups as $group) {
                                if (isset($group['product_rules']) && !empty($group['product_rules'])) {
                                    foreach ($group['product_rules'] as $item) {
                                        $discount_types[] = $item;
                                    }
                                }
                            }
                        }
                        if ($cartRule_obj->reduction_percent != 0) {
                            $total_carrito_desc[] = $cartRule_obj->reduction_percent;
                        } else if ($cartRule_obj->reduction_amount != 0) {
                            $percent = (($cartRule_obj->reduction_amount * 100) / ($order->total_products_wt / $order->conversion_rate));
                            $total_carrito_desc [] = $percent;
                        }
                    }
                    $total_discount_import = $order->total_discounts / $order->conversion_rate;
                    $total_prods_eval = 0;
                    $stocks_no_encontrados = array();
                    $stocks_encontrados = array();
                    //Cogemos la tiendas para la busqueda de stock
                    $tiendas = Db::getInstance()->executeS("select id_tpv_status2 from " . _DB_PREFIX_ . "status2_tpvs where selected = 1 and id_shop = '" . $id_shop . "' and id_group = '" . $id_shop_group . "'");

                    $cliente_completo = Configuration::get("ECOMMANDSYNC_STATUS2_KEYCLIENTE", null, $id_shop_group, $id_shop);
                    $Tipo_Reserva = "";
                    $Reserva_SN = "N";
                    if (($cliente == "I0B2B") || ($cliente == "I0B2C")) {
                        $Tipo_Reserva = "E"; //Entrega a domicilio
                        $Reserva_SN = "S";
                    }
                    $PAGO_EN_TIENDA = "N";
                    if (($cliente_completo == "M020089") && (trim($order->payment) == "Pago en tienda")) {
                        $Tipo_Reserva = "R"; //Recogida en Tienda
                        $Reserva_SN = "S";
                        $PAGO_EN_TIENDA = "S";
                    }
                    $tienda_recogida = "0000000";
                    $Tipo_Recogida = "";
                    if ($cliente_completo == "M020089") {
                        $Tipo_Recogida = (new CarrierCore($order->id_carrier, Context::getContext()->language->id))->name;
                        if (trim($Tipo_Recogida == "Recogida en tienda")) {
                            //$cookie = Context::getContext()->cookie;
                            //$tienda_recogida = Db::getInstance()->getValue("select note from " . _DB_PREFIX_ . "store where id_store = " . $cookie->store);
                            $tienda_recogida = Db::getInstance()->getValue("select note from " . _DB_PREFIX_ . "store where Trim(name) = '" . Trim($address_dev->company) . "'");
                        }
                    }
                    $tpvs = array();
                    if ($PAGO_EN_TIENDA == "S") {
                        $tpvs[] = $tienda_recogida;
                    }
                    foreach ($tiendas as $tienda) {
                        $tpvs[] = $tienda["id_tpv_status2"];
                    }
                    $mensaje_regalo=$order->gift.'@'.$order->gift_message;
                    foreach ($order->getProducts() as $product) {
                        //cogemos el EAN del producto
                        $ean13 = $product["product_ean13"];
                        $tpvs_stock_stockusado = array();

                        if (Configuration::get('STATUS2_PETICION_TRASPASO') == 1) {
                            $parameters_stocks = new stdClass();
                            $parameters_stocks->P_sEnt = array(
                                "IdTPV" => implode(",", $tpvs),
                                "EAN" => $ean13,
                                "DesglosadoXTPV" => 1
                            );
                            $return = $soapClient->WS_ST_Stocks($parameters_stocks);
                            $ob = simplexml_load_string($return->WS_ST_StocksResult->any);
                            $json = json_encode($ob);
                            $configDatastock = json_decode($json, true);

                            //cargamos un array con los tpvs encontrados, el stock actual, y 0 en stock usado
                            if (isset($configDatastock["WS_ST_Stocks_DT"][0])) {
                                foreach ($configDatastock["WS_ST_Stocks_DT"] as $stock) {
                                    if ($stock["Stock"] > 0) {
                                        if (!isset($tpvs_stock_stockusado[$stock["Id_Tpv"]])) {
                                            $tpvs_stock_stockusado [] = array("Id_Tpv" => $stock["Id_Tpv"], "Stock" => (int)$stock["Stock"], "Stockusado" => 0);
                                        }
                                    }
                                }
                            } else { // solo hay un registro de stock
                                if (isset($configDatastock["WS_ST_Stocks_DT"]["Stock"])) {
                                    if ($configDatastock["WS_ST_Stocks_DT"]["Stock"] > 0) {
                                        $tpvs_stock_stockusado [] = array("Id_Tpv" => $configDatastock["WS_ST_Stocks_DT"]["Id_Tpv"], "Stock" => (int)$configDatastock["WS_ST_Stocks_DT"]["Stock"], "Stockusado" => 0);
                                    }
                                }
                            }
                        }

                        for ($i = 1; $i <= (int)$product["product_quantity"]; $i++) {
                            $lineas++;
                            $original_price = ProductCore::getPriceStatic($product["product_id"], true, $product["product_attribute_id"], 6, null, false, false);
                            $original_price_sin = ($product["original_product_price"] / $order->conversion_rate) * $this->devuelve_impuestos();
                            $original_price = $original_price_sin * 1;//$product["product_quantity"];
                            $id_tpvpeticion = Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE', null, $id_shop_group, $id_shop);
                            $id_tpvstock = "";
                            if ($id_tpvpeticion == "M014429" ) {
                                $id_tpvpeticion = 'M014401';
                            }
                            if (Configuration::get('STATUS2_PETICION_TRASPASO') == 1) {
                                //modG: si $configDatastock["WS_ST_Stocks_DT"] no era multi-nivel no recogia bien el idtpv
                                $encontrado = false;
                                if ($PAGO_EN_TIENDA == "S") {
                                    $tpvs_seleccion_stocks = explode(';', $tienda_recogida . ";" . Configuration::get('STATUS2_TPVS_STOCK_PREFERIDOS', null, $id_shop_group, $id_shop));
                                } else {
                                    $tpvs_seleccion_stocks = explode(';', Configuration::get('STATUS2_TPVS_STOCK_PREFERIDOS', null, $id_shop_group, $id_shop));
                                }
                                if (!empty($tpvs_seleccion_stocks)) {
                                    foreach ($tpvs_seleccion_stocks as $tpv_stock_peticion) {
                                        foreach ($tpvs_stock_stockusado as $Tpv => $stock) {
                                            if ($stock["Id_Tpv"] == $tpv_stock_peticion) {
                                                if ($stock["Stock"] > 0 && ($stock["Stockusado"] < $stock["Stock"])) {
                                                    $id_tpvpeticion = $stock["Id_Tpv"];
                                                    $encontrado = true;
                                                    $tpvs_stock_stockusado[$Tpv]["Stockusado"]++;
                                                    break;
                                                }
                                            }
                                        }
                                        if ($encontrado) break;
                                    }
                                    if (!$encontrado) { // no se ha encontrado stock en los tpvs preferidos, seguimos buscando en todos los tpvs
                                        //LOLA CASADEMNUNT PRIMERO BUSCARA POR EL ALMACEN POR QUE ASI ESTARA CONFIGURADO EN LOS TPVS PETICION
                                        //SI NO ENCUENTRA STOCK LLEGARA A ESTA RUTINA DONDE SE ORDENARA EL RESTO DE TPVS POR CANTIAD DE STOCK DESCENDIENTE
                                        if ($cliente == "M0144") {
                                            $sorted = $this->array_orderby($tpvs_stock_stockusado, 'Stock', SORT_DESC); // ordena los stocks de mayor a menor por unidades
                                            $tpvs_stock_stockusado = $sorted;
                                        }
                                        foreach ($tpvs_stock_stockusado as $Tpv => $stock) {
                                            if ($stock["Stock"] > 0 && ($stock["Stockusado"] < $stock["Stock"])) {
                                                $id_tpvpeticion = $stock["Id_Tpv"];
                                                $encontrado = true;
                                                $tpvs_stock_stockusado[$Tpv]["Stockusado"]++;
                                                break;
                                            }
                                        }
                                    }
                                } else { // no hay tpvs preferidos para el stock
                                    //LOLA CASADEMNUNT
                                    //SI NO HAY TPV'S PREFERIDOS TAMBIEN SE ORDENARA EL RESTO DE TPVS POR CANTIDAD DE STOCK DESCENDIENTE
                                    if ($cliente == "M0144") {
                                        $sorted = $this->array_orderby($tpvs_stock_stockusado, 'Stock', SORT_DESC); // ordena los stocks de mayor a menor por unidades
                                        $tpvs_stock_stockusado = $sorted;
                                    }
                                    foreach ($tpvs_stock_stockusado as $Tpv => $stock) {
                                        if ($stock["Stock"] > 0 && ($stock["Stockusado"] < $stock["Stock"])) {
                                            $id_tpvpeticion = $stock["Id_Tpv"];
                                            $encontrado = true;
                                            $tpvs_stock_stockusado[$Tpv]["Stockusado"]++;
                                            break;
                                        }
                                    }
                                }
                                if ($PAGO_EN_TIENDA == "S") {
                                    $id_tpvstock = $id_tpvpeticion;
                                    $id_tpvpeticion = $tienda_recogida;
                                }
                                if (!$encontrado) {
                                    $mensaje = "Status2: No se ha encontrado Stock del  Artículo " . $product["product_name"] . " Ref: " . $product["product_reference"] . " para el pedido con referencia: " . $order->reference;
                                    $stocks_no_encontrados[] = $mensaje;
                                } else {
                                    if ($PAGO_EN_TIENDA == "S") {
                                        $mensaje_encontrado = "Reserva a " . $id_tpvpeticion . " Stock de " . $id_tpvstock . " -> Artículo :" . $product["product_reference"] . "\n\r" . $product["product_name"];
                                    } else {
                                        $mensaje_encontrado = "Petición a " . $id_tpvpeticion . " -> Artículo :" . $product["product_reference"] . "\n\r" . $product["product_name"];
                                    }
                                    $stocks_encontrados[] = $mensaje_encontrado;
                                }
                                if (($id_tpvpeticion == "I0B2B00") || ($id_tpvpeticion == "I0B2C00")) {
                                    $id_tpvpeticion = 'I999900';
                                }
                            }
                            if (count($total_carrito_desc) > 0) {
                                $total_from_prod = ProductCore::getPriceStatic($product["product_id"], true, $product["product_attribute_id"], 6);
                                $total_from_prod = (float)$total_from_prod * 1;//$product["product_quantity"];
                                $total_from_prod = (($product["original_product_price"] / $order->conversion_rate) * $this->devuelve_impuestos()) * 1;// $product["product_quantity"];
                                $DescuentoPromocion = 0;
                                $precio_sin_descontar_nada = $order->total_products_wt / $order->conversion_rate;
                                $descuento_total = $order->total_discounts_tax_incl / $order->conversion_rate;
                                $porcentage_resta = (float)($descuento_total / $precio_sin_descontar_nada) * 100;
                                //$DescuentoPromocion += $porcentage_resta;
                                if ($product["reduction_percent"] != 0) {
                                    //$DescuentoPromocion = (float) $product["reduction_percent"] + $porcentage_resta;
                                    $DescuentoPromocion += $product["reduction_percent"];
                                } else if ($product["reduction_amount_tax_incl"] != 0) {
                                    $porcentaje_amount = (float)(($product["reduction_amount_tax_incl"] / $order->conversion_rate) / (($product["original_product_price"] / $order->conversion_rate) * $this->devuelve_impuestos())) * 100;
                                    $DescuentoPromocion += $porcentaje_amount;
                                }
                                $product["total_price_tax_incl_withcart"] = (float)$total_from_prod - ((float)($total_from_prod * sprintf('%0.2f', $DescuentoPromocion)) / 100);
                                $product["total_price_tax_incl_withcart"] = (float)$product["total_price_tax_incl_withcart"] - ((float)($product["total_price_tax_incl_withcart"] * $porcentage_resta) / 100);
                                $DescuentoPromocion = (float)($product["total_price_tax_incl_withcart"] / $original_price) * 100;
                                $DescuentoPromocion = 100 - $DescuentoPromocion;
                            } else {
                                $product["total_price_tax_incl_withcart"] = $product["total_price_tax_incl"] / $order->conversion_rate;
                                $DescuentoPromocion = (float)($product["total_price_tax_incl_withcart"] / $original_price) * 100;
                                $DescuentoPromocion = 100 - $DescuentoPromocion;
                            }


                            $product["total_price_tax_incl_withcart"] = $product["total_price_tax_incl_withcart"];
                            $total_prods_eval += (float)$product["total_price_tax_incl_withcart"];
                            //dump($order->total_paid);
                            $DescuentoPromocion = $DescuentoPromocion;
                            $descuento_solo_calculo = (1 - (Tools::ps_round(($order->total_paid / $order->conversion_rate) - ($order->total_shipping_tax_incl / $order->conversion_rate), 2) / Tools::ps_round($order->total_products_wt / $order->conversion_rate, 2))) * 100;
                            //dump($product);
                            if ($descuento_solo_calculo >= 0) {
                                if ((($order->total_paid / $order->conversion_rate) - ($order->total_shipping_tax_incl / $order->conversion_rate)) != ($order->total_products_wt / $order->conversion_rate)) {
                                    $paid_no_trans = ($order->total_paid / $order->conversion_rate) - ($order->total_shipping_tax_incl / $order->conversion_rate);
                                    $precio_final = Tools::ps_round($product['unit_price_tax_incl'] / $order->conversion_rate, 2) - (($descuento_solo_calculo * Tools::ps_round($product['unit_price_tax_incl'] / $order->conversion_rate, 2)) / 100);
                                    //$precio_final = $precio_final * 1;//$product["product_quantity"];
                                } else {
                                    $precio_final = Tools::ps_round(($product['unit_price_tax_incl'] / $order->conversion_rate) * 1, 2);//$product["product_quantity"], 2);
                                }

                            } else {
                                $precio_final = Tools::ps_round($original_price, 2) - (($descuento_solo_calculo * Tools::ps_round($original_price, 2)) / 100);
                                //$precio_final = $precio_final * 1;//$product["product_quantity"];
                            }
                            $total_paid += Tools::ps_round($precio_final, 2);
                            $descuento_final = $this->Devuelve_porcentaje_en_positivo((((Tools::ps_round($original_price, 2) - $precio_final) / Tools::ps_round($original_price, 2)) * 100 <= 0) ? 0 : ((Tools::ps_round($original_price, 2) - $precio_final) / Tools::ps_round($original_price, 2)) * 100);
                            $lineas_vent["clsSt_Venta_Lineas"][] = array(
                                "NumLin_Vta" => $lineas,
                                "Reserva_SN" => $Reserva_SN,
                                "TipoReserva" => $Tipo_Reserva,
                                "EAN" => $ean13,
                                //"Articulo" => $reference,
                                //"IdTalla" => $combis_talla,
                                "Cantidad" => 1,
                                "PorIVA" => Configuration::get("STATUS2_IMPUESTOS_VENTA", null, $id_shop_group, $id_shop),
                                "Precio" => Tools::ps_round($original_price_sin, 2),
                                "SubtotalLinea" => Tools::ps_round($precio_final, 2),
                                "DescuentoPromocion" => $descuento_final,
                                "Referencia" => $mensaje_regalo,
                                "IdTPVPeticion" => $id_tpvpeticion,
                                "Id_ECommerce" => $order->reference,
                                "IdTPVStock" => $id_tpvstock
                            );
                        }
                    }
                    //modG: si hay transporte enviamos una línea extra para el transporte
                    if ($order->total_shipping_tax_incl > 0) {
                        $total_paid += $order->total_shipping_tax_incl / $order->conversion_rate;
                        $lineas++;
                        $lineas_vent["clsSt_Venta_Lineas"][] = array(
                            "NumLin_Vta" => $lineas,
                            "Reserva_SN" => $Reserva_SN,
                            "TipoReserva" => $Tipo_Reserva,
                            "EAN" => Configuration::get('ECOMMANDSYNC_STATUS2_EANTRANS', null, $id_shop_group, $id_shop),
                            "Articulo" => 'TRANSPORTE',
                            "IdTalla" => "0",
                            "Cantidad" => 1,
                            "PorIVA" => Configuration::get("STATUS2_IMPUESTOS_VENTA", null, $id_shop_group, $id_shop),
                            "Precio" => Tools::ps_round($order->total_shipping_tax_incl / $order->conversion_rate, 2),//$original_price,
                            "SubtotalLinea" => Tools::ps_round($order->total_shipping_tax_incl / $order->conversion_rate, 2),
                            "DescuentoPromocion" => 0,
                            //"Referencia" => 'TRANSPORTE',
                            "IdTPVPeticion" => $id_tpvpeticion,
                            "Id_ECommerce" => $order->reference,
                            "IdTPVStock" => $id_tpvstock
                        );
                        $total_prods_eval += 5;
                    }
                    // 1 centimo
                    if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.01) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.01;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.01) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.01;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    }
                    // 2 centimos
                    if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.02) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.02;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.02) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.02;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    }
                    // 3 centimos
                    if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.03) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.03;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.03) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.03;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    }
                    //4 centimos
                    if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.04) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.04;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.04) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.04;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    }
                    // 5 centimos
                    if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.05) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.05;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.05) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.05;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    }
                    $total_paid = Tools::ps_round(($order->total_paid / $order->conversion_rate), 2);
                    $param_venta = new stdClass();
                    $date = explode(" ", $order->date_add);
                    $Numero_De_Cliente = "";
                    if ($cliente == "WB000") { //Wehbe ID_CLIENTE
                        $metodos_de_pago = OrderPayment::getByOrderId($order->id);
                        foreach ($metodos_de_pago as $metodo_de_pago) {
                            /*
                            echo("ID -> " . $order->id);
                            echo '<br>';
                            echo("Referencia ->" . $metodo_de_pago->order_reference);
                            echo '<br>';
                            echo("Nombre  -> " . $metodo_de_pago->payment_method);
                            echo '<br>';
                            echo("Numero de Cliente -> " . $metodo_de_pago->card_number);
                            echo '<br>';
                            */
                            $Numero_De_Cliente = $metodo_de_pago->card_number;
                            //PrestaShopLogger::addLog("CARD NUMBER : " . $Numero_De_Cliente. " FIDELIZACION NUMBER : " . $id_status. " " . (string)date("j, Y, g:i a")  , 2);
                        }
                    }
                    $envio_direccion = $address_dev->address1;
                    $envio_cp = $address_dev->postcode;
                    $envio_localidad = $address_dev->city;
                    if ($cliente == "M0153") { // SAN JURJO si el transportista es sanjurjo es recogida en tienda y se pone en envio direccion el nombre de la tienda donde recoger
                        $Nombre_transporte = (new CarrierCore($order->id_carrier, Context::getContext()->language->id))->name;
                        if (substr($Nombre_transporte, 0, 8) == "Sanjurjo") {
                            $envio_direccion = $Nombre_transporte;
                            $envio_cp = "";
                            $envio_localidad = "";
                        }
                    }
                    $nombre = $customer->firstname;
                    $apellidos = $customer->lastname;
                    if ($cliente == "M0144") { //LOLA CASADEMUNT -> DATOS DE FACTURACION en Lola se envian los datos de nombre y apellidos de facturacion
                        $nombre = $address_bill->firstname;
                        $apellidos = $address_bill->lastname;
                    }
                    $nombre_envio = $address_dev->firstname;
                    if ($cliente == "M0200" && trim($Tipo_Recogida == "Recogida en tienda")) { // -> CONDOR SI ES RECOGIDA EN TIENDA SE RECUPERA EL NOMBRE DE LA TIENDA EN NOMBRE DE ENVIO
                        $nombre_envio = Trim($address_dev->company);
                    }
                    $param_venta->P_sText = array(
                        "IdTPV" => Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE', null, $id_shop_group, $id_shop),
                        "NumCaja" => 1,
                        "Almacen" => 1,
                        "FechaOperacion" => $date[0],
                        "HoraOperacion" => $date[1],
                        "Cliente" => $Numero_De_Cliente,
                        "ClienteFid" => $id_status,
                        "Nombre" => $nombre,
                        "Apellidos" => $apellidos,
                        "EMail" => $customer->email,
                        "Direccion" => $address_bill->address1,
                        "CodigoPostal" => $address_bill->postcode,
                        "Localidad" => $address_bill->city,
                        "Provincia" => !empty($address_bill->id_state) ? State::getNameById($address_bill->id_state) : "",
                        "Pais" => CountryCore::getIsoById(CountryCore::getIdByName(false, $address_bill->country)),
                        "Telefono" => empty($address_bill->phone) ? "0" : $address_bill->phone,
                        "Movil" => empty($address_bill->phone_mobile) ? "0" : $address_bill->phone_mobile,
                        "DNI" => empty($address_bill->dni) ? "0" : $address_bill->dni,
                        "EnvioNombre" =>  $nombre_envio,
                        "EnvioApellidos" => $address_dev->lastname,
                        "EnvioDireccion" => $envio_direccion,
                        "EnvioCodigoPostal" => $envio_cp,
                        "EnvioLocalidad" => $envio_localidad,
                        "EnvioProvincia" => !empty($address_dev->id_state) ? State::getNameById($address_dev->id_state) : "",
                        "EnvioPais" => CountryCore::getIsoById(CountryCore::getIdByName(false, $address_dev->country)),
                        "EnvioTelefono" => empty($address_dev->phone) ? "0" : $address_dev->phone,
                        "EnvioMovil" => empty($address_dev->phone_mobile) ? "0" : $address_dev->phone_mobile,
                        "Transporte" => $order->id_carrier.'@'.(new CarrierCore($order->id_carrier, Context::getContext()->language->id))->name,
                        "VentaLineas" => $lineas_vent,
                        "VentaFP" => array("clsSt_Venta_FP" => array(
                            "NumLin_FP" => 1,
                            "DescripcionFormaPago" => $PAGO_EN_TIENDA == "S" ? "PDF" : $order->payment,
                            "SubtotalFormaPago" => $total_paid
                        ))
                    );

                    $return = $soapClient->WS_ST_Ventas($param_venta);
                    $ob = simplexml_load_string($return->WS_ST_VentasResult->any);
                    $json = json_encode($ob);
                    $configData = json_decode($json, true);
                    if ($configData["WS_ST_Ventas_DT"]["ErrorLinea"] == "1") {
                        //envio email y log en caso de error
                        //dump("Status2: El pedido con referencia: " . $order->reference . " no se ha podido crear. Motivo: " . $configData["WS_ST_Ventas_DT"]["ErrorLineaDescripcion"]);
                        PrestaShopLogger::addLog("Status2: El pedido con referencia: " . $order->reference . " no se ha podido crear. Motivo: " . $configData["WS_ST_Ventas_DT"]["ErrorLineaDescripcion"], 2);
                        $msg = "El pedido con referencia: " . $order->reference . " no se ha podido crear. Motivo: " . $configData["WS_ST_Ventas_DT"]["ErrorLineaDescripcion"];
                        Db::getInstance()->execute("insert into " . _DB_PREFIX_ . "status2_pedidos(id_presta, correcto, informado) values(" . $order->id . ",0,0)");
                        mail(Configuration::get("PS_SHOP_EMAIL"), "Fallo Importacion Pedido", $msg);
                    } else {
                        //dump("Pedido importado");
                        if (!is_array($configData["WS_ST_Ventas_CA"]["EANTicket"]) || !is_array($configData["WS_ST_Ventas_CA"]["EANReserva"])) {
                            $customerthread = new CustomerThreadCore();
                            $customerthread->id_customer = $order->id_customer;
                            $customerthread->id_order = $order->id;
                            $customerthread->id_shop = 1;
                            $customerthread->id_contact = 0;
                            $customerthread->token = Tools::passwdGen(12);
                            $customerthread->id_product = 0;
                            $customerthread->id_lang = LanguageCore::getIdByIso('es');
                            $customerthread->email = (new Customer($order->id_customer))->email;
                            $customerthread->save();
                            $mensaje = new CustomerMessageCore();
                            $mensaje->id_customer_thread = $customerthread->id;
                            $mensaje->id_employee = 1;
                            $mensaje->private = 1;
                            $texto_message = $Reserva_SN == "N" ? "Ean generado: " . $configData["WS_ST_Ventas_CA"]["EANTicket"] . "\n\r" : "Ean generado: " . $configData["WS_ST_Ventas_CA"]["EANReserva"] . "\n\r";
                            $mensaje->message = $texto_message;
                            $mensaje->save();
                            //SEGUNDO MENSAJE CON LA BUSQUEDA DE STOCKS
                            if (!empty($stocks_encontrados)) {
                                $customerthread_stock = new CustomerThreadCore();
                                $customerthread_stock->id_customer = $order->id_customer;
                                $customerthread_stock->id_order = $order->id;
                                $customerthread_stock->id_shop = 1;
                                $customerthread_stock->id_contact = 0;
                                $customerthread_stock->token = Tools::passwdGen(12);
                                $customerthread_stock->id_product = 0;
                                $customerthread_stock->id_lang = LanguageCore::getIdByIso('es');
                                $customerthread_stock->email = (new Customer($order->id_customer))->email;
                                $customerthread_stock->save();
                                $mensaje_stock = new CustomerMessageCore();
                                $mensaje_stock->id_customer_thread = $customerthread_stock->id;
                                $mensaje_stock->id_employee = 1;
                                $mensaje_stock->private = 1;
                                $texto_message_stock = "";
                                foreach ($stocks_encontrados as $msg_stock_encontrado) {
                                    $texto_message_stock .= $msg_stock_encontrado . "\n\r";
                                }
                                $mensaje_stock->message = $texto_message_stock;
                                $mensaje_stock->save();
                            }
                        }
                        Db::getInstance()->execute("insert into " . _DB_PREFIX_ . "status2_pedidos(id_presta, correcto, informado) values(" . $order->id . ",1,0)");
                        //Grabamos en Log de Prestashop aquellos no se encontraron
                        if (!empty($stocks_no_encontrados)) {
                            foreach ($stocks_no_encontrados as $msg_error_stock_no_encontrado) {
                                PrestaShopLogger::addLog($msg_error_stock_no_encontrado, 2);
                            }
                        }
                    }
                } else {
                    //envio fallo subida - actualizacion de cliente
                    PrestaShopLogger::addLog("Status2: El usuario con email: " . $customer->email . " no se ha podido crear. Motivo: " . $return->WS_APP_Cliente_Fidelizacion_ActualizarResult, 2);
                    $msg = "El usuario con email: " . $customer->email . " no se ha podido crear. Motivo: " . $return->WS_APP_Cliente_Fidelizacion_ActualizarResult;
                    mail(Configuration::get("PS_SHOP_EMAIL"), "Fallo Importacion Cliente", $msg);
                }
            } else {
                /*
                if ($params["newOrderStatus"]->id == 7 || $params["newOrderStatus"]->id == 6) {
                    $order = new Order($params["id_order"]);
                    //var_dump($order);
                    //echo '<br>';
                    foreach ($order->getProducts() as $product) {
                        //  echo '<br>';
                        //  var_dump($product);
                        //  echo '<br>';
                    }
                }
                */
            }
        }else{
            PrestaShopLogger::addLog("Status2: No está habilitada la venta en el Grupo : ".  $id_shop_group . " Tienda : ". $id_shop , 2);
        }
        fin:
    }


    public function hookActionOrderHistoryAddAfter($params) {
        /*
        $new_os = $params['order_history'];
        $order_id = (int)$new_os->id_order;
        // ...
        // Add a new Order state.
        $orderHistory = new OrderHistory();
        $orderHistory->id_order = $order_id;
        $orderHistory->changeIdOrderState(20, (int)$order_id);
        $orderHistory->save();
        */
        $id_shop = (int)Context::getContext()->shop->id;
        $id_shop_group = (int)Shop::getContextShopGroupID();

        if (Configuration::get("STATUS2_CONTROL_ESTADOS", null, $id_shop_group, $id_shop) == 1) {
            $new_os = $params['order_history'];
            $order = new Order($new_os->id_order);
            if ((int)$order->current_state == 8 || (int)$order->current_state == 6 || (int)$order->current_state == 7) {
                return;
            }
            $result = Db::getInstance()->getValue("select max(informado) from " . _DB_PREFIX_ . "status2_pedidos where id_presta = '" . $order->id . "'");

            if ($result != 1) {
                if (Db::getInstance()->execute("insert into " . _DB_PREFIX_ . "status2_pedidos(id_presta, correcto, informado) values(" . $order->id . ",0,1)")) {
                    $result_2 = Db::getInstance()->getValue("select max(correcto) from " . _DB_PREFIX_ . "status2_pedidos where id_presta = '" . $order->id . "'");
                    $order->setCurrentState($result_2 == 1 ? Configuration::get("STATUS2_ID_ESTADO", null, $id_shop_group, $id_shop) :
                                                             Configuration::get("STATUS2_ID_ESTADO_ERROR", null, $id_shop_group, $id_shop));
                }
            }
        }
    }
    private function devuelve_impuestos()
    {

        return (1 + (float)(Configuration::get("STATUS2_IMPUESTOS") / 100));

    }
    private function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
                }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }
    private function Devuelve_porcentaje_en_positivo($porcentaje)
    {
        if ($porcentaje < 0) {
            $porcentaje = $porcentaje * -1;
        }
        return $porcentaje; // = sprintf('%0.2f', $porcentaje);
    }

    private function getCarrierName($id_order)
    {
        return Db::getInstance()->executeS('SELECT cl.name as carrier_name FROM '._DB_PREFIX_.'order_carrier oc LEFT JOIN '._DB_PREFIX_.'carrier cl	ON (oc.id_carrier = cl.id_carrier) WHERE oc.id_order = '.(int)$id_order);

    }

    public function hookActionOrderReturn($params)
    {
        /* Place your code here. */
        //$orderreturn = OrderReturnCore::getOrdersReturn($params["orderReturn"]->id_customer,$params["orderReturn"]->id_order);
        //$order = new Order($params["orderReturn"]->id_order);
        //$products = OrderReturnCore::getOrdersReturnProducts ($params["orderReturn"]->id,$order);
        //var_dump($products);
        $id_shop = (int)Context::getContext()->shop->id;
        $id_shop_group = (int)Shop::getContextShopGroupID();
        $orderreturn = OrderReturnCore::getOrdersReturn($params["orderReturn"]->id_customer,$params["orderReturn"]->id_order);
        //var_dump($orderreturn);
        //$order = new Order(42);
        $order = new Order($params["orderReturn"]->id_order);
        //var_dump( $order);
        $products = OrderReturnCore::getOrdersReturnProducts ($params["orderReturn"]->id,$order);

        $customer = new Customer($orderreturn[0]["id_customer"]);
        $id_status = Db::getInstance()->getValue("select id_status2 from " . _DB_PREFIX_ . "status2_customers where id_presta = " . $customer->id);
        $date = explode(" ",$orderreturn[0]["date_add"]);
        $lineas = 0;
        $lineas_rma = array("clsSt_Devolucion_Lineas" => array());
        foreach ($products as $product) {
            for ($i = 1; $i <= (int)$product["product_quantity"]; $i++) {
                $lineas++;
                $lineas_rma["clsSt_Devolucion_Lineas"][] = array(
                    "NumLin_Dev" => $lineas,
                    "Reserva_SN" => "",
                    "TipoReserva" => "",
                    "EAN" => $product['product_ean13'],
                    "SKU" => "",
                    "Clave" => "",
                    "NSerie" => "",
                    "MarcaArticulo" => "",
                    "Articulo" => "",
                    "Color" => "",
                    "Talla" => "",
                    "IdTalla" => "",
                    "IdTemporada" => "",
                    "Temporada" => "",
                    "Cantidad" => "1",
                    "Precio" => $product['unit_price_tax_incl'], //esta mal
                    "PorIVA" => $product['tax_rate'],
                    "SubtotalLinea" => "",
                    "DescuentoPromocion" => "",
                    "CodigoDtoPromocion" => "",
                    "DescuentoPuntos" => "",
                    "DescuentoRebajas" => "",
                    "MotivoDto" => "",
                    "Referencia" => "",
                    "IdTPVPeticion" => "",
                    "IdTPVStock" => "",
                    "Id_ECommerce_Original" => $orderreturn[0]['reference'],
                    "Id_ECommerce_Devolucion" => $orderreturn[0]['id_order_return']
                );
            }
        }

        $param_rma = new stdClass();
        $param_rma->P_sText = array(
            "IdTPV" => Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE', null, $id_shop_group, $id_shop),
            "NumCaja" => 1,
            "Almacen" => 1,
            "FechaOperacion" => $date[0],
            "HoraOperacion" => $date[1],
            "Cliente" => "",
            "ClienteFid" => $id_status,
            "EnvioNombre" => $customer->firstname,
            "EnvioApellidos" =>  $customer->lastname,
            "EnvioEMail" => $customer->email,
            "MotivoDevolucion" => $orderreturn[0]["question"],
            "NumeroTracking" => $orderreturn[0]["tracking_number"],
            "DevolucionLineas" => $lineas_rma
        );
        try {
            $soapClient = new SoapClient(Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN", null, $id_shop_group, $id_shop), array('trace' => true, 'connection_timeout' => 5000,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'keep_alive' => false,));
        } catch (SoapFault $soapClient) {
            /*
            echo "<br>";
            echo "Error LibXML :";
            var_dump(libxml_get_last_error());
            echo "<br>";
            echo "Error Soap :";
            var_dump($soapClient);
            echo "<br>";
            die("Imposible conectar");
            */
        }
        try {
            $return = $soapClient->WS_ST_Devoluciones ($param_rma);
            if (!isset($return)) {
                //die ("Error en la recepcion de los datos SOAP");
            }
        } catch (SoapFault $soapClient) {
            /*
            echo "<br>";
            echo "Error LibXML :";
            var_dump(libxml_get_last_error());
            echo "<br>";
            echo "Error Soap :";
            var_dump($soapClient);
            echo "<br>";
            die("Imposible conectar");
            */
        }
        //var_dump($return);

    }

    public function hookActionCustomerAccountAdd($params)
    {

        /* Place your code here. */
    }

    public function hookActionProductAdd($params)
    {
        /* Place your code here. */
    }

    public function hookActionObjectManufacturerAddAfter($params)
    {
        /* Place your code here. */
    }

    public function hookActionObjectSupplierAddAfter($params)
    {
        /* Place your code here. */
    }

    public function hookAdminOrder($params)
    {
        /* Place your code here. */
    }

}
