<?php

include_once dirname(__FILE__) . '/../../../config/config.inc.php';
include_once dirname(__FILE__) . '/../../../init.php';

    $sql= "DROP TABLE `"._DB_PREFIX_."status2_pedidos`";
    DB::getInstance()->execute($sql);

    $sql = "create table if not exists "._DB_PREFIX_."status2_pedidos (id int(11) not null AUTO_INCREMENT,
    id_presta int(11),correcto int(11),informado int(11), primary key(id))";
    DB::getInstance()->execute($sql);

    $sql= "DROP TABLE `"._DB_PREFIX_."preimport_status2`";
    DB::getInstance()->execute($sql);
     
    $sql= "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."preimport_status2` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `Id_MarcaArticulo` varchar(30) DEFAULT NULL,
      `Id_Temporada` varchar(30) DEFAULT NULL,
      `Id_Articulo` varchar(30) DEFAULT NULL,
      `Id_Color` varchar(30) DEFAULT NULL,
      `Id_Talla` varchar(30) DEFAULT NULL,
      `EAN` varchar(15) DEFAULT NULL,
      `Id_Tipo` varchar(30) DEFAULT NULL,
      `FecUltModif` varchar(50) DEFAULT NULL,
      `Descripcion_1` varchar(80) DEFAULT NULL,
      `Precio` varchar(20) DEFAULT NULL,
      `PrecioRebajas` varchar(30) DEFAULT NULL,
      `DescTemp` varchar(20) DEFAULT NULL,
      `Familia` varchar(20) DEFAULT NULL,
      `Subfamilia` varchar(30) DEFAULT NULL,
      `Seccion` varchar(30) DEFAULT NULL,
      `Tipo` varchar(30) DEFAULT NULL,
      `Descripcion_Color` varchar(30) DEFAULT NULL,
      `Descripcion_IdTalla` varchar(30) DEFAULT NULL,
      `OrdenTemporada` varchar(25) DEFAULT NULL,
      `Imagen` varchar(90) DEFAULT NULL,
      `Descripcion_2` varchar(80) DEFAULT NULL,
      `Id_Paleta` varchar(60) DEFAULT NULL,
      `Descatalogado` varchar(20) DEFAULT NULL,
      `Descripcion_Familia` varchar(40) DEFAULT NULL,
      `Descripcion_SubFamilia` varchar(40) DEFAULT NULL,
      `CodigoEstadistico` varchar(50) DEFAULT NULL,
      `Descripcion_CodEst` varchar(50) DEFAULT NULL,
      `Filtro_Ad_1` varchar(30) DEFAULT NULL,
      `Descripcion_Fd1` varchar(30) DEFAULT NULL,
      `Filtro_Ad_2` varchar(30) DEFAULT NULL,
      `Descripcion_Fd2` varchar(25) DEFAULT NULL,
      `Filtro_Ad_3` varchar(30) DEFAULT NULL,
      `Descripcion_Fd3` varchar(30) DEFAULT NULL,
      `Descripcion_Marca` varchar(30) DEFAULT NULL,
      `Descripcion_Seccion` varchar(25) DEFAULT NULL,
      `Descripcion_Tipo` varchar(30) DEFAULT NULL,
      `ECommerce` varchar(10) DEFAULT NULL,
      `Descripcion_WEB_Articulo` varchar(300) DEFAULT NULL,
      `Descripcion_WEB_Color` varchar(30) DEFAULT NULL,
      `ECommerce_CT` varchar(10) DEFAULT NULL,
      `Resuelto` varchar(1) DEFAULT NULL,
      `uniq_id` varchar(250) DEFAULT NULL,
      `id_shop` int(2) DEFAULT NULL,
      `id_group` int(2) DEFAULT NULL,
      `Id_Idioma` varchar(5) DEFAULT NULL,
      `Descripcion_Color_Defecto` varchar(30) DEFAULT NULL,
      `Descripcion_IdTalla_Defecto` varchar(30) DEFAULT NULL,
      `Composicion` varchar(80) DEFAULT NULL,
      `Lavado` varchar(30) DEFAULT NULL,
      `Guia_Tallas` int(3) DEFAULT NULL,
      `Descripcion_Composicion_Defecto` varchar(30) DEFAULT NULL,
      `Agrupacion` varchar(1) DEFAULT NULL,
      `Web_URL` varchar(150) DEFAULT NULL,
      `Web_Categoria` varchar(3) DEFAULT NULL,
      PRIMARY KEY (`id`)
    )";
    DB::getInstance()->execute($sql);
    
    $sql= "SHOW COLUMNS FROM `"._DB_PREFIX_."preimport_status2`";
    $value =DB::getInstance()->execute($sql);
    var_dump ($value);

    $sql= "CREATE INDEX ID_IDX ON "._DB_PREFIX_."preimport_status2(id)";
    DB::getInstance()->execute($sql);

    $sql= "DROP TABLE `"._DB_PREFIX_."preimport_status2_temp`";
    DB::getInstance()->execute($sql);
        
         $sql= "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."preimport_status2_temp` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `Id_MarcaArticulo` varchar(30) DEFAULT NULL,
          `Id_Temporada` varchar(30) DEFAULT NULL,
          `Id_Articulo` varchar(30) DEFAULT NULL,
          `Id_Color` varchar(30) DEFAULT NULL,
          `Id_Talla` varchar(30) DEFAULT NULL,
          `EAN` varchar(15) DEFAULT NULL,
          `Id_Tipo` varchar(30) DEFAULT NULL,
          `FecUltModif` varchar(50) DEFAULT NULL,
          `Descripcion_1` varchar(80) DEFAULT NULL,
          `Precio` varchar(20) DEFAULT NULL,
          `PrecioRebajas` varchar(30) DEFAULT NULL,
          `DescTemp` varchar(20) DEFAULT NULL,
          `Familia` varchar(20) DEFAULT NULL,
          `Subfamilia` varchar(30) DEFAULT NULL,
          `Seccion` varchar(30) DEFAULT NULL,
          `Tipo` varchar(30) DEFAULT NULL,
          `Descripcion_Color` varchar(30) DEFAULT NULL,
          `Descripcion_IdTalla` varchar(30) DEFAULT NULL,
          `OrdenTemporada` varchar(25) DEFAULT NULL,
          `Imagen` varchar(90) DEFAULT NULL,
          `Descripcion_2` varchar(80) DEFAULT NULL,
          `Id_Paleta` varchar(60) DEFAULT NULL,
          `Descatalogado` varchar(20) DEFAULT NULL,
          `Descripcion_Familia` varchar(40) DEFAULT NULL,
          `Descripcion_SubFamilia` varchar(40) DEFAULT NULL,
          `CodigoEstadistico` varchar(50) DEFAULT NULL,
          `Descripcion_CodEst` varchar(50) DEFAULT NULL,
          `Filtro_Ad_1` varchar(30) DEFAULT NULL,
          `Descripcion_Fd1` varchar(30) DEFAULT NULL,
          `Filtro_Ad_2` varchar(30) DEFAULT NULL,
          `Descripcion_Fd2` varchar(25) DEFAULT NULL,
          `Filtro_Ad_3` varchar(30) DEFAULT NULL,
          `Descripcion_Fd3` varchar(30) DEFAULT NULL,
          `Descripcion_Marca` varchar(30) DEFAULT NULL,
          `Descripcion_Seccion` varchar(25) DEFAULT NULL,
          `Descripcion_Tipo` varchar(30) DEFAULT NULL,
          `ECommerce` varchar(10) DEFAULT NULL,
          `Descripcion_WEB_Articulo` varchar(300) DEFAULT NULL,
          `Descripcion_WEB_Color` varchar(30) DEFAULT NULL,
          `ECommerce_CT` varchar(10) DEFAULT NULL,
          `Resuelto` varchar(1) DEFAULT NULL,
          `uniq_id` varchar(250) DEFAULT NULL,
          `Id_Idioma` varchar(5) DEFAULT NULL,
          `Descripcion_Color_Defecto` varchar(30) DEFAULT NULL,
          `Descripcion_IdTalla_Defecto` varchar(30) DEFAULT NULL,
          `Composicion` varchar(80) DEFAULT NULL,
          `Lavado` varchar(30) DEFAULT NULL,
          `Guia_Tallas` int(3) DEFAULT NULL,
          `Descripcion_Composicion_Defecto` varchar(30) DEFAULT NULL,
          `No_Procesado` varchar(1) DEFAULT NULL,
          `Agrupacion` varchar(1) DEFAULT NULL,
          `Web_URL` varchar(150) DEFAULT NULL,
          `Web_Categoria` varchar(3) DEFAULT NULL,
          PRIMARY KEY (`id`)
        )";

        DB::getInstance()->execute($sql);
    $sql= "SHOW COLUMNS FROM `"._DB_PREFIX_."preimport_status2_temp`";
    $value= DB::getInstance()->execute($sql);
    var_dump ($value);

die ("proceso finalizado");

