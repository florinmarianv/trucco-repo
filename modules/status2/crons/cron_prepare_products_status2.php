<?php
include_once dirname(__FILE__) . '/../../../config/config.inc.php';
include_once dirname(__FILE__) . '/../../../init.php';
//include_once dirname(__FILE__) . '/../utilities/process/ImportProductFullProcess.php';



function _debug($message)
{
    echo $message . '<br />';
    flush();
}

require_once('instance.php');

$cliente = substr(Configuration::get("ECOMMANDSYNC_STATUS2_KEYCLIENTE"), 0, 5);
if ($cliente != "M0197" && $cliente != "M0181") { // CONTROL DE PID SOLO EN LINUX
    $pid = new pid('/tmp');
    if ($pid->already_running) {
        PrestaShopLogger::addLog('INICIO SOLAPADO Prepare product NO SE EJECUTARA ' . (string)date("j, Y, g:i a"), 2);
        _debug("El proceso prepare_product, ya se está ejecutando, espere a que termine ......");
        exit;
    }
}
$shops = Shop::getShopsCollection(false);
foreach ($shops as $shop) {
    if (Configuration::get("STATUS2_TIENDA_ACTIVA", null, $shop->id_shop_group, $shop->id) == 1) {
        _debug('TOKEN : ' . Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN", null, $shop->id_shop_group, $shop->id));
        _debug('REGISTROS : ' . Configuration::get("STATUS2_CANTIDAD_DE_REGISTROS_WEBSERVICE", null, $shop->id_shop_group, $shop->id));
        _debug('CLIENTE : ' . Configuration::get("ECOMMANDSYNC_STATUS2_KEYCLIENTE", null, $shop->id_shop_group, $shop->id));
        _debug('TARIFA : ' . Configuration::get('STATUS2_TARIFA', null, $shop->id_shop_group, $shop->id));
        _debug('FECHA PRODUCTOS : ' . Configuration::get("STATUS2_FECHA_CONTROL_PRODUCTS", null, $shop->id_shop_group, $shop->id));
        _debug('TIPO CONSULTA : ' . Configuration::get('STATUS2_TIPOCONSULTA', null, $shop->id_shop_group, $shop->id));

        PrestaShopLogger::addLog('INICIO Prepare product ' . (string)date("j, Y, g:i a"), 1);
        _debug("Ejecutando prepare_product.....");
        ini_set('default_socket_timeout', Configuration::get("STATUS2_TIMEOUT", null, $shop->id_shop_group, $shop->id));
        ini_set('max_execution_time', '0');
        try {
            $soapClient = new SoapClient(Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN", null, $shop->id_shop_group, $shop->id), array('trace' => true, 'connection_timeout' => 5000,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'keep_alive' => false,));
        } catch (SoapFault $soapClient) {
            _debug ("Error LibXML :");
            var_dump(libxml_get_last_error());
            echo "<br>";
            _debug ("Error Soap :");
            var_dump($soapClient);
            echo "<br>";
            _debug("Imposible conectar");
            continue;
        }
        $time_start_global = microtime(true);
        $numeroregistrossolicitados = Configuration::get("STATUS2_CANTIDAD_DE_REGISTROS_WEBSERVICE", null, $shop->id_shop_group, $shop->id);
        $fecha_control = Configuration::get("STATUS2_FECHA_CONTROL_PRODUCTS", null, $shop->id_shop_group, $shop->id);
        $languages = explode(';', Configuration::get('STATUS2_IDIOMAS', null, $shop->id_shop_group, $shop->id));
        $Idioma_Defecto = Configuration::get('STATUS2_IDIOMA_DEFECTO', null, $shop->id_shop_group, $shop->id);
        $Es_Insitu = (Configuration::get("ECOMMANDSYNC_STATUS2_KEYCLIENTE", null, $shop->id_shop_group, $shop->id) == 'I0B2C00' || Configuration::get("ECOMMANDSYNC_STATUS2_KEYCLIENTE", null, $shop->id_shop_group, $shop->id) == 'I0B2B00' ? 'S' : 'N');
        // SE CREA UN SEMAFORO PARA TRUCCO Y QUITAR DOS DIGITOS DEL CODIGO DEL ARTICULO
        foreach ($languages as $language) {

            $total_registros_recibidos = 0;
            $total_productos_procesados = 0;
            //  do {
            $time_start_parcial = microtime(true);
            $parameters = new stdClass();
            $parameters->P_sEnt = array(
                "IdTPV" => Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE', null, $shop->id_shop_group, $shop->id),
                "Tarifa" => Configuration::get('STATUS2_TARIFA', null, $shop->id_shop_group, $shop->id),
                "NumRegistros" => $numeroregistrossolicitados,
                "Date_Time_Stamp" => $fecha_control,
                "TipoConsulta" => Configuration::get('STATUS2_TIPOCONSULTA', null, $shop->id_shop_group, $shop->id),
                "ECommerce" => 1,
                "RegistroInicial" => Configuration::get('STATUS2_REGISTROS_ARTICULOS', null, $shop->id_shop_group, $shop->id),
                "Id_Idioma" => $language
            );

            try {
                $return = $soapClient->WS_APP_Precio_APP($parameters);
                if (!isset($return)) {
                    _debug ("Error en la recepcion de los datos SOAP");
                    continue;
                }
            } catch (SoapFault $soapClient) {

                _debug ("Error LibXML :");
                var_dump(libxml_get_last_error());
                echo "<br>";
                _debug ("Error Soap :");
                var_dump($soapClient);
                die("Imposible conectar");
            }

            $ob = simplexml_load_string($return->WS_APP_Precio_APPResult->any);
            if (!isset($ob)) {
                _debug ("Error en la conversion a XML");
                continue;
            }
            $total_registros_recibidos = $ob->count();
            _debug("Total registros recibidos: " . $total_registros_recibidos);
            if ($total_registros_recibidos > 0) {
                $json = json_encode($ob);
                $configData = json_decode($json, true);
                if (!isset($configData["WS_APP_Precio_APP_DT"])) {
                    _debug ("SIN REGISTROS");
                    continue;
                }
                if (isset($configData["WS_APP_Precio_APP_DT"]["ErrorLinea"])) {
                    var_dump($configData["WS_APP_Precio_APP_DT"]["ErrorLinea"]);
                    continue;
                }
                $products = array();
                if ($total_registros_recibidos > 1) {
                    $products = $configData["WS_APP_Precio_APP_DT"];
                    $last_element = end($configData['WS_APP_Precio_APP_DT']);
                } else {
                    $products = $configData;
                    $last_element = $configData['WS_APP_Precio_APP_DT']["FecUltModif"];
                }
                foreach ($products as $product) {
                    if (is_array($product)) {
                        array_walk($product, function (&$value) {
                            if (is_array($value) && count($value) == 0) {
                                $value = "";
                            }
                            $value = (string)$value;
                            //$value = stripslashes($value);
                            $value = addslashes($value);
                            $value = str_replace("'", " ", $value);
                        });
                        extract($product);
                        // esto controla que el ean no este duplicado, si esta salta al siguiente
                        if (isset($product["EAN"]) && strlen(trim($product["EAN"])) > 1 && (trim($product["EAN"] != ''))) {
                            if (!empty(Db::getInstance()->getValue("select id from " . _DB_PREFIX_ . "preimport_status2 where EAN = '" . $product["EAN"] . "' and (Resuelto = '' or isnull(Resuelto)) and (id_shop = '" . $shop->id . "' and id_group = '" . $shop->id_shop_group . "') and (Id_Idioma = '" . $language . "')"))) {
                                $message = "EAN Repetido: " . $product["EAN"];
                                _debug($message);
                                continue; // con esto se salta a la siguiente vuelta del do
                            }
                            $Resuelto = "";
                            $uniq_id = uniqid();
                            $id_shop = $shop->id;
                            $id_group = $shop->id_shop_group;
                            $Id_Idioma = $language;
                            $Id_Articulo = $Es_Insitu == 'N' ? $product["Id_Articulo"] : substr(trim($product["Id_Articulo"]),0,strlen(trim($product["Id_Articulo"]))-2);
                            Db::getInstance()->insert("preimport_status2", compact("Id_MarcaArticulo", "Id_Temporada", "Id_Articulo", "Id_Color", "Id_Talla", "EAN", "Id_Tipo", "FecUltModif", "Descripcion_1", "Precio", "PrecioRebajas", "DescTemp", "Familia", "Subfamilia", "Seccion", "Tipo", "Descripcion_Color", "Descripcion_IdTalla", "OrdenTemporada", "Imagen", "Descripcion_2", "Id_Paleta", "Descatalogado", "Descripcion_Familia", "Descripcion_SubFamilia", "CodigoEstadistico", "Descripcion_CodEst", "Filtro_Ad_1", "Descripcion_Fd1", "Filtro_Ad_2", "Descripcion_Fd2", "Filtro_Ad_3", "Descripcion_Fd3", "Descripcion_Marca", "Descripcion_Seccion", "Descripcion_Tipo", "ECommerce", "Descripcion_WEB_Articulo", "Descripcion_WEB_Color", "ECommerce_CT", "Resuelto", "uniq_id", "id_shop", "id_group", "Id_Idioma", "Descripcion_Color_Defecto", "Composicion", "Lavado", "Guia_Tallas", "Descripcion_Composicion_Defecto", "Agrupacion","Web_URL","Web_Categoria"), true);

                        } else {
                            $message = 'EAN INVÁLIDO ->Marca: ' . $product["Id_MarcaArticulo"] . ' Id Articulo: ' . $product["Id_Articulo"] . ' Id Color: ' . $product["Id_Color"] . ' Id Talla: ' . $product["Id_Talla"] . ' Id TIPO EAN: ' . $product["Id_Tipo"] . ' EAN: ' . $product["EAN"];
                            _debug($message);
                        }
                    }
                }
                if ($language == $Idioma_Defecto) {
                    if ($total_registros_recibidos != $numeroregistrossolicitados) {
                        if ($total_registros_recibidos > 1) {
                            if (isset($last_element["FecUltModif"])) {
                                $ultima_fecha = $last_element["FecUltModif"];
                            }else{
                                _debug('<b>Erro en la fecha date time stamp del registo:</b>');
                                var_dump($last_element);
                                die;
                            }
                        }else {
                            if (isset($last_element)) {
                                $ultima_fecha = $last_element;
                            }else{
                                _debug ('<b>Erro en la fecha date time stamp vacio </b>');
                                die;
                            }
                        }
                        //if (isset($product["FecUltModif"])) {
                        $ver = (float)phpversion();
                        try {
                            if ($ver >= 7.0) {
                                _debug("7.0 PHP");
                                _debug("ULTIMA FECHA :".$ultima_fecha);
                                $fecha1 = new DateTime($ultima_fecha);
                                $fecha2 = $fecha1->format('YmdHisv');
                                $fecha2 += 1;
                                $fecha3 = new DateTime (substr($fecha2, 0, 14));
                                $fecha4 = $fecha3->format('Y-m-d H:i:s.') . substr($fecha2, 14, 17);
                            } else {
                                _debug("6.0 PHP");
                                _debug("ULTIMA FECHA :" .$ultima_fecha);
                                $fecha_sin_milisg = new DateTime($ultima_fecha);
                                $fecha_sin_milisg->modify('+1 second');
                                $fecha4 = $fecha_sin_milisg->format('Y-m-d H:i:s');
                            }
                        } catch (Exception $e) {
                            $fecha4 = $ultima_fecha;
                        }
                        _debug("FECHA INCREMENTADA :".$fecha4);
                        Configuration::updateValue('STATUS2_FECHA_CONTROL_PRODUCTS', $fecha4, false, $shop->id_shop_group, $shop->id);
                        Configuration::updateValue('STATUS2_REGISTROS_ARTICULOS', 1, null, $shop->id_shop_group, $shop->id);
                        $fecha_control = $fecha4;
                    }
                    else{
                        $siguiente_bloque_registros = (int)Configuration::get('STATUS2_REGISTROS_ARTICULOS', null, $shop->id_shop_group, $shop->id);
                        $siguiente_bloque_registros += (int)$numeroregistrossolicitados;
                        Configuration::updateValue('STATUS2_REGISTROS_ARTICULOS', $siguiente_bloque_registros, null, $shop->id_shop_group, $shop->id);
                        _debug("Paginación registro :" . Configuration::get('STATUS2_REGISTROS_ARTICULOS', null, $shop->id_shop_group, $shop->id));
                    }
                }
                PrestaShopLogger::addLog('BUCLE BD Producto ' . (string)date("j, Y, g:i a") . ' Registros :' . $total_registros_recibidos . ' , Tiempo :' . Tools::ps_round((microtime(true) -
                        $time_start_parcial), 2) . 'Date_Time_Stamp: ' . Configuration::get('STATUS2_FECHA_CONTROL_PRODUCTS') . ' RegistroBucle: ' . Configuration::get('STATUS2_REGISTROS_ARTICULOS'), 1);
                _debug("Ultimo Elemento del BUCLE-> Date Time Stamp:" . $fecha_control . " Idioma : " . $language);
            } else {
                PrestaShopLogger::addLog('BD Producto ' . (string)date("j, Y, g:i a") . ',no se han encontrado productos', 1);
            }
        }
        $total_productos_procesados = $total_productos_procesados + $total_registros_recibidos;
//    } while ($numeroregistrossolicitados == $total_registros_recibidos);
        //PrestaShopLogger::addLog('FINAL BD Producto ' . (string)date("j, Y, g:i a") . ' Registros :' . $total_productos_procesados . ' , Tiempo :' . Tools::ps_round((microtime(true) -
        //        $time_start_global), 2) . 'Date_Time_Stamp: ' . Configuration::get('STATUS2_FECHA_CONTROL_PRODUCTS', null, $shop->id_shop_group, $shop->id) . ' RegistroBucle: ' . Configuration::get('STATUS2_REGISTROS_ARTICULOS', null, $shop->id_shop_group, $shop->id), 1);
        _debug('<b>Total registros procesados:</b>' . $total_productos_procesados);
    }else{
        PrestaShopLogger::addLog('Tienda nº ' . (string)$shop->id . ' no activa -> Prepare Products', 2);
        _debug ('<br><b>Tienda nº ' . (string)$shop->id . ' no activa -> Prepare Products <br>');
    }
}
die ("Fin Preparacion");
