<?php
/**
 * Created by PhpStorm.
 * User: Sergi Martinez
 * Date: 05/06/2020
 * Time: 23:38
 */
include_once dirname(__FILE__) . '/../../../config/config.inc.php';
include_once dirname(__FILE__) . '/../../../init.php';


(new RemarkOrder())->run();


class RemarkOrder
{
    public function run()
    {
        if (!empty(Tools::getValue("id_pedido"))) {
            
            $order = Tools::getValue("id_pedido");
            
            $result = Db::getInstance()->getValue("select max(correcto) from " . _DB_PREFIX_ . "status2_pedidos where id_presta = '" . $order . "'");

            if ($result == 1) {
                $result2 = Db::getInstance()->execute("update  " . _DB_PREFIX_ . "status2_pedidos set correcto = 0 where id_presta = '".$order."'");
                echo $result2 . "pedido ".$order. " actualizado ";
            }else{
                die ("pedido no encontrado o en estado 0");
            }
        }
        else {
            die ("id_pedido no informado.");
        }
    }
}
