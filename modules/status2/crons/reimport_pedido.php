<?php
/**
 * Created by PhpStorm.
 * User: Sergi Martinez
 * Date: 04/04/2020
 * Time: 14:38
 */
include_once dirname(__FILE__) . '/../../../config/config.inc.php';
include_once dirname(__FILE__) . '/../../../init.php';


(new ReimportOrder())->run();


class ReimportOrder
{

    public function run()
    {

        if (!empty(Tools::getValue("id_pedido"))) {

            $id_shop = empty(Tools::getValue("id_shop")) ?  (int)Context::getContext()->shop->id : Tools::getValue("id_shop");
            $id_shop_group = empty(Tools::getValue("id_shop_group")) ?  (int)Shop::getContextShopGroupID() : Tools::getValue("id_shop_group");
            $order = new Order(Tools::getValue("id_pedido"));
            //var_dump($order);
            //die;
            $result = Db::getInstance()->getValue("select max(correcto) from " . _DB_PREFIX_ . "status2_pedidos where id_presta = '" . $order->id . "'");
            if ($result == 1) {
                echo("El pedido " . $order->id . " ya ha sido subido a Status2");
                die();
            }
            if((int)$order->current_state == 8 || (int)$order->current_state == 6 || (int)$order->current_state == 7){
                echo("El estado del pedido " . $order->id . " no es acorde para ser subido a status2");
                die();
            }
            $address_dev = new Address($order->id_address_delivery);
            //EN LOLACASADEMUNT NOS PASARAN AQUI EL NUMERO DE TIENDA DONDE SE RECOGE EL PEDIDO
            //var_dump($address_dev->other);
            //die;
            $address_bill = new Address($order->id_address_invoice);
            $customer = new Customer($order->id_customer);
            $cliente = substr(Configuration::get("ECOMMANDSYNC_STATUS2_KEYCLIENTE", null, $id_shop_group, $id_shop), 0, 5);
            $id_status = null;
            if ($cliente == "WB000") {
                $id_status = Db::getInstance()->getValue("select id_codigocliente_wehbe from " . _DB_PREFIX_ . "customer where id_customer = " . $customer->id);
            }
            if (!$id_status) {
                $id_status = Db::getInstance()->getValue("select id_status2 from " . _DB_PREFIX_ . "status2_customers where id_presta = " . $customer->id);
            }

            try {
                $soapClient = new SoapClient(Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN", null, $id_shop_group, $id_shop), array('trace' => true,
                    'connection_timeout' => 5000,
                    'cache_wsdl' => WSDL_CACHE_NONE,
                    'keep_alive' => false,));
            } catch (SoapFault $soapClient) {
                PrestaShopLogger::addLog("Status2: IMPOSIBLE CONECTAR CON EL SERVIDOR " . time(), 4);
                goto fin;
                //die("Imposible conectar");
            }
            $parameters = new stdClass();
            if ($id_status) {
                $parameters->P_sCodigoCliente = $id_status;
            }
            $parameters->P_sNombre = $customer->firstname;
            $parameters->P_sApellidos = $customer->lastname;
            $parameters->P_sSexo = $customer->id_gender;
            $parameters->P_seMail = $customer->email;
            $parameters->P_sFechaNacimiento = empty($customer->birthday) ? "00000000" : str_replace("-", "", $customer->birthday);
//direcciones
            $parameters->P_sDireccion = $address_bill->address1;
            $parameters->P_sCodigoPostal = $address_bill->postcode;
            $parameters->P_sLocalidad = $address_bill->city;
            $parameters->P_sProvincia = !empty($address_bill->id_state) ? State::getNameById($address_bill->id_state) : "";
            $parameters->P_sPais = CountryCore::getIsoById(CountryCore::getIdByName(false, $address_dev->country));
            $parameters->P_sTelefono = empty($address_bill->phone) ? "0" : $address_bill->phone;
            $parameters->P_sMovil = empty($address_bill->phone_mobile) ? "0" : $address_bill->phone_mobile;
            $parameters->P_sDNI = empty($address_bill->dni) ? "0" : $address_bill->dni;
            $parameters->P_sDireccion_Envio = $address_dev->address1;
            $parameters->P_sCodigoPostal_Envio = $address_dev->postcode;
            $parameters->P_sLocalidad_Envio = $address_dev->city;
            $parameters->P_sProvincia_Envio = !empty($address_dev->id_state) ? State::getNameById($address_dev->id_state) : "";
            $parameters->P_sPais_Envio = CountryCore::getIsoById(CountryCore::getIdByName(false, $address_dev->country));
            $parameters->P_sTelefono_Envio = $address_dev->phone;
            $parameters->P_sMovil_Envio = $address_dev->phone_mobile;
            $return = $soapClient->WS_APP_Cliente_Fidelizacion_Actualizar($parameters);
            $resultado = substr($return->WS_APP_Cliente_Fidelizacion_ActualizarResult, 6, 4);
            //echo $resultado;
            if (is_numeric($resultado)) {
                if (!$id_status) {
                    $id_status = $return->WS_APP_Cliente_Fidelizacion_ActualizarResult;
                    Db::getInstance()->execute("insert into " . _DB_PREFIX_ . "status2_customers(id_status2, id_presta) values(" . $id_status . "," . $customer->id . ")");
                }
                $lineas = 0;
                $lineas_vent = array("clsSt_Venta_Lineas" => array());
                $cart_rules = $order->getCartRules();
                $total_discount_import = 0;
                $total_paid = 0;
                //modG: total ahora es un array de porcentajes
                $total_carrito_desc = [];
                $discount_types = [];
                //modG: Añadimos las cartrules al array de descuentos
                foreach ($cart_rules as $cart_rule) {
                    //$total_carrito_desc += $cart_rule["value"];
                    $cartRule_obj = new CartRule($cart_rule["id_cart_rule"]);
                    //$total_discount_import += $cart_rule["value"];
                    $groups = $cartRule_obj->getProductRuleGroups();
                    if (is_array($groups) && !empty($groups)) {
                        foreach ($groups as $group) {
                            if (isset($group['product_rules']) && !empty($group['product_rules'])) {
                                foreach ($group['product_rules'] as $item) {
                                    $discount_types[] = $item;
                                }
                            }
                        }
                    }
                    if ($cartRule_obj->reduction_percent != 0) {
                        $total_carrito_desc[] = $cartRule_obj->reduction_percent;
                    } else if ($cartRule_obj->reduction_amount != 0) {
                        $percent = (($cartRule_obj->reduction_amount * 100) / ($order->total_products_wt / $order->conversion_rate));
                        $total_carrito_desc [] = $percent;
                    }
                }
                $total_discount_import = $order->total_discounts / $order->conversion_rate;
                $total_prods_eval = 0;
                $stocks_no_encontrados = array();
                $stocks_encontrados = array();
                //Cogemos la tiendas para la busqueda de stock
                $tiendas = Db::getInstance()->executeS("select id_tpv_status2 from " . _DB_PREFIX_ . "status2_tpvs where selected = 1 and id_shop = '".$id_shop."' and id_group = '".$id_shop_group."'");
                $cliente_completo = Configuration::get("ECOMMANDSYNC_STATUS2_KEYCLIENTE", null, $id_shop_group, $id_shop);
                $Tipo_Reserva = "";
                $Reserva_SN = "N";
                if (($cliente == "I0B2B") || ($cliente == "I0B2C")) {
                    $Tipo_Reserva = "E";
                    $Reserva_SN = "S";
                }
                $PAGO_EN_TIENDA = "N";
                if (($cliente_completo == "M020089") && (trim($order->payment) == "Pago en tienda")) {
                    $Tipo_Reserva = "R";
                    $Reserva_SN = "S";
                    $PAGO_EN_TIENDA = "S";
                }
                $tienda_recogida = "0000000";
                $Tipo_Recogida = "";
                if ($cliente_completo == "M020089"){
                    $Tipo_Recogida = (new CarrierCore($order->id_carrier, Context::getContext()->language->id))->name;
                    if (trim($Tipo_Recogida == "Recogida en tienda")) {
                        //$cookie = Context::getContext()->cookie;
                        //$tienda_recogida = Db::getInstance()->getValue("select note from " . _DB_PREFIX_ . "store where id_store = " . $cookie->store);
                        $tienda_recogida = Db::getInstance()->getValue("select note from " . _DB_PREFIX_ . "store where Trim(name) = '" . Trim($address_dev->company). "'");
                    }
                }
                $tpvs = array();
                if ($PAGO_EN_TIENDA == "S"){
                    $tpvs[] = $tienda_recogida;
                }
                foreach ($tiendas as $tienda) {
                    $tpvs[] = $tienda["id_tpv_status2"];
                }
                $mensaje_regalo=$order->gift.'@'.$order->gift_message;
                foreach ($order->getProducts() as $product) {
                    //cogemos el EAN del producto
                    $ean13 = $product["product_ean13"];
                    $tpvs_stock_stockusado = Array();

                    if (Configuration::get('STATUS2_PETICION_TRASPASO') == 1) {
                        $parameters_stocks = new stdClass();
                        $parameters_stocks->P_sEnt = array(
                            "IdTPV" => implode(",", $tpvs),
                            "EAN" => $ean13,
                            "DesglosadoXTPV" => 1
                        );
                        $return = $soapClient->WS_ST_Stocks($parameters_stocks);
                        $ob = simplexml_load_string($return->WS_ST_StocksResult->any);
                        $json = json_encode($ob);
                        $configDatastock = json_decode($json, true);

                        //cargamos un array con los tpvs encontrados, el stock actual, y 0 en stock usado
                        if (isset($configDatastock["WS_ST_Stocks_DT"][0])) {
                            foreach ($configDatastock["WS_ST_Stocks_DT"] as $stock) {
                                if ($stock["Stock"] > 0) {
                                    if (!isset($tpvs_stock_stockusado[$stock["Id_Tpv"]])) {
                                        $tpvs_stock_stockusado [] = Array("Id_Tpv" => $stock["Id_Tpv"], "Stock" => (int)$stock["Stock"], "Stockusado" => 0);
                                    }
                                }
                            }
                        } else { // solo hay un registro de stock
                            if (isset($configDatastock["WS_ST_Stocks_DT"]["Stock"])) {
                                if ($configDatastock["WS_ST_Stocks_DT"]["Stock"] > 0) {
                                    $tpvs_stock_stockusado [] = Array("Id_Tpv" => $configDatastock["WS_ST_Stocks_DT"]["Id_Tpv"],"Stock" => (int)$configDatastock["WS_ST_Stocks_DT"]["Stock"], "Stockusado" => 0);
                                }
                            }
                        }
                    }

                    for ($i = 1; $i <= (int)$product["product_quantity"]; $i++) {
                        $lineas++;
                        $original_price = ProductCore::getPriceStatic($product["product_id"], true, $product["product_attribute_id"], 6, null, false, false);
                        $original_price_sin = ($product["original_product_price"] / $order->conversion_rate) * $this->devuelve_impuestos();
                        $original_price = $original_price_sin * 1 ;//$product["product_quantity"];
                        $id_tpvpeticion = Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE', null, $id_shop_group, $id_shop);
                        $id_tpvstock = "";
                        if ($id_tpvpeticion == "M014429" ) {
                            $id_tpvpeticion = 'M014401';
                        }
                        if (Configuration::get('STATUS2_PETICION_TRASPASO') == 1) {
                            //modG: si $configDatastock["WS_ST_Stocks_DT"] no era multi-nivel no recogia bien el idtpv
                            $encontrado = false;
                            if ($PAGO_EN_TIENDA == "S"){
                                $tpvs_seleccion_stocks = explode(';', $tienda_recogida .";".Configuration::get('STATUS2_TPVS_STOCK_PREFERIDOS', null, $id_shop_group, $id_shop));
                            }
                            else{
                                $tpvs_seleccion_stocks = explode(';', Configuration::get('STATUS2_TPVS_STOCK_PREFERIDOS', null, $id_shop_group, $id_shop));
                            }
                            if (!empty($tpvs_seleccion_stocks)) {
                                foreach ($tpvs_seleccion_stocks as $tpv_stock_peticion) {
                                    foreach ($tpvs_stock_stockusado as $Tpv => $stock) {
                                        if ($stock["Id_Tpv"] == $tpv_stock_peticion) {
                                            if ($stock["Stock"] > 0 && ($stock["Stockusado"] < $stock["Stock"])) {
                                                $id_tpvpeticion = $stock["Id_Tpv"];
                                                $encontrado = true;
                                                $tpvs_stock_stockusado[$Tpv]["Stockusado"]++;
                                                break;
                                            }
                                        }
                                    }
                                    if ($encontrado) break;
                                }
                                if (!$encontrado) { // no se ha encontrado stock en los tpvs preferidos, seguimos buscando en todos los tpvs
                                    //LOLA CASADEMNUNT PRIMERO BUSCARA POR EL ALMACEN POR QUE ASI ESTARA CONFIGURADO EN LOS TPVS PETICION
                                    //SI NO ENCUENTRA STOCK LLEGARA A ESTA RUTINA DONDE SE ORDENARA EL RESTO DE TPVS POR CANTIAD DE STOCK DESCENDIENTE
                                    if ($cliente == "M0144") {
                                        $sorted = $this->array_orderby($tpvs_stock_stockusado, 'Stock', SORT_DESC); // ordena los stocks de mayor a menor por unidades
                                        $tpvs_stock_stockusado = $sorted;
                                    }
                                    foreach ($tpvs_stock_stockusado as $Tpv => $stock ) {
                                        if ($stock["Stock"] > 0 && ($stock["Stockusado"] < $stock["Stock"])) {
                                            $id_tpvpeticion = $stock["Id_Tpv"];
                                            $encontrado = true;
                                            $tpvs_stock_stockusado[$Tpv]["Stockusado"]++;
                                            break;
                                        }
                                    }
                                }
                            } else { // no hay tpvs preferidos para el stock
                                //LOLA CASADEMNUNT
                                //SI NO HAY TPV'S PREFERIDOS TAMBIEN SE ORDENARA EL RESTO DE TPVS POR CANTIDAD DE STOCK DESCENDIENTE
                                if ($cliente == "M0144") {
                                    $sorted = $this->array_orderby($tpvs_stock_stockusado, 'Stock', SORT_DESC); // ordena los stocks de mayor a menor por unidades
                                    $tpvs_stock_stockusado = $sorted;
                                }
                                foreach ($tpvs_stock_stockusado as $Tpv => $stock ) {
                                    if ($stock["Stock"] > 0 && ($stock["Stockusado"] < $stock["Stock"])) {
                                        $id_tpvpeticion = $stock["Id_Tpv"];
                                        $encontrado = true;
                                        $tpvs_stock_stockusado[$Tpv]["Stockusado"]++;
                                        break;
                                    }
                                }
                            }
                            if ($PAGO_EN_TIENDA == "S"){
                                $id_tpvstock = $id_tpvpeticion;
                                $id_tpvpeticion = $tienda_recogida;
                            }
                            if (!$encontrado) {
                                $mensaje = "Status2: No se ha encontrado Stock del  Artículo " . $product["product_name"] . " Ref: " . $product["product_reference"] . " para el pedido con referencia: " . $order->reference;
                                $stocks_no_encontrados[] = $mensaje;
                            } else {
                                if ($PAGO_EN_TIENDA == "S"){
                                    $mensaje_encontrado = "Reserva a " . $id_tpvpeticion . " Stock de ".$id_tpvstock ." -> Artículo :" . $product["product_reference"] . "\n\r" . $product["product_name"];
                                }else{
                                    $mensaje_encontrado = "Petición a " . $id_tpvpeticion . " -> Artículo :" . $product["product_reference"] . "\n\r" . $product["product_name"];
                                }
                                $stocks_encontrados[] = $mensaje_encontrado;
                            }
                            if (($id_tpvpeticion == "I0B2B00") || ($id_tpvpeticion == "I0B2C00")) {
                                $id_tpvpeticion = 'I999900';
                            }
                        }
                        if (count($total_carrito_desc) > 0) {
                            $total_from_prod = ProductCore::getPriceStatic($product["product_id"], true, $product["product_attribute_id"], 6);
                            $total_from_prod = (float)$total_from_prod * 1;//$product["product_quantity"];
                            $total_from_prod = (($product["original_product_price"] / $order->conversion_rate) * $this->devuelve_impuestos()) * 1;// $product["product_quantity"];
                            $DescuentoPromocion = 0;
                            $precio_sin_descontar_nada = $order->total_products_wt / $order->conversion_rate;
                            $descuento_total = $order->total_discounts_tax_incl / $order->conversion_rate;
                            $porcentage_resta = (float)($descuento_total / $precio_sin_descontar_nada) * 100;
                            //$DescuentoPromocion += $porcentage_resta;
                            if ($product["reduction_percent"] != 0) {
                                //$DescuentoPromocion = (float) $product["reduction_percent"] + $porcentage_resta;
                                $DescuentoPromocion += $product["reduction_percent"];
                            } else if ($product["reduction_amount_tax_incl"] != 0) {
                                $porcentaje_amount = (float)(($product["reduction_amount_tax_incl"]/ $order->conversion_rate) / (($product["original_product_price"] / $order->conversion_rate) * $this->devuelve_impuestos())) * 100;
                                $DescuentoPromocion += $porcentaje_amount;
                            }
                            $product["total_price_tax_incl_withcart"] = (float)$total_from_prod - ((float)($total_from_prod * sprintf('%0.2f', $DescuentoPromocion)) / 100);
                            $product["total_price_tax_incl_withcart"] = (float)$product["total_price_tax_incl_withcart"] - ((float)($product["total_price_tax_incl_withcart"] * $porcentage_resta) / 100);
                            $DescuentoPromocion = (float)($product["total_price_tax_incl_withcart"] / $original_price) * 100;
                            $DescuentoPromocion = 100 - $DescuentoPromocion;
                        } else {
                            $product["total_price_tax_incl_withcart"] = $product["total_price_tax_incl"] / $order->conversion_rate;
                            $DescuentoPromocion = (float)($product["total_price_tax_incl_withcart"]  / $original_price) * 100;
                            $DescuentoPromocion = 100 - $DescuentoPromocion;
                        }


                        $product["total_price_tax_incl_withcart"] = $product["total_price_tax_incl_withcart"];
                        $total_prods_eval += (float)$product["total_price_tax_incl_withcart"];
                        //dump($order->total_paid);
                        $DescuentoPromocion = $DescuentoPromocion;
                        $descuento_solo_calculo = (1 - (Tools::ps_round(($order->total_paid / $order->conversion_rate) - ($order->total_shipping_tax_incl / $order->conversion_rate), 2) / Tools::ps_round($order->total_products_wt / $order->conversion_rate, 2))) * 100;
                        //dump($product);
                        if ($descuento_solo_calculo >= 0) {
                            if ((($order->total_paid / $order->conversion_rate ) - ($order->total_shipping_tax_incl / $order->conversion_rate)) != ($order->total_products_wt / $order->conversion_rate)) {
                                $paid_no_trans = ($order->total_paid / $order->conversion_rate) - ($order->total_shipping_tax_incl / $order->conversion_rate);
                                $precio_final = Tools::ps_round($product['unit_price_tax_incl'] / $order->conversion_rate, 2) - (($descuento_solo_calculo * Tools::ps_round($product['unit_price_tax_incl'] / $order->conversion_rate, 2)) / 100);
                                //$precio_final = $precio_final * 1;//$product["product_quantity"];
                            } else {
                                $precio_final = Tools::ps_round(($product['unit_price_tax_incl'] / $order->conversion_rate) * 1,2);//$product["product_quantity"], 2);
                            }

                        } else {
                            $precio_final = Tools::ps_round($original_price , 2) - (($descuento_solo_calculo * Tools::ps_round($original_price, 2)) / 100);
                            //$precio_final = $precio_final * 1;//$product["product_quantity"];
                        }
                        $total_paid += Tools::ps_round($precio_final, 2);
                        $descuento_final = $this->Devuelve_porcentaje_en_positivo((((Tools::ps_round($original_price, 2) - $precio_final) / Tools::ps_round($original_price, 2)) * 100 <= 0) ? 0 : ((Tools::ps_round($original_price, 2) - $precio_final) / Tools::ps_round($original_price, 2)) * 100);
                        $lineas_vent["clsSt_Venta_Lineas"][] = array(
                            "NumLin_Vta" => $lineas,
                            "Reserva_SN" => $Reserva_SN,
                            "TipoReserva" => $Tipo_Reserva,
                            "EAN" => $ean13,
                            //"Articulo" => $reference,
                            //"IdTalla" => $combis_talla,
                            "Cantidad" => 1,
                            "PorIVA" => Configuration::get("STATUS2_IMPUESTOS_VENTA", null, $id_shop_group, $id_shop),
                            "Precio" => Tools::ps_round($original_price_sin, 2),
                            "SubtotalLinea" => Tools::ps_round($precio_final, 2),
                            "DescuentoPromocion" => $descuento_final,
                            "Referencia" => $mensaje_regalo,
                            "IdTPVPeticion" => $id_tpvpeticion,
                            "Id_ECommerce" => $order->reference,
                            "IdTPVStock" => $id_tpvstock
                        );
                    }
                }
                //modG: si hay transporte enviamos una línea extra para el transporte
                if ($order->total_shipping_tax_incl > 0) {
                    $total_paid += $order->total_shipping_tax_incl / $order->conversion_rate;
                    $lineas++;
                    $lineas_vent["clsSt_Venta_Lineas"][] = array(
                        "NumLin_Vta" => $lineas,
                        "Reserva_SN" => $Reserva_SN,
                        "TipoReserva" => $Tipo_Reserva,
                        "EAN" => Configuration::get('ECOMMANDSYNC_STATUS2_EANTRANS', null, $id_shop_group, $id_shop),
                        "Articulo" => 'TRANSPORTE',
                        "IdTalla" => "0",
                        "Cantidad" => 1,
                        "PorIVA" => Configuration::get("STATUS2_IMPUESTOS_VENTA", null, $id_shop_group, $id_shop),
                        "Precio" => Tools::ps_round($order->total_shipping_tax_incl / $order->conversion_rate,2),//$original_price,
                        "SubtotalLinea" => Tools::ps_round($order->total_shipping_tax_incl / $order->conversion_rate,2),
                        "DescuentoPromocion" => 0,
                        //"Referencia" => 'TRANSPORTE',
                        "IdTPVPeticion" => $id_tpvpeticion,
                        "Id_ECommerce" => $order->reference,
                        "IdTPVStock" => $id_tpvstock
                    );
                    $total_prods_eval += 5;
                }
                // 1 centimo
                if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.01) {
                    $total_paid = $total_paid;
                    $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.01;
                    $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.01) {
                    $total_paid = $total_paid;
                    $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.01;
                    $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                }
                // 2 centimos
                if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.02) {
                    $total_paid = $total_paid;
                    $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.02;
                    $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.02) {
                    $total_paid = $total_paid;
                    $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.02;
                    $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                }
                // 3 centimos
                if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.03) {
                    $total_paid = $total_paid;
                    $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.03;
                    $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.03) {
                    $total_paid = $total_paid;
                    $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.03;
                    $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                }
                //4 centimos
                if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.04) {
                    $total_paid = $total_paid;
                    $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.04;
                    $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.04) {
                    $total_paid = $total_paid;
                    $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.04;
                    $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                }
                // 5 centimos
                if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.05) {
                    $total_paid = $total_paid;
                    $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.05;
                    $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.05) {
                    $total_paid = $total_paid;
                    $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.05;
                    $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                }
                $total_paid = Tools::ps_round(($order->total_paid / $order->conversion_rate),2);
                $param_venta = new stdClass();
                $date = explode(" ", $order->date_add);
                $Numero_De_Cliente = "";
                if ($cliente == "WB000") { //Wehbe ID_CLIENTE
                    $metodos_de_pago = OrderPayment::getByOrderId($order->id);
                    foreach ($metodos_de_pago as $metodo_de_pago) {
                        /*
                        echo("ID -> " . $order->id);
                        echo '<br>';
                        echo("Referencia ->" . $metodo_de_pago->order_reference);
                        echo '<br>';
                        echo("Nombre  -> " . $metodo_de_pago->payment_method);
                        echo '<br>';
                        echo("Numero de Cliente -> " . $metodo_de_pago->card_number);
                        echo '<br>';
                        */
                        $Numero_De_Cliente = $metodo_de_pago->card_number;
                        //PrestaShopLogger::addLog("CARD NUMBER : " . $Numero_De_Cliente. " FIDELIZACION NUMBER : " . $id_status. " " . (string)date("j, Y, g:i a")  , 2);
                    }
                }
                $envio_direccion = $address_dev->address1;
                $envio_cp = $address_dev->postcode;
                $envio_localidad = $address_dev->city;
                if ($cliente == "M0153") { // SAN JURJO si el transportista es sanjurjo es recogida en tienda y se pone en envio direccion el nombre de la tienda donde recoger
                    $Nombre_transporte = (new CarrierCore($order->id_carrier, Context::getContext()->language->id))->name;
                    if (substr($Nombre_transporte,0,8) == "Sanjurjo"){
                        $envio_direccion = $Nombre_transporte;
                        $envio_cp = "";
                        $envio_localidad = "";
                    }
                }
                $nombre =  $customer->firstname;
                $apellidos = $customer->lastname;
                if($cliente == "M0144") { //LOLA CASADEMUNT -> DATOS DE FACTURACION en Lola se envian los datos de nombre y apellidos de facturacion
                    $nombre = $address_bill->firstname;
                    $apellidos= $address_bill->lastname;
                }
                $nombre_envio = $address_dev->firstname;
                if ($cliente == "M0200" && trim($Tipo_Recogida == "Recogida en tienda")) { // -> CONDOR SI ES RECOGIDA EN TIENDA SE RECUPERA EL NOMBRE DE LA TIENDA EN NOMBRE DE ENVIO
                    $nombre_envio = Trim($address_dev->company);
                }
                $param_venta->P_sText = array(
                    "IdTPV" => Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE', null, $id_shop_group, $id_shop),
                    "NumCaja" => 1,
                    "Almacen" => 1,
                    "FechaOperacion" => $date[0],
                    "HoraOperacion" => $date[1],
                    "Cliente" => $Numero_De_Cliente,
                    "ClienteFid" => $id_status,
                    "Nombre" => $nombre,
                    "Apellidos" => $apellidos,
                    "EMail" => $customer->email,
                    "Direccion" => $address_bill->address1,
                    "CodigoPostal" => $address_bill->postcode,
                    "Localidad" => $address_bill->city,
                    "Provincia" => !empty($address_bill->id_state) ? State::getNameById($address_bill->id_state) : "",
                    "Pais" => CountryCore::getIsoById(CountryCore::getIdByName(false, $address_bill->country)),
                    "Telefono" => empty($address_bill->phone) ? "0" : $address_bill->phone,
                    "Movil" => empty($address_bill->phone_mobile) ? "0" : $address_bill->phone_mobile,
                    "DNI" => empty($address_bill->dni) ? "0" : $address_bill->dni,
                    "EnvioNombre" => $nombre_envio,
                    "EnvioApellidos" => $address_dev->lastname,
                    "EnvioDireccion" => $envio_direccion,
                    "EnvioCodigoPostal" => $envio_cp,
                    "EnvioLocalidad" => $envio_localidad,
                    "EnvioProvincia" => !empty($address_dev->id_state) ? State::getNameById($address_dev->id_state) : "",
                    "EnvioPais" => CountryCore::getIsoById(CountryCore::getIdByName(false, $address_dev->country)),
                    "EnvioTelefono" => empty($address_dev->phone) ? "0" : $address_dev->phone,
                    "EnvioMovil" => empty($address_dev->phone_mobile) ? "0" : $address_dev->phone_mobile,
                    "Transporte" => $order->id_carrier.'@'.(new CarrierCore($order->id_carrier, Context::getContext()->language->id))->name,
                    "VentaLineas" => $lineas_vent,
                    "VentaFP" => array("clsSt_Venta_FP" => array(
                        "NumLin_FP" => 1,
                        "DescripcionFormaPago" => $PAGO_EN_TIENDA== "S" ? "PDF" : $order->payment,
                        "SubtotalFormaPago" => $total_paid
                    ))
                );
                var_dump ("venta: ", $param_venta);
                $return = $soapClient->WS_ST_Ventas($param_venta);
                $ob = simplexml_load_string($return->WS_ST_VentasResult->any);
                $json = json_encode($ob);
                $configData = json_decode($json, true);
                if ($configData["WS_ST_Ventas_DT"]["ErrorLinea"] == "1") {
                    //envio email y log en caso de error
                    echo("Status2: El pedido con referencia: " . $order->reference . " no se ha podido crear. Motivo: " . $configData["WS_ST_Ventas_DT"]["ErrorLineaDescripcion"]);
                    PrestaShopLogger::addLog("Status2: El pedido con referencia: " . $order->reference . " no se ha podido crear. Motivo: " . $configData["WS_ST_Ventas_DT"]["ErrorLineaDescripcion"], 2);
                    $msg = "El pedido con referencia: " . $order->reference . " no se ha podido crear. Motivo: " . $configData["WS_ST_Ventas_DT"]["ErrorLineaDescripcion"];
                    Db::getInstance()->execute("insert into " . _DB_PREFIX_ . "status2_pedidos(id_presta, correcto, informado) values(" . $order->id . ",0,0)");
                    //mail(Configuration::get("PS_SHOP_EMAIL"),"Fallo Importacion Pedido",$msg);
                } else {
                    echo("Pedido importado");
                    if(!is_array($configData["WS_ST_Ventas_CA"]["EANTicket"]) || !is_array($configData["WS_ST_Ventas_CA"]["EANReserva"])){
                        $customerthread = new CustomerThreadCore();
                        $customerthread->id_customer = $order->id_customer;
                        $customerthread->id_order = $order->id;
                        $customerthread->id_shop = 1;
                        $customerthread->id_contact = 0;
                        $customerthread->token = Tools::passwdGen(12);
                        $customerthread->id_product = 0;
                        $customerthread->id_lang = LanguageCore::getIdByIso('es');
                        $customerthread->email = (new Customer($order->id_customer))->email;
                        $customerthread->save();
                        $mensaje = new CustomerMessageCore();
                        $mensaje->id_customer_thread = $customerthread->id;
                        $mensaje->id_employee = 1;
                        $mensaje->private = 1;
                        $texto_message = $Reserva_SN == "N"  ? "Ean generado: " . $configData["WS_ST_Ventas_CA"]["EANTicket"] . "\n\r" : "Ean generado: " . $configData["WS_ST_Ventas_CA"]["EANReserva"] . "\n\r";
                        $mensaje->message = $texto_message;
                        $mensaje->save();
                        //SEGUNDO MENSAJE CON LA BUSQUEDA DE STOCKS
                        if (!empty($stocks_encontrados)) {
                            $customerthread_stock = new CustomerThreadCore();
                            $customerthread_stock->id_customer = $order->id_customer;
                            $customerthread_stock->id_order = $order->id;
                            $customerthread_stock->id_shop = 1;
                            $customerthread_stock->id_contact = 0;
                            $customerthread_stock->token = Tools::passwdGen(12);
                            $customerthread_stock->id_product = 0;
                            $customerthread_stock->id_lang = LanguageCore::getIdByIso('es');
                            $customerthread_stock->email = (new Customer($order->id_customer))->email;
                            $customerthread_stock->save();
                            $mensaje_stock = new CustomerMessageCore();
                            $mensaje_stock->id_customer_thread = $customerthread_stock->id;
                            $mensaje_stock->id_employee = 1;
                            $mensaje_stock->private = 1;
                            $texto_message_stock = "";
                            foreach ($stocks_encontrados as $msg_stock_encontrado) {
                                $texto_message_stock .= $msg_stock_encontrado . "\n\r";
                            }
                            $mensaje_stock->message = $texto_message_stock;
                            $mensaje_stock->save();
                        }
                    }
                    Db::getInstance()->execute("insert into " . _DB_PREFIX_ . "status2_pedidos(id_presta, correcto, informado) values(" . $order->id . ",1,1)");
                    if (Configuration::get("STATUS2_CONTROL_ESTADOS", null, $id_shop_group, $id_shop) == 1) {
                        $order->setCurrentState(Configuration::get("STATUS2_ID_ESTADO", null, $id_shop_group, $id_shop));
                    }
                    //Grabamos en Log de Prestashop aquellos no se encontraron
                    if (!empty($stocks_no_encontrados)){
                        foreach ($stocks_no_encontrados as $msg_error_stock_no_encontrado){
                            PrestaShopLogger::addLog($msg_error_stock_no_encontrado, 2);
                        }
                    }
                }
            } else {
                echo ("No se pudo crear el cliente fidelizado:");
                echo '<br>';
                echo ($return->WS_APP_Cliente_Fidelizacion_ActualizarResult);
            }
        } elseif (!empty(Tools::getValue("date_from") && !empty(Tools::getValue("date_to")))) {

            $id_shop = empty(Tools::getValue("id_shop")) ?  (int)Context::getContext()->shop->id : Tools::getValue("id_shop");
            $id_shop_group = empty(Tools::getValue("id_shop_group")) ?  (int)Shop::getContextShopGroupID() : Tools::getValue("id_shop_group");
            //$id_shop = (int)Context::getContext()->shop->id;
            //$id_shop_group = (int)Shop::getContextShopGroupID();
            //https://www.tiendapoete.es/modules/status2/crons/reimport_pedido.php?date_from=2019-12-26&date_to=2019-12-26
            //echo(date("Y-m-d"));//ex: 2019-12-27
            $orders_id = OrderCore::getOrdersIdByDate(Tools::getValue("date_from"), Tools::getValue("date_to"));

            foreach ($orders_id as $order_id) {

                $order = new Order($order_id);
                if ((int)$order->current_state == 8 || (int)$order->current_state == 6 || (int)$order->current_state == 7) {
                    echo('El estado del pedido ' . $order->id . ' no es acorde para ser subido a status2');
                    continue;
                }
                $result = Db::getInstance()->getValue("select max(correcto) from " . _DB_PREFIX_ . "status2_pedidos where id_presta = '" . $order->id . "'");
                if ($result == 1) {
                    echo("El pedido " . $order->id . " ya ha sido subido a Status2");
                    continue;
                }
                $address_dev = new Address($order->id_address_delivery);
                //EN LOLACASADEMUNT NOS PASARAN AQUI EL NUMERO DE TIENDA DONDE SE RECOGE EL PEDIDO
                //var_dump($address_dev->other);
                //die;
                $address_bill = new Address($order->id_address_invoice);
                $customer = new Customer($order->id_customer);
                $cliente = substr(Configuration::get("ECOMMANDSYNC_STATUS2_KEYCLIENTE", null, $id_shop_group, $id_shop), 0, 5);
                $id_status = null;
                if ($cliente == "WB000") {
                    $id_status = Db::getInstance()->getValue("select id_codigocliente_wehbe from " . _DB_PREFIX_ . "customer where id_customer = " . $customer->id);
                }
                if (!$id_status) {
                    $id_status = Db::getInstance()->getValue("select id_status2 from " . _DB_PREFIX_ . "status2_customers where id_presta = " . $customer->id);
                }
                try {
                    $soapClient = new SoapClient(Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN"), array('trace' => true,
                        'connection_timeout' => 5000,
                        'cache_wsdl' => WSDL_CACHE_NONE,
                        'keep_alive' => false,));
                } catch (SoapFault $soapClient) {
                    PrestaShopLogger::addLog("Status2: IMPOSIBLE CONECTAR CON EL SERVIDOR " . time(), 4);
                    goto fin;
                    //die("Imposible conectar");
                }
                $parameters = new stdClass();
                if ($id_status) {
                    $parameters->P_sCodigoCliente = $id_status;
                }
                $parameters->P_sNombre = $customer->firstname;
                $parameters->P_sApellidos = $customer->lastname;
                $parameters->P_sSexo = $customer->id_gender;
                $parameters->P_seMail = $customer->email;
                $parameters->P_sFechaNacimiento = empty($customer->birthday) ? "00000000" : str_replace("-", "", $customer->birthday);
                //direcciones
                $parameters->P_sDireccion = $address_bill->address1;
                $parameters->P_sCodigoPostal = $address_bill->postcode;
                $parameters->P_sLocalidad = $address_bill->city;
                $parameters->P_sProvincia = !empty($address_bill->id_state) ? State::getNameById($address_bill->id_state) : "";
                $parameters->P_sPais = CountryCore::getIsoById(CountryCore::getIdByName(false, $address_dev->country));
                $parameters->P_sTelefono = empty($address_bill->phone) ? "0" : $address_bill->phone;
                $parameters->P_sMovil = empty($address_bill->phone_mobile) ? "0" : $address_bill->phone_mobile;
                $parameters->P_sDNI = empty($address_bill->dni) ? "0" : $address_bill->dni;
                $parameters->P_sDireccion_Envio = $address_dev->address1;
                $parameters->P_sCodigoPostal_Envio = $address_dev->postcode;
                $parameters->P_sLocalidad_Envio = $address_dev->city;
                $parameters->P_sProvincia_Envio = !empty($address_dev->id_state) ? State::getNameById($address_dev->id_state) : "";
                $parameters->P_sPais_Envio = CountryCore::getIsoById(CountryCore::getIdByName(false, $address_dev->country));
                $parameters->P_sTelefono_Envio = $address_dev->phone;
                $parameters->P_sMovil_Envio = $address_dev->phone_mobile;
                $return = $soapClient->WS_APP_Cliente_Fidelizacion_Actualizar($parameters);
                $resultado = substr($return->WS_APP_Cliente_Fidelizacion_ActualizarResult, 6, 4);
                //echo $resultado;
                if (is_numeric($resultado)) {
                    if (!$id_status) {
                        $id_status = $return->WS_APP_Cliente_Fidelizacion_ActualizarResult;
                        Db::getInstance()->execute("insert into " . _DB_PREFIX_ . "status2_customers(id_status2, id_presta) values(" . $id_status . "," . $customer->id . ")");
                    }
                    $lineas = 0;
                    $lineas_vent = array("clsSt_Venta_Lineas" => array());
                    $cart_rules = $order->getCartRules();
                    $total_discount_import = 0;
                    $total_paid = 0;
                    //modG: total ahora es un array de porcentajes
                    $total_carrito_desc = [];
                    $discount_types = [];
                    //modG: Añadimos las cartrules al array de descuentos
                    foreach ($cart_rules as $cart_rule) {
                        //$total_carrito_desc += $cart_rule["value"];
                        $cartRule_obj = new CartRule($cart_rule["id_cart_rule"]);
                        //$total_discount_import += $cart_rule["value"];
                        $groups = $cartRule_obj->getProductRuleGroups();
                        if (is_array($groups) && !empty($groups)) {
                            foreach ($groups as $group) {
                                if (isset($group['product_rules']) && !empty($group['product_rules'])) {
                                    foreach ($group['product_rules'] as $item) {
                                        $discount_types[] = $item;
                                    }
                                }
                            }
                        }
                        if ($cartRule_obj->reduction_percent != 0) {
                            $total_carrito_desc[] = $cartRule_obj->reduction_percent;
                        } else if ($cartRule_obj->reduction_amount != 0) {
                            $percent = (($cartRule_obj->reduction_amount * 100) / ($order->total_products_wt / $order->conversion_rate));
                            $total_carrito_desc [] = $percent;
                        }
                    }
                    $total_discount_import = $order->total_discounts / $order->conversion_rate;
                    $total_prods_eval = 0;
                    $stocks_no_encontrados = array();
                    $stocks_encontrados = array();
                    //Cogemos la tiendas para la busqueda de stock
                    $tiendas = Db::getInstance()->executeS("select id_tpv_status2 from " . _DB_PREFIX_ . "status2_tpvs where selected = 1 and id_shop = '" . $id_shop . "' and id_group = '" . $id_shop_group . "'");
                    $cliente_completo = Configuration::get("ECOMMANDSYNC_STATUS2_KEYCLIENTE", null, $id_shop_group, $id_shop);
                    $Tipo_Reserva = "";
                    $Reserva_SN = "N";
                    if (($cliente == "I0B2B") || ($cliente == "I0B2C")) {
                        $Tipo_Reserva = "E";
                        $Reserva_SN = "S";
                    }
                    $PAGO_EN_TIENDA = "N";
                    if (($cliente_completo == "M020089") && (trim($order->payment) == "Pago en tienda")) {
                        $Tipo_Reserva = "R";
                        $Reserva_SN = "S";
                        $PAGO_EN_TIENDA = "S";
                    }
                    $tienda_recogida = "0000000";
                    $Tipo_Recogida = "";
                    if ($cliente_completo == "M020089") {
                        $Tipo_Recogida = (new CarrierCore($order->id_carrier, Context::getContext()->language->id))->name;
                        if (trim($Tipo_Recogida == "Recogida en tienda")) {
                            $tienda_recogida = Db::getInstance()->getValue("select note from " . _DB_PREFIX_ . "store where Trim(name) = '" . Trim($address_dev->company). "'");
                        }
                    }
                    $tpvs = array();
                    if ($PAGO_EN_TIENDA == "S") {
                        $tpvs[] = $tienda_recogida;
                    }
                    foreach ($tiendas as $tienda) {
                        $tpvs[] = $tienda["id_tpv_status2"];
                    }
                    $mensaje_regalo=$order->gift.'@'.$order->gift_message;
                    foreach ($order->getProducts() as $product) {
                        //cogemos el EAN del producto
                        $ean13 = $product["product_ean13"];
                        $tpvs_stock_stockusado = array();

                        if (Configuration::get('STATUS2_PETICION_TRASPASO') == 1) {
                            $parameters_stocks = new stdClass();
                            $parameters_stocks->P_sEnt = array(
                                "IdTPV" => implode(",", $tpvs),
                                "EAN" => $ean13,
                                "DesglosadoXTPV" => 1
                            );
                            $return = $soapClient->WS_ST_Stocks($parameters_stocks);
                            $ob = simplexml_load_string($return->WS_ST_StocksResult->any);
                            $json = json_encode($ob);
                            $configDatastock = json_decode($json, true);

                            //cargamos un array con los tpvs encontrados, el stock actual, y 0 en stock usado
                            if (isset($configDatastock["WS_ST_Stocks_DT"][0])) {
                                foreach ($configDatastock["WS_ST_Stocks_DT"] as $stock) {
                                    if ($stock["Stock"] > 0) {
                                        if (!isset($tpvs_stock_stockusado[$stock["Id_Tpv"]])) {
                                            $tpvs_stock_stockusado [] = array("Id_Tpv" => $stock["Id_Tpv"], "Stock" => (int)$stock["Stock"], "Stockusado" => 0);
                                        }
                                    }
                                }
                            } else { // solo hay un registro de stock
                                if (isset($configDatastock["WS_ST_Stocks_DT"]["Stock"])) {
                                    if ($configDatastock["WS_ST_Stocks_DT"]["Stock"] > 0) {
                                        $tpvs_stock_stockusado [] = array("Id_Tpv" => $configDatastock["WS_ST_Stocks_DT"]["Id_Tpv"], "Stock" => (int)$configDatastock["WS_ST_Stocks_DT"]["Stock"], "Stockusado" => 0);
                                    }
                                }
                            }
                        }

                        for ($i = 1; $i <= (int)$product["product_quantity"]; $i++) {
                            $lineas++;
                            $original_price = ProductCore::getPriceStatic($product["product_id"], true, $product["product_attribute_id"], 6, null, false, false);
                            $original_price_sin = ($product["original_product_price"] / $order->conversion_rate) * $this->devuelve_impuestos();
                            $original_price = $original_price_sin * 1;//$product["product_quantity"];
                            $id_tpvpeticion = Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE', null, $id_shop_group, $id_shop);
                            $id_tpvstock = "";
                            if ($id_tpvpeticion == "M014429" ) {
                                $id_tpvpeticion = 'M014401';
                            }
                            if (Configuration::get('STATUS2_PETICION_TRASPASO') == 1) {
                                //modG: si $configDatastock["WS_ST_Stocks_DT"] no era multi-nivel no recogia bien el idtpv
                                $encontrado = false;
                                if ($PAGO_EN_TIENDA == "S") {
                                    $tpvs_seleccion_stocks = explode(';', $tienda_recogida . ";" . Configuration::get('STATUS2_TPVS_STOCK_PREFERIDOS', null, $id_shop_group, $id_shop));
                                } else {
                                    $tpvs_seleccion_stocks = explode(';', Configuration::get('STATUS2_TPVS_STOCK_PREFERIDOS', null, $id_shop_group, $id_shop));
                                }
                                if (!empty($tpvs_seleccion_stocks)) {
                                    foreach ($tpvs_seleccion_stocks as $tpv_stock_peticion) {
                                        foreach ($tpvs_stock_stockusado as $Tpv => $stock) {
                                            if ($stock["Id_Tpv"] == $tpv_stock_peticion) {
                                                if ($stock["Stock"] > 0 && ($stock["Stockusado"] < $stock["Stock"])) {
                                                    $id_tpvpeticion = $stock["Id_Tpv"];
                                                    $encontrado = true;
                                                    $tpvs_stock_stockusado[$Tpv]["Stockusado"]++;
                                                    break;
                                                }
                                            }
                                        }
                                        if ($encontrado) break;
                                    }
                                    if (!$encontrado) { // no se ha encontrado stock en los tpvs preferidos, seguimos buscando en todos los tpvs
                                        //LOLA CASADEMNUNT PRIMERO BUSCARA POR EL ALMACEN POR QUE ASI ESTARA CONFIGURADO EN LOS TPVS PETICION
                                        //SI NO ENCUENTRA STOCK LLEGARA A ESTA RUTINA DONDE SE ORDENARA EL RESTO DE TPVS POR CANTIAD DE STOCK DESCENDIENTE
                                        if ($cliente == "M0144") {
                                            $sorted = $this->array_orderby($tpvs_stock_stockusado, 'Stock', SORT_DESC); // ordena los stocks de mayor a menor por unidades
                                            $tpvs_stock_stockusado = $sorted;
                                        }
                                        foreach ($tpvs_stock_stockusado as $Tpv => $stock) {
                                            if ($stock["Stock"] > 0 && ($stock["Stockusado"] < $stock["Stock"])) {
                                                $id_tpvpeticion = $stock["Id_Tpv"];
                                                $encontrado = true;
                                                $tpvs_stock_stockusado[$Tpv]["Stockusado"]++;
                                                break;
                                            }
                                        }
                                    }
                                } else { // no hay tpvs preferidos para el stock
                                    //LOLA CASADEMNUNT
                                    //SI NO HAY TPV'S PREFERIDOS TAMBIEN SE ORDENARA EL RESTO DE TPVS POR CANTIDAD DE STOCK DESCENDIENTE
                                    if ($cliente == "M0144") {
                                        $sorted = $this->array_orderby($tpvs_stock_stockusado, 'Stock', SORT_DESC); // ordena los stocks de mayor a menor por unidades
                                        $tpvs_stock_stockusado = $sorted;
                                    }
                                    foreach ($tpvs_stock_stockusado as $Tpv => $stock) {
                                        if ($stock["Stock"] > 0 && ($stock["Stockusado"] < $stock["Stock"])) {
                                            $id_tpvpeticion = $stock["Id_Tpv"];
                                            $encontrado = true;
                                            $tpvs_stock_stockusado[$Tpv]["Stockusado"]++;
                                            break;
                                        }
                                    }
                                }
                                if ($PAGO_EN_TIENDA == "S") {
                                    $id_tpvstock = $id_tpvpeticion;
                                    $id_tpvpeticion = $tienda_recogida;
                                }
                                if (!$encontrado) {
                                    $mensaje = "Status2: No se ha encontrado Stock del  Artículo " . $product["product_name"] . " Ref: " . $product["product_reference"] . " para el pedido con referencia: " . $order->reference;
                                    $stocks_no_encontrados[] = $mensaje;
                                } else {
                                    if ($PAGO_EN_TIENDA == "S") {
                                        $mensaje_encontrado = "Reserva a " . $id_tpvpeticion . " Stock de " . $id_tpvstock . " -> Artículo :" . $product["product_reference"] . "\n\r" . $product["product_name"];
                                    } else {
                                        $mensaje_encontrado = "Petición a " . $id_tpvpeticion . " -> Artículo :" . $product["product_reference"] . "\n\r" . $product["product_name"];
                                    }
                                    $stocks_encontrados[] = $mensaje_encontrado;
                                }
                                if (($id_tpvpeticion == "I0B2B00") || ($id_tpvpeticion == "I0B2C00")) {
                                    $id_tpvpeticion = 'I999900';
                                }
                            }
                            if (count($total_carrito_desc) > 0) {
                                $total_from_prod = ProductCore::getPriceStatic($product["product_id"], true, $product["product_attribute_id"], 6);
                                $total_from_prod = (float)$total_from_prod * 1;//$product["product_quantity"];
                                $total_from_prod = (($product["original_product_price"] / $order->conversion_rate) * $this->devuelve_impuestos()) * 1;// $product["product_quantity"];
                                $DescuentoPromocion = 0;
                                $precio_sin_descontar_nada = $order->total_products_wt / $order->conversion_rate;
                                $descuento_total = $order->total_discounts_tax_incl / $order->conversion_rate;
                                $porcentage_resta = (float)($descuento_total / $precio_sin_descontar_nada) * 100;
                                //$DescuentoPromocion += $porcentage_resta;
                                if ($product["reduction_percent"] != 0) {
                                    //$DescuentoPromocion = (float) $product["reduction_percent"] + $porcentage_resta;
                                    $DescuentoPromocion += $product["reduction_percent"];
                                } else if ($product["reduction_amount_tax_incl"] != 0) {
                                    $porcentaje_amount = (float)(($product["reduction_amount_tax_incl"] / $order->conversion_rate) / (($product["original_product_price"] / $order->conversion_rate) * $this->devuelve_impuestos())) * 100;
                                    $DescuentoPromocion += $porcentaje_amount;
                                }
                                $product["total_price_tax_incl_withcart"] = (float)$total_from_prod - ((float)($total_from_prod * sprintf('%0.2f', $DescuentoPromocion)) / 100);
                                $product["total_price_tax_incl_withcart"] = (float)$product["total_price_tax_incl_withcart"] - ((float)($product["total_price_tax_incl_withcart"] * $porcentage_resta) / 100);
                                $DescuentoPromocion = (float)($product["total_price_tax_incl_withcart"] / $original_price) * 100;
                                $DescuentoPromocion = 100 - $DescuentoPromocion;
                            } else {
                                $product["total_price_tax_incl_withcart"] = $product["total_price_tax_incl"] / $order->conversion_rate;
                                $DescuentoPromocion = (float)($product["total_price_tax_incl_withcart"] / $original_price) * 100;
                                $DescuentoPromocion = 100 - $DescuentoPromocion;
                            }


                            $product["total_price_tax_incl_withcart"] = $product["total_price_tax_incl_withcart"];
                            $total_prods_eval += (float)$product["total_price_tax_incl_withcart"];
                            //dump($order->total_paid);
                            $DescuentoPromocion = $DescuentoPromocion;
                            $descuento_solo_calculo = (1 - (Tools::ps_round(($order->total_paid / $order->conversion_rate) - ($order->total_shipping_tax_incl / $order->conversion_rate), 2) / Tools::ps_round($order->total_products_wt / $order->conversion_rate, 2))) * 100;
                            //dump($product);
                            if ($descuento_solo_calculo >= 0) {
                                if ((($order->total_paid / $order->conversion_rate) - ($order->total_shipping_tax_incl / $order->conversion_rate)) != ($order->total_products_wt / $order->conversion_rate)) {
                                    $paid_no_trans = ($order->total_paid / $order->conversion_rate) - ($order->total_shipping_tax_incl / $order->conversion_rate);
                                    $precio_final = Tools::ps_round($product['unit_price_tax_incl'] / $order->conversion_rate, 2) - (($descuento_solo_calculo * Tools::ps_round($product['unit_price_tax_incl'] / $order->conversion_rate, 2)) / 100);
                                    //$precio_final = $precio_final * 1;//$product["product_quantity"];
                                } else {
                                    $precio_final = Tools::ps_round(($product['unit_price_tax_incl'] / $order->conversion_rate) * 1, 2);//$product["product_quantity"], 2);
                                }

                            } else {
                                $precio_final = Tools::ps_round($original_price, 2) - (($descuento_solo_calculo * Tools::ps_round($original_price, 2)) / 100);
                                //$precio_final = $precio_final * 1;//$product["product_quantity"];
                            }
                            $total_paid += Tools::ps_round($precio_final, 2);
                            $descuento_final = $this->Devuelve_porcentaje_en_positivo((((Tools::ps_round($original_price, 2) - $precio_final) / Tools::ps_round($original_price, 2)) * 100 <= 0) ? 0 : ((Tools::ps_round($original_price, 2) - $precio_final) / Tools::ps_round($original_price, 2)) * 100);
                            $lineas_vent["clsSt_Venta_Lineas"][] = array(
                                "NumLin_Vta" => $lineas,
                                "Reserva_SN" => $Reserva_SN,
                                "TipoReserva" => $Tipo_Reserva,
                                "EAN" => $ean13,
                                //"Articulo" => $reference,
                                //"IdTalla" => $combis_talla,
                                "Cantidad" => 1,
                                "PorIVA" => Configuration::get("STATUS2_IMPUESTOS_VENTA", null, $id_shop_group, $id_shop),
                                "Precio" => Tools::ps_round($original_price_sin, 2),
                                "SubtotalLinea" => Tools::ps_round($precio_final, 2),
                                "DescuentoPromocion" => $descuento_final,
                                "Referencia" => $mensaje_regalo,
                                "IdTPVPeticion" => $id_tpvpeticion,
                                "Id_ECommerce" => $order->reference,
                                "IdTPVStock" => $id_tpvstock
                            );
                        }
                    }
                    //modG: si hay transporte enviamos una línea extra para el transporte
                    if ($order->total_shipping_tax_incl > 0) {
                        $total_paid += $order->total_shipping_tax_incl / $order->conversion_rate;
                        $lineas++;
                        $lineas_vent["clsSt_Venta_Lineas"][] = array(
                            "NumLin_Vta" => $lineas,
                            "Reserva_SN" => $Reserva_SN,
                            "TipoReserva" => $Tipo_Reserva,
                            "EAN" => Configuration::get('ECOMMANDSYNC_STATUS2_EANTRANS', null, $id_shop_group, $id_shop),
                            "Articulo" => 'TRANSPORTE',
                            "IdTalla" => "0",
                            "Cantidad" => 1,
                            "PorIVA" => Configuration::get("STATUS2_IMPUESTOS_VENTA", null, $id_shop_group, $id_shop),
                            "Precio" => Tools::ps_round($order->total_shipping_tax_incl / $order->conversion_rate, 2),//$original_price,
                            "SubtotalLinea" => Tools::ps_round($order->total_shipping_tax_incl / $order->conversion_rate, 2),
                            "DescuentoPromocion" => 0,
                            //"Referencia" => 'TRANSPORTE',
                            "IdTPVPeticion" => $id_tpvpeticion,
                            "Id_ECommerce" => $order->reference,
                            "IdTPVStock" => $id_tpvstock
                        );
                        $total_prods_eval += 5;
                    }
                    // 1 centimo
                    if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.01) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.01;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.01) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.01;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    }
                    // 2 centimos
                    if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.02) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.02;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.02) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.02;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    }
                    // 3 centimos
                    if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.03) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.03;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.03) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.03;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    }
                    //4 centimos
                    if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.04) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.04;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.04) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.04;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    }
                    // 5 centimos
                    if (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == 0.05) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] += 0.05;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    } elseif (sprintf('%0.2f', ($order->total_paid / $order->conversion_rate) - $total_paid) == -0.05) {
                        $total_paid = $total_paid;
                        $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"] -= 0.05;
                        $lineas_vent["clsSt_Venta_Lineas"][0]['DescuentoPromocion'] = $this->Devuelve_porcentaje_en_positivo(((Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2) - $lineas_vent["clsSt_Venta_Lineas"][0]["SubtotalLinea"]) / Tools::ps_round($lineas_vent["clsSt_Venta_Lineas"][0]["Precio"], 2)) * 100);
                    }
                    $total_paid = Tools::ps_round(($order->total_paid / $order->conversion_rate), 2);
                    $param_venta = new stdClass();
                    $date = explode(" ", $order->date_add);
                    $Numero_De_Cliente = "";
                    if ($cliente == "WB000") { //Wehbe ID_CLIENTE
                        $metodos_de_pago = OrderPayment::getByOrderId($order->id);
                        foreach ($metodos_de_pago as $metodo_de_pago) {
                            /*
                            echo("ID -> " . $order->id);
                            echo '<br>';
                            echo("Referencia ->" . $metodo_de_pago->order_reference);
                            echo '<br>';
                            echo("Nombre  -> " . $metodo_de_pago->payment_method);
                            echo '<br>';
                            echo("Numero de Cliente -> " . $metodo_de_pago->card_number);
                            echo '<br>';
                            */
                            $Numero_De_Cliente = $metodo_de_pago->card_number;
                            //PrestaShopLogger::addLog("CARD NUMBER : " . $Numero_De_Cliente. " FIDELIZACION NUMBER : " . $id_status. " " . (string)date("j, Y, g:i a")  , 2);
                        }
                    }
                    $envio_direccion = $address_dev->address1;
                    $envio_cp = $address_dev->postcode;
                    $envio_localidad = $address_dev->city;
                    if ($cliente == "M0153") { // SAN JURJO si el transportista es sanjurjo es recogida en tienda y se pone en envio direccion el nombre de la tienda donde recoger
                        $Nombre_transporte = (new CarrierCore($order->id_carrier, Context::getContext()->language->id))->name;
                        if (substr($Nombre_transporte, 0, 8) == "Sanjurjo") {
                            $envio_direccion = $Nombre_transporte;
                            $envio_cp = "";
                            $envio_localidad = "";
                        }
                    }
                    $nombre = $customer->firstname;
                    $apellidos = $customer->lastname;
                    if ($cliente == "M0144") { //LOLA CASADEMUNT -> DATOS DE FACTURACION en Lola se envian los datos de nombre y apellidos de facturacion
                        $nombre = $address_bill->firstname;
                        $apellidos = $address_bill->lastname;
                    }
                    $nombre_envio = $address_dev->firstname;
                    if ($cliente == "M0200" && trim($Tipo_Recogida == "Recogida en tienda")) { // -> CONDOR SI ES RECOGIDA EN TIENDA SE RECUPERA EL NOMBRE DE LA TIENDA EN NOMBRE DE ENVIO
                        $nombre_envio = Trim($address_dev->company);
                    }
                    $param_venta->P_sText = array(
                        "IdTPV" => Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE', null, $id_shop_group, $id_shop),
                        "NumCaja" => 1,
                        "Almacen" => 1,
                        "FechaOperacion" => $date[0],
                        "HoraOperacion" => $date[1],
                        "Cliente" => $Numero_De_Cliente,
                        "ClienteFid" => $id_status,
                        "Nombre" => $nombre,
                        "Apellidos" => $apellidos,
                        "EMail" => $customer->email,
                        "Direccion" => $address_bill->address1,
                        "CodigoPostal" => $address_bill->postcode,
                        "Localidad" => $address_bill->city,
                        "Provincia" => !empty($address_bill->id_state) ? State::getNameById($address_bill->id_state) : "",
                        "Pais" => CountryCore::getIsoById(CountryCore::getIdByName(false, $address_bill->country)),
                        "Telefono" => empty($address_bill->phone) ? "0" : $address_bill->phone,
                        "Movil" => empty($address_bill->phone_mobile) ? "0" : $address_bill->phone_mobile,
                        "DNI" => empty($address_bill->dni) ? "0" : $address_bill->dni,
                        "EnvioNombre" => $nombre_envio,
                        "EnvioApellidos" => $address_dev->lastname,
                        "EnvioDireccion" => $envio_direccion,
                        "EnvioCodigoPostal" => $envio_cp,
                        "EnvioLocalidad" => $envio_localidad,
                        "EnvioProvincia" => !empty($address_dev->id_state) ? State::getNameById($address_dev->id_state) : "",
                        "EnvioPais" => CountryCore::getIsoById(CountryCore::getIdByName(false, $address_dev->country)),
                        "EnvioTelefono" => empty($address_dev->phone) ? "0" : $address_dev->phone,
                        "EnvioMovil" => empty($address_dev->phone_mobile) ? "0" : $address_dev->phone_mobile,
                        "Transporte" => $order->id_carrier.'@'.(new CarrierCore($order->id_carrier, Context::getContext()->language->id))->name,
                        "VentaLineas" => $lineas_vent,
                        "VentaFP" => array("clsSt_Venta_FP" => array(
                            "NumLin_FP" => 1,
                            "DescripcionFormaPago" => $PAGO_EN_TIENDA == "S" ? "PDF" : $order->payment,
                            "SubtotalFormaPago" => $total_paid
                        ))
                    );

                    $return = $soapClient->WS_ST_Ventas($param_venta);
                    $ob = simplexml_load_string($return->WS_ST_VentasResult->any);
                    $json = json_encode($ob);
                    $configData = json_decode($json, true);
                    if ($configData["WS_ST_Ventas_DT"]["ErrorLinea"] == "1") {
                        //envio email y log en caso de error
                        //dump("Status2: El pedido con referencia: " . $order->reference . " no se ha podido crear. Motivo: " . $configData["WS_ST_Ventas_DT"]["ErrorLineaDescripcion"]);
                        PrestaShopLogger::addLog("Status2: El pedido con referencia: " . $order->reference . " no se ha podido crear. Motivo: " . $configData["WS_ST_Ventas_DT"]["ErrorLineaDescripcion"], 2);
                        $msg = "El pedido con referencia: " . $order->reference . " no se ha podido crear. Motivo: " . $configData["WS_ST_Ventas_DT"]["ErrorLineaDescripcion"];
                        Db::getInstance()->execute("insert into " . _DB_PREFIX_ . "status2_pedidos(id_presta, correcto, informado) values(" . $order->id . ",0,0)");
                        mail(Configuration::get("PS_SHOP_EMAIL"), "Fallo Importacion Pedido", $msg);
                    } else {
                        //dump("Pedido importado");
                        if (!is_array($configData["WS_ST_Ventas_CA"]["EANTicket"]) || !is_array($configData["WS_ST_Ventas_CA"]["EANReserva"])) {
                            $customerthread = new CustomerThreadCore();
                            $customerthread->id_customer = $order->id_customer;
                            $customerthread->id_order = $order->id;
                            $customerthread->id_shop = 1;
                            $customerthread->id_contact = 0;
                            $customerthread->token = Tools::passwdGen(12);
                            $customerthread->id_product = 0;
                            $customerthread->id_lang = LanguageCore::getIdByIso('es');
                            $customerthread->email = (new Customer($order->id_customer))->email;
                            $customerthread->save();
                            $mensaje = new CustomerMessageCore();
                            $mensaje->id_customer_thread = $customerthread->id;
                            $mensaje->id_employee = 1;
                            $mensaje->private = 1;
                            $texto_message = $Reserva_SN == "N" ? "Ean generado: " . $configData["WS_ST_Ventas_CA"]["EANTicket"] . "\n\r" : "Ean generado: " . $configData["WS_ST_Ventas_CA"]["EANReserva"] . "\n\r";
                            $mensaje->message = $texto_message;
                            $mensaje->save();
                            //SEGUNDO MENSAJE CON LA BUSQUEDA DE STOCKS
                            if (!empty($stocks_encontrados)) {
                                $customerthread_stock = new CustomerThreadCore();
                                $customerthread_stock->id_customer = $order->id_customer;
                                $customerthread_stock->id_order = $order->id;
                                $customerthread_stock->id_shop = 1;
                                $customerthread_stock->id_contact = 0;
                                $customerthread_stock->token = Tools::passwdGen(12);
                                $customerthread_stock->id_product = 0;
                                $customerthread_stock->id_lang = LanguageCore::getIdByIso('es');
                                $customerthread_stock->email = (new Customer($order->id_customer))->email;
                                $customerthread_stock->save();
                                $mensaje_stock = new CustomerMessageCore();
                                $mensaje_stock->id_customer_thread = $customerthread_stock->id;
                                $mensaje_stock->id_employee = 1;
                                $mensaje_stock->private = 1;
                                $texto_message_stock = "";
                                foreach ($stocks_encontrados as $msg_stock_encontrado) {
                                    $texto_message_stock .= $msg_stock_encontrado . "\n\r";
                                }
                                $mensaje_stock->message = $texto_message_stock;
                                $mensaje_stock->save();
                            }
                        }
                        Db::getInstance()->execute("insert into " . _DB_PREFIX_ . "status2_pedidos(id_presta, correcto, informado) values(" . $order->id . ",1,1)");
                        if (Configuration::get("STATUS2_CONTROL_ESTADOS", null, $id_shop_group, $id_shop) == 1) {
                            $order->setCurrentState(Configuration::get("STATUS2_ID_ESTADO", null, $id_shop_group, $id_shop));
                        }
                        //Grabamos en Log de Prestashop aquellos no se encontraron
                        if (!empty($stocks_no_encontrados)) {
                            foreach ($stocks_no_encontrados as $msg_error_stock_no_encontrado) {
                                PrestaShopLogger::addLog($msg_error_stock_no_encontrado, 2);
                            }
                        }
                    }
                }
            }
        }
        fin:
    }
    private function Devuelve_porcentaje_en_positivo($porcentaje)
    {
        if ($porcentaje < 0) {
            $porcentaje = $porcentaje * -1;
        }
        return $porcentaje; // = sprintf('%0.2f', $porcentaje);
    }
    private function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }
    private function devuelve_impuestos()
    {

            return (1 +  (float) (Configuration::get("STATUS2_IMPUESTOS")  / 100));

    }
    private function getCarrierName($id_order)
    {
        return Db::getInstance()->executeS('SELECT cl.name as carrier_name FROM '._DB_PREFIX_.'order_carrier oc LEFT JOIN '._DB_PREFIX_.'carrier cl	ON (oc.id_carrier = cl.id_carrier) WHERE oc.id_order = '.(int)$id_order);

    }
}
