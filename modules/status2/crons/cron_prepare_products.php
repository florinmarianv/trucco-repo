<?php
include_once dirname(__FILE__) . '/../../../config/config.inc.php';
include_once dirname(__FILE__) . '/../../../init.php';
$soapClient = new SoapClient(Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN"), array('trace' => true,
    'connection_timeout' => 5000,
    'cache_wsdl' => WSDL_CACHE_NONE,
    'keep_alive' => false,));
$parameters = new stdClass();
$first = true;
$products = array();
$products_fin = array();
$fecha = new DateTime();
$FecHasta = $fecha->format("d/m/Y H:m:s");
if(Tools::getValue("first") == 1){
    $parameters->P_sEnt = array(
        "IdTPV" => Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE'),
        "Tarifa" => 10,
        "NumRegistros" => 5000,
        //"Id_Articulo"=> "ARMONIA",
        "FecDesde"=> $FecHasta,
        //"FecHasta"=> $FecHasta,
        "TipoConsulta" => 2,
        "ECommerce" => 1,
    );
}else{
    $parameters->P_sEnt = array(
        "IdTPV" => Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE'),
        "Tarifa" => 10,
        "NumRegistros" => 5000,
        //"Id_Articulo"=> "ARMONIA",
        "FecDesde"=> $FecHasta,
        "Date_Time_Stamp"=>(new DateTime())->modify("-1 day")->format("Y-m-d H:m:s").".000",
        //"FecHasta"=> $FecHasta,
        "TipoConsulta" => 2,
        "ECommerce" => 1,
    );
}

$return = $soapClient->WS_APP_Precio_APP($parameters);
$ob = simplexml_load_string($return->WS_APP_Precio_APPResult->any);
$json = json_encode($ob);
$configData = json_decode($json, true);

if (isset($configData["WS_APP_Precio_APP_DT"]) && count($configData["WS_APP_Precio_APP_DT"]) > 1) {
    Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."preimport_status2");
    $products = $configData["WS_APP_Precio_APP_DT"];
    //$products_fin = array_merge($products_fin, $this->prepareProducts($products, false));
    foreach ($products as $product){
        if(is_array($product)){
            array_walk($product,function(&$value){
                if(is_array($value) && count($value) == 0){
                    $value = "";
                }
                $value = (string)$value;
                $value = stripslashes($value);
                $value = str_replace("'"," ",$value);
            });
            extract($product);
            Db::getInstance()->insert("preimport_status2",compact("Id_MarcaArticulo","Id_Temporada","Id_Articulo","Id_Color","Id_Talla","EAN","Id_Tipo","FecUltModif","Descripcion_1","Precio","PrecioRebajas","DescTemp","Familia","Subfamilia","Seccion","Tipo","Descripcion_Color","Descripcion_IdTalla","OrdenTemporada","Imagen","Descripcion_2","Id_Paleta","Descatalogado","Descripcion_Familia","Descripcion_SubFamilia","CodigoEstadistico","Descripcion_CodEst","Filtro_Ad_1","Descripcion_Fd1","Filtro_Ad_2","Descripcion_Fd2","Filtro_Ad_3","Descripcion_Fd3","Descripcion_Marca","Descripcion_Seccion","Descripcion_Tipo","ECommerce","Descripcion_WEB_Articulo","Descripcion_WEB_Color","ECommerce_CT"),true);
        }
    }
    $count = 0;
    while(isset($configData["WS_APP_Precio_APP_DT"]) && count($configData["WS_APP_Precio_APP_DT"]) > 1 && !isset($configData["WS_APP_Precio_APP_DT"]["FecUltModif"])){
        $count_total = Db::getInstance()->getValue("select count(EAN) from "._DB_PREFIX_."preimport_status2");
        $count++;
        if(isset($configData["WS_APP_Precio_APP_DT"]) && count($configData["WS_APP_Precio_APP_DT"]) > 1){
            $last_element =end($products);
            $parameters->P_sEnt = array(
                "IdTPV" => Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE'),
                "Tarifa" => 10,
                "NumRegistros" => 5000,
                "Date_Time_Stamp"=>$last_element["FecUltModif"],
                "FecDesde"=> $FecHasta,
                "TipoConsulta" => 2,
                "ECommerce" => 1,
            );
            $products = array();
            $return = $soapClient->WS_APP_Precio_APP($parameters);
            $ob = simplexml_load_string($return->WS_APP_Precio_APPResult->any);
            $json = json_encode($ob);
            $configData = json_decode($json, true);
            if(isset($configData["WS_APP_Precio_APP_DT"])){
                unset($configData["WS_APP_Precio_APP_DT"][0]);
                $products = $configData["WS_APP_Precio_APP_DT"];
                //$products_fin = array_merge($products_fin,$this->prepareProducts($products,$products_fin));
                foreach ($products as $product){
                    if(is_array($product)){
                        array_walk($product,function(&$value){
                            if(is_array($value) && count($value) == 0){
                                $value = "";
                            }
                            $value = (string)$value;
                            $value = stripslashes($value);
                            $value = str_replace("'"," ",$value);
                        });
                        extract($product);
                        if(!empty(Db::getInstance()->getValue("select id from "._DB_PREFIX_."preimport_status2 where EAN = '".$product["EAN"]."'"))){
                            $message = "EAN Repeteido: ".$product["EAN"];
                            echo($message);
                            echo "<br>";
                            continue;
                        }
                        Db::getInstance()->insert("preimport_status2",compact("Id_MarcaArticulo","Id_Temporada","Id_Articulo","Id_Color","Id_Talla","EAN","Id_Tipo","FecUltModif","Descripcion_1","Precio","PrecioRebajas","DescTemp","Familia","Subfamilia","Seccion","Tipo","Descripcion_Color","Descripcion_IdTalla","OrdenTemporada","Imagen","Descripcion_2","Id_Paleta","Descatalogado","Descripcion_Familia","Descripcion_SubFamilia","CodigoEstadistico","Descripcion_CodEst","Filtro_Ad_1","Descripcion_Fd1","Filtro_Ad_2","Descripcion_Fd2","Filtro_Ad_3","Descripcion_Fd3","Descripcion_Marca","Descripcion_Seccion","Descripcion_Tipo","ECommerce","Descripcion_WEB_Articulo","Descripcion_WEB_Color","ECommerce_CT"),true);
                    }
                }
            }
            $count_total2 = Db::getInstance()->getValue("select count(EAN) from "._DB_PREFIX_."preimport_status2");
            if($count_total == $count_total2){
                die("fin recogida de productos");
            }
        }
    }
} else {
    die("no se han encontrado productos");
}
die("fin preparacion");