<?php
/**
 * Created by PhpStorm.
 * User: Sergi Martinez
 * Date: 01/10/2020
 * Time: 14:38
 */
include_once dirname(__FILE__) . '/../../../config/config.inc.php';
include_once dirname(__FILE__) . '/../../../init.php';


(new importNewsLetter())->run();


class importNewsLetter
{

    public function run()
    {
        PrestaShopLogger::addLog('INICIO Prepare NewsLetter ' . (string)date("j, Y, g:i a"), 1);
        $fecha_ultimo_cliente= Configuration::get("STATUS2_CLIENTES_NEWSLETTER");
        $clientes_nuevos = Db::getInstance()->executeS("SELECT * FROM " . _DB_PREFIX_ . "emailsubscription where newsletter_date_add > '". $fecha_ultimo_cliente . "' LIMIT 100");
        //var_dump($clientes_nuevos);
        //die;
        foreach ($clientes_nuevos as $cliente_nuevo) {
            //var_dump(trim($cliente_nuevo["email"]));
            $soapClient = new SoapClient(Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN"), array('trace' => true,
                'connection_timeout' => 5000,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'keep_alive' => false,));
            $parameters_existe = new stdClass();
            $parameters_existe->P_sEmail = trim($cliente_nuevo["email"]);
            $existe = $soapClient->WS_APP_Cliente_Fidelizacion($parameters_existe);
            $ob = simplexml_load_string($existe->WS_APP_Cliente_FidelizacionResult->any);
            $json = json_encode($ob);
            $configData = json_decode($json, true);
            if (isset($configData["WS_APP_Cliente_Fidelizacion_DT"]["ErrorLinea"])) {

                    $parameters = new stdClass();
                    $parameters->P_sNombre = "NEWSLETTER";
                    $parameters->P_sApellidos = "NEWSLETTER";
                    $parameters->P_sSexo = "0";
                    $parameters->P_seMail = $cliente_nuevo["email"];
                    $parameters->P_sFechaNacimiento = "19000101";
                    $parameters->P_sDireccion = ".";
                    $parameters->P_sCodigoPostal = "00000";
                    $parameters->P_sLocalidad = ".";
                    $parameters->P_sProvincia = ".";
                    $parameters->P_sPais = "ES";
                    $parameters->P_sTelefono = "0";
                    $parameters->P_sMovil = "0";
                    $parameters->P_sDNI = "0";
                    $parameters->P_sDireccion_Envio = "";
                    $parameters->P_sCodigoPostal_Envio = "00000";
                    $parameters->P_sLocalidad_Envio = "";
                    $parameters->P_sProvincia_Envio = "";
                    $parameters->P_sPais_Envio = "ES";
                    $parameters->P_sTelefono_Envio = "0";
                    $parameters->P_sMovil_Envio = "0";
                    $return = $soapClient->WS_APP_Cliente_Fidelizacion_Actualizar($parameters);
                    $resultado = substr($return->WS_APP_Cliente_Fidelizacion_ActualizarResult, 4, 6);
                    //var_dump($resultado);
                    if (!is_numeric($resultado)) {
                        //envio fallo subida  de cliente
                        PrestaShopLogger::addLog("Status2: El Email: " . $cliente_nuevo["email"] . " no se ha podido crear.", 2);
                        echo("'<br>");
                        echo("Status2: El Email: " . $cliente_nuevo["email"] . " no se ha podido crear.");
                    } else {
                        PrestaShopLogger::addLog("Status2: Email registrado de NewsLetter : " . $cliente_nuevo["email"], 1);
                        echo("'<br>");
                        echo("Status2: El Email: " . $cliente_nuevo["email"] . " se ha creado en StModa.");
                    }

            }
            else{
                PrestaShopLogger::addLog("Status2: El Email: ". $cliente_nuevo["email"] ." ya existe en StModa.  ". (string)date("j, Y, g:i a"), 2);
                echo ("<br>");
                echo ("Status2: El Email: " . $cliente_nuevo["email"] . " ya existe en StModa.");
            }
            Configuration::updateValue('STATUS2_CLIENTES_NEWSLETTER', $cliente_nuevo["newsletter_date_add"]);
        }
    }

}
