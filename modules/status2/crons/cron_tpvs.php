<?php

include_once dirname(__FILE__) . '/../../../config/config.inc.php';
include_once dirname(__FILE__) . '/../../../init.php';
    
$contextOptions = array(
    'ssl' => array(
    'verify_peer' => false,
    'verify_peer_name' => false,
    'allow_self_signed' => true
    ));

$sslContext = stream_context_create($contextOptions);

$params =  array(
    'trace' => 1,
    'exceptions' => true,
    'cache_wsdl' => WSDL_CACHE_NONE,
    'stream_context' => $sslContext
    );
try {
    $soapClient = new SoapClient(Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN"),$params);
} catch (SoapFault $soapClient) {
    var_dump(libxml_get_last_error());
    var_dump($soapClient);
    die("Imposible conectar");
}
    //public static function get($key, $idLang = null, $idShopGroup = null, $idShop = null, $default = false)
    //public static function updateValue($key, $values, $html = false, $idShopGroup = null, $idShop = null)
    //var_dump(Context::getContext());
    //var_dump(Context::getContext()->shop);
    //$id_lang = (int) Context::getContext()->language->id;
    //$id_shop = (int)Context::getContext()->shop->id;
    //$id_shop_group = (int)Shop::getContextShopGroupID();
    //$tiendastodas = Shop::getContextListShopID();
    //var_dump($tiendastodas);
    //die;
    //foreach (Shop::getContextListShopID() as $tiendas) {
    //    echo (Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN",1,$id_shop_group,$tiendas).'<br>');
    //}
    //$shops = Shop::getShopsCollection(false);
    // foreach ($shops as $shop) {
    //     echo (Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN",null,$shop->id_shop_group, $shop->id).'<br>');
    // }
    //die;
    /*
    ; // [1,2,3]
    echo (Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN",1,$id_shop_group,$id_shop).'<br>');
    //echo Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN",1,1,3);
    echo ('id_shop : '. $id_shop.'<br>');
    echo ('id_shop group:'. $id_shop_group.'<br>');
    */
    //echo ('fin');
    //die;
//$soapClient = new SoapClient('http://wsm019799.stmoda.es:50003/St_WebTda.asmx?WSDL');


$return = $soapClient->WS_APP_TPVs();

$ob= simplexml_load_string($return->WS_APP_TPVsResult->any);
$json  = json_encode($ob);
$configData = json_decode($json, true);
$nuevos = false;
$shops = Shop::getShopsCollection(false);
foreach ($shops as $shop) {
    foreach ($configData["WS_APP_TPVs_DT"] as $tpvs) {
        $id_tpv = Db::getInstance()->getValue("select id_tpv from " . _DB_PREFIX_ . "status2_tpvs where id_tpv_status2 = '" . $tpvs["Id_TPV"] . "' and id_shop = '".$shop->id."' and id_group = '".$shop->id_shop_group."'");
        if (empty($id_tpv)) {
            Db::getInstance()->execute("insert into " . _DB_PREFIX_ . "status2_tpvs (id_tpv_status2,nombre,selected,id_shop,id_group) values('" . $tpvs["Id_TPV"] . "','" . $tpvs["Nombre"] . "',0,'".$shop->id . "','" . $shop->id_shop_group . "')");
            $nuevos = true;
        }
    }
}
if($nuevos){
    //TODO: hacer que envie un mail avisando al cliente
    //MailCore::send(Context::getContext()->language->id,"","Nuevas tiendas","");
}

die ("proceso finalizado");

