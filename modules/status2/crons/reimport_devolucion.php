<?php

/**
 * Created by PhpStorm.
 * User: Sergi Martinez
 * Date: 09/04/2021
 * Time: 23:38
 */
include_once dirname(__FILE__) . '/../../../config/config.inc.php';
include_once dirname(__FILE__) . '/../../../init.php';


(new RemarkDevo())->run();


class RemarkDevo
{
    public function run()
    {
        if (!empty(Tools::getValue("id_customer")) && !empty(Tools::getValue("id_order"))&& !empty(Tools::getValue("id_devolucion"))) {

            $id_shop = (int)Context::getContext()->shop->id;
            $id_shop_group = (int)Shop::getContextShopGroupID();
            $orderreturn = OrderReturnCore::getOrdersReturn(Tools::getValue("id_customer"),Tools::getValue("id_order"));
            if (empty($orderreturn)){
                die("No existe Order Return");
            }
            //var_dump($orderreturn);
            //$order = new Order(42);
            $order = new Order(Tools::getValue("id_order"));
            if (empty($order)){
                die("No existen el pedido");
            }
            //var_dump( $order);
            $products = OrderReturnCore::getOrdersReturnProducts (Tools::getValue("id_devolucion"),$order);
            if (empty($products)){
                die("No existen  products para el id de devolucion");
            }

            $customer = new Customer($orderreturn[0]["id_customer"]);
            $id_status = Db::getInstance()->getValue("select id_status2 from " . _DB_PREFIX_ . "status2_customers where id_presta = " . $customer->id);
            $date = explode(" ",$orderreturn[0]["date_add"]);
            $lineas = 0;
            $lineas_rma = array("clsSt_Devolucion_Lineas" => array());
            foreach ($products as $product) {
                for ($i = 1; $i <= (int)$product["product_quantity"]; $i++) {
                    $lineas++;
                    $lineas_rma["clsSt_Devolucion_Lineas"][] = array(
                        "NumLin_Dev" => $lineas,
                        "Reserva_SN" => "",
                        "TipoReserva" => "",
                        "EAN" => $product['product_ean13'],
                        "SKU" => "",
                        "Clave" => "",
                        "NSerie" => "",
                        "MarcaArticulo" => "",
                        "Articulo" => "",
                        "Color" => "",
                        "Talla" => "",
                        "IdTalla" => "",
                        "IdTemporada" => "",
                        "Temporada" => "",
                        "Cantidad" => "1",
                        "Precio" => $product['unit_price_tax_incl'], //esta mal
                        "PorIVA" => $product['tax_rate'],
                        "SubtotalLinea" => "",
                        "DescuentoPromocion" => "",
                        "CodigoDtoPromocion" => "",
                        "DescuentoPuntos" => "",
                        "DescuentoRebajas" => "",
                        "MotivoDto" => "",
                        "Referencia" => "",
                        "IdTPVPeticion" => "",
                        "IdTPVStock" => "",
                        "Id_ECommerce_Original" => $orderreturn[0]['reference'],
                        "Id_ECommerce_Devolucion" => $orderreturn[0]['id_order_return']
                    );
                }
            }

            $param_rma = new stdClass();
            $param_rma->P_sText = array(
                "IdTPV" => Configuration::get('ECOMMANDSYNC_STATUS2_KEYCLIENTE', null, $id_shop_group, $id_shop),
                "NumCaja" => 1,
                "Almacen" => 1,
                "FechaOperacion" => $date[0],
                "HoraOperacion" => $date[1],
                "Cliente" => "",
                "ClienteFid" => $id_status,
                "EnvioNombre" => $customer->firstname,
                "EnvioApellidos" =>  $customer->lastname,
                "EnvioEMail" => $customer->email,
                "MotivoDevolucion" => $orderreturn[0]["question"],
                "NumeroTracking" => $orderreturn[0]["tracking_number"],
                "DevolucionLineas" => $lineas_rma
            );
            try {
                $soapClient = new SoapClient(Configuration::get("ECOMMANDSYNC_STATUS2_TOKEN", null, $id_shop_group, $id_shop), array('trace' => true, 'connection_timeout' => 5000,
                    'cache_wsdl' => WSDL_CACHE_NONE,
                    'keep_alive' => false,));
            } catch (SoapFault $soapClient) {
                /*
                echo "<br>";
                echo "Error LibXML :";
                var_dump(libxml_get_last_error());
                echo "<br>";
                echo "Error Soap :";
                var_dump($soapClient);
                echo "<br>";
                die("Imposible conectar");
                */
            }
            try {
                $return = $soapClient->WS_ST_Devoluciones ($param_rma);
                if (!isset($return)) {
                    //die ("Error en la recepcion de los datos SOAP");
                }else{
                    var_dump($return);
                    echo '<br>';
                    echo 'Enviada -> Id_Cliente : '. Tools::getValue("id_customer") . ' Id_Orden : ' . Tools::getValue("id_order") . ' Id_Devolucion : ' . Tools::getValue("id_devolucion");
                }
            } catch (SoapFault $soapClient) {
                /*
                echo "<br>";
                echo "Error LibXML :";
                var_dump(libxml_get_last_error());
                echo "<br>";
                echo "Error Soap :";
                var_dump($soapClient);
                echo "<br>";
                die("Imposible conectar");
                */
            }
            //var_dump($return);
        } else {
            die ("FALTA ALGÚN PARAMETRO -> ?id_customer , ?id_order ,  ?id_devolucion");
        }
    }
}
