<?php

/*
 *  2007-2014 PrestaShop
 *
 *  NOTICE OF LICENSE
 *
 *  This source file is subject to the Academic Free License (AFL 3.0)
 *  that is bundled with this package in the file LICENSE.txt.
 *  It is also available through the world-wide-web at this URL:
 *  http://opensource.org/licenses/afl-3.0.php
 *  If you did not receive a copy of the license and are unable to
 *  obtain it through the world-wide-web, please send an email
 *  to license@prestashop.com so we can send you a copy immediately.
 *
 *  DISCLAIMER
 *
 *  Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 *  versions in the future. If you wish to customize PrestaShop for your
 *  needs please refer to http://www.prestashop.com for more information.
 *
 *   @author    PrestaShop SA <contact@prestashop.com>
 *   @copyright 2007-2014 PrestaShop SA
 *   @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *   International Registered Trademark & Property of PrestaShop SA
 * /
 */

include_once dirname(__FILE__) . '/../../../config/config.inc.php';
include_once dirname(__FILE__) . '/../../../init.php';
include_once dirname(__FILE__) . '/../utilities/process/ImportProductFullProcess.php';

function _debug($message) {
    echo $message.'<br />';
    flush();
}
require_once('instance.php');
$cliente = substr(Configuration::get("ECOMMANDSYNC_STATUS2_KEYCLIENTE"), 0, 5);
if ($cliente != "M0197" && $cliente != "M0181") {  // CONTROL DE PID SOLO EN LINUX
    $pid = new pid('/tmp');
    if ($pid->already_running) {
        PrestaShopLogger::addLog('INICIO SOLAPADO Product full NO SE EJECUTARA ' . (string)date("j, Y, g:i a"), 2);
        _debug("El proceso product_full, ya se está ejecutando, espere a que termine ......");
        exit;
    } else {
        PrestaShopLogger::addLog('INICIO Product full ' . (string)date("j, Y, g:i a"), 1);
        _debug("Ejecutando product_full.....");
        $process = new ImportProductFullProcess();
        $process->run();
    }
}else{
    PrestaShopLogger::addLog('INICIO Product full ' . (string)date("j, Y, g:i a"), 1);
    _debug("Ejecutando product_full.....");
    $process = new ImportProductFullProcess();
    $process->run();
}





    
