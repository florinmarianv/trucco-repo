<?php
class Attribute extends AttributeCore
{
    public static function getIdByName($id_attribute_group, $name)
    {
        $sql = new DbQuery();
        $sql->select('a.id_attribute');
        $sql->from('attribute', 'a');
        $sql->leftJoin('attribute_lang','al','al.id_attribute = a.id_attribute');
        $sql->where('a.id_attribute_group = '.(int)$id_attribute_group.' AND al.name = "' . pSQL($name).'"');

        return Db::getInstance()->getValue($sql);
    }

    public static function getGroupByAttributeId($id_attribute)
    {
        $sql = new DbQuery();
        $sql->select('name');
        $sql->from('attribute','a');
        $sql->leftJoin('attribute_group_lang','agl', 'agl.id_attribute_group = a.id_attribute_group AND agl.id_lang = '.Context::getContext()->language->id);
        $sql->where('a.id_attribute = '.(int) $id_attribute);
        return Db::getInstance()->getValue($sql);
    }
}