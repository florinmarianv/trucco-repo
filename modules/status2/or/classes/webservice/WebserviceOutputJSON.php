<?php
class WebserviceOutputJSON extends WebserviceOutputJSONCore {

    public function renderField($field)
    {
        $is_association = (isset($field['is_association']) && $field['is_association'] == true);

        if (is_array($field['value'])) {
            $tmp = array();
            foreach ($this->languages as $id_lang) {
                $tmp[] = array('id' => $id_lang, 'iso_code' => Language::getIsoById($id_lang), 'value' => $field['value'][$id_lang]);
            }
            if (count($tmp) == 1) {
                $field['value'] = $tmp[0]['value'];
            } else {
                $field['value'] = $tmp;
            }
        }
        // Case 1 : fields of the current entity (not an association)
        if (!$is_association) {
            $this->currentEntity[$field['sqlId']]  = $field['value'];
        } else { // Case 2 : fields of an associated entity to the current one
            $this->currentAssociatedEntity[] = array('name' => $field['entities_name'], 'key' => $field['sqlId'], 'value' => $field['value']);
        }
        return '';
    }
        
}