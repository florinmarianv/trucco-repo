<?php
class AttributeGroup extends AttributeGroupCore 
{
  protected $webserviceParameters = array(
    'objectsNodeName' => 'product_options',
    'objectNodeName' => 'product_option',
    'fields' => array(),
    'associations' => array(
        'product_option_values' => array(
            'resource' => 'product_option_value',
            'fields' => array(
                'id' => array()
            ),
        ),
    ),
  );
  public static function getIdByName($name) {
    $sql = new DbQuery();
    $sql->select('id_attribute_group');
    $sql->from('attribute_group_lang', 'agl');
    $sql->where('agl.name = "' . pSQL($name) . '"');

    return Db::getInstance()->getValue($sql);
  }
  public function getWsProductOptionValues() {
    $result = Db::getInstance()->executeS('
			SELECT a.id_attribute AS id
			FROM `' . _DB_PREFIX_ . 'attribute` a
			' . Shop::addSqlAssociation('attribute', 'a') . '
			WHERE a.id_attribute_group = ' . (int) $this->id
    );
    return $result;
  }
}
