<?php
class Product extends ProductCore {

  public static $definition = array(
        'table' => 'product',
        'primary' => 'id_product',
        'multilang' => true,
        'multilang_shop' => true,
        'fields' => array(
            /* Classic fields */
            'id_shop_default' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_manufacturer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_supplier' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'reference' => array('type' => self::TYPE_STRING, 'validate' => 'isReference', 'size' => 64),
            'supplier_reference' => array('type' => self::TYPE_STRING, 'validate' => 'isReference', 'size' => 64),
            'location' => array('type' => self::TYPE_STRING, 'validate' => 'isReference', 'size' => 64),
            'width' => array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'height' => array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'depth' => array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'weight' => array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'quantity_discount' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'ean13' => array('type' => self::TYPE_STRING, 'validate' => 'isEan13', 'size' => 13),
            'isbn' => array('type' => self::TYPE_STRING, 'validate' => 'isIsbn', 'size' => 32),
            'upc' => array('type' => self::TYPE_STRING, 'validate' => 'isUpc', 'size' => 12),
            'cache_is_pack' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'cache_has_attachments' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'is_virtual' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'state' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'additional_delivery_times' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'delivery_in_stock' => array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isGenericName',
                'size' => 255,
            ),
            'delivery_out_stock' => array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isGenericName',
                'size' => 255,
            ),

            /* Shop fields */
            'id_category_default' => array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedId'),
            'id_tax_rules_group' => array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedId'),
            'on_sale' => array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'online_only' => array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'ecotax' => array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice'),
            'minimal_quantity' => array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
            'low_stock_threshold' => array('type' => self::TYPE_INT, 'shop' => true, 'allow_null' => true, 'validate' => 'isInt'),
            'low_stock_alert' => array('type' => self::TYPE_BOOL, 'shop' => true, 'allow_null' => true, 'validate' => 'isBool'),
            'price' => array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice', 'required' => true),
            'wholesale_price' => array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice'),
            'unity' => array('type' => self::TYPE_STRING, 'shop' => true, 'validate' => 'isString'),
            'unit_price_ratio' => array('type' => self::TYPE_FLOAT, 'shop' => true),
            'additional_shipping_cost' => array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice'),
            'customizable' => array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
            'text_fields' => array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
            'uploadable_files' => array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
            'active' => array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'redirect_type' => array('type' => self::TYPE_STRING, 'shop' => true, 'validate' => 'isString'),
            'id_type_redirected' => array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedId'),
            'available_for_order' => array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'available_date' => array('type' => self::TYPE_DATE, 'shop' => true, 'validate' => 'isDateFormat'),
            'show_condition' => array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'condition' => array('type' => self::TYPE_STRING, 'shop' => true, 'validate' => 'isGenericName', 'values' => array('new', 'used', 'refurbished'), 'default' => 'new'),
            'show_price' => array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'indexed' => array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'visibility' => array('type' => self::TYPE_STRING, 'shop' => true, 'validate' => 'isProductVisibility', 'values' => array('both', 'catalog', 'search', 'none'), 'default' => 'both'),
            'cache_default_attribute' => array('type' => self::TYPE_INT, 'shop' => true),
            'advanced_stock_management' => array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'date_add' => array('type' => self::TYPE_DATE, 'shop' => true, 'validate' => 'isDate'),
            'date_upd' => array('type' => self::TYPE_DATE, 'shop' => true, 'validate' => 'isDate'),
            'pack_stock_type' => array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),

            /* Lang fields */
            'meta_description' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 512),
            'meta_keywords' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'meta_title' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'link_rewrite' => array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isLinkRewrite',
                'required' => false,
                'size' => 128,
                'ws_modifier' => array(
                    'http_method' => WebserviceRequest::HTTP_POST,
                    'modifier' => 'modifierWsLinkRewrite',
                ),
            ),
            'name' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCatalogName', 'required' => false, 'size' => 128),
            'description' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'description_short' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'available_now' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'available_later' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'IsGenericName', 'size' => 255),
        ),
        'associations' => array(
            'manufacturer' => array('type' => self::HAS_ONE),
            'supplier' => array('type' => self::HAS_ONE),
            'default_category' => array('type' => self::HAS_ONE, 'field' => 'id_category_default', 'object' => 'Category'),
            'tax_rules_group' => array('type' => self::HAS_ONE),
            'categories' => array('type' => self::HAS_MANY, 'field' => 'id_category', 'object' => 'Category', 'association' => 'category_product'),
            'stock_availables' => array('type' => self::HAS_MANY, 'field' => 'id_stock_available', 'object' => 'StockAvailable', 'association' => 'stock_availables'),
        ),
    );

  public function __construct($id_product = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null) {

    parent::$definition['fields']['url'] = array('type' => self::TYPE_STRING, 'validate' => 'isString');
    parent::__construct($id_product, $full, $id_lang, $id_shop, $context);
    self::$definition['fields']['reference'] = array('type' => self::TYPE_STRING, 'validate' => 'isReference', 'size' => 128);
    self::$definition['fields']['supplier_reference'] = array('type' => self::TYPE_STRING, 'validate' => 'isReference', 'size' => 128);
    $this->webserviceParameters['fields']['quantity'] = array('getter' => 'getWsGlobalStock');
    $this->webserviceParameters['fields']['price_tax_inc'] = array('getter' => 'getWsGlobalPrice', 'setter' => false);
    $this->webserviceParameters['fields']['id_retailer'] = array('getter' => 'getSellerId', 'setter' => false);
    $this->webserviceParameters['fields']['url'] = array('getter' => 'getWsUrl', 'setter' => false);
    unset($this->webserviceParameters['associations']['stock_availables']);
    unset($this->webserviceParameters['associations']['product_option_values']);

    $this->webserviceParameters['associations']['categories']['fields']['name'] = array();
    $this->webserviceParameters['associations']['images']['fields']['img'] = array();
    $this->webserviceParameters['associations']['combinations']['fields'] = array(
        'id_attribute' => array(),
        'name' => array(),
        'reference' => array(),
        'ean13' => array(),
        'upc' => array(),
        'quantity' => array(),
        'price' => array(),
        'price_tax_inc' => array(),
        'images' => array(),
    );
  }

  public static function getAttributesColorList(Array $products, $have_stock = true) {
    if (!count($products))
      return array();

    $id_lang = Context::getContext()->language->id;

    $check_stock = !Configuration::get('PS_DISP_UNAVAILABLE_ATTR');
    if (!$res = Db::getInstance()->executeS('
			SELECT pa.`id_product`, a.`color`, pac.`id_product_attribute`, ' . ($check_stock ? 'SUM(IF(stock.`quantity` > 0, 1, 0))' : '0') . ' qty, a.`id_attribute`, pai.`id_image`, al.`name`, IF(color = "", a.id_attribute, color) group_by
			FROM `' . _DB_PREFIX_ . 'product_attribute` pa
			' . Shop::addSqlAssociation('product_attribute', 'pa') .
            ($check_stock ? Product::sqlStock('pa', 'pa') : '') . '
			JOIN `' . _DB_PREFIX_ . 'product_attribute_combination` pac ON (pac.`id_product_attribute` = product_attribute_shop.`id_product_attribute`)
			JOIN `' . _DB_PREFIX_ . 'attribute` a ON (a.`id_attribute` = pac.`id_attribute`)
			JOIN `' . _DB_PREFIX_ . 'product_attribute_image` pai ON (pac.`id_product_attribute` = pai.`id_product_attribute`)
			JOIN `' . _DB_PREFIX_ . 'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = ' . (int) $id_lang . ')
			JOIN `' . _DB_PREFIX_ . 'attribute_group` ag ON (a.id_attribute_group = ag.`id_attribute_group`)
			WHERE pa.`id_product` IN (' . implode(array_map('intval', $products), ',') . ') AND ag.`is_color_group` = 1
			GROUP BY pa.`id_product`, `group_by`
			' . ($check_stock ? 'HAVING qty > 0' : '') . '
			ORDER BY a.`position` ASC;'
            )
    )
      return false;

    $colors = array();
    foreach ($res as $row) {
      if (Tools::isEmpty($row['color']) && !@filemtime(_PS_COL_IMG_DIR_ . $row['id_attribute'] . '.jpg'))
        continue;

      $colors[(int) $row['id_product']][] = array('id_product_attribute' => (int) $row['id_product_attribute'], 'color' => $row['color'], 'id_product' => $row['id_product'], 'name' => $row['name'], 'id_image' => $row['id_image'], 'id_attribute' => $row['id_attribute']);
    }

    return $colors;
  }

  public function getAdjacentProducts($category_id = null) {
    if ($category_id == null) {
      $category_id = (int) $this->id_category_default;
    }
    $sqlCurrentProduct = 'SELECT position
          FROM ' . _DB_PREFIX_ . 'category_product
          WHERE id_product = ' . (int) $this->id . '
          AND id_category = ' . $category_id;

    $position = Db::getInstance()->getValue($sqlCurrentProduct);
    // get products that are before and after
    $sqlPrevProduct = '
            SELECT cp.id_product, pl.link_rewrite, cp.position, pl.name
            FROM ' . _DB_PREFIX_ . 'category_product cp
            LEFT JOIN ' . _DB_PREFIX_ . 'product_lang pl ON (cp.id_product = pl.id_product)
            LEFT JOIN ' . _DB_PREFIX_ . 'product p ON (cp.id_product = p.id_product)
            JOIN ' . _DB_PREFIX_ . 'product_attribute pa ON (cp.id_product = pa.id_product)
            WHERE (cp.position < ' . (int) ($position ) . ' ) AND cp.id_category = ' . $category_id . ' AND pl.id_lang = ' . (Context::getContext()->language->id) . '
                    AND p.active = 1 AND pl.link_rewrite != ""
            ORDER BY cp.position DESC';

    $previous = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sqlPrevProduct);

    $sqlNextProduct = '
            SELECT cp.id_product, pl.link_rewrite, cp.position, pl.name
            FROM ' . _DB_PREFIX_ . 'category_product cp
            LEFT JOIN ' . _DB_PREFIX_ . 'product_lang pl ON (cp.id_product = pl.id_product)
            LEFT JOIN ' . _DB_PREFIX_ . 'product p ON (cp.id_product = p.id_product)
            JOIN ' . _DB_PREFIX_ . 'product_attribute pa ON (cp.id_product = pa.id_product)
            WHERE (cp.position > ' . (int) ($position ) . ' ) AND cp.id_category = ' . $category_id . ' AND pl.id_lang = ' . (Context::getContext()->language->id) . '
                    AND p.active = 1 AND pl.link_rewrite != ""
            ORDER BY cp.position ASC';

    $next = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sqlNextProduct);



    if ($previous) {
      $p = new Product($previous['id_product']);
      $attributes = $p->getAttributeCombinations(Context::getContext()->language->id);
      if (is_array($attributes) && count($attributes) > 0) {
        foreach ($attributes as $attribute) {
          if ($attribute['active'] && $attribute['default_on']) {
            $previous['id_combination'] = $attribute['id_product_attribute'];
          }
        }
      }
    }

    if ($next) {
      $p = new Product($next['id_product']);
      $attributes = $p->getAttributeCombinations(Context::getContext()->language->id);
      if (is_array($attributes) && count($attributes) > 0) {
        foreach ($attributes as $attribute) {
          if ($attribute['active'] && $attribute['default_on']) {
            $next['id_combination'] = $attribute['id_product_attribute'];
          }
        }
      }
    }
    return array('previous' => $previous, 'next' => $next);
  }

  public function getAttributesGroups($id_lang) {
    if (!Combination::isFeatureActive())
      return array();
    $sql = 'SELECT ag.`id_attribute_group`, ag.`is_color_group`, agl.`name` AS group_name, agl.`public_name` AS public_group_name,
					a.`id_attribute`, al.`name` AS attribute_name, a.`color` AS attribute_color, product_attribute_shop.`id_product_attribute`,
					IFNULL(stock.quantity, 0) as quantity, product_attribute_shop.`price`, product_attribute_shop.`ecotax`, product_attribute_shop.`weight`,
					product_attribute_shop.`default_on`, pa.`reference`, product_attribute_shop.`unit_price_impact`,
					product_attribute_shop.`minimal_quantity`, product_attribute_shop.`available_date`, ag.`group_type`
				FROM `' . _DB_PREFIX_ . 'product_attribute` pa
				' . Shop::addSqlAssociation('product_attribute', 'pa') . '
				' . Product::sqlStock('pa', 'pa') . '
				LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_combination` pac ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
				LEFT JOIN `' . _DB_PREFIX_ . 'attribute` a ON (a.`id_attribute` = pac.`id_attribute`)
				LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group` ag ON (ag.`id_attribute_group` = a.`id_attribute_group`)
				LEFT JOIN `' . _DB_PREFIX_ . 'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute`)
				LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group_lang` agl ON (ag.`id_attribute_group` = agl.`id_attribute_group`)
				' . Shop::addSqlAssociation('attribute', 'a') . '
				WHERE pa.`id_product` = ' . (int) $this->id . '
					AND al.`id_lang` = ' . (int) $id_lang . '
					AND agl.`id_lang` = ' . (int) $id_lang . '
				GROUP BY id_attribute_group, id_product_attribute
				ORDER BY ag.`position` ASC, a.`position` ASC, agl.`name` ASC';
    return Db::getInstance()->executeS($sql);
  }

  public static function getProductsPropertiesCustom($id_lang, $query_result) {
    $results_array = array();

    if (is_array($query_result))
      foreach ($query_result as $row) {
        if ($row2 = Product::getProductProperties($id_lang, $row)) {
          $row2['id_image'] = $row2['id_product'] . '-' . $row2['img_custom'];
          $results_array[] = $row2;
        }
      }
    return $results_array;
  }

  public static function getCombinationCover($id_product, $id_combination, Context $context = null) {
    return parent::getCombinationCover($id_product, $id_combination, $context);
    error_log($id_combination);
    if (!$context)
      $context = Context::getContext();
    $sql = 'SELECT image_shop.`id_image` FROM `' . _DB_PREFIX_ . 'product_attribute_image` i WHERE i.`id_attribute` = ' . (int) $id_combination . '
                AND i.`is_cover` = 1';
    $result = Db::getInstance()->getValue($sql);
    if (!($result !== false && count($result) > 0)) {
      $sql = 'SELECT image_shop.`id_image` FROM `' . _DB_PREFIX_ . 'product_attribute_image` i WHERE i.`id_attribute` = ' . (int) $id_combination . '
                LIMIT 1';
      $result = Db::getInstance()->getValue($sql);
      if (!($result !== false && count($result) > 0)) {
        $temp = Product::getCover($id_product, $context);
        $result = $temp['id_image'];
      }
    }
    return $result;
  }

  public function getCombinationImages($id_lang) {
    if (!Combination::isFeatureActive())
      return false;

    $product_attributes = Db::getInstance()->executeS(
            'SELECT `id_product_attribute`
			FROM `' . _DB_PREFIX_ . 'product_attribute`
			WHERE `id_product` = ' . (int) $this->id
    );

    if (!$product_attributes)
      return false;

    $ids = array();

    foreach ($product_attributes as $product_attribute)
      $ids[] = (int) $product_attribute['id_product_attribute'];

    $result = Db::getInstance()->executeS('
			SELECT pai.`id_image`, pai.`id_product_attribute`, il.`legend`
			FROM `' . _DB_PREFIX_ . 'product_attribute_image` pai
			LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (il.`id_image` = pai.`id_image`)
			LEFT JOIN `' . _DB_PREFIX_ . 'image` i ON (i.`id_image` = pai.`id_image`)
			WHERE pai.`id_product_attribute` IN (' . implode(', ', $ids) . ') AND il.`id_lang` = ' . (int) $id_lang . ' ORDER by i.`position`'
    );

    if (!$result)
      return false;

    $images = array();

    foreach ($result as $row)
      $images[$row['id_product_attribute']][] = $row;

    return $images;
  }

  public function getWsGlobalPrice() {
    return $this->getPrice(true);
  }

  public function getWsGlobalStock() {
    return StockAvailable::getQuantityAvailableByProduct($this->id);
  }

  public function getWsImages() {
    $imagesIds = parent::getWsImages();
    foreach ($imagesIds as $key => $id_image) {
      $imagesIds[$key]['img'] = Context::getContext()->link->getImageLink('product-image' . $id_image['id'], $this->id . '-' . $id_image['id']);
    }
    return $imagesIds;
  }

  public function getWsUrl() {
    $link = new Link();
    $product = new Product($this->id);
    return $link->getProductLink($product);
  }

  public function getWsCategories() {
    $categoriesIds = parent::getWsCategories();
    foreach ($categoriesIds as $key => $id_category) {
      $categoryObj = new Category($id_category['id'], Context::getContext()->language->id);
      $categoriesIds[$key]['name'] = $categoryObj->name;
    }
    return $categoriesIds;
  }

  public function getWsCombinations() {
    $combinationsIds = parent::getWsCombinations();
    foreach ($combinationsIds as $key => $combination) {
      $combinationsObj = new Combination($combination['id']);
      $combinationsIds[$key]['id_attribute'] = $combination['id'];
      $attributeNames = $combinationsObj->getAttributesName(Context::getContext()->language->id);
      $attributeValues = array();
      $i = 1;
      if (Tools::getValue('language')) {
        foreach ($attributeNames as $attributeName) {
          $attributeValues[Tools::getValue('language')][] = array(Attribute::getGroupByAttributeId($attributeName['id_attribute']) => $attributeName['name']);
        }
      } else {
        foreach (Language::getLanguages() as $languge) {
          $i = 1;
          foreach ($attributeNames as $attributeName) {
            $attributeValues[$languge['id_lang']][] = array(Attribute::getGroupByAttributeId($attributeName['id_attribute']) => $attributeName['name']);
          }
        }
      }
      $combinationsIds[$key]['name'] = $attributeValues;
      $combinationsIds[$key]['reference'] = $combinationsObj->reference;
      $combinationsIds[$key]['ean13'] = $combinationsObj->ean13;
      $combinationsIds[$key]['upc'] = $combinationsObj->upc;
      $combinationsIds[$key]['quantity'] = StockAvailable::getQuantityAvailableByProduct($this->id, $combination['id']);
      $combinationsIds[$key]['price'] = Tools::ps_round($this->price + $combinationsObj->price, 2);
      $combinationsIds[$key]['price_tax_inc'] = $this->getPrice(true, $combinationsObj->id);
      $imagesCombination = array();
      $combinationImages = $combinationsObj->getWsImages();
      $i = 0;
      if (Tools::getValue('language')) {
        foreach ($combinationImages as $image) {
          $imagesCombination[Tools::getValue('language')][$i++] = Context::getContext()->link->getImageLink('combination-image' . $image['id'], $this->id_product . '-' . $image['id']);
        }
      } else {
        foreach (Language::getLanguages() as $language) {
          foreach ($combinationImages as $image) {
            $imagesCombination[$language['id_lang']][$i++] = Context::getContext()->link->getImageLink('combination-image' . $image['id'], $this->id_product . '-' . $image['id']);
          }
          $i = 0;
        }
      }
      $combinationsIds[$key]['images'] = $imagesCombination;
    }

    return $combinationsIds;
  }

  public function setCoverWs($id_image) {
    Db::getInstance()->execute('UPDATE `' . _DB_PREFIX_ . 'image_shop` image_shop, `' . _DB_PREFIX_ . 'image` i
			SET image_shop.`cover` = NULL
			WHERE i.`id_product` = ' . (int) $this->id . ' AND i.id_image = image_shop.id_image
			AND image_shop.id_shop=' . (int) Context::getContext()->shop->id);
    Db::getInstance()->execute('UPDATE `' . _DB_PREFIX_ . 'image_shop`
			SET `cover` = 1 WHERE `id_image` = ' . (int) $id_image);

    return true;
  }

  public function addWs($autodate = true, $null_values = false) {
    $success = $this->add($autodate, $null_values);
    if ($success && Configuration::get('PS_SEARCH_INDEXATION')) {
      Search::indexation(false, $this->id);
    }
    if ($success)
      $this->addSupplierReference($this->id_supplier, 0, $this->supplier_reference, $this->wholesale_price, null);
    return $success;
  }

  public function updateWs($null_values = false) {
    $success = parent::update($null_values);
    if ($success && Configuration::get('PS_SEARCH_INDEXATION')) {
      Search::indexation(false, $this->id);
    }
    if ($success)
      $this->addSupplierReference($this->id_supplier, 0, $this->supplier_reference, $this->wholesale_price, null);
    Hook::exec('updateProduct', array('id_product' => (int) $this->id));
    return $success;
  }

}
