<?php
class Currency extends CurrencyCore {

  public static function getIsoCodeByQuery($id_shop = 0) {
    $query = new DbQuery();
    $query->select('c.iso_code');
    $query->from('currency', 'c');
    $query->where('deleted = 0');

    if (Shop::isFeatureActive() && $id_shop > 0) {
      $query->leftJoin('currency_shop', 'cs', 'cs.id_currency = c.id_currency');
      $query->where('id_shop = ' . (int) $id_shop);
    }
    return $query;
  }

  public static function getIsoCodeById($id_currency, $id_shop = 0) {
    $cache_id = 'Currency::getIsoCodeById_' . pSQL($id_currency) . '-' . (int) $id_shop;
    if (!Cache::isStored($cache_id)) {
      $query = Currency::getIsoCodeByQuery($id_shop);
      $query->where('id_currency = ' . (int) ($id_currency));

      $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query->build());
      Cache::store($cache_id, $result);
      return $result;
    }
    return Cache::retrieve($cache_id);
  }

}
