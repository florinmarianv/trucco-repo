<?php
class Customer extends CustomerCore {

  public static $definition = array(
      'table' => 'customer',
      'primary' => 'id_customer',
      'fields' => array(
          'secure_key' => array('type' => self::TYPE_STRING, 'validate' => 'isMd5', 'copy_post' => false),
          'lastname' => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true, 'size' => 32),
          'firstname' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255),
          'email' => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'required' => true, 'size' => 128),
          'passwd' => array('type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'required' => true, 'size' => 32),
          'last_passwd_gen' => array('type' => self::TYPE_STRING, 'copy_post' => false),
          'id_gender' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'birthday' => array('type' => self::TYPE_DATE, 'validate' => 'isBirthDate'),
          'newsletter' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
          'newsletter_date_add' => array('type' => self::TYPE_DATE, 'copy_post' => false),
          'ip_registration_newsletter' => array('type' => self::TYPE_STRING, 'copy_post' => false),
          'optin' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
          'website' => array('type' => self::TYPE_STRING, 'validate' => 'isUrl'),
          'company' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
          'siret' => array('type' => self::TYPE_STRING, 'validate' => 'isSiret'),
          'ape' => array('type' => self::TYPE_STRING, 'validate' => 'isApe'),
          'outstanding_allow_amount' => array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'copy_post' => false),
          'show_public_prices' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
          'id_risk' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
          'max_payment_days' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
          'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
          'deleted' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
          'note' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'size' => 65000, 'copy_post' => false),
          'is_guest' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
          'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
          'id_shop_group' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
          'id_default_group' => array('type' => self::TYPE_INT, 'copy_post' => false),
          'id_lang' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
          'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
          'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
      ),
  );

  public function __construct($id = null) {
    $this->id_default_group = (int) Configuration::get('PS_CUSTOMER_GROUP');
    parent::__construct($id);
    $this->webserviceParameters['associations']['addresses']['resource'] = 'address';
    $this->webserviceParameters['associations']['addresses']['fields'] = array(
        'id_address' => array(),
        'id_address' => array(),
        'id_country' => array(),
        'id_state' => array(),
        'id_customer' => array(),
        'id_manufacturer' => array(),
        'id_supplier' => array(),
        'id_warehouse' => array(),
        'alias' => array(),
        'company' => array(),
        'lastname' => array(),
        'firstname' => array(),
        'address1' => array(),
        'address2' => array(),
        'postcode' => array(),
        'city' => array(),
        'other' => array(),
        'phone' => array(),
        'phone_mobile' => array(),
        'vat_number' => array(),
        'dni' => array(),
        'date_add' => array(),
        'date_upd' => array(),
        'active' => array(),
        'deleted' => array(),
        'country' => array(),
        'state' => array(),
        'iso_code' => array(),
        'state_iso' => array(),
    );
  }

  public function getWsAddresses() {


    $share_order = (bool) Context::getContext()->shop->getGroup()->share_order;
    $cache_id = 'Customer::getAddresses' . (int) $this->id . '-' . (int) Context::getContext()->language->id . '-' . $share_order;
    if (!Cache::isStored($cache_id)) {
      $sql = 'SELECT DISTINCT a.*,c.iso_code, cl.`name` AS country, s.name AS state, s.iso_code AS state_iso
					FROM `' . _DB_PREFIX_ . 'address` a
					LEFT JOIN `' . _DB_PREFIX_ . 'country` c ON (a.`id_country` = c.`id_country`)
					LEFT JOIN `' . _DB_PREFIX_ . 'country_lang` cl ON (c.`id_country` = cl.`id_country`)
					LEFT JOIN `' . _DB_PREFIX_ . 'state` s ON (s.`id_state` = a.`id_state`)
					' . ($share_order ? '' : Shop::addSqlAssociation('country', 'c')) . ' 
					WHERE `id_lang` = ' . (int) Context::getContext()->language->id . ' AND `id_customer` = ' . (int) $this->id . ' AND a.`deleted` = 0';

      $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
      Cache::store($cache_id, $result);
    }
    return Cache::retrieve($cache_id);
  }

}
