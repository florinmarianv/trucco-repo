<?php
class Category extends CategoryCore {

    public static $definition = array(
        'table' => 'category',
        'primary' => 'id_category',
        'multilang' => true,
        'multilang_shop' => true,
        'fields' => array(
            'nleft' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'nright' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'level_depth' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'active' => 			array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'id_parent' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_shop_default' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'is_root_category' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'position' => 			array('type' => self::TYPE_INT),
            'date_add' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'name' => 				array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCatalogName', 'required' => true, 'size' => 128),
            'link_rewrite' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isLinkRewrite', 'required' => true, 'size' => 128),
            'description' => 		array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'meta_title' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 128),
            'meta_description' => 	array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'meta_keywords' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
        ),
    );

    public function getTotalProductsWithCombinations($check_access = true, Context $context = null)
    {
        if (!$context)
            $context = Context::getContext();
        if ($check_access && !$this->checkAccess($context->customer->id))
            return false;

        $sql = 'SELECT COUNT(DISTINCT(pac.`id_attribute`), cp.`id_product`)
                  FROM `'._DB_PREFIX_.'product` p
                  INNER JOIN '._DB_PREFIX_.'product_shop product_shop ON (product_shop.id_product = p.id_product AND product_shop.id_shop = 1)
                  INNER JOIN '._DB_PREFIX_.'product_attribute pa ON (pa.id_product = p.id_product)
                  INNER JOIN '._DB_PREFIX_.'product_attribute_combination pac ON (pac.id_product_attribute = pa.id_product_attribute)
                  INNER JOIN '._DB_PREFIX_.'attribute a ON a.id_attribute = pac.id_attribute
                  INNER JOIN '._DB_PREFIX_.'attribute_group ag ON a.id_attribute_group = ag.id_attribute_group
                  LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`
                  WHERE cp.`id_category` = ' . $this->id .' AND product_shop.`visibility` IN ("both", "catalog") AND product_shop.`active` = 1 AND pac.active = 1 AND ag.is_color_group = 1';

        $result = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        return $result;
    }

}