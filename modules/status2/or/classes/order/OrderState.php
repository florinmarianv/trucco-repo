<?php
class OrderState extends OrderStateCore {
    public static $definition = array(
        'table' => 'order_state',
        'primary' => 'id_order_state',
        'multilang' => true,
        'fields' => array(
            'send_email' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'module_name' => array('type' => self::TYPE_STRING, 'validate' => 'isModuleName'),
            'invoice' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'color' => 		array('type' => self::TYPE_STRING, 'validate' => 'isColor'),
            'logable' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'shipped' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'unremovable' =>array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'delivery' =>	array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'hidden' =>		array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'paid' =>		array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'deleted' =>	array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'name' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 64),
            'template' => 	array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isTplName', 'size' => 64),
        ),
    );

    public static function getOrderStateToCarrier()
    {
        $cache_id = 'OrderState::getOrderStateToCarrier';
        if (!Cache::isStored($cache_id))
        {
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT id_order_state
			FROM `'._DB_PREFIX_.'order_state` os
			WHERE deleted = 0 ');
            Cache::store($cache_id, $result);
        }
        return Cache::retrieve($cache_id);
    }
}