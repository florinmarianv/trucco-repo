<?php
class Combination extends CombinationCore {

  public function __construct($id = null, $id_lang = null, $id_shop = null) {

    self::$definition['fields']['reference'] = array('type' => self::TYPE_STRING, 'size' => 128);
    self::$definition['fields']['supplier_reference'] = array('type' => self::TYPE_STRING, 'size' => 128);

    ObjectModel::__construct($id, $id_lang, $id_shop);
  }

  public static function getCombinationByReference($combination_reference) {

    $sql = new DbQuery();
    $sql->select('id_product_attribute');
    $sql->from('product_attribute');
    $sql->where('reference = \'' . $combination_reference . '\'');

    return (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
  }

}
