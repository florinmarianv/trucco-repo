<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_2_0($module)
{
    $sql= "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."preimport_status2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Id_MarcaArticulo` varchar(30) DEFAULT NULL,
  `Id_Temporada` varchar(30) DEFAULT NULL,
  `Id_Articulo` varchar(30) DEFAULT NULL,
  `Id_Color` varchar(30) DEFAULT NULL,
  `Id_Talla` varchar(30) DEFAULT NULL,
  `EAN` varchar(15) DEFAULT NULL,
  `Id_Tipo` varchar(30) DEFAULT NULL,
  `FecUltModif` varchar(50) DEFAULT NULL,
  `Descripcion_1` varchar(30) DEFAULT NULL,
  `Precio` varchar(20) DEFAULT NULL,
  `PrecioRebajas` varchar(30) DEFAULT NULL,
  `DescTemp` varchar(20) DEFAULT NULL,
  `Familia` varchar(20) DEFAULT NULL,
  `Subfamilia` varchar(30) DEFAULT NULL,
  `Seccion` varchar(30) DEFAULT NULL,
  `Tipo` varchar(30) DEFAULT NULL,
  `Descripcion_Color` varchar(30) DEFAULT NULL,
  `Descripcion_IdTalla` varchar(30) DEFAULT NULL,
  `OrdenTemporada` varchar(25) DEFAULT NULL,
  `Imagen` varchar(90) DEFAULT NULL,
  `Descripcion_2` varchar(80) DEFAULT NULL,
  `Id_Paleta` varchar(60) DEFAULT NULL,
  `Descatalogado` varchar(20) DEFAULT NULL,
  `Descripcion_Familia` varchar(40) DEFAULT NULL,
  `Descripcion_SubFamilia` varchar(40) DEFAULT NULL,
  `CodigoEstadistico` varchar(50) DEFAULT NULL,
  `Descripcion_CodEst` varchar(50) DEFAULT NULL,
  `Filtro_Ad_1` varchar(30) DEFAULT NULL,
  `Descripcion_Fd1` varchar(30) DEFAULT NULL,
  `Filtro_Ad_2` varchar(30) DEFAULT NULL,
  `Descripcion_Fd2` varchar(25) DEFAULT NULL,
  `Filtro_Ad_3` varchar(30) DEFAULT NULL,
  `Descripcion_Fd3` varchar(30) DEFAULT NULL,
  `Descripcion_Marca` varchar(30) DEFAULT NULL,
  `Descripcion_Seccion` varchar(25) DEFAULT NULL,
  `Descripcion_Tipo` varchar(30) DEFAULT NULL,
  `ECommerce` varchar(10) DEFAULT NULL,
  `Descripcion_WEB_Articulo` varchar(30) DEFAULT NULL,
  `Descripcion_WEB_Color` varchar(30) DEFAULT NULL,
  `ECommerce_CT` varchar(10) DEFAULT NULL,
  `Resuelto` varchar(1) DEFAULT NULL,
  `uniq_id` varchar(250) DEFAULT NULL,

  PRIMARY KEY (`id`)
)";
    DB::getInstance()->execute($sql);
    
     $sql= "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."preimport_status2_temp` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `Id_MarcaArticulo` varchar(30) DEFAULT NULL,
      `Id_Temporada` varchar(30) DEFAULT NULL,
      `Id_Articulo` varchar(30) DEFAULT NULL,
      `Id_Color` varchar(30) DEFAULT NULL,
      `Id_Talla` varchar(30) DEFAULT NULL,
      `EAN` varchar(15) DEFAULT NULL,
      `Id_Tipo` varchar(30) DEFAULT NULL,
      `FecUltModif` varchar(50) DEFAULT NULL,
      `Descripcion_1` varchar(30) DEFAULT NULL,
      `Precio` varchar(20) DEFAULT NULL,
      `PrecioRebajas` varchar(30) DEFAULT NULL,
      `DescTemp` varchar(20) DEFAULT NULL,
      `Familia` varchar(20) DEFAULT NULL,
      `Subfamilia` varchar(30) DEFAULT NULL,
      `Seccion` varchar(30) DEFAULT NULL,
      `Tipo` varchar(30) DEFAULT NULL,
      `Descripcion_Color` varchar(30) DEFAULT NULL,
      `Descripcion_IdTalla` varchar(30) DEFAULT NULL,
      `OrdenTemporada` varchar(25) DEFAULT NULL,
      `Imagen` varchar(90) DEFAULT NULL,
      `Descripcion_2` varchar(80) DEFAULT NULL,
      `Id_Paleta` varchar(60) DEFAULT NULL,
      `Descatalogado` varchar(20) DEFAULT NULL,
      `Descripcion_Familia` varchar(40) DEFAULT NULL,
      `Descripcion_SubFamilia` varchar(40) DEFAULT NULL,
      `CodigoEstadistico` varchar(50) DEFAULT NULL,
      `Descripcion_CodEst` varchar(50) DEFAULT NULL,
      `Filtro_Ad_1` varchar(30) DEFAULT NULL,
      `Descripcion_Fd1` varchar(30) DEFAULT NULL,
      `Filtro_Ad_2` varchar(30) DEFAULT NULL,
      `Descripcion_Fd2` varchar(25) DEFAULT NULL,
      `Filtro_Ad_3` varchar(30) DEFAULT NULL,
      `Descripcion_Fd3` varchar(30) DEFAULT NULL,
      `Descripcion_Marca` varchar(30) DEFAULT NULL,
      `Descripcion_Seccion` varchar(25) DEFAULT NULL,
      `Descripcion_Tipo` varchar(30) DEFAULT NULL,
      `ECommerce` varchar(10) DEFAULT NULL,
      `Descripcion_WEB_Articulo` varchar(30) DEFAULT NULL,
      `Descripcion_WEB_Color` varchar(30) DEFAULT NULL,
      `ECommerce_CT` varchar(10) DEFAULT NULL,
      `Resuelto` varchar(1) DEFAULT NULL,
      `uniq_id` varchar(250) DEFAULT NULL,

      PRIMARY KEY (`id`)
    )";

    return DB::getInstance()->execute($sql);
}
