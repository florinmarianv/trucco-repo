<?php
/**
* Card payment REDSYS virtual POS
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2020 idnovate
*  @license   See above
*/

class RedsysOkPaymentModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $id_cart = (int)Tools::getValue('id_cart');
        $id_module = (int)Tools::getValue('id_module');
        $id_order = Order::getOrderByCartId($id_cart);
        $key = Tools::getValue('key');

        if (version_compare(_PS_VERSION_, '1.5.4', '<')) {
            $url = __PS_BASE_URI__.'order-confirmation.php?id_cart='.$id_cart.'&id_module='.$id_module.'&id_order='.$id_order.'&key='.$key;
        } else {
            if (Tools::getValue('Ds_MerchantParameters')) {
                $merchantParameters = Tools::getValue('Ds_MerchantParameters');
                $signatureVersion = Tools::getValue('Ds_SignatureVersion');
                $signature = Tools::getValue('Ds_Signature');

                $values = array(
                        'key' => $key,
                        'id_module' => (int)$id_module,
                        'id_cart' => (int)$id_cart,
                        'id_order' => (int)$id_order,
                        'Ds_MerchantParameters' => $merchantParameters,
                        'Ds_SignatureVersion' => $signatureVersion,
                        'Ds_Signature' => $signature
                    );
            } else {
                $values = array(
                        'key' => $key,
                        'id_module' => (int)$id_module,
                        'id_cart' => (int)$id_cart,
                        'id_order' => (int)$id_order
                    );
            }
            $url = $this->context->link->getPageLink('order-confirmation.php', false, null, $values);
        }

        $this->context->smarty->assign(array(
            'url' => $url,
        ));

        $this->context->smarty->display(_PS_MODULE_DIR_.'redsys/views/templates/front/parent_redirection.tpl');
        die;
    }
}
