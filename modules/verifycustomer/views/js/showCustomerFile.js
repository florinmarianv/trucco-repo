/*
* 2017 Singleton software
*
*  @author Singleton software <info@singleton-software.com>
*  @copyright 2017 Singleton software
*/
$(document).ready(function() {
	if (typeof linkToFile != 'undefined') {
		if (typeof higherThen176 !== 'undefined') {
			$(".form-horizontal[name='customer'] .card-block").after('<a href="' + linkToFile + '" target="_blank">' + fileTypeText + '</a>');
		} else {
			$("#customer_form .form-wrapper").after('<a href="' + linkToFile + '" target="_blank">' + fileTypeText + '</a>');
		}
	}
});