<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

class OAuthDataStore
{

    public function lookupConsumer($consumer_key)
    {
        // implement me
    }

    public function lookupToken($consumer, $token_type, $token)
    {
        // implement me
    }

    public function lookupNonce($consumer, $token, $nonce, $timestamp)
    {
        // implement me
    }

    public function newRequestToken($consumer, $callback = null)
    {
        // return a new token attached to this consumer
    }

    public function newAccessToken($token, $consumer, $verifier = null)
    {
        // return a new access token attached to this consumer
        // for the user associated with this token if the request token
        // is authorized
        // should also invalidate the request token
    }
}
