<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

// vim: foldmethod=marker

/* Generic exception class
 */
//TDK:: use library from prestashop
require_once(dirname(__FILE__) . '/../../../config/config.inc.php');
require_once(dirname(__FILE__) . '/../../../init.php');

if (!class_exists('OAuthException')) {

    class OAuthException extends Exception
    {
        // pass
    }
}

require_once(dirname(__FILE__) . '/OAuthConsumer.php');
require_once(dirname(__FILE__) . '/OAuthToken.php');
require_once(dirname(__FILE__) . '/OAuthSignatureMethod.php');
require_once(dirname(__FILE__) . '/OAuthSignatureMethodHMACSHA1.php');
require_once(dirname(__FILE__) . '/OAuthSignatureMethodPLAINTEXT.php');
require_once(dirname(__FILE__) . '/OAuthSignatureMethodRSASHA1.php');
require_once(dirname(__FILE__) . '/OAuthRequest.php');
require_once(dirname(__FILE__) . '/OAuthServer.php');
require_once(dirname(__FILE__) . '/OAuthDataStore.php');
require_once(dirname(__FILE__) . '/OAuthUtil.php');
