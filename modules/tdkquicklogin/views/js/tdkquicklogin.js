/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
* 
**/
$(document).ready(function(){

	//TDK:: build sliderbar for types
	if($('.tdkql-slidebar').length)
	{
		for (var i=0; i<3; i++)
		{		
			$('.tdkql-slidebar').first().clone().insertAfter('.tdkql-slidebar:last');
			var slidebar_class = '';
			if (tdkql_push == 1)
			{
				slidebar_class = 'push_';
			}
			switch (i)
			{
				case 0:
					slidebar_class += 'slidebar_right';
					break;
				case 1:
					slidebar_class += 'slidebar_top';
					break;
				case 2:
					slidebar_class += 'slidebar_bottom';
					break;
			}
			
			$('.tdkql-slidebar').last().addClass(slidebar_class);
			
		}
		if (tdkql_push == 1)
		{
			$('.tdkql-slidebar').first().addClass('push_slidebar_left');
		}
		else
		{
			$('.tdkql-slidebar').first().addClass('slidebar_left');
		}
	}
	//TDK:: login action
	$('.tdkql-quicklogin').click(function(){
		if (!$(this).hasClass('tdkql-dropdown'))
		{
			if (!$(this).hasClass('active'))
			{
				$(this).addClass('active');
				var type = $(this).data('type');
				var layout = $(this).data('layout');
				//TDK:: disable/enable social login for tdkplatform
				var enable_sociallogin = $(this).data('enable-sociallogin');
				if (type == 'popup')
				{
					if (enable_sociallogin == '' || enable_sociallogin == 'enable')
					{
						$('.tdkql-quicklogin-modal .tdkql-social-login').show();
					}
					else
					{
						$('.tdkql-quicklogin-modal .tdkql-social-login').hide();
					}
					if (layout != 'both')
					{
						//TDK:: active tab navigation					
						$('.tdkql-quicklogin-modal .tdkql-bt').removeClass('tdkql-active');
						$('.tdkql-quicklogin-modal .tdkql-bt-'+layout).addClass('tdkql-active');
						$('.tdkql-quicklogin-modal .tdkql-action').show();
						
						$('.tdkql-quicklogin-modal .tdkql-form').removeClass('tdkql-form-active full-width').addClass('tdkql-form-inactive full-width');
						$('.tdkql-quicklogin-modal .tdkql-'+layout+'-form').removeClass('tdkql-form-inactive').addClass('tdkql-form-active full-width');
					}
					else
					{
						//TDK:: inactive tab navigation
						$('.tdkql-quicklogin-modal .tdkql-action').hide();
						
						$('.tdkql-quicklogin-modal .tdkql-form').removeClass('tdkql-form-inactive full-width').addClass('tdkql-form-active');
					}
					$('.tdkql-quicklogin-modal').modal('show');
				}
				if (type == 'slidebar_left' || type == 'slidebar_right' || type == 'slidebar_top' || type == 'slidebar_bottom')
				{
					if (enable_sociallogin == '' || enable_sociallogin == 'enable')
					{
						$('.tdkql-slidebar .tdkql-social-login').show();
					}
					else
					{
						$('.tdkql-slidebar .tdkql-social-login').hide();
					}
					var prefix_class = type;
					if (tdkql_push == 1)
					{
						prefix_class = 'push_'+ prefix_class;
					}
					if (layout != 'both')
					{
						//TDK:: active tab navigation
						$('.tdkql-slidebar .tdkql-bt').removeClass('tdkql-active');
						$('.tdkql-slidebar .tdkql-bt-'+layout).addClass('tdkql-active');
						$('.tdkql-slidebar .tdkql-action').show();
						
						$('.tdkql-slidebar.'+prefix_class+' .tdkql-form').removeClass('tdkql-form-active full-width').addClass('tdkql-form-inactive full-width');
						$('.tdkql-slidebar.'+prefix_class+' .tdkql-'+layout+'-form').removeClass('tdkql-form-inactive').addClass('tdkql-form-active full-width');
					}
					else
					{
						//TDK:: inactive tab navigation
						$('.tdkql-slidebar .tdkql-action').hide();
						
						$('.tdkql-slidebar.'+prefix_class+' .tdkql-form').removeClass('tdkql-form-inactive full-width').addClass('tdkql-form-active');
					}
					$('.tdkql-slidebar.'+prefix_class).addClass('active');
					$('.tdkql-mask').addClass('active');
					$('body').addClass('tdkql-active-slidebar');
					//TDK:: check auto gen rtl
					if (tdkql_is_gen_rtl && prestashop.language.is_rtl == 1)
					{
						$('body').addClass('tdkql_is_gen_rtl');
					}
					
					if (tdkql_push == 1)
					{
						$('body').addClass('tdkql-active-push');
						var push_value;
						var push_type;
						
						if (type == 'slidebar_left' || type == 'slidebar_right')
						{						
							push_type = "X";
							if (type == 'slidebar_left')
							{
								push_value = $('.tdkql-slidebar.'+prefix_class).outerWidth();
							}
							if (type == 'slidebar_right')
							{
								push_value = -$('.tdkql-slidebar.'+prefix_class).outerWidth();
							}
						}
						
						if (type == 'slidebar_top' || type == 'slidebar_bottom')
						{						
							push_type = "Y";
							if (type == 'slidebar_top')
							{
								push_value = $('.tdkql-slidebar.'+prefix_class).outerHeight();
							}
							if (type == 'slidebar_bottom')
							{
								push_value = -$('.tdkql-slidebar.'+prefix_class).outerHeight();
							}
						}
						
						$('body.tdkql-active-push main').css({
							"-moz-transform": "translate"+push_type+"("+push_value+"px)",
							"-webkit-transform": "translate"+push_type+"("+push_value+"px)",
							"-o-transform": "translate"+push_type+"("+push_value+"px)",
							"-ms-transform": "translate"+push_type+"("+push_value+"px)",
							"transform": "translate"+push_type+"("+push_value+"px)",
						})
						
					}
				}
			}
		}
		
	});
	// $('.tdkql-quicklogin').hover(function(){
		// if (!$(this).hasClass('active'))
		// {
			// var type = $(this).data('type');
			// if (type == 'slidebar_left' || type == 'slidebar_right' || type == 'slidebar_top' || type == 'slidebar_bottom')
			// {
				// var slidebar_class = type;
				// if (tdkql_push == 1)
				// {
					// slidebar_class = 'push_'+type;
				// }
				// $('.tdkql-slidebar').attr('class', 'tdkql-slidebar '+slidebar_class);
			// }
		// }
	// })
	activeEventModalTdkQuickLogin();
	activeEventSlidebarTdkQuickLogin();
	$('.tdkql-dropdown-wrapper').click(function (e) {
		e.stopPropagation();
	});
	//TDK:: display forgotpass form
	// $('.tdkql-forgotpass').hover(function(){
		// var parent_obj = $(this).parents('.tdkql-quicklogin-form');
		// parent_obj.css({'height': 'auto'});
	// });
	$('.tdkql-forgotpass').click(function(){
		var parent_obj = $(this).parents('.tdkql-login-form');
		if (!$(this).hasClass('active'))
		{
			$(this).addClass('active');
			//parent_obj.find('.tdkql-callregister').addClass('disabled');
			parent_obj.find('.tdkql-resetpass-form').slideDown('fast', function(){
				// parent_obj.find('.tdkql-callregister').removeClass('disabled');
			});
		}
		else
		{
			$(this).removeClass('active');
			//parent_obj.find('.tdkql-callregister').addClass('disabled');
			parent_obj.find('.tdkql-resetpass-form').slideUp('fast',function(){
				// parent_obj.find('.tdkql-callregister').removeClass('disabled');
			});
		}
		
		return false;
	});
	
	
	//TDK:: display login form
	// $('.tdkql-calllogin, .tdkql-callregister').hover(function(){
		// if (!$(this).hasClass('disabled'))
		// {
			
			// var parent_obj = $(this).parents('.tdkql-quicklogin-form');
			
			// parent_obj.width(parent_obj.find('.tdkql-form-active').outerWidth()).height(parent_obj.find('.tdkql-form-active').outerHeight());
			// parent_obj.addClass('change');
		// }
	// }, function(){
		// if ($('.tdkql-quicklogin-form.change').length)
		// {
			// setTimeout(function(){
				// var parent_obj = $(this).parents('.tdkql-quicklogin-form');
				// $('.tdkql-quicklogin-form.change').removeClass('change').css({'width':'100%','height': '100%'});
			// },300);
		// }
			
	// });
	$('.tdkql-calllogin-action').click(function(){	
		callLoginForm($(this));
		return false;
	})
	
	//TDK:: display register form
	$('.tdkql-callregister-action').click(function(){
		callRegisterForm($(this));
		return false;
	});
	
	//TDK:: button tab (only form)
	$('.tdkql-bt').click(function(){
		if (!$(this).hasClass('active'))
		{		
			if ($(this).hasClass('tdkql-bt-login'))
			{
				callLoginForm($(this));
			}
			if ($(this).hasClass('tdkql-bt-register'))
			{
				callRegisterForm($(this));
			}
		}
	});
	
	$('.tdkql-resetpass-form-content').submit(function(){
		// if ($(this).find('.form-group.tdkql-has-error').length)
		if ($(this).find('.tdkql-reset-pass-bt').hasClass('validate-ok') || $(this).find('.tdkql-has-error').length)
		{
			return false;
		}
	});
	
	//TDK:: button send email reset password
	$('.tdkql-reset-pass-bt').click(function(){
		if (!$(this).hasClass('active') && !$(this).hasClass('success'))
		{		
			var object_e = $(this);
			var parent_obj = object_e.parents('.tdkql-resetpass-form-content');
			parent_obj.find('.tdkql-form-mesg.has-danger').fadeOut();
			parent_obj.find('.tdkql-form-mesg.has-success').fadeOut();
			// var check_submit = true;
			object_e.addClass('active');
			object_e.find('.tdkql-bt-txt').hide();
			object_e.find('.tdkql-loading').css({'display':'block'});
			
			parent_obj.find('input').each(function(){
				// console.log($(this));
				if (!$(this).val())
				{
					$(this).parent('.form-group').addClass('tdkql-has-error');
					object_e.removeClass('active');
					object_e.find('.tdkql-bt-txt').show();
					object_e.find('.tdkql-loading').hide();
					return false;
				}
				else
				{
					if (!validateEmail($(this).val()))
					{
						$(this).parent('.form-group').addClass('tdkql-has-error');
						object_e.removeClass('active');
						object_e.find('.tdkql-bt-txt').show();
						object_e.find('.tdkql-loading').hide();
						return false;
					}
					else
					{
						$(this).parent('.form-group').removeClass('tdkql-has-error');
					}
				}
			});
			
			
			// console.log('pass');
			// $('.tdkql-quicklogin-modal-review-bt').remove();
			// console.log($( ".new_review_form_content input, .new_review_form_content textarea" ).serialize());
			if (!parent_obj.find('.tdkql-has-error').length)
			{
				parent_obj.find('.tdkql-reset-pass-bt').addClass('validate-ok');
				var tdkql_email_reset = $.trim(parent_obj.find('.tdkql-email-reset').val());
				$.ajax({
					type: 'POST',
					headers: {"cache-control": "no-cache"},
					url: tdkql_ajax_url,
					async: true,
					cache: false,
					data: {
						"ajax": 1,
						"action": "reset-pass",
						"tdkql-email-reset": tdkql_email_reset,
					},
					success: function (result)
					{
						parent_obj.find('.tdkql-form-mesg.has-danger').empty();
						parent_obj.find('.tdkql-form-mesg.has-success').empty();
						var object_result = $.parseJSON(result);
						// console.log(object_result);
						object_e.removeClass('active');					
						object_e.find('.tdkql-loading').hide();
						if (object_result.errors.length)
						{
							$.each(object_result.errors,function(key, val){
								parent_obj.find('.tdkql-form-mesg.has-danger').append('<label class="form-control-label">'+val+'</label>')
							})
							parent_obj.find('.tdkql-form-mesg.has-danger').fadeIn();
							object_e.find('.tdkql-bt-txt').show();
						}
						else
						{
							parent_obj.find('.tdkql-form-content-element').slideUp(function(){
								$(this).remove();
							});
							parent_obj.find('.tdkql-form-mesg.has-success').append('<label class="form-control-label">'+object_result.success[0]+'<strong>'+tdkql_email_reset+'</strong></label>');
							parent_obj.find('.tdkql-form-mesg.has-success').fadeIn();
							object_e.find('.tdkql-success-icon').fadeIn();
							object_e.addClass('success');
						}											
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						console.log("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
					}
				});
			}		
		}
		
	})
	
	$('.tdkql-login-form-content').submit(function(){
		// if ($(this).find('.form-group.tdkql-has-error').length)
		if ($(this).find('.tdkql-login-bt').hasClass('validate-ok') || $(this).find('.tdkql-has-error').length)
		{
			return false;
		}
	});
	
	//TDK:: button login
	$('.tdkql-login-bt').click(function(){
		if (!$(this).hasClass('active') && !$(this).hasClass('success'))
		{		
			var object_e = $(this);
			var parent_obj = object_e.parents('.tdkql-login-form-content');
			parent_obj.find('.tdkql-form-mesg.has-danger').fadeOut();
			parent_obj.find('.tdkql-form-mesg.has-success').fadeOut();
			// var check_submit = true;
			object_e.addClass('active');
			object_e.find('.tdkql-bt-txt').hide();
			object_e.find('.tdkql-loading').css({'display':'block'});
			
			parent_obj.find('input').each(function(){
				// console.log($(this));
				if (!$(this).val())
				{
					$(this).parent('.form-group').addClass('tdkql-has-error');					
				}
				else
				{
					if ($(this).hasClass('tdkql-email-login') && !validateEmail($(this).val()))
					{
						$(this).parent('.form-group').addClass('tdkql-has-error');					
					}
					else
					{
						$(this).parent('.form-group').removeClass('tdkql-has-error');
					}
				}
			});
			
			
			// console.log('pass');
			// $('.tdkql-quicklogin-modal-review-bt').remove();
			// console.log($( ".new_review_form_content input, .new_review_form_content textarea" ).serialize());
			if (!parent_obj.find('.tdkql-has-error').length)
			{
				parent_obj.find('.tdkql-login-bt').addClass('validate-ok');
				var tdkql_email_login = $.trim(parent_obj.find('.tdkql-email-login').val());
				var tdkql_pass_login = $.trim(parent_obj.find('.tdkql-pass-login').val());
				var data_send = {};
				data_send.ajax = 1;
				data_send.action = "customer-login";
				data_send.tdkql_email_login = tdkql_email_login;
				data_send.tdkql_pass_login = tdkql_pass_login;
				if (parent_obj.find('.tdkql-rememberme').length)
				{
					if (parent_obj.find('.tdkql-rememberme').is( ":checked" ))
					{
						var tdkql_remember_login = 1;
					}
					else
					{
						var tdkql_remember_login = 0;
					}
					
					data_send.tdkql_remember_login = tdkql_remember_login;
				}
				// console.log(data_send);
				// return false;
				$.ajax({
					type: 'POST',
					headers: {"cache-control": "no-cache"},
					url: tdkql_ajax_url,
					async: true,
					cache: false,
					data: data_send,
					success: function (result)
					{
						parent_obj.find('.tdkql-form-mesg.has-danger').empty();
						parent_obj.find('.tdkql-form-mesg.has-success').empty();
						var object_result = $.parseJSON(result);
						// console.log(object_result);
						object_e.removeClass('active');					
						object_e.find('.tdkql-loading').hide();
						if (object_result.errors.length)
						{
							$.each(object_result.errors,function(key, val){
								parent_obj.find('.tdkql-form-mesg.has-danger').append('<label class="form-control-label">'+val+'</label>')
							})
							parent_obj.find('.tdkql-form-mesg.has-danger').fadeIn();
							object_e.find('.tdkql-bt-txt').show();
						}
						else
						{
							parent_obj.find('.tdkql-form-content-element').slideUp(function(){
								$(this).remove();
							});
							parent_obj.find('.tdkql-form-mesg.has-success').append('<label class="form-control-label"><strong>'+object_result.success[0]+'</strong></label>');
							parent_obj.find('.tdkql-form-mesg.has-success').fadeIn();
							object_e.find('.tdkql-success-icon').show();
							object_e.addClass('success');
							if (tdkql_redirect)
							{
								window.location.replace(tdkql_myaccount_url);
							}
							else
							{
								location.reload();
							}
						}											
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						console.log("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
					}
				});
				
			}
			else
			{
				object_e.removeClass('active');
				object_e.find('.tdkql-bt-txt').show();
				object_e.find('.tdkql-loading').hide();
				// return false;	
			}
		}
		
	})
	
	$('.tdkql-register-form-content').submit(function(){
		// if ($(this).find('.form-group.tdkql-has-error').length)
		if ($(this).find('.tdkql-register-bt').hasClass('validate-ok') || $(this).find('.tdkql-has-error').length)
		{
			return false;
		}
	});
	
	//TDK:: button login
	$('.tdkql-register-bt').click(function(){
		if (!$(this).hasClass('active') && !$(this).hasClass('success'))
		{		
			var object_e = $(this);
			var parent_obj = object_e.parents('.tdkql-register-form-content');
			parent_obj.find('.tdkql-form-mesg.has-danger').fadeOut();
			parent_obj.find('.tdkql-form-mesg.has-success').fadeOut();
			// var check_submit = true;
			object_e.addClass('active');
			object_e.find('.tdkql-bt-txt').hide();
			object_e.find('.tdkql-loading').css({'display':'block'});
			
			parent_obj.find('input').each(function(){
				// console.log($(this));
				if (!$(this).val())
				{
					$(this).parent('.form-group').addClass('tdkql-has-error');					
				}
				else
				{
					if ($(this).hasClass('tdkql-register-email') && !validateEmail($(this).val()))
					{
						$(this).parent('.form-group').addClass('tdkql-has-error');					
					}
					else
					{
						$(this).parent('.form-group').removeClass('tdkql-has-error');
					}
				}
			});
							
			if (!parent_obj.find('.tdkql-has-error').length)
			{
				parent_obj.find('.tdkql-register-bt').addClass('validate-ok');
				var tdkql_register_firstname = $.trim(parent_obj.find('.tdkql-register-firstname').val());
				var tdkql_register_lastname = $.trim(parent_obj.find('.tdkql-register-lastname').val());
				var tdkql_register_email = $.trim(parent_obj.find('.tdkql-register-email').val());
				var tdkql_register_pass = $.trim(parent_obj.find('.tdkql-register-pass').val());
				$.ajax({
					type: 'POST',
					headers: {"cache-control": "no-cache"},
					url: tdkql_ajax_url,
					async: true,
					cache: false,
					data: {
						"ajax": 1,
						"action": "create-account",
						"tdkql-register-firstname": tdkql_register_firstname,
						"tdkql-register-lastname": tdkql_register_lastname,
						"tdkql-register-email": tdkql_register_email,
						"tdkql-register-pass": tdkql_register_pass,
					},
					success: function (result)
					{
						parent_obj.find('.tdkql-form-mesg.has-danger').empty();
						parent_obj.find('.tdkql-form-mesg.has-success').empty();
						var object_result = $.parseJSON(result);
						// console.log(object_result);
						object_e.removeClass('active');					
						object_e.find('.tdkql-loading').hide();
						if (object_result.errors.length)
						{
							$.each(object_result.errors,function(key, val){
								parent_obj.find('.tdkql-form-mesg.has-danger').append('<label class="form-control-label">'+val+'</label><br/>')
							})
							parent_obj.find('.tdkql-form-mesg.has-danger').fadeIn();
							object_e.find('.tdkql-bt-txt').show();
						}
						else
						{
							parent_obj.find('.tdkql-form-content-element').slideUp(function(){
								$(this).remove();
							});
							parent_obj.find('.tdkql-form-mesg.has-success').append('<label class="form-control-label"><strong>'+object_result.success[0]+'</strong></label>');
							parent_obj.find('.tdkql-form-mesg.has-success').fadeIn();
							object_e.find('.tdkql-success-icon').show();
							object_e.addClass('success');
							if (tdkql_redirect)
							{
								window.location.replace(tdkql_myaccount_url);
							}
							else
							{
								location.reload();
							}
						}											
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						console.log("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
					}
				});
				
			}
			else
			{
				object_e.removeClass('active');
				object_e.find('.tdkql-bt-txt').show();
				object_e.find('.tdkql-loading').hide();
				// return false;	
			}
		}
		
	})
	if (typeof google_client_id != 'undefined' && google_client_id.length)
	{
		// console.log(gapi);
		// console.log(gapi.client.language);
		gapi.load('auth2', function(){
		  // Retrieve the singleton for the GoogleAuth library and set up the client.
		  // console.log(google_client_id);
		  auth2 = gapi.auth2.init({
			client_id: google_client_id,
			cookiepolicy: 'single_host_origin',
			// Request scopes in addition to 'profile' and 'email'
			scope: 'profile email'
		  });
		  // console.log(auth2);
		  attachGoogleSignin();
		});
		// window.___gcfg = {
			// lang: 'zh-CN',
			// parsetags: 'onload'
		  // };
	}
	
	//TDK:: twitter login
	$('.twitter-login-bt').click(function(){
		window.open(tdkql_module_dir+'twitter.php?request=twitter&lang='+prestashop.language.iso_code, '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=100, left=300, width=700, height=600');
	});
});

//TDK:: 
function callLoginForm($element)
{
	var parent_obj = $element.parents('.tdkql-quicklogin-form');
	if ($element.hasClass('tdkql-callreset-action') && !parent_obj.find('.tdkql-forgotpass').hasClass('active'))
	{
		parent_obj.find('.tdkql-forgotpass').trigger('click');
	}
	
	//TDK:: fix case button call register is out of login form
	if ($element.parents('.tdk-quicklogin-wrapper').length) {	
		$element.parents('.tdk-quicklogin-wrapper').find('.tdkql-callregister').show();
	}
	//parent_obj.removeClass('update');
	//parent_obj.width(parent_obj.outerWidth()).height(parent_obj.outerHeight());
	parent_obj.find('.tdkql-login-form').removeClass('tdkql-form-inactive').addClass('tdkql-form-active');
	parent_obj.find('.tdkql-register-form').removeClass('tdkql-form-active').addClass('tdkql-form-inactive');
	
	//TDK:: hide forgot pass form if only call login form
	if (!$element.hasClass('tdkql-callreset-action') && parent_obj.find('.tdkql-forgotpass').hasClass('active'))
	{
		parent_obj.find('.tdkql-forgotpass.active').trigger('click');
	}
	//parent_obj.addClass('update');
	// parent_obj.width(parent_obj.find('.tdkql-form-active').outerWidth()).height(parent_obj.find('.tdkql-form-active').outerHeight());
	// setTimeout(function(){
		
		// parent_obj.css({'width':'100%','height': 'auto'});
	// },300);
	parent_obj.find('.tdkql-bt.tdkql-active').removeClass('tdkql-active');
	parent_obj.find('.tdkql-bt-login').addClass('tdkql-active');
}

//TDK::
function callRegisterForm($element)
{
	//TDK:: fix case button call register is out of login form
	if ($element.parents('.tdk-quicklogin-wrapper').length) {
		var parent_obj = $element.parents('.tdk-quicklogin-wrapper');
		parent_obj.find('.tdkql-callregister').hide();
	} else {
		var parent_obj = $element.parents('.tdkql-quicklogin-form');
	}
	//parent_obj.removeClass('update');
	//parent_obj.width(parent_obj.outerWidth()).height(parent_obj.outerHeight());
	parent_obj.find('.tdkql-register-form').removeClass('tdkql-form-inactive').addClass('tdkql-form-active');
	parent_obj.find('.tdkql-login-form').removeClass('tdkql-form-active').addClass('tdkql-form-inactive');
	//parent_obj.addClass('update');
	// parent_obj.width(parent_obj.find('.tdkql-form-active').outerWidth()).height(parent_obj.find('.tdkql-form-active').outerHeight());
	// setTimeout(function(){
		
		// parent_obj.css({'width':'100%','height': 'auto'});
	// },300);
	parent_obj.find('.tdkql-bt.tdkql-active').removeClass('tdkql-active');
	parent_obj.find('.tdkql-bt-register').addClass('tdkql-active');
}

//TDK:: event for slidebar
function activeEventSlidebarTdkQuickLogin()
{
	$('.tdkql-mask, .tdkql-slidebar-close').click(function(){
		$('.tdkql-mask.active').removeClass('active');
		$('.tdkql-quicklogin.active').removeClass('active');
		$('body.tdkql-active-push main').css({
			"-moz-transform": "translateX(0px)",
			"-webkit-transform": "translateX(0px)",
			"-o-transform": "translateX(0px)",
			"-ms-transform": "translateX(0px)",
			"transform": "translateX(0px)",
			"-moz-transform": "translateY(0px)",
			"-webkit-transform": "translateY(0px)",
			"-o-transform": "translateY(0px)",
			"-ms-transform": "translateY(0px)",
			"transform": "translateY(0px)",
		});
		setTimeout(function(){
			$('body').removeClass('tdkql-active-slidebar tdkql-active-push');
			//TDK:: check auto gen rtl
			if (tdkql_is_gen_rtl && prestashop.language.is_rtl == 1)
			{
				$('body').removeClass('tdkql_is_gen_rtl');
			}
		},300);
			
		$('.tdkql-slidebar.active').removeClass('active');
	});
}

//TDK:: event for modal
function activeEventModalTdkQuickLogin()
{
	$('.tdkql-quicklogin-modal').on('hide.bs.modal', function (e) {
		// console.log('test');
		$('.tdkql-quicklogin.active').removeClass('active');
		
	  // do something...
	});
	
	//TDK:: modal social login
	// $('.tdkql-social-modal').on('hide.bs.modal', function (e) {
		// $('.tdkql-social-modal-mesg').removeClass('active');
	// });
	
}

function validateEmail(email) {
  // var regex = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  // return regex.test(email);
	var reg = /^[a-z\p{L}0-9!#$%&'*+\/=?^`{}|~_-]+[.a-z\p{L}0-9!#$%&'*+\/=?^`{}|~_-]*@[a-z\p{L}0-9]+[._a-z\p{L}0-9-]*\.[a-z\p{L}0-9]+$/i;
	return reg.test(email);
}

//TDK:: facebook login
// function statusChangeCallback(response) {
		
	// if (response.status === 'connected') {
	 
	  // processFbAPI();
	// } else {
	 
	  // console.log('Please log ' +
			// 'into this app.');
	// }
// }
function doFbLogin() {  
	FB.login(function(response) {
		if (response.status === 'connected') {
		  // Logged into your app and Facebook.
		  // console.log(response);
		  processFbAPI();
		} else {
		  // The person is not logged into your app or we are unable to tell.
		  console.log('Please log ' +
				'into this app.');
		}
	} , {scope: 'public_profile,email'}); 
}
// function checkLoginState() {
	// FB.getLoginStatus(function(response) {
	  // statusChangeCallback(response);
	// });
// }

function processFbAPI() {
	$('.tdkql-quicklogin-modal').modal('hide');
	$('.tdkql-mask').trigger('click');
	
	setTimeout(function(){
			
		FB.api('/me?fields=email,birthday,first_name,last_name,name,gender', function(response) {
		
			if (response.email)
			{
				// console.log(response);
				$('.tdkql-social-modal .tdkql-social-modal-mesg').removeClass('active');
				$('.tdkql-social-modal .tdkql-social-modal-mesg.tdkql-social-loading').addClass('active');
				$('.tdkql-social-modal').modal('show');
				// console.log('Successful login for: ' + response.name);
				// console.log('Thanks for logging in, ' + response.name + '!');
				$.ajax({
					type: 'POST',
					headers: {"cache-control": "no-cache"},
					url: tdkql_ajax_url,
					async: true,
					cache: false,
					data: {
						"ajax": 1,
						"action": "social-login",
						"email": response.email,
						"first_name": response.first_name,
						"last_name": response.last_name,
					},
					success: function (result)
					{
						$('.tdkql-social-modal .tdkql-social-modal-mesg').removeClass('active');
						var object_result = $.parseJSON(result);
											
						if (object_result.errors.length)
						{						
							$('.tdkql-social-modal .tdkql-social-modal-mesg.error-login').addClass('active');
						}
						else
						{						
							$('.tdkql-social-modal .tdkql-social-modal-mesg.success').addClass('active');
							if (tdkql_redirect)
							{
								window.location.replace(tdkql_myaccount_url);
							}
							else
							{
								location.reload();
							}
						}											
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						console.log("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
					}
				});
				
			}
			else
			{
				$('.tdkql-social-modal .tdkql-social-modal-mesg').removeClass('active');
				$('.tdkql-social-modal .tdkql-social-modal-mesg.error-email').addClass('active');
				$('.tdkql-social-modal').modal('show');
				// console.log('Fail');
				FB.api('/me/permissions', 'delete', function(response) {
					// console.log(response); 
				});
			}
		});
	
	},300);
}

//TDK:: google login
function attachGoogleSignin() {

	$('.google-login-bt').each(function(){
		auth2.attachClickHandler(this, {},
			function(googleUser) {
				// console.log(googleUser);
				var profile = googleUser.getBasicProfile();
				$('.tdkql-quicklogin-modal').modal('hide');
				$('.tdkql-mask').trigger('click');
				
				setTimeout(function(){  
					if (profile.getEmail())
					{
							// console.log(response);
							$('.tdkql-social-modal .tdkql-social-modal-mesg').removeClass('active');
							$('.tdkql-social-modal .tdkql-social-modal-mesg.tdkql-social-loading').addClass('active');
							$('.tdkql-social-modal').modal('show');
							// console.log('Successful login for: ' + response.name);
							// console.log('Thanks for logging in, ' + response.name + '!');
							$.ajax({
								type: 'POST',
								headers: {"cache-control": "no-cache"},
								url: tdkql_ajax_url,
								async: true,
								cache: false,
								data: {
									"ajax": 1,
									"action": "social-login",
									"email": profile.getEmail(),
									"first_name": profile.getGivenName(),
									"last_name": profile.getFamilyName(),
								},
								success: function (result)
								{
									$('.tdkql-social-modal .tdkql-social-modal-mesg').removeClass('active');
									var object_result = $.parseJSON(result);
														
									if (object_result.errors.length)
									{						
										$('.tdkql-social-modal .tdkql-social-modal-mesg.error-login').addClass('active');
									}
									else
									{						
										$('.tdkql-social-modal .tdkql-social-modal-mesg.success').addClass('active');
										if (tdkql_redirect)
										{
											window.location.replace(tdkql_myaccount_url);
										}
										else
										{
											location.reload();
										}
									}											
								},
								error: function (XMLHttpRequest, textStatus, errorThrown) {
									console.log("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
								}
							});
							
					}
					else
					{
						$('.tdkql-social-modal .tdkql-social-modal-mesg').removeClass('active');
						$('.tdkql-social-modal .tdkql-social-modal-mesg.error-email').addClass('active');
						$('.tdkql-social-modal').modal('show');
						// console.log('Fail');
						auth2.disconnect();
					}
				}, 300);
			}, function(error) {
			  console.log(error);
			}
		
		);
		
	})
    
}

//TDK:: twitter login
function twitterLogin(first_name, last_name, email) {
	$('.tdkql-quicklogin-modal').modal('hide');
	$('.tdkql-mask').trigger('click');
	
	setTimeout(function(){  
		if (email.length)
		{			
			// console.log(response);
			//TDK:: when can't get last name of user
			if (last_name == '')
			{
				last_name = 'Mr/Ms';
			}
			$('.tdkql-social-modal .tdkql-social-modal-mesg').removeClass('active');
			$('.tdkql-social-modal .tdkql-social-modal-mesg.tdkql-social-loading').addClass('active');
			$('.tdkql-social-modal').modal('show');
			// console.log('Successful login for: ' + response.name);
			// console.log('Thanks for logging in, ' + response.name + '!');
			$.ajax({
				type: 'POST',
				headers: {"cache-control": "no-cache"},
				url: tdkql_ajax_url,
				async: true,
				cache: false,
				data: {
					"ajax": 1,
					"action": "social-login",
					"email": email,
					"first_name": first_name,
					"last_name": last_name,
				},
				success: function (result)
				{
					$('.tdkql-social-modal .tdkql-social-modal-mesg').removeClass('active');
					var object_result = $.parseJSON(result);
										
					if (object_result.errors.length)
					{						
						$('.tdkql-social-modal .tdkql-social-modal-mesg.error-login').addClass('active');
					}
					else
					{						
						$('.tdkql-social-modal .tdkql-social-modal-mesg.success').addClass('active');
						if (tdkql_redirect)
						{
							window.location.replace(tdkql_myaccount_url);
						}
						else
						{
							location.reload();
						}
					}											
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					console.log("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
				}
			});
				
		}
		else
		{
			$('.tdkql-social-modal .tdkql-social-modal-mesg').removeClass('active');
			$('.tdkql-social-modal .tdkql-social-modal-mesg.error-email').addClass('active');
			$('.tdkql-social-modal').modal('show');
			// console.log('Fail');
			
		}
	},300);
}


