/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
* 
**/
$(document).ready(function() {
	
	//TDK:: tab change in group config
	var id_panel = $("#tdkquicklogin-setting .tdkquicklogin-tablist li.active a").attr("href");
	$(id_panel).addClass('active').show();
	$('.tdkquicklogin-tablist li').click(function(){
		if(!$(this).hasClass('active'))
		{
			var default_tab = $(this).find('a').attr("href");			
			$('#TDKQUICKLOGIN_DEFAULT_TAB').val(default_tab);
		}
	})
	
});