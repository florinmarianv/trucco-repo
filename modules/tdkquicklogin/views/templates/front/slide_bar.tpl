{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<div class="tdkql-mask"></div>

	<div class="tdkql-slidebar">
		
		<div class="tdkql-slidebar-wrapper">
			<div class="tdkql-slidebar-top">
				<button type="button" class="tdkql-slidebar-close btn btn-secondary">
					<i class="material-icons">&#xE5CD;</i>
					<span>{l s='Close' mod='tdkquicklogin'}</span>
				</button>
			</div>
			{$html_form nofilter}{* HTML form , no escape necessary *}
			
			<div class="tdkql-slidebar-bottom">
				<button type="button" class="tdkql-slidebar-close btn btn-secondary">
					<i class="material-icons">&#xE5CD;</i>
					<span>{l s='Close' mod='tdkquicklogin'}</span>
				</button>
			</div>
		</div>
		
	</div>
