{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{if $fb_enable && $fb_app_id != ''}
	{literal}
	<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '{/literal}{$fb_app_id}{literal}',
				cookie     : true,  // enable cookies to allow the server to access 
									// the session
				xfbml      : true,  // parse social plugins on this page
				version    : 'v3.2', // use graph api version 3.2
				scope: 'email, user_birthday',
			});
		};

		// Load the SDK asynchronously
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/{/literal}{$lang_locale}{literal}/sdk.js";
			{/literal}
			{if $fb_chat_enable}
			{literal}
			js.src = "https://connect.facebook.net/{/literal}{$lang_locale}{literal}/sdk/xfbml.customerchat.js";
			{/literal}
			{/if}
			{literal}
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

	</script>
	{/literal}
{/if}
{if $google_enable && $google_client_id != ''}
<script>
var google_client_id= "{$google_client_id}";

</script>
<script src="https://apis.google.com/js/api:client.js"></script>
{/if}
