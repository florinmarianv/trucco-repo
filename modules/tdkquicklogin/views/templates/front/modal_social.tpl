{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<div class="modal tdkql-social-modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
			<h5 class="modal-title tdkql-social-modal-mesg tdkql-social-loading">
				<span class="tdkql-cssload-speeding-wheel"></span>
			</h5>
			<h5 class="modal-title tdkql-social-modal-mesg error-email">
				<i class="material-icons">&#xE033;</i>
				{l s='Can not login without email!' mod='tdkquicklogin'}
			</h5>
			<h5 class="modal-title tdkql-social-modal-mesg error-email">				
				{l s='Please check your social account and give the permission to use your email info' mod='tdkquicklogin'}
			</h5>
			<h5 class="modal-title tdkql-social-modal-mesg error-login">
				<i class="material-icons">&#xE033;</i>
				{l s='Can not login!' mod='tdkquicklogin'}
			</h5>
			<h5 class="modal-title tdkql-social-modal-mesg error-login">
				{l s='Please contact with us or try to login with another way' mod='tdkquicklogin'}
			</h5>
			<h5 class="modal-title tdkql-social-modal-mesg success">
				<i class="material-icons">&#xE876;</i>
				{l s='Successful!' mod='tdkquicklogin'}
			</h5>
			<h5 class="modal-title tdkql-social-modal-mesg success">			
				{l s='Thanks for logging in' mod='tdkquicklogin'}
			</h5>
		  </div>
		  
		  		 
		</div>
	  </div>
	
</div>