{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<div class="tdkql-quicklogin-form row{if isset($tdk_form_type) && $tdk_form_type != ''} {$tdk_form_type}{/if}">
	{if isset($tdk_navigation_style) && $tdk_navigation_style}
		<div class="tdkql-action{if $tdk_form_layout != 'both'} tdkql-active{else} tdkql-inactive{/if}">
			<div class="tdkql-action-bt">
				<h2 class="tdkql-bt tdkql-bt-login{if $tdk_form_layout == 'login'} tdkql-active{/if}">{l s='Login' mod='tdkquicklogin'}</h2>
			</div>
			<div class="tdkql-action-bt">
				<h2 class="tdkql-bt tdkql-bt-register{if $tdk_form_layout == 'register'} tdkql-active{/if}">{l s='Register' mod='tdkquicklogin'}</h2>
			</div>
		</div>
	{/if}
	<div class="tdkql-form tdkql-login-form col-sm-{if $tdk_form_layout == 'both'}6{else}12{/if}{if $tdk_form_layout == 'login' || $tdk_form_layout == 'both'} tdkql-form-active{else} tdkql-form-inactive{/if}{if $tdk_form_layout != 'both'} full-width{/if}">
		<h3 class="tdkql-login-title">			
			<span class="title-both">
				{l s='Existing Account Login' mod='tdkquicklogin'}
			</span>
		
			<span class="title-only">
				{l s='Glad to see you back' mod='tdkquicklogin'}
			</span>		
		</h3>
		<form class="tdkql-form-content tdkql-login-form-content" action="#" method="post">
			<div class="form-group tdkql-form-mesg has-success">					
			</div>			
			<div class="form-group tdkql-form-mesg has-danger">					
			</div>
			<div class="form-group tdkql-form-content-element">
				<input type="email" class="form-control tdkql-email-login" name="tdkql-email-login" required="" placeholder="{l s='Email Address' mod='tdkquicklogin'}">
			</div>
			<div class="form-group tdkql-form-content-element">
				<input type="password" class="form-control tdkql-pass-login" name="tdkql-pass-login" required="" placeholder="{l s='Password' mod='tdkquicklogin'}">
			</div>
			<div class="form-group text-right">
				<button type="submit" class="form-control-submit tdkql-form-bt tdkql-login-bt btn btn-primary">			
					<span class="tdkql-loading tdkql-cssload-speeding-wheel"></span>
					<i class="tdkql-icon tdkql-success-icon material-icons">&#xE876;</i>
					<i class="tdkql-icon tdkql-fail-icon material-icons">&#xE033;</i>
					<span class="tdkql-bt-txt">					
						{l s='Login' mod='tdkquicklogin'}
					</span>
				</button>
			</div>
			<div class="form-group row tdkql-form-content-element">				
				<div class="col-xs-6">
					{if $tdk_check_cookie}
						<input type="checkbox" class="tdkql-rememberme" name="tdkql-rememberme">
						<label class="form-control-label"><span>{l s='Remember Me' mod='tdkquicklogin'}</span></label>
					{/if}
				</div>				
				<div class="col-xs-6 text-sm-right">
					<a role="button" href="#" class="tdkql-forgotpass">{l s='Lost Your Password?' mod='tdkquicklogin'}?</a>
				</div>
			</div>
		</form>
		<div class="tdkql-resetpass-form">
			<h3>{l s='Reset Password' mod='tdkquicklogin'}</h3>
			<form class="tdkql-form-content tdkql-resetpass-form-content" action="#" method="post">
				<div class="form-group tdkql-form-mesg has-success">					
				</div>			
				<div class="form-group tdkql-form-mesg has-danger">					
				</div>
				<div class="form-group tdkql-form-content-element">
					<input type="email" class="form-control tdkql-email-reset" name="tdkql-email-reset" required="" placeholder="{l s='Email Address' mod='tdkquicklogin'}">
				</div>
				<div class="form-group">					
					<button type="submit" class="form-control-submit tdkql-form-bt tdkql-reset-pass-bt btn btn-primary">			
						<span class="tdkql-loading tdkql-cssload-speeding-wheel"></span>
						<i class="tdkql-icon tdkql-success-icon material-icons">&#xE876;</i>
						<i class="tdkql-icon tdkql-fail-icon material-icons">&#xE033;</i>
						<span class="tdkql-bt-txt">					
							{l s='Reset Password' mod='tdkquicklogin'}
						</span>
					</button>
				</div>
				
			</form>
		</div>
	</div>
	
	<div class="tdkql-form tdkql-register-form col-sm-{if $tdk_form_layout == 'both'}6{else}12{/if}{if $tdk_form_layout == 'register' || $tdk_form_layout == 'both'} tdkql-form-active{else} tdkql-form-inactive{/if}{if $tdk_form_layout != 'both'} full-width{/if}">
		<h3 class="tdkql-register-title">
			{l s='New Account Register' mod='tdkquicklogin'}
		</h3>
		<form class="tdkql-form-content tdkql-register-form-content" action="#" method="post">
			<div class="form-group tdkql-form-mesg has-success">					
			</div>			
			<div class="form-group tdkql-form-mesg has-danger">					
			</div>
			<div class="form-group tdkql-form-content-element">
				<input type="text" class="form-control tdkql-register-firstname" name="tdkql-register-firstname" required="" placeholder="{l s='First Name' mod='tdkquicklogin'}">
			</div>
			<div class="form-group tdkql-form-content-element">
				<input type="text" class="form-control tdkql-register-lastname" name="tdkql-register-lastname" required="" placeholder="{l s='Last Name' mod='tdkquicklogin'}">
			</div>
			<div class="form-group tdkql-form-content-element">
				<input type="email" class="form-control tdkql-register-email" name="tdkql-register-email" required="" placeholder="{l s='Email Address' mod='tdkquicklogin'}">
			</div>
			<div class="form-group tdkql-form-content-element">
				<input type="password" class="form-control tdkql-register-pass" name="tdkql-register-pass" required="" placeholder="{l s='Password' mod='tdkquicklogin'}">
			</div>
			<div class="form-group text-right">				
				<button type="submit" class="form-control-submit tdkql-form-bt tdkql-register-bt btn btn-primary">			
					<span class="tdkql-loading tdkql-cssload-speeding-wheel"></span>
					<i class="tdkql-icon tdkql-success-icon material-icons">&#xE876;</i>
					<i class="tdkql-icon tdkql-fail-icon material-icons">&#xE033;</i>
					<span class="tdkql-bt-txt">					
						{l s='Create an Account' mod='tdkquicklogin'}
					</span>
				</button>
			</div>
			<div class="form-group tdkql-calllogin">
				<div>{l s='Already have an account?' mod='tdkquicklogin'}</div>
				<a role="button" href="#" class="tdkql-calllogin-action">{l s='Log in instead' mod='tdkquicklogin'}</a>
				{l s='Or' mod='tdkquicklogin'}
				<a role="button" href="#" class="tdkql-calllogin-action tdkql-callreset-action">{l s='Reset password' mod='tdkquicklogin'}</a>
			</div>
		</form>
	</div>
</div>