{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<div class="tdkql-social-login clearfix {if $show_button_text}show-bt-txt{/if}">
	<p class="tdkql-social-login-title">
		<span>{l s='or' mod='tdkquicklogin'}</span>
	</p>
	{if $fb_enable && $fb_app_id != ''}
		<!--
		<div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false" scope="public_profile,email" onlogin="checkLoginState();"></div>
		-->
		<button class="btn social-login-bt facebook-login-bt" onclick="doFbLogin();"><span class="fa fa-facebook"></span>{if $show_button_text}{l s='Sign in with Facebook' mod='tdkquicklogin'}{/if}</button>
		
	{/if}
	<!--
		<div class="g-signin2" data-scope="profile email" data-longtitle="true" data-theme="dark" data-onsuccess="googleSignIn" data-onfailure="googleFail"></div>
	-->
	{if $google_enable && $google_client_id != ''}
		<button class="btn social-login-bt google-login-bt"><span class="fa fa-google"></span>{if $show_button_text}{l s='Sign in with Google' mod='tdkquicklogin'}{/if}</button>
	{/if}
	{if $twitter_enable && $twitter_api_key != '' && $twitter_api_secret !== ''}
		<button class="btn social-login-bt twitter-login-bt"><span class="fa fa-twitter"></span>{if $show_button_text}{l s='Sign in with Twitter' mod='tdkquicklogin'}{/if}</button>
	{/if}
</div>
{if isset($login_page) && $login_page}
<hr>
{/if}

