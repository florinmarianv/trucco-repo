{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<div class="modal tdkql-quicklogin-modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		  	<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
		  	</div>		  
		  	<div class="modal-body">
				<div class="tdk-quicklogin-wrapper">
					{$html_form nofilter}{* HTML form , no escape necessary *}
					<div class="form-group tdkql-callregister{if isset($tdk_navigation_style) && $tdk_navigation_style} tdkql-inactive{else} tdkql-active{/if}">
						<span>{l s='New here?' mod='tdkquicklogin'}</span>
						<a role="button" href="#" class="tdkql-callregister-action">{l s='Join us now' mod='tdkquicklogin'}</a>
					</div>
				</div>
		  	</div> 
		  	<div class="modal-footer"></div>
		</div>
	</div>	
</div>