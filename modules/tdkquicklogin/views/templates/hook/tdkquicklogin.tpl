{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{if $isLogged}
	<a class="logout" href="{$logout_url}" rel="nofollow">
        <i class="material-icons">&#xE879;</i>      
		<span class="hidden-sm-down">{l s='Sign out' mod='tdkquicklogin'}</span>
	</a>
	<a class="account" href="{$my_account_url}" title="{l s='View My Account' mod='tdkquicklogin'}" rel="nofollow">
        <i class="material-icons">&#xE7FD;</i>
        <span class="hidden-sm-down">{$customerName}</span>
	</a>
{else}
	{if $tdk_type == 'html'}
		{$html_form nofilter}{* HTML form , no escape necessary *}
	{else}
		{if $tdk_type == 'dropdown'}
			<div class="dropdown">
		{/if}
		{if $tdk_type == 'dropup'}
			<div class="dropup">
		{/if}
				<a href="javascript:void(0)" class="tdkql-quicklogin{if $tdk_type == 'dropdown' || $tdk_type == 'dropup'} tdkql-dropdown dropdown-toggle{/if}" data-enable-sociallogin="{if isset($enable_sociallogin)}{$enable_sociallogin}{/if}" data-type="{$tdk_type}" data-layout="{$tdk_layout}"{if $tdk_type == 'dropdown' || $tdk_type == 'dropup'} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"{/if} title="{l s='Quick Login' mod='tdkquicklogin'}" rel="nofollow">
					<i class="material-icons">&#xE851;</i>
					<span class="hidden-sm-down">{l s='Quick Login' mod='tdkquicklogin'}</span>
				</a>
		{if $tdk_type == 'dropdown' || $tdk_type == 'dropup'}
				<div class="dropdown-menu tdkql-dropdown-wrapper">
					{$html_form nofilter}{* HTML form , no escape necessary *}
				</div>
			</div>
		{/if}
	{/if}
{/if}

