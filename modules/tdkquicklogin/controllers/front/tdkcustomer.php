<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

use PrestaShop\PrestaShop\Core\Crypto\Hashing;

//use PrestaShop\PrestaShop\Core\Crypto\Hashing as Crypto;

class TdkQuickloginTdkCustomerModuleFrontController extends ModuleFrontController
{

    public $php_self;

    public function displayAjax()
    {
        // Add or remove product with Ajax
        $context = Context::getContext();
        $action = Tools::getValue('action');

        $array_result = array();
        $errors = array();
        $success = array();
        // Instance of module class for translations
        //TDK:: reset password
        if ($action == 'reset-pass') {
            //TDK:: check validate
            if (!($email = trim(Tools::getValue('tdkql-email-reset'))) || !Validate::isEmail($email)) {
                $errors[] = $this->l('Invalid email address', 'tdkcustomer');
            } else {
                //TDK:: check email exist
                $customer = new Customer();
                $customer->getByEmail($email);
                if (is_null($customer->email)) {
                    $customer->email = $email;
                }

                if (!Validate::isLoadedObject($customer)) {
                    $errors[] = $this->l('This email is not registered as an account', 'tdkcustomer');
                } elseif (!$customer->active) {
                    $errors[] = $this->l('You cannot regenerate the password for this account.', 'tdkcustomer');
                } elseif ((strtotime($customer->last_passwd_gen . '+' . ($minTime = (int) Configuration::get('PS_PASSWD_TIME_FRONT')) . ' minutes') - time()) > 0) {
                    $errors[] = $this->l('You can regenerate your password only every ', 'tdkcustomer') . (int) $minTime . $this->l(' minute(s)', 'tdkcustomer');
                } else {
                    if (!$customer->hasRecentResetPasswordToken()) {
                        $customer->stampResetPasswordToken();
                        $customer->update();
                    }

                    //TDK:: send mail to reset password
                    $mailParams = array(
                        '{email}' => $customer->email,
                        '{lastname}' => $customer->lastname,
                        '{firstname}' => $customer->firstname,
                        '{url}' => $this->context->link->getPageLink('password', true, null, 'token=' . $customer->secure_key . '&id_customer=' . (int) $customer->id . '&reset_token=' . $customer->reset_password_token),
                    );

                    if (Mail::Send($this->context->language->id, 'password_query', $this->l('Password query confirmation', 'tdkcustomer'), $mailParams, $customer->email, $customer->firstname . ' ' . $customer->lastname)) {
                        $success[] = $this->l('If this email address has been registered in our shop, you will receive a link to reset your password at ', 'tdkcustomer');
                    } else {
                        $errors[] = $this->l('An error occurred while sending the email.', 'tdkcustomer');
                    }
                }
            }
        }

        //TDK:: customer login
        if ($action == 'customer-login') {
            //TDK:: check validate
            if (!($email = trim(Tools::getValue('tdkql_email_login'))) || !Validate::isEmail($email)) {
                $errors[] = $this->l('Invalid email address', 'tdkcustomer');
            }
            if (!($pass = trim(Tools::getValue('tdkql_pass_login'))) || !Validate::isPasswd($pass)) {
                $errors[] = $this->l('Invalid password', 'tdkcustomer');
            }
            if (!Tools::getValue('tdkql_remember_login') && Configuration::get('TDKQUICKLOGIN_ENABLE_CHECKCOOKIE') && (int) Configuration::get('TDKQUICKLOGIN_LIFETIME_COOKIE') != 0) {
                $this->context->cookie->customer_last_activity = time();
            }
            if (!count($errors)) {
                Hook::exec('actionAuthenticationBefore');

                //TDK:: check email exist
                $customer = new Customer();
                $authentication = $customer->getByEmail($email, $pass);

                if (isset($authentication->active) && !$authentication->active) {
                    $errors[] = $this->l('Your account isn\'t available at this time, please contact us', 'tdkcustomer');
                } elseif (!$authentication || !$customer->id || $customer->is_guest) {
                    $errors[] = $this->l('Authentication failed.', 'tdkcustomer');
                } else {
                    //TDK:: update cookie to login
                    $this->context->updateCustomer($customer);

                    Hook::exec('actionAuthentication', array('customer' => $this->context->customer));

                    // Login information have changed, so we check if the cart rules still apply
                    CartRule::autoRemoveFromCart($this->context);
                    CartRule::autoAddToCart($this->context);
                    $success[] = $this->l('You have successfully logged in', 'tdkcustomer');
                }
            }
        }

        //TDK:: create new account
        if ($action == 'create-account') {
            //TDK:: check validate
            if (!($email = trim(Tools::getValue('tdkql-register-email'))) || !Validate::isEmail($email)) {
                $errors[] = $this->l('Invalid email address', 'tdkcustomer');
            }
            if (!($pass = trim(Tools::getValue('tdkql-register-pass'))) || !Validate::isPasswd($pass)) {
                $errors[] = $this->l('Invalid password', 'tdkcustomer');
            }

            if (!($firstname = trim(Tools::getValue('tdkql-register-firstname'))) || !Validate::isName($firstname)) {
                $errors[] = $this->l('Invalid first name', 'tdkcustomer');
            }
            if (!($lastname = trim(Tools::getValue('tdkql-register-lastname'))) || !Validate::isName($lastname)) {
                $errors[] = $this->l('Invalid last name', 'tdkcustomer');
            }

            if (!count($errors)) {
                //TDK:: check email exist to register
                if (Customer::customerExists($email, true, true)) {
                    $errors[] = $this->l('This email is already used, please choose another one or sign in', 'tdkcustomer');
                } else {
                    $hookResult = array_reduce(
                        Hook::exec('actionSubmitAccountBefore', array(), null, true),
                        function ($carry, $item) {
                            return $carry && $item;
                        },
                        true
                    );
                    //TDK:: add new account
                    $customer = new Customer();
                    $customer->firstname = $firstname;
                    $customer->lastname = $lastname;
                    $customer->email = $email;
                    $customer->passwd = $this->get('hashing')->hash($pass, _COOKIE_KEY_);

                    if ($hookResult && $customer->save()) {
                        $this->context->updateCustomer($customer);
                        $this->context->cart->update();
                        //TDK:: send mail new account
                        if (!$customer->is_guest && Configuration::get('PS_CUSTOMER_CREATION_EMAIL')) {
                            Mail::Send($this->context->language->id, 'account', $this->l('Welcome!', 'tdkcustomer'), array(
                                '{firstname}' => $customer->firstname,
                                '{lastname}' => $customer->lastname,
                                '{email}' => $customer->email), $customer->email, $customer->firstname . ' ' . $customer->lastname);
                        }

                        Hook::exec('actionCustomerAccountAdd', array(
                            'newCustomer' => $customer,
                        ));
                        $success[] = $this->l('You have successfully created a new account', 'tdkcustomer');
                    } else {
                        $errors[] = $this->l('An error occurred while creating the new account.', 'tdkcustomer');
                    }
                }
            }
        }

        //TDK:: create new account
        if ($action == 'social-login') {
            if (!($email = trim(Tools::getValue('email'))) || !Validate::isEmail($email)) {
                $errors[] = $this->l('Invalid email address', 'tdkcustomer');
            }

            if (!($firstname = trim(Tools::getValue('first_name'))) || !Validate::isName($firstname)) {
                $errors[] = $this->l('Invalid first name', 'tdkcustomer');
            }
            if (!($lastname = trim(Tools::getValue('last_name'))) || !Validate::isName($lastname)) {
                $errors[] = $this->l('Invalid last name', 'tdkcustomer');
            }

            if (!count($errors)) {
                if (Customer::customerExists($email, true, true)) {
                    Hook::exec('actionAuthenticationBefore');

                    //TDK:: check email exist
                    $customer = new Customer();
                    $authentication = $customer->getByEmail(Tools::getValue('email'), null, true);

                    if (isset($authentication->active) && !$authentication->active) {
                        $errors[] = $this->l('Your account isn\'t available at this time, please contact us', 'tdkcustomer');
                    } elseif (!$authentication || !$customer->id || $customer->is_guest) {
                        $errors[] = $this->l('Authentication failed.', 'tdkcustomer');
                    } else {
                        //TDK:: update cookie to login
                        $this->context->updateCustomer($customer);

                        Hook::exec('actionAuthentication', array('customer' => $this->context->customer));

                        // Login information have changed, so we check if the cart rules still apply
                        CartRule::autoRemoveFromCart($this->context);
                        CartRule::autoAddToCart($this->context);
                        $success[] = $this->l('You have successfully logged in', 'tdkcustomer');
                    }
                } else {
                    $hookResult = array_reduce(
                        Hook::exec('actionSubmitAccountBefore', array(), null, true),
                        function ($carry, $item) {
                            return $carry && $item;
                        },
                        true
                    );
                    //TDK:: add new account
                    $customer = new Customer();
                    $customer->firstname = $firstname;
                    $customer->lastname = $lastname;
                    $customer->email = $email;
                    $password = Tools::passwdGen();
                    $customer->passwd = $this->get('hashing')->hash($password, _COOKIE_KEY_);

                    if ($hookResult && $customer->save()) {
                        $this->context->updateCustomer($customer);
                        $this->context->cart->update();
                        //TDK:: send mail new account
                        if (!$customer->is_guest && Configuration::get('PS_CUSTOMER_CREATION_EMAIL')) {
                            Mail::Send($this->context->language->id, 'account_social', $this->l('Welcome!', 'tdkcustomer'), array(
                                '{firstname}' => $customer->firstname,
                                '{lastname}' => $customer->lastname,
                                '{email}' => $customer->email,
                                '{password}' => $password), $customer->email, $customer->firstname . ' ' . $customer->lastname, null, null, null, null, _PS_MODULE_DIR_ . $this->module->name . '/mails/');
                        }

                        Hook::exec('actionCustomerAccountAdd', array('newCustomer' => $customer));
                        $success[] = $this->l('You have successfully created a new account', 'tdkcustomer');
                    } else {
                        $errors[] = $this->l('An error occurred while creating the new account.', 'tdkcustomer');
                    }
                }
            }
        }

        $array_result['success'] = $success;
        $array_result['errors'] = $errors;
        die(Tools::jsonEncode($array_result));
    }

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        $this->php_self = 'tdkcustomer';

        if (Tools::getValue('ajax')) {
            return;
        }
        parent::initContent();
    }
}
