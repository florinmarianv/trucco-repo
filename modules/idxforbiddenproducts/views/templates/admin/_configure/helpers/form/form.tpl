{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2018 Innova Deluxe SL
* @license   INNOVADELUXE
*}

{extends file="helpers/form/form.tpl"}
{block name="field"}
	{if $input.type == 'priorities'}
            <div class="input-group col-lg-9">
                {foreach key=key item=item from=$input.order name='parent'}
                    <select name="{$input.name}[]">
                        {foreach key=subkey item=subitem from=$input.order}
                        <option value="{$subitem.val}" {if $key == $subkey}selected="selected"{/if}>{$subitem.name}</option>
                        {/foreach}
                    </select>
                    {if not $smarty.foreach.parent.last}
                    <span class="input-group-addon"><i class="icon-chevron-right"></i></span>
                    {/if}
                {/foreach}
            </div>
            {if isset($input.desc) && !empty($input.desc)}
                <div class="col-lg-9 col-lg-offset-3">
                    <p class="help-block">
                        {$input.desc}
                    </p>
                </div>
            {/if}            
        {else}
            {$smarty.block.parent}
	{/if}
{/block}
