{**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2017 Innova Deluxe SL
* @license   INNOVADELUXE
*}

<div class="bootstrap">
      <div class="module_error alert alert-warning" >
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {$msg}
      </div>
</div>