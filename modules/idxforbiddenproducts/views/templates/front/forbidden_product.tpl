{**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2015 Innova Deluxe SL

* @license   INNOVADELUXE
*}

{extends file='page.tpl'}
{block name='page_content'}
    
{capture name=path}{l s='Permission error' mod='idxforbiddenproducts'}{/capture}

<div class="col-md-10">
	<div>
		{$custom_message|escape:'html':'UTF-8' nofilter}
	</div>
</div>

{/block}