<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * @author    Innova Deluxe SL
 * @copyright 2015 Innova Deluxe SL

 * @license   INNOVADELUXE
 */

$sql = array();

$sqlCreate = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'idxforbiddenproducts (
      id_private_products int(11) unsigned NOT NULL auto_increment,
      is_group int(1) unsigned NOT NULL,
      id_user_group int(11) unsigned NOT NULL,
      id_product int (11) unsigned NOT NULL,
      allow_rule int(1) unsigned NOT NULL,
      PRIMARY KEY (id_private_products)
    ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';
Db::getInstance()->execute($sqlCreate);
if (count(Db::getInstance()->executeS('SHOW TABLES LIKE "' . _DB_PREFIX_ . 'deluxeforbiddenproducts"'))>0) {
    $sql1 = "SELECT
    column_name,
    column_type    # or data_type 
FROM information_schema.columns 
WHERE table_name='". _DB_PREFIX_ ."deluxeforbiddenproducts'; ";
    $sql2 = "SELECT
    column_name,
    column_type    # or data_type 
FROM information_schema.columns 
WHERE table_name='". _DB_PREFIX_ ."idxforbiddenproducts'; ";
    $camposDeluxe = Db::getInstance()->executeS($sql1);
    $camposIdx = Db::getInstance()->executeS($sql2);
    if ($camposDeluxe == $camposIdx) {
        $sql[] = 'INSERT INTO `' . _DB_PREFIX_ . 'idxforbiddenproducts` (SELECT * FROM `' . _DB_PREFIX_ . 'deluxeforbiddenproducts`) ';
    } else {
        //obtenemos todos las filas, recorremos el array y añadimos los valores que falten
        $sqlDeluxe = 'SELECT * FROM `' . _DB_PREFIX_ . 'deluxeforbiddenproducts`';
        if ($result = Db::getInstance()->executeS($sqlDeluxe)) {
            foreach ($result as $fila) {
                if (!isset($fila['allow_rule'])) {
                    $fila['allow_rule'] = 1;
                }
                if (!isset($fila['is_group'])) {
                    $fila['is_group'] = 1;
                }
                unset($fila['id_private_products']);
                Db::getInstance()->insert('idxforbiddenproducts', $fila);
            }
        }
    }
}

$sqlCreate = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'idxforbiddenproducts_categories` (
      id_private_category int(10) unsigned NOT NULL auto_increment,
      id_group int(10) unsigned NOT NULL,
      id_category int(10) unsigned NOT NULL,
      is_group int(1) unsigned NOT NULL,
      allow_rule int(1) unsigned NOT NULL,
      PRIMARY KEY (`id_private_category`)
    ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';
Db::getInstance()->execute($sqlCreate);
if (count(Db::getInstance()->executeS('SHOW TABLES LIKE "' . _DB_PREFIX_ . 'deluxeforbiddenproducts_categories"'))>0) {
    $sql1 = "SELECT
    column_name,
    column_type    # or data_type 
FROM information_schema.columns 
WHERE table_name='". _DB_PREFIX_ ."deluxeforbiddenproducts_categories'; ";
    $sql2 = "SELECT
    column_name,
    column_type    # or data_type 
FROM information_schema.columns 
WHERE table_name='". _DB_PREFIX_ ."idxforbiddenproducts_categories'; ";
    $camposDeluxe = Db::getInstance()->executeS($sql1);
    $camposIdx = Db::getInstance()->executeS($sql2);
    if ($camposDeluxe == $camposIdx) {
        $sql[] = 'INSERT INTO `' . _DB_PREFIX_ . 'idxforbiddenproducts_categories` (SELECT * FROM `' . _DB_PREFIX_ . 'deluxeforbiddenproducts_categories`) ';
    } else {
        //obtenemos todos las filas, recorremos el array y añadimos los valores que falten
        $sqlDeluxe = 'SELECT * FROM `' . _DB_PREFIX_ . 'deluxeforbiddenproducts_categories`';
        if ($result = Db::getInstance()->executeS($sqlDeluxe)) {
            foreach ($result as $fila) {
                if (!isset($fila['allow_rule'])) {
                    $fila['allow_rule'] = 1;
                }
                if (!isset($fila['is_group'])) {
                    $fila['is_group'] = 1;
                }
                unset($fila['id_private_category']);
                Db::getInstance()->insert('idxforbiddenproducts_categories', $fila);
            }
        }
    }
}

$sqlCreate = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ .'idxforbiddenproducts_manufacturer` (
      id_private_manufacturer int(10) unsigned NOT NULL auto_increment,
      id_group int(10) unsigned NOT NULL,
      id_manufacturer int(10) unsigned NOT NULL,
      is_group int(1) unsigned NOT NULL,
      allow_rule int(1) unsigned NOT NULL,
      PRIMARY KEY (`id_private_manufacturer`)
    ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';
Db::getInstance()->execute($sqlCreate);
if (count(Db::getInstance()->executeS('SHOW TABLES LIKE "' . _DB_PREFIX_ . 'deluxeforbiddenproducts_manufacturer"'))>0) {
    $sql1 = "SELECT
    column_name,
    column_type    # or data_type 
FROM information_schema.columns 
WHERE table_name='". _DB_PREFIX_ ."deluxeforbiddenproducts_manufacturer'; ";
    $sql2 = "SELECT
    column_name,
    column_type    # or data_type 
FROM information_schema.columns 
WHERE table_name='". _DB_PREFIX_ ."idxforbiddenproducts_manufacturer'; ";
    $camposDeluxe = Db::getInstance()->executeS($sql1);
    $camposIdx = Db::getInstance()->executeS($sql2);
    if ($camposDeluxe == $camposIdx) {
        $sql[] = 'INSERT INTO `' . _DB_PREFIX_ . 'idxforbiddenproducts_manufacturer` (SELECT * FROM `' . _DB_PREFIX_ . 'deluxeforbiddenproducts_manufacturer`) ';
    } else {
        //obtenemos todos las filas, recorremos el array y añadimos los valores que falten
        $sqlDeluxe = 'SELECT * FROM `' . _DB_PREFIX_ . 'deluxeforbiddenproducts_manufacturer`';
        if ($result = Db::getInstance()->executeS($sqlDeluxe)) {
            foreach ($result as $fila) {
                if (!isset($fila['allow_rule'])) {
                    $fila['allow_rule'] = 1;
                }
                if (!isset($fila['is_group'])) {
                    $fila['is_group'] = 1;
                }
                unset($fila['id_private_manufacturer']);
                Db::getInstance()->insert('idxforbiddenproducts_manufacturer', $fila);
            }
        }
    }
}

$sqlCreate = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ .'idxforbiddenproducts_supplier` (
      id_private_supplier int(10) unsigned NOT NULL auto_increment,
      id_group int(10) unsigned NOT NULL,
      id_supplier int(10) unsigned NOT NULL,
      is_group int(1) unsigned NOT NULL,
      allow_rule int(1) unsigned NOT NULL,
      PRIMARY KEY (`id_private_supplier`)
    ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';
Db::getInstance()->execute($sqlCreate);
if (count(Db::getInstance()->executeS('SHOW TABLES LIKE "' . _DB_PREFIX_ . 'deluxeforbiddenproducts_supplier"'))>0) {
     $sql1 = "SELECT
    column_name,
    column_type    # or data_type 
FROM information_schema.columns 
WHERE table_name='". _DB_PREFIX_ ."deluxeforbiddenproducts_supplier'; ";
    $sql2 = "SELECT
    column_name,
    column_type    # or data_type 
FROM information_schema.columns 
WHERE table_name='". _DB_PREFIX_ ."idxforbiddenproducts_supplier'; ";
    $camposDeluxe = Db::getInstance()->executeS($sql1);
    $camposIdx = Db::getInstance()->executeS($sql2);
    if ($camposDeluxe == $camposIdx) {
        $sql[] = 'INSERT INTO `' . _DB_PREFIX_ . 'idxforbiddenproducts_supplier` (SELECT * FROM `' . _DB_PREFIX_ . 'deluxeforbiddenproducts_supplier`) ';
    } else {
        //obtenemos todos las filas, recorremos el array y añadimos los valores que falten
        $sqlDeluxe = 'SELECT * FROM `' . _DB_PREFIX_ . 'deluxeforbiddenproducts_supplier`';
        if ($result = Db::getInstance()->executeS($sqlDeluxe)) {
            foreach ($result as $fila) {
                if (!isset($fila['allow_rule'])) {
                    $fila['allow_rule'] = 1;
                }
                if (!isset($fila['is_group'])) {
                    $fila['is_group'] = 1;
                }
                 unset($fila['id_private_supplier']);
                Db::getInstance()->insert('idxforbiddenproducts_supplier', $fila);
            }
        }
    }
}

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
