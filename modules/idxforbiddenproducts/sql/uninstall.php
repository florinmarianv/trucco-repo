<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * @author    Innova Deluxe SL
 * @copyright 2015 Innova Deluxe SL

 * @license   INNOVADELUXE
 */

$sql = array();
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'idxforbiddenproducts`';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'idxforbiddenproducts_categories`';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'idxforbiddenproducts_manufacturer`';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'idxforbiddenproducts_supplier`';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
