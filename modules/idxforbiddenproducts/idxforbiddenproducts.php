<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * @author    Innova Deluxe SL
 * @copyright 2015 Innova Deluxe SL

 * @license   INNOVADELUXE
 */

if (!class_exists('InnovaTools_2_0_0')) {
    require_once(_PS_ROOT_DIR_ . '/modules/idxforbiddenproducts/libraries/innovatools_2_0_0.php');
}

class Idxforbiddenproducts extends Module
{

    public function __construct()
    {
        $this->name = 'idxforbiddenproducts';
        $this->tab = 'front_office_features';
        $this->version = '1.8.1';
        $this->author_address = '0x899FC2b81CbbB0326d695248838e80102D2B4c53';
        $this->innovatabs = "";
        $this->doclink = $this->name . "/doc/readme_en.pdf";
        $this->author = 'innovaDeluxe';
        $this->module_key = 'f4ad7a6c7721db58419afdc685d1b435';
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->module_tab = false;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Deluxe Forbidden Products');
        $this->description = $this->l('This module allows the  products access, to the customers or customer groups you want.');
    }

    public function install()
    {
        $lang_examples = Language::getLanguages(false);
        $custom_content_example = array();

        foreach ($lang_examples as $lang_example) {
            $custom_content_example[$lang_example['id_lang']] = Tools::htmlentitiesUTF8('
        <' . 'p' . '>' . '<' . 'strong' . '>' . $this->l("You can set custom content into forbidden product access, For example:") . '<' . '/' . 'strong' . '>' . '<' . '/' . 'p' . '>' . '
        <' . 'p' . '>' . '<' . 'strong' . '>' . $this->l("This product is forbidden, you have not access to view it") . '<' . '/' . 'strong' . '>' . '<' . '/' . 'p' . '>');
        }

        Configuration::updateValue(Tools::strtoupper($this->name) . '_CUSTOM', $custom_content_example);
        $this->setDefaultPriorities();

        include(dirname(__FILE__) . '/sql/install.php');

        return parent::install() &&
            $this->registerHook('displayHeader');
    }

    public function setDefaultPriorities()
    {
        $priorities = Tools::jsonEncode(array('product', 'category', 'manufacturer', 'supplier'));
        Configuration::updateValue(Tools::strtoupper($this->name) . '_PRIORITIES', $priorities);

        $customer_priorities = Tools::jsonEncode(array('customer', 'group'));
        Configuration::updateValue(Tools::strtoupper($this->name) . '_PRIORITIESC', $customer_priorities);
    }

    public function uninstall()
    {
        Configuration::deleteByName(Tools::strtoupper($this->name) . '_CUSTOM');
        Configuration::deleteByName(Tools::strtoupper($this->name) . '_PRIORITIES');
        Configuration::deleteByName(Tools::strtoupper($this->name) . '_PRIORITIESC');

        include(dirname(__FILE__) . '/sql/uninstall.php');

        return parent::uninstall();
    }

    public function hookDisplayHeader($params)
    {
        if ($this->active) {
            $this->context->controller->addCSS($this->_path . 'css/' . $this->name . '.css');
        }
    }

    public function getContent()
    {
        $output = $this->innovaTitle();
        $output .= $this->postProcess() . $this->renderForm();
        return $output;
    }

    public function postProcess()
    {
        $this->checktables();
        if (Tools::isSubmit('submitSettings')) {
            if (Tools::isSubmit('idxcustomer')) {
                // customer_category
                $is_group = 0;
                $id_product = Tools::getValue('idxforbiddenproducts_prod');
                $id_user = Tools::getValue('idxcustomer');
                $allow_type = Tools::getValue('idxforbiddenproducts_type');
                if ($this->makeupdate($is_group, $id_user, $id_product, $allow_type)) {
                    return $this->displayConfirmation($this->l('The settings have been updated.'));
                }
            }
        }

        if (Tools::isSubmit('submitPriorities')) {
            $product_priorities = Tools::jsonEncode(Tools::getValue('product_priorities'));
            Configuration::updateValue(Tools::strtoupper($this->name) . '_PRIORITIES', $product_priorities);
            $customer_priorities = Tools::jsonEncode(Tools::getValue('customer_priorities'));
            Configuration::updateValue(Tools::strtoupper($this->name) . '_PRIORITIESC', $customer_priorities);
        }

        if (Tools::isSubmit('submitGroup')) {
            $is_group = 1;
            $id_product = Tools::getValue('group_product');
            $id_group = Tools::getValue('deluxegroup');
            $allow_type = Tools::getValue('group_type');

            if ($this->makeupdate($is_group, $id_group, $id_product, $allow_type)) {
                return $this->displayConfirmation($this->l('The settings have been updated.'));
            }
        }
        
        if (Tools::isSubmit('submitByCategory')) {
            $id_customer = Tools::getValue('allowed_category_by_customer');
            $id_category = Tools::getValue('categories_allowed');
            $allow_type = Tools::getValue('category_type');
            if ($this->insertCategoryRule(0, $id_customer, $id_category, $allow_type)) {
                return $this->displayConfirmation($this->l('The association have been inserted.'));
            } else {
                return $this->displayError($this->l('A problem ocurred while inserting group/category'));
            }
        }

        if (Tools::isSubmit('submitByCategoryGroup')) {
            $id_customer_group = Tools::getValue('allowed_category_by_group');
            $id_category = Tools::getValue('categoriesg_allowed');
            $allow_type = Tools::getValue('categoryg_type');
            if ($this->insertCategoryRule(1, $id_customer_group, $id_category, $allow_type)) {
                return $this->displayConfirmation($this->l('The association have been inserted.'));
            } else {
                return $this->displayError($this->l('A problem ocurred while inserting group/category'));
            }
        }

        if (Tools::isSubmit('submitByManufacturer')) {
            $id_customer = Tools::getValue('allowed_manuf_by_customer');
            $id_manufacturer = Tools::getValue('allowed_manufacturer');
            $allow_type = Tools::getValue('manufacturer_type');
            if ($this->insertManufacturerRule(0, $id_customer, $id_manufacturer, $allow_type)) {
                return $this->displayConfirmation($this->l('The association have been inserted.'));
            } else {
                return $this->displayError($this->l('A problem ocurred while inserting group/manufacturer'));
            }
        }

        if (Tools::isSubmit('submitByManufacturerGroup')) {
            $id_customer_group = Tools::getValue('allowed_manuf_by_customer');
            $id_manufacturer = Tools::getValue('allowed_manufacturer');
            $allow_type = Tools::getValue('manufacturerg_type');
            if ($this->insertManufacturerRule(1, $id_customer_group, $id_manufacturer, $allow_type)) {
                return $this->displayConfirmation($this->l('The association have been inserted.'));
            } else {
                return $this->displayError($this->l('A problem ocurred while inserting group/manufacturer'));
            }
        }

        if (Tools::isSubmit('submitBySupplier')) {
            $id_customer = Tools::getValue('allowed_supp_by_customer');
            $id_supplier = Tools::getValue('allowed_supplier');
            $allow_type = Tools::getValue('supplier_type');
            if ($this->insertSupplierRule(0, $id_customer_group, $id_supplier, $allow_type)) {
                return $this->displayConfirmation($this->l('The association have been inserted.'));
            } else {
                return $this->displayError($this->l('A problem ocurred while inserting group/supplier'));
            }
        }

        if (Tools::isSubmit('submitBySupplierGroup')) {
            $id_customer_group = Tools::getValue('allowed_supp_by_group');
            $id_supplier = Tools::getValue('allowed_supplier');
            $allow_type = Tools::getValue('supplierg_type');
            if ($this->insertSupplierRule(1, $id_customer_group, $id_supplier, $allow_type)) {
                return $this->displayConfirmation($this->l('The association have been inserted.'));
            } else {
                return $this->displayError($this->l('A problem ocurred while inserting group/supplier'));
            }
        }

        if (Tools::isSubmit('delete_customer_product_assoc')) {
            if ($delete = Db::getInstance()->execute('DELETE FROM `' . pSQL(_DB_PREFIX_) . 'idxforbiddenproducts` 
                WHERE `id_private_products` = ' . (int) Tools::getValue('id_config'))) {
                return $this->displayConfirmation($this->l('The association have been deleted.'));
            }
        }

        if (Tools::isSubmit('submitContent')) {
            $languages = Language::getLanguages(false);
            $custom_content = array();
            foreach ($languages as $language) {
                $custom_content[$language['id_lang']] = Tools::htmlentitiesUTF8(Tools::getValue('edit_text_' . $language['id_lang']));
            }

            Configuration::updateValue(Tools::strtoupper($this->name) . '_CUSTOM', $custom_content);
            return $this->displayConfirmation($this->l('The settings have been updated.'));
        }

        if (Tools::isSubmit('delete_allowed_category')) {
            if (Db::getInstance()->execute('DELETE FROM `' . pSQL(_DB_PREFIX_) . 'idxforbiddenproducts_categories` 
                WHERE `id_private_category` = ' . (int) Tools::getValue('id_config'))) {
                return $this->displayConfirmation($this->l('The association group/category have been deleted.'));
            }
        }

        if (Tools::isSubmit('delete_allowed_manufacturer')) {
            if (Db::getInstance()->execute('DELETE FROM `' . pSQL(_DB_PREFIX_) . 'idxforbiddenproducts_manufacturer` 
                WHERE `id_private_manufacturer` = ' . (int) Tools::getValue('id_config'))) {
                return $this->displayConfirmation($this->l('The association group/manufacturer have been deleted.'));
            }
        }

        if (Tools::isSubmit('delete_allowed_supplier')) {
            if (Db::getInstance()->execute('DELETE FROM `' . pSQL(_DB_PREFIX_) . 'idxforbiddenproducts_supplier` 
                WHERE `id_private_supplier` = ' . (int) Tools::getValue('id_config'))) {
                return $this->displayConfirmation($this->l('The association group/supplier have been deleted.'));
            }
        }
        return '';
    }

    public function renderForm()
    {
        return InnovaTools_2_0_0::adminTabWrap($this);
    }

    public function helpGenerateForm()
    {
        return $this->renderFormCustomPage() . $this->renderFormPriorities();
    }

    public function renderFormCustomer()
    {
        $customers = Customer::getCustomers();

        $customersArray = array();
        $all_customer = array(
            'id_customer' => -1,
            'customer_name' => $this->l('Allow all customers')
        );
        $customersArray[-1] = $all_customer;
        foreach ($customers as $key => $customer) {
            $customersArray[$key]['id_customer'] = $customer['id_customer'];
            $customersArray[$key]['customer_name'] = $customer['firstname'] . " " . $customer['lastname'];
        }

        $fields_form = array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                    'title' => $this->l('Add allowed users.'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->l('User:'),
                        'name' => 'idxcustomer',
                        'id' => 'idxforbiddenproducts_customer',
                        'desc' => $this->l('Allow by user.'),
                        'options' => array(
                            'query' => $customersArray,
                            'id' => 'id_customer',
                            'name' => 'customer_name',
                            'value' => 'id_customer'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Product'),
                        'name' => 'idxforbiddenproducts_prod',
                        'id' => 'idxforbiddenproducts_select',
                        'desc' => $this->l('Select a product'),
                        'options' => array(
                            'query' => Product::getSimpleProducts($this->context->language->id),
                            'id' => 'id_product',
                            'name' => 'name',
                            'value' => 'id_product'
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow rule'),
                        'name' => 'idxforbiddenproducts_type',
                        'desc' => $this->l('Set if is an allow or disallow rule, in case of yes only this customer will see the product.'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'idxforbiddenproducts_type_on',
                                'value' => 1,
                                'label' => $this->l('Allow')
                            ),
                            array(
                                'id' => 'idxforbiddenproducts_type_off',
                                'value' => 0,
                                'label' => $this->l('Disallow')
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default')
            ),
        );

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int) $language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->languages = $languages;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitSettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name .
            '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form)) . $this->renderFormGroupCustomer() . $this->renderList();
    }

    public function renderFormGroupCustomer()
    {
        $groups = array();
        $all_groups = array(
            'id_group' => -1,
            'name' => $this->l('Allow all groups'),
        );
        $groups[-1] = $all_groups;
        $groups_prestashop = Group::getGroups($this->context->language->id, $this->context->shop->id);
        $groups = array_merge($groups, $groups_prestashop);

        $fields_form = array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                    'title' => $this->l('Add allowed groups'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->l('Groups:'),
                        'name' => 'deluxegroup',
                        'id' => 'idxforbiddenproducts_customer',
                        'desc' => $this->l('Allow by user groups'),
                        'options' => array(
                            'query' => $groups,
                            'id' => 'id_group',
                            'name' => 'name',
                            'value' => 'id_group'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Product'),
                        'name' => 'group_product',
                        'id' => 'group_product',
                        'desc' => $this->l('Select a product'),
                        'options' => array(
                            'query' => Product::getSimpleProducts($this->context->language->id),
                            'id' => 'id_product',
                            'name' => 'name',
                            'value' => 'id_product'
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow rule'),
                        'name' => 'group_type',
                        'desc' => $this->l('Set if is an allow or disallow rule, in case of yes only this users group will see the product.'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'group_type_on',
                                'value' => 1,
                                'label' => $this->l('Allow')
                            ),
                            array(
                                'id' => 'group_type_off',
                                'value' => 0,
                                'label' => $this->l('Disallow')
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default')
            ),
        );

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int) $language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->languages = $languages;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitGroup';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) .
            '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function renderFormAllowedCategories()
    {
        $customers = Customer::getCustomers();

        $customersArray = array();
        $all_customer = array(
            'id_customer' => -1,
            'customer_name' => $this->l('Allow all customers')
        );
        $customersArray[-1] = $all_customer;
        foreach ($customers as $key => $customer) {
            $customersArray[$key]['id_customer'] = $customer['id_customer'];
            $customersArray[$key]['customer_name'] = $customer['firstname'] . " " . $customer['lastname'];
        }

        $fields_form = array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                    'title' => $this->l('Add rules by customer over products associated to this default categories'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->l('User:'),
                        'name' => 'allowed_category_by_customer',
                        'id' => 'deluxeforbiddenproducts_customer_cat',
                        'desc' => $this->l('Allow by customer '),
                        'options' => array(
                            'query' => $customersArray,
                            'id' => 'id_customer',
                            'name' => 'customer_name',
                            'value' => 'id_customer'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Category'),
                        'name' => 'categories_allowed',
                        'id' => 'categories_allowed',
                        'desc' => $this->l('Select a category'),
                        'options' => array(
                            'query' => Category::getSimpleCategories($this->context->language->id, $this->context->shop->id),
                            'id' => 'id_category',
                            'name' => 'name',
                            'value' => 'id_category'
                        ),
                        'hint' => $this->l('Customer selected in this configuration will be affected with the rule to see associated products to this default category'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow rule'),
                        'name' => 'category_type',
                        'desc' => $this->l('Set if is an allow or disallow rule, in case of yes only this users will see the category.'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'category_type_on',
                                'value' => 1,
                                'label' => $this->l('Allow')
                            ),
                            array(
                                'id' => 'category_type_off',
                                'value' => 0,
                                'label' => $this->l('Disallow')
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default')
            ),
        );

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int) $language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->languages = $languages;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitByCategory';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form)) . $this->renderFormAllowedGroupCategories() . $this->renderListAllowedCategory();
    }

    public function renderFormAllowedGroupCategories()
    {
        $groups = array();
        $all_groups = array(
            'id_group' => -1,
            'name' => $this->l('Allow all groups'),
        );
        $groups[-1] = $all_groups;
        $groups_prestashop = Group::getGroups($this->context->language->id, $this->context->shop->id);
        $groups = array_merge($groups, $groups_prestashop);

        $fields_form = array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                    'title' => $this->l('Add rules by groups over products associated to this default categories'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->l('Groups:'),
                        'name' => 'allowed_category_by_group',
                        'id' => 'idxforbiddenproducts_customer_cat',
                        'desc' => $this->l('Allow by group '),
                        'options' => array(
                            'query' => $groups,
                            'id' => 'id_group',
                            'name' => 'name',
                            'value' => 'id_group'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Category'),
                        'name' => 'categoriesg_allowed',
                        'id' => 'categoriesg_allowed',
                        'desc' => $this->l('Select a category'),
                        'options' => array(
                            'query' => Category::getSimpleCategories(
                                $this->context->language->id,
                                $this->context->shop->id
                            ),
                            'id' => 'id_category',
                            'name' => 'name',
                            'value' => 'id_category'
                        ),
                        'hint' => $this->l('Group selected in this configuration will be able to see associated products  to this default category'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow rule'),
                        'name' => 'categoryg_type',
                        'desc' => $this->l('Set if is an allow or disallow rule, in case of yes only this users group will see the category.'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'categoryg_type_on',
                                'value' => 1,
                                'label' => $this->l('Allow')
                            ),
                            array(
                                'id' => 'categoryg_type_off',
                                'value' => 0,
                                'label' => $this->l('Disallow')
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default')
            ),
        );

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int) $language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->languages = $languages;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitByCategoryGroup';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function renderFormAllowedManufacturer()
    {
        $customers = Customer::getCustomers();

        $customersArray = array();
        $all_customer = array(
            'id_customer' => -1,
            'customer_name' => $this->l('Allow all customers')
        );
        $customersArray[-1] = $all_customer;
        foreach ($customers as $key => $customer) {
            $customersArray[$key]['id_customer'] = $customer['id_customer'];
            $customersArray[$key]['customer_name'] = $customer['firstname'] . " " . $customer['lastname'];
        }

        $fields_form = array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                    'title' => $this->l('Add products associated to this default manufacturer'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->l('User:'),
                        'name' => 'allowed_manuf_by_customer',
                        'id' => 'deluxeforbiddenproducts_customer_manuf',
                        'desc' => $this->l('Allow by user '),
                        'options' => array(
                            'query' => $customersArray,
                            'id' => 'id_customer',
                            'name' => 'customer_name',
                            'value' => 'id_customer'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Manufacturer'),
                        'name' => 'allowed_manufacturer',
                        'id' => 'allowed_manufacturer',
                        'desc' => $this->l('Select a manufacturer'),
                        'options' => array(
                            'query' => Manufacturer::getManufacturers(false, $this->context->language->id, true, false),
                            'id' => 'id_manufacturer',
                            'name' => 'name',
                            'value' => 'id_manufacturer'
                        ),
                        'hint' => $this->l('Group selected in this configuration will be able to see associated products to this default manufacturer'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow rule'),
                        'name' => 'manufacturer_type',
                        'desc' => $this->l('Set if is an allow or disallow rule, in case of yes only this users group will see the manufacturer.'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'manufacturer_type_on',
                                'value' => 1,
                                'label' => $this->l('Allow')
                            ),
                            array(
                                'id' => 'manufacturer_type_off',
                                'value' => 0,
                                'label' => $this->l('Disallow')
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default')
            ),
        );

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int) $language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->languages = $languages;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitByManufacturer';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' .
            $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form)) . $this->renderFormAllowedGroupManufacturer() . $this->renderListAllowedManufacturer();
    }

    public function renderFormAllowedGroupManufacturer()
    {
        $groups = array();
        $all_groups = array(
            'id_group' => -1,
            'name' => $this->l('Allow all groups'),
        );
        $groups[-1] = $all_groups;
        $groups_prestashop = Group::getGroups($this->context->language->id, $this->context->shop->id);
        $groups = array_merge($groups, $groups_prestashop);

        $fields_form = array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                    'title' => $this->l('Add products associated to this default manufacturer'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->l('Groups:'),
                        'name' => 'allowed_manuf_by_customer',
                        'id' => 'idxforbiddenproducts_customer_manuf',
                        'desc' => $this->l('Allow by group '),
                        'options' => array(
                            'query' => $groups,
                            'id' => 'id_group',
                            'name' => 'name',
                            'value' => 'id_group'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Manufacturer'),
                        'name' => 'allowed_manufacturer',
                        'id' => 'allowed_manufacturer',
                        'desc' => $this->l('Select a manufacturer'),
                        'options' => array(
                            'query' => Manufacturer::getManufacturers(
                                false,
                                $this->context->language->id,
                                true,
                                false
                            ),
                            'id' => 'id_manufacturer',
                            'name' => 'name',
                            'value' => 'id_manufacturer'
                        ),
                        'hint' => $this->l('Group selected in this configuration will be able to see associated products to this default manufacturer'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow rule'),
                        'name' => 'manufacturerg_type',
                        'desc' => $this->l('Set if is an allow or disallow rule, in case of yes only this users group will see the manufacturer.'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'manufacturerg_type_on',
                                'value' => 1,
                                'label' => $this->l('Allow')
                            ),
                            array(
                                'id' => 'manufacturerg_type_off',
                                'value' => 0,
                                'label' => $this->l('Disallow')
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default')
            ),
        );

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int) $language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->languages = $languages;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get(
            'PS_BO_ALLOW_EMPLOYEE_FORM_LANG'
        ) : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitByManufacturerGroup';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' .
            $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function renderFormAllowedSupplier()
    {
        $customers = Customer::getCustomers();

        $customersArray = array();
        $all_customer = array(
            'id_customer' => -1,
            'customer_name' => $this->l('Allow all customers')
        );
        $customersArray[-1] = $all_customer;
        foreach ($customers as $key => $customer) {
            $customersArray[$key]['id_customer'] = $customer['id_customer'];
            $customersArray[$key]['customer_name'] = $customer['firstname'] . " " . $customer['lastname'];
        }

        $fields_form = array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                    'title' => $this->l('Add products associated to this default supplier'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->l('Customers:'),
                        'name' => 'allowed_supp_by_customer',
                        'id' => 'deluxeforbiddenproducts_customer_sup',
                        'desc' => $this->l('Allow by user '),
                        'options' => array(
                            'query' => $customersArray,
                            'id' => 'id_customer',
                            'name' => 'customer_name',
                            'value' => 'id_customer'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Supplier'),
                        'name' => 'allowed_supplier',
                        'id' => 'allowed_supplier',
                        'desc' => $this->l('Select a supplier'),
                        'options' => array(
                            'query' => Supplier::getSuppliers(false, (int) Context::getContext()->language->id, true, false, false),
                            'id' => 'id_supplier',
                            'name' => 'name',
                            'value' => 'id_supplier'
                        ),
                        'hint' => $this->l('Group selected in this configuration will be able to see associated products  to this default supplier'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow rule'),
                        'name' => 'supplier_type',
                        'desc' => $this->l('Set if is an allow or disallow rule, in case of yes only this users group will see the supplier.'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'supplier_type_on',
                                'value' => 1,
                                'label' => $this->l('Allow')
                            ),
                            array(
                                'id' => 'supplier_type_off',
                                'value' => 0,
                                'label' => $this->l('Disallow')
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default')
            ),
        );

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int) $language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->languages = $languages;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitBySupplier';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' .
            $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form)) . $this->renderFormAllowedGroupSupplier() . $this->renderListAllowedSupplier();
    }

    public function renderFormAllowedGroupSupplier()
    {
        $groups = array();
        $all_groups = array(
            'id_group' => -1,
            'name' => $this->l('Allow all groups'),
        );
        $groups[-1] = $all_groups;
        $groups_prestashop = Group::getGroups($this->context->language->id, $this->context->shop->id);
        $groups = array_merge($groups, $groups_prestashop);

        $fields_form = array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                    'title' => $this->l('Add products associated to this default supplier'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->l('Group:'),
                        'name' => 'allowed_supp_by_group',
                        'id' => 'idxforbiddenproducts_customer_sup',
                        'desc' => $this->l('Allow by group '),
                        'options' => array(
                            'query' => $groups,
                            'id' => 'id_group',
                            'name' => 'name',
                            'value' => 'id_group'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Supplier'),
                        'name' => 'allowed_supplier',
                        'id' => 'allowed_supplier',
                        'desc' => $this->l('Select a supplier'),
                        'options' => array(
                            'query' => Supplier::getSuppliers(
                                false,
                                (int) Context::getContext()->language->id,
                                true,
                                false,
                                false
                            ),
                            'id' => 'id_supplier',
                            'name' => 'name',
                            'value' => 'id_supplier'
                        ),
                        'hint' => $this->l('Group selected in this configuration will be able to see associated products  to this default supplier'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow rule'),
                        'name' => 'supplierg_type',
                        'desc' => $this->l('Set if is an allow or disallow rule, in case of yes only this users group will see the supplier.'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'supplierg_type_on',
                                'value' => 1,
                                'label' => $this->l('Allow')
                            ),
                            array(
                                'id' => 'supplierg_type_off',
                                'value' => 0,
                                'label' => $this->l('Disallow')
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default')
            ),
        );

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int) $language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->languages = $languages;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get(
            'PS_BO_ALLOW_EMPLOYEE_FORM_LANG'
        ) : 0;

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitBySupplierGroup';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' .
            $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function renderFormCustomPage()
    {
        $fields_form = array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                    'title' => $this->l('Custom page content'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'textarea',
                        'lang' => true,
                        'autoload_rte' => true,
                        'cols' => 10,
                        'rows' => 5,
                        'label' => $this->l('Set custom page content'),
                        'name' => 'edit_text',
                        'desc' => $this->l('Set custom content into forbidden product access.')
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default')
            ),
        );

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int) $language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->languages = $languages;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get(
            'PS_BO_ALLOW_EMPLOYEE_FORM_LANG'
        ) : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitContent';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' .
            $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function renderFormPriorities()
    {
        $priorities_product = $this->getPriorities('product');
        $priorities_customer = $this->getPriorities('customer');

        $fields_form = array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                    'title' => $this->l('Rules priorities'),
                    'icon' => 'icon-sort-amount-desc'
                ),
                'input' => array(
                    array(
                        'type' => 'priorities',
                        'label' => $this->l('Set priorities rule type'),
                        'name' => 'product_priorities',
                        'order' => $priorities_product,
                        'desc' => $this->l('Set the priorities for apply the rules.')
                    ),
                    array(
                        'type' => 'priorities',
                        'label' => $this->l('Set priorities customer/group'),
                        'name' => 'customer_priorities',
                        'order' => $priorities_customer,
                        'desc' => $this->l('Set the priorities for apply the rules.')
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default'
                )
            ),
        );

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int) $language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->languages = $languages;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitPriorities';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getPriorities($source = 'product')
    {

        $priorities = '';
        switch ($source) {
            case 'product':
                $priorities = Tools::jsonDecode(Configuration::get(Tools::strtoupper($this->name) . '_PRIORITIES'));
                break;
            case 'customer':
                $priorities = Tools::jsonDecode(Configuration::get(Tools::strtoupper($this->name) . '_PRIORITIESC'));
                break;
            default:
                return false;
        }

        if (!is_array($priorities)) {
            $this->setDefaultPriorities();
            return $this->getPriorities($source);
        }

        $formatted = array();
        foreach ($priorities as $priority) {
            $val = array(
                'name' => $this->l($priority),
                'val' => $priority
            );
            $formatted[] = $val;
        }

        return $formatted;
    }

    public function getConfigFieldsValues()
    {
        $languages = Language::getLanguages(false);
        $custom_content = array();
        foreach ($languages as $language) {
            $custom_content[$language['id_lang']] = Tools::htmlentitiesDecodeUTF8(
                Configuration::get(Tools::strtoupper($this->name) . '_CUSTOM', $language['id_lang'])
            );
        }
        return array(
            'idxcustomer' => Tools::getValue('idxcustomer'),
            'idxforbiddenproducts_prod' => Tools::getValue('idxforbiddenproducts_prod'),
            'idxforbiddenproducts_type' => Tools::getValue('idxforbiddenproducts_type'),
            'deluxegroup' => Tools::getValue('deluxegroup'),
            'group_product' => Tools::getValue('group_product'),
            'group_type' => Tools::getValue('group_type'),
            'allowed_category_by_customer' => Tools::getValue('allowed_category_by_customer'),
            'allowed_category_by_group' => Tools::getValue('allowed_category_by_group'),
            'categories_allowed' => Tools::getValue('categories_allowed'),
            'categoriesg_allowed' => Tools::getValue('categoriesg_allowed'),
            'category_type' => Tools::getValue('category_type'),
            'categoryg_type' => Tools::getValue('categoryg_type'),
            'allowed_manuf_by_customer' => Tools::getValue('allowed_manuf_by_customer'),
            'allowed_manufacturer' => Tools::getValue('allowed_manufacturer'),
            'manufacturer_type' => Tools::getValue('manufacturer_type'),
            'manufacturerg_type' => Tools::getValue('manufacturerg_type'),
            'allowed_supp_by_customer' => Tools::getValue('allowed_supp_by_customer'),
            'allowed_supp_by_group' => Tools::getValue('allowed_supp_by_group'),
            'allowed_supplier' => Tools::getValue('allowed_supplier'),
            'supplier_type' => Tools::getValue('supplier_type'),
            'supplierg_type' => Tools::getValue('supplierg_type'),
            'edit_text' => Tools::getValue('edit_text', $custom_content),
        );
    }

    public function renderList()
    {
        $selectAllConfiguration = $this->getConfiguration();

        $fields_list = array(
            'id_config' => array(
                'title' => 'ID',
                'align' => 'left',
                'rows' => 50,
                'search' => false,
                'order' => false,
            ),
            'is_group' => array(
                'title' => $this->l('Group/Customer'),
                'align' => 'left',
                'search' => false,
            ),
            'id_user_group' => array(
                'title' => $this->l('Name'),
                'align' => 'left',
            ),
            'id_product' => array(
                'title' => $this->l('Product'),
                'align' => 'left',
            ),
            'allow_rule' => array(
                'title' => $this->l('Type'),
                'align' => 'left',
            ),
        );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        // Actions to be displayed in the "Actions" column
        $helper->actions = array('delete');
        $helper->identifier = 'id_config';
        $helper->show_toolbar = true;
        $helper->title = $this->l('Products status');
        $helper->table = '_customer_product_assoc';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        return '<div id="_customer_product_assoc">' . $helper->generateList($selectAllConfiguration, $fields_list) . '</div>';
    }

    public function renderListAllowedCategory()
    {
        $selectAllConfiguration = $this->getAllowedCatConfiguration();
        $msg = $this->l('No configuration defined to categories');
        if (!$selectAllConfiguration) {
            $this->smarty->assign(array(
                "msg" => $msg
            ));
            return $this->display(__FILE__, "views/templates/admin/empty_alert.tpl");
        }

        $fields_list = array(
            'id_config' => array(
                'title' => 'ID',
                'align' => 'left',
                'rows' => 50
            ),
            'is_group' => array(
                'title' => $this->l('Group/Customer'),
                'align' => 'left',
            ),
            'id_group' => array(
                'title' => $this->l('Name'),
                'align' => 'left',
            ),
            'id_category' => array(
                'title' => $this->l('Categories'),
                'align' => 'left',
            ),
            'allow_rule' => array(
                'title' => $this->l('Type'),
                'align' => 'left',
            ),
        );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        // Actions to be displayed in the "Actions" column
        $helper->actions = array('delete');
        $helper->identifier = 'id_config';
        $helper->show_toolbar = true;
        $helper->title = $this->l('Categories status');
        $helper->table = '_allowed_category';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        return $helper->generateList($selectAllConfiguration, $fields_list);
    }

    public function renderListAllowedManufacturer()
    {
        $selectAllConfiguration = $this->getAllowedManufactuerConfiguration();
        $msg = $this->l('No configuration defined to manufacturer');
        if (!$selectAllConfiguration) {
            $this->smarty->assign(array(
                "msg" => $msg
            ));
            return $this->display(__FILE__, "views/templates/admin/empty_alert.tpl");
        }


        $fields_list = array(
            'id_config' => array(
                'title' => 'ID',
                'align' => 'left',
                'rows' => 50
            ),
            'is_group' => array(
                'title' => $this->l('Group/Customer'),
                'align' => 'left',
            ),
            'id_group' => array(
                'title' => $this->l('Group'),
                'align' => 'left',
            ),
            'id_manufacturer' => array(
                'title' => $this->l('Manufacturer'),
                'align' => 'left',
            ),
            'allow_rule' => array(
                'title' => $this->l('Type'),
                'align' => 'left',
            ),
        );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        // Actions to be displayed in the "Actions" column
        $helper->actions = array('delete');
        $helper->identifier = 'id_config';
        $helper->show_toolbar = true;
        $helper->title = $this->l('Manufacturer status');
        $helper->table = '_allowed_manufacturer';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        return $helper->generateList($selectAllConfiguration, $fields_list);
    }

    public function renderListAllowedSupplier()
    {
        $selectAllConfiguration = $this->getAllowedSupplierConfiguration();
        $msg = $this->l('No configuration defined to suppliers');
        if (!$selectAllConfiguration) {
            $this->smarty->assign(array(
                "msg" => $msg
            ));
            return $this->display(__FILE__, "views/templates/admin/empty_alert.tpl");
        }

        $fields_list = array(
            'id_config' => array(
                'title' => 'ID',
                'align' => 'left',
                'rows' => 50
            ),
            'is_group' => array(
                'title' => $this->l('Group/Customer'),
                'align' => 'left',
            ),
            'id_group' => array(
                'title' => $this->l('Group'),
                'align' => 'left',
            ),
            'id_supplier' => array(
                'title' => $this->l('Suppliers'),
                'align' => 'left',
            ),
            'allow_rule' => array(
                'title' => $this->l('Type'),
                'align' => 'left',
            ),
        );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        // Actions to be displayed in the "Actions" column
        $helper->actions = array('delete');
        $helper->identifier = 'id_config';
        $helper->show_toolbar = true;
        $helper->title = $this->l('Manufacturer status');
        $helper->table = '_allowed_supplier';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        return $helper->generateList($selectAllConfiguration, $fields_list);
    }

    // Custom --------->

    public function setInnovaTabs()
    {

        if (Tools::isSubmit('submitSettings') || Tools::isSubmit('submitGroup') || Tools::isSubmit('submitSettings') || Tools::isSubmit('delete_customer_product_assoc')) {
            $this->module_tab = 'product';
        }
        if (Tools::isSubmit('submitByCategory') || Tools::isSubmit('submitByCategoryGroup') || Tools::isSubmit('delete_allowed_category')) {
            $this->module_tab = 'category';
        }
        if (Tools::isSubmit('submitByManufacturer') || Tools::isSubmit('submitByManufacturerGroup') || Tools::isSubmit('delete_allowed_manufacturer')) {
            $this->module_tab = 'manufacturer';
        }
        if (Tools::isSubmit('submitBySupplier') || Tools::isSubmit('submitBySupplierGroup') || Tools::isSubmit('delete_allowed_supplier')) {
            $this->module_tab = 'supplier';
        }

        $isoLinks = InnovaTools_2_0_0::getIsoLinks($this);
        $this->innovatabs = array();
        $this->innovatabs [] = array(
            "title" => $this->l('Configuration'),
            "icon" => "wrench",
            "link" => "helpGenerateForm",
            "type" => "tab",
            "show" => "both",
            "active" => !$this->module_tab,
        );

        $this->innovatabs [] = array(
            "title" => $this->l('Rules by product'),
            "icon" => "bookmark",
            "link" => "renderFormCustomer",
            "type" => "tab",
            "show" => "both",
            "active" => $this->module_tab == 'product' ? true : false,
        );

        $this->innovatabs [] = array(
            "title" => $this->l('Rules by category'),
            "icon" => "bookmark",
            "link" => "renderFormAllowedCategories",
            "type" => "tab",
            "show" => "both",
            "active" => $this->module_tab == 'category' ? true : false,
        );

        $this->innovatabs [] = array(
            "title" => $this->l('Rules by manufacturer'),
            "icon" => "bookmark",
            "link" => "renderFormAllowedManufacturer",
            "type" => "tab",
            "show" => "both",
            "active" => $this->module_tab == 'manufacturer' ? true : false,
        );

        $this->innovatabs [] = array(
            "title" => $this->l('Rules by supplier'),
            "icon" => "bookmark",
            "link" => "renderFormAllowedSupplier",
            "type" => "tab",
            "show" => "both",
            "active" => $this->module_tab == 'supplier' ? true : false,
        );

        $this->innovatabs [] = array(
            "title" => $this->l('Documentation'),
            "icon" => "file",
            "link" => $this->doclink,
            "type" => "doc",
            "show" => "both",
        );
        $this->innovatabs [] = array(
            "title" => $this->l('Support'),
            "icon" => "life-saver",
            "link" => $isoLinks["support"],
            "type" => "url",
            "show" => "whmcs",
        );
        /*
          $this->innovatabs [] = array(
          "title" => $this->l('Opinion'),
          "icon" => "comments",
          "link" => $isoLinks["ratings"],
          "type" => "url",
          "show" => "both",
          );
         */
        /*
          $this->innovatabs [] = array(
          "title" => $this->l('Certified Agency'),
          "icon" => "trophy",
          "link" => $isoLinks["certified"],
          "type" => "url",
          "show" => "addons",
          );
         */
        $this->innovatabs [] = array(
            "title" => $this->l('Our Modules'),
            "icon" => "cubes",
            "link" => $isoLinks["ourmodules"],
            "type" => "url",
            "show" => "both",
        );
    }

    public function innovaTitle()
    {
        //tabs version
        $innovaTabs = "";
        if (method_exists(get_class($this), "setInnovaTabs")) {
            $innovaTabs = $this->setInnovaTabs();
        }
        $this->smarty->assign(array(
            "module_dir" => $this->_path,
            "module_name" => $this->displayName,
            "module_description" => $this->description,
            "isoLinks" => InnovaTools_2_0_0::getIsoLinks($this),
            "isAddons" => InnovaTools_2_0_0::isAddons($this),
            "tabs" => InnovaTools_2_0_0::getVersionTabs($this),
        ));

        $this->context->controller->addCSS(($this->_path) . "views/css/backinnova.css", "all");
        return $this->display(__FILE__, "views/templates/admin/innova-title.tpl");
    }

    public function makeupdate($is_group, $id, $product, $allow_type)
    {
        if ($id == -1) {
            if (!$is_group) {
                $customers = Customer::getCustomers();
                foreach ($customers as $customer) {
                    $this->makeupdate($is_group, $customer['id_customer'], $product, $allow_type);
                }
            } else {
                $groups = Group::getGroups($this->context->language->id, $this->context->shop->id);
                foreach ($groups as $group) {
                    $this->makeupdate($is_group, $group['id_group'], $product, $allow_type);
                }
            }
        }

        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS(
            'SELECT * FROM `' . pSQL(_DB_PREFIX_ . $this->name) . '` WHERE
            is_group = "' . (int) $is_group . '" AND
            id_user_group = "' . (int) $id . '" AND
            id_product = "' . (int) $product . '"'
        );

        if ($res) {
            $id_private_product = $res[0]['id_private_products'];
            if (!Db::getInstance()->Execute('
                UPDATE ' . pSQL(_DB_PREFIX_ . $this->name) . ' SET
                  is_group = "' . (int) $is_group . '", 
                  id_user_group = "' . (int) $id . '", 
                  id_product = "' . (int) $product . '", 
                  allow_rule = "' . (int) $allow_type . '" 
                WHERE id_private_products= "' . (int) $id_private_product . '"
        ')) {
                return false;
            }
        } else {
            if (!Db::getInstance()->Execute('
          INSERT INTO ' . pSQL(_DB_PREFIX_ . $this->name) . '
            (is_group, id_user_group, id_product, allow_rule)
          VALUES
            ("' . (int) $is_group . '", "' . (int) $id . '", "' . (int) $product . '", "' . (int) $allow_type . '")
      ')) {
                return false;
            }
        }

        return true;
    }

    public function insertCategoryRule($is_group, $id_customer, $id_category, $allow_type)
    {
        if ($id_customer == -1) {
            $result = true;
            if (!$is_group) {
                $customers = Customer::getCustomers();
                foreach ($customers as $customer) {
                    if (!$this->insertCategoryRule($is_group, $customer['id_customer'], $id_category, $allow_type)) {
                        $result = false;
                    }
                }
            } else {
                $groups = Group::getGroups($this->context->language->id, $this->context->shop->id);
                foreach ($groups as $group) {
                    if (!$this->insertCategoryRule($is_group, $group['id_group'], $id_category, $allow_type)) {
                        $result = false;
                    }
                }
            }
            return $result;
        } else {
            $table = pSQL($this->name) . '_categories';
            $sql_exist = 'Select id_private_category from ' . _DB_PREFIX_ . pSQL($table) . ' '
                . ' where is_group = ' . (int) $is_group . ' and id_group = ' . (int) $id_customer . ' and id_category = ' . (int) $id_category;
            $exist = Db::getInstance()->getValue($sql_exist);
            $data = array(
                'is_group' => (int) $is_group,
                'id_group' => (int) $id_customer,
                'id_category' => (int) $id_category,
                'allow_rule' => (int) $allow_type
            );
            if ($exist) {
                $where = 'id_private_category = ' . (int) $exist;
                return Db::getInstance()->update($table, $data, $where);
            } else {
                return Db::getInstance()->insert($table, $data);
            }
        }
    }

    public function insertManufacturerRule($is_group, $id_customer, $id_manufacturer, $allow_type)
    {
        if ($id_customer == -1) {
            $result = true;
            if (!$is_group) {
                $customers = Customer::getCustomers();
                foreach ($customers as $customer) {
                    if (!$this->insertCategoryRule($is_group, $customer['id_customer'], $id_manufacturer, $allow_type)) {
                        $result = false;
                    }
                }
            } else {
                $groups = Group::getGroups($this->context->language->id, $this->context->shop->id);
                foreach ($groups as $group) {
                    if (!$this->insertCategoryRule($is_group, $group['id_group'], $id_manufacturer, $allow_type)) {
                        $result = false;
                    }
                }
            }
            return $result;
        } else {
            $table = pSQL($this->name) . '_manufacturer';
            $sql_exist = 'Select id_private_manufacturer from ' . _DB_PREFIX_ . pSQL($table) . ' '
                . ' where is_group = ' . (int) $is_group . ' and id_group = ' . (int) $id_customer . ' and id_manufacturer = ' . (int) $id_manufacturer;
            $exist = Db::getInstance()->getValue($sql_exist);
            $data = array(
                'is_group' => (int) $is_group,
                'id_group' => (int) $id_customer,
                'id_manufacturer' => (int) $id_manufacturer,
                'allow_rule' => (int) $allow_type
            );
            if ($exist) {
                $where = 'id_private_manufacturer = ' . (int) $exist;
                return Db::getInstance()->update($table, $data, $where);
            } else {
                return Db::getInstance()->insert($table, $data);
            }
        }
    }

    public function insertSupplierRule($is_group, $id_customer, $id_supplier, $allow_type)
    {
        if ($id_customer == -1) {
            $result = true;
            if (!$is_group) {
                $customers = Customer::getCustomers();
                foreach ($customers as $customer) {
                    if (!$this->insertCategoryRule($is_group, $customer['id_customer'], $id_supplier, $allow_type)) {
                        $result = false;
                    }
                }
            } else {
                $groups = Group::getGroups($this->context->language->id, $this->context->shop->id);
                foreach ($groups as $group) {
                    if (!$this->insertCategoryRule($is_group, $group['id_group'], $id_supplier, $allow_type)) {
                        $result = false;
                    }
                }
            }
            return $result;
        } else {
            $table = pSQL($this->name) . '_supplier';
            $sql_exist = 'Select id_private_supplier from ' . _DB_PREFIX_ . pSQL($table) . ' '
                . ' where is_group = ' . (int) $is_group . ' and id_group = ' . (int) $id_customer . ' and id_supplier = ' . (int) $id_supplier;
            $exist = Db::getInstance()->getValue($sql_exist);
            $data = array(
                'is_group' => (int) $is_group,
                'id_group' => (int) $id_customer,
                'id_supplier' => (int) $id_supplier,
                'allow_rule' => (int) $allow_type
            );
            if ($exist) {
                $where = 'id_private_supplier = ' . (int) $exist;
                return Db::getInstance()->update($table, $data, $where);
            } else {
                return Db::getInstance()->insert($table, $data);
            }
        }
    }

    public function getConfiguration()
    {
        $result = array();
        $row = array();
        $query = 'SELECT * FROM `' . pSQL(_DB_PREFIX_ . $this->name) . '`';
        if (Tools::isSubmit('submitFilter_customer_product_assoc') && Tools::getValue('submitFilter_customer_product_assoc') == 1) {
            $query .= ' where 1=1 ';
        }
        $res = Db::getInstance()->ExecuteS($query);
        foreach ($res as $value) {
            $row['id_config'] = $value['id_private_products'];
            $row['is_group'] = $value['is_group'] ? $this->l('Group') : $this->l('Customer');
            $row['id_user_group'] = $value['is_group'] ? $this->getGroup($value['id_user_group']) : $this->getCustomer($value['id_user_group']);
            $row['id_product'] = $this->getProductName($value['id_product']);
            $row['allow_rule'] = $value['allow_rule'] ? $this->l('Allow') : $this->l('Deny');
            $valid = true;
            if (Tools::isSubmit('submitFilter_customer_product_assoc') == 1 && Tools::getValue('submitFilter_customer_product_assoc') == 1) {
                $customer = Tools::getValue('_customer_product_assocFilter_id_user_group');
                if ($customer) {
                    $valid = strpos($row['id_user_group'], $customer);
                }

                $product = Tools::getValue('_customer_product_assocFilter_id_product');
                if ($product) {
                    $valid = strpos($row['id_product'], $product);
                }
            }
            if ($valid) {
                $result[] = $row;
            }
        }
        return $result;
    }

    public function getAllowedCatConfiguration()
    {
        $result = array();
        $row = array();
        $res = Db::getInstance()->ExecuteS('
        SELECT *
        FROM `' . pSQL(_DB_PREFIX_ . $this->name) . '_categories`');
        foreach ($res as $value) {
            $row['id_config'] = $value['id_private_category'];
            $row['is_group'] = $value['is_group'] ? $this->l('Group') : $this->l('Customer');
            $row['id_group'] = $value['is_group'] ? $this->getGroup($value['id_group']) : $this->getCustomer($value['id_group']);
            $oCategory = new Category($value['id_category']);
            $row['id_category'] = $oCategory->getName($this->context->language->id);
            $row['allow_rule'] = $value['allow_rule'] ? $this->l('Allow') : $this->l('Deny');
            $result[] = $row;
            unset($oCategory);
        }

        return $result;
    }

    public function getAllowedManufactuerConfiguration()
    {
        $result = array();
        $row = array();
        $res = Db::getInstance()->ExecuteS('
    SELECT *
    FROM `' . pSQL(_DB_PREFIX_ . $this->name) . '_manufacturer`');
        foreach ($res as $value) {
            $row['id_config'] = $value['id_private_manufacturer'];
            $row['is_group'] = $value['is_group'] ? $this->l('Group') : $this->l('Customer');
            $row['id_group'] = $value['is_group'] ? $this->getGroup($value['id_group']) : $this->getCustomer($value['id_group']);
            $row['id_manufacturer'] = Manufacturer::getNameById($value['id_manufacturer']);
            $row['allow_rule'] = $value['allow_rule'] ? $this->l('Allow') : $this->l('Deny');
            $result[] = $row;
        }

        return $result;
    }

    public function getAllowedSupplierConfiguration()
    {
        $result = array();
        $row = array();
        $res = Db::getInstance()->ExecuteS('
    SELECT *
    FROM `' . pSQL(_DB_PREFIX_ . $this->name) . '_supplier`');
        foreach ($res as $value) {
            $row['id_config'] = $value['id_private_supplier'];
            $row['is_group'] = $value['is_group'] ? $this->l('Group') : $this->l('Customer');
            $row['id_group'] = $value['is_group'] ? $this->getGroup($value['id_group']) : $this->getCustomer($value['id_group']);
            $row['id_supplier'] = Supplier::getNameById($value['id_supplier']);
            $row['allow_rule'] = $value['allow_rule'] ? $this->l('Allow') : $this->l('Deny');
            $result[] = $row;
        }

        return $result;
    }

    public function getCustomer($id)
    {
        if ($id > 0) {
            $oCustomer = new Customer($id);
            return $oCustomer->firstname . ' ' . $oCustomer->lastname;
        }
    }

    public function getGroup($id)
    {
        if ($id) {
            $oGroup = new Group($id, $this->context->language->id, $this->context->shop->id);
            return $oGroup->name;
        }
    }

    public function getProductName($id)
    {
        if ($id) {
            return Product::getProductName($id, null, $this->context->language->id);
        }
    }

    // override methods.

    public function compruebaSiEsProductoProhibido($products, $forbidden)
    {
        if (in_array($products, $forbidden)) {
            return true;
        }
    }

    public function getProductsByCustomer()
    {

        $id_customer = $this->context->customer->id;

        if ($id_customer) {
            $id_groups = Customer::getGroupsStatic($id_customer);
            $where_idcustomer = (int) $id_customer;
            $where_groups = ' in (' . implode(',', $id_groups) . ') ';
        } else {
            $group = Group::getCurrent();
            $where_idcustomer = false;
            $where_groups = ' in (' . (int) $group->id . ') ';
        }

        $priorities = array_reverse($this->getPriorities());
        $sub_prioritioe = array_reverse($this->getPriorities('customer'));
        $forbbiden_products = array();
        foreach ($priorities as $section) {
            $this->setForbbidenProducts($forbbiden_products, $where_idcustomer, $where_groups, $sub_prioritioe, $section);
        }
        
        $updateallowed = array();
        foreach($forbbiden_products as $forbbiden_product) {
            $prod = new Product($forbbiden_product);
            if (!$this->isProductAllowed(
                                $id_customer,
                                $prod->id,
                                $prod->id_category_default,
                                $prod->id_manufacturer,
                                $prod->id_supplier,
                                $this->context->customer->id_default_group
                            )) {
                $updateallowed[]=$forbbiden_product;
            }
            unset($prod);
        }
        
        return $updateallowed;
    }

    public function setForbbidenProducts(&$forbbiden, $where_idcustomer, $where_groups, $sub_priorities, $section)
    {
        $table = '';
        $id = '';
        $user_column = '';
        switch ($section['val']) {
            case 'product':
                $table = '';
                $id = 'id_product';
                $user_column = 'id_user_group';
                break;
            case 'category':
                $table = '_categories';
                $id = 'id_category';
                $user_column = 'id_group';
                break;
            case 'manufacturer':
                $table = '_manufacturer';
                $id = 'id_manufacturer';
                $user_column = 'id_group';
                break;
            case 'supplier':
                $table = '_supplier';
                $id = 'id_supplier';
                $user_column = 'id_group';
                break;
            default:
                return false;
        }

        //Forbbiden
        $products_query = 'SELECT DISTINCT ' . $id . ' FROM ' . pSQL(_DB_PREFIX_ . $this->name . $table) . ' where allow_rule = 1 AND ((is_group = 0 AND ' . pSQL($user_column) . ' != ' . (int) $where_idcustomer . ') || (is_group = 1 AND ' . pSQL($user_column) . ' NOT ' . pSQL($where_groups) . '))';
        $forbbiden_p = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($products_query);
        $for_p_arr = array();
        foreach ($forbbiden_p as $product) {
            $for_p_arr[] = $product[$id];
        }
        $all_p_arr = array();
        foreach ($sub_priorities as $priority) {
            if ($priority['val'] == 'customer') {
                $forbbiden_query = 'SELECT DISTINCT ' . $id . ' FROM ' . pSQL(_DB_PREFIX_ . $this->name . $table) . ' 
                                    where (is_group = 0 AND ' . pSQL($user_column) . ' = ' . (int) $where_idcustomer . ') and allow_rule = 0;';
                $f_result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($forbbiden_query);
                foreach ($f_result as $product) {
                    $for_p_arr[] = $product[$id];
                }
                //$all_p_arr = array();
                $allowed_query = 'SELECT DISTINCT ' . $id . ' FROM ' . pSQL(_DB_PREFIX_ . $this->name . $table) . ' 
                                  where (is_group = 0 AND ' . pSQL($user_column) . ' = ' . (int) $where_idcustomer . ') and allow_rule = 1;';
                $a_result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($allowed_query);
                foreach ($a_result as $product) {
                    $all_p_arr[] = $product[$id];
                }
            } else {
                $forbbiden_query = 'SELECT DISTINCT ' . $id . ' FROM ' . pSQL(_DB_PREFIX_ . $this->name . $table) . ' 
                                    where (is_group = 1 AND ' . pSQL($user_column) . ' ' . pSQL($where_groups) . ') and allow_rule = 0;';
                $f_result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($forbbiden_query);
                foreach ($f_result as $product) {
                    $for_p_arr[] = $product[$id];
                }
                $all_p_arr = array();
                $allowed_query = 'SELECT DISTINCT ' . $id . ' FROM ' . pSQL(_DB_PREFIX_ . $this->name . $table) . ' 
                                  where (is_group = 1 AND ' . pSQL($user_column) . ' ' . pSQL($where_groups) . ') and allow_rule = 1;';
                $a_result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($allowed_query);
                foreach ($a_result as $product) {
                    $all_p_arr[] = $product[$id];
                }
            }
            
            $forbbiden_diff = array_diff($for_p_arr, $all_p_arr);

            if (count($forbbiden_diff) > 0) {
                switch ($section['val']) {
                    case 'product':
                        $forbbiden = array_merge($forbbiden, $forbbiden_diff);
                        break;
                    case 'category':
                        $numProduct = DB::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT count(*)
                        FROM `' . pSQL(_DB_PREFIX_) . 'product`');
                        $total_product_category = array();
                        foreach ($forbbiden_diff as $category) {
                            if ($products_exist = Product::getProducts(Context::getContext()->language->id, 0, $numProduct[0]['count(*)'], "date_add", "ASC", $category)) {
                                foreach ($products_exist as $product) {
                                    $total_product_category[] = $product['id_product'];
                                }
                            }
                        }
                        $forbbiden_by_category = array_diff($total_product_category, $forbbiden);
                        $forbbiden = array_merge($forbbiden, $forbbiden_by_category);
                        break;
                    case 'manufacturer':
                        $total_product_manufacturer = array();
                        foreach ($forbbiden_diff as $manufacturer) {
                            $oManufacturer = new Manufacturer($manufacturer);
                            if ($products_exist = $oManufacturer->getProductsLite(Context::getContext()->language->id)) {
                                foreach ($products_exist as $product) {
                                    $total_product_manufacturer[] = $product['id_product'];
                                }
                            }
                            unset($oManufacturer);
                        }
                        $forbbiden_by_manufacturer = array_diff($total_product_manufacturer, $forbbiden);
                        $forbbiden = array_merge($forbbiden, $forbbiden_by_manufacturer);
                        break;
                    case 'supplier':
                        $total_product_supplier = array();
                        foreach ($forbbiden_diff as $supplier) {
                            $oSupplier = new Supplier($supplier);
                            if ($products_exist = $oSupplier->getProductsLite(Context::getContext()->language->id)) {
                                foreach ($products_exist as $product) {
                                    $total_product_supplier[] = $product['id_product'];
                                }
                            }
                            unset($oSupplier);
                        }
                        $forbbiden_by_supplier = array_diff($total_product_supplier, $forbbiden);
                        $forbbiden = array_merge($forbbiden, $forbbiden_by_supplier);
                        break;
                    default:
                        return false;
                }
            }
        }
        return true;
    }

    public function isProductAllowed($user, $id_product, $id_category, $id_manufacturer, $id_supplier, $id_default_group)
    {
        $userallowed = 1;
        $groupallowed = 1;
        $categoryallowed = 1;
        $manufacturer_allowed = 1;
        $supplier_allowed = 1;

        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
      SELECT COUNT(id_private_supplier) 
      FROM `' . pSQL(_DB_PREFIX_) . 'idxforbiddenproducts_supplier`
      WHERE allow_rule = 1 AND id_group =' . (int) $id_default_group . ' 
      AND id_supplier= ' . (int) $id_supplier);

        if ($res[0]['COUNT(id_private_supplier)'] == 0) {
            $supplier_allowed = 0;
        }

        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
      SELECT COUNT(id_private_manufacturer) 
      FROM `' . pSQL(_DB_PREFIX_) . 'idxforbiddenproducts_manufacturer`
      WHERE allow_rule = 1 AND id_group =' . (int) $id_default_group . ' 
      AND id_manufacturer= ' . (int) $id_manufacturer);

        if ($res[0]['COUNT(id_private_manufacturer)'] == 0) {
            $manufacturer_allowed = 0;
        }
        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
      SELECT COUNT(id_private_category)
      FROM `' . pSQL(_DB_PREFIX_) . 'idxforbiddenproducts_categories`
      WHERE allow_rule = 1 AND id_group =' . (int) $id_default_group . ' 
      AND id_category= ' . (int) $id_category);

        if ($res[0]['COUNT(id_private_category)'] == 0) {
            $categoryallowed = 0;
        }

        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
      SELECT COUNT(id_private_products)
      FROM `' . pSQL(_DB_PREFIX_) . 'idxforbiddenproducts`
      WHERE
        allow_rule = 1 AND
        id_product= ' . (int) $id_product . ' AND
        id_user_group = ' . (int) $user . ' AND
        is_group = 0
    ');

        if ($res[0]['COUNT(id_private_products)'] == 0) {
            $userallowed = 0;
        }


        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
      SELECT COUNT(id_group)    
      FROM `' . pSQL(_DB_PREFIX_) . 'customer_group`
      WHERE id_customer = "' . (int) $user . '" AND id_group IN (
        SELECT id_user_group
        FROM `' . pSQL(_DB_PREFIX_) . 'idxforbiddenproducts`
        WHERE is_group = 1 AND allow_rule = 1 AND id_product= "' . (int) $id_product . '")
      ');

        if ($res[0]['COUNT(id_group)'] == 0) {
            $groupallowed = 0;
        }

        // visitors and guest
        if (!$user) {
            $visitorsallowed = 0;
            $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
                    SELECT COUNT(id_user_group)
                    FROM `' . pSQL(_DB_PREFIX_) . 'idxforbiddenproducts`
                    WHERE is_group = 1 AND id_product = ' . (int) $id_product . '
                    AND allow_rule = 1 AND id_user_group IN (1,2)
            ');

            if ($res[0]['COUNT(id_user_group)'] > 0) {
                $visitorsallowed = 1;
            }
            if (!$visitorsallowed) {
                return false;
            } else {
                return true;
            }
        }

        if ($userallowed || $groupallowed || $categoryallowed ||
            $manufacturer_allowed || $supplier_allowed) {
            return true;
        } else {
            return false;
        }
    }

    public function checktables()
    {

        $colums_to_check = array(
            'idxforbiddenproducts' => array(
                array(
                    'name' => 'allow_rule',
                    'type' => 'int(1) unsigned NOT NULL',
                    'update' => '1'
                )
            ),
            'idxforbiddenproducts_categories' => array(
                array(
                    'name' => 'allow_rule',
                    'type' => 'int(1) unsigned NOT NULL',
                    'update' => '1'
                ),
                array(
                    'name' => 'is_group',
                    'type' => 'int(1) unsigned NOT NULL',
                    'update' => '1'
                )
            ),
            'idxforbiddenproducts_manufacturer' => array(
                array(
                    'name' => 'allow_rule',
                    'type' => 'int(1) unsigned NOT NULL',
                    'update' => '1'
                ),
                array(
                    'name' => 'is_group',
                    'type' => 'int(1) unsigned NOT NULL',
                    'update' => '1'
                )
            ),
            'idxforbiddenproducts_supplier' => array(
                array(
                    'name' => 'allow_rule',
                    'type' => 'int(1) unsigned NOT NULL',
                    'update' => '1'
                ),
                array(
                    'name' => 'is_group',
                    'type' => 'int(1) unsigned NOT NULL',
                    'update' => '1'
                )
            ),
        );

        foreach ($colums_to_check as $table => $columns) {
            foreach ($columns as $column) {
                $exist = Db::getInstance()->executeS('SHOW COLUMNS FROM ' . _DB_PREFIX_ . pSQL($table) . ' where Field = "' . pSQL($column['name']) . '" ;');
                if (!$exist) {
                    $add_sql = 'ALTER TABLE ' . _DB_PREFIX_ . pSQL($table) . ' ADD ' . pSQL($column['name']) . ' ' . pSQL($column['type']) . ';';
                    if ($column['update']) {
                        $update_sql = 'UPDATE ' . _DB_PREFIX_ . pSQL($table) . ' set ' . pSQL($column['name']) . ' = ' . pSQL($column['update']);
                    }
                    Db::getInstance()->execute($add_sql);
                    Db::getInstance()->execute($update_sql);
                }
            }
        }
    }
}
