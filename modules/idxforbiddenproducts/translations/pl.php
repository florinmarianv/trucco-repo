<?php  

global $_MODULE; 
$_MODULE = array(); 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_2eb582649e5f06ce1df64f575c130ab9'] = 'Deluxe Produkty Prywatne'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_0e10a23e87796075f29f9e0df29f3475'] = 'Ten moduł pozwala zarządzać dostępem do produktów dla wybranych użytkowników lub grup.'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_efc226b17e0532afff43be870bff0de7'] = 'Ustawienia zostały zapisane.'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_45fcf8a7dc39fabd61cf976bdc4f132d'] = 'Powiązanie zostało stworzone.'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_8a96e9db3699c84104c6c56a81102584'] = 'Wystąpił problem przy tworzeniu powiązania grupa/kategoria'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_399df6b7061f8a8519518ae943285467'] = 'Wystąpił problem przy tworzeniu powiązania grupa/producent'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_98b52f26d206420dfd7b08bd88e81541'] = 'Wystąpił problem przy tworzeniu powiązania grupa/dostawca'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_b0accf73aa07149ea4f8e94fd3737d48'] = 'Powiązanie zostało usunięte.'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_ef532673490f45535344839f8b3579ad'] = 'Powiązanie grupa/kategoria zostało usunięte.'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_4272871cda0fbe2e1b82703fb36e9de9'] = 'Powiązanie grupa/producent zostało skasowane.'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_53910763ef83e80a8862dd9482740f38'] = 'Powiązanie grupa/dostawca zostało usunięte.'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_794df3791a8c800841516007427a2aa3'] = 'Licencja'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_a77d1540925b8613ddf32b875e4c4685'] = 'Klucz licencyjny'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_ebb36f86737be18cb17842c2e8bd7cda'] = 'Do działania tego modułu jest potrzebny ważny klucz licencyjny'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_cc0bbff7c14b57ecfae28d80593d692e'] = 'Zapisz licencję'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_54ce3f52fc61a73f4f73835533dc247b'] = 'Dodaj upoważnionych użytkowników.'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_5ff9df198222a96f26bf2cb279d83bd8'] = 'Użytkownik:'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_aff4e9362b740227dd736a3fbd99eb00'] = 'Zezwól użytkownikowi.'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_deb10517653c255364175796ace3553f'] = 'Produkt'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_fa0e92ab3c39a4fbdf81351af8d377ef'] = 'Wybierz produkt'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_c9cc8cce247e49bae79f15173ce97354'] = 'Zapisz'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_4335706098986890708ce2d2a40084b7'] = 'Dodaj grupę upoważnionych'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_8783bc7ae2b9e8d93a2bc15150aaefa8'] = 'Grupy:'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_252d4652768cbab4e4a3acf64c2e088e'] = 'Zezwól grupie użytkowników'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_9aefe80fa3265c2f111a27e82f7df1b1'] = 'Dodaj produkty powiązane do do podstawowych kategorii'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_da01d7de48c6e60c01f305cb01b04186'] = 'Zezwól grupie '; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Kategoria'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_b02d8b2c78dfd6cc791eff6f283e5008'] = 'Wybierz kategorię'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_1e1daedecc592795ae649365e93f929f'] = 'Dodaj produkty powiązane do podstawowych producentów'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Producent'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_168e100b6e4f2a4a2e8dc45380be6224'] = 'Wybierz producenta'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_63dc708d447c5097e369091cc98cfc85'] = 'Dodaj produkty powiązane do podstawowych dostawców'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_0d03c63f059d8fe19a8bb6156a4acf29'] = 'Klienci:'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_1365dea1d4555af44feff7eee2a2ceae'] = 'Zezwól użytkownikowi '; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_ec136b444eede3bc85639fac0dd06229'] = 'Dostawca'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_026bbcc09d44c3450c55ee5024ce6a9c'] = 'Wybierz dostawcę'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_ce0e00f1fbd666fd0593ce9b2b0398a4'] = 'Niestandardowa treść strony'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_2f498b2eef03dd6d7dea87219d8a4ba5'] = 'Ustaw niestandardową treść strony'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_5fe05df4df95197703a2dcb223ebefe4'] = 'Ustaw niestandardowe info przy odmowie dostępu do produktów zakazanych.'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_804bf15c6553f77a012bfcf27a3fc219'] = 'Grupa/Klient'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_49ee3087348e8d44e1feda1917443987'] = 'Nazwa'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_b154be0f7753b309aa3e0b54a18795cb'] = 'Status Produktów'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_be7ebc4c439e677116bfb3bc6d36473c'] = 'Brak zdefiniowanej konfigurac dla kategorii'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_03937134cedab9078be39a77ee3a48a0'] = 'Grupa'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_af1b98adf7f686b84cd0b443e022b7a0'] = 'Kategorie'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_d46797ad48a83936a99b8efdaf3a7384'] = 'Status Kategorii'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_4c751a0d487a3191529cf5a7755c88d9'] = 'Brak zdefiniowanej konfiguracji dla producentów'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_39cc3f0554a3a3677e5279a533f7b0af'] = 'Status Producenta'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_f5be7b1b58e49987631dd0339c195fb7'] = 'Brak zdefiniowanej konfiguracji dla dostawców'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_1814d65a76028fdfbadab64a5a8076df'] = 'Dostawcy'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_ce26601dac0dea138b7295f02b7620a7'] = 'Klient'; 
$_MODULE['<{idxforbiddenproducts}prestashop>forbidden_product_24bd724d93069e1ae366244db3023916'] = 'Brak uprawnień'; 
$_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_5b6cf869265c13af8566f192b4ab3d2a'] = 'Dokumentacja';
    $_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_254f642527b45bc260048e30704edb39'] = 'Konfiguracja';
    $_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_db5eb84117d06047c97c9a0191b5fffe'] = 'Wsparcie';
    $_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_608fe14fcdcf04b140aad3b2683a4db6'] = 'Opinie';
    $_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_8f3dab0cf0a62011289dcf37f28402a8'] = 'Agencja certyfikowana';
    $_MODULE['<{idxforbiddenproducts}prestashop>idxforbiddenproducts_0d4dc1ff63dab73ef3e4ba2e43feb256'] = 'Więcej modułów';
