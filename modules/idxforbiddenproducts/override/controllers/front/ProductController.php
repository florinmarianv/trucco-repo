<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * @author    Innova Deluxe SL
 * @copyright 2015 Innova Deluxe SL

 * @license   INNOVADELUXE
 */

class ProductController extends ProductControllerCore
{
    public function initContent()
    {
        parent::initContent();

        // deluxedownloadfiles
        $attachments = array();
    
        if ((bool)Module::isEnabled("deluxedownloadfilesb2b") == true) {
            $deluxeB2bAtt = Module::getInstanceByName('deluxedownloadfilesb2b');
            foreach ($this->product->getAttachments($this->context->language->id) as $key => $att) {
                if (!$deluxeB2bAtt->isAttachmentPrivate($att["id_attachment"])) {
                    $attachments[$key] = $att;
                } else if ($deluxeB2bAtt->isAttachmentAllowed(
                    $this->context->customer->id,
                    $att["id_attachment"]
                )) {
                    $attachments[$key] = $att;
                }
            }
        } else {
            $attachments = $this->product->getAttachments($this->context->language->id);
        }

        $this->context->smarty->assign(array(
            'attachments' => (($this->product->cache_has_attachments) ? $attachments : array()),
        )); // end deluxedownloadfiles

            // deluxeprivateproducts
        $isCategoryPrivate = false;
        if ((bool)Module::isEnabled('deluxeprivatetocategory') == true) {
            $deluxePrivateCategory = Module::getInstanceByName('deluxeprivatetocategory');
            $id_customer = $this->context->customer->id;
            $forbidden_categories = $deluxePrivateCategory->getCategoriesByCustomer($id_customer);
            $category = 0;
            
            if (version_compare(_PS_VERSION_, '1.6.0.0', '<')) {
                $category = $this->category->id;
            } else {
                $category = $this->getCategory()->id_category;
            }
            
            if ($deluxePrivateCategory->isCategoryAllowed($id_customer, $category) ||
                !$deluxePrivateCategory->compruebaSiProductoNoAccesible(
                    $forbidden_categories,
                    Product::getProductCategories(Tools::getValue('id_product'))
                )) {
                $isCategoryPrivate = false;
            } else {
                $isCategoryPrivate = true;
            }
        } // end deluxeprivateproducts
            
            // deluxeprivatecategories
        $isProductPrivate = false;
        if ((bool)Module::isEnabled('idxforbiddenproducts') == true) {
            $deluxeForbbidenP = Module::getInstanceByName('idxforbiddenproducts');
            $id_customer = $this->context->customer->id;
            $forbidden_categories = $deluxeForbbidenP->getProductsByCustomer();
            $id_customer_group = $this->context->customer->id_default_group;
            
            if ($deluxeForbbidenP->isProductAllowed(
                $id_customer,
                Tools::getValue('id_product'),
                $this->product->id_category_default,
                $this->product->id_manufacturer,
                $this->product->id_supplier,
                $id_customer_group
            ) ||
               !$deluxeForbbidenP->compruebaSiEsProductoProhibido(
                   Tools::getValue('id_product'),
                   $forbidden_categories
               )) {
            //parent::initContent();
                $isProductPrivate = false;
            } else {
                $isProductPrivate = true;
                $this->context->smarty->assign(array(
                'custom_message' => Tools::htmlentitiesDecodeUTF8(Configuration::get(
                    'IDXFORBIDDENPRODUCTS_CUSTOM',
                    $this->context->language->id
                )),
                ));
            }
        } // end deluxeprivatecategories

        if ($isCategoryPrivate) {
            $this->setTemplate('../../../modules/idxforbiddenproducts/views/templates/front/forbidden_product.tpl');
        } else if ($isProductPrivate) {
            $this->setTemplate('../../../modules/idxforbiddenproducts/views/templates/front/forbidden_product.tpl');
        }
    }
}
