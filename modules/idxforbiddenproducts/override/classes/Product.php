<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * @author    Innova Deluxe SL
 * @copyright 2015 Innova Deluxe SL

 * @license   INNOVADELUXE
 */

class Product extends ProductCore
{
    public static function getProductsProperties($id_lang, $query_result)
    {
        $results_array = array();

        if ((bool) Module::isEnabled('deluxeprivatetocategory') == true &&
                (bool) Module::isEnabled('idxforbiddenproducts') == true) {
            $deluxePrivateCategories = Module::getInstanceByName('deluxeprivatetocategory');
            $deluxeForbbidenP = Module::getInstanceByName('idxforbiddenproducts');
            $id_default_group = Context::getContext()->customer->id_default_group;
            $id_customer = Context::getContext()->customer->id;
            $forbidden_categories = $deluxePrivateCategories->getCategoriesByCustomer($id_customer);
            $forbidden_products = $deluxeForbbidenP->getProductsByCustomer();
            $results_array = array();
            if (is_array($query_result)) {
                foreach ($query_result as $value => $row) {
                    if ($row2 = Product::getProductProperties($id_lang, $row)) {
                        // si la categoria esta permitida, compruebo el producto
                        if ($deluxePrivateCategories->isCategoryAllowed($id_customer, $row2['id_category_default']) || !$deluxePrivateCategories->compruebaSiProductoNoAccesible($forbidden_categories, self::getProductCategories($row2['id_product']))) {
                            // si el producto esta permitido, a–ado commo permitido
                            if ((@$deluxeForbbidenP->isProductAllowed($id_customer, $row2['id_product'], $row2['id_category_default'], $row2['id_manufacturer'], $row2['id_supplier'], $id_default_group) || !$deluxeForbbidenP->compruebaSiEsProductoProhibido($row2['id_product'], $forbidden_products))) {
                                $results_array[] = $row2;
                            }
                        }
                    }
                }
            }
            return $results_array;
        } else if ((bool) Module::isEnabled('deluxeprivatetocategory') == true &&
                (bool) Module::isEnabled('idxforbiddenproducts') == false) {
            $deluxePrivateCategories = Module::getInstanceByName('deluxeprivatetocategory');
            $id_customer = Context::getContext()->customer->id;
            $forbidden_categories = $deluxePrivateCategories->getCategoriesByCustomer($id_customer);
            $results_array = array();
            if (is_array($query_result)) {
                foreach ($query_result as $value => $row) {
                    if ($row2 = Product::getProductProperties($id_lang, $row)) {
                        if ($deluxePrivateCategories->isCategoryAllowed($id_customer, $row2['id_category_default']) ||
                                !$deluxePrivateCategories->compruebaSiProductoNoAccesible(
                                    $forbidden_categories,
                                    self::getProductCategories($row2['id_product'])
                                )) {
                            $results_array[] = $row2;
                        }
                    }
                }
            }
            return $results_array;
        } else if ((bool) Module::isEnabled('idxforbiddenproducts') == true &&
                (bool) Module::isEnabled('deluxeprivatetocategory') == false) {
            $privateProductsEnabled = true;
            $deluxeForbbidenP = Module::getInstanceByName('idxforbiddenproducts');
            $id_customer = Context::getContext()->customer->id;
            $forbidden_products = $deluxeForbbidenP->getProductsByCustomer();
            $id_default_group = Context::getContext()->customer->id_default_group;
            $results_array = array();
            if (is_array($query_result)) {
                foreach ($query_result as $row) {
                    if ($row2 = Product::getProductProperties($id_lang, $row)) {
                        if ((@$deluxeForbbidenP->isProductAllowed(
                            $id_customer,
                            $row2['id_product'],
                            $row2['id_category_default'],
                            $row2['id_manufacturer'],
                            $row2['id_supplier'],
                            $id_default_group
                        ) || !$deluxeForbbidenP->compruebaSiEsProductoProhibido(
                            $row2['id_product'],
                            $forbidden_products
                        ))) {
                            $results_array[] = $row2;
                        }
                    }
                }
            }

            return $results_array;
        } else {
            return parent::getProductsProperties($id_lang, $query_result);
        }
    }
}
