<?php
/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * @author    Innova Deluxe SL
 * @copyright 2018 Innova Deluxe SL
 * @license   INNOVADELUXE
 */

class Search extends SearchCore
{
    public static function find($id_lang, $expr, $page_number = 1, $page_size = 1, $order_by = 'position', $order_way = 'desc', $ajax = false, $use_cookie = true, Context $context = null)
    {
        if ((bool) Module::isEnabled('idxforbiddenproducts') == true && $ajax) {
            $results = parent::find($id_lang, $expr, $page_number, $page_size, $order_by, $order_way, $ajax, $use_cookie, $context);
            $deluxeForbbidenP = Module::getInstanceByName('idxforbiddenproducts');
            $forbidden_products = $deluxeForbbidenP->getProductsByCustomer();
            $filter_results = array();
            foreach ($results as $product) {
                if (!in_array($product['id_product'], $forbidden_products)) {
                    $filter_results[] = $product;
                } else {
                    if ($deluxeForbbidenP->isProductAllowed(
                        Context::getContext()->customer->id,
                        $product['id_product'],
                        $product['id_category_default'],
                        $product['id_manufacturer'],
                        $product['id_supplier'],
                        Context::getContext()->customer->id_default_group
                    )) {
                        $filter_results[] = $product;
                    }
                }
            }
            return $filter_results;
        } else if ((bool) Module::isEnabled('idxforbiddenproducts') == true && !$ajax) {
            $results = parent::find($id_lang, $expr, $page_number, $page_size, $order_by, $order_way, $ajax, $use_cookie, $context);
            $filter_results = array();
            $filter_results['total'] = 0;
            $filter_results['result'] = array();
            $deluxeForbbidenP = Module::getInstanceByName('idxforbiddenproducts');
            $forbidden_products = $deluxeForbbidenP->getProductsByCustomer();
            $total = 0;
            if ($results) {
                foreach ($results['result'] as $product) {
                    if (!in_array($product['id_product'], $forbidden_products)) {
                        $filter_results['result'][] = $product;
                        $total++;
                    } else {
                        if ($deluxeForbbidenP->isProductAllowed(
                            Context::getContext()->customer->id,
                            $product['id_product'],
                            $product['id_category_default'],
                            $product['id_manufacturer'],
                            $product['id_supplier'],
                            Context::getContext()->customer->id_default_group
                        )) {
                            $filter_results['result'][] = $product;
                            $total++;
                        }
                    }
                    $filter_results['total'] = $total;
                }
            }
            return $filter_results;
        } else {
            return parent::find($id_lang, $expr, $page_number, $page_size, $order_by, $order_way, $ajax, $use_cookie, $context);
        }
    }
}
