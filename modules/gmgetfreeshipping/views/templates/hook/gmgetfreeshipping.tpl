{*
 * Get Free Shipping PrestaShop module.
 *
 * @package   gmgetfreeshipping
 * @author    Dariusz Tryba (contact@greenmousestudio.com)
 * @copyright Copyright (c) Green Mouse Studio (http://www.greenmousestudio.com)
 * @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *}
{if $remaining_to_spend > 0}
   <div class="block_get_free_shipping">
      <p><strong>
            {l s='Spend another' mod='gmgetfreeshipping'}
            {convertPrice price=$remaining_to_spend}
            {l s='to get free shipping for your order!' mod='gmgetfreeshipping'}
         </strong></p>
   </div>
{/if}