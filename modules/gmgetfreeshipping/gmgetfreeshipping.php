<?php
/**
 * Get Free Shipping PrestaShop module.
 *
 * @package   gmgetfreeshipping
 * @author    Dariusz Tryba (contact@greenmousestudio.com)
 * @copyright Copyright (c) Green Mouse Studio (http://www.greenmousestudio.com)
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

if (!defined('_PS_VERSION_')) exit;

class GMGetFreeShipping extends Module
{

    public function __construct()
    {
        $this->name = 'gmgetfreeshipping';
        $this->tab = 'front_office_features';
        $this->version = '1.0.1';
        $this->author = 'GreenMouseStudio.com';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);

        parent::__construct();

        $this->displayName = $this->l('Get free shipping');
        $this->description = $this->l('Display Spend another X to get free shipping message in cart');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        if (Shop::isFeatureActive()) Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install() ||
            !$this->registerHook('shoppingCart')
        ) return false;

        return true;
    }

    public function getContent() {
        return $this->context->smarty->fetch($this->local_path.'views/templates/admin/gms.tpl');
    }

    public function hookShoppingCart($params)
    {
        $free_shipping = Configuration::get('PS_SHIPPING_FREE_PRICE');
        if ($free_shipping) {
            $free_shipping_price = Tools::convertPrice($free_shipping);
            $cart = $this->context->cart;
            $total_without_shipping = $cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING);
            $remaining_to_spend = $free_shipping_price - $total_without_shipping;
            $this->context->smarty->assign(
                array(
                    'free_shipping_price' => $free_shipping_price,
                    'total_without_shipping' => $total_without_shipping,
                    'remaining_to_spend' => $remaining_to_spend
                )
            );
            return $this->display(__FILE__, 'gmgetfreeshipping.tpl');
        }
        return false;
    }
}
