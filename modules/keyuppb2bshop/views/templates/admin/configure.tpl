{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel logocontent">
    	<!--<h3><i class="icon icon-credit-card"></i> {l s='Keyupp' mod='keyuppb2bshop'}</h3>-->
        <div class="logokeyupp">
            <img src="{$module_dir}/views/img/logo-keyupp.png">
        </div>
		<p>
			<span>{l s='Keyupp B2B Shop' mod='keyuppb2bshop'}</span>
			{l s='Este módulo permite crear tu e-commerce en una plataforma B2B. Podrás tener tu tienda privada de tal manera que los clientes que se registren deberán ser validados por un administrador antes de que puedan iniciar sesión y también elegir qué contenido podrán visualizar.' mod='keyuppb2bshop'}<br />
			<br/>
			{l s='¡Gracias por confiar en Keyupp!' mod='keyuppb2bshop'}
		</p>
        <div class="logomodule">
    		<img src="{$module_dir}/views/img/logo-b2bshop.jpg">
   		</div>
</div>

<div class="panel">
	<h3><i class="icon icon-tags"></i> {l s='Documentation' mod='keyuppb2bshop'}</h3>
	<p>
		&raquo; {l s='Descarga la documentación para la configuración de este módulo en PDF.' mod='keyuppb2bshop'} :
		<ul>
			<li><a href="{$module_dir|escape:'htmlall':'UTF-8'}doc/es.pdf" target="_blank">{l s='Español' mod='keyuppb2bshop'}</a></li>
			<li><a href="{$module_dir|escape:'htmlall':'UTF-8'}doc/en.pdf" target="_blank">{l s='Ingles' mod='keyuppb2bshop'}</a></li>
		</ul>
	</p>
	<a class="btn btn-default" href="{$b2bLink|escape:'htmlall':'UTF-8'}">
		{l s='Configure!' mod='keyuppb2bshop'}
	</a>
</div>
