{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $popUpColor}
  <style>
      #dialogB2B .label-dialogB2B, #dialogB2B .ok-dialogB2B{
        background-color: {$popUpColor|escape:'htmlall':'UTF-8'} !important;
      }
  </style>
{/if}

<div id="wrapperB2B">
  <div id="overlayB2B">
    <div id="screenB2B"></div>
    <div id="dialogB2B" class="dialogB2B modal">
      <div class="label-dialogB2B">
        <i class="material-icons">shopping_cart</i>
      </div>
      <div class="body-dialogB2B">
        <p>{l s='This is a ' d='Modules.Keyuppb2bshop.Shop'}
          <span>{l s='B2B shop for professionals'  d='Modules.Keyuppb2bshop.Shop'}</span>
            .{l s='You can requesting an account in this form.' d='Modules.Keyuppb2bshop.Shop'}
        </p>
      </div>
      <div class="ok-dialogB2B">
        <i class="material-icons">done</i>
      </div>
    </div>
  </div>
</div>
