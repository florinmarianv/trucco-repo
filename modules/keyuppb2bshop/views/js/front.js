/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    keyapp
* @copyright keyapp
* @license   keyapp
*/

var modal = {};
modal.hide = function() {
  $('#wrapperB2B, #wrapperB2B .dialogB2B').fadeOut();
};

$(document).ready(function() {
  function show_popup(){
    $('#overlayB2B, #dialogB2B').fadeIn();
  };

  window.setTimeout( show_popup, 1500 );

  $('#wrapperB2B .ok-dialogB2B').click(function() {
    modal.hide();
  });

  $('#wrapperB2B .dialogB2B').click(function(event) {
    event.stopPropagation();
  });
});
