<?php
/**
* 2017 keyupp.
*
*  @author    keyupp <info@keyupp.com>
*  @copyright 2016 keyupp
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

require_once('AdminB2BModel.php');

class AdminB2BValidationPreferencesController extends AdminB2BModel
{
    public function __construct()
    {
        parent::__construct();
        $this->fieldsForm = array(
            $this->getGeneralConfigForm(),
            $this->getMainConfigForm()
        );
    }

    public function getGeneralConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Customer validation configuration'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable customer validation?'),
                        'name' => 'KUB2B_ENABLE_CUSTOMER_VALIDATION',
                        'is_bool' => true,
                        'hint' => array(
                            $this->l('Switch to enable customers validation.'),
                            $this->l('If this configuration is disabled, following options will not be applied.'),
                        ),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    public function getMainConfigForm()
    {
        return array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                'title' => $this->l('Customer validation Options'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'tags',
                        'label' => $this->l('Administrators email'),
                        'name' => 'KUB2B_ADMIN_EMAIL',
                        'hint' => array(
                            $this->l('Alerts of new customers registrations will be notified in this emails accounts.'),
                            $this->l('You\'ll be able to add the emails separated by commas.'),
                        ),
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Message to customers after they have been registered'),
                        'hint' => $this->l('This message will be displayed when customers have been registered'),
                        'name' => 'KUB2B_VALIDATION_MSG',
                        'lang' => true,
                        'autoload_rte' => true
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function getConfigForm()
    {
        $validation_msg = [];
        foreach ($this->context->controller->getLanguages() as $lang) {
            $validation_msg[$lang['id_lang']] = Configuration::get('KUB2B_VALIDATION_MSG', $lang['id_lang']);
        }

        return array(
            'KUB2B_ADMIN_EMAIL' => Configuration::get('KUB2B_ADMIN_EMAIL'),
            'KUB2B_ENABLE_CUSTOMER_VALIDATION' => Configuration::get('KUB2B_ENABLE_CUSTOMER_VALIDATION'),
            'KUB2B_VALIDATION_MSG' => $validation_msg
        );
    }

    public function postProcess()
    {
        if ((bool)Tools::isSubmit('submitSettings') == true) {
            $validation_msg = [];
            foreach ($this->context->controller->getLanguages() as $lang) {
                $validation_msg[$lang['id_lang']] = Tools::getValue('KUB2B_VALIDATION_MSG_'.$lang['id_lang']);
            }

            $validEmails = array_filter(array_map(
                function ($email) {
                    if (Validate::isEmail($email)) {
                        return $email;
                    } else {
                        $this->warnings[] = $this->l('Invalid email account: ') . $email;
                    }
                },
                explode(',', Tools::getValue('KUB2B_ADMIN_EMAIL'))
            ));

            Configuration::updateValue('KUB2B_VALIDATION_MSG', $validation_msg, true);
            Configuration::updateValue('KUB2B_ADMIN_EMAIL', implode(',', $validEmails));
            Configuration::updateValue('KUB2B_ENABLE_CUSTOMER_VALIDATION', Tools::getValue('KUB2B_ENABLE_CUSTOMER_VALIDATION'));
            $this->confirmations[] = $this->l('The settings have been successfully updated');
        }
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
    }
}
