<?php
/**
* 2016 keyupp.
*
*  @author    keyupp <info@keyupp.com>
*  @copyright 2016 keyupp
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class AdminB2BValidationCustomersController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->className = 'Customer';
        $this->table = 'customer';
        $this->_where = 'AND a.active = 0';
        parent::__construct();

        $this->fields_list = array(
            'firstname' => array(
                'title' => $this->l('Firstname'),
            ),
            'lastname' => array(
                'title' => $this->l('Lastname'),
            ),
            'email' => array(
                'title' => $this->l('Customer email'),
            ),
            'active' => array(
                'title' => $this->l('Enable customer'),
                'align' => 'center',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false,
                'class' => 'fixed-width-sm',
            ),
        );

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash',
            ),
        );

        $this->list_no_link = true;
        $this->informations[] = $this->l('Confirmation email will be sent to customers if you click on Enable Customer icon');

        $this->warnings[] = $this->l('Emails to customers option only will works if you have enabled Customer Validation option ');
    }

    public function renderList()
    {
        $this->addRowAction('view');
        $this->addRowAction('delete');
        return parent::renderList();
    }

    public function initToolbar()
    {
        return false;
    }

    public function postProcess()
    {
        parent::postProcess();
        if ($this->display === 'view') {
            $link = Context::getContext()->link;
            $id_customer = Tools::getValue('id_customer');
            Tools::redirectAdmin(
                $link->getAdminLink('AdminCustomers', true).'&viewcustomer&id_customer='.(int)$id_customer
            );
        }
    }

    public function processStatus()
    {
        $customer = parent::processStatus();
        if (Validate::isLoadedObject($customer)) {
            if ((bool)$customer->active == true) {
                if (Configuration::get('KUB2B_CUSTOMER_VALIDATION_ACTIVE')) {
                    $index = Context::getContext()->link->getPageLink(
                        'index',
                        true,
                        $this->context->language->id,
                        null,
                        false
                    );

                    $mail_vars = array(
                        '{firstname}' => $customer->firstname,
                        '{lastname}' => $customer->lastname,
                        '{shop_url}' => $index,
                        '{shop_name}' => Context::getContext()->shop->name,
                    );

                    Mail::Send(
                        Context::getContext()->language->id,
                        'activation-template',
                        $this->l('Your account have been enabled.'),
                        $mail_vars,
                        $customer->email,
                        null,
                        null,
                        null,
                        null,
                        null,
                        dirname(__FILE__) . '/../../mails/',
                        false,
                        Context::getContext()->shop->id
                    );
                }
            }
        }
    }

    protected function getConfigForm()
    {
    }
}
