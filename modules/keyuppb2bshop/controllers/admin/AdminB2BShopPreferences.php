<?php
/**
* 2016 keyupp.
*
*  @author    keyupp <info@keyupp.com>
*  @copyright 2016 keyupp
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

require_once('AdminB2BModel.php');

class AdminB2BShopPreferencesController extends AdminB2BModel
{
    public function __construct()
    {
        parent::__construct();
        $this->fieldsForm = array(
            $this->getGeneralConfigForm(),
            $this->getMainConfigForm()
        );
    }

    public function getGeneralConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('B2B Private shop configuration'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('B2B mode'),
                        'name' => 'KUB2B_ENABLE_B2B',
                        'is_bool' => true,
                        'hint' => array(
                            $this->l('Switch to enable private shop configuration.'),
                            $this->l('If this configuration is disabled, following options will not be applied.'),
                        ),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    public function getMainConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('B2B Private shop configuration'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow contact form?'),
                        'name' => 'KUB2B_ALLOWED_CONTACT_FORM',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow password reminder?'),
                        'name' => 'KUB2B_ALLOWED_PASS_REM',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'name' => 'KUB2B_ALLOWED_CMS[]',
                        'label' => $this->l('Allowed CMS pages'),
                        'options' => array(
                            'query' => $this->getCMSOptions(),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'multiple' => true,
                        'class' => 'chosen'
                    ),
                    array(
                        'type' => 'color',
                        'label' => $this->l('Popup box color'),
                        'name' => 'KUB2B_POPUP_COLOR',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function getCMSOptions()
    {
        $psCms = CMS::listCMS(Context::getContext()->language->id);
        $return = [];
        foreach ($psCms as $cms) {
            $return[] = array(
                    "id" => $cms['id_cms'],
                    "name" => $cms['meta_title'],
                    "val" => $cms['id_cms']
            );
        }
        return $return;
    }

    public function postProcess()
    {
        if ((bool)Tools::isSubmit('submitSettings')) {
            Configuration::updateValue('KUB2B_ENABLE_B2B', Tools::getValue('KUB2B_ENABLE_B2B'));
            Configuration::updateValue('KUB2B_ALLOWED_PASS_REM', Tools::getValue('KUB2B_ALLOWED_PASS_REM'));
            Configuration::updateValue('KUB2B_ALLOWED_CONTACT_FORM', Tools::getValue('KUB2B_ALLOWED_CONTACT_FORM'));
            Configuration::updateValue('KUB2B_POPUP_COLOR', Tools::getValue('KUB2B_POPUP_COLOR'));
            $cms = Tools::getValue('KUB2B_ALLOWED_CMS');
            ($cms && sizeof($cms) > 0)?$cms = implode(',', $cms):$cms='';
            Configuration::updateValue('KUB2B_ALLOWED_CMS', $cms);
            $this->confirmations[] = $this->l('The settings have been succesfully updated.');
            return true;
        }
    }

    protected function getConfigForm()
    {
        return array(
            'KUB2B_ALLOWED_CONTACT_FORM' => Configuration::get('KUB2B_ALLOWED_CONTACT_FORM'),
            'KUB2B_ALLOWED_PASS_REM' => Configuration::get('KUB2B_ALLOWED_PASS_REM'),
            'KUB2B_ENABLE_B2B' => Configuration::get('KUB2B_ENABLE_B2B'),
            'KUB2B_POPUP_COLOR' => Configuration::get('KUB2B_POPUP_COLOR'),
            'KUB2B_ALLOWED_CMS[]' => explode(',', Configuration::get('KUB2B_ALLOWED_CMS'))
        );
    }
}
