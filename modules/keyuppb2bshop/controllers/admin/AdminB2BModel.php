<?php
/**
* 2016 keyupp.
*
*  @author    keyupp <info@keyupp.com>
*  @copyright 2016 keyupp
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

abstract class AdminB2BModel extends ModuleAdminController
{
    protected $tplConfig = 'config.tpl';
    protected $fieldsForm = array();
    
    const MODULE_DIRECTORY = 'keyuppb2bshop/';

    public function __construct()
    {
        $this->bootstrap = true;
        parent::__construct();
    }

    public function setMedia()
    {
        parent::setMedia();
    }

    public function initContent()
    {
        parent::initContent();

        $smarty = $this->context->smarty;

        $smarty->assign(array('form' => $this->renderForm()));
        $content = $smarty->fetch(
            _PS_MODULE_DIR_ . self::MODULE_DIRECTORY .'views/templates/admin/controllers/'.$this->tplConfig
        );

        $smarty->assign(array(
            'content' => $this->content . $content
        ));
    }

    public function renderForm()
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->default_form_language = Context::getContext()->language->id;
        $helper->submit_action = 'submitSettings';
        $helper->name_controller = '';
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigForm(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );
        return $helper->generateForm($this->fieldsForm);
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitSettings')) {
            $form_values = $this->getConfigForm();

            foreach (array_keys($form_values) as $key) {
                Configuration::updateValue($key, Tools::getValue($key));
            }
        }
    }

    abstract protected function getConfigForm();
}
