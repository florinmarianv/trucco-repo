<?php
/**
* 2017 keyupp.
*
*  @author    keyupp <info@keyupp.com>
*  @copyright 2016 keyupp
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class KeyuppB2bshopValidationModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        if (!Module::isEnabled('keyuppb2bshop')) {
            Tools::redirect('index');
        }

        $this->display_column_left = false;
        $this->display_column_right = false;
        parent::initContent();
        $this->context->smarty->assign(array(
            'validate_msg' => Configuration::get(
                'KUB2B_VALIDATION_MSG',
                $this->context->language->id
            ),
        ));

        $this->setTemplate('module:keyuppb2bshop/views/templates/front/validation.tpl');
    }
}
