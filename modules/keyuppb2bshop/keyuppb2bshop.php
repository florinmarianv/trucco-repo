<?php
/**
* 2017 keyupp.
*
*  @author    keyupp <info@keyupp.com>
*  @copyright 2017 keyupp
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class KeyuppB2bShop extends Module implements WidgetInterface
{
    public $ps_languages = array();
    protected $admin_controllers = array();
    protected static $module_hooks = array(
        'displayHeader',
        'displayFooter',
        'actionCustomerAccountAdd',
        'displayBackOfficeHeader'
    );

    public function __construct()
    {
        $this->name = 'keyuppb2bshop';
        $this->tab = 'administration';
        $this->version = '1.0.2';
        $this->author = 'Keyupp';
        $this->need_instance = 1;
        $this->bootstrap = true;
        $this->controllers = array('validation');
        $this->module_key = '53cdc88f0d9514949d06f48fa26e91e5';
        $this->templateFile = 'module:keyuppb2bshop/views/templates/front/alertb2bpage.tpl';

        parent::__construct();

        $this->ps_languages = Language::getLanguages(false);
        $this->displayName = $this->l('B2B Shop');
        $this->description = $this->l('Turn into your shop on B2B proffesional shop');
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->admin_controllers = array(
            'AdminB2BShop' => ['name' => 'Manage B2B', 'id_parent' => 0],
            'AdminB2BShopPreferences' => ['name' => 'B2B Preferences', 'id_parent' => 0],
            'AdminB2BValidationPreferences' => ['name' => 'Manage validation preferences', 'id_parent' => 0],
            'AdminB2BValidationCustomers' => ['name' => 'Manage customers validation', 'id_parent' => 0],
        );
    }

    public function install()
    {
        $validate_msg = array();
        foreach ($this->ps_languages as $language) {
            $validate_msg[$language['id_lang']] = '
                Your account must be approved by an admin before you can login.';
        }

        Configuration::updateValue('KUB2B_ENABLE_B2B', 1);
        Configuration::updateValue('KUB2B_ALLOWED_CONTACT_FORM', 1);
        Configuration::updateValue('KUB2B_ALLOWED_PASS_REM', 1);
        Configuration::updateValue('KUB2B_ENABLE_CUSTOMER_VALIDATION', 1);
        Configuration::updateValue('KUB2B_ALLOWED_CMS', 0);
        Configuration::updateValue('KUB2B_CUSTOMER_VALIDATION_ACTIVE', 1);
        Configuration::updateValue('KUB2B_VALIDATION_MSG', $validate_msg);
        Configuration::updateValue('KUB2B_POPUP_COLOR', '#2fb5d2');
        Configuration::updateValue(
            'KUB2B_ADMIN_EMAIL',
            Configuration::get('PS_SHOP_EMAIL')
        );

        return parent::install() &&
            $this->installAdminTabs() &&
            $this->registerHook(KeyuppB2bShop::$module_hooks);
    }

    protected function installAdminTabs()
    {
        $has_been_installed = true;
        $parent = 0;
        foreach ($this->admin_controllers as $class_name => $controller) {
            $tab = new Tab();
            $tab->active = 1;
            $tab->class_name = $class_name;
            $tab->module = $this->name;
            $tab->id_parent = $parent;
            foreach ($this->ps_languages as $lang) {
                $tab->name[$lang['id_lang']] = $this->getTabNameByIsoCodeLang($class_name, $lang['iso_code']);
            }

            $has_been_installed &= (bool)$tab->add();
            if ($class_name === 'AdminB2BShop') {
                $parent = $tab->id;
            }
        }

        return $has_been_installed;
    }

    public function uninstall()
    {
        foreach ($this->admin_controllers as $class_name => $controller) {
            $tab = new Tab(Tab::getIdFromClassName($class_name));
            $tab->delete();
            unset($tab);
        }

        Configuration::deleteByName('KUB2B_ENABLE_B2B');
        Configuration::deleteByName('KUB2B_ALLOWED_CONTACT_FORM');
        Configuration::deleteByName('KUB2B_ALLOWED_PASS_REM');
        Configuration::deleteByName('KUB2B_ALLOWED_CMS');
        Configuration::deleteByName('KUB2B_CUSTOMER_VALIDATION_ACTIVE');
        Configuration::deleteByName('KUB2B_VALIDATION_MSG');
        Configuration::deleteByName('KUB2B_ADMIN_EMAIL');
        Configuration::deleteByName('KUB2B_ENABLE_CUSTOMER_VALIDATION');
        Configuration::deleteByName('KUB2B_POPUP_COLOR');

        return parent::uninstall();
    }

    public function getContent()
    {
        $this->context->smarty->assign(array(
          'module_dir' => $this->_path,
          'b2bLink' => $this->context->link->getAdminLink('AdminB2BShopPreferences', true)
        ));

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
        return $output;
    }

    public function hookDisplayHeader()
    {
        if (Configuration::get('KUB2B_ENABLE_B2B')) {
            if (!$this->context->customer->isLogged()) {
                if (!(Tools::getValue('controller') == 'authentication' ||
                        Tools::getValue('controller') == 'validation' ||
                        (Configuration::get('KUB2B_ALLOWED_CONTACT_FORM') &&
                        Tools::getValue('controller') == 'contact') ||
                        (Configuration::get('KUB2B_ENABLE_CUSTOMER_VALIDATION')) &&
                        Tools::getValue('create_account') == 1 ||
                        (Configuration::get('KUB2B_ALLOWED_PASS_REM') &&
                        Tools::getValue('controller') == "password") ||
                        (Tools::getValue('controller') == 'cms' &&
                        $this->isCmsAllowed(Tools::getValue('id_cms'))))) {
                    Tools::redirect($this->context->link->getPageLink('authentication', array('back'=>'my-account')));
                }
            }

            $this->context->controller->registerStylesheet(
                'modules-keyuppb2bshop',
                'modules/'.$this->name.'/views/css/front.css',
                ['media' => 'all', 'priority' => 150]
            );

            $this->context->controller->registerJavascript(
                'modules-keyuppb2bshop',
                'modules/'.$this->name.'/views/js/front.js',
                ['position' => 'bottom', 'priority' => 150]
            );
        }
    }

    public function renderWidget($hookName, array $configuration)
    {
        if ($this->context->controller->php_self === 'authentication' && !Tools::getValue('create_account')) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
            return $this->fetch($this->templateFile);
        }

        return "";
    }

    public function getWidgetVariables($hookName, array $configuration)
    {
        return array(
            'validate_users' => Configuration::get('KUB2B_CUSTOMER_VALIDATION_ACTIVE'),
            'popUpColor' => Configuration::get('KUB2B_POPUP_COLOR')
        );
    }

    public function hookActionCustomerAccountAdd($params)
    {
        if (Configuration::get('KUB2B_CUSTOMER_VALIDATION_ACTIVE')) {
            $customer = new Customer($this->context->customer->id);
            $this->context->customer->mylogout();
            $customer->active = 0;
            $customer->update();
            $id_lang = $this->context->language->id;
            $mail_vars = array(
                '{firstname}' => $customer->firstname,
                '{lastname}' => $customer->lastname,
                '{shop_url}' => $this->context->link->getPageLink('index', true, $id_lang, null, false),
                '{shop_name}' => $this->context->shop->name,
            );

            $admin_email = Configuration::get('PS_SHOP_EMAIL');
            if (Configuration::get('KUB2B_ADMIN_EMAIL') && null !== Configuration::get('KUB2B_ADMIN_EMAIL')) {
                $admin_email = explode(',', Configuration::get('KUB2B_ADMIN_EMAIL'));
            }

            Mail::Send(
                $this->context->language->id,
                'account',
                $this->l('You have registered our shops.'),
                $mail_vars,
                Tools::getValue('email'),
                null,
                null,
                null,
                null,
                null,
                dirname(__FILE__) . '/mails/',
                false,
                $this->context->shop->id
            );

            Mail::Send(
                $this->context->language->id,
                'new-user',
                $this->l('New user to validate.'),
                array(),
                $admin_email,
                null,
                null,
                null,
                null,
                null,
                dirname(__FILE__) . '/mails/',
                false,
                $this->context->shop->id
            );

            Tools::redirect($this->context->link->getModuleLink('keyuppb2bshop', 'validation'));
        }
    }

    public function hookDisplayBackofficeHeader()
    {
        if (Tools::getValue('configure') == $this->name) {
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    public function isCmsAllowed($id_cms)
    {
        $cms_allowed = explode(',', Configuration::get('KUB2B_ALLOWED_CMS'));
        return in_array($id_cms, $cms_allowed);
    }

    public function getTabNameByIsoCodeLang($controllerName, $iso_code)
    {
        $names = array(
            'AdminB2BShop' => array(
                'en' => 'Manage B2B',
                'es' => "Gestión de tiendas B2B"
            ),
            'AdminB2BShopPreferences' => array(
                'en' => 'B2B Preferences',
                'es' => 'Tienda privada B2B'
            ),
            'AdminB2BValidationPreferences' => array(
                'en' => 'Manage validation preferences',
                'es' => 'Gestión validación de usuarios'
            ),
            'AdminB2BValidationCustomers' => array(
                'en' => 'Manage customers validation',
                'es' => 'Clientes para validar'
            ),
        );

        if (array_key_exists($iso_code, $names[$controllerName])) {
            return $names[$controllerName][$iso_code];
        }

        return $names[$controllerName]['en'];
    }
}
