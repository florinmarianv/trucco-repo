<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

use PrestaShop\PrestaShop\Adapter\ObjectPresenter;
use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Tdkblockinfo extends Module implements WidgetInterface
{

    public function __construct()
    {
        $this->name = 'tdkblockinfo';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'TDK Studio';
        $this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('TDK Block Info');
        $this->description = $this->l('Adds a block allowing customers to select a language, currency, my account... for your stores.');
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        // $this->registerHook('actionAdminControllerSetMedia');
    }

    public function install()
    {
        if (!parent::install() || !$this->registerTDKHook() || !Configuration::updateValue('TDK_BLOCKINFO_USEINFO', 0)) {
            return false;
        }
        $this->_installTDKDataSample();
        Configuration::updateValue('TDK_INSTALLED_TDKBLOCKINFO', '1');
        return true;
    }
    
    public function uninstall()
    {
        return Configuration::deleteByName('TDK_BLOCKINFO_USEINFO') &&
            parent::uninstall();
    }

    private function _installTDKDataSample()
    {
        if (!file_exists(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php')) {
            return false;
        }
        require_once(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php');

        $sample = new TDKDatasample(1);
		Configuration::updateValue('TDK_TEST_A100____', Configuration::get('TDK_TEST_A100____').$this->name.'--'.date('Y-m-d H:i:s').'/');
		if ($sample->processImport($this->name)) {
			//TDK:: fix for case can not install sample when install theme (can not get theme name to find directory folder)
			Configuration::updateValue('TDK_INSTALLED_SAMPLE_TDKBLOCKINFO', 1);
			return true;
		} else {
			return false;
		}
    }

    public function getWidgetVariables($hookName, array $params)
    {
        $languages = Language::getLanguages(true, $this->context->shop->id);
        $context = $this->context;

        foreach ($languages as &$lang) {
            $lang['name_simple'] = $this->getNameSimple($lang['name']);
        }
        
        if (!count($languages)) {
            return false;
        }
 
        if (Configuration::get('PS_RESTRICT_DELIVERED_COUNTRIES')) {
            $countries = Carrier::getDeliveredCountries($context->language->id, true, true);
        } else {
            $countries = Country::getCountries($context->language->id, true);
        }
        
        $current_currency = null;
        $serializer = new ObjectPresenter;
        $currencies = array_map(
            function ($currency) use ($serializer, $context, &$current_currency) {
                $currencyArray = $serializer->present($currency);
                
                $currencyArray['sign'] = $currency->sign;

                $url = $context->link->getLanguageLink($context->language->id);

                $extraParams = array(
                    'SubmitCurrency' => 1,
                    'id_currency' => $currency->id
                );

                $partialQueryString = http_build_query($extraParams);
                $separator = empty(parse_url($url)['query']) ? '?' : '&';

                $url .= $separator . $partialQueryString;

                $currencyArray['url'] = $url;

                if ($currency->id === $context->currency->id) {
                    $currencyArray['current'] = true;
                    $current_currency = $currencyArray;
                } else {
                    $currencyArray['current'] = false;
                }

                return $currencyArray;
            },
            Currency::getCurrencies(true, true)
        );
        
        $list_variable = array();
        $list_variable['lang_iso'] = $this->context->language->iso_code;
        $list_variable['countries'] = $countries;
        $list_variable['img_lang_url'] = _THEME_LANG_DIR_;
        $list_variable['currencies'] = $currencies;
        $list_variable['current_currency'] = $current_currency;
        $list_variable['cookie'] = $this->context->cookie;
        $list_variable['languages'] = $languages;
        $list_variable['current_language'] = array(
            'id_lang' => $this->context->language->id,
            'name' => $this->context->language->name,
            'name_simple' => $this->getNameSimple($this->context->language->name)
        );
        $list_variable['blockcurrencies_sign'] = $this->context->currency->sign;
        $list_variable['catalog_mode'] = Configuration::get('PS_CATALOG_MODE');
        
        //TDK:: add parameters for user info
        $enable_userinfo = Configuration::get('TDK_BLOCKINFO_USEINFO');
        $list_variable['enable_userinfo'] = $enable_userinfo;
        if ($enable_userinfo == 1) {
            $logged = $this->context->customer->isLogged();

            if ($logged) {
                $customerName = $this->context->customer->firstname.' '.$this->context->customer->lastname;
            } else {
                $customerName = '';
            }

            $link = $this->context->link;
            
            $list_variable['logged'] = $logged;
            $list_variable['customerName'] = $customerName;
            $list_variable['logout_url'] = $link->getPageLink('index', true, null, 'mylogout');
            $list_variable['my_account_url'] = $link->getPageLink('my-account', true);
        }
        return $list_variable;
    }
    
    private function getNameSimple($name)
    {
        return preg_replace('/\s\(.*\)$/', '', $name);
    }

    public function renderWidget($hookName, array $params)
    {
       
        $this->smarty->assign($this->getWidgetVariables($hookName, $params));
        return $this->fetch('module:tdkblockinfo/views/templates/hook/tdkblockinfo.tpl');
    }
    
    public function getContent()
    {
        $output = '';
        if (Tools::isSubmit('submitTDKBlockInfo')) {
            Configuration::updateValue('TDK_BLOCKINFO_USEINFO', (int)(Tools::getValue('TDK_BLOCKINFO_USEINFO')));
            $output .= $this->displayConfirmation($this->l('Settings updated.'));
        }
        return $this->renderForm();
    }
    
    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Block User Info'),
                        'name' => 'TDK_BLOCKINFO_USEINFO',
                        'is_bool' => true,
                        'desc' => $this->l('Enable/Disable Block User Info.'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table =  $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitTDKBlockInfo';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab
        .'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'TDK_BLOCKINFO_USEINFO' => (bool)Tools::getValue('TDK_BLOCKINFO_USEINFO', Configuration::get('TDK_BLOCKINFO_USEINFO')),
        );
    }

    public function hookDisplayHeader($params)
    {
        $this->context->controller->registerStylesheet('modules-tdkblockinfo-tdkblockinfo', 'modules/tdkblockinfo/views/css/tdkblockinfo.css', array('media' => 'all', 'priority' => 150));
        $this->context->controller->registerJavascript('modules-tdkblockinfo-tdkblockinfo', 'modules/tdkblockinfo/views/js/tdkblockinfo.js', array('position' => 'bottom', 'priority' => 150));
    }
    /**
     * FIX BUG 1.7.3.3 : install theme lose hook displayHome, displayTdkProfileProduct
     * because ajax not run hookActionAdminBefore();
     */
    public function autoRestoreSampleData()
    {
        if (Hook::isModuleRegisteredOnHook($this, 'actionAdminBefore', (int)Context::getContext()->shop->id)) {
            $theme_manager = new stdclass();
            $theme_manager->theme_manager = 'theme_manager';
            $this->hookActionAdminBefore(array(
                'controller' => $theme_manager,
            ));
        }
    }
    
    /**
     * TDK Common method
     * TDK Resgister all hook for module
     */
    public function registerTDKHook()
    {
        $res = true;
        $res &= $this->registerHook('displayHeader');
        $res &= $this->registerHook('displayNav2');
        $res &= $this->registerHook('actionAdminControllerSetMedia');
                               
        return $res;
    }
    /**
     * Run only one when install/change Theme_of_Tdk Studio
     */
    public function hookActionAdminBefore($params)
    {
        $this->unregisterHook('actionAdminBefore');
        if (isset($params) && isset($params['controller']) && isset($params['controller']->theme_manager)) {
            // Validate : call hook from theme_manager
        } else {
            // Other module call this hook -> duplicate data
            return;
        }
        
        # FIX : update Prestashop by 1-Click module -> NOT NEED RESTORE DATABASE
        $tdk_version = Configuration::get('TDK_CURRENT_VERSION');
        if ($tdk_version != false) {
            $ps_version = Configuration::get('PS_VERSION_DB');
            $versionCompare =  version_compare($tdk_version, $ps_version);
            if ($versionCompare != 0) {
                // Just update Prestashop
                Configuration::updateValue('TDK_CURRENT_VERSION', $ps_version);
                return;
            }
        }
        
        # WHENE INSTALL THEME, INSERT HOOK FROM DATASAMPLE IN THEME
        $hook_from_theme = false;
        if (file_exists(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php')) {
            require_once(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php');
            $sample = new TDKDataSample();
            if ($sample->processHook($this->name)) {
                $hook_from_theme = true;
            };
        }
        
        # INSERT HOOK FROM MODULE_DATASAMPLE
        if ($hook_from_theme == false) {
            $this->registerTdkHook();
        }
        
        # WHEN INSTALL MODULE, NOT NEED RESTORE DATABASE IN THEME
        $install_module = (int)Configuration::get('TDK_INSTALLED_TDKBLOCKINFO', 0);
        if ($install_module) {
            Configuration::updateValue('TDK_INSTALLED_TDKBLOCKINFO', '0');    // next : allow restore sample
			//TDK:: fix for case can not install sample when install theme (can not get theme name to find directory folder)
            if ((int)Configuration::get('TDK_INSTALLED_SAMPLE_TDKBLOCKINFO', 0)) {				
				return;
			}
        }
        
        # INSERT DATABASE FROM THEME_DATASAMPLE
        if (file_exists(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php')) {
            require_once(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php');
            $sample = new TDKDataSample();
            $sample->processImport($this->name);
        }
    }
    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookActionAdminControllerSetMedia()
    {
        $this->autoRestoreSampleData();
    }
}
