<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_2d58ea6eb7349c439f770f335f085a48'] = 'TDK Block Info';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_2a17e6edf00a51b29bf95c84ffa02fdd'] = 'Aggiunge un blocco che consente ai clienti di selezionare una lingua, valuta, il mio account ... per i tuoi negozi.';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_f38f5974cdc23279ffe6d203641a8bdf'] = 'Impostazioni aggiornate.';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_f4f70727dc34561dfde1a3c529b6205c'] = 'Impostazioni';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_27e86e8ba547905b1e374817add18330'] = 'Info Blocca utente';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_0105bbbc3f8d8fa1c91d26357e89d25b'] = 'Attiva / Disattiva Blocca utente Info.';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Abilitato';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_b9f5c797ebbf55adccdd8539a65a0241'] = 'Disabilitato';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_c9cc8cce247e49bae79f15173ce97354'] = 'Salvare';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_51ac4bf63a0c6a9cefa7ba69b4154ef1'] = 'Ambiente';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_4994a8ffeba4ac3140beb89e8d41f174'] = 'Lingua';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_386c339d37e737a436499d423a77df0c'] = 'Moneta';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_2cbfb6731610056e1d0aaacde07096c1'] = 'Visualizza il mio conto cliente';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_8b1a9953c4611296a827abf8c47804d7'] = 'Ciao';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_c87aacf5673fada1108c9f809d354311'] = 'Disconnessione';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_d4151a9a3959bdd43690735737034f27'] = 'Accedi al tuo conto cliente';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Registrati';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Il mio account';
$_MODULE['<{tdkblockinfo}prestashop>tdkblockinfo_6ff063fbc860a79759a7369ac32cee22'] = 'Check-out';
