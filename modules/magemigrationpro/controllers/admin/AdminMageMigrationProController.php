<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MigrationPro
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@migration-pro.com
 *
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe MigrationPro
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la MigrationPro est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
 *
 * @author    MigrationPro
 * @copyright Copyright (c) 2012-2020 MigrationPro
 * @license   Commercial license
 * @package   MigrationPro: Magento To PrestaShop Migration Tool
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/loggers/MageMigrationProDBWarningLogger.php');
require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/loggers/MageMigrationProDBErrorLogger.php');

class AdminMageMigrationProController extends AdminController
{
    // --- response vars:

    public $errors = array();
    protected $response;

    // --- request vars:

    protected $stepNumber;
    protected $toStepNumber;

    // -- dynamic vars:

    protected $forceIds = true;
    protected $forceCategoryIds = false;
    protected $forceProductIds = false;
    protected $forceCustomerIds = false;
    protected $forceOrderIds = false;
    protected $forceManufacturerIds = false;
    protected $ps_validation_errors = true;

    protected $truncate = false;

    // --- source cart vars:

    protected $url_cart;
    protected $server_path = '/migration_pro/server.php';
    protected $token_cart;
    protected $cms;
    protected $image_category;
    protected $image_product;
    protected $image_manufacturer;
    protected $image_supplier;
    protected $table_prefix;
    protected $version;
    protected $charset;
    protected $blowfish_key;
    protected $cookie_key;
    protected $mapping;
    protected $languagesForQuery;
    protected $speed;

    // --- helper objects

    protected $query;
    protected $client;

    public function __construct()
    {
        $this->display = 'edit';
        parent::__construct();
        $this->controller_type = 'moduleadmin'; //instead of AdminController’s admin
        $tab = new Tab($this->id); // an instance with your tab is created; if the tab is not attached to the module, the exception will be thrown
        if (!$tab->module) {
            throw new PrestaShopException('Admin tab ' . get_class($this) . ' is not a module tab');
        }
        $this->module = Module::getInstanceByName($tab->module);
        if (!$this->module->id) {
            throw new PrestaShopException("Module {$tab->module} not found");
        }
        $this->tabAccess = Profile::getProfileAccess($this->context->employee->id_profile, Tab::getIdFromClassName('AdminMageMigrationPro'));

        $this->stepNumber = (int)Tools::getValue('step_number');
        $this->toStepNumber = (int)Tools::getValue('to_step_number');

        $this->initParams();
        if ($this->stepNumber > 1 || empty($this->stepNumber)) {
            $this->mapping = MageMigrationProMapping::listMapping(true, true);

            if (isset($this->mapping['languages'])) {
                // -- unset language where value = 0
                if (($key = array_search(0, $this->mapping['languages'])) !== false) {
                    unset($this->mapping['languages'][$key]);
                }
                $keys = array_keys($this->mapping['languages']);
                $this->languagesForQuery = implode(',', $keys);
            }

            if (!empty($this->url_cart) && !empty($this->token_cart)) {
                $this->client = new MageClient($this->url_cart . $this->server_path, $this->token_cart);
                if (Configuration::get($this->module->name . '_debug_mode')) {
                    $this->client->debugOn();
                } else {
                    $this->client->debugOff();
                }
            }
            $this->query = new MageQuery();
            $this->query->setVersion($this->version);
            $this->query->setCart($this->cms);
            $this->query->setPrefix($this->table_prefix);
            $this->query->setLanguages($this->languagesForQuery);
            $this->query->setRowCount($this->speed);
            $this->query->setRecentData($this->recent_data);
        }

        if (Shop::getContext() == Shop::CONTEXT_ALL || Shop::getContext() == Shop::CONTEXT_GROUP) {
            Shop::setContext(Shop::CONTEXT_SHOP, Shop::getCompleteListOfShopsID()[0]);
        }
    }


    private function initHelperObjects()
    {
        $this->mapping = MageMigrationProMapping::listMapping(true, true);

        if (isset($this->mapping['languages'])) {
            // -- unset language where value = 0
            if (($key = array_search(0, $this->mapping['languages'])) !== false) {
                unset($this->mapping['languages'][$key]);
            }
            $keys = array_keys($this->mapping['languages']);
            $this->languagesForQuery = implode(',', $keys);
        }

        if (!empty($this->url_cart) && !empty($this->token_cart)) {
            $this->client = new MageClient($this->url_cart . $this->server_path, $this->token_cart);
            if (Configuration::get($this->module->name . '_debug_mode')) {
                $this->client->debugOn();
            } else {
                $this->client->debugOff();
            }
        }
        $this->query = new MageQuery();
        $this->query->setVersion($this->version);
        $this->query->setCart($this->cms);
        $this->query->setPrefix($this->table_prefix);
        $this->query->setLanguages($this->languagesForQuery);
        $this->query->setRowCount($this->speed);
        $this->query->setRecentData($this->recent_data);
    }

    // --- request processes
    public function postProcess()
    {
        parent::postProcess();
    }

    public function clearSmartyCache()
    {
        Tools::enableCache();
        Tools::clearCache($this->context->smarty);
        Tools::restoreCacheSettings();
    }

    public function ajaxProcessValidateStep()
    {
        $this->response = array('has_error' => false, 'has_warning' => false);

        if (!$this->tabAccess['edit']) {
            $this->errors[] = Tools::displayError('You do not have permission to use this wizard.');
        } else {
            // -- reset old data and response url to start new migration
            if (Tools::getIsset('resume')) {
                $this->response['step_form'] = $this->module->renderStepThree();
            } elseif (Tools::getValue('to_step_number') >= Tools::getValue('step_number')) {
                $this->validateStepFieldsValue();
            }
        }

        if (count($this->errors)) {
            $this->response['has_error'] = true;
            $this->response['errors'] = $this->errors;
        } else {
            $this->response['has_error'] = false;
            $this->response['errors'] = null;
        }
        if (count($this->warnings)) {
            $this->response['has_warning'] = true;
            $this->response['warnings'] = $this->warnings;
        } else {
            $this->response['has_warning'] = false;
            $this->response['warnings'] = null;
        }

        die(Tools::jsonEncode($this->response));
    }

    // --- validate functions

    private function validateStepFieldsValue()
    {
        if ($this->stepNumber == 1) {
            // validate url
            if (!Tools::getValue('source_shop_url')) {
                $this->errors[] = $this->l('The Url field is required.');
            } elseif (!Validate::isAbsoluteUrl(Tools::getValue('source_shop_url'))) {
                $this->errors[] = $this->l('Please enter a valid URL. Protocol is required (http://, https:// or ftp://)');
            }

            // validate token
            if (!Tools::getValue('source_shop_token')) {
                $this->errors[] = $this->l('The token field is required.');
            }

            if ($this->truncateModuleTables()) {
                $this->errors[] = $this->module->l('Permission access to truncate module tables is denied! Please contact with support team!');
            }

            if (empty($this->errors)) {
                $this->client = new MageClient(preg_replace('/\/migration_pro\/server\.php/', '', pSQL(Tools::getValue('source_shop_url'))) . $this->server_path, Tools::getValue('source_shop_token'));
                if (Configuration::get($this->module->name . '_debug_mode')) {
                    $this->client->debugOn();
                } else {
                    $this->client->debugOff();
                }
                if ($this->client->check()) {
                    $content = $this->client->getContent();
                    $this->saveParamsToConfiguration($content);
                    $this->initHelperObjects();
                    if ($this->requestToCartDetails()) {
                        $this->response['step_form'] = $this->module->renderStepTwo();
                    }
                } else {
                    $this->errors[] = Tools::displayError('Please check URL') . ' - ' . $this->client->getMessage();
                }
            }
        } elseif ($this->stepNumber == 2) {
            $maps = Tools::getValue('map');
            $languageSumValue = array_sum($maps['languages']);
            $languageDiffResArray = array_diff_assoc($maps['languages'], array_unique($maps['languages']));
            if (!($languageSumValue > 0 && empty($languageDiffResArray))) {
                $this->errors[] = $this->l('Select a different language for each source language.');
            }

            $shopSumValue = array_sum($maps['multi_shops']);
            $shopDiffResArray = array_diff_assoc($maps['multi_shops'], array_unique($maps['multi_shops']));
            if (!($shopSumValue > 0 && empty($shopDiffResArray))) {
                $this->errors[] = $this->l('Select a different shop for each source shop.');
            }

            if (empty($this->errors)) {
                $this->initHelperObjects();
                Configuration::updateValue($this->module->name . '_force_category_ids', (int)Tools::getValue('force_category_ids'));
                Configuration::updateValue($this->module->name . '_force_product_ids', (int)Tools::getValue('force_product_ids'));
                Configuration::updateValue($this->module->name . '_force_customer_ids', (int)Tools::getValue('force_customer_ids'));
                Configuration::updateValue($this->module->name . '_force_order_ids', (int)Tools::getValue('force_order_ids'));
                Configuration::updateValue($this->module->name . '_force_manufacturer_ids', (int)Tools::getValue('force_manufacturer_ids'));
                Configuration::updateValue($this->module->name . '_ps_validation_errors', Tools::getValue('ps_validation_errors'));
                Configuration::updateValue($this->module->name . '_clear_data', (int)Tools::getValue('clear_data'));
                Configuration::updateValue($this->module->name . '_migrate_recent_data', Tools::getValue('migrate_recent_data'));
                Configuration::updateValue($this->module->name . '_query_row_count', self::convertSpeedNameToNumeric(Tools::getValue('speed')));
                if ($this->createMapping($maps) && $this->createProcess()) {
                    $this->saveMappingValues(MageMigrationProMapping::listMapping(true, true));
                    // turn on allow html iframe on
                    if (!Configuration::get('PS_ALLOW_HTML_IFRAME')) {
                        Configuration::updateValue('PS_ALLOW_HTML_IFRAME', 1);
                        Configuration::updateValue($this->module->name . '_allow_html_iframe', 1);
                    }

                    $this->response['step_form'] = $this->module->renderStepThree();
                } else {
                    $this->errors[] = $this->l('Select a minimum of one data type to start migrating.');
                }
            }
        }
    }


    public function ajaxProcessClearCache()
    {
        if (Tools::getValue('clear_cache')) {
            ini_set('max_execution_time', 0);
            Tools::clearSmartyCache();
            Tools::clearXMLCache();
            Media::clearCache();
            Tools::generateIndex();
            Search::indexation(true);

            $this->response['has_error'] = false;
            $this->response['has_warning'] = false;
            if (count($this->errors)) {
                $this->response['has_error'] = true;
                $this->response['errors'] = $this->errors;
            }
            if (count($this->warnings)) {
                $this->response['has_warning'] = true;
                $this->response['warnings'] = $this->warnings;
            }

            die(Tools::jsonEncode($this->response));
        }
    }

    public function ajaxProcessDebugOn()
    {
        if (Tools::getValue('turn') == 1) {
            Configuration::updateValue($this->module->name . '_debug_mode', 1);
        } else {
            Configuration::updateValue($this->module->name . '_debug_mode', 0);
        }
    }

    public function ajaxProcessSpeedUp()
    {
        if (!empty(Tools::getValue('speed'))) {
            Configuration::updateValue($this->module->name . '_query_row_count', Tools::getValue('speed'));
        }
    }

    public function ajaxProcessImportProcess($die = true)
    {
        $this->response = array('has_error' => false, 'has_warning' => false);

        if (!$this->tabAccess['edit']) {
            $this->errors[] = Tools::displayError('You do not have permission to use this wizard.');
        } else {
            $activeProcess = MageMigrationProProcess::getActiveProcessObject();
            if (Validate::isLoadedObject($activeProcess)) {
                $this->query->setOffset($activeProcess->imported);
                if ($activeProcess->imported == 0) {
                    if ($this->truncate && !$this->truncateTables($activeProcess->type)) {
                        $this->errors[] = 'Can\'t clear current data on Target shop ' . Db::getInstance()->getMsgError();
                    }
                    $activeProcess->time_start = date('Y-m-d H:i:s', time());
                    $activeProcess->save();
                }


                if ($activeProcess->type == 'taxes') {
                    $this->importTaxes($activeProcess);
                    $this->clearSmartyCache();
                } elseif ($activeProcess->type == 'manufacturers') {
                    $this->importManufacturers($activeProcess);
                    $this->clearSmartyCache();
                } elseif ($activeProcess->type == 'categories') {
                    $this->importCategories($activeProcess);
                    $this->clearSmartyCache();
                } elseif ($activeProcess->type == 'products') {
                    $this->importProducts($activeProcess);
                    $this->clearSmartyCache();
                } elseif ($activeProcess->type == 'customers') {
                    $this->importCustomers($activeProcess);
                    $this->clearSmartyCache();
                } elseif ($activeProcess->type == 'orders') {
                    $this->importOrders($activeProcess);
                    $this->clearSmartyCache();
                }
            } else {
                die('no process.');
            }
            $this->response['percent'] = MageMigrationProProcess::calculateImportedDataPercent();
            if ($this->response['percent'] == 100) {
                // turn off allow html iframe feature
                if (Configuration::get($this->module->name . '_allow_html_iframe')) {
                    Configuration::updateValue('PS_ALLOW_HTML_IFRAME', 0, null, 0, 0);
                    Configuration::updateValue($this->module->name . '_allow_html_iframe', 0, null, 0, 0);
                }
            }
        }

        if (count($this->errors)) {
            $this->response['has_error'] = true;
            $this->response['errors'] = $this->errors;
        }
        if (count($this->warnings)) {
            $this->response['has_warning'] = true;
            $this->response['warnings'] = $this->warnings;
        }

        if ($die) {
            die(Tools::jsonEncode($this->response));
        }
    }

    // --- import functions

    private function importTaxes($process)
    {
        $this->client->setPostData($this->query->taxRulesGroup());
        if ($this->client->query()) {
            $taxRulesGroups = $this->client->getContent();
            $this->client->setPostData($this->query->taxSqlSecond(self::getCleanIDs($taxRulesGroups, 'id_tax_rules_group')));
            if ($this->client->query()) {
                $taxRules = $this->client->getContent();
                $this->client->serializeOn();
                $this->client->setPostData($this->query->taxSqlThird(self::getCleanIDs($taxRules, 'id_tax')));
                if ($this->client->query()) {
                    $taxes = $this->client->getContent();
                    $import = new MageImport($process, $this->version, $this->url_cart, $this->forceIds);
                    $import->setPsValidationErrors($this->ps_validation_errors);
                    $import->taxes($taxes, $taxRulesGroups, $taxRules);
                    $this->errors = $import->getErrorMsg();
                    $this->warnings = $import->getWarningMsg();
                    $this->response = $import->getResponse();
                } else {
                    $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                }
            } else {
                $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
            }
        } else {
            $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
        }
    }

    private function importManufacturers($process)
    {
        $productEntityTypeId = $this->getEntityTypeId('product');
        if ($productEntityTypeId === null) {
            return;
        }
        $this->query->entityTypeId = $productEntityTypeId;
        $this->client->setPostData($this->query->getAllAttributes());
        if ($this->client->query()) {
            $attributes = $this->client->getContent();
            $this->query->saveAttributeIds($attributes);
            $this->client->setPostData($this->query->getManufacturerAttributeId());
            if ($this->client->query()) {
                $manufacturerAttributeId = $this->client->getContent();
                $this->client->setPostData($this->query->getManufacturerAttributeOptionIds(self::getCleanIDs($manufacturerAttributeId, 'attribute_id')));
                if ($this->client->query()) {
                    $manufacturerOptionIds = $this->client->getContent();
                    $this->client->serializeOn();
                    $this->client->setPostData($this->query->getManufacturerShopLang(self::getCleanIDs($manufacturerOptionIds, 'option_id')));
                    if ($this->client->query()) {
                        $manufacturerShopLang = $this->client->getContent();
                        $import = new MageImport($process, $this->version, $this->url_cart, $this->forceManufacturerIds);
                        $import->setPsValidationErrors($this->ps_validation_errors);
                        $import->setImagePath($this->image_manufacturer);
                        $import->manufacturers($manufacturerShopLang);
                        $this->errors = $import->getErrorMsg();
                        $this->warnings = $import->getWarningMsg();
                        $this->response = $import->getResponse();
                    } else {
                        $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                    }
                } else {
                    $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                }
            } else {
                $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
            }
        } else {
            $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
        }
    }

    private function importCategories($process)
    {
        $categoryEntityTypeId = $this->getEntityTypeId('category');
        if ($categoryEntityTypeId === null) {
            return ;
        }
        $this->query->entityTypeId = $categoryEntityTypeId;
        $this->client->serializeOff();
        $this->client->setPostData($this->query->getAllAttributes());
        if ($this->client->query()) {
            $attributes = $this->client->getContent();
            $this->query->saveAttributeIds($attributes);
            $this->client->setPostData($this->query->parentCategoriesStoresWebsites());
            if ($this->client->query()) {
                $rootCategoriesStoresWebsites = $this->client->getContent();
                $this->client->setPostData($this->query->category());
                if ($this->client->query()) {
                    $categories = $this->client->getContent();
                    $this->client->serializeOn();
                    $this->client->setPostData($this->query->categorySqlSecond(self::getCleanIDs($categories, 'id_category')));
                    if ($this->client->query()) {
                        $categoriesAdditionalSecond = $this->client->getContent();

                        $import = new MageImport($process, $this->version, $this->url_cart, $this->forceCategoryIds, $this->client, $this->query);
                        $import->setPsValidationErrors($this->ps_validation_errors);
                        $import->setImagePath($this->image_category);

                        $import->categories($categories, $categoriesAdditionalSecond, $rootCategoriesStoresWebsites, false);
                        $this->errors = $import->getErrorMsg();
                        $this->warnings = $import->getWarningMsg();
                        $this->response = $import->getResponse();
                    }
                } else {
                    $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                }
            } else {
                $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
            }
        } else {
            $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
        }
    }

    private function importProducts($process)
    {
        $productEntityTypeId = $this->getEntityTypeId('product');
        if ($productEntityTypeId === null) {
            return;
        }
        $this->query->entityTypeId = $productEntityTypeId;
        $this->client->setPostData($this->query->getAllAttributes());
        if ($this->client->query()) {
            $attributes = $this->client->getContent();
            $this->query->saveAttributeIds($attributes);
//            $this->client->setPostData($this->query->childProducts());
//            if ($this->client->query()) {
//                $childProducts = $this->client->getContent();
//                $childProductIds = self::getCleanIDs($childProducts, 'product_id');
            $this->client->setPostData($this->query->products());
            if ($this->client->query()) {
                $products = $this->client->getContent();
                $productIds = self::getCleanIDs($products, 'entity_id');
//                    $this->client->setPostData($this->query->allProducts());
//                    if ($this->client->query()) {
//                        $allProducts = $this->client->getContent();
//                        $allProductsIds = self::getCleanIDs($allProducts, 'entity_id');
                $this->client->serializeOn();
                $this->client->setPostData($this->query->productSqlSecond($productIds));
                if ($this->client->query()) {
                    $productAdditionalSecond = $this->client->getContent();
                    $id_attribute_group = self::getCleanIDs($productAdditionalSecond['attribute_group'], 'attribute_id');
//                            $id_image = self::getCleanIDs($productAdditionalSecond['image'], 'value_id');
                    $id_tag = self::getCleanIDs($productAdditionalSecond['product_tag'], 'tag_id');
                    $attribute_ids = self::getCleanIDs($productAdditionalSecond['product_attribute_ids'], 'attribute_id');
                    $id_product_attributes = self::getCleanIDs($productAdditionalSecond['product_attribute'], 'id_product_attribute');
                    $default_attribute_set = self::getCleanIDs($productAdditionalSecond['default_attribute_set'], 'attribute_set_id');
                    $this->client->setPostData($this->query->productSqlThird($id_attribute_group, $productIds, $id_tag, $attribute_ids, $id_product_attributes, $default_attribute_set));
                    if ($this->client->query()) {
                        $productAdditionalThird = $this->client->getContent();
                        $this->client->serializeOff();
                        $default_attribute_group_ids = self::getCleanIDs($productAdditionalThird['default_attribute_groups'], 'attribute_group_id');
                        $this->client->setPostData($this->query->defaultAttributeIds($default_attribute_group_ids));
                        if ($this->client->query()) {
                            $defaultAttribute = $this->client->getContent();
                            $defaultAttributeIds = self::getCleanIDs($defaultAttribute, 'attribute_id');
                            $this->client->setPostData($this->query->customAttributeIds($defaultAttributeIds));
                            if ($this->client->query()) {
                                $customAttribute = $this->client->getContent();
                                $customAttributeIds = self::getCleanIDs($customAttribute, 'attribute_id');
                                $this->client->setPostData($this->query->allCustomAttributesOptionValues($customAttributeIds));
                                if ($this->client->query()) {
                                    $allCustomAttributeOptionsValues = $this->client->getContent();

                                    $import = new MageImport($process, $this->version, $this->url_cart, $this->forceProductIds);
                                    $import->setPsValidationErrors($this->ps_validation_errors);
                                    $import->setImagePath($this->image_product);
                                    $import->setImageSupplierPath($this->image_supplier);

                                    $import->products($products, $productAdditionalSecond, $productAdditionalThird, $customAttribute, $allCustomAttributeOptionsValues);
                                    $this->errors = $import->getErrorMsg();
                                    $this->warnings = $import->getWarningMsg();
                                    $this->response = $import->getResponse();
                                } else {
                                    $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                                }
                            } else {
                                $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                            }
                        } else {
                            $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                        }
//                            } else {
//                                $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
//                            }
                    } else {
                        $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                    }
                } else {
                    $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                }
            } else {
                $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
            }
//            } else {
//                $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
//            }
        } else {
            $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
        }
    }

    private function importCustomers($process)
    {
        $this->client->setPostData($this->query->getAllAttributes('1,2'));
        if ($this->client->query()) {
            $attributes = $this->client->getContent();
            $this->query->saveAttributeIds($attributes);
            $customerAddressAdditional = null;
            $customerAdditionalSecond = null;
            $this->client->setPostData($this->query->customers());
            $this->client->serializeOff();
            if ($this->client->query()) {
                $customers = $this->client->getContent();
                $id_customers = self::getCleanIDs($customers, 'id_customer');

                $this->client->setPostData($this->query->address($id_customers));
                if ($this->client->query()) {
                    $addresses = $this->client->getContent();
                    $id_address = self::getCleanIDs($addresses, 'id_address');
                    $this->client->serializeOn();
                    $this->client->setPostData($this->query->customerInfos($id_customers));
                    if ($this->client->query()) {
                        $customerAdditionalSecond = $this->client->getContent();
                    } else {
                        $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                    }

                    $this->client->setPostData($this->query->customerAddressInfos($id_address));
                    if ($this->client->query()) {
                        $customerAddressAdditional = $this->client->getContent();
                    } else {
                        $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                    }

                    $import = new MageImport($process, $this->version, $this->url_cart, $this->forceCustomerIds);
                    $import->setPsValidationErrors($this->ps_validation_errors);
                    $import->customers($customers, $addresses, $customerAdditionalSecond, $customerAddressAdditional);
                    $this->errors = $import->getErrorMsg();
                    $this->warnings = $import->getWarningMsg();
                    $this->response = $import->getResponse();
                } else {
                    $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                }
            } else {
                $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
            }
        }
    }

    private function importOrders($process)
    {
        $this->client->setPostData($this->query->order());
        if ($this->client->query()) {
            $orders = $this->client->getContent();
            $id_order = self::getCleanIDs($orders, 'id_order');
            $id_address_delivery = self::getCleanIDs($orders, 'billing_address_id');
            $id_currency = self::getCleanIDs($orders, 'id_currency');
            $this->client->serializeOn();
            $this->client->setPostData($this->query->orderSqlSecond($id_order, $id_address_delivery, $id_currency));
            if ($this->client->query()) {
                $ordersAdditionalSecond = $this->client->getContent();

                $order_detail_product_ids = self::getCleanIDs($ordersAdditionalSecond['order_item'], 'product_id');
                $this->client->setPostData($this->query->orderSqlThird($id_order, $order_detail_product_ids));
                if ($this->client->query()) {
                    $ordersAdditionalThird = $this->client->getContent();
                    $import = new MageImport($process, $this->version, $this->url_cart, $this->forceOrderIds);
                    $import->setPsValidationErrors($this->ps_validation_errors);
                    $import->orders($orders, $ordersAdditionalSecond, $ordersAdditionalThird);
                    $this->errors = $import->getErrorMsg();
                    $this->warnings = $import->getWarningMsg();
                    $this->response = $import->getResponse();
                } else {
                    $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                }
            } else {
                $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
            }
        } else {
            $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
        }
    }

    // --- Internal helper methods:

    private function truncateModuleTables()
    {
        $res = Db::getInstance()->execute('TRUNCATE TABLE  `' . _DB_PREFIX_ . 'mage_migrationpro_data`');
        $res &= Db::getInstance()->execute('TRUNCATE TABLE  `' . _DB_PREFIX_ . 'mage_migrationpro_process`');
        $res &= Db::getInstance()->execute('TRUNCATE TABLE  `' . _DB_PREFIX_ . 'mage_migrationpro_mapping`');
        $res &= Db::getInstance()->execute('TRUNCATE TABLE  `' . _DB_PREFIX_ . 'mage_migrationpro_data_product_attributes`');

        if (!$res) {
            return false;
        }
    }

    private function createMapping($maps)
    {
        $res = true;
        foreach ($maps as $map) {
            foreach ($map as $key => $val) {
                $mapping = new MageMigrationProMapping($key);
                $mapping->local_id = $val;
                $res &= $mapping->save();
            }
        }

        return $res;
    }

    private function createProcess()
    {
        $res = Db::getInstance()->execute('TRUNCATE TABLE  `' . _DB_PREFIX_ . 'mage_migrationpro_process`');
        $res &= Db::getInstance()->execute('TRUNCATE TABLE  `' . _DB_PREFIX_ . 'mage_migrationpro_data`');
        MageMigrationProDBErrorLogger::removeErrorLogs();
        MageMigrationProDBWarningLogger::removeWarningLogs();
        MageMigrationProDBWarningLogger::removeLogFile();

        $this->client->setPostData($this->query->getCountInfo());
        $this->client->serializeOn();
        if ($this->client->query()) {
            foreach ($this->client->getContent() as $processKey => $processCount) {
                if (isset($processCount[0]['c']) && !empty($processCount[0]['c'])) {
                    $process = new MageMigrationProProcess();
                    $process->type = $processKey;
                    $process->total = (int)$processCount[0]['c'];
                    $process->imported = 0;
                    $process->id_source = 0;
                    $process->error = 0;
                    $process->point = 0;
                    $process->time_start = 0;
                    $process->finish = 0;
                    $res &= $process->add();
                }
            }

            return $res;
        }

        return false;
    }

    private function saveParamsToConfiguration($content)
    {
        Configuration::updateValue($this->module->name . '_url', preg_replace('/\/migration_pro\/server\.php/', '', pSQL(Tools::getValue('source_shop_url'))));
        Configuration::updateValue($this->module->name . '_token', pSQL(Tools::getValue('source_shop_token')));
        Configuration::updateValue($this->module->name . '_cms', pSQL($content['cms']));
        Configuration::updateValue($this->module->name . '_image_category', pSQL($content['image_category']));
        Configuration::updateValue($this->module->name . '_image_product', pSQL($content['image_product']));
        Configuration::updateValue($this->module->name . '_image_manufacturer', pSQL($content['image_manufacturer']));
        Configuration::updateValue($this->module->name . '_image_supplier', pSQL($content['image_supplier']));
        Configuration::updateValue($this->module->name . '_table_prefix', pSQL($content['table_prefix']));
        Configuration::updateValue($this->module->name . '_version', pSQL($content['version']));
        Configuration::updateValue($this->module->name . '_charset', pSQL($content['charset']));
        Configuration::updateValue($this->module->name . '_blowfish_key', pSQL($content['blowfish_key']));
        Configuration::updateValue($this->module->name . '_cookie_key', pSQL($content['cookie_key']));

        $this->initParams();
    }

    private function initParams()
    {
        if (Shop::getContext() == Shop::CONTEXT_ALL || Shop::getContext() == Shop::CONTEXT_GROUP) {
            Shop::setContext(Shop::CONTEXT_SHOP, Shop::getCompleteListOfShopsID()[0]);
        }

        $this->url_cart = Configuration::get($this->module->name . '_url');
        $this->token_cart = Configuration::get($this->module->name . '_token');
        $this->cms = Configuration::get($this->module->name . '_cms');
        $this->image_category = Configuration::get($this->module->name . '_image_category');
        $this->image_product = Configuration::get($this->module->name . '_image_product');
        $this->image_manufacturer = Configuration::get($this->module->name . '_image_manufacturer');
        $this->image_supplier = Configuration::get($this->module->name . '_image_supplier');
        $this->table_prefix = Configuration::get($this->module->name . '_table_prefix');
        $this->version = Configuration::get($this->module->name . '_version');
        $this->charset = Configuration::get($this->module->name . '_charset');
        $this->blowfish_key = Configuration::get($this->module->name . '_blowfish_key');
        $this->cookie_key = Configuration::get($this->module->name . '_cookie_key');
        $this->forceCategoryIds = Configuration::get($this->module->name . '_force_category_ids');
        $this->forceProductIds = Configuration::get($this->module->name . '_force_product_ids');
        $this->forceCustomerIds = Configuration::get($this->module->name . '_force_customer_ids');
        $this->forceOrderIds = Configuration::get($this->module->name . '_force_order_ids');
        $this->forceManufacturerIds = Configuration::get($this->module->name . '_force_manufacturer_ids');
        $this->ps_validation_errors = Configuration::get($this->module->name . '_ps_validation_errors');
        $this->truncate = Configuration::get($this->module->name . '_clear_data');
        $this->speed = Configuration::get($this->module->name . '_query_row_count');
        $this->recent_data = Configuration::get($this->module->name . '_migrate_recent_data');
    }

    private function requestToCartDetails()
    {
        // --- get default values from source cart

        $this->client->setPostData($this->query->getDefaultShopValues());
        $this->client->serializeOn();
        $this->client->query();
        $resultDefaultShopValues = $this->client->getContent();
        $this->query->setVersion($this->version);
        $this->client->setPostData($this->query->getMappingInfo());
        $this->client->query();
        $mappingInformation = $this->client->getContent();
        $mapping_value = MageMigrationProSaveMapping::listMapping(true, true);
        if (is_array($mappingInformation)) {
            if (Db::getInstance()->execute('TRUNCATE TABLE  `' . _DB_PREFIX_ . 'mage_migrationpro_mapping`')) {
                foreach ($mappingInformation as $mappingType => $mappingObject) {
                    foreach ($mappingObject as $value) {
                        if (empty($value['source_name'])) {
                            continue;
                        }
                        $mapping = new MageMigrationProMapping();
                        $mapping->type = $mappingType;
                        $mapping->source_id = $value['source_id'];
                        $mapping->source_name = $value['source_name'];
                        if (!empty($mapping_value)) {
                            $mapping->local_id = $mapping_value[$mappingType][$value['source_id']];
                        }
                        if (!$mapping->save()) {
                            $this->errors[] = 'Can\'t save to datebase mapping information. ';
                        }
                    }
                }
            } else {
                $this->errors[] = 'Can\'t truncate mapping table';
            }
        }
        if (empty($this->errors)) {
            return true;
        }

        return false;
    }

    public function saveMappingValues($mapping_values)
    {

        if (Db::getInstance()->execute('TRUNCATE TABLE  `' . _DB_PREFIX_ . 'magemigrationpro_save_mapping`')) {
            foreach ($mapping_values as $mappingType => $mappingObject) {
                foreach ($mappingObject as $source_id => $local_id) {
                    $mapping = new MageMigrationProSaveMapping();
                    $mapping->type = $mappingType;
                    $mapping->source_id = $source_id;
                    $mapping->source_name = $mappingType;
                    $mapping->local_id = $local_id;
                    if (!$mapping->save()) {
                        $this->errors[] = $this->module->l('Can\'t save to database mapping information. ');
                    }
                }
            }
        }
    }

    protected function truncateTables($case)
    {
        $res = false;
        switch ($case) {
            case 'taxes':
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'tax`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'tax_lang`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'tax_rule`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'tax_rules_group`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'tax_rules_group_shop`');
                break;
            case 'manufacturers':
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'manufacturer`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'manufacturer_lang`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'manufacturer_shop`');
                foreach (scandir(_PS_MANU_IMG_DIR_) as $d) {
                    if (preg_match('/^[0-9]+(\-(.*))?\.jpg$/', $d)) {
                        unlink(_PS_MANU_IMG_DIR_ . $d);
                    }
                }
                break;
            case 'categories':
                $res &= Db::getInstance()->execute('
					DELETE FROM `' . _DB_PREFIX_ . 'category`
					WHERE id_category NOT IN (' . (int)Configuration::get('PS_HOME_CATEGORY') . ', ' . (int)Configuration::get('PS_ROOT_CATEGORY') . ')');
                $res &= Db::getInstance()->execute('
					DELETE FROM `' . _DB_PREFIX_ . 'category_lang`
					WHERE id_category NOT IN (' . (int)Configuration::get('PS_HOME_CATEGORY') . ', ' . (int)Configuration::get('PS_ROOT_CATEGORY') . ')');
                $res &= Db::getInstance()->execute('
					DELETE FROM `' . _DB_PREFIX_ . 'category_shop`
					WHERE `id_category` NOT IN (' . (int)Configuration::get('PS_HOME_CATEGORY') . ', ' . (int)Configuration::get('PS_ROOT_CATEGORY') . ')');
                $res &= Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'category` AUTO_INCREMENT = 3');
                $res &= Db::getInstance()->execute('DELETE  FROM `' . _DB_PREFIX_ . 'magemigrationpro_link_rewrite` WHERE entity_type = "category"');

                foreach (scandir(_PS_CAT_IMG_DIR_) as $d) {
                    if (preg_match('/^[0-9]+(\-(.*))?\.jpg$/', $d)) {
                        unlink(_PS_CAT_IMG_DIR_ . $d);
                    }
                }
                break;
            case 'products':
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_shop`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'feature_product`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_lang`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'category_product`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_tag`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'image`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'image_lang`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'image_shop`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'specific_price`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'specific_price_priority`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_carrier`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'cart_product`');
//                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'compare_product`');
                if (count(Db::getInstance()->executeS('SHOW TABLES LIKE \'' . _DB_PREFIX_ . 'favorite_product\' '))) { //check if table exist
                    $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'favorite_product`');
                }
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_attachment`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_country_tax`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_download`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_group_reduction_cache`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_sale`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_supplier`');
//                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'scene_products`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'warehouse_product_location`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'stock`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'stock_available`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'stock_mvt`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'customization`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'customization_field`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'customization_field_lang`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'supply_order_detail`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'attribute_impact`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_attribute`');
                $res &= Db::getInstance()->execute('DELETE  FROM `' . _DB_PREFIX_ . 'magemigrationpro_link_rewrite` WHERE entity_type = "product"');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_attribute_shop`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_attribute_combination`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_attribute_image`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'pack`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'feature`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'feature_lang`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'feature_product`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'feature_shop`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'feature_value`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'feature_value_lang`');
                Image::deleteAllImages(_PS_PROD_IMG_DIR_);
                if (!file_exists(_PS_PROD_IMG_DIR_)) {
                    mkdir(_PS_PROD_IMG_DIR_);
                }
//                break;
//            case 'combinations':
//                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'attribute`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'attribute_impact`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'attribute_lang`');
//                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'attribute_group`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'attribute_group_lang`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'attribute_group_shop`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'attribute_shop`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_attribute`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_attribute_shop`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_attribute_combination`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'product_attribute_image`');
                $res &= Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'attribute`');
                $res &= Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'attribute_group`');
                $res &= Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'stock_available` WHERE id_product_attribute != 0');
//            case 'suppliers':
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'supplier`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'supplier_lang`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'supplier_shop`');
                foreach (scandir(_PS_SUPP_IMG_DIR_) as $d) {
                    if (preg_match('/^[0-9]+(\-(.*))?\.jpg$/', $d)) {
                        unlink(_PS_SUPP_IMG_DIR_ . $d);
                    }
                }
//                break;
                break;
            case 'customers':
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'customer`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'customer_group`');
//                break;
//            case 'addresses':
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'address`');
                break;
            case 'orders':
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'orders`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'order_detail`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'order_history`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'order_invoice`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'order_payment`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'order_invoice_payment`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'order_invoice_tax`');
                $res &= Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'order_carrier`');
                break;
        }
        Image::clearTmpDir();

        return $res;
    }

    // --- Static utility methods:
    public static function getCleanIDs($rows, $key, $return_array = false)
    {
        if (empty($rows)) {
            return 0;
        }
        $result = array();
        if (is_array($rows) && !empty($rows)) {
            foreach ($rows as $row) {
                if (is_numeric($row[$key])) {
                    $result[] = $row[$key];
                } else {
                    $result[] = "'" . $row[$key] . "'";
//                    $result[] = $row[$key];
                }
            }
            $result = array_unique($result);
            if ($return_array) {
                return $result;
            } else {
                $result = implode(',', $result);
            }

            return $result;
        } else {
            return 'null';
        }
    }

    public static function convertSpeedNameToNumeric($speed)
    {
        switch ((string)$speed) {
            case 'VerySlow':
                $row_count = 2;
                break;
            case 'Slow':
                $row_count = 5;
                break;
            case 'Normal':
                $row_count = 10;
                break;
            case 'Fast':
                $row_count = 25;
                break;
            case 'VeryFast':
                $row_count = 85;
                break;
            case 'MigrationProSpeed':
                $row_count = 100;
                break;
            default:
                $row_count = 10;
                break;
        }

        return $row_count;
    }

    public static function getAttributeIds($attributes)
    {
        $result = array();
        foreach ($attributes as $attributeId => $attributeName) {
            $result[$attributeId] = $attributeName;
        }

        return $result;
    }

    private function getEntityTypeId($typeName)
    {
        $this->client->setPostData($this->query->getEntityTypeIds());
        if ($this->client->query()) {
            $entityTypes = $this->client->getContent();
            foreach ($entityTypes as $entityType) {
                if (preg_match('/' .$typeName  . '/', $entityType['entity_type_code'])) {
                    return $entityType['entity_type_id'];
                }
            }
        } else {
            $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
            return null;
        }

        $this->errors[] = $this->l('Entity type ID for ' . $typeName . ' does not exist in Magento');
        return null;
    }
}
