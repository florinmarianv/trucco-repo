{*
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from MigrationPro
* Use, copy, modification or distribution of this source file without written
* license agreement from the MigrationPro is strictly forbidden.
* In order to obtain a license, please contact us: contact@migration-pro.com
*
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe MigrationPro
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de la MigrationPro est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
*
* @author    MigrationPro
* @copyright Copyright (c) 2012-2020 MigrationPro
* @license   Commercial license
* @package   MigrationPro: Magento To PrestaShop Migration Tool
*}

{*<div class="panel">
	<h3><i class="icon icon-credit-card"></i> {l s='MigrationPro: Magento To PrestaShop Migration Tool'
		mod='magemigrationpro'}</h3>
	<p>
		<strong>{l s='Here is my new generic module!' mod='magemigrationpro'}</strong><br />
		{l s='Thanks to PrestaShop, now I have a great module.' mod='magemigrationpro'}<br />
		{l s='I can configure it using the following configuration form.' mod='magemigrationpro'}
	</p>
	<br />
	<p>
		{l s='This module will boost your sales!' mod='magemigrationpro'}
	</p>
</div>*}

<div class="panel">
	<h3><i class="icon icon-tags"></i> {l s='Migration Guide: Documentation' mod='magemigrationpro'}</h3>
	<p>
		&raquo; {l s='PDF documentation to configure the module' mod='magemigrationpro'} :
			<a href="{$module_dir|escape:'javascript':'UTF-8'}documentation/readme_en.pdf" target="_blank">{l
				s='Documentation' mod='magemigrationpro'}</a>
	</p>
	<p>
		&raquo; {l s='Magento Connector file:' mod='magemigrationpro'} :
			<a href="{$module_dir|escape:'javascript':'UTF-8'}assets/migration_pro.zip" target="_blank">{l s='Download' mod='magemigrationpro'}</a>
	</p>
</div>
