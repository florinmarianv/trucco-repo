<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MigrationPro
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@migration-pro.com
 *
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe MigrationPro
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la MigrationPro est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
 *
 * @author    MigrationPro
 * @copyright Copyright (c) 2012-2020 MigrationPro
 * @license   Commercial license
 * @package   MigrationPro: Magento To PrestaShop Migration Tool
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

@ini_set("gd.jpeg_ignore_warning", 1);
@ini_set("display_errors", 0);
@ini_set('max_execution_time', 0);
//@ini_set('error_reporting', 1);
@ini_set('memory_limit', '-1');

require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/MageMigrationProMapping.php');
require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/MageMigrationProProcess.php');
require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/MageMigrationProSaveMapping.php');
require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/MageMigrationProData.php');
require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/MageLinkRewrite.php');
require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/MageMigrationProMigratedData.php');
require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/MageMigrationProDataProductAttributes.php');
require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/MigrationProMage.php');
require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/MagePasswordEncrypt.php');
require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/MageClient.php');
require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/MageQuery.php');
require_once(_PS_MODULE_DIR_ . 'magemigrationpro/classes/MageImport.php');

class MageMigrationPro extends Module
{
    protected $wizard_steps;

    public function __construct()
    {
        $this->name = 'magemigrationpro';
        $this->tab = 'migration_tools';
        $this->version = '3.3.0';
        $this->author = 'MigrationPro';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->module_key = '3075f6e874237aba893e3bfa7bc51655';
        $this->author_address = '0x24cA4dE04f3EC79296742139589b4f9A9892E1ec';

        parent::__construct();

        $this->displayName = $this->l('MigrationPro: Magento To PrestaShop Migration Tool');
        $this->description = $this->l('MigrationPro is an easy, fast and safe migration way of your data.');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        // Prepare tab
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = 'AdminMageMigrationPro';
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = 'MigrationPro';
        }
        $tab->id_parent = -1;
        $tab->module = $this->name;

        include(dirname(__FILE__) . '/sql/install.php');
        Configuration::updateValue('PS_PRODUCT_SHORT_DESC_LIMIT', 80000);

        if (!$tab->add() || !parent::install() || !$this->registerHook('header') || !$this->registerHook('backOfficeHeader') || !$this->registerHook('ActionAdminControllerSetMedia') || !$this->registerHook('actionBeforeAuthentication')) {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        include(dirname(__FILE__) . '/sql/uninstall.php');


        $id_tab = (int)Tab::getIdFromClassName('AdminMageMigrationPro');

        if ($id_tab) {
            $tab = new Tab($id_tab);
            $tab->delete();
        }

        if (!parent::uninstall()) {
            return false;
        }

        return true;
    }

    public function initWizard()
    {
        $this->wizard_steps = array(
            'name' => 'migrationpro_wizard',
            'steps' => array(
                array(
                    'title' => $this->l('Connection'),
                ),
                array(
                    'title' => $this->l('Configuration'),
                ),
                array(
                    'title' => $this->l('Migration'),
                )
            )
        );
    }

    // steps form fields
    public function renderStepOne()
    {
        $this->fields_form = array(
            'form' => array(
                'id_form' => 'step_migrationpro_connection',
                'legend' => array(
                    'title' => $this->l('Start New Migration'),
                    'icon' => 'icon-AdminTools'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Source Url'),
                        'name' => 'source_shop_url',
                        'required' => true,
                        'hint' => array(
                            $this->l('The source Magento Url.'),
                            $this->l('Enter a valid URL. Protocol is required (http://, https:// or ftp://)')
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Token'),
                        'name' => 'source_shop_token',
                        'required' => true,
                        'hint' => $this->l('The access token from source Magento')
                    )
                )
            )
        );

        $fields_value = $this->getStepOneFieldsValues();

        return $this->renderGenericForm(array('form' => $this->fields_form), $fields_value);
    }

    public function renderStepTwo()
    {
        $mappings = MageMigrationProMapping::listMapping(true);
        if (empty($mappings)) {
            return false;
        }
        // Shops mapping
        $multiShopsInputs = array();

        foreach ($mappings['multi_shops'] as $key => $val) {
            $multiShopsInputs[] = array(
                'type' => 'select',
                'label' => $val['source_name'],
                'name' => "map[multi_shops][$key]",
                'required' => true,
                'options' => array(
                    'query' => Shop::getShops(),
                    'id' => 'id_shop',
                    'name' => 'name'
                ),
//                'condition' => Shop::isFeatureActive(),
                'default_value' => Shop::getContextShopID(),
                'hint' => $this->l('Select target shop, wich you want migrate this shop')
            );
        }

        $multiShops = array(
            'form' => array(
                'id_form' => 'step_migrationpro_configuration',
                'legend' => array(
                    'title' => $this->l('Shops'),
                    'icon' => 'icon-shopping-cart'
                ),
                'input' => $multiShopsInputs
            )
        );

        // Currencies Mapping
        $currenciesInputs = array();

        foreach ($mappings['currencies'] as $key => $val) {
            $currenciesInputs[] = array(
                'type' => 'select',
                'label' => $val['source_name'],
                'hint' => $this->l('Select target Cart currencies properly Source Magento Store currencies. This is needed for Creating currencies in Target Prestashop Store'),
                'name' => "map[currencies][$key]",
                'required' => true,
                'options' => array(
                    'query' => Currency::getCurrencies(),
                    'id' => 'id_currency',
                    'name' => 'name'
                ),
//                'default_value' => Currency::getCurrent()
            );
        }

        $currencies = array(
            'form' => array(
                'id_form' => 'step_migrationpro_configuration',
                'legend' => array(
                    'title' => $this->l('Currencies'),
                    'icon' => 'icon-money'
                ),
                'input' => $currenciesInputs
            )
        );

        // Languages Mapping
        $languagesInputs = array();

        foreach ($mappings['languages'] as $key => $val) {
            $languagesInputs[] = array(
                'type' => 'select',
                'label' => $val['source_name'],
                'hint' => $this->l('Select target Cart languages properly source Magento Store languages. This is needed for Creating languages in Target Prestashop Store.'),
                'name' => "map[languages][$key]",
                'required' => true,
                'options' => array(
                    'query' => array_merge(array(array('id_lang' => 0, 'name' => 'none')), Language::getLanguages()),
                    'id' => 'id_lang',
                    'name' => 'name'
                ),
            );
        }

        $languages = array(
            'form' => array(
                'id_form' => 'step_migrationpro_configuration',
                'legend' => array(
                    'title' => $this->l('Languages'),
                    'icon' => 'icon-AdminParentLocalization'
                ),
                'input' => $languagesInputs
            )
        );


        // Orders Status Mapping
        $ordersStatusInputs = array();

        foreach ($mappings['order_states'] as $key => $val) {
            $ordersStatusInputs[] = array(
                'type' => 'select',
                'label' => $val['source_name'],
                'hint' => $this->l('Select target Cart order status properly sorce Magento Store order status. This is needed for Creating order status in Target Prestashop Store.'),
                'name' => "map[order_states][$key]",
                'required' => true,
                'options' => array(
                    'query' => OrderState::getOrderStates($this->context->language->id),
                    'id' => 'id_order_state',
                    'name' => 'name'
                ),
            );
        }

        $ordersStatus = array(
            'form' => array(
                'id_form' => 'step_migrationpro_configuration',
                'legend' => array(
                    'title' => $this->l('Order status'),
                    'icon' => 'icon-time'
                ),
                'input' => $ordersStatusInputs
            )
        );


        // Customer Groups Mapping
        $customerGroupInputs = array();

        foreach ($mappings['customer_groups'] as $key => $val) {
            $customerGroupInputs[] = array(
                'type' => 'select',
                'label' => $val['source_name'],
                'hint' => $this->l('Select target Cart Customer Group properly source Magento Store Customer Group. This is needed for Creating Customer Group in Target Prestashop Store.'),
                'name' => "map[customer_groups][$key]",
                'required' => true,
                'options' => array(
                    'query' => Group::getGroups($this->context->language->id),
                    'id' => 'id_group',
                    'name' => 'name'
                ),
            );
        }

        $customerGroup = array(
            'form' => array(
                'id_form' => 'step_migrationpro_configuration',
                'legend' => array(
                    'title' => $this->l('Customer Groups'),
                    'icon' => 'icon-AdminParentCustomer'
                ),
                'input' => $customerGroupInputs
            )
        );

        //entities to migrate
        $entitiesToMigrate = array(
            array(
                'type' => 'switch',
                'label' => $this->l("Select All"),
                'hint' => $this->l('Select All Boxes'),
                'name' => 'entities_select_all',
                'id' => 'entities_select_all',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )

            ),
            array(
                'type' => 'switch',
                'label' => $this->l("Taxes"),
                'hint' => $this->l('For migrate Taxes enable this option'),
                'name' => 'entities_taxes',
                'id' => 'entities_taxes',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )

            ),
            array(
                'type' => 'switch',
                'label' => "Manufacturers",
                'hint' => $this->l('For migrate Manufacturers enable this option'),
                'name' => 'entities_manufacturers',
                'id' => 'entities_manufacturers',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )

            ),
            array(
                'type' => 'switch',
                'label' => "Categories",
                'hint' => $this->l('For migrate Categories enable this option'),
                'name' => 'entities_categories',
                'id' => 'entities_categories',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )

            ),
            array(
                'type' => 'switch',
                'label' => "Products",
                'hint' => $this->l('For migrate Products enable this option'),
                'name' => 'entities_products',
                'id' => 'entities_products',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )

            ),
            array(
                'type' => 'switch',
                'label' => "Customers",
                'hint' => $this->l('For migrate Customers enable this option'),
                'name' => 'entities_customers',
                'id' => 'entities_customers',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )

            ),
            array(
                'type' => 'switch',
                'label' => "Orders",
                'hint' => $this->l('For migrate Orders enable this option'),
                'name' => 'entities_orders',
                'id' => 'entities_orders',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )

            )
        );

        $entitiesToMigrate = array(
            'form' => array(
                'id_form' => 'step_migrationpro_configuration',
                'legend' => array(
                    'title' => $this->l('Data to Migrate'),
                    'icon' => 'icon-AdminCatalog'
                ),
                'input' => $entitiesToMigrate
            )
        );

        // Additional Options

        $advancedOptionsArray = array(
            array(
                'type' => 'switch',
                'label' => $this->l("Select All"),
                'hint' => $this->l('Select All Boxes'),
                'name' => 'force_select_all',
                'id' => 'force_select_all',
                'is_bool' => true,
                'desc' => $this->l('You can select all options if you want'),
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )

            ),
            array(
                'type' => 'switch',
                'label' => $this->l("Clean Target"),
                'hint' => $this->l('This feature delete all informations of target shop before Migration'),
                'name' => 'clear_data',
                'id' => 'clear_data',
                'is_bool' => true,
                'desc' => $this->l('Delete target shops all information'),
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )

            ),
            array(
                'type' => 'switch',
                'label' => $this->l("Migrate recent data"),
                'hint' => $this->l('This feature migrates data that are not yet in the target PrestaShop'),
                'name' => 'migrate_recent_data',
                'id' => 'migrate_recent_data',
                'is_bool' => true,
                'desc' => $this->l('Migrate recent data only (will work only after a first migration)'),
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )

            ),
//            array(
//                'type'    => 'switch',
//                'label'   => "Keep Categories ID",
//                'hint'    => $this->l('If you want keep SEO of your old Shop, we recomend keep old Categories Id'),
//                'name'    => 'force_category_ids',
//                'id'      => 'force_category_ids',
//                'is_bool' => true,
//                'desc'    => $this->l('This option keep old Categories ID on the new Store'),
//                'values'  => array(
//                    array(
//                        'id'    => 'active_on',
//                        'value' => true,
//                        'label' => $this->l('Enabled')
//                    ),
//                    array(
//                        'id'    => 'active_off',
//                        'value' => false,
//                        'label' => $this->l('Disabled')
//                    )
//                )
//            ),
            array(
                'type' => 'switch',
                'label' => "Keep Products ID",
                'hint' => $this->l('If you want keep SEO of your old Shop, we recomend keep old Products Id'),
                'name' => 'force_product_ids',
                'id' => 'force_product_ids',
                'is_bool' => true,
                'desc' => $this->l('This option keep old Products ID on the new Store'),
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )

            ),
            array(
                'type' => 'switch',
                'label' => "Keep Customers ID",
                'hint' => $this->l('if there is no another problem, we recommend keep old Customers ID'),
                'name' => 'force_customer_ids',
                'id' => 'force_customer_ids',
                'is_bool' => true,
                'desc' => $this->l('This option keep old Customers ID on the new Store'),
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )

            ),
            array(
                'type' => 'switch',
                'label' => "Keep Orders ID",
                'hint' => $this->l('if there is no another problem, we recommend keep old Orders ID'),
                'name' => 'force_order_ids',
                'id' => 'force_order_ids',
                'is_bool' => true,
                'desc' => $this->l('This option keep old Orders ID on the new Store'),
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )
            ),
            array(
                'type' => 'switch',
                'label' => "Keep Manufacturers ID",
                'hint' => $this->l('if there is no another problem, we recommend keep old Manufacturers ID'),
                'name' => 'force_manufacturer_ids',
                'id' => 'force_manufacturer_ids',
                'is_bool' => true,
                'desc' => $this->l('This option keep old Manufacturers ID on the new Store'),
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                )
            ),
//            array(
//                'type'    => 'switch',
//                'label'   => "Data Validation",
//                'hint'    => $this->l('If this feature disabled the module will migarte only the valid data proper to target PrestaShop'),
//                'name'    => 'ps_validation_errors',
//                'id'      => 'ps_validation_errors',
//                'is_bool' => true,
//                'desc'    => $this->l('In Magento and PrestaShop has different validation rules.
//                                       If you Want Migrate Only the Valid Data proper to Target PS select this feature to off.
//                                       If you want All of your data turn on this feature then the module give you the Validtion Errors
//                                       then you need to correct them from source manualy'),
//                'values'  => array(
//                    array(
//                        'id'    => 'active_on',
//                        'value' => true,
//                        'label' => $this->l('Enabled')
//                    ),
//                    array(
//                        'id'    => 'active_off',
//                        'value' => false,
//                        'label' => $this->l('Disabled')
//                    )
//                )
//            )
        );

        $advancedOptions = array(
            'form' => array(
                'id_form' => 'step_migrationpro_configuration',
                'legend' => array(
                    'title' => $this->l('Utility Options'),
                    'icon' => 'icon-AdminTools'
                ),
                'input' => $advancedOptionsArray
            )
        );

        $speedOptionsInputs = array();
        $speedOptionsInputs[] = array(
            'type' => 'text',
            'label' => $this->l('Select Migration Speed:'),
            'name' => "speed",
            'hint' => $this->l('You can select your migration speed with this option, by default this options equal to normal, if you want use this options we recommend consider your servers power.'),
            'id' => 'input_speed_range_slider',
        );

        $speedOptions = array(
            'form' => array(
                'id_form' => 'step_migrationpro_configuration',
                'legend' => array(
                    'title' => $this->l('Advanced Options'),
                    'icon' => 'icon-AdminTools'
                ),
                'input' => $speedOptionsInputs
            )
        );


        $fields_value = $this->getStepTwoFieldsValues();

        return $this->renderGenericForm(array(
            $multiShops,
            $currencies,
            $languages,
            $ordersStatus,
            $customerGroup,
            $entitiesToMigrate,
            $advancedOptions,
            $speedOptions
        ), $fields_value);
    }

    public function renderStepThree()
    {
        if (count($lastExecutingProcesses = MageMigrationProProcess::getAll())) {
            $this->context->smarty->assign('processes', $lastExecutingProcesses);
        }

        $this->context->smarty->assign(array(
            'percent' => MageMigrationProProcess::calculateImportedDataPercent(),
        ));

        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/view.tpl');

        return $output;
    }

    // form fields values
    public function getStepOneFieldsValues()
    {
        return array(
            'source_shop_url' => Configuration::get($this->name . '_url'),
            'source_shop_token' => Configuration::get($this->name . '_token'),
        );
    }

    public function getStepTwoFieldsValues()
    {
        $mappings = MageMigrationProMapping::listMapping();

        $fieldValues = array(
            'entities_taxes' => 0,
            'entities_manufacturers' => 0,
            'entities_categories' => 0,
            'entities_products' => 0,
            'entities_products' => 0,
            'entities_customers' => 0,
            'entities_orders' => 0,
            'entities_select_all' => 0,
            'force_category_ids' => 0,
            'force_product_ids' => 0,
            'force_customer_ids' => 0,
            'clear_data' => 0,
            'force_order_ids' => 0,
            'force_manufacturer_ids' => 0,
            'ps_validation_errors' => 0,
            'migrate_recent_data' => 0,
            'force_select_all' => 0,
            'speed' => 'Normal'
        );
        if (!empty($mappings)) {
            foreach ($mappings as $map) {
                $fieldValues['map[' . $map['type'] . '][' . $map['id_mapping'] . ']'] = $map['local_id'];
            }
        }

        return $fieldValues;
    }

    // form fields value validation

    // helper functions for form
    public function renderGenericForm($fields_form, $fields_value, $tpl_vars = array())
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->tpl_vars = array_merge(array(
            'fields_value' => $fields_value,
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        ), $tpl_vars);

        return $helper->generateForm($fields_form);
    }

    public function getFieldValue($key)
    {
        return Tools::getValue($key);
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {

        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitMigrationproModule')) == true) {
            $this->postProcess();
        }

        $this->initWizard();
        $this->context->smarty->assign('module_dir', $this->_path);
        $howToIntroduction = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        $this->context->smarty->assign(array(
            'wizard_steps' => $this->wizard_steps,
            'validate_url' => $this->context->link->getAdminLink('AdminMageMigrationPro'),
            'warningLogPath' => $this->_path . 'classes/loggers/warning_logs.txt',
            'downloadLogText' => $this->l('Download log file'),
            'multistore_enable' => Shop::isFeatureActive(),
            'wizard_contents' => array(
                'contents' => array(
                    0 => $howToIntroduction . $this->renderStepOne(),
                    1 => $this->renderStepTwo(),
                    2 => ''
                )
            ),
            'labels' => array(
                'next' => $this->l('Next'),
                'previous' => $this->l('Previous'),
                'finish' => $this->l('Migrate')
            )
        ));
        $output = '';
        $processObject = MageMigrationProProcess::getActiveProcessObject();
        if (Validate::isLoadedObject($processObject) && $lastExecutingProcesses = MageMigrationProProcess::getAll()) {
            if (count($lastExecutingProcesses = MageMigrationProProcess::getAll())) {
                $this->context->smarty->assign('processes', $lastExecutingProcesses);
            }
//            $this->context->smarty->assign('processes', $lastExecutingProcesses);
        }

        $this->context->smarty->assign(array(
            'percent' => MageMigrationProProcess::calculateImportedDataPercent(),
        ));

        $output .= $this->context->smarty->fetch($this->local_path . 'views/templates/admin/wizard.tpl');

        return $output;//.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMigrationproModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'MIGRATIONPRO_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'MIGRATIONPRO_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'MIGRATIONPRO_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'MIGRATIONPRO_LIVE_MODE' => Configuration::get('MIGRATIONPRO_LIVE_MODE', true),
            'MIGRATIONPRO_ACCOUNT_EMAIL' => Configuration::get('MIGRATIONPRO_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'MIGRATIONPRO_ACCOUNT_PASSWORD' => Configuration::get('MIGRATIONPRO_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    public function hookActionBeforeAuthentication()
    {
        $mail = Tools::getValue('tdkql_email_login');
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $pass = Tools::getValue('tdkql_pass_login');
        } else {
            $pass = Tools::getValue('tdkql_pass_login');
        }


        if (Validate::isEmail($mail)) {
            $result = MigrationProMage::getMageUser($mail);
            if (!empty($result)) {
                $hashedpass = $result[0]['passwd'];
                $id_customer = (int)$result[0]['id_customer'];
                if (Configuration::get('magemigrationpro_version') == 2) {
                    $magePasswordEncryptor = new MagePasswordEncrypt($hashedpass, 'md5');
                } else {
                    $magePasswordEncryptor = new MagePasswordEncrypt($hashedpass);
                }
                if ($magePasswordEncryptor->comparePassword($pass)) {
                    $customer = new Customer($id_customer);
                    $customer->passwd = Tools::encrypt($pass);
//                    if (version_compare(_PS_VERSION_, '1.7', '>=')) {
//                        $customer->passwd = Tools::hash($pass);
//                    } else {
//                    }
                    if ($customer->save()) {
                        MigrationProMage::deleteUserById($id_customer);
                    }
                }
               /* else {
               $crypto = ServiceLocator::get('\\PrestaShop\\PrestaShop\\Core\\Crypto\\Hashing');
               $hashedPass = $crypto->hash($pass);
               if ($hashedPass == $hashedpass) {
               $customer = new Customer($id_customer);
               if (version_compare(_PS_VERSION_, '1.7', '>=')) {
               $customer->passwd = Tools::hash($pass);
               } else {
               $customer->passwd = Tools::encrypt($pass);
               }
               if ($customer->save()) {
               MigrationProMage::deleteUserById($id_customer);
               }
               }
               }*/
            }
        }
    }

    /**
     * Add the CSS  files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name || Tools::getValue('configure') == $this->name) {
            $this->context->controller->addCSS($this->_path . 'views/css/migrationpro.css');
            $this->context->controller->addCSS($this->_path . 'views/css/rangeslider/ion.rangeSlider.css');
            $this->context->controller->addCSS($this->_path . 'views/css/rangeslider/ion.rangeSlider.skinModern.css');
        }
    }

    /**
     * Add the JavaScript files you want to be loaded in the BO.
     */
    public function hookActionAdminControllerSetMedia()
    {
        if (Tools::getValue('module_name') == $this->name || Tools::getValue('configure') == $this->name) {
            $this->context->controller->addJquery();
            $this->context->controller->addJqueryPlugin('typewatch');
            $this->context->controller->addJS($this->_path . '/views/js/migrationpro_wizard.js');
            $this->context->controller->addJS($this->_path . '/views/js/smartWizardMigrationPro.js');
            $this->context->controller->addJS($this->_path . '/views/js/rangeslider/ion.rangeSlider.min.js');
        }
    }
}
