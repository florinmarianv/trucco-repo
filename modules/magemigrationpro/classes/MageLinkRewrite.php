<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MigrationPro
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@migration-pro.com
 *
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe MigrationPro
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la MigrationPro est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
 *
 * @author    MigrationPro
 * @copyright Copyright (c) 2012-2020 MigrationPro
 * @license   Commercial license
 * @package   MigrationPro: Magento To PrestaShop Migration Tool
 */

class MageLinkRewrite extends ObjectModel
{
    public $id;

    public $entity_id;

    public $entity_type;

    public $source_link_rewrite;

    public static $definition = array(
        'table'   => 'magemigrationpro_link_rewrite',
        'primary' => 'id',
        'fields'  => array(
            'entity_id' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
            'entity_type'      => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
            'source_link_rewrite'      => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
        ),
    );

    public static function importLinkRewrite($entity_id, $link_rewrite, $type)
    {
        return Db::getInstance()->execute('REPLACE INTO ' . _DB_PREFIX_ . 'magemigrationpro_link_rewrite (entity_id, entity_type, source_link_rewrite) VALUES ('  .(int)$entity_id . ', \'' . pSQL($type) . '\', \'' . pSQL($link_rewrite) . '\')');
    }

    public static function getEntityIdByLinkRewrite($linkRewrite, $type)
    {
        return Db::getInstance()->getValue('SELECT entity_id FROM ' . _DB_PREFIX_ . 'magemigrationpro_link_rewrite WHERE source_link_rewrite = \'' . $linkRewrite . '\' AND entity_type = \'' . $type . '\'');
    }
}
