<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MigrationPro
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@migration-pro.com
 *
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe MigrationPro
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la MigrationPro est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
 *
 * @author    MigrationPro
 * @copyright Copyright (c) 2012-2020 MigrationPro
 * @license   Commercial license
 * @package   MigrationPro: Magento To PrestaShop Migration Tool
 */

require_once "loggers/MageLogger.php";
require_once "MageValidator.php";

class MageImport
{
    const UNFRIENDLY_ERROR = false;

    // --- Objects, Option & response vars:
    private $validator;
    protected $obj;
    protected $process;
    protected $client;
    protected $query;
    protected $url;
    protected $force_ids;
    protected $regenerate;
    protected $image_path;
    protected $image_supplier_path;
    protected $version;
    protected $shop_is_feature_active;
    protected $mapping;
    protected $ps_validation_errors = true;

    protected $error_msg = array();
    protected $warning_msg = array();
    protected $response;

    private $logger;

    // --- Constructor / destructor:

    public function __construct(MageMigrationProProcess $process, $version, $url_cart, $force_ids, MageClient $client = null, MageQuery $query = null)
    {
        $this->regenerate = false;
        $this->process = $process;
        $this->version = $version;
        $this->url = $url_cart;
        $this->force_ids = $force_ids;
        $this->client = $client;
        $this->query = $query;
        $this->mapping = MageMigrationProMapping::listMapping(true, true);
        $this->shop_is_feature_active = Shop::isFeatureActive();
        $this->logger = new MageLogger();
        $this->validator = new MageValidator();
    }

    // --- Configuration methods:

    public function setImagePath($string)
    {
        $this->image_path = pSQL($string);
    }

    public function setImageSupplierPath($string)
    {
        $this->image_supplier_path = pSQL($string);
    }

    public function setPsValidationErrors($bool)
    {
        $this->ps_validation_errors = $bool;
    }

    public function preserveOn()
    {
        $this->force_ids = true;
    }

    public function preserveOff()
    {
        $this->force_ids = false;
    }

    // --- After object methods:

    public function getErrorMsg()
    {
        return $this->error_msg;
    }


    public function getWarningMsg()
    {
        return $this->warning_msg;
    }

    public function getResponse()
    {
        return $this->response;
    }

    // --- Import methods:

    public function taxes($taxes, $taxRulesGroups, $taxRules)
    {
        $importedTaxRates = array();
        foreach ($taxes['tax_rate'] as $tax) {
            if ($taxObject = $this->createObjectModel('Tax', (int)$tax['id_tax'])) {
                $taxObject->rate = (float)$tax['rate'];
                $taxObject->active = 1;
                $taxObject->deleted = 0;

                $taxObject->name[Configuration::get('PS_LANG_DEFAULT')] = pSQL(Tools::substr($tax['name'], 0, 32));

                foreach ($taxes['tax_rate_lang'] as $taxLang) {
                    if ($taxLang['tax_calculation_rate_id'] == $tax['id_tax']) {
                        $taxObject->name[self::getLanguageID($taxLang['store_id'])] = pSQL(Tools::substr($taxLang['value'], 0, 32));
                    }
                }

                $res = false;
                $err_tmp = '';

                $this->validator->setObject($taxObject);
                $this->validator->checkFields();
                $tax_error_tmp = $this->validator->getValidationMessages();
                if ($taxObject->id && Tax::existsInDatabase($taxObject->id, 'tax')) {
                    try {
                        $res = $taxObject->update();
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }
                if (!$res) {
                    try {
                        $res = $taxObject->add(false);
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    $this->showMigrationMessageAndLog(sprintf('Tax (ID: %1$s) cannot be saved. %2$s', (isset($tax['id_tax']) && !empty($tax['id_tax'])) ? Tools::safeOutput($tax['id_tax']) : 'No ID', $err_tmp), 'Tax');
                } else {
                    self::addLog('Tax', (int)$tax['id_tax'], $taxObject->id);
                }

                $this->showMigrationMessageAndLog($tax_error_tmp, 'Tax');
            }
        }
        // import tax rules group
        foreach ($taxRulesGroups as $taxRulesGroup) {
            if ($taxRulesGroupModel = $this->createObjectModel('TaxRulesGroup', (int)$taxRulesGroup['id_tax_rules_group'])) {
                $taxRulesGroupModel->name = pSQL($taxRulesGroup['name']);
                if (empty($taxRulesGroup['date_add']) && empty($taxRulesGroup['date_upd'])) {
                    $taxRulesGroupModel->date_add = date('Y-m-d H:i:s');
                    $taxRulesGroupModel->date_upd = date('Y-m-d H:i:s');
                }
                $taxRulesGroupModel->active = 1;

                if (empty($taxRulesGroups['tax_rules_group_shop'])) {
                    $taxRulesGroupModel->id_shop_list = Shop::getCompleteListOfShopsID();
                }


                $res = false;
                $err_tmp = '';

                $this->validator->setObject($taxRulesGroupModel);
                $this->validator->checkFields();
                $tax_rule_group_error_tmp = $this->validator->getValidationMessages();
                if ($taxRulesGroupModel->id && TaxRulesGroup::existsInDatabase($taxRulesGroupModel->id, 'tax_rules_group')) {
                    try {
                        $res = $taxRulesGroupModel->update();
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }
                if (!$res) {
                    try {
                        $res = $taxRulesGroupModel->add(false);
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    $this->showMigrationMessageAndLog(sprintf('Tax Rules Group (ID: %1$s) cannot be saved. %2$s', (isset($taxRulesGroup['id_tax_rules_group']) && !empty($taxRulesGroup['id_tax_rules_group'])) ? Tools::safeOutput($taxRulesGroup['id_tax_rules_group']) : 'No ID', $err_tmp), 'TaxRulesGroup');
                } else {
                    // import tax rules for this group
                    foreach ($taxRules as $taxRule) {
                        if (in_array($taxRule['id_tax'], $importedTaxRates)) {
                            continue;
                        }
                        if ($taxRuleModel = $this->createObjectModel('TaxRule', (int)$taxRule['id_tax_rule'])) {
                            $taxRuleModel->id_tax_rules_group = (int)$taxRule['id_tax_rules_group'];
                            $taxRuleModel->id_country = Country::getByIso(pSQL($taxRule['id_country']));
                            $taxRuleModel->id_state = State::getIdByIso(pSQL($taxRule['id_state']));
                            $taxRuleModel->id_tax = (int)$taxRule['id_tax'];
                            if (empty($taxRule['zip_is_range'])) {
                                $taxRuleModel->zipcode_from = (int)$taxRule['tax_postcode'];
                                $taxRuleModel->zipcode_to = 0;
                            } else {
                                $taxRuleModel->zipcode_from = (int)$taxRule['zipcode_from'];
                                $taxRuleModel->zipcode_to = (int)$taxRule['zipcode_to'];
                            }
                            $taxRuleModel->behavior = 1;

                            $res = false;
                            $err_tmp = '';

                            $this->validator->setObject($taxRuleModel);
                            $this->validator->checkFields();
                            $tax_rule_error_tmp = $this->validator->getValidationMessages();
                            if ($taxRuleModel->id && TaxRule::existsInDatabase($taxRuleModel->id, 'tax_rule')) {
                                try {
                                    $res = $taxRuleModel->update();
                                } catch (PrestaShopException $e) {
                                    $err_tmp = $e->getMessage();
                                }
                            }
                            if (!$res) {
                                try {
                                    $res = $taxRuleModel->add(false);
                                    $importedTaxRates[] = (int)$taxRule['id_tax'];
                                } catch (PrestaShopException $e) {
                                    $err_tmp = $e->getMessage();
                                }
                            }

                            if (!$res) {
                                $this->showMigrationMessageAndLog(sprintf('Tax Rule (ID: %1$s) cannot be saved. %2$s', (isset($taxRule['id_tax_rule']) && !empty($taxRule['id_tax_rule'])) ? Tools::safeOutput($taxRule['id_tax_rule']) : 'No ID', $err_tmp), 'TaxRule');
                            } else {
                                self::addLog('TaxRule', (int)$taxRule['id_tax_rule'], $taxRuleModel->id);
                            }
                            $this->showMigrationMessageAndLog($tax_rule_error_tmp, 'TaxRule');
                        }
                    }
                    if (count($this->error_msg) == 0) {
                        self::addLog('TaxRulesGroup', $taxRulesGroup['id_tax_rules_group'], $taxRulesGroupModel->id);
                    }
                }
                $this->showMigrationMessageAndLog($tax_rule_group_error_tmp, 'TaxRulesGroup');
            }
        }

        $this->updateProcess(count($taxRulesGroups));
    }

    public function manufacturers($manufacturerShopLang)
    {
        $count = 0;
        foreach ($manufacturerShopLang['manufacturers'] as $manufacturer) {
            if ($manufacturerObj = $this->createObjectModel('Manufacturer', (int)$manufacturer['id_manufacturer'])) {
                $manufacturerObj->name = pSQL($manufacturer['name']);
                $manufacturerObj->date_add = date('Y-m-d H:i:s');
                $manufacturerObj->date_upd = date('Y-m-d H:i:s');
                $manufacturerObj->active = 1;
//                foreach ($manufacturerShopLang['manufacturers'] as $lang) {
//                    if ($lang['id_manufacturer'] == $manufacturer['id_manufacturer']) {
//                        $lang['id_lang'] = self::getLanguageID($lang['id_lang']);
//                        $manufacturerObj->description[$lang['id_lang']] = $lang['description'];
//                        $manufacturerObj->short_description[$lang['id_lang']] = $lang['short_description'];
//                        $manufacturerObj->meta_title[$lang['id_lang']] = $lang['meta_title'];
//                        $manufacturerObj->meta_description[$lang['id_lang']] = $lang['meta_description'];
//                        $manufacturerObj->meta_keywords[$lang['id_lang']] = $lang['meta_keywords'];
//                    }
//                }
                // Add to _shop relations
                $manufacturerObj->id_shop_list = Shop::getCompleteListOfShopsID();

                $res = false;
                $err_tmp = '';
                $this->validator->setObject($manufacturerObj);
                $this->validator->checkFields();
                $manufacturer_error_tmp = $this->validator->getValidationMessages();
                if ($manufacturerObj->id && $manufacturerObj->manufacturerExists($manufacturerObj->id)) {
                    try {
                        $res = $manufacturerObj->update();
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    try {
                        $res = $manufacturerObj->add(false);
                        $count++;
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }
                if (!$res) {
                    $this->showMigrationMessageAndLog(sprintf('Manufacturer (ID: %1$s) cannot be saved. %2$s', (isset($manufacturer['id_manufacturer']) && !empty($manufacturer['id_manufacturer'])) ? Tools::safeOutput($manufacturer['id_manufacturer']) : 'No ID', $err_tmp), 'Manufacturer');
                } else {
                    $url = $this->url . $this->image_path . (int)$manufacturer['id_manufacturer'] . '.jpg';
                    if (self::imageExits($url) && !(MageImport::copyImg($manufacturerObj->id, null, $url, 'manufacturers', $this->regenerate))) {
                        $this->warning_msg[] = $url . ' ' . Tools::displayError('cannot be copied.');
                    }

                    self::addLog('Manufacturer', (int)$manufacturer['id_manufacturer'], $manufacturerObj->id);
                }
                $this->showMigrationMessageAndLog($manufacturer_error_tmp, 'Manufacturer');
            }
        }

        $this->updateProcess($count);
    }

    public function categories($categories, $categoriesAdditionalSecond, $storeAndWebsiteIds, $innerMethodCall = false)
    {
        self::saveAttributeIds($categoriesAdditionalSecond['all_category_attributes']);
        foreach ($categories as $category) {
            $link_original = array();
            $rootCategoryId = explode('/', $category['path'])[1];
            $categoryShopId = array();
            $continueLoop = true;
            foreach ($storeAndWebsiteIds as $store) {
                if ($store['root_category_id'] == $rootCategoryId) {
                    if (!in_array(self::getShopID($store['website_id']), $categoryShopId)) {
                        $categoryShopId[] = (int)self::getShopID($store['website_id']);
                    }
                    $continueLoop = false;
                }
            }
            if ($continueLoop) {
                continue;
            }


            if ($categoryObj = $this->createObjectModel('Category', (int)$category['id_category'])) {
                foreach ($categoriesAdditionalSecond['category_active_value'] as $active) {
                    if ($category['id_category'] == $active['id_category']) {    // checks if category ids are equal
                        $categoryObj->active = (bool)$active['active'];
                    }
                }
                if (isset($category['id_parent']) && (int)$category['id_parent'] != 1) {
                    if (!Category::categoryExists(self::getLocalId('category', (int)$category['id_parent'], 'data'))) {
                        // -- if parent category not exist create it
                        $this->client->serializeOff();
                        $this->client->setPostData($this->query->getAllAttributes('3'));
                        if ($this->client->query()) {
                            $attributes = $this->client->getContent();
                            $this->query->saveAttributeIds($attributes);
                            $this->client->setPostData($this->query->parentCategoriesStoresWebsites());
                            if ($this->client->query()) {
                                $parentRootCategoriesStoresWebsites = $this->client->getContent();
                                $this->client->setPostData($this->query->singleCategory($category['id_parent']));
                                if ($this->client->query()) {
                                    $parentCategory = $this->client->getContent();
                                    $this->client->serializeOn();
                                    $this->client->setPostData($this->query->categorySqlSecond(AdminMageMigrationProController::getCleanIDs($parentCategory, 'id_category')));
                                    if ($this->client->query()) {
                                        $parentCategoryAdditionalSecond = $this->client->getContent();

                                        $import = new MageImport($this->process, $this->version, $this->url, $this->force_ids, $this->client, $this->query);
                                        $import->setImagePath($this->image_path);

                                        $import->categories($parentCategory, $parentCategoryAdditionalSecond, $parentRootCategoriesStoresWebsites, true);
                                        $this->errors = $import->getErrorMsg();
                                        $this->warnings = $import->getWarningMsg();
                                        $this->response = $import->getResponse();
                                    } else {
                                        $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                                    }
                                } else {
                                    $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                                }
                            } else {
                                $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                            }
                        } else {
                            $this->errors[] = $this->l('Can\'t execute query to source Shop. ' . $this->client->getMessage());
                        }
                    }
                    $categoryObj->id_parent = self::getLocalId('category', (int)$category['id_parent'], 'data');
                } else {
                    $categoryObj->id_parent = Configuration::get('PS_HOME_CATEGORY');
                }

                $categoryObj->id_parent = self::getLocalId('category', (int)$category['id_parent'], 'data');
                $categoryObj->id_parent = $categoryObj->id_parent ? $categoryObj->id_parent : Configuration::get('PS_HOME_CATEGORY');
                $categoryObj->position = (int)$category['position'];
                $categoryObj->date_add = $category['date_add'];
                $categoryObj->date_upd = $category['date_upd'];

                foreach ($categoriesAdditionalSecond['category_name'] as $categoryName) {
                    if ($categoryName['entity_id'] == $category['id_category']) {
                        if ($categoryName['store_id'] == 0) {
                            $lang_id = Configuration::get('PS_LANG_DEFAULT');
                        } else {
                            $lang_id = self::getLanguageID((int)$categoryName['store_id']);
                        }
                        if ($categoryName['attribute_id'] == $this->name_id) {
                            $categoryObj->name[$lang_id] = pSQL($categoryName['value']);
                        }
                    }
                }

                foreach ($categoriesAdditionalSecond['category_link_rewrite_meta_title'] as $categoryLinkRewriteMetaTitle) {
                    if ($categoryLinkRewriteMetaTitle['entity_id'] == $category['id_category']) {
                        if ($categoryLinkRewriteMetaTitle['store_id'] == 0) {
                            $lang_id = Configuration::get('PS_LANG_DEFAULT');
                        } else {
                            $lang_id = self::getLanguageID((int)$categoryLinkRewriteMetaTitle['store_id']);
                        }
                        if ($categoryLinkRewriteMetaTitle['attribute_id'] == $this->meta_title_id) {
                            $categoryObj->meta_title[$lang_id] = Tools::substr(pSQL($categoryLinkRewriteMetaTitle['value']), 0, 128);
                        } elseif ($categoryLinkRewriteMetaTitle['attribute_id'] == $this->url_key_id) {
                            $categoryObj->link_rewrite[$lang_id] = pSQL($categoryLinkRewriteMetaTitle['value']);
                            $link_original[] = pSQL($categoryLinkRewriteMetaTitle['value']);
                        }

                        if (isset($categoryObj->link_rewrite[$lang_id]) && !empty($categoryObj->link_rewrite[$lang_id])) {
                            $valid_link = Validate::isLinkRewrite($categoryObj->link_rewrite[$lang_id]);
                        } else {
                            $valid_link = false;
                        }
                        if (!$valid_link) {
                            if (!isset($categoryObj->name[$lang_id]) && empty($categoryObj->name[$lang_id])) {
                                continue;
                            }
                            $categoryObj->link_rewrite[$lang_id] = Tools::link_rewrite($categoryObj->name[$lang_id]);

                            if ($categoryObj->link_rewrite[$lang_id] == '') {
                                $categoryObj->link_rewrite[$lang_id] = 'friendly-url-autogeneration-failed';
                                $this->warning_msg[] = sprintf('URL rewriting failed to auto-generate a friendly URL for: %s', $categoryObj->name[$categoryName['store_id']]);
                            }

                            $this->warning_msg[] = sprintf('Rewrite link for %1$s (ID: %2$s) was re-written as %3$s.', $categoryLinkRewriteMetaTitle['value'], (isset($category['id_category']) && !empty($category['id_category'])) ? $category['id_category'] : 'null', $categoryObj->link_rewrite[$lang_id]);
                        }
//                        }
                    }
                }

                foreach ($categoriesAdditionalSecond['category_description_meta_description_meta_keywords'] as $desctiptionAndMetaLangs) {
                    if ($desctiptionAndMetaLangs['entity_id'] == $category['id_category']) {
                        if ($desctiptionAndMetaLangs['store_id'] == 0) {
                            $desctiptionAndMetaLangs['store_id'] = Configuration::get('PS_LANG_DEFAULT');
                        } else {
                            $desctiptionAndMetaLangs['store_id'] = self::getLanguageID((int)$desctiptionAndMetaLangs['store_id']);
                        }
                        if ($desctiptionAndMetaLangs['attribute_id'] == $this->meta_description_id) {
                            $categoryObj->meta_description[$desctiptionAndMetaLangs['store_id']] = ValidateCore::isGenericName(Tools::substr(pSQL($desctiptionAndMetaLangs['value']), 0, 255)) ? Tools::substr(pSQL($desctiptionAndMetaLangs['value']), 0, 255) : 'empty (PrestaShop does not validate this field.)';
                        } elseif ($desctiptionAndMetaLangs['attribute_id'] == $this->meta_title_id) {
                            $categoryObj->meta_title[$desctiptionAndMetaLangs['store_id']] = Tools::substr(pSQL($desctiptionAndMetaLangs['value']), 0, 128);
                        } elseif ($desctiptionAndMetaLangs['attribute_id'] == $this->meta_keywords_id) {
                            $categoryObj->meta_keywords[$desctiptionAndMetaLangs['store_id']] = Tools::substr(pSQL($desctiptionAndMetaLangs['value']), 0, 255);
                        } elseif ($desctiptionAndMetaLangs['attribute_id'] == $this->description_id) {
                            $categoryObj->description[$desctiptionAndMetaLangs['store_id']] = Tools::htmlentitiesDecodeUTF8($desctiptionAndMetaLangs['value']);
                        }
                    }
                }


                $categoryObj->id_shop_list = $categoryShopId;

                if (!$this->shop_is_feature_active) {
                    $categoryObj->id_shop_default = 1;
                } else {
                    $categoryObj->id_shop_default = 5;
                }

                $res = false;
                $err_tmp = '';

                $this->validator->setObject($categoryObj);
                $this->validator->checkFields();
                $category_error_tmp = $this->validator->getValidationMessages();
                if ($categoryObj->id && $categoryObj->id == $categoryObj->id_parent) {
                    $this->showMigrationMessageAndLog(Tools::displayError('A category cannot be its own parent'), 'Category');
                    continue;
                }


                /* No automatic nTree regeneration for import */
                $categoryObj->doNotRegenerateNTree = true;
                // If id category AND id category already in base, trying to update
                if ($categoryObj->id && $categoryObj->categoryExists($categoryObj->id)) {
                    try {
                        $res = $categoryObj->update();
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }


                // If no id_category or update failed
                if (!$res) {
                    try {
                        $res = $categoryObj->add(false);
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                        if ($err_tmp == 'Parent category does not exist') {
                            $err_tmp = 'Parent category with ID ' . $categoryObj->id_parent . ' does not exist.';
                        }
                    }
                }
                $this->showMigrationMessageAndLog($category_error_tmp, 'Category');

                // If both failed, mysql error
                if (!$res) {
                    $this->showMigrationMessageAndLog(sprintf('Category (ID: %1$s) cannot be saved. %2$s', (isset($category['id_category']) && !empty($category['id_category'])) ? Tools::safeOutput($category['id_category']) : 'No ID', $err_tmp), 'Category');
                } else {
                    $url = '';
                    foreach ($categoriesAdditionalSecond['category_image'] as $categoryImage) {
                        if ($categoryImage['entity_id'] == $category['id_category']) {
                            $url = $this->url . $this->image_path . '/' . pSQL($categoryImage['value']);
                        }
                    }

                    if (self::imageExits($url) && !(MageImport::copyImg($categoryObj->id, null, $url, 'categories', $this->regenerate))) {
                        $this->warning_msg[] = $url . ' ' . Tools::displayError('cannot be copied.');
                    }

                    self::addLog('Category', $category['id_category'], $categoryObj->id);

                    foreach ($link_original as $langId => $link_rewrite) {
                         MageLinkRewrite::importLinkRewrite($categoryObj->id, $link_rewrite, 'category');
                    }
                }

                if (empty($categoryShopId)) {
                    continue;
                }

                DB::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'category_shop WHERE id_category = ' . (int)$categoryObj->id . ' AND id_shop
                NOT IN (' . implode(',', array_values($categoryShopId)) . ')');
            }
        }

        if (!$innerMethodCall) {
            $this->updateProcess(count($categories));
        }
        Category::regenerateEntireNtree();
    }

    public function products($products, $productAdditionalSecond, $productAdditionalThird, $customAttribute, $allCustomAttributeOptionsValues)
    {
        Module::setBatchMode(true);
        self::saveAttributeIds($productAdditionalSecond['all_attributes']);
        $colors = require 'colors.php';

        // import attribute group
        foreach ($productAdditionalSecond['attribute_group'] as $attributeGroup) {
            if ($attributeGroupObj = $this->createObjectModel('AttributeGroup', (int)$attributeGroup['attribute_id'])) {
                if ($attributeGroup['attribute_id'] == $this->color_id) {
                    $attributeGroupObj->is_color_group = 1;
                } else {
                    $attributeGroupObj->is_color_group = 0;
                }
                $attributeGroupObj->group_type = ($attributeGroupObj->is_color_group) ? 'color' : 'select';
                foreach ($productAdditionalSecond['attribute_group_lang'] as $lang) {
                    if ($attributeGroup['attribute_id'] == $lang['attribute_id']) {
                        $lang['id_lang'] = Configuration::get('PS_LANG_DEFAULT');
//
                        $attributeGroupObj->name[$lang['id_lang']] = pSQL($lang['attribute_code']);
                        $attributeGroupObj->public_name[$lang['id_lang']] = pSQL($lang['frontend_label']);
                    }
                }

                // Add to _shop relations
                $attributeGroupObj->id_shop_list = array(Configuration::get('PS_SHOP_DEFAULT'));

                $res = false;
                $err_tmp = '';

                $this->validator->setObject($attributeGroupObj);
                $this->validator->checkFields();
                $attribute_group_error_tmp = $this->validator->getValidationMessages();
                if ($attributeGroupObj->id && AttributeGroup::existsInDatabase($attributeGroupObj->id, 'attribute_group')) {
                    try {
                        $res = $attributeGroupObj->update();
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    try {
                        $res = $attributeGroupObj->add(false);
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    $this->showMigrationMessageAndLog(sprintf('AttributeGroup (ID: %1$s) cannot be saved. %2$s', (isset($attributeGroup['id_attribute_group']) && !empty($attributeGroup['id_attribute_group'])) ? Tools::safeOutput($attributeGroup['id_attribute_group']) : 'No ID', $err_tmp), 'AttributeGroup');
                } else {
                    self::addLog('AttributeGroup', $attributeGroup['attribute_id'], $attributeGroupObj->id);
                }
                $this->showMigrationMessageAndLog($attribute_group_error_tmp, 'AttributeGroup');
            }
        }
        // import attribute
        foreach ($productAdditionalThird['attribute'] as $attribute) {
            if ($attributeObj = $this->createObjectModel('Attribute', $attribute['option_id'])) {
                $attributeObj->id_attribute_group = self::getLocalID('attributegroup', (int)$attribute['attribute_id'], 'data');
                if ($this->version == 1) {
                    foreach ($colors as $colorName => $colorHexCode) {
                        if (!strcasecmp($colorName, $attribute['value'])) {
                            $attributeObj->color = pSQL('#' . $colorHexCode);
                        }
                    }
                } else {
                    if (preg_match('|\#|', $attribute['color'])) {
                        $attributeObj->color = pSQL($attribute['color']);
                    } else {
                        $attributeObj->color = null;
                    }
                }
                foreach ($productAdditionalThird['attribute_lang'] as $lang) {
                    if ($attribute['option_id'] == $lang['option_id']) {
                        if ($lang['store_id'] == 0) {
                            $lang['id_lang'] = Configuration::get('PS_LANG_DEFAULT');
                        } else {
                            $lang['id_lang'] = self::getLanguageID((int)$lang['store_id']);
                        }
                        $attributeObj->name[$lang['id_lang']] = pSQL($lang['value']);
                    }
                }

                // Add to _shop relations
                $attributeObj->id_shop_list = Shop::getCompleteListOfShopsID();

                if (empty($attributeObj->name[Configuration::get('PS_LANG_DEFAULT')])) {
                    continue;
                }
                $res = false;
                $err_tmp = '';

                $this->validator->setObject($attributeObj);
                $this->validator->checkFields();
                $attribute_error_tmp = $this->validator->getValidationMessages();
                if ($attributeObj->id && Attribute::existsInDatabase($attributeObj->id, 'attribute')) {
                    try {
                        $res = $attributeObj->update();
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    try {
                        $res = $attributeObj->add(false);
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    $this->showMigrationMessageAndLog(sprintf('Attribute (ID: %1$s) cannot be saved. %2$s', (isset($attribute['option_id']) && !empty($attribute['option_id'])) ? Tools::safeOutput($attribute['option_id']) : 'No ID', $err_tmp), 'Attribute');
                } else {
                    self::addLog('Attribute', $attribute['option_id'], $attributeObj->id);
                }
                $this->showMigrationMessageAndLog($attribute_error_tmp, 'Attribute');
            }
        }

        // import feature
        foreach ($customAttribute as $feature) {
            if ($featureObj = $this->createObjectModel('Feature', (int)$feature['attribute_id'])) {
                $featureObj->position = Feature::getHigherPosition() + 1;

                $langId = Configuration::get('PS_LANG_DEFAULT');
                $featureObj->name[$langId] = pSQL($feature['frontend_label']);

                // Add to _shop relations
                $featureObj->id_shop_list = Shop::getCompleteListOfShopsID();

                $res = false;
                $err_tmp = '';

                $this->validator->setObject($featureObj);
                $this->validator->checkFields();
                $feature_error_tmp = $this->validator->getValidationMessages();
                if ($featureObj->id && Feature::existsInDatabase($featureObj->id, 'feature')) {
                    try {
                        $res = $featureObj->update();
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    try {
                        $res = $featureObj->add(false);
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    $this->showMigrationMessageAndLog(sprintf('Feature (ID: %1$s) cannot be saved. %2$s', (isset($feature['attribute_id']) && !empty($feature['attribute_id'])) ? Tools::safeOutput($feature['attribute_id']) : 'No ID', $err_tmp), 'Feature');
                } else {
                    self::addLog('Feature', $feature['attribute_id'], $featureObj->id);
                }
                $this->showMigrationMessageAndLog($feature_error_tmp, 'Feature');
            }
        }

        // import feature value
        foreach ($allCustomAttributeOptionsValues as $featureValue) {
            if ($featureValueObj = $this->createObjectModel('FeatureValue', (int)$featureValue['option_id'])) {
                $featureValueObj->id_feature = self::getLocalId('feature', (int)$featureValue['attribute_id'], 'data');
                $featureValueObj->custom = 0;
                if ($featureValue['store_id'] == 0) {
                    $langId = Configuration::get('PS_LANG_DEFAULT');
                } else {
                    $langId = self::getLanguageID((int)$featureValue['store_id']);
                }
                $featureValueObj->value[$langId] = pSQL($featureValue['value']);

                $res = false;
                $err_tmp = '';

                $this->validator->setObject($featureValueObj);
                $this->validator->checkFields();
                $feature_value_error_tmp = $this->validator->getValidationMessages();
                if ($featureValueObj->id && FeatureValue::existsInDatabase($featureValueObj->id, 'feature_value')) {
                    try {
                        $res = $featureValueObj->update();
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    try {
                        $res = $featureValueObj->add(false);
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    $this->showMigrationMessageAndLog(sprintf('FeatureValue (ID: %1$s) cannot be saved. %2$s', (isset($featureValue['option_id']) && !empty($featureValue['option_id'])) ? Tools::safeOutput($featureValue['option_id']) : 'No ID', $err_tmp), 'FeatureValue');
                } else {
                    self::addLog('FeatureValue', $featureValue['option_id'], $featureValueObj->id);
                }
                $this->showMigrationMessageAndLog($feature_value_error_tmp, 'FeatureValue');
            }
        }

        foreach ($productAdditionalThird['tag'] as $tag) {
            if ($tagObject = $this->createObjectModel('Tag', (int)$tag['tag_id'])) {
//                $tagObject->id_lang = self::getLanguageID($tag['id_lang']);
//                $tagObject->name = $tag['name'];

                foreach (MageMigrationProMapping::listMapping(true, true)['languages'] as $sourceId => $localID) {
                    $tagObject->id_lang = (int)$localID;
                }
                $tagObject->name = pSQL($tag['name']);

                $res = false;
                $err_tmp = '';

                $this->validator->setObject($tagObject);
                $this->validator->checkFields();
                $tag_error_tmp = $this->validator->getValidationMessages();
                if ($tagObject->id && Tag::existsInDatabase($tagObject->id, 'tag')) {
                    try {
                        $res = $tagObject->update();
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    try {
                        $res = $tagObject->add(false);
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    $this->showMigrationMessageAndLog(sprintf('Tag (ID: %1$s) cannot be saved. %2$s', (isset($tag['id_tag']) && !empty($tag['id_tag'])) ? Tools::safeOutput($tag['id_tag']) : 'No ID', $err_tmp), 'Tag');
                } else {
                    self::addLog('Tag', $tag['tag_id'], $tagObject->id);
                }
                $this->showMigrationMessageAndLog($tag_error_tmp, 'Tag');
            }
        }
        // import Products
        foreach ($products as $product) {
            $link_original = array();
            if ($productObj = $this->createObjectModel('Product', (int)$product['entity_id'])) {
                foreach ($productAdditionalSecond['all_product_attributes'] as $productManufacturer) {
                    if ($productManufacturer['entity_id'] == $product['entity_id'] && $productManufacturer['attribute_id'] == $this->manufacturer_id) {
                        $productObj->id_manufacturer = self::getLocalID('manufacturer', (int)$productManufacturer['value'], 'data');
                    }
                }
                $productObj->reference = Tools::substr(pSQL($product['sku']), 0, 32);
                $productObj->location = isset($product['location']) ? pSQL($product['location']) : null;
                $productObj->width = isset($this->width_id) ? self::getAttributeOptionValue((int)$product['entity_id'], (int)$this->width_id, $productAdditionalSecond['all_product_attributes'], $productAdditionalThird['attribute_option_values']) : 0;
                $productObj->height = isset($this->height_id) ? self::getAttributeOptionValue((int)$product['entity_id'], (int)$this->height_id, $productAdditionalSecond['all_product_attributes'], $productAdditionalThird['attribute_option_values']) : null;
                $productObj->depth = isset($this->height_id) ? self::getAttributeOptionValue((int)$product['entity_id'], (int)$this->depth_id, $productAdditionalSecond['all_product_attributes'], $productAdditionalThird['attribute_option_values']) : null;
                $productObj->weight = self::getAttributeOptionValue((int)$product['entity_id'], (int)$this->weight_id, $productAdditionalSecond['product_weight_price_special_price_cost']);
//                $productObj->quantity_discount = $product['quantity_discount'];
//                $productObj->ean13 = $product['ean13'];
                if (!empty($productAdditionalSecond['product_ean'])) {
                    foreach ($productAdditionalSecond['product_ean'] as $ean) {
                        if ($ean['entity_id'] == $product['entity_id']) {
                            $productObj->ean13 = $ean['value'];
                        }
                    }
                }
//                $productObj->upc = $product['upc'];
//                $productObj->cache_is_pack = $product['cache_is_pack'];
//                $productObj->cache_has_attachments = $product['cache_has_attachments'];
                foreach ($productAdditionalSecond['category_product'] as $productCategory) {
                    if ($productCategory['product_id'] == $product['entity_id']) {
                        $productObj->id_category_default = self::getLocalId('category', (int)$productCategory['category_id'], 'data');
                        if ($productObj->id_category_default != 0) {
                            break;
                        }
                    }
                }
                foreach ($productAdditionalSecond['product_tax_rule_groups'] as $productTaxRuleGroup) {
                    if ($productTaxRuleGroup['entity_id'] == $product['entity_id']) {
                        $productObj->id_tax_rules_group = self::getLocalId('taxRulesGroup', (int)$productTaxRuleGroup['value'], 'data');
                        break;
                    }
                }
                foreach ($productAdditionalSecond['products_quantity'] as $productQuantity) {
                    if ($productQuantity['product_id'] == $product['entity_id']) {
                        if (!empty($productQuantity['min_sale_qty'])) {
                            $productObj->minimal_quantity = ceil((int)$productQuantity['min_sale_qty']);
                        } else {
                            $productObj->minimal_quantity = 1;
                        }
                        if ($productQuantity['backorders'] == 0) {
                            $productObj->out_of_stock = 0;
                        } elseif ($productQuantity['backorders'] == 1) {
                            $productObj->out_of_stock = 1;
                        } else {
                            $productObj->out_of_stock = 2;
                        }
                        $productObj->quantity = (int)$productQuantity['qty'];
                    }
                }

                //Check magento version whether price include tax or not
                if (!Tools::isEmpty($productObj->id_tax_rules_group) && Configuration::get('magemigrationpro_version') == 1) {
                    $taxRate = self::getTaxRate($productObj->id_tax_rules_group);
                    $productObj->price = number_format((self::getAttributeOptionValue((int)$product['entity_id'], (int)$this->price_id, $productAdditionalSecond['product_weight_price_special_price_cost']) / (number_format($taxRate)/100+1)), 2);
                } else {
                    $productObj->price = self::getAttributeOptionValue((int)$product['entity_id'], (int)$this->price_id, $productAdditionalSecond['product_weight_price_special_price_cost']);
                }
                $totalPrice = self::getAttributeOptionValue((int)$product['entity_id'], (int)$this->price_id, $productAdditionalSecond['product_weight_price_special_price_cost']);

                if (Tools::isEmpty($productObj->price)) {
                    $productObj->price = 0;
                }
                $productObj->wholesale_price = self::getAttributeOptionValue((int)$product['entity_id'], (int)$this->cost_id, $productAdditionalSecond['product_weight_price_special_price_cost']);
//                $productObj->unity = (int)$product['unity'];
//                $productObj->unit_price_ratio = (int)$product['unit_price_ratio'];
//                $productObj->additional_shipping_cost = $product['additional_shipping_cost'];
//                $productObj->customizable = $product['customizable'];
//                $productObj->text_fields = $product['text_fields'];
//                $productObj->uploadable_files = $product['uploadable_files'];
                $productObj->active = (self::getProductStatus($productAdditionalSecond['product_status'], $product['entity_id']) == 1) ? 1 : 0;
                $productObj->available_for_order = 1;
                foreach ($productAdditionalSecond['all_product_attributes'] as $productEntityIntAttributes) {
                    $this->new_id = isset($this->new_id) ? $this->new_id : null;
                    if ((int)$productEntityIntAttributes['entity_id'] == (int)$product['entity_id'] && (int)$productEntityIntAttributes['attribute_id'] == (int)$this->new_id) {
                        if ($productEntityIntAttributes['value'] == 1) {
                            $productObj->condition = 'new';
                        } else {
                            $productObj->condition = 'used';
                        }
                        break;
                    }
                }
                $productObj->show_price = 1;
                $productObj->indexed = 0; // always zero for new PS $product['indexed'];
                $productObj->cache_default_attribute = isset($product['cache_default_attribute']) ? (int)$product['cache_default_attribute'] : null;
                $productObj->date_add = date('Y-m-d H:i:s');
                $productObj->date_upd = date('Y-m-d H:i:s');
//                    $productObj->isbn = $product['isbn'];
                $productObj->is_virtual = isset($product['is_virtual']) ? (int)$product['is_virtual'] : null;
//                    $productObj->redirect_type = '404';
//                    $productObj->id_product_redirected = 0;
//                    $productObj->available_date = $product['available_date'];
//                    $productObj->show_condition = $product['show_condition'];
//                    if($product['available_date'] == '')
                foreach ($productAdditionalSecond['all_product_attributes'] as $allProductAttributes) {
                    if ($allProductAttributes['entity_id'] == $product['entity_id'] && $allProductAttributes['attribute_id'] == $this->visibility_id) {
                        switch ($allProductAttributes['value']) {
                            case 1:
//                                $productObj->visibility = 'none';
                                $productObj->visibility = 'both';
                                break;
                            case 2:
                                $productObj->visibility = 'catalog';
                                break;
                            case 3:
                                $productObj->visibility = 'search';
                                break;
                            case 4:
                                $productObj->visibility = 'both';
                                break;
                        }
                        break;
                    }
                }

                $productShopId = array();
                foreach ($productAdditionalSecond['product_website'] as $website) {
                    if ($website['product_id'] == $product['entity_id']) {
                        $productShopId[] = self::getShopID((int)$website['website_id']);
                    }
                }
                foreach ($productAdditionalThird['product_attribute_shop'] as $productsAttributesShop) {
                    if ($productsAttributesShop['parent_id'] == $product['entity_id']) {
                        if (!in_array(self::getShopID($productsAttributesShop['website_id']), $productShopId)) {
                            $productShopId[] = self::getShopID((int)$productsAttributesShop['website_id']);
                        }
                    }
                }
                // Add to _shop relations

                $productObj->id_shop_list = $productShopId;
                $productObj->id_shop_default = isset($productShopId[0]) ? $productShopId[0] : 0;

                //language fields
                foreach ($productAdditionalSecond['product_name_meta_title_meta_description_link_rewrite'] as $nameMetaTitleDesc) {
                    $linkRewrite = false;
                    if ($nameMetaTitleDesc['entity_id'] == $product['entity_id']) {
                        if ($nameMetaTitleDesc['store_id'] == 0) {
                            $nameMetaTitleDesc['store_id'] = Configuration::get('PS_LANG_DEFAULT');
                        } else {
                            $nameMetaTitleDesc['store_id'] = self::getLanguageID((int)$nameMetaTitleDesc['store_id']);
                        }
                        switch ($nameMetaTitleDesc['attribute_id']) {
                            case $this->name_id:
                                $productObj->name[$nameMetaTitleDesc['store_id']] = preg_replace('/<|>|;|=|\#|\{|\}/', '', str_replace("\\'", "'", pSQL($nameMetaTitleDesc['value'])));
                                break;
                            case $this->meta_title_id:
                                $productObj->meta_title[$nameMetaTitleDesc['store_id']] = pSQL($nameMetaTitleDesc['value']);
                                break;
                            case $this->meta_description_id:
                                $productObj->meta_description[$nameMetaTitleDesc['store_id']] = preg_replace('/<|>|=|\{|\}/', '', Tools::htmlentitiesDecodeUTF8(str_replace("\\r\\n", "\n", str_replace("\\'", "'", Tools::substr(pSQL($nameMetaTitleDesc['value']), 0, 255)))));
                                break;
                            case $this->url_key_id:
                                $productObj->link_rewrite[$nameMetaTitleDesc['store_id']] = pSQL($nameMetaTitleDesc['value']);
                                $linkRewrite = true;
                                $link_original[] = pSQL($nameMetaTitleDesc['value']);
                                break;
                        }
                        if ($linkRewrite) {
                            if (isset($productObj->link_rewrite[$nameMetaTitleDesc['store_id']]) && !empty($productObj->link_rewrite[$nameMetaTitleDesc['store_id']])) {
                                $valid_link = Validate::isLinkRewrite($productObj->link_rewrite[(int)$nameMetaTitleDesc['store_id']]);
                            } else {
                                $valid_link = false;
                            }
                            if (!$valid_link) {
                                if (!isset($productObj->name[(int)$nameMetaTitleDesc['store_id']])) {
                                    continue;
                                }
                                $productObj->link_rewrite[(int)$nameMetaTitleDesc['store_id']] = Tools::link_rewrite($productObj->name[(int)$nameMetaTitleDesc['store_id']]);

                                if ($productObj->link_rewrite[(int)$nameMetaTitleDesc['store_id']] == '') {
                                    $productObj->link_rewrite[(int)$nameMetaTitleDesc['store_id']] = 'friendly-url-autogeneration-failed';
                                    $this->warning_msg[] = sprintf('URL rewriting failed to auto-generate a friendly URL for: %s', $productObj->name[(int)$nameMetaTitleDesc['store_id']]);
                                }

                                $this->warning_msg[] = sprintf('Rewrite link for product (ID: %2$s) was re-written as %3$s.', null, (isset($product['entity_id']) && !empty((int)$product['entity_id'])) ? (int)$product['entity_id'] : 'null', $productObj->link_rewrite[(int)$nameMetaTitleDesc['store_id']]);
                            }
                        }
//                        $productObj->meta_description[$nameMetaTitleDesc['id_lang']] = $nameMetaTitleDesc['meta_description'];
//                        $productObj->meta_keywords[$nameMetaTitleDesc['id_lang']] = $nameMetaTitleDesc['meta_keywords'];
//                        $productObj->meta_title[$nameMetaTitleDesc['id_lang']] = $nameMetaTitleDesc['meta_title'];
//                        $productObj->name[$nameMetaTitleDesc['id_lang']] = $nameMetaTitleDesc['name'];

                        /*$productObj->available_now[(int)$nameMetaTitleDesc['store_id']] = $nameMetaTitleDesc['available_now'];
                        $productObj->available_later[(int)$nameMetaTitleDesc['store_id']] = $nameMetaTitleDesc['available_later'];*/
                    }
                }

                foreach ($productAdditionalThird['product_meta_keyword_description_short_description'] as $metaKeywordShortDesc) {
                    if ($metaKeywordShortDesc['entity_id'] == $product['entity_id']) {
                        if ($metaKeywordShortDesc['store_id'] == 0) {
                            $metaKeywordShortDesc['store_id'] = Configuration::get('PS_LANG_DEFAULT');
                        } else {
                            $metaKeywordShortDesc['store_id'] = self::getLanguageID((int)$metaKeywordShortDesc['store_id']);
                        }
                        switch ($metaKeywordShortDesc['attribute_id']) {
                            case $this->meta_keyword_id:
                                $productObj->meta_keywords[$metaKeywordShortDesc['store_id']] = Tools::substr(pSQL($metaKeywordShortDesc['value']), 0, 255);
                                break;
                            case $this->description_id:
                                $productObj->description[$metaKeywordShortDesc['store_id']] = Tools::htmlentitiesDecodeUTF8($metaKeywordShortDesc['value']);
                                break;
                            case $this->short_description_id:
                                $productObj->description_short[$metaKeywordShortDesc['store_id']] = Tools::htmlentitiesDecodeUTF8($metaKeywordShortDesc['value']);
                                break;
                        }
                    }
                }

                $res = false;
                $err_tmp = '';

                $this->validator->setObject($productObj);
                $this->validator->checkFields();
                $product_error_tmp = $this->validator->getValidationMessages();
                if ($productObj->id && Product::existsInDatabase((int)$productObj->id, 'product')) {
                    try {
                        $res = $productObj->update();
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    try {
                        $res = $productObj->add(false);
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }
                if (!$res) {
                    $this->showMigrationMessageAndLog(sprintf('Product (ID: %1$s) cannot be saved. %2$s', (isset($product['entity_id']) && !empty($product['entity_id'])) ? Tools::safeOutput($product['entity_id']) : 'No ID', $err_tmp), 'Product');
                } else {
                    // set quantity to StockAvailable
                    $productStockAvailableQuantity = 0;
                    foreach ($productAdditionalSecond['product_attribute'] as $productAttributeQuantities) {
                        if ($product['entity_id'] == $productAttributeQuantities['id_product']) {
                            $productStockAvailableQuantity += (int)$productAttributeQuantities['quantity'];
                        }
                    }
                    if ($productStockAvailableQuantity == 0) {
                        $productStockAvailableQuantity = $productObj->quantity;
                        StockAvailable::setQuantity($productObj->id, 0, $productStockAvailableQuantity, $productObj->id_shop_default);
                        StockAvailable::setProductDependsOnStock($productObj->id, 0, $productObj->id_shop_default);
                        StockAvailable::setProductOutOfStock($productObj->id, $productObj->out_of_stock, $productObj->id_shop_default);
                    }
                    //import Category_Product
                    $sql_values = array();
                    foreach ($productAdditionalSecond['category_product'] as $categoryProduct) {
                        if ($categoryProduct['product_id'] == $product['entity_id']) {
                            $category_local_id = (int)self::getLocalID('category', (int)$categoryProduct['category_id'], 'data');
                            if ((int)$category_local_id != 0) {
                                $sql_values[] = '(' . $category_local_id . ', ' . (int)$productObj->id . ', ' . (int)$categoryProduct['position'] . ')';
                            }
                        }
                    }
                    if (!empty($sql_values)) {
                        $result = Db::getInstance()->execute('REPLACE INTO `' . _DB_PREFIX_ . 'category_product` (`id_category`, `id_product`, `position`) VALUES ' . implode(',', $sql_values));

                        if (!$result) {
                            $this->showMigrationMessageAndLog(Tools::displayError('Can\'t add category_product. ' . Db::getInstance()->getMsgError()), 'Product');
                        }
                    }

                    //import specific price for customer groups
                    $secondsFirst = '00';
                    $secondsSecond = '00';
                    foreach ($productAdditionalSecond['customer_group_prices'] as $specificPriceForGroup) {
                        if ($product['entity_id'] == $specificPriceForGroup['entity_id']) {
                            if ($specificPriceForGroupObj = $this->createObjectModel('SpecificPrice', $specificPriceForGroup['value_id'])) {
                                $specificPriceForGroupObj->id_shop = $productObj->id_shop_default;
                                $specificPriceForGroupObj->id_product = $productObj->id;
                                $specificPriceForGroupObj->id_currency = 0;
                                $specificPriceForGroupObj->id_country = 0;
                                $specificPriceForGroupObj->id_group = self::getCustomerGroupID((int)$specificPriceForGroup['customer_group_id']);
                                $specificPriceForGroupObj->price = -1;
                                if ($this->version == 1) {
                                    $specificPriceForGroupObj->from_quantity = 1;
                                } else {
                                    $specificPriceForGroupObj->from_quantity = (int)$specificPriceForGroup['qty'];
                                }
                                $specificPriceForGroupObj->reduction = empty($totalPrice - (float)$specificPriceForGroup['value']) ? 0 : abs($totalPrice - (float)$specificPriceForGroup['value']);
                                $specificPriceForGroupObj->reduction_type = 'amount';
                                $specificPriceForGroupObj->from = '0000-00-00 00:00:' . ($secondsFirst++ < 10 && $secondsFirst != 0 ? '0' . $secondsFirst : $secondsFirst);
                                $specificPriceForGroupObj->to = '0000-00-00 00:00:' . ($secondsSecond++ < 10 && $secondsSecond != 0 ? '0' . $secondsSecond : $secondsSecond);
                                if (empty($specificPriceForGroupObj->from)) {
                                    $specificPriceForGroupObj->from = '0000-00-00 00:00:' . ($secondsFirst++ < 10 && $secondsFirst != 0 ? '0' . $secondsFirst : $secondsFirst);
                                }
                                if (empty($specificPriceForGroupObj->to)) {
                                    $specificPriceForGroupObj->to = '0000-00-00 00:00:' . ($secondsSecond++ < 10 && $secondsSecond != 0 ? '0' . $secondsSecond : $secondsSecond);
                                }
                                $specificPriceForGroupObj->id_customer = 0;

                                $specificPriceForGroupObj->id_shop_group = Shop::getGroupFromShop($specificPriceForGroupObj->id_shop);

                                $specificPriceForGroupObj->id_cart = 0;
                                $specificPriceForGroupObj->id_product_attribute = 0;
                                $specificPriceForGroupObj->id_specific_price_rule = 0;

                                $specificPriceForGroupObj->reduction_tax = 1;

                                $res = false;
                                $err_tmp = '';

                                $this->validator->setObject($specificPriceForGroupObj);
                                $this->validator->checkFields();
                                $specific_price_for_group_error_tmp = $this->validator->getValidationMessages();
                                if ($specificPriceForGroupObj->id && SpecificPrice::existsInDatabase($specificPriceForGroupObj->id, 'specific_price')) {
                                    try {
                                        $res = $specificPriceForGroupObj->update();
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }

                                if (!$res) {
                                    try {
                                        $res = $specificPriceForGroupObj->add(false);
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }

                                if (!$res) {
                                    if (!preg_match('/Duplicate/i', $err_tmp)) {
                                        $this->showMigrationMessageAndLog(sprintf('SpecificPrice (ID: %1$s) cannot be saved. %2$s', (isset($specificPriceForGroup['value_id']) && !empty((int)$specificPriceForGroup['value_id'])) ? Tools::safeOutput($specificPriceForGroup['value_id']) : 'No ID', $err_tmp), 'SpecificPrice');
                                    }
                                } else {
                                    self::addLog('SpecificPrice', (int)$specificPriceForGroup['value_id'], $specificPriceForGroupObj->id);
                                }
                                $this->showMigrationMessageAndLog($specific_price_for_group_error_tmp, 'SpecificPrice');
                            }
                        }
                    }
                    //import specific tier prices for magento 1.9
                    if (isset($productAdditionalSecond['tier_prices']) && !empty($productAdditionalSecond['tier_prices'])) {
                        foreach ($productAdditionalSecond['tier_prices'] as $specificTierPrice) {
                            if ($product['entity_id'] == $specificTierPrice['entity_id']) {
                                if ($specificTierPriceObj = $this->createObjectModel('SpecificPrice', (int)$specificTierPrice['value_id'])) {
                                    $specificTierPriceObj->id_shop = self::getShopID((int)$specificTierPrice['website_id']);
                                    $specificTierPriceObj->id_product = $productObj->id;
                                    $specificTierPriceObj->id_currency = 0;
                                    $specificTierPriceObj->id_country = 0;
                                    $specificTierPriceObj->id_group = self::getCustomerGroupID((int)$specificTierPrice['customer_group_id']);
                                    $specificTierPriceObj->price = -1;
                                    $specificTierPriceObj->from_quantity = (int)$specificTierPrice['qty'];
                                    $specificTierPriceObj->reduction = empty($totalPrice - (float)$specificTierPrice['value']) ? 0 : abs($totalPrice - (float)$specificTierPrice['value']);
                                    $specificTierPriceObj->reduction_type = 'amount';
                                    $specificTierPriceObj->from = '0000-00-00 00:00:' . ($secondsFirst++ < 10 && $secondsFirst != 0 ? '0' . $secondsFirst : $secondsFirst);
                                    $specificTierPriceObj->to = '0000-00-00 00:00:' . ($secondsSecond++ < 10 && $secondsSecond != 0 ? '0' . $secondsSecond : $secondsSecond);
                                    if (empty($specificTierPriceObj->from)) {
                                        $specificTierPriceObj->from = '0000-00-00 00:00:' . ($secondsFirst++ < 10 && $secondsFirst != 0 ? '0' . $secondsFirst : $secondsFirst);
                                    }
                                    if (empty($specificTierPriceObj->to)) {
                                        $specificTierPriceObj->to = '0000-00-00 00:00:' . ($secondsSecond++ < 10 && $secondsSecond != 0 ? '0' . $secondsSecond : $secondsSecond);
                                    }
                                    $specificTierPriceObj->id_customer = 0;

                                    $specificTierPriceObj->id_shop_group = Shop::getGroupFromShop($specificTierPriceObj->id_shop);

                                    $specificTierPriceObj->id_cart = 0;
                                    $specificTierPriceObj->id_product_attribute = 0;
                                    $specificTierPriceObj->id_specific_price_rule = 0;

                                    $specificTierPriceObj->reduction_tax = 1;

                                    $res = false;
                                    $err_tmp = '';

                                    $this->validator->setObject($specificTierPriceObj);
                                    $this->validator->checkFields();
                                    $specific_tier_price_error_tmp = $this->validator->getValidationMessages();
                                    if ($specificTierPriceObj->id && SpecificPrice::existsInDatabase($specificTierPriceObj->id, 'specific_price')) {
                                        try {
                                            $res = $specificTierPriceObj->update();
                                        } catch (PrestaShopException $e) {
                                            $err_tmp = $e->getMessage();
                                        }
                                    }

                                    if (!$res) {
                                        try {
                                            $res = $specificTierPriceObj->add(false);
                                        } catch (PrestaShopException $e) {
                                            $err_tmp = $e->getMessage();
                                        }
                                    }

                                    if (!$res) {
                                        if (!preg_match('/Duplicate/i', $err_tmp)) {
                                            $this->showMigrationMessageAndLog(sprintf('SpecificPrice (ID: %1$s) cannot be saved. %2$s', (isset($specificTierPrice['value_id']) && !empty($specificTierPrice['value_id'])) ? Tools::safeOutput($specificTierPrice['value_id']) : 'No ID', $err_tmp), 'SpecificPrice');
                                        }
                                    } else {
                                        self::addLog('SpecificPrice', (int)$specificTierPrice['value_id'], $specificTierPriceObj->id);
                                    }
                                    $this->showMigrationMessageAndLog($specific_tier_price_error_tmp, 'SpecificPrice');
                                }
                            }
                        }
                    }
                    //import specific price
                    foreach ($productAdditionalSecond['specific_price'] as $specificPrice) {
                        if ($product['entity_id'] == $specificPrice['entity_id']) {
                            if ($specificPriceObj = $this->createObjectModel('SpecificPrice', (int)$specificPrice['value_id'])) {
                                $specificPriceObj->id_shop = $productObj->id_shop_default;
                                $specificPriceObj->id_product = $productObj->id;
                                $specificPriceObj->id_currency = 0;
                                $specificPriceObj->id_country = 0;
                                $specificPriceObj->id_group = 0;
                                $specificPriceObj->price = -1;
                                $specificPriceObj->from_quantity = 1;
                                $specificPriceObj->reduction = empty($totalPrice - (float)$specificPrice['value']) ? 0 : abs($totalPrice - (float)$specificPrice['value']);
                                $specificPriceObj->reduction_type = 'amount';
                                foreach ($productAdditionalSecond['specific_price_date'] as $specificPriceDate) {
                                    if ($product['entity_id'] == $specificPriceDate['entity_id']) {
                                        if ($specificPriceDate['attribute_id'] == $this->special_from_date_id) {
                                            if (empty($specificPriceDate['value'])) {
                                                $specificPriceObj->from = '0000-00-00 00:00:00';
                                            } else {
                                                $specificPriceObj->from = $specificPriceDate['value'];
                                            }
                                        } elseif ($specificPriceDate['attribute_id'] == $this->special_to_date_id) {
                                            if (empty($specificPriceDate['value'])) {
                                                $specificPriceObj->to = '0000-00-00 00:00:00';
                                            } else {
                                                $specificPriceObj->to = $specificPriceDate['value'];
                                            }
                                        }
                                    }
                                }
                                if (empty($specificPriceObj->from)) {
                                    $specificPriceObj->from = '0000-00-00 00:00:00';
                                }
                                if (empty($specificPriceObj->to)) {
                                    $specificPriceObj->to = '0000-00-00 00:00:00';
                                }
                                $specificPriceObj->id_customer = (isset($specificPrice['id_customer']) && !empty($specificPrice['id_customer'])) ? self::getLocalId('customer', $specificPrice['id_customer'], 'data') : 0;

                                $specificPriceObj->id_shop_group = Shop::getGroupFromShop($specificPriceObj->id_shop);

                                $specificPriceObj->id_cart = 0;
                                $specificPriceObj->id_product_attribute = 0;
                                $specificPriceObj->id_specific_price_rule = 0;

                                $specificPriceObj->reduction_tax = 1;

                                $res = false;
                                $err_tmp = '';

                                $this->validator->setObject($specificPriceObj);
                                $this->validator->checkFields();
                                $specific_price_error_tmp = $this->validator->getValidationMessages();
                                if ($specificPriceObj->id && SpecificPrice::existsInDatabase($specificPriceObj->id, 'specific_price')) {
                                    try {
                                        $res = $specificPriceObj->update();
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }

                                if (!$res) {
                                    try {
                                        $res = $specificPriceObj->add(false);
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }

                                if (!$res) {
                                    if (!preg_match('/Duplicate/i', $err_tmp)) {
                                        $this->showMigrationMessageAndLog(sprintf('SpecificPrice (ID: %1$s) cannot be saved. %2$s', (isset($specificPrice['value_id']) && !empty($specificPrice['value_id'])) ? Tools::safeOutput($specificPrice['value_id']) : 'No ID', $err_tmp), 'SpecificPrice');
                                    }
                                } else {
                                    self::addLog('SpecificPrice', $specificPrice['value_id'], $specificPriceObj->id);
                                }
                                $this->showMigrationMessageAndLog($specific_price_error_tmp, 'SpecificPrice');
                            }
                        }
                    }
                    // import downloadable products
                    foreach ($productAdditionalSecond['product_download'] as $productDownload) {
                        if ($product['entity_id'] == $productDownload['product_id']) {
                            if ($productDownloadObject = $this->createObjectModel('ProductDownload', $productDownload['link_id'])) {
                                $productDownloadObject->id_product = $productObj->id;
                                $productUrlArray = explode('/', $productDownload['link_file']);
                                $productDownloadObject->display_filename = $productUrlArray[count($productUrlArray) - 1];
                                $productDownloadObject->filename = ProductDownload::getNewFilename();
                                $productDownloadObject->date_add = date('Y-m-d H:i:s');
                                $productDownloadObject->date_expiration = null;
                                $productDownloadObject->nb_days_accessible = null;
                                $productDownloadObject->nb_downloadable = null;
                                $productDownloadObject->active = 1;
                                $productDownloadObject->is_shareable = 0;
                                $res = false;
                                $err_tmp = '';

                                $this->validator->setObject($productDownloadObject);
                                $this->validator->checkFields();
                                $product_download_error_tmp = $this->validator->getValidationMessages();
                                if ($productDownloadObject->id && ProductDownload::existsInDatabase($productDownloadObject->id, 'product_download')) {
                                    try {
                                        $res = $productDownloadObject->update();
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }

                                if (!$res) {
                                    try {
                                        $res = $productDownloadObject->add(false);
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }

                                if (!$res) {
                                    $this->warning_msg[] = sprintf('ProductDownload (ID: %1$s) cannot be saved. Product (ID: 
                                                %2$s). %3$s', (isset($productDownload['link_id']) && !empty($productDownload['link_id'])) ? Tools::safeOutput($productDownload['link_id']) : 'No ID', $productObj->id, $err_tmp);
                                } else {
                                    $client = new MageClient($this->url . '/migration_pro/server.php', Configuration::get('migrationpro_token'));
                                    $client->setPostData('media/downloadable/files/links/' . $productDownload['link_file']);
                                    $client->setTimeout(999);
                                    $client->query('file');
                                    file_put_contents(getcwd() . '/../download/' . $productDownloadObject->filename, $client->getContent());

                                    self::addLog('ProductDownload', $productDownload['link_id'], $productDownloadObject->id);
                                    $updateVirtualProduct = new Product($productObj->id);
                                    $updateVirtualProduct->is_virtual = 1;
                                    $updateVirtualProduct->update();
                                }
                                $this->showMigrationMessageAndLog($product_download_error_tmp, 'ProductDownload');
                            }
                        }
                    }
//                        //import images
                    $highestPositions = array();
                    foreach ($productAdditionalThird['image'] as $image) {
//                            if (isset($image['disabled']) && $image['disabled'] != 0) {
//                                continue;
//                            }
                        if ($imageObject = $this->createObjectModel('Image', (int)$image['value_id'])) {
                            if ($product['entity_id'] == $image['entity_id']) {
                                $imageObject->id_product = $productObj->id;
                                foreach ($productAdditionalThird['image_cover'] as $image_cover) {
                                    if ($image_cover['value'] == $image['value'] && $image_cover['entity_id'] == $image['entity_id']) {
                                        $imageObject->cover = true;
                                        $highestPositions[] = array('image_id' => (int)$image['value_id'], 'position' => (int)$image['position']);
                                    } else if ($image_cover['entity_id'] == $image['entity_id']) {
                                        $lastHighestPositionNumber = count($highestPositions) - 1;
                                        if ($image['position'] < $highestPositions[$lastHighestPositionNumber]['position'] && (int)$image['position'] !== 0) {
                                            $imageObject->cover = 1;
                                            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'image SET cover = NULL WHERE id_image = ' . (int)self::getLocalID('image', (int)$highestPositions[$lastHighestPositionNumber]['image_id'], 'data'));
                                            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'image_shop SET cover = NULL WHERE id_image = ' . (int)self::getLocalID('image', (int)$highestPositions[$lastHighestPositionNumber]['image_id'], 'data'));
                                            $highestPositions[] = array('image_id' => (int)$image['value_id'], 'position' => (int)$image['position']);
                                        }
                                    }
                                }

//                                if (empty($highestPositions)) {
//                                    $imageObject->cover = true;
//                                    $highestPositions[] = array('image_id' => (int)$image['value_id'], 'position' => (int)$image['position']);
//                                } else {
//                                    $lastHighestPositionNumber = count($highestPositions) - 1;
//                                    if ($image['position'] < $highestPositions[$lastHighestPositionNumber]['position'] && (int)$image['position'] !== 0) {
//                                        $imageObject->cover = 1;
//                                        Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'image SET cover = NULL WHERE id_image = ' . (int)self::getLocalID('image', (int)$highestPositions[$lastHighestPositionNumber]['image_id'], 'data'));
//                                        Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'image_shop SET cover = NULL WHERE id_image = ' . (int)self::getLocalID('image', (int)$highestPositions[$lastHighestPositionNumber]['image_id'], 'data'));
//                                        $highestPositions[] = array('image_id' => (int)$image['value_id'], 'position' => (int)$image['position']);
//                                    }
//                                }
                            } else {
                                foreach ($productAdditionalSecond['product_attribute'] as $productAttributeImage) {
                                    if ($productAttributeImage['id_product_attribute'] == $image['entity_id'] && $productAttributeImage['id_product'] == $product['entity_id']) {
                                        $imageObject->id_product = $productObj->id;
                                        $imageObject->cover = false;
                                    }
                                }
                            }

                            if (empty($imageObject->id_product)) {
                                continue;
                            }


                            $url = $this->url . $this->image_path . Image::getImgFolderStatic((int)$image['value_id']) . (int)$image['value_id'] . '.jpg';
                            if (!self::imageExits($url)) {
                                $url = $this->url . $this->image_path . pSQL(Tools::strReplaceFirst('.JPG', '.jpg', $image['value']));
                            }
                            if (!self::imageExits($url)) {
                                continue;
                            }
                            $imageObject->position = Image::getHighestPosition($productObj->id) + 1;
                            //language fields
                            if ($image['label'] != null) {
                                if ($image['store_id'] == 0) {
                                    $lang_id = Configuration::get('PS_LANG_DEFAULT');
                                    $imageObject->label[$lang_id] = $image['label'];
                                } else {
                                    $lang_id = self::getLanguageID((int)$image['store_id']);
                                    $imageObject->label[$lang_id] = $imageObject->label[Configuration::get('PS_LANG_DEFAULT')] = $image['label'];
                                }
                            }
                            // Add to _shop relations
                            $imageObject->id_shop_list = $productObj->id_shop_list;

                            $res = false;
                            $err_tmp = '';

                            $this->validator->setObject($imageObject);
                            $this->validator->checkFields();
                            $image_error_tmp = $this->validator->getValidationMessages();
                            if ($imageObject->id && Image::existsInDatabase($imageObject->id, 'image')) {
                                try {
                                    $res = $imageObject->update();
                                } catch (PrestaShopException $e) {
                                    $err_tmp = $e->getMessage();
                                }
                            }

                            if (!$res) {
                                try {
                                    $res = $imageObject->add(false);
                                } catch (PrestaShopException $e) {
                                    $err_tmp = $e->getMessage();
                                }
                            }

                            if (!$res) {
                                $this->showMigrationMessageAndLog(sprintf('Image (ID: %1$s) cannot be saved. %2$s', (isset($image['value_id']) && !empty($image['value_id'])) ? Tools::safeOutput($image['value_id']) : 'No ID', $err_tmp), 'Image');
                            } else {
                                $url = $this->url . $this->image_path . Image::getImgFolderStatic((int)$image['value_id']) . (int)$image['value_id'] . '.jpg';

                                if (!self::imageExits($url)) {
                                    $url = $this->url . $this->image_path . pSQL(Tools::strReplaceFirst('.JPG', '.jpg', $image['value']));
                                }

                                if (self::imageExits($url) && !(MageImport::copyImg($productObj->id, $imageObject->id, $url, 'products', $this->regenerate))) {
                                    $this->warning_msg[] = $url . ' ' . Tools::displayError('cannot be copied.');
                                }

                                self::addLog('Image', $image['value_id'], $imageObject->id);
                            }
                            $this->showMigrationMessageAndLog($image_error_tmp, 'Image');
                        }
                    }

//                        //import Product Attribute
                    $is_default = true;
                    foreach ($productAdditionalSecond['product_attribute'] as $productAttribute) {
                        if ($productAttribute['id_product'] == $product['entity_id']) {
                            if ($combinationModel = $this->createObjectModel('Combination', (int)$productAttribute['id_product_attribute'])) {
                                $combinationModel->id_product = $productObj->id;
                                $combinationModel->location = isset($productAttribute['location']) ? pSQL($productAttribute['location']) : null;
                                foreach ($productAdditionalThird['combination_ean'] as $combinationEan) {
                                    if ($combinationEan['entity_id'] == $productAttribute['id_product_attribute']) {
                                        $combinationModel->ean13 = $combinationEan['value'];
                                    }
                                }
                                $combinationModel->upc = isset($productAttribute['upc']) ? pSQL($productAttribute['upc']) : null;
                                foreach ($productAdditionalSecond['products_quantity'] as $productAttributeQuantity) {
                                    if ($productAttribute['id_product_attribute'] == $productAttributeQuantity['product_id']) {
                                        $combinationModel->quantity = isset($productAttributeQuantity['qty']) ? (int)$productAttributeQuantity['qty'] : null;
                                    }
                                }

                                foreach ($productAdditionalThird['product_attribute_prices'] as $attribute_price) {
                                    if ($attribute_price['entity_id'] == $productAttribute['id_product_attribute']) {
                                        $combinationModel->price = abs((float)Tools::ps_round($totalPrice - (float)$attribute_price['value'], _PS_PRICE_COMPUTE_PRECISION_));
                                    }
                                }
                                $combinationModel->wholesale_price = self::getAttributeOptionValue((int)$productAttribute['id_product_attribute'], (int)$this->cost_id, $productAdditionalSecond['product_weight_price_special_price_cost']);
                                $combinationModel->ecotax = isset($productAttribute['ecotax']) ? (int)$productAttribute['ecotax'] : null;
                                $combinationModel->weight = self::getAttributeOptionValue((int)$productAttribute['id_product_attribute'], (int)$this->weight_id, $productAdditionalSecond['product_weight_price_special_price_cost']);
                                $combinationModel->unit_price_impact = isset($productAttribute['unit_price_impact']) ? (float)$productAttribute['unit_price_impact'] : null;
                                $combinationModel->minimal_quantity = (isset($productAttribute['minimal_quantity']) && !empty($productAttribute['minimal_quantity'])) ? (int)$productAttribute['minimal_quantity'] : 1;
                                $combinationModel->reference = self::getSkuOfProducts($productAdditionalSecond['product_attribute_sku'], $productAttribute['id_product_attribute']);
                                if ($is_default) {
                                    $combinationModel->default_on = 1;
                                    $is_default = false;
                                } else {
                                    $combinationModel->default_on = null;
                                }
                                if ($this->version >= 1.5) {
//                                        $combinationModel->isbn = $productAttribute['isbn'];
//                                        $combinationModel->available_date = $productAttribute['available_date'];
                                }

                                $combinationShopList = array();
                                foreach ($productAdditionalThird['product_attribute_shop'] as $combinationShop) {
                                    if ($combinationShop['product_id'] == $productAttribute['id_product_attribute']) {
                                        if (!in_array(self::getShopID($combinationShop['website_id']), $combinationShopList)) {
                                            $combinationShopList[] = self::getShopID((int)$combinationShop['website_id']);
                                        }
                                    }
                                }
                                // Add to _shop relations
                                $combinationModel->id_shop_list = $combinationShopList;

                                $res = false;
                                $err_tmp = '';

                                $this->validator->setObject($combinationModel);
                                $this->validator->checkFields();
                                $combination_error_tmp = $this->validator->getValidationMessages();
                                if ($combinationModel->id && Combination::existsInDatabase($combinationModel->id, 'product_attribute')) {
                                    try {
                                        $res = $combinationModel->update();
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }

                                if (!$res) {
                                    try {
                                        $res = $combinationModel->add(false);
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }

                                if (!$res) {
                                    $this->showMigrationMessageAndLog(sprintf('Product attribute (ID: %1$s) cannot be saved. %2$s', (isset($productAttribute['id_product_attribute']) && !empty($productAttribute['id_product_attribute'])) ? Tools::safeOutput($combinationModel->id) : 'No ID', $err_tmp), 'Combination');
                                } else {
                                    MageMigrationProDataProductAttributes::import('Combination', $productAttribute['id_product_attribute'], $combinationModel->id);

                                    // set quantity for Combination to StockAvailable
                                    if ($productAttribute['out_of_stock'] == 0) {
                                        $outOfStock = 0;
                                    } elseif ($productAttribute['out_of_stock'] == 1) {
                                        $outOfStock = 1;
                                    } else {
                                        $outOfStock = 2;
                                    }

                                    StockAvailable::setQuantity($combinationModel->id_product, $combinationModel->id, $productAttribute['quantity'], isset($combinationModel->id_shop_list[0]) ? $combinationModel->id_shop_list[0] : 0);
                                    StockAvailable::setProductDependsOnStock($combinationModel->id_product, 0, isset($combinationModel->id_shop_list[0]) ? $combinationModel->id_shop_list[0] : 0, $combinationModel->id);
                                    StockAvailable::setProductOutOfStock($combinationModel->id_product, $outOfStock, isset($combinationModel->id_shop_list[0]) ? $combinationModel->id_shop_list[0] : 0, $combinationModel->id);
                                    //import product_attribute_combination
                                    $sql_values = array();
                                    foreach ($productAdditionalThird['product_attribute_combination'] as $productAttributeCombination) {
                                        if ($productAttributeCombination['id_product_attribute'] == $productAttribute['id_product_attribute']) {
                                            $id_attribute = self::getLocalID('attribute', (int)$productAttributeCombination['id_attribute'], 'data');
                                            if (empty($id_attribute)) {
                                                continue;
                                            }
                                            $sql_values[] = '(' . (int)$id_attribute . ', ' . (int)self::getLocalID('combination', (int)$productAttributeCombination['id_product_attribute'], 'data') . ')';
                                        }
                                    }
                                    if (!empty($sql_values)) {
                                        $result = Db::getInstance()->execute('REPLACE INTO `' . _DB_PREFIX_ . 'product_attribute_combination` (`id_attribute`, `id_product_attribute`) VALUES ' . implode(',', $sql_values));
                                        if (!$result) {
                                            $this->showMigrationMessageAndLog(Tools::displayError('Can\'t add product_attribute_combination. ' . Db::getInstance()->getMsgError()), 'Combination');
                                        }
                                    }

                                    //import product_attribute_image
                                    $sql_values = array();
                                    foreach ($productAdditionalThird['product_attribute_image'] as $productAttributeImage) {
                                        if ($productAttributeImage['id_product_attribute'] == $productAttribute['id_product_attribute']) {
                                            $sql_values[] = '(' . (int)$combinationModel->id . ', ' . (int)self::getLocalID('image', (int)$productAttributeImage['id_image'], 'data') . ')';
                                        }
                                    }
                                    if (!empty($sql_values)) {
                                        $result = Db::getInstance()->execute('REPLACE INTO `' . _DB_PREFIX_ . 'product_attribute_image` (`id_product_attribute`, `id_image`) VALUES ' . implode(',', $sql_values));
                                        if (!$result) {
                                            $this->showMigrationMessageAndLog(Tools::displayError('Can\'t add product_attribute_image. ' . Db::getInstance()->getMsgError()), 'Combination');
                                        }
                                    }
                                }
                                $this->showMigrationMessageAndLog($combination_error_tmp, 'Combination');
                            }
                        }
                    }

                    //import feature_product
                    foreach ($productAdditionalSecond['all_product_attributes'] as $featureProduct) {
                        if ($featureProduct['entity_id'] == $product['entity_id']) {
                            if (self::getLocalId('feature', (int)$featureProduct['attribute_id'], 'data') !== 0) {
                                Product::addFeatureProductImport($productObj->id, self::getLocalId('feature', (int)$featureProduct['attribute_id'], 'data'), self::getLocalId('featurevalue', (int)$featureProduct['value'], 'data'));
                            }
                        }
                    }

                    $sql_values = array();
                    foreach ($productAdditionalSecond['product_tag'] as $productTag) {
                        if ($productTag['product_id'] == $product['entity_id']) {
                            $sql_values[] = '(' . (int)$productObj->id . ', ' . (int)self::getLocalID('tag', (int)$productTag['tag_id'], 'data') . ', ' . (int)Configuration::get('PS_LANG_DEFAULT') . ')';
                        }
                    }
                    if (!empty($sql_values)) {
                        $result = Db::getInstance()->execute('REPLACE INTO `' . _DB_PREFIX_ . 'product_tag` (`id_product`, `id_tag`, `id_lang`) 
                                VALUES ' . implode(',', $sql_values));
                        if (!$result) {
                            $this->showMigrationMessageAndLog(Tools::displayError('Can\'t add product_tag. ' . Db::getInstance()->getMsgError()), 'Tag');
                        }
                    }


                    if (count($this->error_msg) == 0) {
                        self::addLog('Product', $product['entity_id'], $productObj->id);
                        Module::processDeferedFuncCall();
                        Module::processDeferedClearCache();
                        Tag::updateTagCount();
                        foreach ($link_original as $langId => $link_rewrite) {
                            MageLinkRewrite::importLinkRewrite($productObj->id, $link_rewrite, 'product');
                        }
                    }
                }
                $this->showMigrationMessageAndLog($product_error_tmp, 'Product');
            }
        }

        $this->updateProcess(count($products));
    }

    public function customers($customers, $addresses, $customerAdditionalSecond, $customerAddressAdditional)
    {
        self::saveAttributeIds($customerAdditionalSecond['all_attributes']);
        foreach ($customers as $customer) {
            if ($customerObject = $this->createObjectModel('Customer', $customer['id_customer'])) {
                $customerObject->secure_key = md5((int)$customer['id_customer']);
                $customerObject->lastname = isset($customer['lastname']) ? preg_replace('/[0-9]|!|<|>|,|;|\?|=|\+|(|)|@|#|"|°|{|}|_|$|%|:/', '', pSQL($customer['lastname'])) : 'Doe';
                $customerObject->firstname = isset($customer['firstname']) ? preg_replace('/[0-9]|!|<|>|,|;|\?|=|\+|(|)|@|#|"|°|{|}|_|$|%|:/', '', pSQL($customer['firstname'])) : 'John';
                $customerObject->email = isset($customer['email']) ? pSQL($customer['email']) : 'johndoe@mail.com';
                $customerObject->passwd = isset($customer['passwd']) ? Tools::encrypt($customer['passwd']) : Tools::encrypt(rand(1, 10000));
//                $customerObject->last_passwd_gen = isset($customer['last_passwd_gen']) ? $customer['last_passwd_gen'] : null;
                if (isset($customer['id_gender']) && $customer['id_gender'] == 0) {
                    $customerObject->id_gender = 1;
                } else {
                    $customerObject->id_gender = isset($customer['id_gender']) ? (int)$customer['id_gender'] : 1;
                }
                $customerObject->birthday = isset($customer['birthday']) ? date('Y-m-d H:i:s', $customer['birthday']) : '';
                if (isset($customerAdditionalSecond['newsletter']) && !empty($customerAdditionalSecond['newsletter'])) {
                    foreach ($customerAdditionalSecond['newsletter'] as $newsletter) {
                        if (($newsletter['customer_id'] == $customer['id_customer']) && ($newsletter['subscriber_status'] == 1)) {
                            $customerObject->newsletter = (int)1;
                            $customerObject->newsletter_date_add = date('Y-m-d H:i:s');
                        }
                    }
                }
//                $customerObject->newsletter = (int)$customer['newsletter'];
//                $customerObject->newsletter_date_add = $customer['newsletter_date_add'];
//                $customerObject->optin = (int)$customer['optin'];
                $customerObject->active = isset($customer['active']) ? (int)$customer['active'] : 1;
                $customerObject->deleted = 0;
//                $customerObject->note = pSQL($customer['note']);
//                $customerObject->is_guest = (int)$customer['is_guest'];
                $customerObject->id_default_group = isset($customer['id_default_group']) ? self::getCustomerGroupID((int)$customer['id_default_group']) : 1;
                $customerObject->date_add = isset($customer['date_add']) ? $customer['date_add'] : date('Y-m-d H:i:s');
                if (isset($customer['date_add']) && ValidateCore::isDate($customer['date_add'])) {
                    $customerObject->date_add = $customer['date_add'];
                } else {
                    $customerObject->date_add = date('Y-m-d H:i:s');
                }
                if (isset($customer['date_upd']) && ValidateCore::isDate($customer['date_upd'])) {
                    $customerObject->date_upd = $customer['date_upd'];
                } else {
                    $customerObject->date_upd = date('Y-m-d H:i:s');
                }

//                $customerObject->ip_registration_newsletter = pSQL($customer['ip_registration_newsletter']);
//                $customerObject->website = pSQL($customer['website']);
//                $customerObject->company = pSQL($customer['company']);
//                $customerObject->siret = pSQL($customer['siret']);
//                $customerObject->ape = pSQL($customer['ape']);
//                $customerObject->outstanding_allow_amount = (float)$customer['outstanding_allow_amount'];
//                $customerObject->show_public_prices = (int)$customer['show_public_prices'];
//                $customerObject->id_risk = (int)$customer['id_risk'];
//                $customerObject->max_payment_days = (int)$customer['max_payment_days'];
                $customerObject->id_shop = isset($customer['id_shop']) ? self::getShopID((int)$customer['id_shop']) : null;
                $customerObject->id_shop_group = isset($customerObject->id_shop) ? Shop::getGroupFromShop($customerObject->id_shop) : null;
                $customerObject->id_lang = isset($customer['store_id']) ? self::getLanguageID((int)$customer['store_id']) : null;
//                $customerObject->reset_password_token = pSQL($customer['reset_password_token']);
//                $customerObject->reset_password_validity = $customer['reset_password_validity'];
                if ($customerAdditionalSecond != null) {
                    foreach ($customerAdditionalSecond['customer_infos'] as $customerInfo) {
                        if ($customerInfo['entity_id'] == $customer['id_customer']) {
                            if ($customerInfo['attribute_id'] == $this->lastname_id) {
                                $customerObject->lastname = empty($customerInfo['value']) ? 'empty' : preg_replace('/[0-9]|!|<|>|,|;|\?|=|\+|(|)|@|#|"|°|{|}|_|$|%|:/', '', pSQL($customerInfo['value']));
                            } elseif ($customerInfo['attribute_id'] == $this->firstname_id) {
                                $customerObject->firstname = preg_replace('/[0-9]|!|<|>|,|;|\?|=|\+|(|)|@|#|"|°|{|}|_|$|%|:/', '', pSQL($customerInfo['value']));
                            } elseif ($customerInfo['attribute_id'] == $this->password_hash_id) {
                                $customer['passwd'] = $customerInfo['value'];
                                $customerObject->passwd = Tools::encrypt($customerInfo['value']);
                            } elseif ($customerInfo['attribute_id'] == $this->dob_id) {
                                $customerObject->birthday = $customerInfo['value'];
                            }
                        }
                    }
                }

                foreach ($customerAdditionalSecond['customer_gender'] as $customerGender) {
                    if ($customerGender['entity_id'] == $customer['id_customer']) {
                        if ($customerGender['attribute_id'] == $this->gender_id) {
                            if ($customerGender['value'] == 0) {
                                $customerObject->id_gender = 1;
                            } else {
                                $customerObject->id_gender = (int)$customerGender['value'];
                            }
                        }
                    }
                }

                if (!ValidateCore::isPasswd($customerObject->passwd)) {
                    $customerObject->passwd = md5('123456');
//                    $this->showMigrationMessageAndLog('Password of customer with ID ' . $customer['id_customer'] . ' is empty (or not valid for PrestaShop). For that reason, the module set default (123456) value as a password for this customer.', null, true);
                }
                Configuration::updateValue('PS_GUEST_CHECKOUT_ENABLED', 1);
                $res = false;
                $err_tmp = '';

                $this->validator->setObject($customerObject);
                $this->validator->checkFields();
                $customer_error_tmp = $this->validator->getValidationMessages();
                if ($customerObject->id && Customer::existsInDatabase($customerObject->id, 'customer')) {
                    try {
                        $res = $customerObject->update();
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }
                if (!$res) {
                    try {
                        $res = $customerObject->add(false);
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    $this->showMigrationMessageAndLog(sprintf('Customer (ID: %1$s) cannot be saved. %2$s', (isset($customer['id_customer']) && !empty((int)$customer['id_customer'])) ? Tools::safeOutput($customer['id_customer']) : 'No ID', $err_tmp), 'Customer');
                } else {
                    foreach ($addresses as $address) {
                        if ($address['id_customer'] == $customer['id_customer']) {
                            if ($addressObject = $this->createObjectModel('Address', (int)$address['id_address'])) {
                                $addressObject->id_customer = $customerObject->id;
                                $addressObject->id_manufacturer = isset($address['id_manufacturer']) ? self::getLocalId('manufacturer', (int)$address['id_manufacturer'], 'data') : null;
                                $addressObject->id_supplier = isset($address['id_supplier']) ? self::getLocalId('supplier', (int)$address['id_supplier'], 'data') : null;
                                if ($this->version == 2) {
                                    $addressObject->id_country = (int)Country::getByIso(pSQL($address['id_country']));
                                    $addressObject->id_state = State::getIdByName(pSQL($address['id_state']));
                                }
                                $addressObject->alias = pSQL('Home');
                                $addressObject->company = isset($address['company']) ? pSQL($address['company']) : 'Company';
                                $addressObject->lastname = isset($address['lastname']) ? preg_replace('/[0-9]|!|<|>|,|;|\?|=|\+|(|)|@|#|"|°|{|}|_|$|%|:/', '', pSQL($address['lastname'])) : 'empty';
                                $addressObject->firstname = isset($address['firstname']) ? preg_replace('/[0-9]|!|<|>|,|;|\?|=|\+|(|)|@|#|"|°|{|}|_|$|%|:/', '', pSQL($address['firstname'])) : 'empty';
                                $addressObject->vat_number = isset($address['vat_number']) ? pSQL($address['vat_number']) : null;
                                $addressObject->address1 = pSQL(' ');
                                if (isset($address['id_state'])) {
                                    $addressObject->address1 .= ' ' . pSQL($address['id_state']);
                                }
                                if (isset($address['city'])) {
//                                        $addressObject->address1 .= ' ' . pSQL(' ') . pSQL($address['city']);
                                }
                                if (isset($address['street'])) {
                                    $addressObject->address1 .= ' ' . pSQL(' ') . pSQL($address['street']);
                                }
                                $addressObject->address2 = isset($address['address2']) ? pSQL($address['address2']) : null;
                                $addressObject->postcode = isset($address['postcode']) ? preg_replace('/[^a-zA-Z 0-9-]/', '', pSQL($address['postcode'])) : null;
                                $addressObject->city = isset($address['city']) ? pSQL($address['city']) : null;
                                $addressObject->other = isset($address['other']) ? pSQL($address['other']) : null;
                                $addressObject->phone = isset($address['phone']) && ValidateCore::isPhoneNumber($address['phone']) ? preg_replace('/[^+0-9. ()\/-]/', '', pSQL($address['phone'])) : null;
                                $addressObject->phone_mobile = isset($address['phone_mobile']) ? preg_replace('/[^+0-9. ()\/-]/', '', pSQL($address['phone_mobile'])) : null;
                                $addressObject->dni = isset($address['dni']) ? empty($address['dni']) ? 'dni' : pSQL($address['dni']) : 'dni';
                                if (isset($address['is_active']) && $address['is_active'] != 0) {
                                    $addressObject->deleted = 0;
                                }
                                $addressObject->date_add = isset($address['date_add']) ? $address['date_add'] : null;
                                $addressObject->date_upd = isset($address['date_upd']) ? $address['date_upd'] : null;
                                $addressObject->id_warehouse = (isset($address['id_warehouse']) && !empty($address['id_warehouse'])) ? (int)$address['id_warehouse'] : null;

                                if ($customerAddressAdditional != null) {
                                    //save a country code for empty countries
                                    if (empty(Configuration::get('mageigrationpro_default_country'))) {
                                        foreach ($customerAddressAdditional['customer_main_infos'] as $customerAdd) {
                                            if ($customerAdd['attribute_id'] == $this->country_id_adr_id) {
                                                if (!empty($customerAdd['value'])) {
                                                    Configuration::updateValue('mageigrationpro_default_country', $customerAdd['value']);
                                                }
                                            }
                                        }
                                    }
                                    foreach ($customerAddressAdditional['customer_main_infos'] as $customerAddress) {
                                        if ($customerAddress['entity_id'] == $address['id_address']) {
                                            if ($customerAddress['attribute_id'] == $this->country_id_adr_id) {
                                                if (empty($customerAddress['value'])) { // when empty country code, get saved country code
                                                    $customerAddress['value'] = Configuration::get('mageigrationpro_default_country') ? Configuration::get('mageigrationpro_default_country') : 'FR';
                                                }
                                                $addressObject->id_country = (int)Country::getByIso(pSQL($customerAddress['value']));
                                            } elseif ($customerAddress['attribute_id'] == $this->region_adr_id) {
                                                $addressObject->id_state = State::getIdByName(pSQL($customerAddress['value']));
//                                                    $addressObject->address1 .= ' ' . pSQL($customerAddress['value']);
                                            } elseif ($customerAddress['attribute_id'] == 24) {
                                                $addressObject->company = pSQL($customerAddress['value']);
                                            } elseif ($customerAddress['attribute_id'] == $this->lastname_adr_id) {
                                                $addressObject->lastname = preg_replace('/[0-9]|!|<|>|,|;|\?|=|\+|(|)|@|#|"|°|{|}|_|$|%|:/', '', pSQL($customerAddress['value']));
                                            } elseif ($customerAddress['attribute_id'] == $this->firstname_adr_id) {
                                                $addressObject->firstname = preg_replace('/[0-9]|!|<|>|,|;|\?|=|\+|(|)|@|#|"|°|{|}|_|$|%|:/', '', pSQL($customerAddress['value']));
                                            } elseif ($customerAddress['attribute_id'] == $this->vat_id_adr_id) {
                                                $addressObject->vat_number = pSQL($customerAddress['value']);
                                            } elseif ($customerAddress['attribute_id'] == $this->city_adr_id) {
                                                $addressObject->city = pSQL($customerAddress['value']);
//                                                    $addressObject->address1 .= ' ' . pSQL($customerAddress['value']);
                                            } elseif ($customerAddress['attribute_id'] == $this->postcode_adr_id) {
                                                $addressObject->postcode = preg_replace('/[^a-zA-Z 0-9-]/', '', pSQL($customerAddress['value']));
                                            } elseif ($customerAddress['attribute_id'] == 31) {
                                                $addressObject->phone = pSQL($customerAddress['value']);
//                                                $addressObject->phone = isset($address['phone']) && ValidateCore::isPhoneNumber($address['phone']) ? preg_replace('/[^+0-9. ()\/-]/', '', pSQL($customerAddress['value'])) : null;
                                            }
                                        }
                                    }

                                    if (empty($addressObject->id_country)) {
                                        $addressObject->id_country = 0;
                                    }

                                    if (empty($addressObject->city)) {
                                        $addressObject->city = 'Empty';
                                    }
                                    foreach ($customerAddressAdditional['customer_street'] as $customerAddressStreet) {
                                        if ($customerAddressStreet['entity_id'] == $address['id_address']) {
                                            if (isset($this->street_adr_id) && $customerAddressStreet['attribute_id'] == $this->street_adr_id) {
                                                $addressObject->street = pSQL($customerAddressStreet['value']);
                                                $addressObject->address1 .= ' ' . pSQL($customerAddressStreet['value']);
                                            }
                                        }
                                    }
                                }

                                $res = false;
                                $err_tmp = '';

                                $this->validator->setObject($addressObject);
                                $this->validator->checkFields();
                                $address_error_tmp = $this->validator->getValidationMessages();
                                if ($addressObject->id && Address::existsInDatabase($addressObject->id, 'address')) {
                                    try {
                                        $res = $addressObject->update();
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }
                                if (!$res) {
                                    try {
                                        $res = $addressObject->add(false);
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }

                                if (!$res) {
                                    $this->showMigrationMessageAndLog(sprintf('Address (ID: %1$s) cannot be saved. %2$s', (isset($address['id_address']) && !empty($address['id_address'])) ? Tools::safeOutput($address['id_address']) : 'No ID', $err_tmp), 'Address');
                                } else {
                                    self::addLog('Address', $address['id_address'], $addressObject->id);
                                }
                                $this->showMigrationMessageAndLog($address_error_tmp, 'Address');
                            }
                        }
                    }
                    if (count($this->error_msg) == 0) {
                        self::addLog('Customer', $customer['id_customer'], $customerObject->id);
                        MigrationProMage::storeCustomerPass($customerObject->id, $customer['email'], $customer['passwd']);
                    }
                }
                $this->showMigrationMessageAndLog($customer_error_tmp, 'Customer');
            }
        }
        $this->updateProcess(count($customers));
    }

    public function orders($orders, $ordersAdditionalSecond, $ordersAdditionalThird)
    {
        $invoice_number = 1;
        foreach ($orders as $order) {
            if ($orderModel = $this->createObjectModel('Order', (int)$order['id_order'], 'orders')) {
                foreach ($ordersAdditionalSecond['address'] as $orderAddress) {
                    if ($orderAddress['parent_id'] == $order['id_order']) {
                        if ($orderAddress['address_type'] == 'shipping') {
                            $orderModel->id_address_delivery = self::getLocalID('address', (int)$orderAddress['customer_address_id'], 'data');
                        } elseif ($orderAddress['address_type'] == 'billing') {
                            $orderModel->id_address_invoice = self::getLocalID('address', (int)$orderAddress['customer_address_id'], 'data');
                        }
                    }
                }
                if (empty($orderModel->id_address_delivery)) {
                    $orderModel->id_address_delivery = $orderModel->id_address_invoice;
                }
                $orderModel->id_cart = (int)$order['id_cart'];
                foreach (MageMigrationProMapping::listMapping(true, false) as $row => $data) {
                    if ($row == 'currencies') {
                        foreach ($data as $currency) {
                            if (preg_match("/{$currency['source_name']}/", $order['id_currency'])) {
                                $orderModel->id_currency = (int)$currency['local_id'];
                            }
                        }
                    }
                }
                $orderModel->id_lang = Configuration::get('PS_LANG_DEFAULT');
                $orderModel->id_customer = self::getLocalId('customer', (int)$order['id_customer'], 'data');
                if (empty($orderModel->id_customer)) {
                    if (empty($order['customer_email'])) {
                        $lastname = '';
                        $firstname = '';
                        if (!empty($order['customer_firstname'])) {
                            $firstname = $order['customer_firstname'];
                        }
                        if (!empty($order['customer_lastname'])) {
                            $lastname = $order['customer_lastname'];
                        }
                        foreach ($ordersAdditionalSecond['address'] as $orderAddress) {
                            if ($orderAddress['parent_id'] == $order['id_order']) {
                                if (empty($firstname) && !empty(preg_replace('|\(.*\)|', '', $orderAddress['firstname']))) {
                                    $firstname = preg_replace('|\(.*\)|', '', $orderAddress['firstname']);
                                }
                                if (empty($lastname) && !empty(preg_replace('|\(.*\)|', '', $orderAddress['lastname']))) {
                                    $lastname = preg_replace('|\(.*\)|', '', $orderAddress['lastname']);
                                }
                            }
                        }
                        if (!empty($lastname) && !empty($firstname)) {
                            $orderModel->id_customer = Db::getInstance()->getValue('SELECT id_customer FROM ' . _DB_PREFIX_ . 'customer WHERE firstname = \'' . pSQL($firstname) . '\' AND lastname = \'' . pSQL($lastname) . '\'');
                            if (empty($orderModel->id_customer)) {
                                $newCustomer = new Customer();
                                $newCustomer->firstname = $firstname;
                                $newCustomer->lastname = $lastname;
                                $newCustomer->email = empty($order['customer_email']) ? 'empty@email.com' : str_replace(',', '.', pSQL($order['customer_email']));
                                $newCustomer->passwd = md5('123456');
                                $newCustomer->id_shop = $orderModel->id_shop;
                                $newCustomer->id_shop_group = $orderModel->id_shop_group;
                                try {
                                    $res = $newCustomer->add();
                                } catch (Exception $e) {
                                    $this->showMigrationMessageAndLog(sprintf('Customer with name %1$s cannot be saved. %2$s', $order['customer_firstname'] . ' ' . $order['customer_lastname'], $e->getMessage()), 'Customer');
                                }
                                $orderModel->id_customer = $newCustomer->id;
                            }
                        } else {
                            $newCustomer = new Customer();
                            $newCustomer->firstname = empty($firstname) ? 'empty' : preg_replace('|\(.*\)|', '', pSQL($firstname));
                            $newCustomer->lastname = empty($lastname) ? 'empty' : preg_replace('|\(.*\)|', '', pSQL($lastname));
                            $newCustomer->email = empty($order['customer_email']) ? 'empty@email.com' : str_replace(',', '.', pSQL($order['customer_email']));
                            $newCustomer->passwd = md5('123456');
                            $newCustomer->id_shop = $orderModel->id_shop;
                            $newCustomer->id_shop_group = $orderModel->id_shop_group;
                            try {
                                $res = $newCustomer->add();
                            } catch (Exception $e) {
                                $this->showMigrationMessageAndLog(sprintf('Customer with name %1$s cannot be saved. %2$s', $order['customer_firstname'] . ' ' . $order['customer_lastname'], $e->getMessage()), 'Customer');
                            }
                            $orderModel->id_customer = $newCustomer->id;
                        }
                    } else {
                        $orderModel->id_customer = Db::getInstance()->getValue('SELECT id_customer FROM ' . _DB_PREFIX_ . 'customer WHERE email = \'' . pSQL($order['customer_email']) . '\'');
                        if (empty($orderModel->id_customer)) {
                            $lastname = '';
                            $firstname = '';
                            if (!empty($order['customer_firstname'])) {
                                $firstname = $order['customer_firstname'];
                            }
                            if (!empty($order['customer_lastname'])) {
                                $lastname = $order['customer_lastname'];
                            }
                            if (empty($firstname) && empty($lastname)) {
                                foreach ($ordersAdditionalSecond['address'] as $orderAddress) {
                                    if ($orderAddress['parent_id'] == $order['id_order']) {
                                        if (empty($firstname) && !empty(preg_replace('|\(.*\)|', '', $orderAddress['firstname']))) {
                                            $firstname = preg_replace('|\(.*\)|', '', $orderAddress['firstname']);
                                        }
                                        if (empty($lastname) && !empty(preg_replace('|\(.*\)|', '', $orderAddress['lastname']))) {
                                            $lastname = preg_replace('|\(.*\)|', '', $orderAddress['lastname']);
                                        }
                                    }
                                }
                            }
                            if (!empty($lastname) && !empty($firstname)) {
                                $orderModel->id_customer = Db::getInstance()->getValue('SELECT id_customer FROM ' . _DB_PREFIX_ . 'customer WHERE firstname = \'' . pSQL($firstname) . '\' AND lastname = \'' . pSQL($lastname) . '\'');
                                if (empty($orderModel->id_customer)) {
                                    $newCustomer = new Customer();
                                    $newCustomer->firstname = empty($firstname) ? 'empty' : preg_replace('|\(.*\)|', '', pSQL($firstname));
                                    $newCustomer->lastname = empty($lastname) ? 'empty' : preg_replace('|\(.*\)|', '', pSQL($lastname));
                                    $newCustomer->email = empty($order['customer_email']) ? 'empty@email.com' : str_replace(',', '.', pSQL($order['customer_email']));
                                    $newCustomer->passwd = md5('123456');
                                    $newCustomer->id_shop = $orderModel->id_shop;
                                    $newCustomer->id_shop_group = $orderModel->id_shop_group;
                                    try {
                                        $res = $newCustomer->add();
                                    } catch (Exception $e) {
                                        $this->showMigrationMessageAndLog(sprintf('Customer with name %1$s cannot be saved. %2$s', $order['customer_firstname'] . ' ' . $order['customer_lastname'], $e->getMessage()), 'Customer');
                                    }
                                    $orderModel->id_customer = $newCustomer->id;
                                }
                            }
                        }
                    }
                    if (!isset($newCustomer) && empty($newCustomer)) {
                        $newCustomer = new Customer($orderModel->id_customer);
                    }
                    foreach ($ordersAdditionalSecond['address'] as $orderAddress) {
                        if ($orderAddress['parent_id'] == $order['id_order']) {
                            $newCustomersAddress = new Address();
                            $newCustomersAddress->id_customer = $newCustomer->id;
                            $newCustomersAddress->id_manufacturer = 0;
                            $newCustomersAddress->id_supplier = 0;
                            $newCustomersAddress->id_country = empty($orderAddress['country_id']) ? 0 : (int)Country::getByIso($orderAddress['country_id']);
                            $newCustomersAddress->id_state = (int)State::getIdByIso($orderAddress['region']);
                            if (empty($newCustomersAddress->id_state)) {
                                $newCustomersAddress->id_state = (int)State::getIdByName($orderAddress['region']);
                            }
                            $newCustomersAddress->alias = 'Home';
                            $newCustomersAddress->company = '';
                            $newCustomersAddress->lastname = preg_replace('|\(.*\)|', '', $orderAddress['lastname']);
                            $newCustomersAddress->firstname = preg_replace('|\(.*\)|', '', $orderAddress['firstname']);
                            if (($newCustomer->firstname == 'empty') && !empty($newCustomersAddress->firstname)) {
                                $newCustomer->firstname = $newCustomersAddress->firstname;
                                $newCustomer->update();
                            }
                            if (($newCustomer->lastname == 'empty') && !empty($newCustomersAddress->lastname)) {
                                $newCustomer->lastname = $newCustomersAddress->lastname;
                                $newCustomer->update();
                            }
                            $newCustomersAddress->vat_number = '';
                            $newCustomersAddress->address1 = Tools::substr($orderAddress['region'] . ' ' . $orderAddress['city'] . ' ' . $orderAddress['street'], 0, 128);
                            if (!ValidateCore::isAddress($newCustomersAddress->address1)) {
                                $newCustomersAddress->address1 = $orderAddress['region'];
                            }
                            $newCustomersAddress->address2 = '';
                            $newCustomersAddress->postcode = Tools::substr($orderAddress['postcode'], 0, 12);
                            if (!ValidateCore::isPostCode($newCustomersAddress->postcode)) {
                                $newCustomersAddress->postcode = null;
                            }
                            $newCustomersAddress->city = str_replace('_', ' ', $orderAddress['city']);
                            $newCustomersAddress->other = isset($orderAddress['other']) ? $orderAddress['other'] : null;
                            $newCustomersAddress->phone = trim(explode(';', str_replace(array('[', ']'), '', $orderAddress['telephone']))[0]);
//                            $newCustomersAddress->phone = trim(explode(';', $orderAddress['telephone'])[0]);
                            if (!ValidateCore::isPhoneNumber($newCustomersAddress->phone)) {
                                $newCustomersAddress->phone = null;
                            }
                            $newCustomersAddress->phone_mobile = empty($orderAddress['phone_mobile']) ? $newCustomersAddress->phone_mobile = str_replace('.', '', $orderAddress['telephone']) : $orderAddress['phone_mobile'];
                            $newCustomersAddress->dni = 'dni';
                            $newCustomersAddress->deleted = 0;
                            $newCustomersAddress->date_add = date('Y-m-d H:i:s');
                            $newCustomersAddress->date_upd = date('Y-m-d H:i:s');
                            if (empty($newCustomersAddress->lastname)) {
                                $newCustomersAddress->lastname = 'Empty';
                            }
                            if (empty($newCustomersAddress->firstname)) {
                                $newCustomersAddress->firstname = 'Empty';
                            }
                            if (empty($newCustomersAddress->city)) {
                                $newCustomersAddress->city = 'Empty';
                            }
                            try {
                                $res = $newCustomersAddress->add(false);
                            } catch (Exception $e) {
                                $this->showMigrationMessageAndLog(sprintf('Address of the Customer with name %1$s cannot be saved. %2$s', $orderAddress['firstname'] . ' ' . $orderAddress['lastname'], $e->getMessage()), 'Address');
                            }
                            if (empty($orderModel->id_address_delivery)) {
                                $orderModel->id_address_delivery = $newCustomersAddress->id;
                            }
                            if (empty($orderModel->id_address_invoice)) {
                                $orderModel->id_address_invoice = $newCustomersAddress->id;
                            }
                            break;
                        }
                    }
                }
                $orderModel->id_carrier = 1;  //
                $orderModel->secure_key = md5((int)$order['id_customer']);
                foreach ($ordersAdditionalSecond['sales_order_payment'] as $orderPaymentMethod) {
                    if ($orderPaymentMethod['parent_id'] == $order['id_order']) {
                        $orderModel->payment = pSQL($orderPaymentMethod['method']);
                    }
                }
                $orderModel->module = (empty($order['module'])) ? 'cheque' : pSQL($order['module']);
//                $orderModel->recyclable = $order['recyclable'];
//                $orderModel->gift = $order['gift'];
//                $orderModel->gift_message = $order['gift_message'];   //
                $orderModel->total_discounts = abs((float)$order['discount_amount']);
                $orderModel->total_paid = (float)$order['grand_total'];
                $orderModel->total_paid_real = empty($order['total_paid_real']) ? 0 : (float)$order['total_paid_real'];
//                $orderModel->total_products = $order['total_products'];
                $orderModel->total_products = (float)$order['total_products'];
                $orderModel->total_products_wt = abs((float)$order['grand_total'] - (float)$order['shipping_incl_tax']);
                $orderModel->total_shipping = (float)$order['shipping_incl_tax'];
                if ($order['shipping_incl_tax'] != 0) {
                    $orderModel->carrier_tax_rate = abs((float)Tools::ps_round($order['shipping_incl_tax'] * 100 / (float)$order['shipping_amount'] - 100, _PS_PRICE_COMPUTE_PRECISION_));
                } else {
                    $orderModel->carrier_tax_rate = 0;
                }
//                $orderModel->total_wrapping = $order['total_wrapping'];
                $orderModel->shipping_number = isset($order['shipping_number']) ? (int)$order['shipping_number'] : null;
                $orderModel->conversion_rate = 1;

                foreach ($ordersAdditionalSecond['sales_invoice'] as $invoice) {
                    if (isset($invoice['id_order']) && isset($order['id_order']) && $order['id_order'] == $invoice['id_order']) {
                        $orderModel->invoice_number = (int)$invoice['id_invoice'];
                        $orderModel->invoice_date = $invoice['date_add'];  //
                    }
                }
//                $orderModel->delivery_number = $order['delivery_number'];
//                $orderModel->delivery_date = $order['delivery_date'];
                $orderModel->valid = 1;
                $orderModel->date_add = $order['date_add'];
                $orderModel->date_upd = $order['date_upd'];
//                $taxRate = (float)$order['subtotal_incl_tax'] * 100 / (float)$order['total_products'] - 100;
                if (((float)$order['total_products'] - 100) == 0 || ((float)$order['total_products'] - 100) == 0.0) {
                    $taxRate = ((float)$order['subtotal_incl_tax'] * 100) / 1;
                } else {
                    $taxRate = ((float)$order['subtotal_incl_tax'] * 100) / ((float)$order['total_products'] - 100);
                }
                $taxRate = abs(Tools::ps_round($taxRate, _PS_PRICE_COMPUTE_PRECISION_));
//                if ($this->version >= 1.5) {
                foreach ($ordersAdditionalSecond['order_shop_and_state'] as $orderShop) {
                    if ($orderShop['entity_id'] == $order['id_order']) {
                        foreach ($ordersAdditionalSecond['all_websites'] as $orderWebsite) {
                            if (preg_match("/{$orderWebsite['name']}/", $orderShop['store_name'])) {
                                $orderModel->id_shop = self::getShopID((int)$orderWebsite['website_id']);
                            }
                        }
                    }
                }
                $orderModel->id_shop_group = Shop::getGroupFromShop($orderModel->id_shop);
                foreach ($ordersAdditionalSecond['order_shop_and_state'] as $orderState) {
                    if ($orderState['entity_id'] == $order['id_order']) {
                        foreach (MageMigrationProMapping::listMapping(true, false) as $row => $data) {
                            if ($row == 'order_states') {
                                foreach ($data as $order_state) {
                                    if (preg_match("/{$order_state['source_name']}/", $orderState['status'])) {
                                        $orderModel->current_state = (int)$order_state['local_id'];
                                    }
                                }
                            }
                        }
                    }
                }
//                    $taxAmount = $order['total_products'] - $order['total_products'];
//                    $orderModel->mobile_theme = $order['mobile_theme'];
                $orderModel->total_discounts_tax_incl = abs(number_format(($orderModel->total_discounts * $taxRate) / 100 + $orderModel->total_discounts, 2, '.', ''));
//                $orderModel->total_discounts_tax_incl = number_format(($orderModel->total_discounts * $taxRate) / 100 + $orderModel->total_discounts, 2, '.', '');
//                    $orderModel->total_discounts_tax_incl = $orderModel->total_discounts;
                $orderModel->total_discounts_tax_excl = $orderModel->total_discounts;
                $orderModel->total_paid_tax_incl = (float)$order['grand_total'];
                $orderModel->total_paid_tax_excl = abs((float)$order['grand_total'] - (float)$order['tax_amount']);
                $orderModel->total_shipping_tax_incl = (float)$order['shipping_incl_tax'];
                $orderModel->total_shipping_tax_excl = abs((float)$order['shipping_incl_tax'] - (float)$order['shipping_tax_amount']);
//                    $orderModel->total_wrapping_tax_incl = $order['total_wrapping_tax_incl'];
//                    $orderModel->total_wrapping_tax_excl = $order['total_wrapping_tax_excl'];
//                    $orderModel->round_mode = $order['round_mode'];
//                    $orderModel->round_type = $order['round_type'];
                $orderModel->reference = Order::generateReference();

                $res = false;
                $err_tmp = '';

                $this->validator->setObject($orderModel);
                $this->validator->checkFields();
                $order_error_tmp = $this->validator->getValidationMessages();
                if ($orderModel->id && self::existsInDatabase($orderModel->id, 'orders', 'order')) {
                    try {
                        $res = $orderModel->update();
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }
                if (!$res) {
                    try {
                        $res = $orderModel->add(false);
                    } catch (PrestaShopException $e) {
                        $err_tmp = $e->getMessage();
                    }
                }

                if (!$res) {
                    $this->showMigrationMessageAndLog(sprintf('Order (ID: %1$s) cannot be saved. %2$s', (isset($order['id_order']) && !empty($order['id_order'])) ? Tools::safeOutput($order['id_order']) : 'No ID', $err_tmp), 'Order');
                } else {
                    if (count($this->error_msg) == 0) {
                        self::addLog('Order', $order['id_order'], $orderModel->id);
                    }
                    // import Order Detail
                    foreach ($ordersAdditionalSecond['order_item'] as $orderDetail) {
                        if ($orderDetail['order_id'] == $order['id_order']) {
                            if ($orderDetailModel = $this->createObjectModel('OrderDetail', (int)$orderDetail['item_id'])) {
                                $orderDetailModel->id_order = $orderModel->id;
                                $orderDetailModel->product_id = self::getLocalID('product', (int)$orderDetail['product_id'], 'data');
                                foreach ($ordersAdditionalSecond['product_and_product_atributes'] as $productAndProductAttributeFirst) {
                                    if ($orderDetail['product_id'] == $productAndProductAttributeFirst['parent_id']) {
                                        $orderDetailModel->product_attribute_id = self::getLocalID('combination', (int)$productAndProductAttributeFirst['product_id'], 'data');
                                        break;
                                    }
                                }
                                if ($orderDetailModel->product_id == 0) {
                                    foreach ($ordersAdditionalSecond['product_and_product_atributes'] as $productAndProductAttributeSecond) {
                                        if ($productAndProductAttributeSecond['product_id'] == $orderDetail['product_id']) {
                                            $orderDetailModel->product_id = self::getLocalID('product', (int)$productAndProductAttributeSecond['parent_id'], 'data');
                                            $orderDetailModel->product_attribute_id = self::getLocalID('combination', (int)$productAndProductAttributeSecond['product_id'], 'data');
                                            break;
                                        }
                                    }
                                }
//

                                $orderDetailModel->product_name = pSQL($orderDetail['name']);
                                $orderDetailModel->product_quantity = (int)$orderDetail['qty_ordered'];
                                $orderDetailModel->product_quantity_in_stock = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT SUM(quantity) FROM ' . _DB_PREFIX_ . 'stock_available WHERE id_product = ' . (int)$orderDetailModel->product_id);
//                                    $orderDetailModel->product_quantity = abs((int)$orderDetail['qty_ordered']);
                                $orderDetailModel->product_quantity_return = abs((int)$orderDetail['qty_canceled']);
                                $orderDetailModel->product_quantity_refunded = abs((int)$orderDetail['qty_refunded']);
                                $orderDetailModel->product_quantity_reinjected = isset($orderDetail['product_quantity_reinjected']) ? (int)$orderDetail['product_quantity_reinjected'] : null;
                                $orderDetailModel->product_price = (float)$orderDetail['price'];
//                                    $orderDetailModel->reduction_percent = $orderDetail['reduction_percent'];
//                                    $orderDetailModel->reduction_amount = $orderDetail['reduction_amount'];
//                                    $orderDetailModel->group_reduction = $orderDetail['group_reduction'];
//                                    $orderDetailModel->product_quantity_discount = $orderDetail['product_quantity_discount'];
//                                    $orderDetailModel->product_ean13 = $orderDetail['product_ean13'];
//                                    $orderDetailModel->product_upc = $orderDetail['product_upc'];
//                                    $orderDetailModel->product_reference = $orderDetail['product_reference'];
//                                    $orderDetailModel->product_supplier_reference = $orderDetail['product_supplier_reference'];
                                $orderDetailModel->product_weight = (float)$orderDetail['weight'];
//                                    $orderDetailModel->tax_name = $orderDetail['tax_name'];
                                $orderDetailModel->tax_rate = (float)$orderDetail['tax_percent'];
//                                    $orderDetailModel->ecotax = $orderDetail['ecotax'];
//                                    $orderDetailModel->ecotax_tax_rate = $orderDetail['ecotax_tax_rate'];
//                                    $orderDetailModel->discount_quantity_applied = $orderDetail['discount_quantity_applied'];
//                                    $orderDetailModel->download_hash = $orderDetail['download_hash'];
//                                    $orderDetailModel->download_nb = $orderDetail['download_nb'];
//                                    $orderDetailModel->download_deadline = $orderDetail['download_deadline'];

                                $orderDetailModel->id_warehouse = (isset($orderDetail['id_warehouse']) && !empty((int)$orderDetail['id_warehouse'])) ? (int)$orderDetail['id_warehouse'] : 0;

//                                    $orderDetailModel->id_shop = (isset($orderDetail['id_shop']) && !empty
//                                        ($orderDetail['id_shop'])) ? self::getShopID($orderDetail['id_shop']) : Context::getContext()->shop->id;
//                                    $orderDetailModel->id_shop = Configuration::get('PS_SHOP_DEFAULT');
                                if (empty($orderModel->id_shop)) {
                                    $orderDetailModel->id_shop = Configuration::get('PS_SHOP_DEFAULT');
                                } else {
                                    $orderDetailModel->id_shop = $orderModel->id_shop;
                                }
//                                    if ($this->version >= 1.5) {
                                foreach ($ordersAdditionalSecond['sales_invoice'] as $salesInvoice) {
                                    if ($salesInvoice['order_id'] == $order['id_order']) {
                                        $orderDetailModel->id_order_invoice = self::getLocalID('orderInvoice', (int)$salesInvoice['entity_id'], 'data');
                                    }
                                }
//                                        $orderDetailModel->reduction_amount_tax_incl = $orderDetail['discount_amount'] + $orderDetail['tax_amount'];
//                                        $orderDetailModel->reduction_amount_tax_excl = $orderDetail['discount_amount'] - $orderDetail['tax_amount'];
//                                        $orderDetailModel->product_isbn = $orderDetail['product_isbn'];
//                                        $orderDetailModel->tax_computation_method = $orderDetail['tax_computation_method'];
//                                        $orderDetailModel->id_tax_rules_group = self::getLocalId('taxrulesgroup', $orderDetail['id_tax_rules_group'], 'data');
                                $orderDetailModel->total_price_tax_incl = ((float)$orderDetail['row_total'] * (float)$orderDetail['tax_percent']) / 100 + (float)$orderDetail['row_total'];
                                $orderDetailModel->total_price_tax_excl = (float)$orderDetail['price'];
                                $orderDetailModel->unit_price_tax_incl = ((float)$orderDetail['price'] * (float)$orderDetail['tax_percent'] / 100) + (float)$orderDetail['price'];
                                $orderDetailModel->unit_price_tax_excl = (float)$orderDetail['price'];
                                if ($orderDetailModel->unit_price_tax_excl == 0) {
                                    continue;
                                }
//                                        $orderDetailModel->total_shipping_price_tax_excl = $orderDetail['total_shipping_price_tax_excl'];
//                                        $orderDetailModel->total_shipping_price_tax_incl = $orderDetail['total_shipping_price_tax_incl'];
                                $orderDetailModel->total_shipping_price_tax_incl = (float)$order['shipping_incl_tax'];
                                $orderDetailModel->total_shipping_price_tax_excl = abs((float)$order['shipping_incl_tax'] - (float)$order['shipping_tax_amount']);
//                                        $orderDetailModel->purchase_supplier_price = $orderDetail['purchase_supplier_price'];
                                $orderDetailModel->original_product_price = (float)$orderDetail['price'];
                                foreach ($ordersAdditionalThird['products_wholesale_prices'] as $productWholesalePrice) {
                                    if ($productWholesalePrice['entity_id'] == $orderDetail['product_id']) {
                                        $orderDetailModel->original_wholesale_price = (float)$productWholesalePrice['value'];
                                    }
                                }


                                $res = false;
                                $err_tmp = '';

                                $this->validator->setObject($orderDetailModel);
                                $this->validator->checkFields();
                                $order_detail_error_tmp = $this->validator->getValidationMessages();
                                if ($orderDetailModel->id && OrderDetail::existsInDatabase($orderDetailModel->id, 'order_detail')) {
                                    try {
                                        $res = $orderDetailModel->update();
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }
                                if (!$res) {
                                    try {
                                        $res = $orderDetailModel->add(false);
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }

                                if (!$res) {
                                    $this->showMigrationMessageAndLog(sprintf('Order Detail (ID: %1$s) cannot be saved. %2$s', (isset($orderDetail['item_id']) && !empty($orderDetail['item_id'])) ? Tools::safeOutput($orderDetail['item_id']) : 'No ID', $err_tmp), 'OrderDetail');
                                } else {
                                    self::addLog('OrderDetail', $orderDetail['item_id'], $orderDetailModel->id);
                                }
                                $this->showMigrationMessageAndLog($order_detail_error_tmp, 'OrderDetail');
                            }
                        }
                    }
                    $orderIdsWithPaymentIds = array();
                    // import Order Payment
                    foreach ($ordersAdditionalSecond['sales_order_payment'] as $orderPayment) {
                        if ($orderPayment['parent_id'] == $order['id_order'] && !empty($orderPayment['amount_paid'])) {
                            if ($orderPaymentModel = $this->createObjectModel('OrderPayment', (int)$orderPayment['entity_id'])) {
                                $orderPaymentModel->order_reference = Db::getInstance()->executeS('
                                    SELECT `reference`
                                    FROM `' . _DB_PREFIX_ . 'orders`
                                    WHERE `id_order` =  ' . (int)$orderModel->id)[0]['reference'];

                                $currencyCode = MageMigrationProMapping::listMapping(true, false, true)['currencies'][$order['order_currency_code']];
                                $orderPaymentModel->id_currency = isset($currencyCode) ? (int)$currencyCode : Configuration::get('PS_CURRENCY_DEFAULT');
                                $orderPaymentModel->amount = (float)$orderPayment['amount_paid'];
                                $orderPaymentModel->payment_method = isset($order['method']) ? pSQL($order['method']) : $orderPayment['method'];
                                $orderPaymentModel->conversion_rate = (int)$orderModel->conversion_rate;
                                $orderPaymentModel->transaction_id = (is_null($orderPayment['last_trans_id']) || empty($orderPayment['last_trans_id'])) ? $orderPayment['cc_trans_id'] : $orderPayment['last_trans_id'];
                                $orderPaymentModel->card_number = pSQL($orderPayment['cc_number_enc']);
//                                    $orderPaymentModel->card_brand = $orderPayment['card_brand'];
                                $orderPaymentModel->card_expiration = pSQL($orderPayment['cc_exp_month'] . '/' . $orderPayment['cc_exp_year']);
                                $orderPaymentModel->card_holder = pSQL($orderPayment['cc_owner']);
                                $orderPaymentModel->date_add = $orderPayment['date_add'];


                                $res = false;
                                $err_tmp = '';

                                $this->validator->setObject($orderPaymentModel);
                                $this->validator->checkFields();
                                $order_payment_error_tmp = $this->validator->getValidationMessages();
                                if ($orderPaymentModel->id && OrderPayment::existsInDatabase($orderPaymentModel->id, 'order_payment')) {
                                    try {
                                        $res = $orderPaymentModel->update();
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }
                                if (!$res) {
                                    try {
                                        $res = $orderPaymentModel->add(false);
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }

                                if (!$res) {
                                    $this->showMigrationMessageAndLog(sprintf('Order Payment (ID: %1$s) cannot be saved. %2$s', (isset($orderPayment['entity_id']) && !empty($orderPayment['entity_id'])) ? Tools::safeOutput($orderPayment['entity_id']) : 'No ID', $err_tmp), 'OrderPayment');
                                } else {
                                    self::addLog('OrderPayment', $orderPayment['entity_id'], $orderPaymentModel->id);
                                    $orderIdsWithPaymentIds[$orderPayment['parent_id']] = $orderPayment['entity_id'];
                                }
                                $this->showMigrationMessageAndLog($order_payment_error_tmp, 'OrderPayment');
                            }
                        }
                    }
//                        // import Invoice
                    $ordersIdsWithInvoicesAndTaxes = array();
                    foreach ($ordersAdditionalSecond['sales_invoice'] as $orderInvoice) {
                        if ($orderInvoice['order_id'] == $order['id_order']) {
                            if ($orderInvoiceModel = $this->createObjectModel('OrderInvoice', (int)$orderInvoice['entity_id'])) {
                                $orderInvoiceModel->id_order = (int)$orderModel->id;
                                $orderInvoiceModel->number = isset($orderInvoice['increment_id']) ? $orderInvoice['increment_id'] : (int)$invoice_number;
                                $orderInvoiceModel->total_discount_tax_excl = (float)$order['discount_amount'];
                                $orderInvoiceModel->total_discount_tax_incl = number_format(((float)$order['discount_amount'] * $taxRate) / 100 + (float)$order['discount_amount'], 2, '.', '');

//                                    $orderInvoiceModel->total_paid_tax_incl = $order['subtotal_incl_tax'];
                                $orderInvoiceModel->total_paid_tax_incl = (float)$order['grand_total'];
                                $orderInvoiceModel->total_paid_tax_excl = abs((float)$order['grand_total'] - (float)$order['tax_amount']);
                                $orderInvoiceModel->total_products = (float)$order['total_products'];
                                $orderInvoiceModel->total_products_wt = abs((float)$order['grand_total'] - (float)$order['shipping_incl_tax']);
                                $orderInvoiceModel->total_shipping_tax_excl = abs((float)$order['shipping_incl_tax'] - (float)$order['shipping_tax_amount']);
                                $orderInvoiceModel->total_shipping_tax_incl = (float)$order['shipping_incl_tax'];
                                $orderInvoiceModel->date_add = $orderInvoice['created_at'];
                                $orderInvoiceModel->invoice_address = isset($orderInvoice['billing_address']) ? pSQL(self::getLocalID('address', (int)$orderInvoice['billing_address'], 'data')) : null;
                                $orderInvoiceModel->delivery_address = isset($orderInvoice['shipping_address']) ? pSQL(self::getLocalID('address', (int)$orderInvoice['shipping_address'], 'data')) : null;
                                $orderInvoiceModel->note = isset($orderInvoice['customer_note']) ? pSQL($orderInvoice['customer_note']) : null;
                                $orderInvoiceModel->date_add = $orderInvoice['created_at'];
//


                                $res = false;
                                $err_tmp = '';

                                $this->validator->setObject($orderInvoiceModel);
                                $this->validator->checkFields();
                                $order_invoice_error_tmp = $this->validator->getValidationMessages();
                                if ($orderInvoiceModel->id && OrderInvoice::existsInDatabase($orderInvoiceModel->id, 'order_invoice')) {
                                    try {
                                        $res = $orderInvoiceModel->update();
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }
                                if (!$res) {
                                    try {
                                        $res = $orderInvoiceModel->add(false);
                                    } catch (PrestaShopException $e) {
                                        $err_tmp = $e->getMessage();
                                    }
                                }

                                if (!$res) {
                                    $this->showMigrationMessageAndLog(sprintf('Order Invoice (ID: %1$s) cannot be saved. %2$s', (isset($orderInvoice['entity_id']) && !empty($orderInvoice['entity_id'])) ? Tools::safeOutput($orderInvoice['entity_id']) : 'No ID', $err_tmp), 'OrderInvoice');
                                } else {
                                    self::addLog('OrderInvoice', (int)$orderInvoice['entity_id'], $orderInvoiceModel->id);
                                    $order_detail_id = Db::getInstance()->executeS('SELECT id_order_detail FROM ' . _DB_PREFIX_ . 'order_detail WHERE id_order = ' . (int)$orderInvoiceModel->id_order);
                                    if (!empty($order_detail_id[0]['id_order_detail'])) {
                                        $orderDetailUpd = new OrderDetail((int)$order_detail_id[0]['id_order_detail']);
                                        $orderDetailUpd->id_order_invoice = $orderInvoiceModel->id;
                                        $orderDetailUpd->update();
                                    }
                                    $invoice_number++;
                                    $ordersIdsWithInvoicesAndTaxes[$orderInvoice['order_id']] = (float)$orderInvoice['shipping_tax_amount'];

                                    //import Invoice_Payment
                                    $sql_values = array();
                                    foreach ($orderIdsWithPaymentIds as $orderId => $idPayment) {
                                        if ($orderId == $orderInvoice['order_id']) {
                                            $sql_values[] = '(' . (int)$orderInvoiceModel->id . ', ' . (int)self::getLocalID('orderpayment', (int)$idPayment, 'data') . ', ' . (int)$orderModel->id . ')';
                                        }
                                    }
                                    if (!empty($sql_values)) {
                                        $result = Db::getInstance()->execute('REPLACE INTO `' . _DB_PREFIX_ . 'order_invoice_payment` (`id_order_invoice`, `id_order_payment`, `id_order`) VALUES ' . implode(',', $sql_values));

                                        if (!$result) {
                                            $this->showMigrationMessageAndLog(Tools::displayError('Can\'t add order_invoice_payment. ' . Db::getInstance()->getMsgError()), 'OrderInvoice');
                                        }
                                    }
                                }
                                $this->showMigrationMessageAndLog($order_invoice_error_tmp, 'OrderInvoice');

                                //import Invoice_Tax
                                $sql_values = array();
                                foreach ($ordersAdditionalThird['invoice_tax'] as $invoiceTax) {
                                    if (array_key_exists($invoiceTax['order_id'], $ordersIdsWithInvoicesAndTaxes)) {
                                        $id_tax = Db::getInstance()->execute('SELECT id_tax FROM ' . _DB_PREFIX_ . 'tax_lang WHERE name = \'' . (int)$invoiceTax['code'] . '\'');
                                        $taxAmount = $ordersIdsWithInvoicesAndTaxes[(int)$invoiceTax['order_id']];
                                        $sql_values[] = '(' . (int)$orderInvoiceModel->id . ', \'shipping\' , ' . (int)$id_tax . ',' . (float)$taxAmount . ')';
                                    }
                                }
                                if (!empty($sql_values)) {
                                    $result = Db::getInstance()->execute('REPLACE INTO `' . _DB_PREFIX_ . 'order_invoice_tax` (`id_order_invoice`, `type`, `id_tax`, `amount`) VALUES ' . implode(',', $sql_values));
                                    if (!$result) {
                                        $this->showMigrationMessageAndLog(Tools::displayError('Can\'t add order_invoice_tax. ' . Db::getInstance()->getMsgError()), 'OrderInvoice');
                                    }
                                }
                            }
                        }
                    }
//                        if (count($this->error_msg) == 0) {
//                            self::addLog('Order', $order['id_order'], $orderModel->id);
//                        }
                }
                $this->showMigrationMessageAndLog($order_error_tmp, 'Order');
            }
        }

        $this->updateProcess(count($orders));
    }

    // --- Internal helper methods:

    private function createObjectModel($className, $objectID, $table_name = '')
    {
        if (!MageMigrationProData::exist($className, $objectID)) {
            // -- if keep old IDs and if exists in DataBase
            // -- else  isset($objectID) 1&& (int)$objectID

            if (!empty($table_name)) {
                $existInDataBase = self::existsInDatabase((int)$objectID, Tools::strtolower($table_name), Tools::strtolower($className));
            } else {
                $existInDataBase = $className::existsInDatabase((int)$objectID, $className::$definition['table']);
                // [For PrestaShop Team] - This code call class definition attribute extended from ObjectModel class
                // like Order::$definition
            }

            if ($existInDataBase && $this->force_ids && $className != 'Combination') {
                $this->obj = new $className((int)$objectID);
            } else {
                $this->obj = new $className();
            }

            if ($this->force_ids) {
                $this->obj->force_id = true;
                $this->obj->id = $objectID;
            }
            return $this->obj;
        }
    }

    private function updateProcess($count)
    {
        if (!count($this->error_msg) && $count > 0) {
            $this->process->imported += $count;//@TODO count of item
//            $this->process->id_source = $source_id;
            if ($this->process->total <= $this->process->imported) {
                $this->process->finish = 1;
                $this->response['execute_time'] = number_format((time() - strtotime($this->process->time_start)), 3, '.', '');
            }
            $this->response['type'] = $this->process->type;
            $this->response['total'] = (int)$this->process->total;
            $this->response['imported'] = (int)$this->process->imported;
            $this->response['process'] = ($this->process->finish == 1) ? 'finish' : 'continue';
            $this->process->save();

            if (!MageMigrationProProcess::getActiveProcessObject()) {
                $allWarningMessages = $this->logger->getAllWarnings();
                $this->warning_msg = $allWarningMessages;
            }
        }
    }

    private static function getSkuOfProducts($product_attribute_sku, $product_id)
    {
        foreach ($product_attribute_sku as $value) {
            if ($value['entity_id'] == $product_id) {
                return $value['sku'];
            }
        }
    }

    private static function getProductStatus($product, $product_id)
    {
        foreach ($product as $value) {
            if ($value['entity_id'] == $product_id) {
                return $value['value'];
            }
        }
    }

    private static function existsInDatabase($id_entity, $table, $entity_name)
    {
        $row = Db::getInstance()->getRow('
			SELECT `id_' . bqSQL($entity_name) . '` as id
			FROM `' . _DB_PREFIX_ . bqSQL($table) . '` e
			WHERE e.`id_' . bqSQL($entity_name) . '` = ' . (int)$id_entity, false);

        return isset($row['id']);
    }

    private static function copyImg($id_entity, $id_image, $url, $entity = 'products', $regenerate = false)
    {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));
        if (empty($id_image)) {
            $id_image = null;
        }
        switch ($entity) {
            default:
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();
                break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_ . (int)$id_entity;
                break;
            case 'manufacturers':
                $path = _PS_MANU_IMG_DIR_ . (int)$id_entity;
                break;
            case 'suppliers':
                $path = _PS_SUPP_IMG_DIR_ . (int)$id_entity;
                break;
        }

        $url = urldecode(trim($url));
        $parced_url = parse_url($url);

        if (isset($parced_url['path'])) {
            $uri = ltrim($parced_url['path'], '/');
            $parts = explode('/', $uri);
            foreach ($parts as &$part) {
                $part = rawurlencode($part);
            }
            unset($part);
            $parced_url['path'] = '/' . implode('/', $parts);
        }

        if (isset($parced_url['query'])) {
            $query_parts = array();
            parse_str($parced_url['query'], $query_parts);
            $parced_url['query'] = http_build_query($query_parts);
        }

        if (!function_exists('http_build_url')) {
            require_once(_PS_TOOL_DIR_ . 'http_build_url/http_build_url.php');
        }

        $url = http_build_url('', $parced_url);
        // [For PrestaShop Team] Before called require_once(_PS_TOOL_DIR_ . 'http_build_url/http_build_url.php');

        $orig_tmpfile = $tmpfile;

        if (Tools::copy($url, $tmpfile)) {
            // Evaluate the memory required to resize the image: if it's too much, you can't resize it.
            if (!ImageManager::checkImageMemoryLimit($tmpfile)) {
                @unlink($tmpfile);

                return false;
            }

            $tgt_width = $tgt_height = 0;
            $src_width = $src_height = 0;
            $error = 0;
            ImageManager::resize($tmpfile, $path . '.jpg', null, null, 'jpg', false, $error, $tgt_width, $tgt_height, 5, $src_width, $src_height);
            if ($regenerate) { //@TODO add to step-2 regenerate images after import
                $images_types = ImageType::getImagesTypes($entity, true);
                $previous_path = null;
                $path_infos = array();
                $path_infos[] = array($tgt_width, $tgt_height, $path . '.jpg');
                foreach ($images_types as $image_type) {
                    $tmpfile = self::getBestPath($image_type['width'], $image_type['height'], $path_infos);

                    if (ImageManager::resize($tmpfile, $path . '-' . Tools::stripslashes($image_type['name']) . '.jpg', $image_type['width'], $image_type['height'], 'jpg', false, $error, $tgt_width, $tgt_height, 5, $src_width, $src_height)) {
                        // the last image should not be added in the candidate list if it's bigger than the original image
                        if ($tgt_width <= $src_width && $tgt_height <= $src_height) {
                            $path_infos[] = array(
                                $tgt_width,
                                $tgt_height,
                                $path . '-' . Tools::stripslashes($image_type['name']) . '.jpg'
                            );
                        }
                        if ($entity == 'products') {
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int)$id_entity . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int)$id_entity . '.jpg');
                            }
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int)$id_entity . '_' . (int)Context::getContext()->shop->id . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int)$id_entity . '_' . (int)Context::getContext()->shop->id . '.jpg');
                            }
                        }
                    }
                    if (in_array($image_type['id_image_type'], $watermark_types)) {
                        Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
                    }
                }
            }
        } else {
            @unlink($orig_tmpfile);

            return false;
        }
        unlink($orig_tmpfile);

        return true;
    }

    private static function getBestPath($tgt_width, $tgt_height, $path_infos)
    {
        $path_infos = array_reverse($path_infos);
        $path = '';
        foreach ($path_infos as $path_info) {
            list($width, $height, $path) = $path_info;
            if ($width >= $tgt_width && $height >= $tgt_height) {
                return $path;
            }
        }

        return $path;
    }

    private static function imageExits($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($response_code === 200) {
            return true;
        } else {
            return false;
        }
    }

    private function getLocalID($map_type, $sourceID, $table_type = 'map')
    {
        if ($table_type === "map") {
            $result = (isset($this->mapping[$map_type][$sourceID]) && !empty($this->mapping[$map_type][$sourceID])) ? $this->mapping[$map_type][$sourceID] : 0;
        } elseif ($map_type == "combination" && $table_type === "data") {
            $result = MageMigrationProDataProductAttributes::getLocalID($map_type, $sourceID);
        } else {
            $result = (int)MageMigrationProData::getLocalID($map_type, $sourceID);
            if ($result == 0) {
                $result = MageMigrationProMigratedData::getLocalID($map_type, $sourceID);
            }
        }

        return (int)$result;
    }

    private function getLanguageID($source_lang_id)
    {
        return $this->getLocalID('languages', (int)$source_lang_id);
    }

    private function getShopID($source_shop_id)
    {
        return $this->getLocalID('multi_shops', (int)$source_shop_id);
    }

    private function getCurrencyID($source_currency_id)
    {
        return $this->getLocalID('currencies', (int)$source_currency_id);
    }

    private function getOrderStateID($source_order_state_id)
    {
        return $this->getLocalID('order_states', (int)$source_order_state_id);
    }

    private function getCustomerGroupID($source_customer_group_id)
    {
        return $this->getLocalID('customer_groups', (int)$source_customer_group_id);
    }

    public static function getTaxRate($id_tax_rules_group)
    {
        return (int)Db::getInstance()->getValue('SELECT t.rate FROM ' .  _DB_PREFIX_ . 'tax_rule tr
                     LEFT JOIN ' .  _DB_PREFIX_ . 'tax t ON tr.id_tax = t.id_tax 
                     WHERE tr.id_tax_rules_group = ' . $id_tax_rules_group . ' 
                     AND tr.id_country = ' . (int)Configuration::get('PS_COUNTRY_DEFAULT'));
    }

    private static function defaultValue($input, $default)
    {
        if (isset($input) && !empty($input)) {
            return $input;
        } else {
            return $default;
        }
    }

    private function getChangedIdShop($dataFromSourceCart, $idKeyName)
    {
        $result = array();

        foreach ($dataFromSourceCart as $data) {
            if ($data['value'] == null) {
                continue;
            } else {
                $result[$data[$idKeyName]][] = self::getShopID($data['store_id']);
            }
        }

        return $result;
    }

    private function saveAttributeIds($attributeIdsArray)
    {
        foreach ($attributeIdsArray as $attribute) {
            if ($attribute['entity_type_id'] == 2) {
                $attribute['attribute_code'] .= '_adr_id';
            } else {
                $attribute['attribute_code'] .= '_id';
            }
            $this->{$attribute['attribute_code']} = (int)$attribute['attribute_id'];
        }

        return true;
    }

    private function selectProductAttribute($rows)
    {
        $result = $rows;
        $attributeIds = array();
        for ($i = 0; $i < count($result); $i++) {
            if (in_array($result[$i]['id_attribute'], $attributeIds)) {
                unset($result[$i]);
            } else {
                $attributeIds[] = (int)$result[$i]['id_attribute'];
            }
        }

        return $result;
    }

    private function getAttributeOptionValue($entity_id, $attribute_id, $entityAttributesArray, $attributeOptionValues = null, $columnName = 'value')
    {
        $attributeOptionValue = null;
        foreach ($entityAttributesArray as $row) {
            if ($row['entity_id'] == $entity_id && $row['attribute_id'] == $attribute_id) {
                if ($attributeOptionValues == null) {
                    return $row[$columnName];
                }
                foreach ($attributeOptionValues as $optionValue) {
                    if ($row['value'] == $optionValue['option_id']) {
                        $attributeOptionValue = pSQL($optionValue['value']);
                    }
                }
            }
        }

        return $attributeOptionValue;
    }

    private function showMigrationMessageAndLog($log, $entityType, $showOnlyWarning = false)
    {
        if ($this->ps_validation_errors) {
            if ($showOnlyWarning) {
                if (is_array($log)) {
                    foreach ($log as $logIndex => $logText) {
                        $this->logger->addWarningLog($logText, $entityType);
                    }
                } else {
                    $this->logger->addWarningLog($log, $entityType);
                }
            } else {
                if (is_array($log)) {
                    foreach ($log as $logIndex => $logText) {
                        $this->logger->addErrorLog($logText, $entityType);
                        $this->error_msg[] = $logText;
                    }
                } else {
                    $this->logger->addErrorLog($log, $entityType);
                    $this->error_msg[] = $log;
                }
            }
        } else {
            if (is_array($log)) {
                foreach ($log as $logIndex => $logText) {
                    $this->logger->addWarningLog($logText, $entityType);
                }
            } else {
                $this->logger->addWarningLog($log, $entityType);
            }
        }
    }

    public static function addLog($entity_type, $source_id, $local_id)
    {
        MageMigrationProData::import((string)$entity_type, (int)$source_id, (int)$local_id);
        MageMigrationProMigratedData::import((string)$entity_type, (int)$source_id, (int)$local_id);
    }
}
