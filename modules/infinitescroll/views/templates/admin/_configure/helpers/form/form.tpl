{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{extends file="helpers/form/form.tpl"}
{block name="field"}
    {if $input.name == 'INFINITESCROLL_ENABLE_DISABLE'}
        {$smarty.block.parent}
        <div class="form-group row">
            <label class="control-label col-lg-3"> {l s='Select the pages in which you want the plugin to work.' mod='infinitescroll'} </label>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4">
                        <h4 style="margin-top:5px;">{l s='Selected Pages' mod='infinitescroll'}</h4>
                        <select id="fields" multiple="multiple" name="INFINITESCROLL_FIELDS_SELECTED[]" style="max-width: 300px; height: 130px;">
                            {if !empty($fields_selectm)}
                                {if in_array('BestSalesController', $fields_selectm)}
                                    <option value="BestSalesController" selected>{l s='BestSales' mod='infinitescroll'}</option>
                                {/if}
                                {if in_array('CategoryController', $fields_selectm)}
                                    <option value="CategoryController" selected>{l s='Category' mod='infinitescroll'}</option>
                                {/if}
                                {if in_array('ManufacturerController', $fields_selectm)}
                                    <option value="ManufacturerController" selected>{l s='Manufacturer' mod='infinitescroll'}</option>
                                {/if}
                                {if in_array('NewProductsController', $fields_selectm)}
                                    <option value="NewProductsController" selected>{l s='NewProducts' mod='infinitescroll'}</option>
                                {/if}
                                {if in_array('PricesDropController', $fields_selectm)}
                                    <option value="PricesDropController" selected>{l s='PricesDrop' mod='infinitescroll'}</option>
                                {/if}
                                {if in_array('SupplierController', $fields_selectm)}
                                    <option value="SupplierController" selected>{l s='Supplier' mod='infinitescroll'}</option>
                                {/if}
                                {if in_array('SearchController', $fields_selectm)}
                                    <option value="SearchController" selected>{l s='Search' mod='infinitescroll'}</option>
                                {/if}
                            {/if}
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <h4 style="margin-top:5px;">{l s='Pages List' mod='infinitescroll'}</h4>
                        <select id="availableFields" multiple="multiple" style="width: 300px; height: 130px;">
                                <option value="BestSalesController" selected>{l s='BestSales' mod='infinitescroll'}</option>
                                <option value="CategoryController" selected>{l s='Category' mod='infinitescroll'}</option>
                                <option value="ManufacturerController" selected>{l s='Manufacturer' mod='infinitescroll'}</option>
                                <option value="NewProductsController" selected>{l s='NewProducts' mod='infinitescroll'}</option>
                                <option value="PricesDropController" selected>{l s='PricesDrop' mod='infinitescroll'}</option>
                                <option value="SupplierController" selected>{l s='Supplier' mod='infinitescroll'}</option>
                                <option value="SearchController" selected>{l s='Search' mod='infinitescroll'}</option>
                        </select>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-4">
                        <a id="removeField" class="btn btn-default" href="#">
                            <i class="icon-arrow-right"></i>
                            {l s='Remove' mod='infinitescroll'}
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a id="addField" class="btn btn-default" href="#">
                            <i class="icon-arrow-left"></i>
                            {l s='Add' mod='infinitescroll'}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    {else}
        {$smarty.block.parent}
    {/if}
{/block}