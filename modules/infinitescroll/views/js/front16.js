/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2018 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */
document.addEventListener("DOMContentLoaded", function() {
    'use strict';

    var loadMoreBlock = '<button class="load-more btn btn-default button exclusive-medium"><span>'+LOAD_MORE_TEXT+'</span></button>';
    var page = 2;
    var all_pages;
    var gif = '<div class="loader-gif"><p>'+DO_LOADER_MESSAGE+'</p><img src="'+GIF_IMG_PATH+''+GIF+'"/></div>';
    var scrollToTop = '<p class="scroll-to-top"><i class="icon-circle-arrow-up"></i></p>';
    var endMessage = ('<p class="end-message">'+END_MESSAGE+'</p>');
    $('body').on('click', '.scroll-to-top', function() {
        var offset = -100;
        $('html, body').animate({
            scrollTop: $("#header_logo").offset().top + offset
        }, 1000);
    });
    if (DO_SHOW_ALL) {
        loadCss();
        if (SHOW_ALL_TEXT != '') {
            $('form.showall button.exclusive-medium span').html(SHOW_ALL_TEXT);
        }
    } else {
        $('form.showall button.exclusive-medium').hide();
    }
    if (DO_SCROLL_TOP) {
        $('body').append(scrollToTop);
    }


    //Check if the page has blocklayered
    if ($('#layered_block_left').length) {
        var new_url;
        var initial_url = window.location.href;
        $(document).ajaxSuccess(function (event, xhr, options) {
            // if block layered is fired.
            if (typeof options.url !== 'undefined' && options.url.indexOf('blocklayered/blocklayered-ajax.php') !== -1) {
                if (options.url.indexOf('infinitScroll=1') !== -1) {
                    // self executed
                    if (DO_SHOW_ALL) {
                        loadCss();
                        if (SHOW_ALL_TEXT != '') {
                            $('form.showall button.exclusive-medium span').html(SHOW_ALL_TEXT);
                        }
                    } else {
                        $('form.showall button.exclusive-medium').hide();
                    }
                    if (page == all_pages) {
                        $('ul#product_list').after(endMessage);
                    }
                    page++;
                } else {
                    // blocklayered executed
                    refreshAttrsBL();
                    if (all_pages == "") {
                        $('ul#product_list').after(endMessage);
                    }
                }
            }
        });
        $('body').on('click' , '.load-more' , function () {
            loadMoreOffBlockBL();
            loadContentBlockBL();
        });
        if (DO_LOAD_MORE) {
            var run_countBlock = 0;
        }
        refreshAttrsBL();

        window.infinitescroll_called_ajax = false;

        //This runs only on category pages, only when blocklayer module is active
        if ($('#category').length == 1) {
            $(window).bind('scroll', function(){
                elementVisibleBL();
            });
        }
    } else {
        var triggerElem;
        var state;
        var total_pages;
        var links;
        var run_count = -1;
        var page_nr = 3;
        var pos;
        var rest;
        var new_url;
        var initial_url = window.location.href;
        var loadMore = '<button class="load-more btn btn-default button exclusive-medium"><span>'+LOAD_MORE_TEXT+'</span></button>';

        $(document).ajaxSuccess(function (event, xhr, options) {
            // if block layered is fired.
            if (typeof options.url !== 'undefined' && options.url.indexOf('?p=') !== -1) {
                if (DO_SHOW_ALL) {
                    loadCss();
                    if (SHOW_ALL_TEXT != '') {
                        $('form.showall button.exclusive-medium span').html(SHOW_ALL_TEXT);
                    }
                } else {
                    $('form.showall button.exclusive-medium').hide();
                }
            }
        });

        // lastProductVisibleNoBL();
        $(window).bind('scroll', function () {
            lastProductVisibleNoBL();
        });
        $('body').on('click' , '.load-more' , function () {
            loadMoreOffNoBL();
            loaderOn();
            loadContentNoBL();
        });
        $(document).ajaxComplete(function(){
            new_url = window.location.href;
            if ( initial_url != new_url ) {
                refreshAttrsNoBL();
                initial_url = new_url;
            }
        });

        refreshAttrsNoBL();
    }

    // Block Layered Functions -------------
    function isScrolledIntoViewBL(elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }
    function refreshAttrsBL() {
        page = 2;
        loadMoreOffBlockBL();
        if ($('#pagination_previous').length) {
            if (DO_SHOW_ALL) {
                loadCss();
                if (SHOW_ALL_TEXT != '') {
                    $('form.showall button.exclusive-medium span').html(SHOW_ALL_TEXT);
                }
            } else {
                $('form.showall button.exclusive-medium').hide();
            }
            all_pages = $('.pagination li:nth-last-child(2) a span').last().text().trim();
        } else {
            if (DO_SHOW_ALL) {
                loadCss();
                if (SHOW_ALL_TEXT != '') {
                    $('form.showall button.exclusive-medium span').html(SHOW_ALL_TEXT);
                }
            } else {
                $('form.showall button.exclusive-medium').hide();
            }
            all_pages = $('.pagination li:last-child a span').last().text().trim();
        }
        window.infinitescroll_called_ajax = false;
        if (DO_LOAD_MORE) {
            run_countBlock = 0;
        }
        if (DO_SHOW_ALL) {
            loadCss();
            if (SHOW_ALL_TEXT != '') {
                $('form.showall button.exclusive-medium span').html(SHOW_ALL_TEXT);
            }
        } else {
            $('form.showall button.exclusive-medium').hide();
        }
        if ($('p.end-message').length) {
            $('p.end-message').remove();
        }
    }
    function loadContentBlockBL() {
        window.infinitescroll_called_ajax = true;
        reloadContentInfiniteScroll('&p='+page+'');
        loaderOn();
    }
    function loadMoreOnBlockBL() {
        $('ul#product_list').after(loadMoreBlock);
        $('button.load-more').css({ "color": LOAD_MORE_FONT_COLOR});
        $('button.load-more span').css({    "border-color": LOAD_MORE_BACKGROUND_COLOR,
            "text-shadow": "none",
            "background": LOAD_MORE_BACKGROUND_COLOR,
        });
    }
    function loadMoreOffBlockBL() {
        $('.load-more').remove();
    }
    function elementVisibleBL() {
        if (isScrolledIntoViewBL($('.product_list > *:last-child')) && !window.infinitescroll_called_ajax && ( page <= all_pages ) && !($('.load-more').length)) {
            if ( run_countBlock == 1 ) {
                loadMoreOnBlockBL();
                window.infinitescroll_called_ajax = true;
                run_countBlock++;
            } else {
                window.infinitescroll_called_ajax = true;
                reloadContentInfiniteScroll('&p='+page+'');
                loaderOn();
                run_countBlock++;
                if ( run_countBlock - 1 > DO_LOAD_STEPS ) {
                    run_countBlock = 1;
                }
            }
        }
    };
    // Block Layered Functions -------------

    // NO Block Layered Functions -------------
    function refreshAttrsNoBL() {
        page_nr = 3;
        if ($('#pagination_previous').length) {
            total_pages = $('.pagination li:nth-last-child(2) a span').last().text().trim();
            links = $('ul.pagination li:nth-child(3) a').attr('href');
            if(total_pages < 2) {
                $('ul#product_list').after(endMessage);
            }
        } else {
            total_pages = $('.pagination li:last-child a span').last().text().trim();
            links = $('ul.pagination li:nth-child(2) a').attr('href');
            if(total_pages < 2) {
                $('ul#product_list').after(endMessage);
            }
        }
        if (DO_LOAD_MORE) {
            run_count = 0;
        }
        if (DO_SCROLL_TOP) {
            $('body').append(scrollToTop);
        }
        if (DO_SHOW_ALL) {
            loadCss();
            if (SHOW_ALL_TEXT != '') {
                $('form.showall button.exclusive-medium span').html(SHOW_ALL_TEXT);
            }
        } else {
            $('form.showall button.exclusive-medium').hide();
        }
    }
    function loadMoreOnNoBL() {
        $('ul#product_list').after(loadMore);
        $('button.load-more').css({ "color": LOAD_MORE_FONT_COLOR});
        $('button.load-more span').css({    "border-color": LOAD_MORE_BACKGROUND_COLOR,
            "text-shadow": "none",
            "background": LOAD_MORE_BACKGROUND_COLOR,
        });
    }
    function loadMoreOffNoBL() {
        $('.load-more').remove();
    }
    function loadContentNoBL() {
        $("#product_list").append($("<div>").load(''+links+' #product_list > *', function() {
            loaderOff();
            $('#product_list li ~ div > li').unwrap();
            if (page_nr - 1 > total_pages) {
                $('ul#product_list').after(endMessage);
            }
        }));
        if (page_name == "search") {
            pos = links.search('p=');
            rest = links.slice(pos);
            links = links.replace(rest, "");
            rest = rest.slice(6);
            links += "p=" + page_nr + rest;
        } else {
            pos = links.search('p=');
            rest = links.slice(pos);
            links = links.replace(rest, "p=" + page_nr);
        }
        page_nr++;
        state = 2;
    }
    function lastProductVisibleNoBL() {
        triggerElem = $('ul#product_list .right-block').last().offset().top - $(window).outerHeight();
        if ( triggerElem > $(window).scrollTop() && state != 1 ) {
            state = 1;
        } else if (( triggerElem < $(window).scrollTop() && state != 2 ) && ( page_nr - 1 <= total_pages ) && !($('.load-more').length)) {
            if ( run_count == 1 ) {
                loadMoreOnNoBL();
                state = 2;
                run_count++;
            } else {
                loaderOn();
                $("#product_list").append($("<div>").load(''+links+' #product_list > *', function() {
                    loaderOff();
                    $('#product_list li ~ div > li').unwrap();
                    if ($('#list').hasClass('selected')) {
                        $('#list').click();
                    }
                    if (page_nr - 1 > total_pages) {
                        $('ul#product_list').after(endMessage);
                    }
                }));
                if (page_name == "search") {
                    pos = links.search('p=');
                    rest = links.slice(pos);
                    links = links.replace(rest, "");
                    rest = rest.slice(6);
                    links += "p=" + page_nr + rest;
                } else {
                    pos = links.search('p=');
                    rest = links.slice(pos);
                    links = links.replace(rest, "p=" + page_nr);
                }
                page_nr++;
                if (DO_LOAD_MORE) {
                    run_count++;
                }
                state = 2;
                if ( run_count - 1 > DO_LOAD_STEPS ) {
                    run_count = 1;
                }
            }
        }
    };
    // NO Block Layered Functions -------------

    //General Functions -------------
    function loadCss() {
        $('form.showall button.exclusive-medium span').css({    "background": SHOW_ALL_BACKGROUND_COLOR,
            "border-color": SHOW_ALL_BACKGROUND_COLOR,
            "text-shadow": "none",
            "color": SHOW_ALL_FONT_COLOR});
    }
    function loaderOn() {
        $('ul#product_list').after(gif);
        $('ul#product_list > *').css('opacity','0.7');
    }
    function loaderOff() {
        $('.loader-gif').remove();
        $('ul#product_list > *').css('opacity', '1');
    }
    //General Functions -------------

    function reloadContentInfiniteScroll(params_plus) {
        stopAjaxQuery();
        loaderOff();

        var data = $('#layered_form').serialize();
        $('.layered_slider').each( function () {
            var sliderStart = $(this).slider('values', 0);
            var sliderStop = $(this).slider('values', 1);
            if (typeof(sliderStart) == 'number' && typeof(sliderStop) == 'number') {
                data += '&'+$(this).attr('id')+'='+sliderStart+'_'+sliderStop;
            }
        });

        $(['price', 'weight']).each(function(it, sliderType) {
            if ($('#layered_'+sliderType+'_range_min').length) {
                data += '&layered_'+sliderType+'_slider='+$('#layered_'+sliderType+'_range_min').val()+'_'+$('#layered_'+sliderType+'_range_max').val();
            }
        });

        $('#layered_form .select option').each( function () {
            if($(this).attr('id') && $(this).parent().val() == $(this).val()) {
                data += '&'+$(this).attr('id') + '=' + $(this).val();
            }
        });

        if ($('.selectProductSort').length && $('.selectProductSort').val()) {
            if ($('.selectProductSort').val().search(/orderby=/) > 0) {
                // Old ordering working
                var splitData = [
                    $('.selectProductSort').val().match(/orderby=(\w*)/)[1],
                    $('.selectProductSort').val().match(/orderway=(\w*)/)[1]
                ];
            } else {
                // New working for default theme 1.4 and theme 1.5
                var splitData = $('.selectProductSort').val().split(':');
            }
            data += '&orderby='+splitData[0]+'&orderway='+splitData[1];
        }
        if ($('select[name=n]:first').length) {

            if (params_plus) {
                data += '&n=' + $('select[name=n]:first').val();
            } else {
                data += '&n=' + $('div.pagination form.showall').find('input[name=n]').val();
            }
        }

        var slideUp = true;
        if (typeof params_plus === 'undefined' || (!(typeof params_plus === 'string'))) {
            params_plus = '';
            slideUp = false;
        }

        // Get nb items per page
        var n = '';
        if (params_plus) {
            $('div.pagination select[name=n]').children().each(function(it, option) {
                if (option.selected)
                    n = '&n=' + option.value;
            });
        }

        var ajaxQuery = $.ajax(
            {
                type: 'GET',
                url: baseDir + 'modules/blocklayered/blocklayered-ajax.php',
                data: data+params_plus+n + '&infinitScroll=1',
                dataType: 'json',
                cache: false, // @todo see a way to use cache and to add a timestamps parameter to refresh cache each 10 minutes for example
                success: function(result)
                {
                    window.infinitescroll_called_ajax  = false;
                    if (typeof(result) === 'undefined') {
                        return;
                    }

                    if (result.meta_description != '') {
                        $('meta[name="description"]').attr('content', result.meta_description);
                    }

                    if (result.meta_keywords != '') {
                        $('meta[name="keywords"]').attr('content', result.meta_keywords);
                    }

                    if (result.meta_title != '') {
                        $('title').html(result.meta_title);
                    }

                    if (result.heading != '') {
                        $('h1.page-heading .cat-name').html(result.heading);
                    }

                    $('#layered_block_left').replaceWith(utf8_decode(result.filtersBlock));
                    $('.category-product-count, .heading-counter').html(result.categoryCount);

                    if (result.nbRenderedProducts == result.nbAskedProducts) {
                        $('div.clearfix.selector1').hide();
                    }

                    if (result.productList) {
                        var product_list_html = $.parseHTML(result.productList);
                        //Adaugare produse blocklayer la scroll
                        for(i in product_list_html) {
                            if($(product_list_html[i]).attr('id') == 'product_list') {
                                $('.product_list').append(utf8_decode($(product_list_html[i]).html()));
                                //Remove loader gif and reinitialise it to work at the next scroll
                                $('ul#product_list p').remove();
                                loaderOff();
                            }
                        }
                    }

                    $('.product_list').css('opacity', '1');
                    if ($.browser.msie) {
                        // Fix bug with IE8 and aliasing
                        $('.product_list').css('filter', '');
                    }

                    if (result.pagination.search(/[^\s]/) >= 0) {
                        var pagination = $('<div/>').html(result.pagination);
                        var pagination_bottom = $('<div/>').html(result.pagination_bottom);

                        if ($('<div/>').html(pagination).find('#pagination').length) {
                            $('#pagination').show();
                            $('#pagination').replaceWith(pagination.find('#pagination'));
                        } else {
                            $('#pagination').hide();
                        }

                        if ($('<div/>').html(pagination_bottom).find('#pagination_bottom').length) {
                            $('#pagination_bottom').show();
                            $('#pagination_bottom').replaceWith(pagination_bottom.find('#pagination_bottom'));
                        } else {
                            $('#pagination_bottom').hide();
                        }
                    } else {
                        $('#pagination').hide();
                        $('#pagination_bottom').hide();
                    }

                    paginationButton(result.nbRenderedProducts, result.nbAskedProducts);

                    // On submiting nb items form, relaod with the good nb of items
                    $('div.pagination form').on('submit', function(e) {
                        e.preventDefault();
                        var val = $('div.pagination select[name=n]').val();

                        $('div.pagination select[name=n]').children().each(function(it, option) {
                            if (option.value == val) {
                                $(option).attr('selected', true);
                            } else {
                                $(option).removeAttr('selected');
                            }
                        });

                        // Reload products and pagination
                        reloadContent();
                    });
                    if (typeof(ajaxCart) != "undefined") {
                        ajaxCart.overrideButtonsInThePage();
                    }

                    if (typeof(reloadProductComparison) == 'function') {
                        reloadProductComparison();
                    }

                    var filters = result.filters;
                    initFilters();
                    initSliders();

                    var current_friendly_url = result.current_friendly_url;

                    // Currente page url
                    if (typeof(current_friendly_url) === 'undefined') {
                        current_friendly_url = '#';
                    }

                    // Get all sliders value
                    $(['price', 'weight']).each(function(it, sliderType) {
                        if ($('#layered_'+sliderType+'_slider').length) {
                            // Check if slider is enable & if slider is used
                            if (typeof($('#layered_'+sliderType+'_slider').slider('values', 0)) != 'object') {
                                if ($('#layered_'+sliderType+'_slider').slider('values', 0) != $('#layered_'+sliderType+'_slider').slider('option' , 'min') ||
                                    $('#layered_'+sliderType+'_slider').slider('values', 1) != $('#layered_'+sliderType+'_slider').slider('option' , 'max')) {
                                    current_friendly_url += '/'+blocklayeredSliderName[sliderType]+'-'+$('#layered_'+sliderType+'_slider').slider('values', 0)+'-'+$('#layered_'+sliderType+'_slider').slider('values', 1);
                                }
                            }
                        } else if ($('#layered_'+sliderType+'_range_min').length) {
                            current_friendly_url += '/'+blocklayeredSliderName[sliderType]+'-'+$('#layered_'+sliderType+'_range_min').val()+'-'+$('#layered_'+sliderType+'_range_max').val();
                        }
                    });
                    //Url changer

                    if (current_friendly_url != '#/show-all') {
                        $('div.clearfix.selector1').show();
                    }

                    lockLocationChecking = true;


                    $('.hide-action').each(function() {
                        hideFilterValueAction(this);
                    });

                    if (display instanceof Function) {
                        var view = $.totalStorage('display');

                        if (view && view != 'grid') {
                            display(view);
                        }
                    }
                }
            });
        ajaxQueries.push(ajaxQuery);
    }
});