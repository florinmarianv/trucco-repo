/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2018 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */
(function(){
    var pos;
    var rest;
    $('.pagination').hide();
    var triggerAtY;
    var state;
    var total_pages;
    var link;
    var page_nr = 3;
    if ($('.pagination li a.previous[rel=prev]').length) {
        total_pages = $('.pagination li:nth-last-child(2) a').text().trim();
        link = $('ul.page-list li:nth-child(3) a').attr('href');
    } else {
        total_pages = $('.pagination li:last-child a').text().trim();
        link = $('ul.page-list li:nth-child(2) a').attr('href');
    }
    if (window.location.href == $('button.js-search-filters-clear-all').attr('data-search-url')) {
        $('button.js-search-filters-clear-all').prop('disabled', true);
    } else {
        $('button.js-search-filters-clear-all').prop('disabled', false);
    }
    var new_url;
    var initial_url = window.location.href;
    var gif = '<div class="loader-gif"><p>'+DO_LOADER_MESSAGE+'</p><img src="'+GIF_IMG_PATH+''+GIF+'"/></div>';
    var showAll = '<button class="show-all btn btn-primary">'+SHOW_ALL_TEXT+'</button>';
    var loadMore = '<button class="load-more btn btn-primary">'+LOAD_MORE_TEXT+'</button>';
    var scrollToTop = '<p class="scroll-to-top"><i class="material-icons md48">slideshow</i></p>';
    var endMessage = ('<p class="end-message">'+END_MESSAGE+'</p>');

    if (DO_LOAD_MORE) {
        var run_count = 0;
    }
    if (DO_SHOW_ALL) {
        if ( total_pages > 0 ) {
            $('div#js-product-list-top').append(showAll);
        }
    }
    if (DO_SCROLL_TOP) {
        $('body').append(scrollToTop);
    }

    function refreshAttrs() {
        $('.pagination').hide();
        page_nr = 3;
        if ($('.pagination li a.previous[rel=prev]').length) {
            total_pages = $('.pagination li:nth-last-child(2) a').text().trim();
            link = $('ul.page-list li:nth-child(3) a').attr('href');
        } else {
            total_pages = $('.pagination li:last-child a').text().trim();
            link = $('ul.page-list li:nth-child(2) a').attr('href');
        }
        if (DO_LOAD_MORE) {
            run_count = 0;
        }
        if (DO_SCROLL_TOP) {
            $('body').append(scrollToTop);
        }
        if(DO_SHOW_ALL) {
            if ( total_pages > 0 ) {
                $('div#js-product-list-top').append(showAll);
            }
        }
    }

    function loaderOn() {
        $('.products.row').append(gif);
        $('.products.row').css('opacity','0.7');
    }
    function loaderOff() {
        $('.loader-gif').remove();
        $('.products.row').css('opacity','1');
    }
    if (DO_LOAD_MORE) {
        function loadMoreOn() {
            $('.products.row').after(loadMore);
            $('button.load-more').css({	"background-color": LOAD_MORE_BACKGROUND_COLOR,
                "color": LOAD_MORE_FONT_COLOR});
        }
        function loadMoreOff() {
            $('.load-more').remove();
        }
    }

    elementVisible = function() {
        triggerAtY = $('.products .product-title').last().offset().top - $(window).outerHeight();
        if ( triggerAtY > $(window).scrollTop() && state != 1 ) {
            state = 1;
        } else if (( triggerAtY < $(window).scrollTop() && state != 2 ) && ( page_nr - 1 <= total_pages ) && !($('.load-more').length)) {
            if ( run_count == 1 ) {
                loadMoreOn();
                state = 2;
                run_count++;
            } else {
                loaderOn();
                $("div#js-product-list .products.row").append($("<div>").load(''+link+' div#js-product-list .products.row > *', function() {
                    loaderOff();
                    $('div#js-product-list .products.row > *:last-child > *').unwrap();
                    if (page_nr - 1 > total_pages) {
                        $('#js-product-list').append(endMessage);
                    }
                }));
                if (prestashop.urls.current_url.indexOf("controller=search") >= 0) {
                    pos = link.search('page=');
                    rest = link.slice(pos);
                    link = link.replace(rest, "");
                    rest = rest.slice(6);
                    link += "page=" + page_nr + rest;
                } else {
                    pos = link.search('page=');
                    rest = link.slice(pos);
                    link = link.replace(rest, "page=" + page_nr);
                }
                page_nr++;
                run_count++;
                state = 2;
                if ( run_count - 1 > DO_LOAD_STEPS ) {
                    run_count = 1;
                }
            }
        }
    };


    function loadContent() {
        $("div#js-product-list .products.row").append($("<div>").load(''+link+' div#js-product-list .products.row > *', function() {
            loaderOff();
            $('div#js-product-list .products.row > *:last-child > *').unwrap();
            if (page_nr - 1 > total_pages) {
                $('#js-product-list').append(endMessage);
            }
        }));
        if (prestashop.urls.current_url.indexOf("controller=search") >= 0) {
            pos = link.search('page=');
            rest = link.slice(pos);
            link = link.replace(rest, "");
            rest = rest.slice(6);
            link += "page=" + page_nr + rest;
        } else {
            pos = link.search('page=');
            rest = link.slice(pos);
            link = link.replace(rest, "page=" + page_nr);
        }
        page_nr++;
        state = 2;
    }
    function loadAll() {
        $("div#js-product-list .products.row").append($("<div>").load(''+link+' div#js-product-list .products.row > *', function() {
            $('div#js-product-list .products.row > *:last-child > *').unwrap();
        }));
        if (prestashop.urls.current_url.indexOf("controller=search") >= 0) {
            pos = link.search('page=');
            rest = link.slice(pos);
            link = link.replace(rest, "");
            rest = rest.slice(6);
            link += "page=" + page_nr + rest;
        } else {
            pos = link.search('page=');
            rest = link.slice(pos);
            link = link.replace(rest, "page=" + page_nr);
        }
        page_nr++;
        state = 2;
    }
    function loadCss() {
        $('button.show-all').css({	"background-color": SHOW_ALL_BACKGROUND_COLOR,
            "color": SHOW_ALL_FONT_COLOR});
    }

    $(document).ready(elementVisible, loadCss());
    $(window).bind('scroll', elementVisible);

    $('body').one('click' , '.show-all' , function () {
        if($('.load-more').length) {
            loadMoreOff();
        }
        $('.show-all').remove();
        for (var i = 2; i <= total_pages; i++) {
            loadAll();
        }
        $('#js-product-list').append(endMessage);
    });
    $('body').on('click' , '.load-more' , function () {
        loadMoreOff();
        loaderOn();
        loadContent();
    });
    $('body').on('click', '.scroll-to-top', function() {
        var offset = -100;
        $('html, body').animate({
            scrollTop: $("img.logo.img-responsive").offset().top + offset
        }, 1000);
    });
    $(document).ajaxComplete(function(){
        if (window.location.href == $('button.js-search-filters-clear-all').attr('data-search-url')) {
            $('button.js-search-filters-clear-all').prop('disabled', true);
        } else {
            $('button.js-search-filters-clear-all').prop('disabled', false);
        }
        new_url = window.location.href;
        if ( initial_url != new_url ) {
            refreshAttrs();
            initial_url = new_url;
        }
    });
})();