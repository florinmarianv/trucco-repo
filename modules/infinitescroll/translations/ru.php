<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_94a8d79e2842a79427ff33cb94176f02'] = 'Full Bundles Stocks PRO';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_7852715afb6fae034ef58f976c8a3d97'] = 'Автоматическая синхронизация количества пакетов в соответствии с содержимым';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_3150fdb7a9af95c37c95b41d54759875'] = 'Вы действительно хотите удалить модуль Full Bundles Stocks PRO?';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_e60263ac381ae4a9ad460a5c706b9b5a'] = 'Конфигурация сохранена';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_164cfc4939c5d6a6aafc209129a6270f'] = 'Проверить наличие автоматического обновления запасов  \"packs\"';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_a0573e4eb1100ef403b452e702db8442'] = 'Запасы обновлены !';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_402e7a087747cb56c718bde84651f96a'] = 'Успех!';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_520f9b37af10a8807ddc4ccfa8a9ad23'] = 'Обратите внимание!';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_dc3fd488f03d423a04da27ce66274c1b'] = 'Внимание!';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_9c6a7a30381b05d66b716f23c588817e'] = 'Не удалось обновить!';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_5235282ea376d75bfc6d21b9ce5e4b43'] = 'Нет пакетов!';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_902b0d55fddef6f8d651fe1035b7d4bd'] = 'Ошибка';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_c6701602de1528fd79d5eff3e8eb1edd'] = 'Что-то пошло не так!';
$_MODULE['<{bundlesstocks}prestashop>configure_6ee5e1dab41638b3c45208ee1819278d'] = 'Синхронизировать';
$_MODULE['<{bundlesstocks}prestashop>configure_bfa8a7819e4fec7d82b464b7d8eb7da8'] = 'Нажмите, чтобы обновить запасы для всех пакетов';
