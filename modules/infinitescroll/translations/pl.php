<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_94a8d79e2842a79427ff33cb94176f02'] = 'Full Bundles Stocks PRO';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_7852715afb6fae034ef58f976c8a3d97'] = 'Automatycznie zsynchronizuj ilość paczek zgodnie z zapasem zawierającym przedmioty';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_3150fdb7a9af95c37c95b41d54759875'] = 'Czy na pewno chcesz odinstalować moduł pełnego pakietu Stocks PRO ? ';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_e60263ac381ae4a9ad460a5c706b9b5a'] = 'Konfiguracja została zapisana';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_f4f70727dc34561dfde1a3c529b6205c'] = 'Ustawienia';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_164cfc4939c5d6a6aafc209129a6270f'] = 'Sprawdź automatyczną aktualizację zapasów \"packs \"';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_c9cc8cce247e49bae79f15173ce97354'] = 'Zapisać';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_a0573e4eb1100ef403b452e702db8442'] = 'Zapasy zostały zaktualizowane!';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_402e7a087747cb56c718bde84651f96a'] = 'Powodzenie!';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_520f9b37af10a8807ddc4ccfa8a9ad23'] = 'Ogłoszenie!';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_dc3fd488f03d423a04da27ce66274c1b'] = 'Ostrzeżenie!';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_9c6a7a30381b05d66b716f23c588817e'] = 'Aktualizacja nie powiodła się!';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_5235282ea376d75bfc6d21b9ce5e4b43'] = 'Brak zdefiniowanych paczek!';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_902b0d55fddef6f8d651fe1035b7d4bd'] = 'Błąd';
$_MODULE['<{bundlesstocks}prestashop>bundlesstocks_c6701602de1528fd79d5eff3e8eb1edd'] = 'Coś poszło nie tak!';
$_MODULE['<{bundlesstocks}prestashop>configure_6ee5e1dab41638b3c45208ee1819278d'] = 'Synchronizować';
$_MODULE['<{bundlesstocks}prestashop>configure_bfa8a7819e4fec7d82b464b7d8eb7da8'] = 'Kliknij, aby zaktualizować stan wszystkich paczek';
