<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{infinitescroll}prestashop>infinitescroll_9d2861a099c1126bd0d865b1c95a5361'] = 'Infinite Scroll	';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_0a30dc33a8812604df50d4df678ae3d3'] = 'Acest modul îți permite să afișezi produsele într-un scroll infinit pe pagină.';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_6c58031e7824c3cd6febc2f2107b0652'] = 'Configurare actualizată';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_52f4393e1b52ba63e27310ca92ba098c'] = 'Setări generale';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_e8b29de9f129834fd1593f05ace47703'] = 'Activează modulul';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_8110eae95ded27f8d4c7dd6ea6e4f5d1'] = 'Activează sau dezactivează modulul';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activat';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_b9f5c797ebbf55adccdd8539a65a0241'] = 'Dezactivat';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_c9cc8cce247e49bae79f15173ce97354'] = 'Salvează';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_da22c93ccb398c72070f4000cc7b59a1'] = 'Personalizare';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_ffa0c7e4e94ab8a5a1a714637c29b891'] = 'Activează butonul Arată tot';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_5b3a7f000014c70515d7ea9a51b6c0f8'] = 'Folosește pentru a afișa butonul ”Arată tot” pentru a afișa toate produsele din prima.';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_ec59e123745ad02d01f79313b06b72b4'] = 'Textul care apare pe buton și care afișează toate produsele. (”Arată tot”)';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_9dffbf69ffba8bc38bc4e01abf4b1675'] = 'Text';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_75e7422f2fe253fc69d3f6c148d197ee'] = 'Culoare backgroumd';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_2203fc761490c5856809e1199d5284b5'] = 'Culoare font';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_e6756c243a9ed38c4868026c710a03e5'] = 'Afișează butonul Încarcă mai multe';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_b7b74e87d5020728693d40a6b56fb888'] = 'Folosește pentru a afișa butonul ”Încarcă mai multe”';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_67708bfaaae3be1b86a98e17b92d6c6e'] = 'Textul care apare pe buton și care încarcă mai multe produse. (”Încarcă mai multe”)';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_59b570f440c1dd01b168fbb5ae219e58'] = 'Pașii de încărcare';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_351901bb0a4085c4d0a9defa35b7e502'] = 'Butonul derulează în sus';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_8641ec6a21cf7e1d9d80d49fceb965e7'] = 'Folosește acest buton pentru a derula pagina înapoi sus.';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_3521732c0d5a358f4a04daff01d288ad'] = 'Selectează un încărcător care va apărea de fiecare dată când produsele se vor încărca.';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_129a860545e73b19bb88ab90211cd0b0'] = 'Încărcător';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_2058dfe0009d3ff972403fd918bd7e24'] = 'Urcă încărcătorul tău';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_84e118af94741bd336a01728c9a5f79d'] = 'Urcă încărcătorul';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_3f2654c5d09fe18e61e256bbff0c5bcb'] = 'Scrie mesajul care apare de fiecare dată când se încarcă produsele';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_75c94591df27a47b94a4d9216cef8e6d'] = 'Mesajul încărcătorului';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_676ef5f7d55fd652fe7212efc81a5eac'] = 'Introdu mesajul care apare când toate produsele au fost încărcate';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_33ed62b413cdff2e28595bdb4f5bc992'] = 'Mesajul de final al produselor';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_82345e3aed96820f0bbe495d073450ea'] = 'Fișierul este o imagine -\' .$check[\"mime\"] . \'.';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_a4dca1d5a2e7e634c754722f0a8d7056'] = 'Fișierul nu este o imagine.';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_f87ec306f5feaebda9785a7a6d446590'] = 'Scuze, fișierul există deja.';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_1e72f46250a34d103164814c2ccafc24'] = 'Scuze, doar fișierele GIF sunt permise.';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_64a7bd4c45d9c17db46c3cdff25895a6'] = 'Scuze, fișierul tău nu a fost încărcat.';
$_MODULE['<{infinitescroll}prestashop>infinitescroll_9b56eb1bbb1be09d962ffa317175d95b'] = 'Scuze, a apărut o eroare în timp ce încărcai fișierul.';
$_MODULE['<{infinitescroll}prestashop>form_869a0830e1f6c535cd704cfaeb9ba90e'] = 'Selectează paginile în care vrei să funcționează plugin-ul';
$_MODULE['<{infinitescroll}prestashop>form_c56a6c335b8f357feb0ea215d8c62184'] = 'Paginile selectate';
$_MODULE['<{infinitescroll}prestashop>form_f50b74a83883c9a44040bcecdde6b2fc'] = 'Cele mai vândute';
$_MODULE['<{infinitescroll}prestashop>form_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Categorie';
$_MODULE['<{infinitescroll}prestashop>form_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Producător';
$_MODULE['<{infinitescroll}prestashop>form_82821b662d92b4378763af71a550fe08'] = 'Produse noi';
$_MODULE['<{infinitescroll}prestashop>form_a950f3cb6c24a50a34c4f74ad74020e3'] = 'Cădere de prețuri';
$_MODULE['<{infinitescroll}prestashop>form_ec136b444eede3bc85639fac0dd06229'] = 'Furnizor';
$_MODULE['<{infinitescroll}prestashop>form_13348442cc6a27032d2b4aa28b75a5d3'] = 'Caută';
$_MODULE['<{infinitescroll}prestashop>form_c46015e7999d8dac93dbf51a86a4f3b9'] = 'Lista de pagini';
$_MODULE['<{infinitescroll}prestashop>form_1063e38cb53d94d386f21227fcd84717'] = 'Șterge';
$_MODULE['<{infinitescroll}prestashop>form_ec211f7c20af43e742bf2570c3cb84f9'] = 'Adaugă';
