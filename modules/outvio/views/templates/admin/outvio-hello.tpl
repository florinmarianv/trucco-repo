{*
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Outvio OU
 *  @copyright 2017-2018 Outvio OU
 *  @license   Outvio OU
 *}

<h4>{$message|escape:'htmlall':'UTF-8'}</h4>

