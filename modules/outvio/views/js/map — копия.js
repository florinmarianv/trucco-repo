<script>
  	17
		

  // Initialize and add the map

	var locations = [
	
	];
	function initMap()  {

		var lat = [];
		var lng = [];

		for (var i = 0 ; i < locations.length; i++) {
			//list_hotspot.push(locations[i]);
			if (locations[i].lat > 1 || locations[i].lng > 1) {
				lat.push(locations[i].lat);
				lng.push(locations[i].lng);
			}

			//lat.push(locations[i].lat);
			//lng.push(locations[i].lng);
		}
		var lat_min = Math.min.apply(null, lat);
		var lat_max = Math.max.apply(null, lat);
		var lng_min = Math.min.apply(null, lng);
		var lng_max = Math.max.apply(null, lng);
		var center_lat = (lat_max + lat_min) / 2;
		var center_lng = (lng_max + lng_min) / 2;
		//console.log(center_lat + '     '+center_lng);
		var center =  {
			lat: center_lat,
			lng: center_lng
		};

		var map_zoom = 13;
		if (window.innerWidth < 500) {
			map_zoom = 12;
		}
		var map = new google.maps.Map(document.getElementById('map'),  {zoom: map_zoom });
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({address: '11614', componentRestrictions:{country:'EE'}}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                		//console.log('set center zip');
                        map.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                                map: map,
                                position: results[0].geometry.location
                        });
                } else {
                        alert("Geocode was not successful for the following reason: " + status);
                }
        });
		var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		var infoWin = new google.maps.InfoWindow();
		
		var markers = locations.map(function(location, i) {
			var marker = new google.maps.Marker({
		  		icon: "http://prestashopdemo.zlabsolutions.com/1761//modules/outvio/views/img/marker.png",
		    	position: location,
				draggable: false,
				map: map,
				zIndex: 0
		  	});

			  google.maps.event.addListener(marker, 'click', function(evt) {
			    
			    var clatlng = {
				    lat: location.lat,
				    lng: location.lng

			    };
			    map.setCenter(clatlng);
			    var formatted_address = '';
			    infoWin.setContent('<br><h3>'+location.point_name + '</h3>');
			    geocoder.geocode({ location: clatlng }, (results, status) => {
				    if (status === "OK") {
				    	if (results[0]) {
				    		console.log('results saved');
				    		console.log(results[0]);
				    		infoWin.setContent('<br><h3>'+location.point_name + '</h3><p>'+results[0].formatted_address+'</p>'+ '<p>'+location.wh+'</p>' + '<input type="hidden" class="select_pickup" value="" data-info="'+ location.json_info+'"  \>');
				    	}
				    }
			    });
			    //console.log(formatted_address);
			    //infoWin.setContent('<p>'+location.address+'</p>' + '<p>'+formatted_address+'</p>' + '<p>'+location.wh+'</p>' + '<input type="hidden" class="select_pickup" value="" data-info="'+ location.json_info+'"  \>');
			    Outv.ajaxSavePoint(location.json_info);
			    infoWin.open(map, marker);
			    $('button[name="confirmDeliveryOption"]').prop('disabled', false);
			  })
		  return marker;
		});
		// Add a marker clusterer to manage the markers.
		var markerCluster = new MarkerClusterer(map, markers,
		    {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

		//find center
		/*
		var lat = [];
		var lng = [];
		for (var i = locations.length - 1; i >= 0; i--) {
			lat.push(locations[i].lat);
			lng.push(locations[i].lng);
		}
		
		var lat_min = Math.min(lat);
		var lat_max = Math.max(lat);
		var lng_min = Math.min(lng);
		var lng_max = Math.max(lng);


		/*
		map.setCenter(new google.maps.LatLng(
			((lat_max + lat_min) / 2.0),
			((lng_max + lng_min) / 2.0)
		));
		*/
		/*
		map.fitBounds(new google.maps.LatLngBounds(
			//bottom left
			new google.maps.LatLng(lat_min, lng_min),
			//top right
			new google.maps.LatLng(lat_max, lng_max)
		));
		*/
		/*
		var bounds = new google.maps.LatLngBounds();
		var infowindow = new google.maps.InfoWindow();

		for (i = 0; i < locations.length; i++) {  
		  var marker = new google.maps.Marker({
		    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		    map: map
		  });

		  //extend the bounds to include each marker's position
		  bounds.extend(marker.position);
		  google.maps.event.addListener(marker, 'click', (function(marker, i) {
		    return function() {
		      //console.log('work');
		      infowindow.setContent('<br>'+locations[i][0]);
		      infowindow.open(map, marker);
		    }
		  })(marker, i));
		}
		map.fitBounds(bounds);

		//(optional) restore the zoom level after the map is done scaling
		var listener = google.maps.event.addListener(map, "idle", function () {
		    map.setZoom(12);
		    google.maps.event.removeListener(listener);
		});
		*/
		//end find center	
	}

  </script>