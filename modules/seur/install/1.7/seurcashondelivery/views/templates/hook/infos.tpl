{*
 *  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
 *
 * @author    Línea Gráfica E.C.E. S.L.
 * @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
 * @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
 *}
<div class="alert alert-info">
<img src="../modules/seurcashondelivery/img/logoSeur.png" style="float:left; margin-right:15px;">
<p><strong>{l s='This module allows you to accept secure payments by Seur cash on delivery.' mod='seurcashondelivery'}</strong></p>
    {l s='Cash on delivery by SEUR' mod='seurcashondelivery'}:<br>
    {l s='Cost:' mod='seurcashondelivery'} {Tools::displayPrice($coste)}<br>
    {l s='Fee:' mod='seurcashondelivery'} {Tools::displayPrice($cart_Amount)}<br>
    <strong>{l s='Total:' mod='seurcashondelivery'} {Tools::displayPrice($total)}</strong>

</div>
