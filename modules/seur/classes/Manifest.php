<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2015 PrestaShop SA
 * @version  Release: 0.4.4
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
    exit;

class SeurManifest
{
    public static function createManifest($id_orders)
    {
        $merchants  = array();

        foreach($id_orders as $id_order)
        {
            $seurOrder = new SeurOrder((int)$id_order);

            $order = new Order((int)$seurOrder->id_order);

            $order_manifest = array();

            $order_manifest['id'] =  $seurOrder->id;
            $order_manifest['reference'] =  $order->reference;
            $order_manifest['consig_name'] =  $seurOrder->firstname." ".$seurOrder->lastname;
            $order_manifest['consig_address'] =  $seurOrder->address1." ".$seurOrder->address2;
            $order_manifest['consig_postalcode'] =  $seurOrder->postcode;
            $order_manifest['consig_phone'] =  $seurOrder->phone_mobile;
            $order_manifest['bultos'] =  $seurOrder->numero_bultos;
            $order_manifest['producto'] =  $seurOrder->product;
            $order_manifest['ecb'] =   explode(" - ", $seurOrder->ecb);
            $order_manifest['servicio'] =  $seurOrder->service;
            $order_manifest['bultos'] =  $seurOrder->numero_bultos;
            $order_manifest['peso'] =  $seurOrder->peso_bultos;
//            $order_manifest['cashondelivery'] =  $seurOrder->cashondelivery;

            //validamos que es un pedido contra reembolso de seur
            if($order->module == "seurcashondelivery"){
            //añadimos total del pago en el campo reembolso
                $order_manifest['cashondelivery'] = $seurOrder->total_paid;
            //seteamos el valor
                Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('UPDATE `'._DB_PREFIX_.'seur2_order` SET `cashondelivery` = '.$seurOrder->total_paid);
            }else{
                $order_manifest['cashondelivery'] = $seurOrder->cashondelivery;
            }

            $order_manifest['otros'] = $seurOrder->other;
            $state = new State($seurOrder->id_state);
            $order_manifest['state'] = $state->name;
            $country = new Country($seurOrder->id_country, Context::getContext()->language->id);
            $order_manifest['country'] = $country->name;

            $merchants[$seurOrder->id_seur_ccc][] = $order_manifest;

            $seurOrder->manifested = 1;
            $seurOrder->save();
        }


        foreach ($merchants as $key => $orders)
        {
            $pdf = new PDFGenerator();
            $pdf->SetHeaderMargin(10);
            $pdf->SetPrintHeader(true);
            $pdf->setFont($pdf->font, '', 5, '', false);
            $pdf->setHeaderFont(array($pdf->font, '', 7, '', false));
            $pdf->setFooterFont(array($pdf->font, '', 8, '', false));
            $pdf->SetMargins(15, 30, 15);

            $smarty = Context::getcontext()->smarty;

            $company = Configuration::get(SEUR2_MERCHANT_COMPANY);
            $cif = Configuration::get(SEUR2_MERCHANT_NIF_DNI);

            $merchant = new SeurCCC($key);

            $smarty->assign("ccc", $merchant->ccc);
            $smarty->assign("company", $company);
            $smarty->assign("cif", $cif);
            $smarty->assign("franchise", $merchant->franchise);
            $smarty->assign("street_type", $merchant->street_type);
            $smarty->assign("street_name", $merchant->street_name);
            $smarty->assign("street_number", $merchant->street_number);
            $smarty->assign("state", $merchant->state);
            $smarty->assign("postalcode", $merchant->post_code);
            $smarty->assign("city", $merchant->town);
            $smarty->assign("date", date('d/m/Y'));
            $smarty->assign("hour", date('H:i'));
            $smarty->assign("orders", $orders);


            $manifest_header = $smarty->fetch(_PS_MODULE_DIR_."seur/views/templates/admin/manifest-header.tpl");

            $pdf->createHeader($manifest_header);

            $pdf->AddPage('P', 'A4');

            $manifest_content = $smarty->fetch(_PS_MODULE_DIR_."seur/views/templates/admin/manifest-content.tpl");
            $pdf->writeHTML($manifest_content, false, false, false, false, 'P');

            $pdf->Output("Manifiesto_".$merchant->ccc."_".date('YmdHis').".pdf", 'I');

        }
    }


    public function processBulkGeneraETQNacional(){
        $id_orders = Tools::getValue('orderBox');

        $pdf = new PDFGenerator();
        $pdf->SetPrintHeader(false);
        $pdf->SetFontSize(12);
        $pdf->SetMargins(15, 50, 15);

        $this->context->cookie->__set('id_orders',json_encode($id_orders));
        $this->context->cookie->update();

        foreach($id_orders as $id_order)
        {
            $order = new Order((int)$id_order);
            $address = new Address((int)$order->id_address_delivery);
            $customer = new Customer((int)$order->id_customer);
            $country = new Country($address->id_country, Context::getContext()->language->id);
            $state = new State($address->id_state, Context::getContext()->language->i);

            $order->etiquetado = 1;
            $order->save();

            Context::getcontext()->smarty->assign("address", $address);
            Context::getcontext()->smarty->assign("customer", $customer);
            Context::getcontext()->smarty->assign("state", $state);
            Context::getcontext()->smarty->assign("country", $country);

            $carrier = new Carrier($order->id_carrier);
            $address_template = Context::getContext()->smarty->fetch(_PS_MODULE_DIR_."seur/views/templates/admin/manifest.tpl");

            $pdf->AddPage('P', 'A4');
            $pdf->writeHTML($address_template, false, false, false, false, 'P');
        }
        $pdf->Output("Manifiesto'.$this->tipo.date('YmdHis').'.pdf", 'I');
    }


}
