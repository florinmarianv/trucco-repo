<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @version  Release: 0.4.4
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class SeurPickup
{
	public static function createPickup($id_seur_ccc=1)
	{
		if ((int)date('H') < '14')
//		if ((int)date('H') < '22')
		{
			try
			{
				$sc_options = array(
					'connection_timeout' => 30
				);

				$soap_client = new SoapClient((string)Configuration::get('SEUR2_URLWS_R'), $sc_options);

				$merchant_data = SeurLib::getMerchantData($id_seur_ccc);

				if (!isset($merchant_data['street_number']))
					return false;

				$numeroVia = filter_var($merchant_data['street_number'], FILTER_SANITIZE_NUMBER_INT);

				$plano = '
					<recogida>
						<usuario>'.pSQL(Configuration::get('SEUR2_WS_USERNAME')).'</usuario>
						<password>'.pSQL(Configuration::get('SEUR2_WS_PASSWORD')).'</password>
						<razonSocial>'.pSQL(Configuration::get('SEUR2_MERCHANT_COMPANY')).'</razonSocial>
						<nombreEmpresa>'.pSQL(Configuration::get('SEUR2_MERCHANT_COMPANY')).'</nombreEmpresa>
						<nombreContactoOrdenante>'.pSQL(Configuration::get('SEUR2_MERCHANT_FIRSTNAME')).'</nombreContactoOrdenante>
						<apellidosContactoOrdenante>'.pSQL(Configuration::get('SEUR2_MERCHANT_LASTNAME')).'</apellidosContactoOrdenante>
						<prefijoTelefonoOrdenante>34</prefijoTelefonoOrdenante>
						<telefonoOrdenante>'.pSQL($merchant_data['phone']).'</telefonoOrdenante>
						<prefijoFaxOrdenante />
						<faxOrdenante />
						<nifOrdenante>'.Configuration::get('SEUR2_MERCHANT_NIF_DNI').'</nifOrdenante>
						<paisNifOrdenante>ES</paisNifOrdenante>
						<mailOrdenante>'.pSQL($merchant_data['email']).'</mailOrdenante>
						<tipoViaOrdenante>'.pSQL($merchant_data['street_type']).'</tipoViaOrdenante>
						<calleOrdenante>'.pSQL($merchant_data['street_name']).'</calleOrdenante>
						<tipoNumeroOrdenante>N.</tipoNumeroOrdenante>
						<numeroOrdenante>'.pSQL($numeroVia).'</numeroOrdenante>
						<escaleraOrdenante>'.pSQL($merchant_data['staircase']).'</escaleraOrdenante>
						<pisoOrdenante>'.pSQL($merchant_data['floor']).'</pisoOrdenante>
						<puertaOrdenante>'.pSQL($merchant_data['door']).'</puertaOrdenante>
						<codigoPostalOrdenante>'.pSQL($merchant_data['post_code']).'</codigoPostalOrdenante>
						<poblacionOrdenante>'.pSQL($merchant_data['town']).'</poblacionOrdenante>
						<provinciaOrdenante>'.pSQL($merchant_data['state']).'</provinciaOrdenante>
						<paisOrdenante>'.pSQL($merchant_data['country']).'</paisOrdenante>

						<diaRecogida>'.pSQL(sprintf( '%02d', date('d') )).'</diaRecogida>
						<mesRecogida>'.date('m').'</mesRecogida>
						<anioRecogida>'.date('Y').'</anioRecogida>
						<servicio>1</servicio>
						<horaMananaDe></horaMananaDe>
						<horaMananaA></horaMananaA>
						<numeroBultos>1</numeroBultos>
						<mercancia>2</mercancia>
						<horaTardeDe>16:00</horaTardeDe>
						<horaTardeA>20:00</horaTardeA>
						<tipoPorte>F</tipoPorte>
						<observaciones></observaciones>
						<tipoAviso>EMAIL</tipoAviso>
						<idiomaContactoOrdenante>'.pSQL($merchant_data['country']).'</idiomaContactoOrdenante>

						<razonSocialDestino></razonSocialDestino>
						<nombreContactoDestino></nombreContactoDestino>
						<apellidosContactoDestino></apellidosContactoDestino>
						<telefonoDestino></telefonoDestino>
						<tipoViaDestino></tipoViaDestino>
						<calleDestino></calleDestino>
						<tipoNumeroDestino></tipoNumeroDestino>
						<numeroDestino></numeroDestino>
						<escaleraDestino />
						<pisoDestino />
						<puertaDestino />
						<codigoPostalDestino></codigoPostalDestino>
						<poblacionDestino></poblacionDestino>
						<provinciaDestino></provinciaDestino>
						<paisDestino></paisDestino>
						<prefijoTelefonoDestino></prefijoTelefonoDestino>

						<razonSocialOrigen>'.Configuration::get('SEUR2_MERCHANT_COMPANY').'</razonSocialOrigen>
						<nombreContactoOrigen>'.Configuration::get('SEUR2_MERCHANT_FIRSTNAME').'</nombreContactoOrigen>
						<apellidosContactoOrigen>'.Configuration::get('SEUR2_MERCHANT_LASTNAME').'</apellidosContactoOrigen>
						<telefonoRecogidaOrigen>'.pSQL($merchant_data['phone']).'</telefonoRecogidaOrigen>
						<tipoViaOrigen>'.pSQL($merchant_data['street_type']).'</tipoViaOrigen>
						<calleOrigen>'.pSQL($merchant_data['street_name']).'</calleOrigen>
						<tipoNumeroOrigen>N.</tipoNumeroOrigen>
						<numeroOrigen>'.pSQL($numeroVia).'</numeroOrigen>
						<escaleraOrigen>'.pSQL($merchant_data['staircase']).'</escaleraOrigen>
						<pisoOrigen>'.pSQL($merchant_data['floor']).'</pisoOrigen>
						<puertaOrigen>'.pSQL($merchant_data['door']).'</puertaOrigen>
						<codigoPostalOrigen>'.pSQL($merchant_data['post_code']).'</codigoPostalOrigen>
						<poblacionOrigen>'.pSQL($merchant_data['town']).'</poblacionOrigen>
						<provinciaOrigen>'.pSQL($merchant_data['state']).'</provinciaOrigen>
						<paisOrigen>'.pSQL($merchant_data['country']).'</paisOrigen>
						<prefijoTelefonoOrigen>34</prefijoTelefonoOrigen>

						<producto>2</producto>
						<entregaSabado>N</entregaSabado>
						<entregaNave>N</entregaNave>
						<tipoEnvio>N</tipoEnvio>
						<valorDeclarado>0</valorDeclarado>
						<listaBultos>1;1;1;1;1/</listaBultos>
						<cccOrdenante>'.pSQL($merchant_data['ccc']).'-'.pSQL($merchant_data['franchise']).'</cccOrdenante>
						<numeroReferencia></numeroReferencia>
						<ultimaRecogidaDia />
						<nifOrigen>'.Configuration::get('SEUR2_MERCHANT_NIF_DNI').'</nifOrigen>
						<paisNifOrigen>'.pSQL($merchant_data['country']).'</paisNifOrigen>
						<aviso>N</aviso>
						<cccDonde>'.pSQL($merchant_data['ccc']).'-'.pSQL($merchant_data['franchise']).'</cccDonde >
						<cccAdonde></cccAdonde>
						<tipoRecogida>N</tipoRecogida>
					 </recogida>
			   ';

				$data = array(
					'in0' => $plano
				);

				$response = $soap_client->crearRecogida($data);

//				print_r($data);
//				print_r($response);

				$string_xml = htmlspecialchars_decode((($response->out)));
				$xml = simplexml_load_string($string_xml);

				if (!empty($xml->DESCRIPCION)){

					return (string)$xml->DESCRIPCION;
				}
				elseif (!self::insertPickup((int)$id_seur_ccc, (string)$xml->LOCALIZADOR, (string)$xml->NUM_RECOGIDA, (float)$xml->TASACION))
					return 'Error en base de datos.'; // @TODO check if must be translatable

				Configuration::updateValue('SEUR_CONFIGURATION_OK', true);
			}
			catch (PrestaShopException $e)
			{
				//$e->displayMessage();
				return false;
			}
			catch (SoapFault $e)
			{
    				//$e->displayMessage();
    				return false;
			}
		}
		else
		{
			$module_instance = Module::getInstanceByName('seur');
			$module_instance->adminDisplayWarning($module_instance->l('Pickups after 2pm cannot be arranged via module, contact us by phone to arrange it manually.'));
		}
	}

	private static function insertPickup($id_seur_ccc,$localizer, $numPickup, $tasacion)
	{
		return Db::getInstance()->Execute('
			REPLACE INTO `'._DB_PREFIX_.'seur2_pickup`
				(`id_seur_pickup`, `localizer`, `num_pickup`, `tasacion`, `date`)
			VALUES
				('.(int)$id_seur_ccc.', "'.pSQL($localizer).'", "'.pSQL($numPickup).'", "'.(float)$tasacion.'", NULL)
		');
	}

	public static function getLastPickup($id_seur_ccc=1)
	{
		$pickup_data = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT c.id_seur_ccc, p.*
			FROM `'._DB_PREFIX_.'seur2_ccc` c
			LEFT JOIN `'._DB_PREFIX_.'seur2_pickup` p ON c.id_seur_ccc = p.id_seur_pickup AND SUBSTR(date,1,10) = "'.date('Y-m-d').'"
			WHERE c.id_seur_ccc = '.(int)$id_seur_ccc) ;

		return $pickup_data;
	}

    public static function cancelPickup($id_seur_ccc=1){
            try
            {
                $sc_options = array(
                    'connection_timeout' => 30
                );

                $pickup = self::getLastPickup($id_seur_ccc);

                $soap_client = new SoapClient((string)Configuration::get('SEUR2_URLWS_R'), $sc_options);

                $merchant_data = SeurLib::getMerchantData($id_seur_ccc);

                if (!isset($merchant_data['street_number']))
                    return false;

                $data = array(
                    'in0' => "",
                    'in1' => $pickup['num_pickup'],
                    'in2' => pSQL(Configuration::get('SEUR2_WS_USERNAME')),
                    'in3' => pSQL(Configuration::get('SEUR2_WS_PASSWORD')),
                );

                $response = $soap_client->anularRecogida($data);

                if(strpos($response->out, 'Error')===0){

                    return (string)$response->out;
                }
                elseif (!self::deletePickup((int)$id_seur_ccc))
                    return 'Error en base de datos.'; // @TODO check if must be translatable

                Configuration::updateValue('SEUR_CONFIGURATION_OK', true);
            }
            catch (PrestaShopException $e)
            {
                //$e->displayMessage();
                return false;
            }
            catch (SoapFault $e)
            {
                //$e->displayMessage();
                return false;
            }
    }

    private static function deletePickup($id_seur_ccc)
    {
        return Db::getInstance()->Execute('
			DELETE FROM `'._DB_PREFIX_.'seur2_pickup`
			WHERE id_seur_pickup = '.(int)$id_seur_ccc);
    }

}
