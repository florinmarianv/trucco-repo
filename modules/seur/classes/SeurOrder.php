<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2015 PrestaShop SA
 *  @version  Release: 0.4.4
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
    exit;

class SeurOrder extends ObjectModel
{
    public $id_seur_order;
    public $id_order;
    public $id_seur_ccc;
    public $id_address_delivery;
    public $id_status;
    public $status_text;
    public $id_seur_carrier;
    public $product;
    public $service;
    public $numero_bultos;
    public $peso_bultos;
    public $ecb;
    public $labeled;
    public $manifested;
    public $date_labeled;
    public $codfee;
    public $cashondelivery;
    public $total_paid;
    public $firstname;
    public $lastname;
    public $id_country;
    public $id_state;
    public $address1;
    public $address2;
    public $postcode;
    public $city;
    public $dni;
    public $other;
    public $phone;
    public $phone_mobile;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'seur2_order',
        'primary' => 'id_seur_order',
        'multilang' => false,
        'fields' => array(
            'id_order' =>               array('type' => self::TYPE_INT, 'required' => true, 'validate' => 'isInt'),
            'id_seur_ccc' =>            array('type' => self::TYPE_INT, 'required' => true, 'validate' => 'isInt'),
            'numero_bultos' =>          array('type' => self::TYPE_INT, 'required' => true, 'validate' => 'isInt'),
            'peso_bultos' =>            array('type' => self::TYPE_FLOAT, 'required' => true, 'validate' => 'isFloat'),
            'ecb' =>                    array('type' => self::TYPE_STRING, 'size' => 68),
            'labeled' =>                array('type' => self::TYPE_BOOL, 'required' => true),
            'manifested' =>             array('type' => self::TYPE_BOOL, 'required' => true),
            'date_labeled' =>           array('type' => self::TYPE_DATE),
            'codfee' =>                 array('type' => self::TYPE_FLOAT, 'required' => true, 'validate' => 'isPrice'),
            'cashondelivery' =>         array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'id_address_delivery' =>    array('type' => self::TYPE_INT, 'required' => true, 'validate' => 'isInt'),
            'id_status' =>              array('type' => self::TYPE_INT, 'required' => true, 'validate' => 'isInt'),
            'status_text' =>            array('type' => self::TYPE_STRING),
            'id_seur_carrier' =>        array('type' => self::TYPE_INT, 'required' => true, 'validate' => 'isInt'),
            'service' =>                array('type' => self::TYPE_STRING, 'required' => true, 'size' => 5),
            'product' =>                array('type' => self::TYPE_STRING, 'required' => true, 'size' => 5),
            'total_paid' =>             array('type' => self::TYPE_FLOAT, 'required' => true, 'validate' => 'isPrice'),
            'firstname' =>              array('type' => self::TYPE_STRING, 'required' => true, 'size' => 32),
            'lastname' =>               array('type' => self::TYPE_STRING, 'required' => true, 'size' => 32),
            'id_country' =>             array('type' => self::TYPE_INT, 'required' => true, 'validate' => 'isInt'),
            'id_state' =>               array('type' => self::TYPE_INT, 'required' => true, 'validate' => 'isInt'),
            'address1' =>               array('type' => self::TYPE_STRING, 'required' => true, 'size' => 128),
            'address2' =>               array('type' => self::TYPE_STRING, 'size' => 128),
            'postcode' =>               array('type' => self::TYPE_STRING, 'required' => true, 'size' => 12),
            'city' =>                   array('type' => self::TYPE_STRING, 'size' => 64),
            'dni' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isDniLite', 'size' => 16),
            'other' =>                  array('type' => self::TYPE_STRING ),
            'phone' =>                  array('type' => self::TYPE_STRING, 'size' => 32),
            'phone_mobile' =>           array('type' => self::TYPE_STRING, 'size' => 32),
        ),
    );

    public static function getByOrder($id_order){

        $sql = "SELECT id_seur_order FROM `"._DB_PREFIX_."seur2_order` so WHERE id_order = ".(int)$id_order;

        $id_seur_order =  Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);

        if((int)$id_seur_order)
            $seurOrder = new SeurOrder((int)$id_seur_order);
        else
            $seurOrder = NULL;

        return($seurOrder);
    }

    public static function getUpdatableOrders(){

        $sql = "SELECT DISTINCT id_order FROM `"._DB_PREFIX_."seur2_order` so 
                LEFT JOIN `"._DB_PREFIX_."seur2_status` ss ON ss.id_status=so.id_status 
                WHERE ecb != '' AND (grupo !='ENTREGADO' OR so.id_status = 0) AND date_labeled > (DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) ";


        $results =  Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        $orders = array();
        foreach($results as $result){
            $orders[] = $result['id_order'];
        }

        return($orders);

    }

    public static function getStatusExpedition($type, $cod){

        $sql = "SELECT * FROM `"._DB_PREFIX_."seur2_status` ss WHERE cod_tipo_situ = '".$type."' AND cod_situ = '".(int)$cod."'";

        $results =  Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);

        return($results);

    }


    public static function getGroupStatusExpedition($id_status){

        $sql = "SELECT grupo FROM `"._DB_PREFIX_."seur2_status` ss WHERE id_status = '".$id_status."'";

        $result =  Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);

        return($result);

    }


    public function setCurrentStatus($id_status,$status_text)
    {
        $this->id_status = $id_status;
        $this->status_text = $status_text;
        $this->save();

        $group = self::getGroupStatusExpedition($id_status);

        switch ($group){
            case "EN TRANSITO":
                $id_status_order = (int)ConfigurationCore::get('SEUR2_STATUS_IN_TRANSIT');
                break;
            case "APORTAR SOLUCIÓN":
                $id_status_order = (int)ConfigurationCore::get('SEUR2_STATUS_CONTRIBUTE_SOLUTION');
                break;
            case "DEVOLUCION EN CURSO":
                $id_status_order = (int)ConfigurationCore::get('SEUR2_STATUS_RETURN_IN_PROGRESS');
                break;
            case "DISPONIBLE PARA RECOGER EN TIENDA":
                $id_status_order = (int)ConfigurationCore::get('SEUR2_STATUS_AVAILABLE_IN_STORE');
                break;
            case "ENTREGADO":
                $id_status_order = (int)ConfigurationCore::get('SEUR2_STATUS_DELIVERED');
                break;
            case "INCIDENCIA":
                $id_status_order = (int)ConfigurationCore::get('SEUR2_STATUS_INCIDENCE');
                break;
            case "DOCUMENTACION RECTIFICADA":
                $id_status_order = (int)ConfigurationCore::get('SEUR2_STATUS_IN_TRANSIT');
                break;

        }

        if($id_status_order) {
            $order = new Order((int)$this->id_order);

            if($order->current_state != (int)$id_status_order)
                $order->setCurrentState((int)$id_status_order);
        }
    }

}