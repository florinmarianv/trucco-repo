<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @version  Release: 0.4.4
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

if (!defined('SEUR_MODULE_NAME'))
	define('SEUR_MODULE_NAME', 'seur');

class SeurLib
{
	public static $baleares_states = array(
		'ES-IB' => 'Baleares'
	);

	public static $canarias_states = array(
		'ES-TF' => 'Santa Cruz de Tenerife', 'ES-GC' => 'Las Palmas'
	);

	public static $ceuta_melilla_states = array(
		'ES-CE' => 'Ceuta', 'ES-ML' => 'Melilla'
	);

	public static $spain_states = array(
		'ES-VI' => 'Alava',     'ES-AB' => 'Albacete',   'ES-A' => 'Alicante',       'ES-AL' => 'Almeria',          'ES-O' => 'Asturias',
		'ES-AV' => 'Avila',     'ES-BA' => 'Badajoz',    'ES-B' => 'Barcelona',      'ES-BU' => 'Burgos',           'ES-CC' => 'Caceres',
		'ES-CA' => 'Cadiz',     'ES-S' => 'Cantabria',   'ES-CS'  => 'Castellon',    'ES-CR' => 'Ciudad Real',      'ES-CO' => 'Cordoba',
		'ES-CU' => 'Cuenca',    'ES-GI' => 'Gerona',     'ES-GR' => 'Granada',       'ES-GU' => 'Guadalajara',      'ES-SS' => 'Guipuzcua',
		'ES-H' => 'Huelva',     'ES-HU' => 'Huesca',     'ES-J' => 'Jaen',           'ES-C'  => 'La Coruña',		'ES-L' => 'Lerida',
		'ES-LO' => 'La Rioja',  'ES-LE' => 'Leon',       'ES-LU' => 'Lugo',          'ES-MA' => 'Malaga',           'ES-M' => 'Madrid',
		'ES-MU' => 'Murcia',    'ES-NA' => 'Navarra',    'ES-OU' => 'Orense',        'ES-P' => 'Palencia',          'ES-PO' => 'Pontevedra',
		'ES-SA' => 'Salamanca', 'ES-SG' => 'Segovia',    'ES-SE' => 'Sevilla',       'ES-SO' => 'Soria',            'ES-T' => 'Tarragona',
		'ES-TE' => 'Teruel',    'ES-TO' => 'Toledo',     'ES-V' => 'Valencia',       'ES-VA' => 'Valladolid',       'ES-BI' => 'Vizcaya',
		'ES-ZA' => 'Zamora',    'ES-Z' => 'Zaragoza'
	);

	public static $street_types = array(
		'AUT' => 'AUTOVIA',          'AVD' => 'AVENIDA',  'CL' => 'CALLE',     'CRT' => 'CARRETERA',
		'CTO' => 'CENTRO COMERCIAL', 'EDF' => 'EDIFICIO', 'ENS' => 'ENSANCHE', 'GTA' => 'GLORIETA',
		'GRV' => 'GRAN VIA',         'PSO' => 'PASEO',    'PZA' => 'PLAZA',    'POL' => 'POLIGONO INDUSTRIAL',
		'RAM' => 'RAMBLA',           'RDA' => 'RONDA',    'ROT' => 'ROTONDA',  'TRV' => 'TRAVESIA',
		'URB' => 'URBANIZACION'
	);

	public static $seur_countries = array(
		'ES' => 'ESPAÑA', 'PT' => 'PORTUGAL'
	);

	public static $seur_zones = array(
		0 => 'Provincia',    1 => 'Peninsula',    2 => 'Portugal',
		3 => 'Baleares',     4 => 'Canarias',     5 => 'Ceuta/Melilla'
	);

	public static function displayErrors($error = null)
	{
		if (!empty($error))
		{
			if (version_compare(_PS_VERSION_, '1.5', '>='))
				echo '<div class="error"><p>'.$error.'</p></div>';
			else
				echo '<div class="error"><p><img src="../img/admin/warning.gif" border="0" alt="Error Icon" /> '.$error.'</p></div>'; // @TODO echo ??
		}
	}

/*
	public static function getConfiguration()
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT *
			FROM `'._DB_PREFIX_.'seur_configuration`
			WHERE `id_seur_configuration` = 1'
		);
	}

	public static function getConfigurationField($campo)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT '.pSQL($campo).'
			FROM `'._DB_PREFIX_.'seur_configuration`
			WHERE `id_seur_configuration` = 1'
		);
	}

	public static function setConfigurationField($campo, $valor)
	{
		return Db::getInstance()->Execute('
			UPDATE `'._DB_PREFIX_.'seur_configuration`
			SET `'.pSQL($campo).'`="'.(int)$valor.'"
			WHERE `id_seur_configuration` = 1'
		);
	}
*/
	public static function getOrderPos($id_cart)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT *
			FROM `'._DB_PREFIX_.'seur2_order_pos`
			WHERE `id_cart` = '.(int)$id_cart
		);
	}

	public static function getMerchantData($id_merchant = 1)
	{
	    if($id_merchant == 0)
            $id_merchant = 1;

		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT *
			FROM `'._DB_PREFIX_.'seur2_ccc`
			WHERE `id_seur_ccc` = '.(int)$id_merchant
		);
	}

	public static function getMerchantField($campo,$id_merchant = 1)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT '.pSQL($campo).'
			FROM `'._DB_PREFIX_.'seur2_ccc`
			WHERE `id_seur_ccc` = '.(int)$id_merchant
		);
	}

	public static function setMerchantField($campo, $valor)
	{
		return Db::getInstance()->Execute('
			UPDATE `'._DB_PREFIX_.'seur_merchant`
			SET `'.pSQL($campo).'`="'.pSQL($valor).'"
			WHERE `id_seur_datos` = 1'
		);
	}

	public static function isPricesConfigured()
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT COUNT( * )
			FROM  `'._DB_PREFIX_.'delivery`
			WHERE  `price` != 0
			AND  `id_carrier` IN (
				SELECT  `id_seur_carrier` AS `id`
				FROM  `'._DB_PREFIX_.'seur_history`
				WHERE  `active` =1
			)'
		);
	}

	public static function getSeurCarrier($id_carrier)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT sc.*,c.name
			FROM `'._DB_PREFIX_.'seur2_carrier` sc
			LEFT JOIN `'._DB_PREFIX_.'carrier` c ON c.id_reference=sc.carrier_reference AND c.active=1 AND c.deleted=0
			WHERE c.`id_carrier` = "'.pSQL($id_carrier).'"'
		);
	}

    public static function getSeurPOSCarrier()
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT sc.*,c.name
			FROM `'._DB_PREFIX_.'seur2_carrier` sc
			LEFT JOIN `'._DB_PREFIX_.'carrier` c ON c.id_reference=sc.carrier_reference AND c.active=1 AND c.deleted=0
			WHERE sc.shipping_type = 2');
    }


	public static function getSeurCarriers($active = true)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT sc.*, c.id_carrier
			FROM `'._DB_PREFIX_.'seur2_carrier` sc
			LEFT JOIN `'._DB_PREFIX_.'carrier` c ON c.id_reference=sc.carrier_reference AND c.deleted=0
			'.($active ? 'WHERE c.active = 1' : '')
		);
	}

	public static function getLastSeurCarriers()
	{
		Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('UPDATE `'._DB_PREFIX_.'seur_history` SET `active`= 0');

		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT `id_seur_carrier` AS `id`, `type`
			FROM `'._DB_PREFIX_.'seur_history`
			GROUP BY `type`'
		);
	}

	public static function updateSeurCarriers($new_carriers_seur)
	{
		$olds_carriers_seur = self::getSeurCarriers(false);

		$sql_new_carriers = 'INSERT IGNORE INTO `'._DB_PREFIX_.'seur_history` VALUES ';
		$sql_disable = 'UPDATE `'._DB_PREFIX_.'seur_history` SET `active` = 0 WHERE ';
		$sql_enable = 'UPDATE `'._DB_PREFIX_.'seur_history` SET `active` = 1 WHERE ';

		$enable_olds_carriers = false;

		foreach ($olds_carriers_seur as $old_carrier)
		{
			foreach ($new_carriers_seur as $key_new => $new_carrier_seur)
			{
				if (($new_carrier_seur['id'] == $old_carrier['id']) && ($new_carrier_seur['type'] == $old_carrier['type']))
				{
					if ($old_carrier['active'] == 0)
					{
						$sql_enable .= ' (`id_seur_carrier` = '.(int)$old_carrier['id'].' AND `type` = "'.pSQL($old_carrier['type']).'") OR ';
						$sql_disable .= '`type` ="'.pSQL($old_carrier['type']).'" OR ';
						$enable_olds_carriers = true;
					}
					unset($new_carriers_seur[$key_new]);
				}
			}
		}

		foreach ($new_carriers_seur as $new_carrier_seur)
		{
			$sql_new_carriers .= '('.(int)$new_carrier_seur['id'].',"'.pSQL($new_carrier_seur['type']).'",1),';
			$sql_disable .= '`type` ="'.pSQL($new_carrier_seur['type']).'" OR ';
		}

		$sql_disable = trim($sql_disable, 'OR ');
		$sql_disable .= ';';

		$sql_enable = trim($sql_enable, 'OR ');
		$sql_enable .= ';';

		if (!empty($new_carriers_seur))
		{
			Db::getInstance()->Execute($sql_disable);

			$sql_new_carriers = trim($sql_new_carriers, ',');
			$sql_new_carriers .= ';';
			Db::getInstance()->Execute($sql_new_carriers);
		}

		if ($enable_olds_carriers)
		{
			Db::getInstance()->Execute($sql_disable);
			Db::getInstance()->Execute($sql_enable);
		}
	}

    public static function isSeurOrder($id_order)
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT *
			FROM `'._DB_PREFIX_.'seur2_order`
			WHERE `id_order` ='.(int)$id_order
        );
    }

    public static function isSeurCarrier($id_carrier)
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT sc.*,c.name
			FROM `'._DB_PREFIX_.'seur2_carrier` sc
			LEFT JOIN `'._DB_PREFIX_.'carrier` c ON c.id_reference=sc.carrier_reference
			WHERE c.`id_carrier` = "'.pSQL($id_carrier).'"'
        );
    }


	public static function getSeurOrder($id_order)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT *
			FROM `'._DB_PREFIX_.'seur2_order`
			WHERE `id_order` ='.(int)$id_order
			);
	}

	public static function setSeurOrder($id_order, $ecb, $bultos, $peso, $labeled = false,$codfee = null, $id_address_delivery = 0, $total_paid = 0)
	{
		$exists = self::getSeurOrder($id_order);

        $id_order_carrier = Db::getInstance()->getValue('
			SELECT `id_order_carrier`
			FROM `'._DB_PREFIX_.'order_carrier`
			WHERE `id_order` = '.(int)$id_order);

        if ($id_order_carrier) {
            $order_carrier = new OrderCarrier($id_order_carrier);
            $order_carrier->tracking_number = $ecb;
        }

        // Si está configurado el cambio automático de estado al etiquetar, cambiamos de estado.
        if(Configuration::get('SEUR2_SENDED_ORDER') && $labeled)
        {
            $order = new OrderCore($id_order);

            $id_state = Configuration::get('PS_OS_SHIPPING');

            if($order->current_state != $id_state)
                $order->setCurrentState($id_state);
        }

		if ($exists) {
            $result = Db::getInstance()->Execute('
				UPDATE ' . _DB_PREFIX_ . 'seur2_order
				SET `ecb`="' . $ecb . '", `peso_bultos`=' . (float)$peso . ', `labeled`=' . (int)$labeled . ', `numero_bultos`=' . (int)$bultos . (($codfee != null) ? ',  `codfee`=' . (float)$codfee : '') . (($total_paid != 0) ? ' ,  `total_paid`=' . (float)$total_paid : '') . ', date_labeled="' . date('Y-m-d') . '"
				WHERE `id_order` =' . (int)$id_order
            );


        }
		else {
            $result = Db::getInstance()->Execute('
				INSERT INTO `' . _DB_PREFIX_ . 'seur2_order` (id_order, ecb, numero_bultos, peso_bultos, labeled, codfee, id_address_delivery,total_paid, date_labeled) 
				VALUES (' . (int)$id_order . ',"' . $ecb . '", ' . (int)$bultos . ',' . (float)$peso . ', 0, ' . (float)$codfee . ',' . (int)$id_address_delivery . ',' . (float)$total_paid . ',"' . date('Y-m-d') . '");'
            );
        }


        $payment_module = Db::getInstance()->getValue('
			SELECT `module`
			FROM `'._DB_PREFIX_.'orders`
			WHERE `id_order` = '.(int)$id_order);

        if($payment_module=='seurcashondelivery') {
            $result = Db::getInstance()->Execute('
				UPDATE ' . _DB_PREFIX_ . 'seur2_order
				SET `cashondelivery`=' . (float)$total_paid . '
				WHERE `id_order` =' . (int)$id_order
            );
        }

        return $result;
    }

	public static function getModulosPago()
	{
		$modules = Module::getModulesOnDisk();
		$paymentModules = array();

		foreach ($modules as $module)
		{
			if ($module->tab == 'payments_gateways')
			{
				if ($module->id)
				{
					if (!get_class($module) == 'SimpleXMLElement')
						$module->country = array();

					$countries = DB::getInstance()->ExecuteS('
						SELECT `id_country`
						FROM `'._DB_PREFIX_.'module_country`
						WHERE `id_module` = '.(int)$module->id
					);

					foreach ($countries as $country)
						$module->country[] = (int)$country['id_country'];

					if (!get_class($module) == 'SimpleXMLElement')
						$module->currency = array();

					$currencies = DB::getInstance()->ExecuteS('
						SELECT `id_currency`
						FROM `'._DB_PREFIX_.'module_currency`
						WHERE `id_module` = "'.(int)$module->id.'"
					');

					foreach ($currencies as $currency)
						$module->currency[] = (int)$currency['id_currency'];

					if (!get_class($module) == 'SimpleXMLElement')

						$module->group = array();
					$groups = DB::getInstance()->ExecuteS('
						SELECT `id_group`
						FROM `'._DB_PREFIX_.'module_group`
						WHERE `id_module` = "'.(int)$module->id.'"
					');

					foreach ($groups as $group)
						$module->group[] = (int)$group['id_group'];

				}
				else
				{
					$module->country = null;
					$module->currency = null;
					$module->group = null;
				}
				$paymentModules[] = $module;
			}
		}

		return $paymentModules;
	}

    public static function displayWarningSeur()
    {
        if (version_compare(_PS_VERSION_, '1.5', '<'))
            Context::getContext()->smarty->assign('ps_14', true);

        if (!version_compare(_PS_VERSION_, '1.6', '<'))
            Context::getContext()->smarty->assign('ps_16', true);
    }

    public static function isPrinted($id_order)
    {
        return DB::getInstance()->getValue('
			SELECT `labeled`
			FROM `'._DB_PREFIX_.'seur2_order`
			WHERE `id_order` = "'.(int)$id_order.'"
		');
    }

    public static function replaceAccentedChars($text)
    {
        if (version_compare(_PS_VERSION_, '1.4.5.1', '>='))
            return Tools::replaceAccentedChars($text);

        $patterns = array(
            /* Lowercase */
            /* a  */ '/[\x{00E0}\x{00E1}\x{00E2}\x{00E3}\x{00E4}\x{00E5}\x{0101}\x{0103}\x{0105}\x{0430}]/u',
            /* b  */ '/[\x{0431}]/u',
            /* c  */ '/[\x{00E7}\x{0107}\x{0109}\x{010D}\x{0446}]/u',
            /* d  */ '/[\x{010F}\x{0111}\x{0434}]/u',
            /* e  */ '/[\x{00E8}\x{00E9}\x{00EA}\x{00EB}\x{0113}\x{0115}\x{0117}\x{0119}\x{011B}\x{0435}\x{044D}]/u',
            /* f  */ '/[\x{0444}]/u',
            /* g  */ '/[\x{011F}\x{0121}\x{0123}\x{0433}\x{0491}]/u',
            /* h  */ '/[\x{0125}\x{0127}]/u',
            /* i  */ '/[\x{00EC}\x{00ED}\x{00EE}\x{00EF}\x{0129}\x{012B}\x{012D}\x{012F}\x{0131}\x{0438}\x{0456}]/u',
            /* j  */ '/[\x{0135}\x{0439}]/u',
            /* k  */ '/[\x{0137}\x{0138}\x{043A}]/u',
            /* l  */ '/[\x{013A}\x{013C}\x{013E}\x{0140}\x{0142}\x{043B}]/u',
            /* m  */ '/[\x{043C}]/u',
            /* n  */ '/[\x{00F1}\x{0144}\x{0146}\x{0148}\x{0149}\x{014B}\x{043D}]/u',
            /* o  */ '/[\x{00F2}\x{00F3}\x{00F4}\x{00F5}\x{00F6}\x{00F8}\x{014D}\x{014F}\x{0151}\x{043E}]/u',
            /* p  */ '/[\x{043F}]/u',
            /* r  */ '/[\x{0155}\x{0157}\x{0159}\x{0440}]/u',
            /* s  */ '/[\x{015B}\x{015D}\x{015F}\x{0161}\x{0441}]/u',
            /* ss */ '/[\x{00DF}]/u',
            /* t  */ '/[\x{0163}\x{0165}\x{0167}\x{0442}]/u',
            /* u  */ '/[\x{00F9}\x{00FA}\x{00FB}\x{00FC}\x{0169}\x{016B}\x{016D}\x{016F}\x{0171}\x{0173}\x{0443}]/u',
            /* v  */ '/[\x{0432}]/u',
            /* w  */ '/[\x{0175}]/u',
            /* y  */ '/[\x{00FF}\x{0177}\x{00FD}\x{044B}]/u',
            /* z  */ '/[\x{017A}\x{017C}\x{017E}\x{0437}]/u',
            /* ae */ '/[\x{00E6}]/u',
            /* ch */ '/[\x{0447}]/u',
            /* kh */ '/[\x{0445}]/u',
            /* oe */ '/[\x{0153}]/u',
            /* sh */ '/[\x{0448}]/u',
            /* shh*/ '/[\x{0449}]/u',
            /* ya */ '/[\x{044F}]/u',
            /* ye */ '/[\x{0454}]/u',
            /* yi */ '/[\x{0457}]/u',
            /* yo */ '/[\x{0451}]/u',
            /* yu */ '/[\x{044E}]/u',
            /* zh */ '/[\x{0436}]/u',

            /* Uppercase */
            /* A  */ '/[\x{0100}\x{0102}\x{0104}\x{00C0}\x{00C1}\x{00C2}\x{00C3}\x{00C4}\x{00C5}\x{0410}]/u',
            /* B  */ '/[\x{0411}]]/u',
            /* C  */ '/[\x{00C7}\x{0106}\x{0108}\x{010A}\x{010C}\x{0426}]/u',
            /* D  */ '/[\x{010E}\x{0110}\x{0414}]/u',
            /* E  */ '/[\x{00C8}\x{00C9}\x{00CA}\x{00CB}\x{0112}\x{0114}\x{0116}\x{0118}\x{011A}\x{0415}\x{042D}]/u',
            /* F  */ '/[\x{0424}]/u',
            /* G  */ '/[\x{011C}\x{011E}\x{0120}\x{0122}\x{0413}\x{0490}]/u',
            /* H  */ '/[\x{0124}\x{0126}]/u',
            /* I  */ '/[\x{0128}\x{012A}\x{012C}\x{012E}\x{0130}\x{0418}\x{0406}]/u',
            /* J  */ '/[\x{0134}\x{0419}]/u',
            /* K  */ '/[\x{0136}\x{041A}]/u',
            /* L  */ '/[\x{0139}\x{013B}\x{013D}\x{0139}\x{0141}\x{041B}]/u',
            /* M  */ '/[\x{041C}]/u',
            /* N  */ '/[\x{00D1}\x{0143}\x{0145}\x{0147}\x{014A}\x{041D}]/u',
            /* O  */ '/[\x{00D3}\x{014C}\x{014E}\x{0150}\x{041E}]/u',
            /* P  */ '/[\x{041F}]/u',
            /* R  */ '/[\x{0154}\x{0156}\x{0158}\x{0420}]/u',
            /* S  */ '/[\x{015A}\x{015C}\x{015E}\x{0160}\x{0421}]/u',
            /* T  */ '/[\x{0162}\x{0164}\x{0166}\x{0422}]/u',
            /* U  */ '/[\x{00D9}\x{00DA}\x{00DB}\x{00DC}\x{0168}\x{016A}\x{016C}\x{016E}\x{0170}\x{0172}\x{0423}]/u',
            /* V  */ '/[\x{0412}]/u',
            /* W  */ '/[\x{0174}]/u',
            /* Y  */ '/[\x{0176}\x{042B}]/u',
            /* Z  */ '/[\x{0179}\x{017B}\x{017D}\x{0417}]/u',
            /* AE */ '/[\x{00C6}]/u',
            /* CH */ '/[\x{0427}]/u',
            /* KH */ '/[\x{0425}]/u',
            /* OE */ '/[\x{0152}]/u',
            /* SH */ '/[\x{0428}]/u',
            /* SHH*/ '/[\x{0429}]/u',
            /* YA */ '/[\x{042F}]/u',
            /* YE */ '/[\x{0404}]/u',
            /* YI */ '/[\x{0407}]/u',
            /* YO */ '/[\x{0401}]/u',
            /* YU */ '/[\x{042E}]/u',
            /* ZH */ '/[\x{0416}]/u');

        // ö to oe
        // å to aa
        // ä to ae

        $replacements = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 'ss', 't', 'u', 'v', 'w', 'y', 'z', 'ae', 'ch',
            'kh', 'oe', 'sh', 'shh', 'ya', 'ye', 'yi', 'yo', 'yu', 'zh', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
            'P', 'R', 'S', 'T', 'U', 'V', 'W', 'Y', 'Z', 'AE', 'CH', 'KH', 'OE', 'SH', 'SHH', 'YA', 'YE', 'YI', 'YO', 'YU', 'ZH'
        );

        return preg_replace($patterns, $replacements, $text);
    }

    public static function setAsPrinted($id_order)
    {
        return DB::getInstance()->Execute('
			UPDATE `'._DB_PREFIX_.'seur2_order`
			SET `labeled` = 1
			WHERE `id_order` = "'.(int)$id_order.'"
		');
    }

    public static function getListCCC()
    {
	    return DB::getInstance()->ExecuteS('
			SELECT * FROM `'._DB_PREFIX_.'seur2_ccc`
	     ');
    }

    public static function isGeoLabel($id_seur_ccc)
    {
        $seur_ccc = new SeurCCC($id_seur_ccc);

        $geolabel = $seur_ccc->geolabel;

        if($geolabel == 1){
            return true;
        }else{
            return false;
        }
    }

    public static function  hasAnyGeoLabel(){

	    $is = DB::getInstance()->ExecuteS('
			SELECT * FROM `'._DB_PREFIX_.'seur2_ccc` WHERE  geolabel = 1
	     ');

	    if($is ){
            return true;
        }else{
            return false;
        }
    }

    public static function  cccAnyGeoLabel(){

        $is = DB::getInstance()->ExecuteS('
			SELECT * FROM `'._DB_PREFIX_.'seur2_ccc` WHERE  geolabel = 1
	     ');

        if($is ){
            $ccc = $is[0]['id_seur_ccc'];

        }

        return $ccc;
    }

    public static function  cccNoGeoLabel(){

        $is = DB::getInstance()->ExecuteS('
			SELECT * FROM `'._DB_PREFIX_.'seur2_ccc` WHERE  geolabel = 0
	     ');

        if($is ){
            $ccc = $is[0]['id_seur_ccc'];

        }

        return $ccc;
    }
}