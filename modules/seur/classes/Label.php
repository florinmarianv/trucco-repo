<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2015 PrestaShop SA
 * @version  Release: 0.4.4
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
    exit;

class SeurLabel
{
    public static function createLabels($id_order,$label_data, $merchant_data, $is_geolabel=false)
    {
        try {
            if (Validate::isFileName($label_data['pedido']))
                $label_name = $label_data['pedido'];
            else {
                $module_instance = Module::getInstanceByName('seur');
                return false;
            }

            $sc_options = array(
                'connection_timeout' => 30
            );

            $soap_client = new SoapClient(Configuration::get('SEUR2_URLWS_ET'), $sc_options);

            $notification = (Configuration::get('SEUR2_SETTINGS_NOTIFICATION')==1 ? Configuration::get('SEUR2_SETTINGS_NOTIFICATION_TYPE'):0);
            $advice_checkbox = Configuration::get('SEUR2_SETTINGS_ALERT');
            $distribution_checkbox = Configuration::get('SEUR2_SETTINGS_ALERT_TYPE');

            $seur_order = SeurOrder::getByOrder($id_order);


            $servicio = $seur_order->service;
            $producto = $seur_order->product;


            $seur_carrier = new SeurCarrier((int)$seur_order->id_seur_carrier);

            $mercancia = false;

            if ($seur_carrier->shipping_type == 3 && ($label_data['iso'] != 'ES' &&
                    $label_data['iso'] != 'PT' && $label_data['iso'] != 'AD'))
            {
                $mercancia = true;
            }

            $claveReembolso = '';
            $valorReembolso = '';


            if (isset($label_data['reembolso']) && ($label_data['iso'] == 'ES' || $label_data['iso'] == 'PT' || $label_data['iso'] == 'AD')) {
                $claveReembolso = 'F';
                $valorReembolso = (float)$label_data['reembolso'];
            }

            if (isset($label_data['cod_centro']) && ($label_data['iso'] == 'ES' || $label_data['iso'] == 'PT' || $label_data['iso'] == 'AD')) {
                $servicio = 1;
                $producto = 48;
            }

            $total_weight = $label_data['total_kilos'];
            $total_packages = $label_data['total_bultos'];

            if($total_weight == 0) $total_weight = 1;
            if($total_packages == 0 || $servicio==77) $total_packages = 1;

            $pesoBulto = $total_weight / $total_packages;


            if ($pesoBulto < 1)//1kg
            {
                $pesoBulto = 1;
                $total_weight = $total_packages;
            }

            $cont = 0;
            $xml = '<?xml version="1.0" encoding="ISO-8859-1"?><root><exp>';

            for ($i = 0; $i <= (float)$total_packages - 1; $i++) {
                $cont++;

                $xml .= '<bulto>
							<cit>' . $merchant_data['cit'] . '</cit>
							<nif>' . pSQL(Configuration::get('SEUR2_MERCHANT_NIF_DNI')) . '</nif>
							<ccc>' . $merchant_data['ccc'] . '</ccc>
							<servicio>' . pSQL($servicio) . '</servicio>
							<producto>' . pSQL($producto) . '</producto>';

                if ($mercancia)
                    $xml .= '<id_mercancia>400</id_mercancia>';

                $cod_Centro = "";

                if(isset($label_data['cod_centro']))
                    $cod_Centro = pSQL($label_data['cod_centro']);

                $xml .= '<cod_centro>'.$cod_Centro.'</cod_centro>
							<total_bultos>' . pSQL($total_packages) . '</total_bultos>
							<total_kilos>' . pSQL($total_weight) . '</total_kilos>
							<pesoBulto>' . pSQL($pesoBulto) . '</pesoBulto>
							<observaciones>' . pSQL($label_data['info_adicional']) . '</observaciones>
							<referencia_expedicion>' . pSQL($label_data['pedido']) . '</referencia_expedicion>
							<ref_bulto>' . pSQL($label_data['pedido'] . sprintf('%03d', (int)$i + 1)) . '</ref_bulto>
							<clavePortes>F</clavePortes>
							<clavePod></clavePod>
							<claveReembolso>' . pSQL($claveReembolso) . '</claveReembolso>
							<valorReembolso>' . pSQL($valorReembolso) . '</valorReembolso>
							<libroControl></libroControl>
							<nombre_consignatario>' . pSQL($label_data['name']) . '</nombre_consignatario>
							<direccion_consignatario>' . pSQL($label_data['direccion_consignatario']) . '</direccion_consignatario>
							<tipoVia_consignatario>CL</tipoVia_consignatario>
							<tNumVia_consignatario>N</tNumVia_consignatario>
							<numVia_consignatario></numVia_consignatario>
							<escalera_consignatario></escalera_consignatario>
							<piso_consignatario></piso_consignatario>
							<puerta_consignatario></puerta_consignatario>
							<poblacion_consignatario>' . pSQL($label_data['consignee_town']) . '</poblacion_consignatario>';

                if (!empty($label_data['codPostal_consignatario'])){
                    $cod_postal_orig = pSQL(trim($label_data['codPostal_consignatario']));
                    $cod_postal = str_replace(" ","",$cod_postal_orig);
                    $xml .= '<codPostal_consignatario>' . $cod_postal . '</codPostal_consignatario>';
                }

                $xml .= '   <pais_consignatario>' . pSQL($label_data['iso'])  . '</pais_consignatario>
							<codigo_pais_origen>' . pSQL($merchant_data['country']) . '</codigo_pais_origen>
							<email_consignatario>' . pSQL($label_data['email_consignatario']) . '</email_consignatario>
							<sms_consignatario>' . ((int)$notification ? pSQL($label_data['movil']) : '') . '</sms_consignatario>
							<test_sms>' . (((int)$notification==2 || (int)$notification==3) ? 'S' : 'N') . '</test_sms>
							<test_preaviso>' . ((int)$advice_checkbox ? 'S' : 'N') . '</test_preaviso>
							<test_reparto>' . ((int)$distribution_checkbox ? 'S' : 'N') . '</test_reparto>
							<test_email>' . (((int)$notification==1 || (int)$notification==3) ? 'S' : 'N') . '</test_email>
							<eci>N</eci>
							<et>N</et>
							<telefono_consignatario>' . pSQL($label_data['telefono_consignatario']) . '</telefono_consignatario>
							<atencion_de>' . pSQL($label_data['companyia']) . '</atencion_de>
						 </bulto>
						 ';
            }

            $xml .= '</exp></root>';

            $xml_name = (int)$merchant_data['franchise'] . '_' . (int)$merchant_data['cit'] . '_' . date('dmYHi') . '.xml';

            $make_pickup = false;
            $auto = false;
            $pickup_data = SeurPickup::getLastPickup($merchant_data['id_seur_ccc']);

            if (Configuration::get('SEUR2_SETTINGS_PICKUP') == 1) {
                $auto = true;
            }

            if (!empty($pickup_data)) {
                $datepickup = explode(' ', $pickup_data['date']);
                $datepickup = $datepickup[0];

                if (strtotime(date('Y-m-d')) != strtotime($datepickup))
                    $make_pickup = true;

            } else {
                $make_pickup = true;
            }


            if (Configuration::get('SEUR2_SETTINGS_PRINT_TYPE') == 1) {

                if($is_geolabel){

                    $in4 = 'GL';

                    $data = array(
                        'in0' => $merchant_data['ws_user'],
                        'in1' => $merchant_data['ws_password'],
                        'in2' => 'ZEBRA',
                        'in3' => 'LP2844-Z',
                        'in4' => $in4,
                        'in5' => $xml,
                        'in6' => $xml_name,
                        'in7' => pSQL(Configuration::get('SEUR2_MERCHANT_NIF_DNI')),
                        'in8' => pSQL($merchant_data['franchise']),
                        'in9' => '-1',
                        'in10' => 'prestashop',
                    );

                    $response = $soap_client->impresionIntegracionConECBWS($data);

                    if ($response->out == 'ERROR' || $response->out->mensaje != 'OK')
                        return false;
                    // @TODO check if must be translatable
                    else {
                        $ecb = $response->out->ECB->string;

                        if(!is_array($ecb))
                            $ecb = array($ecb);

                        $ecb = implode(" - ",$ecb);

                        SeurLib::setSeurOrder($id_order, $ecb, (float)$total_packages, (float)$total_weight, 1);

                        $txt = (string)$response->out->traza;

                        //$pdfDecoded = base64_decode($txt);
                        $pdfDecoded = $txt;

                        $curl = curl_init();

                        curl_setopt($curl, CURLOPT_URL, "http://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/");
                        curl_setopt($curl, CURLOPT_POST, TRUE);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $pdfDecoded);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept: application/pdf")); // omit this line to get PNG images back
                        $pdf = curl_exec($curl);

                        if (is_writable(_PS_MODULE_DIR_ . 'seur/files/deliveries_labels/'))
                            file_put_contents(_PS_MODULE_DIR_ . 'seur/files/deliveries_labels/' . $label_name . '.pdf', $pdf);


                        if ($make_pickup && $auto)
                            SeurPickup::createPickup();
                    }

                }else{

                    $data = array(
                        'in0' => $merchant_data['ws_user'],
                        'in1' => $merchant_data['ws_password'],
                        'in2' => $xml,
                        'in3' => $xml_name,
                        'in4' => pSQL(Configuration::get('SEUR2_MERCHANT_NIF_DNI')),
                        'in5' => pSQL($merchant_data['franchise']),
                        'in6' => '-1',
                        'in7' => 'prestashop',
                    );

                    $response = $soap_client->impresionIntegracionPDFConECBWS($data);

                    if ($response->out == 'ERROR') {

                        return false;

                    }

                    if ($response->out->mensaje != 'OK') {
                        echo($response->out->mensaje );
                        return false;
                    }
                    else {
                        $pdf = base64_decode($response->out->PDF);

                        if (is_writable(_PS_MODULE_DIR_ . 'seur/files/deliveries_labels/'))
                            file_put_contents(_PS_MODULE_DIR_ . 'seur/files/deliveries_labels/' . $label_name . '.pdf', $pdf);


                        $ecb = $response->out->ECB->string;

                        if(!is_array($ecb))
                            $ecb = array($ecb);

                        $ecb = implode("",$ecb);

                        SeurLib::setSeurOrder($id_order, $ecb, (float)$total_packages, (float)$total_weight, 1);


                        if ($make_pickup && $auto) {
                            SeurPickup::createPickup();
                        }
                    }
                }
            } elseif (Configuration::get('SEUR2_SETTINGS_PRINT_TYPE') == 2) {
                if($is_geolabel){
                    $in4 = 'GL';
                }else{
                    $in4 = '2C';
                }
                $data = array(
                    'in0' => $merchant_data['ws_user'],
                    'in1' => $merchant_data['ws_password'],
                    'in2' => 'ZEBRA',
                    'in3' => 'LP2844-Z',
                    'in4' => $in4,
                    'in5' => $xml,
                    'in6' => $xml_name,
                    'in7' => pSQL(Configuration::get('SEUR2_MERCHANT_NIF_DNI')),
                    'in8' => pSQL($merchant_data['franchise']),
                    'in9' => '-1',
                    'in10' => 'prestashop',
                );

                $response = $soap_client->impresionIntegracionConECBWS($data);

                if ($response->out == 'ERROR' || $response->out->mensaje != 'OK')
                    return false;
                // @TODO check if must be translatable
                else {
                    $ecb = $response->out->ECB->string;

                    if(!is_array($ecb))
                        $ecb = array($ecb);

                    $ecb = implode(" - ",$ecb);

                    SeurLib::setSeurOrder($id_order, $ecb, (float)$total_packages, (float)$total_weight, 1);

                    $label_name = $label_data['pedido'];

                    if (is_writable(_PS_MODULE_DIR_ . 'seur/files/deliveries_labels/'))
                        file_put_contents(_PS_MODULE_DIR_ . 'seur/files/deliveries_labels/' . pSQL($label_name) . '.txt', (string)$response->out->traza);

                    if ($make_pickup && $auto)
                        SeurPickup::createPickup();
                }
            }
        } catch (PrestaShopException $e) {
            print_r("Se ha producido una excepción de prestashop: <br/><br/>".$e);
            die();
            return false;
        } catch (SoapFault $e) {
            print_r("Se ha producido una excepción de Soap: <br/><br/>".$e);
            die();
            $module_instance = Module::getInstanceByName('seur');
            $module_instance->adminDisplayWarning($module_instance->l('Connection Error.'));
            return false;
        }

        return true;
    }

    public static function createLabelsGeoLabel($id_order,$label_data, $merchant_data)
    {

        $url_pruebas = "https://api.seur.com/geolabel/api/shipment/addShipment";
        $url_produccion = "https://api.seur.com/geolabel/api/shipment/addShipment";


        $notification = (Configuration::get('SEUR2_SETTINGS_NOTIFICATION')==1 ? Configuration::get('SEUR2_SETTINGS_NOTIFICATION_TYPE'):0);
        $advice_checkbox = Configuration::get('SEUR2_SETTINGS_ALERT');
        $distribution_checkbox = Configuration::get('SEUR2_SETTINGS_ALERT_TYPE');

        $seur_order = SeurOrder::getByOrder($id_order);

        $servicio = $seur_order->service;
        $producto = $seur_order->product;


        $seur_carrier = new SeurCarrier((int)$seur_order->id_seur_carrier);

        $mercancia = false;

        if ($seur_carrier->shipping_type == 3 && ($label_data['iso'] != 'ES' &&
                $label_data['iso'] != 'PT' && $label_data['iso'] != 'AD'))
        {
            $mercancia = true;
        }

        $claveReembolso = '';
        $valorReembolso = '';


        if (isset($label_data['reembolso']) && ($label_data['iso'] == 'ES' || $label_data['iso'] == 'PT' || $label_data['iso'] == 'AD')) {
            $claveReembolso = 'F';
            $valorReembolso = (float)$label_data['reembolso'];
        }

        if (isset($label_data['cod_centro']) && ($label_data['iso'] == 'ES' || $label_data['iso'] == 'PT' || $label_data['iso'] == 'AD')) {
            $servicio = 1;
            $producto = 48;
        }

        $total_weight = $label_data['total_kilos'];
        $total_packages = $label_data['total_bultos'];

        if($total_weight == 0) $total_weight = 1;
        if($total_packages == 0 || $servicio==77) $total_packages = 1;

        $pesoBulto = $total_weight / $total_packages;


        if ($pesoBulto < 1)//1kg
        {
            $pesoBulto = 1;
            $total_weight = $total_packages;
        }

        $name = $seur_order->firstname." ".$seur_order->lastname;

        //setup request to send json via POST
        $data = array(
            "customerBussinesUnit" => pSQL($merchant_data['franchise']),
            "customerCode" => pSQL($merchant_data['ccc']),
            "customerReference" => pSQL($label_data['pedido']),
            "service" => pSQL($servicio),
            "product" => pSQL($producto),
            "senderNIF" => pSQL(Configuration::get('SEUR2_MERCHANT_NIF_DNI')),
            "senderName" => "",
            "senderStreet" => pSQL($merchant_data['street_name']),
            "senderStreetType" => pSQL($merchant_data['street_type']),
            "sendetStreetNumType" => "N",
            "senderStreetNumber" => pSQL($merchant_data['street_number']),
            "senderDoorway" => "",
            "sendetFloor" => pSQL($merchant_data['floor']),
            "senderGate" => pSQL($merchant_data['door']),
            "senderStair" => "",
            "senderCity" => pSQL($merchant_data['town']),
            "senderPostcode" => pSQL($merchant_data['post_code']),
            "senderCountry" => pSQL($merchant_data['country']),
            "senderPhone" => pSQL($merchant_data['phone']),
            "consigneeName" => pSQL($name),
            "consigneeStreet" => pSQL($label_data['direccion_consignatario']),
            "consigneeStreetType" => "CL",
            "consigneeStreetNumType" => "N",
            "consigneeStreetNumber" => "",
            "consigneeDoorway" => "",
            "consigneeFloor" => "",
            "consgineeGate" => "",
            "consigneeStair"  => "",
            "consigneeCity" => pSQL($label_data['consignee_town']),
            "consigneePostcode"  => pSQL($label_data['codPostal_consignatario']),
            "consigneeCountry" => pSQL($label_data['iso']),
            "consigneePhone" =>  pSQL($label_data['telefono_consignatario']),
            "consigneeContact" => pSQL($label_data['companyia']),
            "chargesKey" => "F",
            "proofReceiptKey" => "",
            "cargoType" => "1",
            "refundKey" => pSQL($claveReembolso),
            "refundAmount" => pSQL($valorReembolso),
            "insuranceKey" => "",
            "insuranceAmount" => 0,
            "declaredValues" => 0,
            "declaredValueCurrency"  => "EUR",
            "recordBookCheck" => "",
            "exchangeKey"  => "",
            "saturdayCheck"  => "",
            "coments"  => pSQL($label_data['info_adicional']),
            "proposedDate"  => "",
            "notificationCheck" => ((int)$advice_checkbox ? 'S' : 'N'),
            "distributionCheck"  => ((int)$distribution_checkbox ? 'S' : 'N'),
            "comunicationTypeCheck"  => "3",
            "mobilePhone"  => ((int)$notification ? pSQL($label_data['movil']) : ''),
            "email"  => pSQL($label_data['email_consignatario']),
            "pudoID"  => "",
            "createShipment" => "Y",
            "collection" =>  "N",
            "createShipmentReturn" =>  "N",
            "printLabels" =>  "Y",
            "parcels" =>array (
                array(
                    "senderReference" => pSQL($label_data['pedido']),
                    "partnerReference" =>"",
                    "parcelNumber" => "",
                    "ecb" => "",
                    "parcelWeight" => pSQL($total_weight),
                    "parcelLength" => 0,
                    "parcelWidth" => 0,
                    "parcelHeight" => 0
                )
            )
        );


        $shipment = json_encode($data);

        $username = $merchant_data['ws_user'];
        $password = $merchant_data['ws_password'];

        $ch = curl_init($url_pruebas);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $shipment);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($shipment),
                'user: '. $username,
                'password: '.$password
            )
        );

        $response = curl_exec($ch);

        $response = json_decode($response);

        if ($response->status != 'OK' ){
            return false;
        } else {

            $label_name = $label_data['pedido'];

            if (Configuration::get('SEUR2_SETTINGS_PICKUP') == 1) {
                $auto = true;
            }

            if (!empty($pickup_data)) {
                $datepickup = explode(' ', $pickup_data['date']);
                $datepickup = $datepickup[0];

                if (strtotime(date('Y-m-d')) != strtotime($datepickup))
                    $make_pickup = true;

            } else {
                $make_pickup = true;
            }


            $ecb = $response->parcels[0]->ecb;

            if(!is_array($ecb))
                $ecb = array($ecb);

            $ecb = implode(" - ",$ecb);

            SeurLib::setSeurOrder($id_order, $ecb, (float)$total_packages, (float)$total_weight, 1);
            $txt = (string)$response->parcels[0]->label;

            if(Configuration::get('SEUR2_SETTINGS_PRINT_TYPE') == 1){

                $pdfDecoded = base64_decode($txt);

                $curl = curl_init();

                curl_setopt($curl, CURLOPT_URL, "http://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/");
                curl_setopt($curl, CURLOPT_POST, TRUE);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $pdfDecoded);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept: application/pdf")); // omit this line to get PNG images back
                $pdf = curl_exec($curl);

                if (is_writable(_PS_MODULE_DIR_ . 'seur/files/deliveries_labels/'))
                    file_put_contents(_PS_MODULE_DIR_ . 'seur/files/deliveries_labels/' . $label_name . '.pdf', $pdf);
            }else{

                $txt = base64_decode($txt);
                
                if (is_writable(_PS_MODULE_DIR_ . 'seur/files/deliveries_labels/'))
                    file_put_contents(_PS_MODULE_DIR_ . 'seur/files/deliveries_labels/' . pSQL($label_name) . '.txt', $txt);

            }

            if ($make_pickup && $auto)
                SeurPickup::createPickup();
        }

    }
}
