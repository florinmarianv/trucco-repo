﻿{*
	*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
	*
	* @author    Línea Gráfica E.C.E. S.L.
	* @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
	* @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
*}
<script type="text/javascript">
    {literal}
    var seur_ajaxcall_url = '{/literal}{$link->getAdminLink('AdminModules', true)|escape:'quotes':'UTF-8'}{literal}&configure=seur&module_name=seur&ajax=1';
    var urlmodule = '</literal>{$urlmodule}<literal>'
    {/literal}
</script>


<div id="panel-move">
    <div class="panel panel-seur">
        <div class='module_logo col-md-2 col-lg-1'>
        </div>
        <div class='seur_opcion col-md-10 col-lg-6'>
            <div class="seur_texto col-md-8">
                <span class="header">{l s='¿Quieres enviar este pedido con Seur?' mod='seur'}</span><br>
                <span>{l s='Pulsa el botón para convertir este envío con SEUR' mod='seur'}</span><br>
            </div>
            <div class="seur_boton pull-right">
                <button class="btn btn-default btn-disk pull-right" id="seur_new_order"
                        name="seur_shipping_order_edit" type="button" value="1">
                    {l s='Send by SEUR' mod='seur'}
                </button>
            </div>
        </div>
        <div class='seur_form col-md-10 col-lg-6'>
            {if count($carriers)}
            <form action="{$urlmodule}" method="post">
            <div class="seur_select pull-left">
                <select name="seur_carrier" id="seur_carrier" >
                    <option value="">{l s='Selecciona un transportista' mod='seur'}</option>
                    {foreach from=$carriers item=carrier}
                        <option value="{$carrier.id_seur_carrier}">{$carrier.name}</option>
                    {/foreach}
                </select>
            </div>
            <div class="seur_boton">
                <button type="submit" class="btn btn-default btn-disk pull-right" id="seur_confirm_carrier"
                        name="seur_shipping_order_edit" type="button" value="1">
                    {l s='Confirm' mod='seur'}
                </button>
            </div>
            </form>
            {else}
                <div class="seur_texto pull-left">
                    <span class="header">{l s='No ha dado de alta transportistas de Seur' mod='seur'}</span><br>
                    <span>{l s='Acceda a la configuración de Transportistas SEUR para darlos de alta' mod='seur'}</span><br>
                </div>
            {/if}
        </div>
        <div style='clear:both; height:0; margin: 0; padding:0'></div>
    </div>
</div>
