<?php
/*
	*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
	*
	* @author    Línea Gráfica E.C.E. S.L.
	* @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
	* @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
*/

require_once(_PS_MODULE_DIR_ . DIRECTORY_SEPARATOR . 'seur' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'SeurOrder.php');

class AdminSeurShippingController extends ModuleAdminController
{

    public function __construct()
    {

        $module = Module::getInstanceByName('seur');

        $this->addJQuery();
        $this->addJS($module->getPath() . 'views/js/seurController.js');

        $this->bootstrap = true;
        $this->name = 'AdminSeurShipping';
        $this->table = 'seur2_order';
        $this->identifier = "id_seur_order";
        $this->_defaultOrderWay = "DESC";
        $this->className = 'SeurOrder';
        $this->lang = false;
        $this->module = $module;
        $this->addRowAction('view');
        $this->addRowAction('delete');
        $this->explicitSelect = true;
        $this->allow_export = true;
        $this->deleted = false;
        $this->context = Context::getContext();
        $this->show_toolbar = false;
        $this->page_header_toolbar_btn = array();

        $this->bulk_actions = array();

/*            'print_labels' => array(
                'text' => $this->l('Print labels selected shiping'),
                'icon' => 'icon-barcode',
            ),
            'manifest' => array(
                'text' => $this->l('Generate manifest selected shiping'),
                'icon' => 'icon-file-o',
            )
        );
*/

        $this->fields_list = array(
            'id_seur_order' => array(
                'title' => $this->module->l('ID seur order'),
                'align' => 'left',
                'orderby' => false,
                'search' => false,
                'class' => 'hidden',
            ),
            'id_order' => array(
                'title' => $this->module->l('ID order'),
                'align' => 'left',
                'orderby' => false,
                'search' => false,
                'filter_key' => 'a!id_order',
                'class' => 'hidden',
            ),
            'order_date' => array(
                'title' => $this->module->l('Date Order'),
                'align' => 'left',
                'orderby' => true,
                'type' => 'datetime',
                'search' => true,
                'filter_key' => 'o!date_add'
            ),
            'order_reference' => array(
                'title' => $this->module->l('Reference'),
                'align' => 'left',
                'orderby' => true,
                'search' => true,
            ),
            'firstname' => array(
                'title' => $this->module->l('Firstname'),
                'align' => 'left',
                'orderby' => true,
                'search' => true,
            ),
            'lastname' => array(
                'title' => $this->module->l('Lastname'),
                'align' => 'left',
                'orderby' => true,
                'search' => true,
            ),
            'ccc' => array(
                'title' => $this->module->l('CCC'),
                'align' => 'left',
                'orderby' => true,
                'search' => true,
                'filter_key' => 'sc!ccc'
            ),
            'address1' => array(
                'title' => $this->module->l('Address'),
                'align' => 'left',
                'orderby' => true,
                'search' => true,
            ),
            'postcode' => array(
                'title' => $this->module->l('Postal code'),
                'align' => 'left',
                'orderby' => true,
                'search' => true,
            ),
            'city' => array(
                'title' => $this->module->l('City'),
                'align' => 'left',
                'orderby' => true,
                'search' => true,
            ),
            'state' => array(
                'title' => $this->module->l('State'),
                'align' => 'left',
                'orderby' => true,
                'filter_key' => 'st!name',
                'search' => true,
            ),
            'country' => array(
                'title' => $this->module->l('Country'),
                'align' => 'left',
                'orderby' => true,
                'filter_key' => 'c!name',
                'search' => true,
            ),
            'order_status_name' => array(
                'title' => $this->module->l('Status'),
                'align' => 'left',
                'orderby' => true,
                'search' => true,
                'filter_key' => 's!name'
            ),
            'labeled' => array(
                'title' => $this->module->l('Labeled'),
                'align' => 'left',
                'orderby' => true,
                'type' => 'bool',
                'active' => 'status',
                'filter_key' => 'a!labeled'

            ),
            'manifested' => array(
                'title' => $this->module->l('Manifested'),
                'align' => 'left',
                'orderby' => true,
                'type' => 'bool',
                'active' => 'status',
                'filter_key' => 'a!manifested'
            ),
            'date_labeled' => array(
                'title' => $this->module->l('Date labeled'),
                'align' => 'left',
                'orderby' => true,
                'type' => 'date',
            ),
        );

        $this->context->smarty->assign('controlador', 'AdminShippingReturns');


        $this->_join .= ' 			
            LEFT JOIN `' . _DB_PREFIX_ . 'orders` o ON a.id_order = o.id_order
			LEFT JOIN `' . _DB_PREFIX_ . 'order_state_lang` s ON s.id_order_state = o.current_state AND s.id_lang=' . (int)Context::getContext()->language->id . '
			LEFT JOIN `' . _DB_PREFIX_ . 'country_lang` c ON c.id_country = a.id_country AND c.id_lang=' . (int)Context::getContext()->language->id . '
			LEFT JOIN `' . _DB_PREFIX_ . 'state` st ON st.id_state = a.id_state
            LEFT JOIN `' . _DB_PREFIX_ . 'seur2_ccc` sc ON sc.id_seur_ccc = a.id_seur_ccc';

        $this->_select .= 'a.firstname,a.lastname, IF(a.date_labeled = \'0000-00-00 00:00:00\', NULL, a.date_labeled) as date_labeled, s.name, o.date_add, o.reference as order_reference, c.name as country, st.name as state,';

        AdminController::__construct();
    }

    public function renderView(){
        $seurOrder = new SeurOrder(Tools::getValue('id_seur_order'));

        Tools::redirectAdmin(Context::getContext()->link->getAdminLink('AdminOrders',Tools::getAdminTokenLite('AdminOrders')).'&vieworder&id_order='.$seurOrder->id_order);
    }

    public function renderList(){

        if (count($this->module->errorConfigure())) {
            Tools::redirectAdmin('index.php?controller=adminmodules&configure=seur&token=' . Tools::getAdminTokenLite('AdminModules') . '&module_name=seur&settings=1');
            die();
        }

        if ((int)Tools::getValue('AddNewOrder')){
            $this->addOrder((int)Tools::getValue('AddNewOrder'),(int)Tools::getValue('seur_carrier'));
        }

        if (Tools::getValue('action') == "print_label") {
            $this->printLabel((int)Tools::getValue('id_order'));
            die();
        }

        if (Tools::getValue('action') == "print_labels") {
            $this->printLabelsTxt(Tools::getValue('id_orders'));
            die();
        }

        if (Tools::getValue('action') == "edit_order") {
            $this->editOrder((int)Tools::getValue('id_order'));
            $url = Context::getContext()->link->getAdminLink('AdminOrders');
            Tools::redirectAdmin($url . '&id_order='.(int)Tools::getValue('id_order').'&vieworder');
            die();
        }

        if (Tools::getValue('massive_action') == "print_labels") {
            if(version_compare(_PS_VERSION_, '1.6', '<'))
                $orders = Tools::getValue('seur2_orderBox');

            else
                $orders = Tools::getValue('shippingBox');

            $print_labels = $this->printLabels($orders);
        }

        if (Tools::getValue('massive_action') == "manifest") {
/*            if(version_compare(_PS_VERSION_, '1.7', '<'))
                $orders = Tools::getValue('seur2_orderBox');

            else
*/
            $orders = Tools::getValue('shippingBox');

            $manifest = $this->manifest($orders);
        }

        $this->context->smarty->assign(
            array(
                'url_module' => $this->context->link->getAdminLink('AdminModules', true) . "&configure=seur&module_name=seur",
                'url_controller_shipping' => $this->context->link->getAdminLink('AdminSeurShipping', true),
                'url_controller_collecting' => $this->context->link->getAdminLink('AdminSeurCollecting', true),
                'url_controller_tracking' => $this->context->link->getAdminLink('AdminSeurTracking', true),
                'url_controller_returns' => $this->context->link->getAdminLink('AdminSeurReturns', true),
                'img_path' => $this->module->getPath() . 'views/img/',
                'module_path' => 'index.php?controller=AdminModules&configure=' . $this->module->name . '&token=' . Tools::getAdminToken("AdminModules" . (int)(Tab::getIdFromClassName("AdminModules")) . (int)$this->context->cookie->id_employee),
            ));

        $selecttab = "shipping";
        $this->context->smarty->assign(
            array('tabSelect' => $selecttab)
        );


        $smarty = $this->context->smarty;
        $html = "";

        if(isset($print_labels) && count($print_labels))
        {
            $this->context->smarty->assign(
                array('print_labels' => $print_labels)
            );

            if(Configuration::get('SEUR2_SETTINGS_PRINT_TYPE')==2) {
                $html .= $smarty->fetch(_PS_MODULE_DIR_ . 'seur/views/templates/admin/print_labels_txt.tpl');
            }
            else{
                $html .= $smarty->fetch(_PS_MODULE_DIR_ . 'seur/views/templates/admin/print_labels.tpl');
            }
        }

        if(isset($manifest))
        {
            $this->context->smarty->assign(
                array('manifest' => $manifest)
            );

            $html .= $smarty->fetch(_PS_MODULE_DIR_ . 'seur/views/templates/admin/manifest.tpl');;
        }



        $html .= $smarty->fetch(_PS_MODULE_DIR_ . 'seur/views/templates/admin/header.tpl');;
        $html .= $smarty->fetch(_PS_MODULE_DIR_ . 'seur/views/templates/admin/tabs.tpl');;
        $html .= parent::renderList();
        $html .= $smarty->fetch(_PS_MODULE_DIR_ . 'seur/views/templates/admin/massives.tpl');;

        return $html;
    }

    public function printLabels($id_orders)
    {

        $print_labels = array();

        if(!isset($id_orders) || !is_array($id_orders))
            $id_orders = array();

        foreach ($id_orders as $id_seur_order) {
            $success = (int)$this->createLabel($id_seur_order);

            if($success){
                $print_labels[] = $success;
            }
        }

        return $print_labels;
    }

    public function createLabel($id_seur_order)
    {


        $seur_order = new SeurOrder($id_seur_order);

        if($seur_order->labeled)
            return (int)$id_seur_order;

        $id_order = $seur_order->id_order;

        $versionSpecialClass = '';
        if (!file_exists(_PS_MODULE_DIR_ . 'seur/img/logonew_32.png') && file_exists(_PS_MODULE_DIR_ . 'seur/img/logonew.png'))
            ImageManager::resize(_PS_MODULE_DIR_ . 'seur/img/logonew.png', _PS_MODULE_DIR_ . 'seur/img/logonew_32.png', 32, 32, 'png');
        if (version_compare(_PS_VERSION_, '1.5', '<'))
            $versionSpecialClass = 'ver14';
        SeurLib::displayWarningSeur();

        if ($this->module->isConfigured()) {

            $cookie = $this->context->cookie;
            $token = Tools::getValue('token');
            $back = Tools::safeOutput($_SERVER['REQUEST_URI']);
            $seur_carriers = SeurLib::getSeurCarriers(false);
            $ids_seur_carriers = array();

            foreach ($seur_carriers as $value) {
                $ids_seur_carriers[] = (int)$value['carrier_reference'];
            }
            $order = new Order((int)$id_order);

            $carrierAssign = new Carrier($order->id_carrier);
            $address_saved = DB::getInstance()->getValue('SELECT `id_address_delivery` FROM `' . _DB_PREFIX_ . 'seur2_order`	WHERE `id_order` = "' . (int)$order->id . '"');
//            $pickup_carrier = DB::getInstance()->getValue('SELECT `id_seur_carrier`	FROM `'._DB_PREFIX_.'seur2_Carrier` WHERE `type` = "SEP" AND `active` = 1');
//            if ($address_saved === '0' && $pickup_carrier === $order->id_carrier)
//                $this->context->smarty->assign('pickup_point_warning', true);

            if (!Validate::isLoadedObject($order))
                return false;
            $delivery_price = $order_weigth = 0;
            $products = $order->getProductsDetail();

            foreach ($products as $product) {
                $order_weigth += (float)$product['product_weight'] * (float)$product['product_quantity'];
            }

            $order_weigth = ($order_weigth < 1.0 ? 1.0 : (float)$order_weigth);
            $customer = new Customer((int)$order->id_customer);
            $iso_country = Country::getIsoById((int)$seur_order->id_country);
            if ($iso_country == 'PT') {
                $post_code = explode(' ', $seur_order->postcode);
                $post_code = $post_code[0];
            } else
                $post_code = $seur_order->postcode;

            $carrier = new Carrier((int)$order->id_carrier);

            if (in_array((int)$carrier->id_reference, $ids_seur_carriers)) {

                if (!SeurLib::getSeurOrder((int)$order->id))
                    SeurLib::setSeurOrder((int)$order->id, '',1, $order_weigth, null, $this->calculateCartAmount(new Cart($order->id_cart)));
                elseif (Tools::getValue('numBultos') && Tools::getValue('pesoBultos'))
                    SeurLib::setSeurOrder((int)$order->id, '', (int)Tools::getValue('numBultos'), str_replace(',', '.', Tools::getValue('pesoBultos')), null);
                $order_data = SeurLib::getSeurOrder((int)$order->id);
                $response_post_code = SeurTown::getTowns($post_code);
                $order_weigth = ((float)$order_weigth != $order_data['peso_bultos'] ? (float)$order_data['peso_bultos'] : (float)$order_weigth);
                $order_weigth = ($order_weigth < 1.0 ? 1.0 : (float)$order_weigth);


                if (is_object($response_post_code)) {
                    $towns = array();
                    $num = (int)$response_post_code->attributes()->NUM[0];

                    for ($i = 1; $i <= $num; $i++) {
                        $name = 'REG' . $i;
                        $towns[] = utf8_decode((string)$response_post_code->$name->NOM_POBLACION);
                    }
                }
                $name = $seur_order->firstname . ' ' . $seur_order->lastname;
                $direccion = $seur_order->address1 . ' ' . $seur_order->address2;
                $newcountry = new Country((int)$seur_order->id_country, (int)$cookie->id_lang);
                $iso_merchant = SeurLib::getMerchantField('country');
                $rate_data = array(
                    'town' => $seur_order->city,
                    'peso' => (float)$order_weigth,
                    'post_code' => $post_code,
                    'bultos' => $order_data['numero_bultos'],
                    'ccc' => SeurLib::getMerchantField('ccc'),
                    'franchise' => SeurLib::getMerchantField('franchise'),
                    'iso' => $newcountry->iso_code,
                    'iso_merchant' => $iso_merchant,
                    'id_employee' => $cookie->id_employee,
                    'token' => Tools::getAdminTokenLite('AdminOrders'),
                    'back' => $back,
                    'product' => $seur_order->product,
                    'service' => $seur_order->service
                );


                $order_messages_str = '';
                $info_adicional_str = $seur_order->other;
                $order_messages = Message::getMessagesByOrderId((int)$id_order);
                if (is_array($order_messages)) {
                    foreach ($order_messages as $order_messag_tmp)
                        $order_messages_str .= "\n" . $order_messag_tmp['message'];

                    if (substr_count($order_messages_str, "\n") > 5)
                        $order_messages_str = str_replace(array("\r", "\n"), ' | ', $order_messages_str);

                    if (Tools::strlen($order_messages_str) > 250)
                        $order_messages_str = Tools::substr($order_messages_str, 0, 247) . '...';

                    $order_messages_str = trim($order_messages_str);
                }
                if (!empty($order_messages_str)) {
                    $info_adicional_str = $order_messages_str;
                }


                $label_data = array(
                    'pedido' => $order->reference,
                    'total_bultos' => $order_data['numero_bultos'],
                    'total_kilos' => (float)$order_weigth,
                    'direccion_consignatario' => $direccion,
                    'consignee_town' => $seur_order->city,
                    'codPostal_consignatario' => $post_code,
                    'telefono_consignatario' => (!empty($seur_order->phone) ? $seur_order->phone : $seur_order->phone_mobile),
                    'movil' => (!empty($seur_order->phone_mobile) ? $seur_order->phone_mobile : $seur_order->phone),
                    'name' => $name,
                    'companyia' => (!empty($seur_order->company) ? $seur_order->company : ''),
                    'email_consignatario' => Validate::isLoadedObject($customer) ? $customer->email : '',
                    'dni' => $seur_order->dni,
                    'info_adicional' => $info_adicional_str,
                    'country' => $newcountry->name,
                    'iso' => $newcountry->iso_code,
                    'iso_merchant' => $iso_merchant,
                    'admin_dir' => utf8_encode(_PS_ADMIN_DIR_),
                    'id_employee' => $cookie->id_employee,
                    'token' => Tools::getAdminTokenLite('AdminOrders'),
                    'back' => $back
                );

                if (strcmp($order->module, 'seurcashondelivery') == 0) {
                    $rate_data['reembolso'] = (float)$order_data['total_paid'];
                    $label_data['reembolso'] = (float)$order_data['total_paid'];
                    $label_data['clave_reembolso'] = "F";
                    $label_data['valor_reembolso'] = (float)$order_data['total_paid'];
                }
                else{
                    $label_data['clave_reembolso'] = "";
                    $label_data['valor_reembolso'] = "0";
                }


                /* COMPROBAMOS SI ES UN TRANSPORTISTA DE RECOGIDA EN PUNTO DE VENTA Y REESCRIBIMOS*/

                $servicio = $seur_order->service;
                $producto = $seur_order->product;


                $datospos = SeurLib::getOrderPos((int)$order->id_cart);

                if (!empty($datospos) && $datospos && $servicio==1 && $producto==48)
                {
                    $label_data = array(
                        'pedido' => $order->reference,
                        'total_bultos' => $label_data['total_bultos'],
                        'total_kilos' => (float)$label_data['total_kilos'],
                        'direccion_consignatario' => $direccion,
                        'consignee_town' => $datospos['city'],
                        'codPostal_consignatario' => $datospos['postal_code'],
                        'telefono_consignatario' => (!empty($seur_order->phone) ? $seur_order->phone : $seur_order->phone_mobile),
                        'movil' => (!empty($seur_order->phone_mobile) ? $seur_order->phone_mobile : $seur_order->phone),
                        'name' => $name,
                        'companyia' => $datospos['company'],
                        'email_consignatario' => Validate::isLoadedObject($customer) ? $customer->email : '',
                        'dni' => $seur_order->dni,
                        'info_adicional' => $info_adicional_str,
                        'country' => $newcountry->name,
                        'iso' => $newcountry->iso_code,
                        'cod_centro' => $datospos['id_seur_pos'],
                        'iso_merchant' => $iso_merchant
                    );
                    $rate_data['cod_centro'] = $datospos['id_seur_pos'];
                }

                $rate = 0;
                if ($iso_country == 'ES' || $iso_country == 'PT' || $iso_country == 'AD') {
                    $xml = SeurRate::getPrivateRate($rate_data);

                    if (is_object($xml))
                        foreach ($xml as $tarifa) {
                            $delivery_price += (float)$tarifa->VALOR;
                            if ($tarifa->COD_CONCEPTO_IMP == 70)
                                $rate = $tarifa->VALOR;
                        }
                }

                $id_seur_ccc = $seur_order->id_seur_ccc;

                $merchant_data = SeurLib::getMerchantData((int)$id_seur_ccc);

                if ((int) SeurLib::isPrinted((int)$order->id)) {
                    $success = true;
                }
                else{
                    if( Configuration::get('SEUR2_SETTINGS_PRINT_TYPE') == 2 && SeurLib::isGeoLabel($id_seur_ccc) ){
                    //la llamada para que los destinos peninusulares (ES, PT, A) llamen al createLabels original pero cambiando el parametro in4 por GL
                        if ($iso_country == 'ES' || $iso_country == 'PT' || $iso_country == 'AD') {
                            $success = SeurLabel::createLabels((int)$order->id, $label_data, $merchant_data, SeurLib::hasAnyGeoLabel());
                        }else{
                            $success = SeurLabel::createLabelsGeoLabel((int)$order->id, $label_data, $merchant_data);
                        }
                    }else{
                        if( SeurLib::isGeoLabel($id_seur_ccc)){
                            if ($iso_country == 'ES' || $iso_country == 'PT' || $iso_country == 'AD') {
                                $success = SeurLabel::createLabels((int)$order->id, $label_data, $merchant_data, SeurLib::hasAnyGeoLabel());
                            }else{
                                $success = SeurLabel::createLabelsGeoLabel((int)$order->id, $label_data, $merchant_data);
                            }
                        }else {
                            $success = SeurLabel::createLabels((int)$order->id, $label_data, $merchant_data, false);
                        }
                    }
                }
                if ($success === true) {
                    if (!SeurLib::setAsPrinted((int)$order->id))
                        $this->context->smarty->assign('error', $this->l('Could not set printed value for this order'));
                } else {
                    $this->context->smarty->assign('error', $this->l('Could not set printed value for this order'));
                    return false;
                }

                $seur_carriers = SeurLib::getSeurCarriers(false);
                $pickup = SeurPickup::getLastPickup($id_seur_ccc);
                if (!empty($pickup)) {
                    $pickup_date = explode(' ', $pickup['date']);
                    $pickup_date = $pickup_date[0];
                }

                $address_error = 0;
                if (!empty($towns) && !in_array(mb_strtoupper(SeurLib::replaceAccentedChars($seur_order->city), 'UTF-8'), $towns))
                    $address_error = 1;
                $pickup_s = 0;
                if ($pickup && strtotime(date('Y-m-d')) >= strtotime($pickup_date))
                    $pickup_s = 1;


                /* Consultar estado */

                $state = SeurExpedition::getExpeditions(array('reference_number' => $order->reference));
                $is_empty_state = false;
                $xml_s = false;
                if (empty($state->out))
                    $is_empty_state = true;
                else {
                    $string_xml = htmlspecialchars_decode($state->out);
                    $string_xml = str_replace('&', '&amp; ', $string_xml);
                    $xml_s = simplexml_load_string($string_xml);

                    if (!$xml_s->EXPEDICION)
                        $is_empty_state = true;
                }


                $rate_data_ajax = Tools::jsonEncode($rate_data);
                $path = '../modules/seur/js/';
                $file = (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . __PS_BASE_URI__ . 'modules/seur/files/deliveries_labels/' . sprintf('%06d', (int)$order->id) . '.txt';
                $filePath = _PS_MODULE_DIR_ . 'seur/files/deliveries_labels/' . sprintf('%06d', (int)$order->id) . '.txt';
                $label_data['file'] = $file;
                $this->context->smarty->assign(array(
                    'path' => $this->module->path,
                    'request_uri' => $_SERVER['REQUEST_URI'],
                    'module_instance' => $this,
                    'address_error' => $address_error,
                    'address_error_message' => $this->l('Addressess error, please check the customer address.'),
                    'pickup_s' => $pickup_s,
                    'pickup' => $pickup,
                    'isEmptyState' => $is_empty_state,
                    'xml' => $xml_s,
                    'order_data' => $order_data,
                    'iso_country' => $iso_country,
                    'order_weigth' => $order_weigth,
                    'delivery_price' => $delivery_price,
                    'delivery_rate' => $rate,
                    'delivery_price_tax_excl' => ($delivery_price - $rate),
                    'rate_data_ajax' => $rate_data_ajax,
                    'js_path' => $path,
                    'token' => $token,
                    'order' => $order,
                    'label_data' => $label_data,
                    'fileExists' => file_exists($filePath),
                    'file' => $file,
                    'datospos' => $datospos,
                    'versionSpecialClass' => $versionSpecialClass,
                    'configured' => (int)Configuration::get('SEUR_Configured'),
                    'printed' => (bool)(SeurLib::isPrinted((int)$order->id))
                ));
                $module_instance = Module::getInstanceByName('seur');
                $text = $module_instance->l('Label')." ".$order->reference." ".$module_instance->l('generated');
//                $this->adminDisplayInformation($text);

                return (int)$id_seur_order;
            }
            else{
                $module_instance = Module::getInstanceByName('seur');
                if(!in_array((int)$carrierAssign->id_reference, $ids_seur_carriers)){
                    $text = $module_instance->l('Label')." ".$order->reference." ".$module_instance->l('don\'t generated: ');
                    $text .= $module_instance->l('Carrier not Seur.');
//                    $this->adminDisplayWarning($text);
                }

            }
        }
//        }
    }

    public function manifest($id_orders)
    {

        if(!isset($id_orders) || !is_array($id_orders))
            $id_orders = array();

        $manifest = $this->generateManifest($id_orders);


        return $manifest;
    }

    private function editOrder($id_order)
    {

        $num_bultos     = (int)Tools::getvalue(num_bultos);
        $peso           = (float)Tools::getvalue(peso);
        $firstname      = Tools::getvalue(firstname);
        $lastname       = Tools::getvalue(lastname);
        $phone          = Tools::getvalue(phone);
        $phone_mobile   = Tools::getvalue(phone_mobile);
        $dni            = Tools::getvalue(dni);
        $other          = Tools::getvalue(other);
        $address1       = Tools::getvalue(address1);
        $address2       = Tools::getvalue(address2);
        $postcode       = Tools::getvalue(postcode);
        $city           = Tools::getvalue(city);
        $id_country     = (int)Tools::getvalue(id_country);
        $id_state       = (int)Tools::getvalue(id_state);
        $id_seur_ccc    = (int)Tools::getvalue('id_seur_ccc');


        $seur_order = SeurOrder::getByOrder($id_order);
        $seur_order->numero_bultos  = $num_bultos;
        $seur_order->peso_bultos    = $peso;
        $seur_order->firstname      = pSQL($firstname);
        $seur_order->lastname       = pSQL($lastname);
        $seur_order->phone          = pSQL($phone);
        $seur_order->phone_mobile   = pSQL($phone_mobile);
        $seur_order->dni            = pSQL($dni);
        $seur_order->other          = pSQL($other);
        $seur_order->address1       = pSQL($address1);
        $seur_order->address2       = pSQL($address2);
        $seur_order->postcode       = pSQL($postcode);
        $seur_order->city           = pSQL($city);
        $seur_order->id_country     = (int)$id_country;
        $seur_order->id_state       = (int)$id_state;
        $seur_order->id_seur_ccc    = (int)$id_seur_ccc;

        $seur_order->save();

    }

    private function addOrder($id_order,$id_seur_carrier){

        $order = new OrderCore((int)$id_order);

        $seur_order = new SeurOrder();
        $seur_order->id_order = (int)$id_order;
        $seur_order->id_seur_ccc = 1;
        $seur_order->id_seur_carrier = (int)$id_seur_carrier;

        $address_delivery = new AddressCore($order->id_address_delivery);

        $seur_carrier = new SeurCarrier((int)$id_seur_carrier);

        $peso = $order->getTotalWeight();

        $seur_order->numero_bultos = 1;
        $seur_order->peso_bultos = $peso?$peso:1;
        $seur_order->firstname = $address_delivery->firstname;
        $seur_order->lastname = $address_delivery->lastname;
        $seur_order->address1 = $address_delivery->address1;
        $seur_order->address2 = $address_delivery->address2;
        $seur_order->postcode = $address_delivery->postcode;
        $seur_order->phone = $address_delivery->phone;
        $seur_order->phone_mobile = $address_delivery->phone_mobile;
        $seur_order->city = $address_delivery->city;
        $seur_order->id_state = $address_delivery->id_state;
        $seur_order->id_country = $address_delivery->id_country;
        $seur_order->dni = $address_delivery->dni;
        $seur_order->other = $address_delivery->other;
        $seur_order->labeled = 0;
        $seur_order->manifested = 0;
        $seur_order->codfee = 0;
        $seur_order->id_address_delivery = $order->id_address_delivery;
        $seur_order->id_status = 0;
        $seur_order->service = $seur_carrier->service;
        $seur_order->product = $seur_carrier->product;
        $seur_order->total_paid = $order->total_paid_real;

        $seur_order->save();

        $seur_carrier = new SeurCarrier($id_seur_carrier);

        $carrier = Carrier::getCarrierByReference($seur_carrier->carrier_reference);
        $order->id_carrier = $carrier->id;
        $order->save();
    }


    private function printLabelsTxt($string)
    {
        $id_orders = explode('_',$string);

        ob_end_clean();
        header('Content-type: application/rtf');
        header('Content-Disposition: inline; filename=ZEBRA_label_'.date('YmdHis').'.txt');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');

        foreach($id_orders as $id_order) {
            $seur_order = new SeurOrder($id_order);
            $order = new Order($seur_order->id_order);

            $this->createLabel($seur_order->id);


            $name = $order->reference;
            $directory = _PS_MODULE_DIR_ . 'seur/files/deliveries_labels/';

            if (file_exists($directory . $name . '.txt') && ($fp = Tools::file_get_contents($directory . $name . '.txt'))) {
                echo $fp;
            }
        }
        exit;
        $this->context->smarty->assign('error', $this->l('Document was already printed, but is missing in module directory'));
    }

    private function printLabel($id_order)
    {
        $seur_order = new SeurOrder($id_order);
        $order = new Order($seur_order->id_order);

        $this->createLabel($seur_order->id);

        $type = (Configuration::get('SEUR2_SETTINGS_PRINT_TYPE')==1? 'pdf':'txt');

        $name = $order->reference;
        $directory = _PS_MODULE_DIR_.'seur/files/deliveries_labels/';

        if ($type == 'txt')
        {
            if (file_exists($directory.$name.'.txt') && ($fp = Tools::file_get_contents($directory.$name.'.txt')))
            {

                ob_end_clean();
                header('Content-type: application/rtf');
                header('Content-Disposition: inline; filename='.$name.'.txt');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');

                echo $fp;
                exit;
/*
                echo "<html>
                    <body><div id='textoTotalDatosImpresionTermica'>".$fp."</div>
                    <script type='text/JavaScript' language='JavaScript1.2'>
                    var texto = document.getElementById('textoTotalDatosImpresionTermica').value ;
                    console.log = texto;
                    document.write('<div>');
                    document.write(texto);
                    document.write('</div>');
                    window.print();
           
                    var int=self.setInterval('cerrar()',50);
            
                    function cerrar(){
                        int=window.clearInterval(int);
                        window.close();
                    }
            
                    </script>   
                    </body></html>";
                exit;
*/
            }
        }
        elseif ($type == 'pdf')
        {
            if (file_exists($directory.$name.'.pdf') && ($fp = Tools::file_get_contents($directory.$name.'.pdf')))
            {
                ob_end_clean();
                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename='.$name.'.pdf');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');

                echo $fp;
                exit;
            }
        }
        $this->context->smarty->assign('error', $this->l('Document was already printed, but is missing in module directory'));
    }

    private function getExpeditions($references){

        $states = array();
        foreach($references as $reference) {
            $states[] = array((int)$reference=>SeurExpedition::getExpeditions(array('reference_number' => $reference)));
        }

    }




    public function adminDisplayError($msg)
    {
        if (!($this->context->controller instanceof AdminController)) {
            return false;
        }
        $this->context->controller->errors[] = $msg;
    }

    public function adminDisplayWarning($msg)
    {
        if (!($this->context->controller instanceof AdminController)) {
            return false;
        }
        $this->context->controller->warnings[] = $msg;
    }

    public function adminDisplayInformation($msg)
    {
        if (!($this->context->controller instanceof AdminController)) {
            return false;
        }
        $this->context->controller->informations[] = $msg;
    }


    private function generateManifest($id_orders){

        $manifest = SeurManifest::createManifest($id_orders);

    }

}
