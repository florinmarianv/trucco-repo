<?php
/*
	*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
	*
	* @author    Línea Gráfica E.C.E. S.L.
	* @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
	* @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
*/

require_once(_PS_MODULE_DIR_ . DIRECTORY_SEPARATOR . 'seur' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'SeurOrder.php');

class AdminSeurCollectingController extends ModuleAdminController
{

    public function __construct()
    {
        $module = Module::getInstanceByName('seur');;

        $this->addJQuery();
        $this->addJS($module->getPath() . 'views/js/seurController.js');

        $this->bootstrap = true;
        $this->name = 'AdminSeurCollecting';
        $this->table = 'seur2_order';
        $this->className = 'SeurOrder';
        $this->lang = false;
        $this->module = $module;
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        $this->explicitSelect = true;
        $this->allow_export = true;
        $this->deleted = false;
        $this->context = Context::getContext();
        $this->show_messages = array();


        AdminController::__construct();

        $this->context->smarty->assign('controlador', 'AdminSeurCollecting');

        if(Tools::isSubmit('request_pickup')){
            $id_seur_ccc = Tools::getValue('id_seur_ccc');
            $response = $this->requestPickup($id_seur_ccc);

            if($response){
                $this->adminDisplayError($response);
            }
            else{
                $this->adminDisplayInformation("Todo bien");
            }
        }

        if(Tools::isSubmit('cancel_pickup')){
            $id_seur_ccc = Tools::getValue('id_seur_ccc');
            $response = $this->cancelPickup($id_seur_ccc);
            if($response){
                $this->adminDisplayError($response);
            }
            else{
                $texto = $this->module->l("Solicitud realizada con éxito");

                $this->adminDisplayInformation($texto);
            }
        }
    }

    public function renderList()
    {

        if (count($this->module->errorConfigure())) {
            Tools::redirectAdmin('index.php?controller=adminmodules&configure=seur&token=' . Tools::getAdminTokenLite('AdminModules') . '&module_name=seur&settings=1');
            die();
        }

        $this->context->smarty->assign(
            array(
                'url_module' => $this->context->link->getAdminLink('AdminModules', true) . "&configure=seur&module_name=seur",
                'url_controller_shipping' => $this->context->link->getAdminLink('AdminSeurShipping', true),
                'url_controller_collecting' => $this->context->link->getAdminLink('AdminSeurCollecting', true),
                'url_controller_tracking' => $this->context->link->getAdminLink('AdminSeurTracking', true),
                'url_controller_returns' => $this->context->link->getAdminLink('AdminSeurReturns', true),
                'img_path' => $this->module->getPath() . 'views/img/',
                'module_path' => 'index.php?controller=AdminModules&configure=' . $this->module->name . '&token=' . Tools::getAdminToken("AdminModules" . (int)(Tab::getIdFromClassName("AdminModules")) . (int)$this->context->cookie->id_employee),
            ));

        $selecttab = "collecting";
        $this->context->smarty->assign(
            array('tabSelect' => $selecttab,
                    'show_messages' => $this->show_messages
            )
        );

        $page = $this->renderCollecting();


        $smarty = $this->context->smarty;
        $html = "";

        $html .= $smarty->fetch(_PS_MODULE_DIR_ . 'seur/views/templates/admin/header.tpl');;
        $html .= $smarty->fetch(_PS_MODULE_DIR_ . 'seur/views/templates/admin/tabs.tpl');;
        $html .= $page;
        return $html;
    }


    public function renderCollecting()
    {
        $pickupFixed = Configuration::get('SEUR2_SETTINGS_PICKUP');
        $pickupSolicited = array();

        if($pickupFixed==1) {
            $seur_cccs = SeurCCC::getCCCs();

            foreach ($seur_cccs as $seur_ccc) {
                $pickupSolicited[$seur_ccc['ccc']] = SeurPickup::getLastPickup($seur_ccc['id_seur_ccc']);
            }
        }

        $this->context->smarty->assign(
            array('pickupFixed' => $pickupFixed,
                'pickupSolicited' => $pickupSolicited)
        );

        $smarty = $this->context->smarty;
        $html = $smarty->fetch(_PS_MODULE_DIR_ . 'seur/views/templates/admin/collecting.tpl');;

        return $html;

    }


    private function requestPickup($id_ccc)
    {
        $pickup = new SeurPickup();

        return $pickup->createPickup($id_ccc);
    }

    private function cancelPickup($id_ccc)
    {
        $pickup = new SeurPickup();

        return $pickup->cancelPickup($id_ccc);
    }



    public function adminDisplayError($msg)
    {
        $this->show_messages[] = $this->module->displayError($msg);
    }

    public function adminDisplayWarning($msg)
    {
        $this->show_messages[] = $this->module->displayWarnings($msg);
    }

    public function adminDisplayInformation($msg)
    {
        $this->show_messages[] = $this->module->displayConfirmation($msg);
    }

}
