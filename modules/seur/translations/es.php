<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{seur}prestashop>seur_6911bf56fa3b5c1a9f10b8516e8ad2d7'] = 'Módulo SEUR';
$_MODULE['<{seur}prestashop>seur_ce9ff9c83a70bca4c14981329bc513f0'] = 'Configuración SEUR';
$_MODULE['<{seur}prestashop>seur_171c8a1450039eb0af20386662330a7b'] = 'Gestión de pedidos';
$_MODULE['<{seur}prestashop>seur_29f5ea42f62efe82083459119be704ad'] = 'Gestión de recogidas';
$_MODULE['<{seur}prestashop>seur_78eacb970a94604fd3fe53f73e473530'] = 'Seguimiento de envíos';
$_MODULE['<{seur}prestashop>seur_db846c966b4da4f4d43d0d5c3867b823'] = 'Transportistas SEUR';
$_MODULE['<{seur}prestashop>seur_b18c55a0468d0ede3b0c27a54cb7a075'] = 'SEUR';
$_MODULE['<{seur}prestashop>seur_94341d5e96f821356fc905f77c65eaa7'] = 'Gestione sus envíos con SEUR. Lider en el envío expres nacional e internacional';
$_MODULE['<{seur}prestashop>seur_bb8956c67b82c7444a80c6b2433dd8b4'] = '¿Está seguro que desea desinatalar el módulo de Seur?';
$_MODULE['<{seur}prestashop>seur_47a71944c58ecf5b83d85432a98d0ab8'] = 'La extención SOAP no está instalada en su servidor';
$_MODULE['<{seur}prestashop>seur_e3690f6acd056b98df16296efbb27a5f'] = 'El módulo no está configurado';
$_MODULE['<{seur}prestashop>seur_8913c3521d0f2bc4de8e8cb96ef5c0f0'] = 'La extensión SOAP debe estar habilitada en su servidor para usar este módulo';
$_MODULE['<{seur}prestashop>seur_92d4773a9f32fc3333cb8238471cf763'] = 'La extensión cURL debe estar habilitada en su servidor para usar este módulo';
$_MODULE['<{seur}prestashop>seur_7d83d7f536f37ae091cab8fcdb40a52b'] = 'Hooks no registrados';
$_MODULE['<{seur}prestashop>seur_27c6d0251345f7d9622647ab05752e76'] = 'Hook orderDetailDisplayed no registrado';
$_MODULE['<{seur}prestashop>seur_10447f99f041a76b57ad4bbdbe24337e'] = 'Hook displayOrderDetail no registrado';
$_MODULE['<{seur}prestashop>seur_8c66f4f015902186730e6809942e5f81'] = 'Error al crear los tabs';
$_MODULE['<{seur}prestashop>seur_2829b8d15db7569f028b490f5da8e7fd'] = 'Error al desinstalar el módulo de Transportista Seur';
$_MODULE['<{seur}prestashop>seur_3a50355807a61935d3ca8afda43a2f32'] = 'Error al crear las tablas de base de datos';
$_MODULE['<{seur}prestashop>seur_8acaed5c8fe69c137a1029ca4434c53b'] = 'Error al desinstalar el módulo de pago contrarrembolso de Seur';
$_MODULE['<{seur}prestashop>seur_e1105b2bf4b19b73d18b8ad9d36b8e93'] = 'Error al desinstalar los tabs';
$_MODULE['<{seur}prestashop>seur_452a1d5c110bb90b477773ee6fea18d9'] = 'Error al borrar la configuración';
$_MODULE['<{seur}prestashop>seur_29bb568ce10279f602548320ce605ec2'] = 'Sin estado';
$_MODULE['<{seur}prestashop>seur_6f16a5f8ff5d75ab84c018adacdfcbb7'] = 'Campo';
$_MODULE['<{seur}prestashop>seur_b8351d8ea7d20e622b6ce667a0a198c7'] = 'NIF/DNI';
$_MODULE['<{seur}prestashop>seur_c2cf4885cb21927bd13159279b817826'] = 'no definido';
$_MODULE['<{seur}prestashop>seur_ea4e637681560e841ad8ed8d93b81093'] = 'NOMBRE';
$_MODULE['<{seur}prestashop>seur_5f90c0d7af28b260cd68e4c12f4a6875'] = 'APELLIDOS';
$_MODULE['<{seur}prestashop>seur_ddd741a14cac2a68f2906ef29302c5cb'] = 'No ha configurado aún ningún CCC';
$_MODULE['<{seur}prestashop>seur_defb99e69a9f1f6e06f15006b1f166ae'] = 'CCC';
$_MODULE['<{seur}prestashop>seur_dc1490d4ed836d9ed85d182c2079dc12'] = 'CIT';
$_MODULE['<{seur}prestashop>seur_52677a2eb86ab1f5eb1fda4e27a3b341'] = 'FRANQUICIA';
$_MODULE['<{seur}prestashop>seur_f9dd946cc89c1f3b41a0edbe0f36931d'] = 'TELÉFONO';
$_MODULE['<{seur}prestashop>seur_61a649a33f2869e5e35fbb7aff3a80d9'] = 'EMAIL';
$_MODULE['<{seur}prestashop>seur_b366af5629348aa322be08e2c404bfac'] = 'TIPO DE VÍA';
$_MODULE['<{seur}prestashop>seur_dc475df2aa2c5a6eb34ef2edaf3c2ef4'] = 'NOMBRE DE VÍA';
$_MODULE['<{seur}prestashop>seur_a3b0d6a083a5f097938b89a3999f02a4'] = 'CÓDIGO POSTAL';
$_MODULE['<{seur}prestashop>seur_859214628431995197c0558f7b5f8ffc'] = 'CIUDAD';
$_MODULE['<{seur}prestashop>seur_2b848a8cc886d253d21a77c43cd50aae'] = 'PROVINCIA';
$_MODULE['<{seur}prestashop>seur_6ddc09dc456001d9854e9fe670374eb2'] = 'PAIS';
$_MODULE['<{seur}prestashop>seur_34f55eca38e0605a84f169ff61a2a396'] = 'NÚMERO';
$_MODULE['<{seur}prestashop>seur_17732abb60373dc2c653994d5f571ba6'] = 'USUARIO WS';
$_MODULE['<{seur}prestashop>seur_fa931a2497f83eea9885e3857b426be3'] = 'CONTRASEÑA WS';
$_MODULE['<{seur}prestashop>seur_6a2416dc88db290d4e4eb3a0f0853274'] = 'CLICL & COLLECT';
$_MODULE['<{seur}prestashop>getexpeditionajax_3b8769d187c6c3306c50729bc43a9903'] = 'Sin resultados';
$_MODULE['<{seur}prestashop>getlabelajax_dd10089eaca154339f9d2da5e8c22d6d'] = 'No se han encontrado resultados';
$_MODULE['<{seur}prestashop>getlabelajax_7a9dcdb96c4eb6396922fd15774b279a'] = 'Error al intentar obtener datos, por favor pruebe más tarde.';
$_MODULE['<{seur}prestashop>getpickuppointsajax_58433aef6eb48a2ce46ad12312346a24'] = 'Nº Centro: %1$s - Nº Vía: %2$s';
$_MODULE['<{seur}prestashop>label_af153862b972d8d3bd94d90cbd5f00d8'] = 'Error de conexión';
$_MODULE['<{seur}prestashop>pickup_32b9b5f573fd1aeded665827b976f1c0'] = 'Las recogidas no puede ser solicitadas depués de las 14:00h. Contcátenos por teléfono para incluirla manualmente.';
$_MODULE['<{seur}prestashop>user_816c8630ba665eb761cd2e4be8021174'] = 'Correo no enviado';
$_MODULE['<{seur}prestashop>adminseurcarriercontroller_8c2857a9ad1d8f31659e35e904e20fa6'] = 'Logo';
$_MODULE['<{seur}prestashop>adminseurcarriercontroller_49ee3087348e8d44e1feda1917443987'] = 'Nombre';
$_MODULE['<{seur}prestashop>adminseurcarriercontroller_ee02d8c78ed4ab750836ecb5cb8cd184'] = 'Tipo de envío';
$_MODULE['<{seur}prestashop>adminseurcarriercontroller_c2ba7e785c49050f48da9aacc45c2b85'] = 'Servicio';
$_MODULE['<{seur}prestashop>adminseurcarriercontroller_deb10517653c255364175796ace3553f'] = 'Producto';
$_MODULE['<{seur}prestashop>adminseurcarriercontroller_29aa46cc3d2677c7e0f216910df600ff'] = 'Envío gratuito';
$_MODULE['<{seur}prestashop>adminseurcarriercontroller_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Activo';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_49a02846fc51f47ee6c1e8ae4137eaac'] = 'ID envío Seur';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_7f55e0bf3baaa409935bb64ee9a53391'] = 'ID pedido';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_63d5049791d9d79d86e9a108b0a999ca'] = 'Referencia';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_b359ff11f34454ffb70e51479914a919'] = 'Fecha de pedido';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_49ee3087348e8d44e1feda1917443987'] = 'Nombre';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_dd7bf230fde8d4836917806aff6a6b27'] = 'Dirección';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_572ed696f21038e6cc6c86bb272a3222'] = 'Código Postal';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_57d056ed0984166336b7879c2af3657f'] = 'Ciudad';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_46a2a41cc6e552044816a2d04634545d'] = 'Provincia';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_59716c97497eb9694541f7c3d37b1a4d'] = 'Pais';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_ec53a8c4f07baed5d8825072c89799be'] = 'Estado';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_ee4716f32c4b0bc2fd8298e2ab7649d2'] = 'Etiquetado';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_9ca3f478932b75f11664189ee653f727'] = 'Manifestado';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_d3b206d196cd6be3a2764c1fb90b200f'] = 'Borrar seleccionados';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_e25f0ecd41211b01c83e5fec41df4fe7'] = '¿Borrar los elementos seleccionados?';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_fc2a40770d5d59cc9c3249db6d731b71'] = 'Gestión de Envíos';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_8589975d09aa5ce8b86a60eac72f0977'] = 'ID envío Seur';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_5ee9b37d3b842ed3bce5b643271c27eb'] = 'Seguimiento de envíos';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_987fd9158c047165d254d12f4c6e22b1'] = 'Editar pedido';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_1794831c3a570248cde6715ac6c007a6'] = '# Pedido';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_f4655d0c2bc52764d6ceb0a70fb691f3'] = '# Paquete';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_2a6fa68d3df59dc0b3ec27e6c6b0febf'] = 'Peso de paquetes';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_9b07c1269fe3afd3f2bbae338f93ce07'] = 'Impreso';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_004220ccfcee3ee674fc16b1022060f6'] = 'No impreso';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_c253a690671dc18ea7468aee5ad35b3b'] = 'Recargo';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_ea067eb37801c5aab1a1c685eb97d601'] = 'Total pagado';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_20db0bfeecd8fe60533206a2b5e9891a'] = 'Nombre';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_8d3f5eff9c40ee315d452392bed5309b'] = 'Apellido';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_956205f0d2c8352f3d92aa3438f1b646'] = 'Dirección 1';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_2e21e83375deefc4a3620ab667157e27'] = 'Dirección 2';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_25f75488c91cb6c3bab92672e479619f'] = 'Código postal';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_6311ae17c1ee52b36e68aaf4ad066387'] = 'Otros';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_bcc254b55c4a1babdf1dcb82c207506b'] = 'Teléfono';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_f0e1fc6f97d36cb80f29196e2662ffde'] = 'Móvil';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_265454b6b30f4e1fa210b6a5ce8eecef'] = 'No es posible imprimir valor para este envío';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_8ee543b76429cfab8101d4e7f3349484'] = 'Error de dirección, por favor revise la dirección del cliente';
$_MODULE['<{seur}prestashop>adminseurreturnscontroller_b52e0a313e6646e9c2646dec768615ed'] = 'El documento ya fue impreso, pero no se ha localizado.';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_d15239db95ca2ff8e160736ad360161b'] = 'Imprimr etiquetas de los envíos seleccionados';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_750da5cf2a5f0453868054f942e7249d'] = 'Generar manifiesto de los envíos seleccionados';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_49a02846fc51f47ee6c1e8ae4137eaac'] = 'ID envío SEUR';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_7f55e0bf3baaa409935bb64ee9a53391'] = 'ID pedido';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_b359ff11f34454ffb70e51479914a919'] = 'Fecha';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_63d5049791d9d79d86e9a108b0a999ca'] = 'Referencia';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_04176f095283bc729f1e3926967e7034'] = 'Nombre';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_dff4bf10409100d989495c6d5486035e'] = 'Apellidos';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_defb99e69a9f1f6e06f15006b1f166ae'] = 'CCC';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_dd7bf230fde8d4836917806aff6a6b27'] = 'Dirección';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_572ed696f21038e6cc6c86bb272a3222'] = 'Código Postal';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_57d056ed0984166336b7879c2af3657f'] = 'Ciudad';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_46a2a41cc6e552044816a2d04634545d'] = 'Provincia';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_59716c97497eb9694541f7c3d37b1a4d'] = 'Pais';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_ec53a8c4f07baed5d8825072c89799be'] = 'Estado';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_ee4716f32c4b0bc2fd8298e2ab7649d2'] = 'Etiquetado';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_9ca3f478932b75f11664189ee653f727'] = 'Manifestado';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_f8031d705a871758c7d5399d35b5eaff'] = 'Fecha de etiquetado';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_265454b6b30f4e1fa210b6a5ce8eecef'] = 'No es posible imprimir la etiqueta de este pedido.';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_8ee543b76429cfab8101d4e7f3349484'] = 'Error en direcciones, por favor revise la dirección de su cliente.';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_b021df6aac4654c454f46c77646e745f'] = 'Etiqueta';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_44c8575cd10d543dc1da875941b7dee3'] = 'generada:';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_0b3d46991ae671a43d12cfa6bf85417c'] = 'no generada:';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_8cf677d5ee66456c5411b49811685684'] = 'El transportista no está asignado a Seur';
$_MODULE['<{seur}prestashop>adminseurshippingcontroller_b52e0a313e6646e9c2646dec768615ed'] = 'El documento ya ha sido impreso, pero no está disponible.';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_49a02846fc51f47ee6c1e8ae4137eaac'] = 'ID Envío';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_7f55e0bf3baaa409935bb64ee9a53391'] = 'ID pedido';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_63d5049791d9d79d86e9a108b0a999ca'] = 'Referencia';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_b359ff11f34454ffb70e51479914a919'] = 'Fecha';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_04176f095283bc729f1e3926967e7034'] = 'Nombre';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_dff4bf10409100d989495c6d5486035e'] = 'Apellidos';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_dd7bf230fde8d4836917806aff6a6b27'] = 'Dirección';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_572ed696f21038e6cc6c86bb272a3222'] = 'Código Postal';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_57d056ed0984166336b7879c2af3657f'] = 'Ciudad';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_46a2a41cc6e552044816a2d04634545d'] = 'Provincia';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_59716c97497eb9694541f7c3d37b1a4d'] = 'País';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_2bc58dd0ab1ecf7fc764070f296a21ae'] = 'Estado de Envío';
$_MODULE['<{seur}prestashop>adminseurtrackingcontroller_5ee9b37d3b842ed3bce5b643271c27eb'] = 'Nº Tracking';
$_MODULE['<{seur}prestashop>seurcashondelivery_c437198917bf49b16dab100b8d0df396'] = 'Pago contrareembolso de SEUR';
$_MODULE['<{seur}prestashop>seurcashondelivery_12559007a93dc4fce3e915cf0b03462e'] = 'Este módulo se ejecuta bajo el módulo SEUR';
$_MODULE['<{seur}prestashop>seurcashondelivery_e3690f6acd056b98df16296efbb27a5f'] = 'Aún no ha configurado su módulo SEUR';
$_MODULE['<{seur}prestashop>seurcashondelivery_d704b4ce33aa79cae671701d42136c15'] = 'Atención: la clave de seguridad está vacía, chequee su cuenta de pago antes de la validación';
$_MODULE['<{seur}prestashop>seurcashondelivery_c2808546f3e14d267d798f4e0e6f102e'] = 'Personalizado';
$_MODULE['<{seur}prestashop>seurcashondelivery_9137796c15dd92e5553c3f29574d0968'] = 'Código de descuento.';
$_MODULE['<{seur}prestashop>cargocomprareembolso_e910dabc81357f81e33ea7eb8a6201c0'] = 'Total';
$_MODULE['<{seur}prestashop>seurcashondelivery_a02758d758e8bec77a33d7f392eb3f8a'] = 'No se ha configurado moneda para este módulo';
$_MODULE['<{seur}prestashop>carrier_80a579f847be7d8c960d458e2549ca84'] = 'Transportista SEUR';
$_MODULE['<{seur}prestashop>carrier_914419aa32f04011357d3b604a86d7eb'] = 'Transportista';
$_MODULE['<{seur}prestashop>carrier_67838a580a67c01392c8bc906654228a'] = 'Seleccione transportista';
$_MODULE['<{seur}prestashop>carrier_a5e5f13072d2fc79386143a3683268c1'] = 'No existen transportistas adicionales. Es necesrio que añada nuevos transportistas accediendo a';
$_MODULE['<{seur}prestashop>carrier_18f923cbdf0cbb4da6173d71e7cbd727'] = 'Tipo de envío';
$_MODULE['<{seur}prestashop>carrier_8af8437a86db9102ed3d40d00760f0a4'] = 'SEUR Nacional';
$_MODULE['<{seur}prestashop>carrier_a5856316c97662047930724d96cfa6ed'] = 'SEUR Puntos Pick Up';
$_MODULE['<{seur}prestashop>carrier_3568e4097aac64697a75d4fc110b6696'] = 'SEUR Internacional';
$_MODULE['<{seur}prestashop>carrier_c2ba7e785c49050f48da9aacc45c2b85'] = 'Servicio';
$_MODULE['<{seur}prestashop>carrier_b114a5802c18b0a16f0581369e6a1cf9'] = 'Seleccione el servicio';
$_MODULE['<{seur}prestashop>carrier_deb10517653c255364175796ace3553f'] = 'Producto';
$_MODULE['<{seur}prestashop>carrier_94f4a55d52037aee6f01a214739d0fec'] = 'Seleccione el producto';
$_MODULE['<{seur}prestashop>carrier_29aa46cc3d2677c7e0f216910df600ff'] = 'Gastos de envío gratuitos';
$_MODULE['<{seur}prestashop>carrier_93cba07454f06a4a960172bbd6e2a435'] = 'Si ';
$_MODULE['<{seur}prestashop>carrier_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{seur}prestashop>carrier_576b71e577fe4c87b715ff1cdc5adbd4'] = 'Por peso';
$_MODULE['<{seur}prestashop>carrier_a661e7efc47042e13285ab95b6bfd06e'] = 'Por precio';
$_MODULE['<{seur}prestashop>carrier_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Activar';
$_MODULE['<{seur}prestashop>carrier_5e74fd45affd2d41866fe499fad07d44'] = 'Activar/Desactivar transportista';
$_MODULE['<{seur}prestashop>carrier_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{seur}prestashop>carrier_ea4788705e6873b424c65e91c2846b19'] = 'Cancelar';
$_MODULE['<{seur}prestashop>collecting_59353628044b399d1d7a6f882914a045'] = 'Usted tiene configurado la recogida fija de envíos. Asegúrese que tiene contratado este servicio.';
$_MODULE['<{seur}prestashop>collecting_ed906952d470c3fc0156e4c2bdf08b00'] = 'CCC:';
$_MODULE['<{seur}prestashop>collecting_4ecb6b097d9979cf35e16a4f337ede97'] = 'Recogida prevista';
$_MODULE['<{seur}prestashop>collecting_ea4788705e6873b424c65e91c2846b19'] = 'Cancelar recogida';
$_MODULE['<{seur}prestashop>collecting_c3df84196e9dc18f5c2c31c1c8af9427'] = 'No tiene ninguna recogida activa en este momento.';
$_MODULE['<{seur}prestashop>collecting_1db85505ee2482cd5c6060582f77d826'] = 'Si lo desea, puede solicitarla aquí';
$_MODULE['<{seur}prestashop>collecting_2522d67efaef5ab5bc384723beb2f68c'] = 'Solicitar recogida';
$_MODULE['<{seur}prestashop>massives_acb08e162ed9d79b693bffa54955ecdf'] = 'Acciones masivas';
$_MODULE['<{seur}prestashop>massives_0e5291f591ac257aa9dab81fe5391f61'] = 'Imprimir Etiquetas';
$_MODULE['<{seur}prestashop>massives_b4e736b30f6dde59cae9f1ecf94bd96a'] = 'Generar manifiesto';
$_MODULE['<{seur}prestashop>massives_9beaafd25d55374e9e6fcb956b289829'] = 'Ejecutar acciones masivas';
$_MODULE['<{seur}prestashop>merchant_896890bfb700eac98300d639ca970f2b'] = 'Comerciante';
$_MODULE['<{seur}prestashop>merchant_51ac4bf63a0c6a9cefa7ba69b4154ef1'] = 'Configuración';
$_MODULE['<{seur}prestashop>merchant_9362e23da8739de45d878998b7d01e5d'] = 'Por favor, complete los siguientes puntos antes de continuar';
$_MODULE['<{seur}prestashop>merchant_dfcf99d96f0b9942bf171f30d152f6e0'] = 'Datos de identificación';
$_MODULE['<{seur}prestashop>merchant_ee6c5448de834f26a24c55b25946a5b4'] = 'NIF / CIF';
$_MODULE['<{seur}prestashop>merchant_e0d0b21965c5e2d8e4edde28270f8a7a'] = 'Añada el nif/cif de su empresa';
$_MODULE['<{seur}prestashop>merchant_bc910f8bdf70f29374f496f05be0330c'] = 'Nombre';
$_MODULE['<{seur}prestashop>merchant_b6ee799e7694c9df02d212a80c9eae85'] = 'Añada su nombre';
$_MODULE['<{seur}prestashop>merchant_8d3f5eff9c40ee315d452392bed5309b'] = 'Apellidos';
$_MODULE['<{seur}prestashop>merchant_8b7094b3219d50c7d71137eb2d3c7356'] = 'Alñada sus apellidos';
$_MODULE['<{seur}prestashop>merchant_1c76cbfe21c6f44c1d1e59d54f3e4420'] = 'Empresa';
$_MODULE['<{seur}prestashop>merchant_4b861256dd3a29c5f60a9dadec17fb2a'] = 'Añada el nombre de su empresa';
$_MODULE['<{seur}prestashop>merchant_63ff15059c346e3227b0595b12ad4d5e'] = 'Datos de cuenta';
$_MODULE['<{seur}prestashop>merchant_dc1490d4ed836d9ed85d182c2079dc12'] = 'CIT';
$_MODULE['<{seur}prestashop>merchant_defb99e69a9f1f6e06f15006b1f166ae'] = 'CCC';
$_MODULE['<{seur}prestashop>merchant_e25a12b446d6cde310552e0e2a6bca58'] = 'Será proporcionado por SEUR (código numérico entre 1 y 5 dígitos)';
$_MODULE['<{seur}prestashop>merchant_5f15e6a842d93e3e3f2a4e69d7e06bf5'] = 'Franquicia';
$_MODULE['<{seur}prestashop>merchant_8f9bfe9d1345237cb3b2b205864da075'] = 'Usuario';
$_MODULE['<{seur}prestashop>merchant_b9b57aae83585e17ede4570dcede353c'] = 'Contraseña';
$_MODULE['<{seur}prestashop>merchant_bcc254b55c4a1babdf1dcb82c207506b'] = 'Teléfono';
$_MODULE['<{seur}prestashop>merchant_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'Email';
$_MODULE['<{seur}prestashop>merchant_4771eae25bb5ccbcd85e053eae6870f1'] = 'Tipo de vía';
$_MODULE['<{seur}prestashop>merchant_beb41faeb129db97323294da99b2a139'] = 'Añada el tipo de vía de su dirección';
$_MODULE['<{seur}prestashop>merchant_c33d42f0d402f01275e850c1083a57fd'] = 'Nombre de vía';
$_MODULE['<{seur}prestashop>merchant_4be0991df82ec8f122804f04347eb508'] = 'Añada el nombre de su vía';
$_MODULE['<{seur}prestashop>merchant_572ed696f21038e6cc6c86bb272a3222'] = 'Código postal';
$_MODULE['<{seur}prestashop>merchant_dd6f481f56cff3f0122b57343b4f708a'] = 'Añada su código postal';
$_MODULE['<{seur}prestashop>merchant_57d056ed0984166336b7879c2af3657f'] = 'Ciudad / Población';
$_MODULE['<{seur}prestashop>merchant_f67fdd86a499050e0585bca9ea023188'] = 'Añada su población';
$_MODULE['<{seur}prestashop>merchant_46a2a41cc6e552044816a2d04634545d'] = 'Provincia';
$_MODULE['<{seur}prestashop>merchant_707059a2acc7aa67ebcfe0425e9406e3'] = 'Añada su provincia';
$_MODULE['<{seur}prestashop>merchant_59716c97497eb9694541f7c3d37b1a4d'] = 'País';
$_MODULE['<{seur}prestashop>merchant_8ad3253f8899a76e8a13686c54fea4ce'] = 'Añada su país';
$_MODULE['<{seur}prestashop>merchant_b2ee912b91d69b435159c7c3f6df7f5f'] = 'Número';
$_MODULE['<{seur}prestashop>merchant_59f9f261e44cff40d862ebcce95be7fc'] = 'Escalera';
$_MODULE['<{seur}prestashop>merchant_f3f6d0343d56ce88ce7958170ed05cb3'] = 'Planta';
$_MODULE['<{seur}prestashop>merchant_f44e14d49cd011d1e873d9fe0c4624f1'] = 'Puerta';
$_MODULE['<{seur}prestashop>merchant_64f91b3f9a1b8f45df00f87f1669e6d4'] = 'eDevoluciones';
$_MODULE['<{seur}prestashop>merchant_93cba07454f06a4a960172bbd6e2a435'] = 'Sí';
$_MODULE['<{seur}prestashop>merchant_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{seur}prestashop>merchant_5447157fe5bda8ceeae572eb6a8c3e96'] = 'Url eDevoluciones';
$_MODULE['<{seur}prestashop>merchant_1ebe583f4812f53d3293dc9d312594eb'] = 'Datos de conexión';
$_MODULE['<{seur}prestashop>merchant_4b6bce86e8032e5eae8fd51b802e66d4'] = 'Usuario en www.seur.com';
$_MODULE['<{seur}prestashop>merchant_53403b98594be26d98d79be6d42d8dac'] = 'Contraseña en www.seur.com';
$_MODULE['<{seur}prestashop>merchant_2540fcd01189420f10f0f5723b6d6f2f'] = 'Click & Collect';
$_MODULE['<{seur}prestashop>merchant_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Activar';
$_MODULE['<{seur}prestashop>merchant_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{seur}prestashop>order_data_27c58297f5bbebdcd774deb3b1b112d0'] = 'Ver desglose';
$_MODULE['<{seur}prestashop>order_data_1532f8207b4caa1b82941b95bed3eed9'] = 'Gastos de envío pagados por el cliente';
$_MODULE['<{seur}prestashop>order_data_9480897f379b92eb60d8b859863f3a9c'] = 'Gastos de envío cargados por SEUR';
$_MODULE['<{seur}prestashop>order_data_34f1a90ce43473df343231a70f7e324c'] = 'Envío SEUR';
$_MODULE['<{seur}prestashop>order_data_1b54be9dd6014bd3950dee5480dce55a'] = 'Seguimiento de envío';
$_MODULE['<{seur}prestashop>order_data_ec53a8c4f07baed5d8825072c89799be'] = 'Estado';
$_MODULE['<{seur}prestashop>order_data_b50c19ad58c059fc326bcc32d452fc27'] = '# Pack';
$_MODULE['<{seur}prestashop>order_data_8c489d0946f66d17d73f26366a4bf620'] = 'Peso';
$_MODULE['<{seur}prestashop>order_data_7dce122004969d56ae2e0245cb754d35'] = 'Editar';
$_MODULE['<{seur}prestashop>order_data_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{seur}prestashop>order_data_165977bc8006193936272e96ef4fd7a2'] = 'Imprimir etiqueta';
$_MODULE['<{seur}prestashop>order_data_f4393f810a031fe9363fee601b3b4b85'] = 'Editar envío';
$_MODULE['<{seur}prestashop>order_data_c4e5d32f689491f5cfaa96eed93b24b3'] = 'Nombre';
$_MODULE['<{seur}prestashop>order_data_dff4bf10409100d989495c6d5486035e'] = 'Apellidos';
$_MODULE['<{seur}prestashop>order_data_bcc254b55c4a1babdf1dcb82c207506b'] = 'Teléfono';
$_MODULE['<{seur}prestashop>order_data_fa07a3ee1639b60af128f7ab30da34e3'] = 'Móvil';
$_MODULE['<{seur}prestashop>order_data_ad41da5b673a2b9fbea3844025e9980d'] = 'DNI / NIF';
$_MODULE['<{seur}prestashop>order_data_0be8406951cdfda82f00f79328cf4efc'] = 'Observaciones';
$_MODULE['<{seur}prestashop>order_data_93d03fe37ab3c6abc2a19dd8e41543bd'] = 'Dirección 1';
$_MODULE['<{seur}prestashop>order_data_22fcffe02ab9eda5b769387122f2ddce'] = 'Dirección 2';
$_MODULE['<{seur}prestashop>order_data_25f75488c91cb6c3bab92672e479619f'] = 'Código postal';
$_MODULE['<{seur}prestashop>order_data_57d056ed0984166336b7879c2af3657f'] = 'Ciudad';
$_MODULE['<{seur}prestashop>order_data_59716c97497eb9694541f7c3d37b1a4d'] = 'País';
$_MODULE['<{seur}prestashop>order_data_46a2a41cc6e552044816a2d04634545d'] = 'Provincia';
$_MODULE['<{seur}prestashop>order_new_shipping_21f5443c4731944c95d8f302757f1798'] = ' ¿Quieres enviar este pedido con Seur?';
$_MODULE['<{seur}prestashop>order_new_shipping_456f267dae1f865a4f56fa1d9d95bc9f'] = 'Pulsa el botón para convertir este envío con SEUR';
$_MODULE['<{seur}prestashop>order_new_shipping_11d8386ed44275fc36de0172f2490040'] = 'Enviar con SEUR';
$_MODULE['<{seur}prestashop>order_new_shipping_972968bef1e79b6b6b09310de941d6e7'] = 'Selecciona un transportista';
$_MODULE['<{seur}prestashop>order_new_shipping_70d9be9b139893aa6c69b5e77e614311'] = 'Confirmar';
$_MODULE['<{seur}prestashop>order_new_shipping_c023bfc0d34442656fbdc860db9e4dcb'] = 'No ha dado de alta transportistas de Seur';
$_MODULE['<{seur}prestashop>order_new_shipping_3b7ce5eb0ae082b270a74216f62473bd'] = 'Acceda a la configuración de Transportistas SEUR para darlos de alta';
$_MODULE['<{seur}prestashop>settings_896890bfb700eac98300d639ca970f2b'] = 'Comerciante';
$_MODULE['<{seur}prestashop>settings_51ac4bf63a0c6a9cefa7ba69b4154ef1'] = 'Configuración';
$_MODULE['<{seur}prestashop>settings_22d2c7f90dc862df71ac7aff95183fb2'] = 'Pago contra reembolso';
$_MODULE['<{seur}prestashop>settings_6d8ea40ff2232bb12c9426947469953b'] = 'Activar contra reembolso';
$_MODULE['<{seur}prestashop>settings_f3d9007355f94299014eb97e06a4422e'] = 'Porcentaje comisión';
$_MODULE['<{seur}prestashop>settings_92ea878b60beff3b460c70cfa353c5fb'] = 'Comisión mínima';
$_MODULE['<{seur}prestashop>settings_e00b428741ac1b347a4e8875e5aa9020'] = 'Disponible desde';
$_MODULE['<{seur}prestashop>settings_41d47df3b3eb35278f78758315c2a2e6'] = 'Disponible hasta';
$_MODULE['<{seur}prestashop>settings_cb8b68786cfa3121ae4fef6eb165b182'] = 'Introduzca un porcentaje para cargar los pedidos como recargo y/o un importe en caso de no llegar al mínimo';
$_MODULE['<{seur}prestashop>settings_ca134be74038652f9dd1b1320d763051'] = 'Notificaciones y Avisos';
$_MODULE['<{seur}prestashop>settings_a274f4d4670213a9045ce258c6c56b80'] = 'Notificaciones';
$_MODULE['<{seur}prestashop>settings_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'Email';
$_MODULE['<{seur}prestashop>settings_4cecb21b44628b17c436739bf6301af2'] = 'SMS';
$_MODULE['<{seur}prestashop>settings_841fbcb820fb3de1537b59920c415271'] = 'Email y SMS';
$_MODULE['<{seur}prestashop>settings_920ef1532758573dd081bd228ad82625'] = 'SEUR le notificará cuando se realice el envío. Para usar esta funcionalidad de contratarla previamente.';
$_MODULE['<{seur}prestashop>settings_df583ae7ba964fd4806b55904da81813'] = 'Avisos';
$_MODULE['<{seur}prestashop>settings_82b7eaae964748b9471e0edce9de05cf'] = 'SEUR le informará cuando el paquete esté enviado. Para usar esta funcionalidad de contratarla previamente';
$_MODULE['<{seur}prestashop>settings_13dba24862cf9128167a59100e154c8d'] = 'Impresora';
$_MODULE['<{seur}prestashop>settings_e4204641574e4827600356b4dcacd276'] = 'PDF';
$_MODULE['<{seur}prestashop>settings_dca074dfe9ee9df94eed9179670d8e0f'] = 'Etiqueta';
$_MODULE['<{seur}prestashop>settings_d0d318cbcc8798a7d05ea564ba26988c'] = 'Seleccione Pdf para impresora normal. La impresora térmica debe proveerla SEUR.';
$_MODULE['<{seur}prestashop>settings_d15b505cce09406de9f918cb1f8c641c'] = 'Recogida';
$_MODULE['<{seur}prestashop>settings_086247a9b57fde6eefee2a0c4752242d'] = 'Automática';
$_MODULE['<{seur}prestashop>settings_cb8156d7b7b7c84c3ea4d4bdfad72edc'] = 'La recogida automática es generada automáticamente con el primer pedido del día.';
$_MODULE['<{seur}prestashop>settings_2de1239f236684c5bee75bd524a38a51'] = 'Fija';
$_MODULE['<{seur}prestashop>settings_035ce976b45e27071283e043675f713d'] = 'La recogida fija debe estar contratada con SEUR para pasar cada día a recoger.';
$_MODULE['<{seur}prestashop>settings_3ed05816aa05c30d857bf811549f7656'] = 'Api-Key de Google ';
$_MODULE['<{seur}prestashop>settings_0956d2fbd5d5c29844a4d21ed2f76e0c'] = 'Es necesario que disponga de una Clave de acceso API facilitada por Google. Para ello acceda a la siguiente dirección y siga las instrucciones indicadas : https://developers.google.com/maps';
$_MODULE['<{seur}prestashop>settings_936ccdb97115e9f35a11d35e3d5b5cad'] = 'Pulse aquí';
$_MODULE['<{seur}prestashop>settings_60d48be9e34e2e81904649094c6e9dcc'] = 'Capturar envíos de otros transportistas';
$_MODULE['<{seur}prestashop>settings_fa259c8e1eadbc6f5e4f51f038e3bbd0'] = 'Activar captura de pedidos';
$_MODULE['<{seur}prestashop>settings_c4dbbe57f78b929cb008552c9f0fca13'] = 'Estados SEUR';
$_MODULE['<{seur}prestashop>settings_221347de0e4f6192c3b832b289cbc41a'] = 'Cambiar estado de los pedidos a \"Enviado\" al etiquetar';
$_MODULE['<{seur}prestashop>settings_7ec4f8b296984ffe6ea829b7e1743577'] = 'En tránsito';
$_MODULE['<{seur}prestashop>settings_1f78ac0c97f5b1a0d3a24a11b68b1a8f'] = 'Devolución en progreso';
$_MODULE['<{seur}prestashop>settings_6a1d69d596bdd82e636bd9a989c2520c'] = 'Disponible recogida en tienda';
$_MODULE['<{seur}prestashop>settings_51463a1dcc4d123ce92cb94386fc2a04'] = 'Intervención requerida';
$_MODULE['<{seur}prestashop>settings_67edd3b99247c9eb5884a02802a20fa7'] = 'Entregado';
$_MODULE['<{seur}prestashop>settings_845713314d6a5922040b6e52af5aa7fa'] = 'Incidencia';
$_MODULE['<{seur}prestashop>tabs_c1aa636bbd0ad55b24c53b2c3ce34aa8'] = 'Gestión de pedidos';
$_MODULE['<{seur}prestashop>tabs_f7ab8cf8da9785bfb361f35443da47dd'] = 'Gestión de recogidas';
$_MODULE['<{seur}prestashop>tabs_5ee9b37d3b842ed3bce5b643271c27eb'] = 'Seguimiento de envíos';
$_MODULE['<{seur}prestashop>tabs_4762fdc5fdf2ab3118f73fe252a6668b'] = 'Devoluciones';
$_MODULE['<{seur}prestashop>warning_message_7b83d3f08fa392b79e3f553b585971cd'] = 'Aviso';
$_MODULE['<{seur}prestashop>welcome_67caf2522e37be870daaf89293b8a4a1'] = 'Bienvenidos al Módulo de SEUR';
$_MODULE['<{seur}prestashop>welcome_f40e918584ba39dbc4c83a9ecab1b21e'] = 'SEUR';
$_MODULE['<{seur}prestashop>welcome_312c5bb2a2ee24704f721f622066b26f'] = 'Prestashop';
$_MODULE['<{seur}prestashop>welcome_afb52560f6a105783e5eb5d96134f193'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quam diam, pharetra vel quam ut, pulvinar varius ex. Maecenas tristique ut ante eu congue. Proin fringilla risus ante. Cras mollis finibus sollicitudin.';
$_MODULE['<{seur}prestashop>welcome_2b30ebd619ddbbd1438381928c756864'] = 'Ponte en contacto con el equipo de SEUR llamando a este teléfono';
$_MODULE['<{seur}prestashop>welcome_1f8261d17452a959e013666c5df45e07'] = '902 376 994';
$_MODULE['<{seur}prestashop>welcome_5d2f389329bae0c6057c16fcf6828269'] = 'http://www.seur.com/seur-esolutions.do';
$_MODULE['<{seur}prestashop>welcome_baa63cd6dfcced377f0ccb7351fd0771'] = 'Ya soy cliente';
$_MODULE['<{seur}prestashop>header_29fedb0cc66c3d3c8c63adf9bf402bdb'] = 'No es posible encontrar un punto de recogida cercano';
$_MODULE['<{seur}prestashop>orderdetail_b18c55a0468d0ede3b0c27a54cb7a075'] = 'SEUR';
$_MODULE['<{seur}prestashop>orderdetail_63d5049791d9d79d86e9a108b0a999ca'] = 'Referencia';
$_MODULE['<{seur}prestashop>orderdetail_b41345e412f177a82b7640447968a5ed'] = 'Estado';
$_MODULE['<{seur}prestashop>orderdetail_77cd9fa27c50288413e2ead667fdda84'] = 'Sigue tu pedido online';
$_MODULE['<{seur}prestashop>seur_872830b58e8f9b4e359c48459a2106b4'] = 'Seleccione un punto de recogida.';
$_MODULE['<{seur}prestashop>seur_891a56988de26d467f9fffd4beb50ea9'] = 'Seleccione un punto de recogida.';
$_MODULE['<{seur}prestashop>seur_1c3fc304ac7839accdf99b6bc6db544c'] = 'Seleccione un punto de recogida antes de continuar con su pedido.';
$_MODULE['<{seur}prestashop>seur_868108483bbfb4acada747c0ef06b3a9'] = 'Punto de recogida seleccionado';
$_MODULE['<{seur}prestashop>seur_1c76cbfe21c6f44c1d1e59d54f3e4420'] = 'Empresa';
$_MODULE['<{seur}prestashop>seur_dd7bf230fde8d4836917806aff6a6b27'] = 'Dirección';
$_MODULE['<{seur}prestashop>seur_57d056ed0984166336b7879c2af3657f'] = 'Ciudad';
$_MODULE['<{seur}prestashop>seur_25f75488c91cb6c3bab92672e479619f'] = 'Código postal';
$_MODULE['<{seur}prestashop>seur_440500daca3e14c4db9733d6006f423a'] = 'Horarios';
$_MODULE['<{seur}prestashop>seur_bcc254b55c4a1babdf1dcb82c207506b'] = 'Teléfono';
$_MODULE['<{seur}prestashop>seur17_872830b58e8f9b4e359c48459a2106b4'] = 'Selecciones un punto de recogida';
$_MODULE['<{seur}prestashop>seur17_891a56988de26d467f9fffd4beb50ea9'] = 'Selecciones un punto de recogida';
$_MODULE['<{seur}prestashop>seur17_1c3fc304ac7839accdf99b6bc6db544c'] = 'Selecciones un punto de recogida antes de proceder con su pedido';
$_MODULE['<{seur}prestashop>seur17_868108483bbfb4acada747c0ef06b3a9'] = 'Punto de recogida seleccionado';
$_MODULE['<{seur}prestashop>seur17_1c76cbfe21c6f44c1d1e59d54f3e4420'] = 'Empresa';
$_MODULE['<{seur}prestashop>seur17_dd7bf230fde8d4836917806aff6a6b27'] = 'Dirección';
$_MODULE['<{seur}prestashop>seur17_57d056ed0984166336b7879c2af3657f'] = 'Ciudad';
$_MODULE['<{seur}prestashop>seur17_25f75488c91cb6c3bab92672e479619f'] = 'Código postal';
$_MODULE['<{seur}prestashop>seur17_440500daca3e14c4db9733d6006f423a'] = 'Horarios';
$_MODULE['<{seur}prestashop>seur17_bcc254b55c4a1babdf1dcb82c207506b'] = 'Teléfono';
