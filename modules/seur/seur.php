<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

if (!class_exists('SeurLib'))
    require_once(_PS_MODULE_DIR_.'seur/classes/SeurLib.php');

if (!class_exists('SeurCCC'))
    require_once(_PS_MODULE_DIR_.'seur/classes/SeurCCC.php');

if (!class_exists('SeurOrder'))
    require_once(_PS_MODULE_DIR_.'seur/classes/SeurOrder.php');

if (!class_exists('SeurCarrier'))
    require_once(_PS_MODULE_DIR_.'seur/classes/SeurCarrier.php');


include_once(_PS_MODULE_DIR_.'seur/classes/Range.php');
include_once(_PS_MODULE_DIR_.'seur/classes/User.php');
include_once(_PS_MODULE_DIR_.'seur/classes/Rate.php');
include_once(_PS_MODULE_DIR_.'seur/classes/Town.php');
include_once(_PS_MODULE_DIR_.'seur/classes/Pickup.php');
include_once(_PS_MODULE_DIR_.'seur/classes/Expedition.php');
include_once(_PS_MODULE_DIR_.'seur/classes/Label.php');
include_once(_PS_MODULE_DIR_.'seur/classes/Manifest.php');





class Seur extends CarrierModule
{
    protected $config_form = false;
    public $path;
    private $js_url;

    public function __construct()
    {

        $this->name = 'seur';
        $this->tab = 'shipping_logistics';
        $this->version = '2.1.1.3';
        $this->author = 'Línea Gráfica';
        $this->need_instance = 0;

        $this->tabs = array();

        $this->js_url = 'https://maps.google.com/maps/api/js?key=' . Configuration::get('SEUR2_GOOGLE_API_KEY');


        $this->tabs['AdminSeurAdmin'] = array(
            'label' => $this->l('Módulo SEUR'),
            'rootClass' => true,
        );
        $this->tabs['AdminSeurConfig'] = array(
            'label' => $this->l('Configuración SEUR'),
            'rootClass' => false,
            'parent' => 'AdminSeurAdmin',
        );
        $this->tabs['AdminSeurShipping'] = array(
            'label' => $this->l('Gestión de pedidos'),
            'rootClass' => false,
            'parent' => 'AdminSeurAdmin',
        );
        $this->tabs['AdminSeurCollecting'] = array(
            'label' => $this->l('Gestión de recogidas'),
            'rootClass' => false,
            'parent' => 'AdminSeurAdmin',
        );
        $this->tabs['AdminSeurTracking'] = array(
            'label' => $this->l('Seguimiento de envíos'),
            'rootClass' => false,
            'parent' => 'AdminSeurAdmin',
        );
        $this->tabs['AdminSeurCarrier'] = array(
            'label' => $this->l('Transportistas SEUR'),
            'rootClass' => false,
            'parent' => 'AdminSeurAdmin',
        );
        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->path = $this->_path;

        $this->displayName = $this->l('SEUR');
        $this->description = $this->l('Manage your shipments with SEUR. Leader in the Express Shipping, National or International.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');

        /** Backward compatibility 1.4 / 1.5 */
        if (version_compare(_PS_VERSION_, '1.5', '<')) {
            require_once(_PS_MODULE_DIR_ . $this->name . '/backward_compatibility/backward.php');
            $this->context = Context::getContext();
            $this->smarty = $this->context->smarty;
        } else {
            $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        }

        if (!class_exists('SoapClient')) {
            $this->active = 1; //only to display warning before install
            $this->warning = $this->l('The SOAP extension is not enabled on your server, please contact to your hosting provider.');
        } elseif (!$this->isConfigured()) {
            $this->warning = $this->l('Still has not configured their SEUR module.');
        }
    }

    /*************************************************************************************
     *                                  INSTALL
     ************************************************************************************/

    public function install()
    {
        if (!extension_loaded('soap') || !class_exists('SoapClient')) {
            $this->_errors[] = $this->l('SOAP extension should be enabled on your server to use this module.');
            return false;
        }

        if (extension_loaded('curl') == false) {
            $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
            return false;
        }


        if (!parent::install()
            || !$this->registerHook('adminOrder')
            || !$this->registerHook('orderDetail')
            || !$this->registerHook('updateCarrier')
            || !$this->registerHook('displayOrderConfirmation')
            || !$this->registerHook('header')
            || !$this->registerHook('backOfficeHeader')
            || !$this->registerHook('actionValidateOrder')
        ) {
            $this->l('Hooks not registered');
            return false;
        }

        if (version_compare(_PS_VERSION_, '1.7.0', '<')) {
            $this->registerHook('extraCarrier');
        } else {
            $this->registerHook('displayAfterCarrier');
            $this->registerHook('actionFrontControllerSetMedia');
        }

        if (version_compare(_PS_VERSION_, '1.5.4', '<')) {
            if (!$this->registerHook('orderDetailDisplayed')) {
                $this->_errors[] = $this->l('Hook orderDetailDisplayed not registered');
                return false;
            }
        } else {
            if (!$this->registerHook('displayOrderDetail')) {
                $this->_errors[] = $this->l('Hook displayOrderDetail not registered');
                return false;
            }
        }

        if (
        !$this->createAdminTab()
        ) {
            $this->uninstall();
            $this->_errors[] = $this->l('Error to create Tabs');
            return false;
        }

        $this->createOrderStates();


        if (!$this->installSeurCashOnDelivery()
        ) {
            $this->uninstall();
            $this->_errors[] = $this->l('Error to install Cash on delivery');
            return false;
        }


        if (!$this->createDatabases()
//            || !$this->createCarriers()
//            || !$this->installSeurCashOnDelivery()
        ) {
            $this->uninstall();
            $this->_errors[] = $this->l('Error to create Data Base');
            return false;
        }

        return true;
    }

    public function createDatabases()
    {
        include(dirname(__FILE__) . '/sql/install.php');

        /* Webservices default configuration */
        Configuration::updateValue('SEUR2_URLWS_SP', 'https://ws.seur.com/WSEcatalogoPublicos/servlet/XFireServlet/WSServiciosWebPublicos?wsdl');
        Configuration::updateValue('SEUR2_URLWS_R', 'https://ws.seur.com/webseur/services/WSCrearRecogida?wsdl');
        Configuration::updateValue('SEUR2_URLWS_E', 'https://ws.seur.com/webseur/services/WSConsultaExpediciones?wsdl');
        Configuration::updateValue('SEUR2_URLWS_A', 'https://ws.seur.com/webseur/services/WSConsultaAlbaranes?wsdl');
        Configuration::updateValue('SEUR2_URLWS_ET', 'http://cit.seur.com/CIT-war/services/ImprimirECBWebService?wsdl');
        Configuration::updateValue('SEUR2_URLWS_M', 'http://cit.seur.com/CIT-war/services/DetalleBultoPDFWebService?wsdl');
        Configuration::updateValue('SEUR2_URLWS_ET_GEOLABEL', 'https://api.seur.com/geolabel/swagger-ui.html#/add-shipment-controller');

        /* Global configuration */
        Configuration::updateValue('SEUR2_REMCAR_TIPO_CARGO', '');
        Configuration::updateValue('SEUR2_REMCAR_CARGO', 5.5);
        Configuration::updateValue('SEUR2_REMCAR_CARGO_MIN', 0);
        Configuration::updateValue('SEUR2_NACIONAL_SERVICE', '031');
        Configuration::updateValue('SEUR2_NACIONAL_PRODUCT', '002');
        Configuration::updateValue('SEUR2_INTERNACIONAL_SERVICE', '077');
        Configuration::updateValue('SEUR2_INTERNACIONAL_PRODUCT', '070');
        Configuration::updateValue('SEUR2_WS_USERNAME', '');
        Configuration::updateValue('SEUR2_WS_PASSWORD', '');
        Configuration::updateValue('SEUR2_COD_REEMBOLSO', 40);

        /*
                if (Context::getContext()->shop->isFeatureActive() == true) {
                    $fields = '(`id_seur_configuration`, `international_orders`, `seur_cod`, `pos`, `notification_advice_radio`, `notification_distribution_radio`, `print_type`, `tarifa`, `pickup`, `advice_checkbox`, `distribution_checkbox`, `id_shop`)';
                    $values = '(NULL, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1)';
                } else {
                    $fields = '(`id_seur_configuration`, `international_orders`, `seur_cod`, `pos`, `notification_advice_radio`, `notification_distribution_radio`, `print_type`, `tarifa`, `pickup`, `advice_checkbox`, `distribution_checkbox`)';
                    $values = '(NULL, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0)';
                }

                $seur_configuration = 'INSERT INTO `' . _DB_PREFIX_ . 'seur_configuration` ' . $fields . ' VALUES ' . $values . ';';

                $seur_merchant = 'INSERT INTO `' . _DB_PREFIX_ . 'seur_merchant`
                    (`id_seur_datos`, `user`, `pass`, `cit`, `ccc`, `nif_dni`, `name`, `first_name`, `franchise`, `company_name`, `street_type`, `street_name`, `street_number`, `staircase`, `floor`, `door`, `post_code`, `town`, `state`, `country`, `phone`, `fax`, `email`)
                    VALUES (NULL, "USER", "PASS", "CCC", "CIT", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);';

                $result =  Db::getInstance()->Execute($seur_configuration) && Db::getInstance()->Execute($seur_merchant);
        */
        return true;
    }


    public function createAdminTab()
    {
        $this->uninstallTab();

        $flagInstall = true;

        // Build menu tabs
        foreach ($this->tabs as $className => $data) {
            // Check if exists
            if (!$id_tab = Tab::getIdFromClassName($className)) {
                if ($data['rootClass']) {
                    $this->_confirmations[] = "Instalando Tab $className<br>\n";
                    $flagInstall = $flagInstall && $this->installModuleTab($className, $data['label'], 0);
                } else {
                    $this->_confirmations[] = "Instalando Tab $className cuyo padre es " . $data['parent'] . "<br>\n";
                    $flagInstall = $flagInstall && $this->installModuleTab($className, $data['label'], (int)Tab::getIdFromClassName($data['parent']));
                }
            }
        }

        return $flagInstall;
    }


    public function installModuleTab($tabClass, $tabName, $idTabParent)
    {
        Logger::addLog("ADMIN TAB Install. $tabClass, $tabName, $idTabParent", 1);

        // Create tab object
        $tab = new Tab();
        $tab->class_name = $tabClass;
        $tab->id_parent = $idTabParent;
        $tab->module = $this->name;
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $this->l($tabName);
        }

        return $tab->save();
    }

    /**
     * Create new order states
     */
    public function createOrderStates()
    {

        // PEDIDO CON INCIDENCIA

        if (!Configuration::get('SEUR2_STATUS_INCIDENCE')) {
            $order_state = new OrderState();
            $order_state->name = array();

            foreach (Language::getLanguages() as $language) {
                if (Tools::strtolower($language['iso_code']) == 'es') {
                    $order_state->name[$language['id_lang']] = 'SEUR: Envío con Incidencia';
                } else {
                    $order_state->name[$language['id_lang']] = 'SEUR: Shipping Incidence';
                }

            }

            $order_state->send_email = false;
            $order_state->color = '#ffd31c';
            $order_state->hidden = true;
            $order_state->delivery = false;
            $order_state->logable = true;
            $order_state->invoice = true;

            if ($order_state->add()) {
                $source = dirname(__FILE__) . '/views/img/seur_estado.gif';
                $destination = dirname(__FILE__) . '/../../img/os/' . (int)$order_state->id . '.gif';
                copy($source, $destination);
            }
            Configuration::updateValue('SEUR2_STATUS_INCIDENCE', (int)$order_state->id);
        }

        // PEDIDO EN TRANSITO

        if (!Configuration::get('SEUR2_STATUS_IN_TRANSIT')) {
            $order_state = new OrderState();
            $order_state->name = array();

            foreach (Language::getLanguages() as $language) {
                if (Tools::strtolower($language['iso_code']) == 'es') {
                    $order_state->name[$language['id_lang']] = 'SEUR: Envío en tránsito';
                } else {
                    $order_state->name[$language['id_lang']] = 'SEUR: Shipping in transit';
                }

            }

            $order_state->send_email = false;
            $order_state->color = '#006aff';
            $order_state->hidden = true;
            $order_state->delivery = false;
            $order_state->logable = true;
            $order_state->invoice = true;

            if ($order_state->add()) {
                $source = dirname(__FILE__) . '/views/img/seur_estado.gif';
                $destination = dirname(__FILE__) . '/../../img/os/' . (int)$order_state->id . '.gif';
                copy($source, $destination);
            }
            Configuration::updateValue('SEUR2_STATUS_IN_TRANSIT', (int)$order_state->id);
        }

        // DEVOLUCIÓN EN PROGRESO

        if (!Configuration::get('SEUR2_STATUS_RETURN_IN_PROGRESS')) {
            $order_state = new OrderState();
            $order_state->name = array();

            foreach (Language::getLanguages() as $language) {
                if (Tools::strtolower($language['iso_code']) == 'es') {
                    $order_state->name[$language['id_lang']] = 'SEUR: Devolución en progreso';
                } else {
                    $order_state->name[$language['id_lang']] = 'SEUR: Return in progress';
                }

            }

            $order_state->send_email = false;
            $order_state->color = '#bf0044';
            $order_state->hidden = true;
            $order_state->delivery = false;
            $order_state->logable = true;
            $order_state->invoice = true;

            if ($order_state->add()) {
                $source = dirname(__FILE__) . '/views/img/seur_estado.gif';
                $destination = dirname(__FILE__) . '/../../img/os/' . (int)$order_state->id . '.gif';

                copy($source, $destination);
            }
            Configuration::updateValue('SEUR2_STATUS_RETURN_IN_PROGRESS', (int)$order_state->id);
        }

        Configuration::updateValue('SEUR2_STATUS_DELIVERED', 5);

        // PEDIDO DISPONIBLE RECOGIDA EN TIENDA

        if (!Configuration::get('SEUR2_STATUS_AVAILABLE_IN_STORE')) {
            $order_state = new OrderState();
            $order_state->name = array();

            foreach (Language::getLanguages() as $language) {
                if (Tools::strtolower($language['iso_code']) == 'es') {
                    $order_state->name[$language['id_lang']] = 'SEUR: Disponible en tienda';
                } else {
                    $order_state->name[$language['id_lang']] = 'SEUR: Available in store';
                }
            }

            $order_state->send_email = false;
            $order_state->color = '#00a762';
            $order_state->hidden = false;
            $order_state->delivery = false;
            $order_state->logable = true;
            $order_state->invoice = true;

            if ($order_state->add()) {
                $source = dirname(__FILE__) . '/views/img/seur_estado.gif';
                $destination = dirname(__FILE__) . '/../../img/os/' . (int)$order_state->id . '.gif';

                copy($source, $destination);
            }
            Configuration::updateValue('SEUR2_STATUS_AVAILABLE_IN_STORE', (int)$order_state->id);
        }

        // PEDIDO REQUIERE INTERVENCIÓN PARA SOLUCIÓN

        if (!Configuration::get('SEUR2_STATUS_CONTRIBUTE_SOLUTION')) {
            $order_state = new OrderState();
            $order_state->name = array();

            foreach (Language::getLanguages() as $language) {
                if (Tools::strtolower($language['iso_code']) == 'es') {
                    $order_state->name[$language['id_lang']] = 'SEUR: Intervención requerida';
                } else {
                    $order_state->name[$language['id_lang']] = 'SEUR: Contribute solution';
                }
            }

            $order_state->send_email = false;
            $order_state->color = '#d74900';
            $order_state->hidden = false;
            $order_state->delivery = false;
            $order_state->logable = true;
            $order_state->invoice = true;

            if ($order_state->add()) {
                $source = dirname(__FILE__) . '/views/img/seur_estado.gif';
                $destination = dirname(__FILE__) . '/../../img/os/' . (int)$order_state->id . '.gif';

                copy($source, $destination);
            }
            Configuration::updateValue('SEUR2_STATUS_CONTRIBUTE_SOLUTION', (int)$order_state->id);
        }

        Configuration::updateValue('SEUR2_STATUS_DELIVERED', Configuration::get("PS_OS_DELIVERED"));

    }

    public function installSeurCashOnDelivery()
    {
        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            if (!is_dir(_PS_MODULE_DIR_ . 'seurcashondelivery')) {
                $module_dir = _PS_MODULE_DIR_ . str_replace(array('.', '/', '\\'), array('', '', ''), 'seurcashondelivery');
                $this->recursiveDeleteOnDisk($module_dir);
            }
            $dir = _PS_MODULE_DIR_ . $this->name . '/install/1.5/seurcashondelivery';
            if (!is_dir($dir))
                return false;

            $this->copyDirectory($dir, _PS_MODULE_DIR_ . 'seurcashondelivery');
            $cash_on_delivery = Module::GetInstanceByName('seurcashondelivery');

            return $cash_on_delivery->install();
        } else {
            if (!is_dir(_PS_MODULE_DIR_ . 'seurcashondelivery')) {
                $module_dir = _PS_MODULE_DIR_ . str_replace(array('.', '/', '\\'), array('', '', ''), 'seurcashondelivery');
                $this->recursiveDeleteOnDisk($module_dir);
            }
            $dir = _PS_MODULE_DIR_ . $this->name . '/install/1.7/seurcashondelivery';
            if (!is_dir($dir))
                return false;

            $this->copyDirectory($dir, _PS_MODULE_DIR_ . 'seurcashondelivery');
            $cash_on_delivery = Module::GetInstanceByName('seurcashondelivery');

            return $cash_on_delivery->install();
        }
    }

    public function recursiveDeleteOnDisk($dir)
    {
        if (strpos(realpath($dir), realpath(_PS_MODULE_DIR_)) === false)
            return;
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object)
                if ($object != '.' && $object != '..') {
                    if (filetype($dir . '/' . $object) == 'dir')
                        $this->recursiveDeleteOnDisk($dir . '/' . $object);
                    else
                        unlink($dir . '/' . $object);
                }
            reset($objects);
            rmdir($dir);
        }
    }

    public function copyDirectory($source, $target)
    {
        if (!is_dir($source)) {
            copy($source, $target);
            return null;
        }

        @mkdir($target);
        chmod($target, 0755);
        $d = dir($source);
        $nav_folders = array('.', '..');
        while (false !== ($file_entry = $d->read())) {
            if (in_array($file_entry, $nav_folders))
                continue;

            $s = "$source/$file_entry";
            $t = "$target/$file_entry";
            self::copyDirectory($s, $t);
        }
        $d->close();
    }

    /*************************************************************************************
     *                                  UNINSTALL
     ************************************************************************************/


    public function uninstall()
    {
        include(dirname(__FILE__) . '/sql/uninstall.php');

        if (!$this->uninstallSeurCashOnDelivery()) {
            $this->_errors[] = $this->l('Error to Uninstall Seur Cash on delivery');
            return false;
        }

        if (!$this->uninstallTab()) {

            $this->_errors[] = $this->l('Error to Uninstall tabs');
            return false;
        }

        if (!$this->deleteSettings()) {

            $this->_errors[] = $this->l('Error to Delete Settings');
            return false;
        }
        return parent::uninstall();
    }

    private function uninstallTab()
    {
        $flagUninstall = true;

        $roots = array();
        $childs = array();

        foreach ($this->tabs as $className => $data) {
            if (isset($data['rootClass']) && $data['rootClass']) {
                $roots[$className] = $data;
            } else {
                $childs[$className] = $data;
            }
        }


        // Unbuild Menu
        foreach ($childs as $className => $data) {
            $this->_confirmations[] = "Desinstalando Tab $className<br>\n";
            $flagUninstall = $flagUninstall && $this->uninstallModuleTab($className);
        }

        foreach ($roots as $className => $data) {

            if (!$this->tabHasChilds($className)) {
                $this->_confirmations[] = "Desinstalando Tab $className<br>\n";
                $flagUninstall = $flagUninstall && $this->uninstallModuleTab($className);
            }
        }

        //return $flagUninstall;
        return true;
    }

    private function uninstallSeurCashOnDelivery()
    {

        if ($module = Module::getInstanceByName('seurcashondelivery')) {
            if (Module::isInstalled($module->name) && !$module->uninstall())
                return false;

            $module_dir = _PS_MODULE_DIR_ . str_replace(array('.', '/', '\\'), array('', '', ''), $module->name);
            $this->recursiveDeleteOnDisk($module_dir);
        }

        return true;
    }

    public function tabHasChilds($className)
    {
        $id_tab = Tab::getIdFromClassName($className);
        if ($id_tab) {
            $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'tab` WHERE `id_parent` = ' . $id_tab;
            $hijos = Db::getInstance()->executeS($sql);
            return (bool)count($hijos);
        }
    }

    public function uninstallModuleTab($tabClass)
    {

        $idTab = Tab::getIdFromClassName($tabClass);
        Logger::addLog("ADMIN TAB Uninstall. $tabClass, $idTab", 1);
        if ($idTab != 0) {
            $tab = new Tab($idTab);
            if ($tab->delete()) {
                return true;
            }
        }
        return false;
    }


    private function deleteSettings()
    {
        $success = true;

        $success &= Configuration::deleteByName('SEUR2_URLWS_SP');
        $success &= Configuration::deleteByName('SEUR2_URLWS_R');
        $success &= Configuration::deleteByName('SEUR2_URLWS_E');
        $success &= Configuration::deleteByName('SEUR2_URLWS_A');
        $success &= Configuration::deleteByName('SEUR2_URLWS_ET');
        $success &= Configuration::deleteByName('SEUR2_URLWS_M');

        $success &= Configuration::deleteByName('SEUR2_REMCAR_CARGO');
        $success &= Configuration::deleteByName('SEUR2_REMCAR_CARGO_MIN');
        $success &= Configuration::deleteByName('SEUR2_FREE_PRICE');
        $success &= Configuration::deleteByName('SEUR2_FREE_WEIGTH');
        $success &= Configuration::deleteByName('SEUR2_WS_USERNAME');
        $success &= Configuration::deleteByName('SEUR2_WS_PASSWORD');
        $success &= Configuration::deleteByName('SEUR2_COD_REEMBOLSO');

        $success &= Configuration::deleteByName('SEUR2_MERCHANT_NIF_DNI');
        $success &= Configuration::deleteByName('SEUR2_MERCHANT_FIRSTNAME');
        $success &= Configuration::deleteByName('SEUR2_MERCHANT_LASTNAME');
        $success &= Configuration::deleteByName('SEUR2_MERCHANT_COMPANY');

        $success &= Configuration::deleteByName('SEUR2_MERCHANT_USER');
        $success &= Configuration::deleteByName('SEUR2_MERCHANT_PASS');
        $success &= Configuration::deleteByName('SEUR2_WS_USERNAME');
        $success &= Configuration::deleteByName('SEUR2_WS_PASSWORD');
        $success &= Configuration::deleteByName('SEUR2_GOOGLE_API_KEY');


        $success &= Configuration::deleteByName('SEUR2_SETTINGS_COD');
        $success &= Configuration::deleteByName('SEUR2_SETTINGS_COD_FEE_PERCENT');
        $success &= Configuration::deleteByName('SEUR2_SETTINGS_COD_FEE_MIN');
        $success &= Configuration::deleteByName('SEUR2_SETTINGS_COD_MIN');
        $success &= Configuration::deleteByName('SEUR2_SETTINGS_COD_MAX');
        $success &= Configuration::deleteByName('SEUR2_SETTINGS_NOTIFICATION');
        $success &= Configuration::deleteByName('SEUR2_SETTINGS_NOTIFICATION_TYPE');
        $success &= Configuration::deleteByName('SEUR2_SETTINGS_ALERT');
        $success &= Configuration::deleteByName('SEUR2_SETTINGS_ALERT_TYPE');
        $success &= Configuration::deleteByName('SEUR2_SETTINGS_PRINT_TYPE');
        $success &= Configuration::deleteByName('SEUR2_SETTINGS_PICKUP');
        $success &= Configuration::deleteByName('SEUR2_GOOGLE_API_KEY');

        /*
                $success &= Configuration::deleteByName('SEUR2_STATUS_IN_TRANSIT');
                $success &= Configuration::deleteByName('SEUR2_STATUS_RETURN_IN_PROGRESS');
                $success &= Configuration::deleteByName('SEUR2_STATUS_AVAILABLE_IN_STORE');
                $success &= Configuration::deleteByName('SEUR2_STATUS_CONTRIBUTE_SOLUTION');
                $success &= Configuration::deleteByName('SEUR2_STATUS_DELIVERED');
                $success &= Configuration::deleteByName('SEUR2_STATUS_INCIDENCE');
        */

        $success &= Configuration::deleteByName('SEUR2_SENDED_ORDER');

        return $success;
    }


    /**
     * Load the configuration form
     */
    public function getContent()
    {

        if (Tools::getValue('ajax', 0)) {
            echo $this->proccessAjax();
            die();
        }

        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitMerchantSeur')) == true) {
            $this->postProcessMerchant();
        }

        if (((bool)Tools::isSubmit('submitSettingsSeur')) == true) {
            $this->postProcessSettings();
        }

        $errors = $this->errorConfigure();
        if (count($errors)) {
            $this->context->smarty->assign(
                array(
                    'errors' => $this->displayError($this->errorConfigure()),
                ));
        }


        $this->context->smarty->assign(
            array(
                'url_module' => $this->context->link->getAdminLink('AdminModules', true) . "&configure=seur&module_name=seur",
                'img_path' => $this->_path . 'views/img/',
                'module_path' => 'index.php?controller=AdminModules&configure=' . $this->name . '&token=' . Tools::getAdminToken("AdminModules" . (int)(Tab::getIdFromClassName("AdminModules")) . (int)$this->context->cookie->id_employee),
                'lista_ccc' =>  SeurLib::getListCCC()

            ));

        $this->context->smarty->assign('module_dir', $this->_path);

        if (count($this->errorConfigure()) > 0 && !Tools::getValue('settings') && !Tools::getValue('merchant')) {
            $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/welcome.tpl');
            return $output;
        }
        if (count($this->errorConfigure()) == 0 && Tools::getValue('settings')) {
            $this->loadParamsSettingsSmarty();
            $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/settings.tpl');
            return $output;
        }

        if ($_GET['ccc']) {
            $id_ccc = (int) $_GET['ccc'];
        }


        $this->loadParamsMerchantSmarty($id_ccc);
        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/merchant.tpl');
        return $output;
    }


    public function getPath()
    {
        return $this->_path;
    }

    /**
     *  DESARROLLO DE MËTODOS ABSTRACTOS OBLIGADOS
     */


    public function getOrderShippingCost($params, $shipping_cost)
    {
        /*
                if (Context::getContext()->customer->logged == true)
                {
                    $id_address_delivery = Context::getContext()->cart->id_address_delivery;
                    $address = new Address($id_address_delivery);

                    return $shipping_cost;
                }
        */
        if ($id_seur_carrier = (int)SeurCarrier::getSeurCarrierByIdCarrier($params->id_carrier)) {

            $carrier = new Carrier($params->id_carrier);
            $seur_carrier = new SeurCarrier($id_seur_carrier);

            if ($seur_carrier->free_shipping) {

                $min_price = $seur_carrier->free_shipping_price;
                $min_weight = $seur_carrier->free_shipping_weight;

                // CONSULTAMOS EL TOTAL DEL CARRITO CON IVA
                $cart_price = $params->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING, $params->getProducts());

                // CONSULTAMOS EL PESO DEL CARRITO CON IVA
                $cart_weight = $params->getTotalWeight($params->getProducts());

                if ($min_price == 0 && $min_weight == 0) {
                    return 0;
                }

                if ($min_price > 1 && $min_price <= $cart_price) {
                    return 0;
                }

                if ($min_weight > 1 && $min_weight <= $cart_weight) {
                    return 0;
                }
            }
        }
        return $shipping_cost;
    }

    public function getOrderShippingCostExternal($params)
    {
        return true;
    }
    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
            $this->context->controller->addJquery();
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            $this->context->controller->addJS($this->_path . 'views/js/front.js');
            $this->context->controller->addCSS($this->_path . 'views/css/front.css');
        }

        $this->context->smarty->assign(
            array(
                'img_path' => $this->_path.'views/img/',
        ));


        $seur_carrier_pos = SeurLib::getSeurPOSCarrier();
        $pos_is_enabled = SeurCarrier::isPosActive();



        $this->context->smarty->assign(
            array(
                'id_seur_pos' => (int)$seur_carrier_pos['id_seur_carrier'],
                'seur_dir' => $this->_path,
                'ps_version' => _PS_VERSION_
            )
        );

        $httpsStr = 0;
        if (version_compare(_PS_VERSION_, '1.5', '>='))
        {
            $page = (Tools::getValue('controller') ? Tools::getValue('controller') : null);
            if (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode())
                $httpsStr = 1;
        }
        else
        {
            $page = explode('/', $_SERVER['SCRIPT_NAME']);
            $page = end($page);
            if (Configuration::get('PS_SSL_ENABLED') || (isset($_SERVER['HTTPS']) && (Tools::strtolower($_SERVER['HTTPS']) == 'on')))
                $httpsStr = 1;
        }


        if ($pos_is_enabled && ($page == 'order-opc.php' || $page == 'order.php' || $page == 'orderopc' || $page == 'order')) {
            $this->context->controller->addCSS($this->_path . 'views/css/seurGMap.css');

            if (Tools::version_compare(_PS_VERSION_, '1.7', '<'))
            {
                $this->context->controller->addJS($this->_path . 'views/js/seurGMap.js');
                $this->context->controller->addJS($this->js_url);
            }

            $current_customer_addresses_ids = array();
            foreach ($this->context->customer->getAddresses((int)$this->context->language->id) as $address)
                $current_customer_addresses_ids[] = (int)$address['id_address'];
            $this->context->smarty->assign('customer_addresses_ids', $current_customer_addresses_ids);


            return $this->display(__FILE__, 'views/templates/hook/header.tpl');
        }

    }

    public function hookUpdateCarrier($params)
    {
        /**
         * Not needed since 1.5
         * You can identify the carrier by the id_reference
        */




    }

    public function hookActionValidateOrder($params)
    {
        $order = $params['order'];
        $orderStatus = $params['orderStatus'];
        $estados_cancelados = array();
        $estados_cancelados[] = Configuration::get('PS_OS_ERROR');
        $estados_cancelados[] = Configuration::get('PS_OS_CANCELED');

        if (SeurLib::isSeurOrder($order->id)) {
            return true;
        }

        if (!SeurLib::isSeurCarrier($order->id_carrier)) {
            return true;
        }

        if (in_array($orderStatus,$estados_cancelados)) {
            return true;
        }

        //Lineagrafica - F.Torres -> Obtenemos todos los datos del envío.


        $address = new Address((int)$order->id_address_delivery);

        $seurOrder = new SeurOrder();

        $id_ccc = (int)SeurCCC::getCCCDefault();

        $seur_carrier_array = SeurLib::getSeurCarrier($order->id_carrier);

        $seur_carrier = new SeurCarrier($seur_carrier_array['id_seur_carrier']);


        $seurOrder->id_order = $order->id;
        $seurOrder->numero_bultos = 1;
        $seurOrder->id_status = 0;
        $seurOrder->id_seur_ccc = $id_ccc;
        $seurOrder->peso_bultos = $order->getTotalWeight();
        $seurOrder->id_address_delivery = $order->id_address_delivery;
        $seurOrder->id_seur_carrier = $seur_carrier->id_seur_carrier;
        $seurOrder->date_labeled = NULL;
        $seurOrder->product = $seur_carrier->product;
        $seurOrder->service = $seur_carrier->service;
        $seurOrder->firstname = $address->firstname;
        $seurOrder->lastname = $address->lastname;
        $seurOrder->id_country = $address->id_country;
        $seurOrder->id_state = $address->id_state;
        $seurOrder->dni = $address->dni;
        $seurOrder->other = $address->other;
        $seurOrder->phone = $address->phone;
        $seurOrder->phone_mobile = $address->phone_mobile;

        /* Comprobamos si es una recogida en punto de venta */


        $pickup_point_info = SeurLib::getOrderPos((int)$params['order']->id_cart);

        if($pickup_point_info && $seur_carrier->product == 48 && $seur_carrier->service== 1) {
            $seurOrder->address1 = $pickup_point_info['address'];
            $seurOrder->address2 = "";
            $seurOrder->postcode = $pickup_point_info['postal_code'];
            $seurOrder->city = $pickup_point_info['city'];
        }
        else{
            $seurOrder->address1 = $address->address1;
            $seurOrder->address2 = $address->address2;
            $seurOrder->postcode = $address->postcode;
            $seurOrder->city = $address->city;
        }



        $seurOrder->cashondelivery = 0;
        $seurOrder->codfee = 0;

        $seurOrder->total_paid = $order->total_paid_real;

        $seurOrder->labeled = 0;
        $seurOrder->manifested = 0;

        $seurOrder->save();

        return true;
    }

    public function hookDisplayOrderDetail($params)
    {
        return $this->hookOrderDetailDisplayed($params);
    }

    public function hookOrderDetailDisplayed($params)
    {
        if ($this->isConfigured())
        {
            $seur_carriers = SeurLib::getSeurCarriers(false);
            $ids_seur_carriers = array();


            foreach ($seur_carriers as $value) {
                $ids_seur_carriers[] = (int)$value['carrier_reference'];
            }

            $order = new Order((int)$params['order']->id);
            $orderSeur = SeurOrder::getByOrder((int)$params['order']->id);

            if (!Validate::isLoadedObject($orderSeur))
                return false;

            $referencia = $order->reference;
            $fecha = $orderSeur->date_labeled;

            if($orderSeur->id_status)
                $url_tracking = "https://www.seur.com/livetracking/pages/seguimiento-online.do?segOnlineIdentificador=".$referencia."&segOnlineFecha=".substr($fecha,8,2)."-".substr($fecha,5,2)."-".substr($fecha,0,4);
            else
                $url_tracking = "";

            $this->context->smarty->assign(
                array(
                    'logo' => $this->_path.'views/img/logo_seur.png',
                    'reference' => $order->reference,
                    'delivery' => "",
                    'seur_order_state' => (!empty($orderSeur->status_text) ? (string)$orderSeur->status_text : $this->l('Sin estado')),
                    'url_tracking' => $url_tracking
                )
            );
            return $this->display(__FILE__, 'views/templates/hook/orderDetail.tpl');
        }
    }


    public function hookDisplayAfterCarrier($params)
    {
        if ($this->isConfigured())
        {
            $process_type = Configuration::get('PS_ORDER_PROCESS_TYPE');
            $seur_carriers = SeurLib::getSeurCarriers(true);
            $pos_is_enabled = SeurCarrier::isPosActive();

            $seur_carriers_without_pos = '';
            $seur_carrier_pos = '';
            foreach ($seur_carriers as $seur_carrier) {
                if ($seur_carrier['shipping_type'] != 2) {
                    $seur_carriers_without_pos .= (int)$seur_carrier['id_carrier'] . ',';
                }
                else{
                    $seur_carrier_pos = (int)$seur_carrier['id_carrier'];
                }
            }

            $seur_carriers_without_pos = trim($seur_carriers_without_pos, ',');

            if ($process_type == '0')
                $this->context->smarty->assign('id_address', $this->context->cart->id_address_delivery);

            $this->context->smarty->assign(
                array(
                    'posEnabled' => $pos_is_enabled,
                    'cookie' => $this->context->cookie,
                    'id_seur_pos' => $seur_carrier_pos,
                    'seur_resto' => $seur_carriers_without_pos,
                    'src' => $this->_path.'img/unknown.gif',
                    'ps_version' => version_compare(_PS_VERSION_, '1.5', '<') ? 'ps4' : version_compare(_PS_VERSION_, '1.7', '<') ? 'ps5' : 'ps7'
                )
            );

            return $this->display(__FILE__, 'views/templates/hook/seur17.tpl');
        }
    }


    public function hookActionFrontControllerSetMedia($params)
    {
        if (!$this->context) {
            $context = Context::getContext();
        } else {
            $context = $this->context;
        }

        $controller = $context->controller;
        if ($controller instanceof OrderController) {
            if (Tools::version_compare(_PS_VERSION_, '1.7.0.2', '>=')) {

                $this->context->controller->registerjavascript(
                    $this->name . '-gmap',
                    $this->js_url,
                    array(
                        'position' => 'head',
                        'priority' => 150,
                        'server' => 'remote',
                        'inline' => false,
                    )
                );
            }
        }


        $this->context->controller->addCSS($this->_path.'views/css/seurGMap.css');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');

        $this->context->controller->registerjavascript(
            $this->name . '-seurgmap',
            'modules/' . $this->name . '/views/js/seurGMap.js',
            array(
                'position' => 'bottom',
                'priority' => 150,
                'server' => 'local',
                'inline' => false,
            )
        );

        $this->context->controller->registerjavascript(
            $this->name . '-front',
            'modules/' . $this->name . '/views/js/frontMap.js',
            array(
                'position' => 'bottom',
                'priority' => 150,
                'server' => 'local',
                'inline' => false,
            )
        );

        Media::addJsDef(array(
           'baseDir' => __PS_BASE_URI__
        ));
    }

    public function hookExtraCarrier($params)
    {
        if ($this->isConfigured())
        {
            $process_type = Configuration::get('PS_ORDER_PROCESS_TYPE');
            $seur_carriers = SeurLib::getSeurCarriers(true);
            $pos_is_enabled = SeurCarrier::isPosActive();

            $seur_carriers_without_pos = '';
            $seur_carrier_pos = '';
            foreach ($seur_carriers as $seur_carrier) {
                if ($seur_carrier['shipping_type'] != 2) {
                    $seur_carriers_without_pos .= (int)$seur_carrier['id_carrier'] . ',';
                }
                else{
                    $seur_carrier_pos = (int)$seur_carrier['id_carrier'];
                }
            }

            $seur_carriers_without_pos = trim($seur_carriers_without_pos, ',');

            if ($process_type == '0')
                $this->context->smarty->assign('id_address', $this->context->cart->id_address_delivery);

            $this->context->smarty->assign(
                array(
                    'posEnabled' => $pos_is_enabled,
                    'id_seur_pos' => $seur_carrier_pos,
                    'seur_resto' => $seur_carriers_without_pos,
                    'src' => $this->_path.'img/unknown.gif',
                    'ps_version' => version_compare(_PS_VERSION_, '1.5', '<') ? 'ps4' : 'ps5'
                )
            );

            return $this->display(__FILE__, 'views/templates/hook/seur.tpl');
        }
    }



    /**
     * Save form data.
     */
    protected function postProcessMerchant()
    {

        /*
        Configuration::updateValue("SEUR2_MERCHANT_STREET_TYPE", Tools::getValue("SEUR2_MERCHANT_STREET_TYPE"));
        Configuration::updateValue("SEUR2_MERCHANT_STREET_NAME", Tools::getValue("SEUR2_MERCHANT_STREET_NAME"));
        Configuration::updateValue("SEUR2_MERCHANT_POST_CODE", Tools::getValue("SEUR2_MERCHANT_POST_CODE"));
        Configuration::updateValue("SEUR2_MERCHANT_CITY", Tools::getValue("SEUR2_MERCHANT_CITY"));
        Configuration::updateValue("SEUR2_MERCHANT_STATE", Tools::getValue("SEUR2_MERCHANT_STATE"));
        Configuration::updateValue("SEUR2_MERCHANT_COUNTRY", Tools::getValue("SEUR2_MERCHANT_COUNTRY"));
        Configuration::updateValue("SEUR2_MERCHANT_NUMBER", Tools::getValue("SEUR2_MERCHANT_NUMBER"));
        Configuration::updateValue("SEUR2_MERCHANT_STAIR", Tools::getValue("SEUR2_MERCHANT_STAIR"));
        Configuration::updateValue("SEUR2_MERCHANT_FLOOR", Tools::getValue("SEUR2_MERCHANT_FLOOR"));
        Configuration::updateValue("SEUR2_MERCHANT_DOOR", Tools::getValue("SEUR2_MERCHANT_DOOR"));
        Configuration::updateValue("SEUR2_MERCHANT_USER", Tools::getValue("SEUR2_MERCHANT_USER"));
        Configuration::updateValue("SEUR2_MERCHANT_PASS", Tools::getValue("SEUR2_MERCHANT_PASS"));
        */

        Configuration::updateValue("SEUR2_MERCHANT_NIF_DNI", Tools::getValue("SEUR2_MERCHANT_NIF_DNI"));
        Configuration::updateValue("SEUR2_MERCHANT_FIRSTNAME", Tools::getValue("SEUR2_MERCHANT_FIRSTNAME"));
        Configuration::updateValue("SEUR2_MERCHANT_LASTNAME", Tools::getValue("SEUR2_MERCHANT_LASTNAME"));
        Configuration::updateValue("SEUR2_MERCHANT_COMPANY", Tools::getValue("SEUR2_MERCHANT_COMPANY"));
        Configuration::updateValue("SEUR2_WS_USERNAME", Tools::getValue("SEUR2_WS_USERNAME"));
        Configuration::updateValue("SEUR2_WS_PASSWORD", Tools::getValue("SEUR2_WS_PASSWORD"));
        Configuration::updateValue("SEUR2_MERCHANT_CLICKCOLLECT", Tools::getValue("SEUR2_MERCHANT_CLICKCOLLECT"));

        if(Tools::getValue("id_seur_ccc")== 0){
            $id_ccc = Tools::getValue("id_seur_ccc");

        }else{
            /*$sql = "SELECT id_seur_ccc FROM `"._DB_PREFIX_."seur2_ccc` ORDER BY id_seur_ccc";
            $id_ccc = (int)Db::getInstance()->getValue($sql);*/
            $id_ccc = Tools::getValue("id_seur_ccc");
        }

        $seur_ccc = new SeurCCC($id_ccc);

        if(Tools::getValue("ccc") == '' ||
            Tools::getValue("cit") == '' ||
            Tools::getValue("franchise") == '' ||
            Tools::getValue("ws_user") == '' )  {
            return false;
        }

        print_r(Tools::getValue("nombre_personalizado"));

        $seur_ccc->ccc = Tools::getValue("ccc");
        $seur_ccc->cit = Tools::getValue("cit");
        $seur_ccc->franchise = Tools::getValue("franchise");
        $seur_ccc->nombre_personalizado = Tools::getValue("nombre_personalizado");
        $seur_ccc->phone = Tools::getValue("phone");
        $seur_ccc->fax = Tools::getValue("fax");
        $seur_ccc->email = Tools::getValue("email");
        $seur_ccc->ws_user = Tools::getValue("ws_user");
        $seur_ccc->ws_password = Tools::getValue("ws_password");
        $seur_ccc->e_devoluciones = Tools::getValue("eDevoluciones");
        $seur_ccc->url_devoluciones = Tools::getValue("urleDevoluciones");
        $seur_ccc->click_connect = Tools::getValue("clickCollect");

        $seur_ccc->post_code = Tools::getValue("post_code");
        $seur_ccc->street_type = Tools::getValue("street_type");
        $seur_ccc->street_name = Tools::getValue("street_name");
        $seur_ccc->town = Tools::getValue("town");
        $seur_ccc->state = Tools::getValue("state");
        $seur_ccc->country = Tools::getValue("country");
        $seur_ccc->street_number = Tools::getValue("street_number");
        $seur_ccc->staircase = Tools::getValue("staircase");
        $seur_ccc->floor = Tools::getValue("floor");
        $seur_ccc->door = Tools::getValue("door");
        $seur_ccc->geolabel = Tools::getValue("geolabel");

        $seur_ccc->save();

    }

    protected function postProcessSettings()
    {
        Configuration::updateValue("SEUR2_SETTINGS_COD", Tools::getValue("SEUR2_SETTINGS_COD"));
        Configuration::updateValue("SEUR2_SETTINGS_COD_FEE_PERCENT", Tools::getValue("SEUR2_SETTINGS_COD_FEE_PERCENT"));
        Configuration::updateValue("SEUR2_SETTINGS_COD_FEE_MIN", Tools::getValue("SEUR2_SETTINGS_COD_FEE_MIN"));
        Configuration::updateValue("SEUR2_SETTINGS_COD_MIN", Tools::getValue("SEUR2_SETTINGS_COD_MIN"));
        Configuration::updateValue("SEUR2_SETTINGS_COD_MAX", Tools::getValue("SEUR2_SETTINGS_COD_MAX"));
        Configuration::updateValue("SEUR2_SETTINGS_NOTIFICATION", Tools::getValue("SEUR2_SETTINGS_NOTIFICATION"));
        Configuration::updateValue("SEUR2_SETTINGS_NOTIFICATION_TYPE", Tools::getValue("SEUR2_SETTINGS_NOTIFICATION_TYPE"));
        Configuration::updateValue("SEUR2_SETTINGS_ALERT", Tools::getValue("SEUR2_SETTINGS_ALERT"));
        Configuration::updateValue("SEUR2_SETTINGS_ALERT_TYPE", Tools::getValue("SEUR2_SETTINGS_ALERT_TYPE"));
        Configuration::updateValue("SEUR2_SETTINGS_PRINT_TYPE", Tools::getValue("SEUR2_SETTINGS_PRINT_TYPE"));
        Configuration::updateValue("SEUR2_SETTINGS_PICKUP", Tools::getValue("SEUR2_SETTINGS_PICKUP"));
        Configuration::updateValue("SEUR2_GOOGLE_API_KEY", Tools::getValue("SEUR2_GOOGLE_API_KEY"));
        Configuration::updateValue("SEUR2_CAPTURE_ORDER", Tools::getValue("SEUR2_CAPTURE_ORDER"));
        Configuration::updateValue("SEUR2_STATUS_DELIVERED", Tools::getValue("select_status_SEUR2_STATUS_DELIVERED"));
        Configuration::updateValue("SEUR2_STATUS_IN_TRANSIT", Tools::getValue("select_status_SEUR2_STATUS_IN_TRANSIT"));
        Configuration::updateValue("SEUR2_STATUS_INCIDENCE", Tools::getValue("select_status_SEUR2_STATUS_INCIDENCE"));
        Configuration::updateValue("SEUR2_STATUS_RETURN_IN_PROGRESS", Tools::getValue("select_status_SEUR2_STATUS_RETURN_IN_PROGRESS"));
        Configuration::updateValue("SEUR2_STATUS_AVAILABLE_IN_STORE", Tools::getValue("select_status_SEUR2_STATUS_AVAILABLE_IN_STORE"));
        Configuration::updateValue("SEUR2_STATUS_CONTRIBUTE_SOLUTION", Tools::getValue("select_status_SEUR2_STATUS_CONTRIBUTE_SOLUTION"));
        Configuration::updateValue("SEUR2_SENDED_ORDER", Tools::getValue("SEUR2_SENDED_ORDER"));
    }

    private function loadParamsMerchantSmarty($id_ccc){


        if($id_ccc == NULL){
            $sql = "SELECT id_seur_ccc FROM `"._DB_PREFIX_."seur2_ccc` ORDER BY id_seur_ccc";
            $id_ccc = (int)Db::getInstance()->getValue($sql);
        }


        if($id_ccc)
        {
            $seur_ccc = new SeurCCC($id_ccc);

            $this->context->smarty->assign(
                array(
                'id_seur_ccc' => $seur_ccc->id_seur_ccc,
                'ccc' => $seur_ccc->ccc,
                'cit' => $seur_ccc->cit,
                'franchise' => $seur_ccc->franchise,
                'nombre_personalizado' => $seur_ccc->nombre_personalizado,
                'phone' => $seur_ccc->phone,
                'email' => $seur_ccc->email,
                'ws_user' => $seur_ccc->ws_user,
                'ws_password' => $seur_ccc->ws_password,
                'eDevoluciones' => $seur_ccc->e_devoluciones,
                'urleDevoluciones' => $seur_ccc->url_devoluciones,
                'street_type' => $seur_ccc->street_type,
                'street_name' => $seur_ccc->street_name,
                'post_code' => $seur_ccc->post_code,
                'town' => $seur_ccc->town,
                'state' => $seur_ccc->state,
                'country' => $seur_ccc->country,
                'street_number' => $seur_ccc->street_number,
                'staircase' => $seur_ccc->staircase,
                'floor' => $seur_ccc->floor,
                'door' => $seur_ccc->door,
                'geolabel' => $seur_ccc->geolabel
            ));
        }
        else
        {
            $this->context->smarty->assign(
                array(
                'id_seur_ccc' => '',
                'cit' => '',
                'ccc' => '',
                'franchise' => '',
                'phone' => '',
                'email' => '',
                'ws_user' => '',
                'ws_password' => '',
                'eDevoluciones' => '',
                'urleDevoluciones' => '',
                'street_type' =>  '',
                'street_name' =>  '',
                'post_code' =>  '',
                'town' =>  '',
                'state' =>  '',
                'country' =>  '',
                'street_number' =>  '',
                'staircase' =>  '',
                'floor' =>  '',
                'door' =>  '',
                ));
        }


        $this->context->smarty->assign(
            array(
                'clickCollect' => Configuration::get('SEUR2_MERCHANT_CLICKCOLLECT'),
                'nif_cif' => Configuration::get('SEUR2_MERCHANT_NIF_DNI'),
                'firstname' => Configuration::get('SEUR2_MERCHANT_FIRSTNAME'),
                'lastname' => Configuration::get('SEUR2_MERCHANT_LASTNAME'),
                'company' => Configuration::get('SEUR2_MERCHANT_COMPANY'),
/*
                'user' => Configuration::get('SEUR2_MERCHANT_USER'),
                'pass' => Configuration::get('SEUR2_MERCHANT_PASS'),
*/
                'user_www' => Configuration::get('SEUR2_WS_USERNAME'),
                'pass_www' => Configuration::get('SEUR2_WS_PASSWORD'),
            ));

    }

    private function loadParamsSettingsSmarty(){

        $status_ps = Db::getInstance()->executeS('SELECT o.*, l.name FROM `' . _DB_PREFIX_ . 'order_state` o LEFT JOIN `' . _DB_PREFIX_ . 'order_state_lang` l ON o.id_order_state = l.id_order_state AND id_lang=' . Context::getContext()->language->id);
        $module = Module::getInstanceByName('seurcashondelivery');

        $this->context->smarty->assign(
            array(
                'status_ps' => $status_ps,
                'cashDelivery' => $module->active,
                'cod_fee_percent' => Configuration::get('SEUR2_SETTINGS_COD_FEE_PERCENT'),
                'cod_fee_min' => Configuration::get('SEUR2_SETTINGS_COD_FEE_MIN'),
                'cod_min' => Configuration::get('SEUR2_SETTINGS_COD_MIN'),
                'cod_max' => Configuration::get('SEUR2_SETTINGS_COD_MAX'),
                'notification' => Configuration::get('SEUR2_SETTINGS_NOTIFICATION'),
                'notification_type' => Configuration::get('SEUR2_SETTINGS_NOTIFICATION_TYPE'),
                'alerts' => Configuration::get('SEUR2_SETTINGS_ALERT'),
                'alerts_type' => Configuration::get('SEUR2_SETTINGS_ALERT_TYPE'),
                'print_type' => Configuration::get('SEUR2_SETTINGS_PRINT_TYPE'),
                'collection_type' => Configuration::get('SEUR2_SETTINGS_PICKUP'),
                'status_in_transit' => Configuration::get('SEUR2_STATUS_IN_TRANSIT'),
                'status_return_in_progress' => Configuration::get('SEUR2_STATUS_RETURN_IN_PROGRESS'),
                'status_available_in_store' => Configuration::get('SEUR2_STATUS_AVAILABLE_IN_STORE'),
                'status_contribute_solution' => Configuration::get('SEUR2_STATUS_CONTRIBUTE_SOLUTION'),
                'status_incidence' => Configuration::get('SEUR2_STATUS_INCIDENCE'),
                'status_delivered' => Configuration::get('SEUR2_STATUS_DELIVERED'),
                'google_key' => Configuration::get('SEUR2_GOOGLE_API_KEY'),
                'capture_order' => Configuration::get('SEUR2_CAPTURE_ORDER'),
                'sended_order' => Configuration::get('SEUR2_SENDED_ORDER'),
            ));
    }


    public function ajaxProcessActivateCashonDelivery()
    {
        $module = Module::getInstanceByName('seurcashondelivery');
        if (Validate::isLoadedObject($module)) {
            if ($module->getPermission('configure')) {
                $module->enable();
            }
        }
        return true;
    }

    public function ajaxProcessDeactivateCashonDelivery()
    {
        $module = Module::getInstanceByName('seurcashondelivery');
        if (Validate::isLoadedObject($module)) {
            if ($module->getPermission('configure')) {
                $module->disable();
            }
        }
        return true;
    }



    public function proccessAjax()
    {
        $context = Context::getContext();
        $out = array();

        if (Tools::getValue('action', 0) == 'setActiveCarrier') {

            $carrier_reference = Tools::getValue('carrier_reference', 0);
            $sql = "SELECT active FROM `"._DB_PREFIX_."carrier` WHERE id_reference=".(int)$carrier_reference." AND deleted=0" ;

            $active = (int)Db::getInstance()->getValue($sql);


            $out['result'] = 'OK';
            $out['response'] = $active;
        }

        return Tools::jsonEncode($out);
    }


    public function isConfigured()
    {
        return count($this->errorConfigure())==0;
    }


    public function errorConfigure()
    {
        $sql = "SELECT * FROM `"._DB_PREFIX_."seur2_ccc` ORDER BY id_seur_ccc";

        try {
            $ccc = Db::getInstance()->getRow($sql);
        }
        catch(Exception $e){
            return array($this->l("Module not installed"));
        }

        $error = array();
        if (Configuration::get('SEUR2_MERCHANT_NIF_DNI')=='') {
            $error[] = $this->l('Field')." ".$this->l('NIF/DNI')." ".$this->l('is not defined');
        }
        if (Configuration::get('SEUR2_MERCHANT_FIRSTNAME')=='') {
            $error[] = $this->l('Field')." ".$this->l('FIRSTNAME')." ".$this->l('is not defined');
        }
        if (Configuration::get('SEUR2_MERCHANT_LASTNAME')=='') {
            $error[] = $this->l('Field')." ".$this->l('LASTNAME')." ".$this->l('is not defined');
        }
        if (Configuration::get('SEUR2_MERCHANT_COMPANY')=='') {
            $error[] = $this->l('Field')." ".$this->l("COMPANY")." ".$this->l('is not defined');
        }



        if ($ccc['id_seur_ccc']==''){
            $error[] = $this->l('Not setup any count ccc yet');
        }
        if ($ccc['ccc']==''){
            $error[] = $this->l('Field')." ".$this->l('CCC')." ".$this->l('is not defined');
        }
        if ($ccc['cit']==''){
            $error[] = $this->l('Field')." ".$this->l('CIT')." ".$this->l('is not defined');
        }
        if ($ccc['franchise']==''){
            $error[] = $this->l('Field')." ".$this->l('FRANCHISE')." ".$this->l('is not defined');
        }
        if ($ccc['phone']==''){
            $error[] = $this->l('Field')." ".$this->l('PHONE')." ".$this->l('is not defined');
        }
        if ($ccc['email']==''){
            $error[] = $this->l('Field')." ".$this->l('EMAIL')." ".$this->l('is not defined');
        }


        if ($ccc['street_type']=='') {
            $error[] = $this->l('Field')." ".$this->l('STREET TYPE')." ".$this->l('is not defined');
        }
        if ($ccc['street_name']=='') {
            $error[] = $this->l('Field')." ".$this->l('STREET NAME')." ".$this->l('is not defined');
        }
        if ($ccc['post_code']=='') {
            $error[] = $this->l('Field')." ".$this->l('POST CODE')." ".$this->l('is not defined');
        }
        if ($ccc['town']=='') {
            $error[] = $this->l('Field')." ".$this->l('CITY')." ".$this->l('is not defined');
        }
        if ($ccc['state']=='') {
            $error[] = $this->l('Field')." ".$this->l('STATE')." ".$this->l('is not defined');
        }
        if ($ccc['country']=='') {
            $error[] = $this->l('Field')." ".$this->l('COUNTRY')." ".$this->l('is not defined');
        }
        if ($ccc['street_number']=='') {
            $error[] = $this->l('Field')." ".$this->l('NUMBER')." ".$this->l('is not defined');
        }


        if (Configuration::get('SEUR2_WS_USERNAME')=='') {
            $error[] = $this->l('Field')." ".$this->l('USER WS')." ".$this->l('is not defined');
        }
        if (Configuration::get('SEUR2_WS_PASSWORD')=='') {
            $error[] = $this->l('Field')." ".$this->l('PASSWORD WS')." ".$this->l('is not defined');
        }
/*
        if (Configuration::get('SEUR2_MERCHANT_CLICKCOLLECT')==''){
            $error[] = $this->l('Field')." ".$this->l('CLICK COLLECT')." ".$this->l('is not defined');
        }
*/
        return $error;
    }

    public function hookAdminOrder($params)
    {
        if ($this->isConfigured()) {
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
            $order = new Order((int)$params['id_order']);

            $carrier = new Carrier($order->id_carrier);

/*
            $sql = 'SELECT o.*, a.name as sender, a.email as supplier_email, o.status_shipping, o.status_collection, sc.name as status_collection_name, sc.upgradeable as status_collection_upgradeable, sc.color as status_collection_color, sc.status_code as status_collection_code, ss.name as status_shipping_name, ss.upgradeable  as status_shipping_upgradeable, ss.color as status_shipping_color, ss.status_code as status_shipping_code
				FROM `' . _DB_PREFIX_ . 'zeleris_orders` o
				LEFT JOIN `' . _DB_PREFIX_ . 'zeleris_address` a ON a.id_zeleris_address=o.id_order_address_origen
				LEFT JOIN `' . _DB_PREFIX_ . 'zeleris_status` sc ON sc.id_zeleris_status=o.status_collection
				LEFT JOIN `' . _DB_PREFIX_ . 'zeleris_status` ss ON ss.id_zeleris_status=o.status_shipping
				WHERE o.id_order = ' . (int)$params['id_order'];
*/

            $sql = 'SELECT o.*, ss.*
				FROM `' . _DB_PREFIX_ . 'seur2_order` o 
                LEFT JOIN `' . _DB_PREFIX_ . 'seur2_status` ss ON ss.id_status = o.id_status
				WHERE o.id_order = ' . (int)$params['id_order'];

            $seur_order = Db::getInstance()->getRow($sql);

            if ($seur_order!=NULL) {

                $grupo = $seur_order['grupo'];

                $texto_grupo = "Pendiente";
                switch ($grupo) {
                    case "ENTREGADO":
                        $texto_grupo = "<span class='badge badge-success'>".$seur_order['status_text']."</span>";
                        break;
                    case "APORTAR SOLUCIÓN":
                    case "INCIDENCIA":
                        $texto_grupo = "<span class='badge badge-danger'>".$seur_order['status_text']."</span>";
                        break;
                    case "EN TRÁNSITO":
                    case "DISPONIBLE PARA RECOGER EN TIENDA":
                        $texto_grupo = "<span class='badge badge-warning'>".$seur_order['status_text']."</span>";
                        break;
                }

                $num_bultos = (int)$seur_order['numero_bultos'];
                $peso = (int)$seur_order['peso_bultos'];
                $ecb = $seur_order['ecb'];

                if($num_bultos == 0 || $seur_order['product']==77)
                    $num_bultos = 1;

                if($peso == 0)
                    $peso = 1;

                $gastos_envio = $order->total_shipping;
                $referencia = $order->reference;

                $fecha = $seur_order['date_labeled'];

                $url_tracking = "https://www.seur.com/livetracking/pages/seguimiento-online.do?segOnlineIdentificador=".$referencia."&segOnlineFecha=".substr($fecha,8,2)."-".substr($fecha,5,2)."-".substr($fecha,0,4);

                $this->context->smarty->assign('shippings', $seur_order);
                $url = $this->context->link->getAdminLink('AdminSeurShipping',true);
                $this->context->smarty->assign('urlmodule', $url);

                $tracking = $seur_order['ecb'];

                $firstname = $seur_order['firstname'];
                $lastname = $seur_order['lastname'];
                $phone = $seur_order['phone'];
                $phone_mobile = $seur_order['phone_mobile'];
                $dni = $seur_order['dni'];
                $address1 = $seur_order['address1'];
                $address2 = $seur_order['address2'];
                $postcode = $seur_order['postcode'];
                $city = $seur_order['city'];
                $id_country = $seur_order['id_country'];
                $id_state = $seur_order['id_state'];
                $other = $seur_order['other'];
                $countries = Country::getCountries(Context::getContext()->language->id);
                $states = State::getStatesByIdCountry($id_country);

                $iso_country = Country::getIsoById((int)$id_country);

                if ($iso_country == 'ES' || $iso_country == 'PT' || $iso_country == 'AD') {
                    //$id_seur_ccc = $seur_order['id_seur_ccc'];
                    $id_seur_ccc = SeurLib::cccNoGeoLabel();
                }else{
                    if(SeurLib::cccAnyGeoLabel()){
                        $id_seur_ccc = SeurLib::cccAnyGeoLabel();
                    }else{
                        //$id_seur_ccc = $seur_order['id_seur_ccc'];
                        $id_seur_ccc = SeurLib::cccNoGeoLabel();
                    }
                }

                $this->context->smarty->assign('estado', $texto_grupo);
                $this->context->smarty->assign('num_bultos', $num_bultos);
                $this->context->smarty->assign('peso', $peso);
                $this->context->smarty->assign('gastos_envio', $gastos_envio);
                $this->context->smarty->assign('tracking', $tracking);
                $this->context->smarty->assign('url_tracking', $url_tracking);
                $this->context->smarty->assign('labeled', $seur_order['labeled']);
                $this->context->smarty->assign('classic', (int)($seur_order['service']==77));

                $this->context->smarty->assign('firstname', $firstname);
                $this->context->smarty->assign('lastname', $lastname);
                $this->context->smarty->assign('phone', $phone);
                $this->context->smarty->assign('phone_mobile', $phone_mobile);
                $this->context->smarty->assign('dni', $dni);
                $this->context->smarty->assign('address1', $address1);
                $this->context->smarty->assign('address2', $address2);
                $this->context->smarty->assign('postcode', $postcode);
                $this->context->smarty->assign('city', $city);
                $this->context->smarty->assign('id_country', $id_country);
                $this->context->smarty->assign('id_state', $id_state);
                $this->context->smarty->assign('other', $other);
                $this->context->smarty->assign('states', $states);
                $this->context->smarty->assign('countries', $countries);

                $this->context->smarty->assign('list_ccc', SeurLib::getListCCC());
                $this->context->smarty->assign('id_seur_ccc', $id_seur_ccc);

                $this->context->smarty->assign('print_label', Context::getContext()->link->getAdminLink('AdminSeurShipping')."&action=print_label&id_order=".$seur_order['id_seur_order']);
                $this->context->smarty->assign('url_edit_order', Context::getContext()->link->getAdminLink('AdminSeurShipping')."&action=edit_order&id_order=".$seur_order['id_order']);

                return $this->display(__FILE__, 'views/templates/admin/order_data.tpl');
            } else if(Configuration::get('SEUR2_CAPTURE_ORDER')) {
                $url = $this->context->link->getAdminLink('AdminSeurShipping',
                        true) . '&AddNewOrder=' . (int)$params['id_order'];

                $urlCarrier = $this->context->link->getAdminLink('SeurCarrier',
                        true);


                $carriers = SeurCarrier::getSeurCarriers();
                $this->context->smarty->assign('carriers', $carriers);
                $this->context->smarty->assign('urlmodule', $url);
                $this->context->smarty->assign('urlcarrier', $urlCarrier);

                return $this->display(__FILE__, 'views/templates/admin/order_new_shipping.tpl');
            }
        }
    }

    public function ajaxProcessUpdateShippings()
    {
        $orders = SeurOrder::getUpdatableOrders();


        foreach($orders as $id_order)
        {
            $order = new Order($id_order);

            /* Consultar estado */


            $state = SeurExpedition::getExpeditions(array('reference_number' =>$order->reference));

            $is_empty_state = false;
            $xml_s = false;
            if (empty($state->out))
                $is_empty_state = true;
            else {
                $string_xml = htmlspecialchars_decode($state->out);
                $string_xml = str_replace('&', '&amp; ', $string_xml);
                $xml_s = simplexml_load_string($string_xml);

                if (!$xml_s->EXPEDICION)
                    $is_empty_state = true;
                else{
                    $expedicion = $xml_s->EXPEDICION;
                    $cod_situacion = $expedicion->COD_SITUACION;
                    preg_match('/(.)-(.)([0-9]*)/', $cod_situacion, $matches, PREG_OFFSET_CAPTURE);

                    if(count($matches) == 4)
                    {
                        $tipo_situ = $matches[2][0];
                        $cod_situ = $matches[3][0];

                        $status = SeurOrder::getStatusExpedition($tipo_situ,(int)$cod_situ);
                        try {

                            $order_seur = SeurOrder::getByOrder($id_order);
                            $order_seur->setCurrentStatus($status['id_status'], $expedicion->DESCRIPCION_PARA_CLIENTE);
                        }
                        catch (Exception $e){
                            echo "Error al actualizar estado pedido ".$id_order."<br/>";
                        }
                    }
                }
            }
        }

        return true;
    }

}
