<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class TdkSlideshowStatus extends Module
{
    private static $instance = null;

    public static function getInstance()
    {
        if (self::$instance == null) {
            # module validation
            self::$instance = new TdkSlideshowStatus();
        }
        return self::$instance;
    }
    const SLIDER_TARGET_SAME = 'same';
    const SLIDER_TARGET_NEW = 'new';

    public function getSliderTargetOption()
    {
        return array(
            array('id' => self::SLIDER_TARGET_SAME, 'name' => $this->l('Same Window')),
            array('id' => self::SLIDER_TARGET_NEW, 'name' => $this->l('New Window')),
        );
    }
    const SLIDER_STATUS_DISABLE = '0';
    const SLIDER_STATUS_ENABLE = '1';
    const SLIDER_STATUS_COMING = '2';
    const GROUP_STATUS_DISABLE = '0';
    const GROUP_STATUS_ENABLE = '1';
}
