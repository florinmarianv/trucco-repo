<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class TdkSlideshowGroup extends ObjectModel
{

    public $title;
    public $active;
    public $hook;
    public $id_shop;
    public $params;
    public $active_pf;
    public $data = array();
    public $randkey;
    public $tdk_module = null;

    /**
     * @see ObjectModel::$definition
     */
    public function setModule($module)
    {
        $this->tdk_module = $module;
    }

    public static $definition = array(
        'table' => 'tdkslideshow_groups',
        'primary' => 'id_tdkslideshow_groups',
        'fields' => array(
            'title' => array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 255),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'hook' => array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isCleanHtml', 'size' => 64),
            'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
            'params' => array('type' => self::TYPE_HTML, 'lang' => false),
            'active_pf' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'randkey' => array('type' => self::TYPE_STRING, 'lang' => false, 'size' => 255),
        )
    );

    public function add($autodate = true, $null_values = false)
    {
        $res = parent::add($autodate, $null_values);

        return $res;
    }

    public static function groupExists($id_group)
    {
        $req = 'SELECT gr.`id_tdkslideshow_groups` as id_group
                FROM `' . _DB_PREFIX_ . 'tdkslideshow_groups` gr
                WHERE gr.`id_tdkslideshow_groups` = ' . (int) $id_group;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($req);
        return ($row);
    }

    public function getGroups($active, $id_shop)
    {
        $this->context = Context::getContext();
        if (!isset($id_shop)) {
            $id_shop = $this->context->shop->id;
        }
        $req = 'SELECT * FROM `' . _DB_PREFIX_ . 'tdkslideshow' . '_groups` gr
                WHERE (`id_shop` = ' . (int) $id_shop . ')' . ($active ? ' AND gr.`active` = 1' : ' ');
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($req);
    }

    /**
     * Delele all Slides follow id_group
     */
    public static function deleteAllSlider($id_group)
    {
        $sql = 'SELECT sl.`id_tdkslideshow_slides` as id
                FROM `' . _DB_PREFIX_ . 'tdkslideshow_slides` sl
                WHERE sl.`id_group` = ' . (int) $id_group;
        $sliders = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if ($sliders) {
            $where = '';
            foreach ($sliders as $sli) {
                # module validation
                $where .= $where ? ',' . (int) $sli['id'] : (int) $sli['id'];
            }

            # SLIDE
            $sql = 'DELETE FROM `' . _DB_PREFIX_ . 'tdkslideshow_slides` '
                    . 'WHERE `id_tdkslideshow_slides` IN (' . $where . ')';
            Db::getInstance()->execute($sql);

            # SLIDE_LANG
            $sql = 'DELETE FROM `' . _DB_PREFIX_ . 'tdkslideshow_slides_lang` '
                    . 'WHERE `id_tdkslideshow_slides` IN (' . $where . ')';
            Db::getInstance()->execute($sql);
        }
    }

    public function delete()
    {
        $res = parent::delete();

        if ($res) {
            $this->deleteAllSlider((int) $this->id);
        }

        return $res;
    }

    /**
     * Get and validate StartWithSlide field.
     */
    public static function showStartWithSlide($start_with_slide = 0, $slider = array())
    {
        $result = 1;
        if (is_array($slider)) {
            $start_with_slide = (int) $start_with_slide;
            $slider_num = count($slider);
            // 1 <= $start_with_slide <= $slider_num
            if (1 <= $start_with_slide && $start_with_slide <= $slider_num) {
                $result = $start_with_slide;
            }
        }

        $result--; // index begin from 0
        return $result;
    }

    public function getDelay()
    {
        $temp_result = Tools::jsonDecode(TdkSlideshowSlide::base64Decode($this->params), true);
        $result = $temp_result['delay'];

        return $result;
    }

    public static function getGroupOption()
    {
        $result = array();
        $mod_group = new TdkSlideshowGroup();
        $groups = $mod_group->getGroups(null, null);

        foreach ($groups as $group) {
            $temp = array();
            $temp['id'] = $group['id_tdkslideshow_groups'];
            $temp['name'] = $group['title'];
            $result[] = $temp;
        }
        return $result;
    }

    /**
     * Get group to frontend
     */
    public static function getActiveGroupByHook($hook_name = '', $active = 1)
    {
        $id_shop = Context::getContext()->shop->id;
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'tdkslideshow_groups gr
                WHERE gr.id_shop = ' . (int) $id_shop . ' AND gr.hook = "' . pSQL($hook_name) . '"' . ($active ? ' AND gr.`active` = 1' : ' ') . '
                ORDER BY gr.id_tdkslideshow_groups';

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
    }

    /**
     * Get group to preview
     */
    public static function getGroupByID($id_group)
    {
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'tdkslideshow_groups gr
                WHERE gr.id_tdkslideshow_groups = ' . (int) $id_group;

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
    }

    public function setData($data = array())
    {
        if (is_array($data)) {
            $this->data = $data;
        }
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function beforeLoad()
    {
        $data = $this->data;

        #Timer
        if ($data['timer_show'] == TdkSlideshowConfig::TIMER_HIDE_AUTO) {
            $data['autoAdvance'] = 1;
        } elseif ($data['timer_show'] == TdkSlideshowConfig::TIMER_HIDE_STOP) {
            $data['autoAdvance'] = 0;
        } elseif ($data['timer_show'] == TdkSlideshowConfig::TIMER_SHOW_AUTO) {
            $data['autoAdvance'] = 1;
        } elseif ($data['timer_show'] == TdkSlideshowConfig::TIMER_SHOW_STOP) {
            $data['autoAdvance'] = 0;
        }

        # Navigator
        if ($data['navigator_type'] == TdkSlideshowConfig::IVIEW_NAV_THUMBNAIL) {
            $data['controlNavThumbs'] = 1;
        } elseif ($data['navigator_type'] == TdkSlideshowConfig::IVIEW_NAV_BULLET) {
            $data['controlNavThumbs'] = 0;
        }

        # Multi language
        $data['playLabel'] = $this->tdk_module->l('Play');
        $data['pauseLabel'] = $this->tdk_module->l('Pause');
        $data['closeLabel'] = $this->tdk_module->l('Close');
        $data['nextLabel'] = $this->tdk_module->l('Next');
        $data['previousLabel'] = $this->tdk_module->l('Previous');

        $this->data = $data;
        return $this;
    }

    public function loadFrontEnd()
    {
        return $this->getData();
    }

    public function count()
    {
        $sql = 'SELECT id_tdkslideshow_groups FROM ' . _DB_PREFIX_ . 'tdkslideshow_groups';
        $groups = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        $number_groups = count($groups);
        return $number_groups;
    }

    public static function autoCreateKey()
    {
        $sql = 'SELECT ' . self::$definition['primary'] . ' FROM ' . _DB_PREFIX_ . self::$definition['table'] .
                ' WHERE randkey IS NULL OR randkey = ""';

        $rows = Db::getInstance()->executes($sql);
        foreach ($rows as $row) {
            $mod_group = new TdkSlideshowGroup((int) $row[self::$definition['primary']]);
            include_once(_PS_MODULE_DIR_ . 'tdkslideshow/libs/Helper.php');
            $mod_group->randkey = TdkSlideshowHelper::genKey();
            $mod_group->update();
        }
    }
}
