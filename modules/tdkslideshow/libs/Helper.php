<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
* 
**/

if (!class_exists('TdkSlideshowHelper')) {

    class TdkSlideshowHelper
    {
        const MODULE_NAME = 'tdkslideshow';

        public static function l($string, $specific = false, $name = '')
        {
            if (empty($name)) {
                $name = self::MODULE_NAME;
            }
            return Translate::getModuleTranslation($name, $string, ($specific) ? $specific : $name);
        }

        /**
         * Copy js, css to theme folder
         */
        public static function copyToTheme()
        {
            include_once(_PS_MODULE_DIR_.'tdkslideshow/libs/phpcopy.php');

            $theme_dir = _PS_ROOT_DIR_.'/themes/'._THEME_NAME_.'/';
            $module_dir = _PS_MODULE_DIR_.self::MODULE_NAME.'/';

            $theme_js_dir = $theme_dir.'modules/tdkslideshow/views/js';
            $theme_css_dir = $theme_dir.'modules/tdkslideshow/views/css';

            // Create js folder
            mkdir($theme_js_dir, 0755, true);
            PhpCopy::safeCopy($module_dir.'views/js/', $theme_js_dir);

            // Create css folder
            mkdir($theme_css_dir, 0755, true);
            PhpCopy::safeCopy($module_dir.'views/css/', $theme_css_dir);

            $url = 'index.php?controller=adminmodules&configure=tdkslideshow&token='.Tools::getAdminTokenLite('AdminModules')
                    .'&tab_module=front_office_features&module_name=tdkslideshow';
            Tools::redirectAdmin($url);
        }
        
        public static function getImgThemeUrl()
        {
            # TdkSlideshowHelper::getImgThemeUrl()
            static $img_theme_url;
            if (!$img_theme_url) {
                // Not exit image or icon
                // $img_theme_url = _THEME_IMG_DIR_.'modules/tdkslideshow/';
				$img_theme_url = _THEME_DIR_.'assets/img/modules/tdkslideshow/images/';
            }

            return $img_theme_url;
        }
    
        public static function getImgThemeDir()
        {
            static $img_theme_dir;
            if (!$img_theme_dir) {
                $img_theme_dir = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/assets/img/modules/tdkslideshow/images/';
            }
            return $img_theme_dir;
        }
        
        public static function genKey()
        {
            return md5(time().rand());
        }
    }
}
