{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you cant resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{extends file=$layout}
{block name='content'}
	{if $tdkslideshow_tpl == 1}
		{include file='./tdkslideshow.tpl'}
	{else}
		{include file='module:tdkslideshow/views/templates/front/tdkslideshow.tpl'}
	{/if}
{/block}
