{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you cant resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<div class="alert alert-warning tdk-lib-error">{l s='Can not show TdkSlideShow in CMS page. Please check that The Group of TdkSlideShow is exist.' mod='tdkslideshow'}</div>
