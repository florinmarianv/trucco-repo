{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you cant resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<div class="bootstrap" id="tdk_error">
    <div class="module_error alert alert-danger" >
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		{l s='I am so sorry, you are using free version of Tdk slideshow module, could you please update module to pro version to use this function' mod='tdkslideshow'}
		<br/>
		<a href="#" target="_blank">{l s='Click here to buy Tdk Slideshow Prestashop Module' mod='tdkslideshow'}</a>
	</div>
</div>
        