<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class AdminTdkSlideshowMenuModuleController extends ModuleAdminControllerCore
{

    public function __construct()
    {
        parent::__construct();
        if (Configuration::get('TDKSLIDESHOW_GROUP_DE') && Configuration::get('TDKSLIDESHOW_GROUP_DE') != '') {
            $url = 'index.php?controller=adminmodules&configure=tdkslideshow&editgroup=1&id_group='.Configuration::get('TDKSLIDESHOW_GROUP_DE').'&tab_module=front_office_features&module_name=tdkslideshow&token='.Tools::getAdminTokenLite('AdminModules');
        } else {
            $url = 'index.php?controller=adminmodules&configure=tdkslideshow&tab_module=front_office_features&module_name=tdkslideshow&token='.Tools::getAdminTokenLite('AdminModules');
        }
        Tools::redirectAdmin($url);
    }
}
