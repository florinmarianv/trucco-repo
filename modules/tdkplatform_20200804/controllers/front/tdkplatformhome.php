<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

class TdkPlatformTdkPlatformHomeModuleFrontController extends FrontController
{
    public function __construct()
    {
        parent::__construct();
        $this->display_column_left  = false;
        $this->display_column_right = false;
    }

    public function initContent()
    {
        parent::initContent();
        $this->addJS(_THEME_JS_DIR_.'index.js');

        $this->context->smarty->assign(array(
            'HOOK_HOME' => Hook::exec('displayHome'),
            'HOOK_HOME_TAB' => Hook::exec('displayHomeTab'),
            'HOOK_HOME_TAB_CONTENT' => Hook::exec('displayHomeTabContent')
        ));
        $this->display_column_left = false;
        $this->display_column_right = false;
        $this->context->smarty->assign(array(
            'page_name'           => 'index',
        ));
        $this->setTemplate('index.tpl');
    }

    /**
     * set html <body id="index"
     */
    public function getPageName()
    {
        $page_name = 'index';
        return $page_name;
    }
    
    public function getTemplateVarPage()
    {
        $page = parent::getTemplateVarPage();
        unset($page['body_classes']['page-']);
        $page['body_classes']['page-index'] = true;
        return $page;
    }
}
