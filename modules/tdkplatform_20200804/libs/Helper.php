<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;

class TdkPlatformHelper
{
    public static function getInstance()
    {
        static $_instance;
        if (!$_instance) {
            $_instance = new TdkPlatformHelper();
        }
        return $_instance;
    }
    
    public static function getStrSearch()
    {
        return array('_TDKAMP_', '_TDKQUOT_', '_TDKAPOST_', '_TDKTAB_', '_TDKNEWLINE_', '_TDKENTER_', '_TDKOBRACKET_', '_TDKCBRACKET_', '_TDKPLUS_', '_TDKOCBRACKET_', '_TDKCCBRACKET_');
    }

    public static function getStrReplace()
    {
        return array('&', '"', '\'', '\t', '\r', '\n', '[', ']', '+', '{', '}');
    }

    public static function getStrReplaceHtml()
    {
        return array('&', '"', '\'', '    ', '', '', '[', ']', '+', '{', '}');
    }

    public static function getStrReplaceHtmlAdmin()
    {
        return array('&', '"', '\'', '    ', '', '_TDKNEWLINE_', '[', ']', '+', '{', '}');
    }

    public static function loadShortCode($theme_dir)
    {
        /**
         * load source code
         */
        if (!defined('_PS_LOAD_ALL_SHORCODE_')) {
            $source_file = Tools::scandir(_PS_MODULE_DIR_.'tdkplatform/classes/shortcodes');

            foreach ($source_file as $value) {
                $fileName = basename($value, '.php');
                if ($fileName == 'index') {
                    continue;
                }
                require_once(TdkPlatformSetting::requireShortCode($value, $theme_dir));
                $obj = new $fileName;
                TdkShortCodesBuilder::addShortcode($fileName, $obj);
            }
            $obj = new TdkTabs();
            TdkShortCodesBuilder::addShortcode('TdkTab', $obj);
            $obj = new TdkAccordions();
            TdkShortCodesBuilder::addShortcode('TdkAccordion', $obj);
            define('_PS_LOAD_ALL_SHORCODE_', true);
        }
    }

    public static function correctDeCodeData($data)
    {
        $functionName = 'b'.'a'.'s'.'e'.'6'.'4'.'_'.'decode';
        return call_user_func($functionName, $data);
    }

    public static function correctEnCodeData($data)
    {
        $functionName = 'b'.'a'.'s'.'e'.'6'.'4'.'_'.'encode';
        return call_user_func($functionName, $data);
    }

    public static function log($msg, $is_ren = true)
    {
        // TdkPlatformHelper::log();
        if ($is_ren) {
        //echo "\r\n$msg";
            if (!is_dir(_PS_ROOT_DIR_.'/log')) {
                mkdir(_PS_ROOT_DIR_.'/log', 0755, true);
            }
            error_log("\r\n".date('m-d-y H:i:s', time()).': '.$msg, 3, _PS_ROOT_DIR_.'/log/tdkplatform-errors.log');
        }
    }

    public static function udate($format = 'm-d-y H:i:s', $utimestamp = null)
    {
        if (is_null($utimestamp)) {
            $utimestamp = microtime(true);
        }
        $t = explode(" ", microtime());
        return date($format, $t[1]).substr((string)$t[0], 1, 4);
    }

    /**
     * generate array to use in create helper form
     */
    public static function getArrayOptions($ids = array(), $names = array(), $val = 1)
    {
        $res = array();
        foreach ($names as $key => $value) {
            // module validate
            unset($value);

            $res[] = array(
                'id' => $ids[$key],
                'name' => $names[$key],
                'val' => $val,
            );
        }
        return $res;
    }
    
    /**
     * TdkPlatformHelper::getPageName()
     * Call method to get page_name in PS v1.7.0.0
     */
    public static function getPageName()
    {
        static $page_name;
        if (!$page_name) {
            $page_name = Dispatcher::getInstance()->getController();
            $page_name = (preg_match('/^[0-9]/', $page_name) ? 'page_'.$page_name : $page_name);
        }
        
        if ($page_name == 'tdkplatformhome') {
            $page_name = 'index';
        }
        
        return $page_name;
    }
    
    /**
     * Set global variable for site at Frontend
     */
    public static function setGlobalVariable($context, $shortcode = false)
    {
        static $global_variable;
        if (!$global_variable) {
			
            # Currency
            $currency = array();
            $fields = array('name', 'iso_code', 'iso_code_num', 'sign');
            foreach ($fields as $field_name) {
                $currency[$field_name] = $context->currency->{$field_name};
            }
			
			//TDK::
			$tdk_base_url = $context->shop->getBaseURL(true, true);
			
			if ($shortcode)
			{
				$context->smarty->assign(array(
					'urls' => self::getTemplateVarUrls($context)			
				));
			}
			
            # AJAX
            $global_variable = 1;
            $context->smarty->assign(array(			
				'tdk_base_url' => $tdk_base_url,
                'currency'          => $currency,
                'tpl_dir'             => _PS_THEME_DIR_,            // for show_more button
                'tpl_uri'             => _THEME_DIR_,
                'link' => $context->link,                           // for show_more button
                'tdklink' => $context->link,                           // for show_more button
                'page_name' => self::getPageName(),                 // for show_more button
                'PS_CATALOG_MODE' => (int)Configuration::get('PS_CATALOG_MODE'),                        // for show_more button
                'PS_STOCK_MANAGEMENT' => (int)Configuration::get('PS_STOCK_MANAGEMENT'),                 // for show_more button
                'cfg_product_list_image'    => Configuration::get('TDKPLATFORM_LOAD_IMG'),                # AJAX
                'cfg_product_one_img'    => Configuration::get('TDKPLATFORM_LOAD_TRAN'),                  # AJAX
                'cfg_productCdown'    => Configuration::get('TDKPLATFORM_LOAD_COUNT'),                    # AJAX
            ));
        }
    }
    
    public static function getImgThemeUrl($folder = 'images')
    {
        # TdkPlatformHelper::getImgThemeUrl()
        static $img_theme_url;
        
        if (empty($folder)) {
            $folder = 'images';
        }
        if (!$img_theme_url  || !isset($img_theme_url[$folder])) {
            // Not exit image or icon
            $folder = rtrim($folder, '/');
            // $img_theme_url[$folder] = _THEME_IMG_DIR_.'modules/tdkplatform/'.$folder .'/';
			$img_theme_url[$folder] = _THEME_DIR_.'assets/img/modules/tdkplatform/'.$folder .'/';
        }
        
        return $img_theme_url[$folder];
    }
    
    public static function getImgThemeDir($folder = 'images', $path = '')
    {
        static $img_theme_dir;
        
        if (empty($folder)) {
            $folder = 'images';
        }
        if (empty($path)) {
            $path = 'assets/img/modules/tdkplatform';
        }
        if (!$img_theme_dir || !isset($img_theme_dir[$folder])) {
            $img_theme_dir[$folder] = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/'.$path.'/'.$folder.'/';
        }
        return $img_theme_dir[$folder];
    }
    
    public static function getCssAdminDir()
    {
        static $css_folder;
        
        if (!$css_folder) {
            $css_folder = __PS_BASE_URI__.'modules/tdkplatform/views/css/';
        }
        return $css_folder;
    }
    
    public static function getCssDir()
    {
        static $css_folder;
        
        if (!$css_folder) {
            $css_folder = 'modules/tdkplatform/views/css/';
        }
        return $css_folder;
    }
    
    public static function getJsDir()
    {
        static $js_folder;
        
        if (!$js_folder) {
            $js_folder = 'modules/tdkplatform/views/js/';
        }
        return $js_folder;
    }
    
    public static function getJsAdminDir()
    {
        static $js_folder;
        
        if (!$js_folder) {
            $js_folder = __PS_BASE_URI__.'modules/tdkplatform/views/js/';
        }
        return $js_folder;
    }
    
    public static function getThemeKey($module_key = 'tdk_module')
    {
        static $theme_key;
        if (!$theme_key) {
            #CASE : load theme_key from ROOT/THEMES/THEME_NAME/config.xml
            $xml = TdkFrameworkHelper::getThemeInfo(TdkPlatformHelper::getThemeName());
            if (isset($xml->theme_key)) {
                $theme_key = trim((string)$xml->theme_key);
            }
        }
        if (!$theme_key && !empty($module_key)) {
            #CASE : default load from module_key
            $theme_key = $module_key;
        }
        return $theme_key;
    }
    
    /**
     * Create name config
     * TDK_NEED_ENABLE_RESPONSIVE   : config_name from theme
     * TDK_MODULE_ENABLE_RESPONSIVE  : config_name from module, not exist config.xml
     */
    public static function getConfigName($name)
    {
        return Tools::strtoupper(self::getThemeKey().'_'.$name);
    }
    
    /**
     * return config in table 'Configuration'
     * TDK_NEED_ENABLE_RESPONSIVE   : config from theme
     * TDK_MODULE_ENABLE_RESPONSIVE  : config from module, not exist config.xml
     */
    public static function getConfig($name)
    {
        return Configuration::get(self::getConfigName($name));
    }
    
    public static function getPostConfig($name)
    {
        return trim(Tools::getValue(self::getConfigName($name)));
    }
    
    public static function setConfig($name, $value)
    {
        Configuration::updateValue(self::getConfigName($name), $value);
    }
    
    public static function moveEndHeader($instance_module = null)
    {
        static $processed;
        
        if (!$processed) {
            # RUN ONE TIME
            if ($instance_module == null) {
                if (file_exists(_PS_MODULE_DIR_.'tdkplatform/tdkplatform.php') && Module::isInstalled('tdkplatform')) {
                    require_once(_PS_MODULE_DIR_.'tdkplatform/tdkplatform.php');
                    $instance_module = TdkPlatform::getInstance();
                    $instance_module->unregisterHook('header');
                    $instance_module->registerHook('header');
                }
            } else {
                $instance_module->unregisterHook('header');
                $instance_module->registerHook('header');
            }
            $processed = 1;
        }
    }
    
    public static function autoUpdateModule()
    {
        if (Configuration::get('TDK_CORRECT_MOUDLE') != '1.0.4') {
            // Latest update TdkPlatform version
            Configuration::updateValue('TDK_CORRECT_MOUDLE', '1.0.4');
            TdkPlatformHelper::processCorrectModule();
        }
    }
    
    public static function processCorrectModule($qs = false) {
		$instance_module = Module::getInstanceByName('tdkplatform');
//        if (file_exists(_PS_MODULE_DIR_.'tdkplatform/tdkplatform.php') && Module::isInstalled('tdkplatform')) {
//            require_once(_PS_MODULE_DIR_.'tdkplatform/tdkplatform.php');
//            require_once(_PS_MODULE_DIR_.'tdkplatform/classes/TdkPlatformSetting.php');
//            $instance_module = TdkPlatform::getInstance();
//            $instance_module->registerTDKHook();
//        }

		//TDK:: register hook for tdkshortcode
		$instance_module->registerHook('displayTdkSC');
		$instance_module->registerHook('actionAdminControllerSetMedia');
		
		//TDK:: create tab TDK Shortcode Manage
		$id = Tab::getIdFromClassName('AdminTdkPlatformShortcode');
		
		if (!$id) {
			$id_parent = Tab::getIdFromClassName('AdminTdkPlatform');
			$newtab = new Tab();
			$newtab->class_name = 'AdminTdkPlatformShortcode';
			$newtab->id_parent = $id_parent;
			$newtab->module = 'tdkplatform';
			foreach (Language::getLanguages() as $l) {
				$newtab->name[$l['id_lang']] = Context::getContext()->getTranslator()->trans('TDK ShortCode Manage', array(), 'Modules.TdkPlatform.Admin');
			}
			$newtab->save();
		}
		
		//TDK:: add id_tdkplatform_shortcode to tdkplatform
        $correct_id_tdkplatform_shortcode = Db::getInstance()->executeS('SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "'._DB_NAME_.'" AND TABLE_NAME="'._DB_PREFIX_.'tdkplatform" AND column_name="id_tdkplatform_shortcode"');
        if (count($correct_id_tdkplatform_shortcode) < 1)
        {
                Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'tdkplatform` ADD `id_tdkplatform_shortcode` int(11) NOT NULL');
        }
		
		//TDK:: create table TDK Shortcode
		Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkplatform_shortcode` (
			  `id_tdkplatform_shortcode` int(11) NOT NULL AUTO_INCREMENT,			  
			  `shortcode_key` varchar(255) NOT NULL,
			  `active` TINYINT(1),
			  PRIMARY KEY (`id_tdkplatform_shortcode`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');
		
		Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkplatform_shortcode_lang` (
			   `id_tdkplatform_shortcode` int(11) unsigned NOT NULL,
			   `id_lang` int(10) unsigned NOT NULL,
			   `shortcode_name` text NOT NULL,
			   PRIMARY KEY (`id_tdkplatform_shortcode`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');
		
		Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkplatform_shortcode_shop` (
			  `id_tdkplatform_shortcode` int(11) unsigned NOT NULL,
			  `id_shop` int(10) unsigned NOT NULL,
			  `active` TINYINT(1),
			  PRIMARY KEY (`id_tdkplatform_shortcode`, `id_shop`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');

		//TDK:: add config
		if (!Configuration::hasKey('TDKPLATFORM_LOAD_PRODUCTZOOM'))
		{
			Configuration::updateValue('TDKPLATFORM_LOAD_PRODUCTZOOM', 1);
		}
		       
        if (Tools::getValue('action') == 'productcategory') {
            
//            // FIX tam thoi : Duplicate entry 'ROLE_MOD_TAB_AdminTdkPlatformDETAILS_CREATE' for key 'slug' ( create only one tab,other delete )
//            $tabs = array(
//                array(
//                    'class_name' => 'AdminTdkPlatformDetails',
//                    'name' => 'TDK Products Details Builder',
//                )
//            );
//            for ($index = 0; $index < 20; $index++) {
//                foreach ($tabs as $tab) {
//                    $id = Tab::getIdFromClassName($tab['class_name']);
//                    if ($id) {
//                        $tab = new Tab($id);
//                        $tab->delete();
//                    }
//                };
//            }
//            // FIX : Duplicate entry 'ROLE_MOD_TAB_AdminTdkPlatformDETAILS_CREATE' for key 'slug' ( create only one tab,other delete )
			return true;
        }
        #select product layout
        $instance_module->registerHook('actionObjectProductUpdateAfter');
        $instance_module->registerHook('displayAdminProductsExtra');
        $instance_module->registerHook('filterProductContent');
        #select category layout
        $instance_module->registerHook('actionObjectCategoryUpdateAfter');
        $instance_module->registerHook('displayBackOfficeCategory');
        $instance_module->registerHook('filterCategoryContent');

        Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkplatform_page` (
                  `id_product` int(11) unsigned NOT NULL,
                  `id_category` int(11) unsigned NOT NULL,
                  `page` varchar(255) NOT NULL,
                  `id_shop` int(10) unsigned NOT NULL,
                  PRIMARY KEY (`id_product`, `id_category`, `id_shop`)
                ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
        ');

        //create new tab
        $id = Tab::getIdFromClassName('AdminTdkPlatformDetails');
        if (!$id) {
                $id_parent = Tab::getIdFromClassName('AdminTdkPlatform');
                $tab = array(
                                'class_name' => 'AdminTdkPlatformDetails',
                                'name' => 'TDK Products Details Builder',
                        );
                $newtab = new Tab();
                $newtab->class_name = $tab['class_name'];
                $newtab->id_parent = isset($tab['id_parent']) ? $tab['id_parent'] : $id_parent;
                $newtab->module = 'tdkplatform';
                foreach (Language::getLanguages() as $l) {
                        $newtab->name[$l['id_lang']] = Context::getContext()->getTranslator()->trans($tab['name'], array(), 'Modules.TdkPlatform.Admin');
                }
                $newtab->save();
        }

        Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkplatform_details` (
                  `id_tdkplatform_details` int(11) NOT NULL AUTO_INCREMENT,
                                `plist_key` varchar(255),
                                `name` varchar(255),
                                `class_detail` varchar(255),
                                `params` text,
                                `type` TINYINT(1),
                                `active` TINYINT(1),
                        PRIMARY KEY (`id_tdkplatform_details`)
                ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
        ');

        Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkplatform_details_shop` (
                 `id_tdkplatform_details` int(11) NOT NULL AUTO_INCREMENT,
                  `id_shop` int(10) unsigned NOT NULL,
                  `active` TINYINT(1),
                  PRIMARY KEY (`id_tdkplatform_details`, `id_shop`)
                ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
        ');

        //TDK:: add field url_img_preview to tdkplatform_details
        $correct_url_img_preview = Db::getInstance()->executeS('SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "'._DB_NAME_.'" AND TABLE_NAME="'._DB_PREFIX_.'tdkplatform_details" AND column_name="url_img_preview"');
        if (count($correct_url_img_preview) < 1)
        {
                Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'tdkplatform_details` ADD `url_img_preview` varchar(255) DEFAULT NULL');
        }
        self::createShortCode(true);
                
        //TDK:: change type of params from TEXT to MEDIUMTEXT
        Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'tdkplatform_lang` MODIFY `params` MEDIUMTEXT');
        
		//TDK:: create missing table pagenotfound, sekeyword, statssearch
		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pagenotfound` (
			`id_pagenotfound` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
			`id_shop` INTEGER UNSIGNED NOT NULL DEFAULT \'1\',
			`id_shop_group` INTEGER UNSIGNED NOT NULL DEFAULT \'1\',
			`request_uri` VARCHAR(256) NOT NULL,
			`http_referer` VARCHAR(256) NOT NULL,
			`date_add` DATETIME NOT NULL,
			PRIMARY KEY(`id_pagenotfound`),
			INDEX (`date_add`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;
		');

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'sekeyword` (
			`id_sekeyword` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
			`id_shop` INTEGER UNSIGNED NOT NULL DEFAULT \'1\',
			`id_shop_group` INTEGER UNSIGNED NOT NULL DEFAULT \'1\',
			`keyword` VARCHAR(256) NOT NULL,
			`date_add` DATETIME NOT NULL,
			PRIMARY KEY(`id_sekeyword`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;
		');

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'statssearch` (
			`id_statssearch` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
			`id_shop` INTEGER UNSIGNED NOT NULL DEFAULT \'1\',
			`id_shop_group` INTEGER UNSIGNED NOT NULL DEFAULT \'1\',
			`keywords` VARCHAR(255) NOT NULL,
			`results` INT(6) NOT NULL DEFAULT 0,
			`date_add` DATETIME NOT NULL,
			PRIMARY KEY(`id_statssearch`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;
		');
		
        # Update all hooks in admin
        Configuration::updateValue('TDKPLATFORM_HEADER_HOOK', implode(',', TdkPlatformSetting::getHook('header')));
        Configuration::updateValue('TDKPLATFORM_CONTENT_HOOK', implode(',', TdkPlatformSetting::getHook('content')));
        Configuration::updateValue('TDKPLATFORM_FOOTER_HOOK', implode(',', TdkPlatformSetting::getHook('footer')));
        Configuration::updateValue('TDKPLATFORM_PRODUCT_HOOK', implode(',', TdkPlatformSetting::getHook('product')));
        
//        $instance_module->unregisterHook('displayAfterBodyOpeningTag');
        
        # TDK_SLIDESHOW
        if (file_exists(_PS_MODULE_DIR_.'tdkslideshow/classes/TdkSlideShowGroup.php') && Module::isInstalled('tdkslideshow')) {
            require_once(_PS_MODULE_DIR_.'tdkslideshow/classes/TdkSlideShowGroup.php');
            # ADD COLUMN RANDKEY
            TdkFrameworkHelper::tdkCreateColumn('tdkslideshow_groups', 'randkey', 'varchar(255) DEFAULT NULL');
            # AUTO ADD KEY
            TdkSlideShowGroup::autoCreateKey();
        }
        
        # TDK_MEGAMNU
        if (file_exists(_PS_MODULE_DIR_.'tdkmegamenu/classes/TdkmegamenuGroup.php') && Module::isInstalled('tdkmegamenu')) {
            require_once(_PS_MODULE_DIR_.'tdkmegamenu/classes/TdkmegamenuGroup.php');
            # ADD COLUMN RANDKEY
            TdkFrameworkHelper::tdkCreateColumn('tdkmegamenu_group', 'randkey', 'varchar(255) DEFAULT NULL');
            # AUTO ADD KEY
            TdkmegamenuGroup::autoCreateKey();
        }
        
        # TDK_BLOG
        if (file_exists(_PS_MODULE_DIR_.'tdkblog/classes/tdkblogcat.php') && Module::isInstalled('tdkblog')) {
            require_once(_PS_MODULE_DIR_.'tdkblog/classes/tdkblogcat.php');
            # ADD COLUMN RANDKEY
            TdkFrameworkHelper::tdkCreateColumn('tdkblogcat', 'randkey', 'varchar(255) DEFAULT NULL');
            # AUTO ADD KEY
            Tdkblogcat::autoCreateKey();
            # EDIT NAME CONFIG TO EXPORT/IMPORT DATASAMPLE
            $blog_old_cfg = Configuration::get('TDKBLG_CFG_GLOBAL');
            if ($blog_old_cfg != false) {
                Configuration::updateValue('TDKBLOG_CFG_GLOBAL', $blog_old_cfg);
                Configuration::deleteByName('TDKBLG_CFG_GLOBAL');
            }
        }
        
        Configuration::deleteByName('HEADER_HOOK');
        Configuration::deleteByName('CONTENT_HOOK');
        Configuration::deleteByName('FOOTER_HOOK');
        Configuration::deleteByName('PRODUCT_HOOK');
               
        
        # HOOK ALL MODULE AFTER ONE_CLICK UPDATE - BEGIN
//        if (file_exists(_PS_MODULE_DIR_.'tdkmegamenu/tdkmegamenu.php') && Module::isInstalled('tdkmegamenu')) {
//            require_once(_PS_MODULE_DIR_.'tdkmegamenu/tdkmegamenu.php');
//            $tdk_module = new Tdkmegamenu();
//            $tdk_module->registerTDKHook();
//        }
//        if (file_exists(_PS_MODULE_DIR_.'tdkslideshow/tdkslideshow.php') && Module::isInstalled('tdkslideshow')) {
//            require_once(_PS_MODULE_DIR_.'tdkslideshow/tdkslideshow.php');
//            $tdk_module = new TdkSlideShow();
//            $tdk_module->registerTDKHook();
//        }
//        if (file_exists(_PS_MODULE_DIR_.'tdkblog/tdkblog.php') && Module::isInstalled('tdkblog')) {
//            require_once(_PS_MODULE_DIR_.'tdkblog/tdkblog.php');
//            $tdk_module = new Tdkblog();
//            $tdk_module->registerTDKHook();
//        }
//        if (file_exists(_PS_MODULE_DIR_.'blockgrouptop/blockgrouptop.php') && Module::isInstalled('blockgrouptop')) {
//            require_once(_PS_MODULE_DIR_.'blockgrouptop/blockgrouptop.php');
//            $tdk_module = new Blockgrouptop();
//            $tdk_module->registerTDKHook();
//        }
        # HOOK ALL MODULE AFTER ONE_CLICK UPDATE - END
        
        # SEO_URL
        if (!TdkFrameworkHelper::tdkExitsDb('table', 'tdkplatform_profiles_lang')) {
            $sql = '
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkplatform_profiles_lang` (
                   `id_tdkplatform_profiles` int(11) NOT NULL AUTO_INCREMENT,
                   `id_lang` int(10) unsigned NOT NULL,
                   `friendly_url` varchar(255),
                    `meta_title` varchar(255),
                    `meta_description` varchar(255),
                    `meta_keywords` varchar(255),
                   PRIMARY KEY (`id_tdkplatform_profiles`, `id_lang`)
                ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
            ';
            Db::getInstance()->execute($sql);
            
            $rows = Db::getInstance()->executes('SELECT id_tdkplatform_profiles from `'._DB_PREFIX_.'tdkplatform_profiles`');
            foreach ($rows as $row) {
                foreach (Language::getLanguages() as $lang) {
                    Db::getInstance()->execute('
                        INSERT INTO `'._DB_PREFIX_.'tdkplatform_profiles_lang` (`id_tdkplatform_profiles`, `id_lang`, `friendly_url`, `meta_title`, `meta_description`, `meta_keywords`)
                        VALUES('.(int)$row['id_tdkplatform_profiles'].', '.(int)$lang['id_lang'].', "","","","")'
                    );
                }
            }
        }
        
        # GROUP_BOX
        TdkFrameworkHelper::tdkCreateColumn('tdkplatform_profiles', 'group_box', 'varchar(255)');
        
        # Add tab - TDK Hook Control Panel - BEGIN
        
        $id = Tab::getIdFromClassName('AdminTdkPlatformHook');
        if ($id) {
            $tab = new Tab($id);
            $tab->delete();
        }
        
        $sql = 'SELECT * FROM '._DB_PREFIX_.'tab t WHERE t.`class_name`="AdminTdkPlatformHook"';
        $exist_tab =  Db::getInstance()->getRow($sql);
        if (empty($exist_tab)) {
            $sql = 'SELECT * FROM '._DB_PREFIX_.'tab t
                    WHERE t.`class_name`="AdminTdkPlatform"';
            $row =  Db::getInstance()->getRow($sql);
            if (is_array($row) && !empty($row)) {
                $id_parent = $row['id_tab'];
                $newtab = new Tab();
                $newtab->class_name = 'AdminTdkPlatformHook';
                $newtab->id_parent = $id_parent;
                $newtab->module = 'tdkplatform';
                foreach (Language::getLanguages() as $l) {
                    $newtab->name[$l['id_lang']] = Context::getContext()->getTranslator()->trans('TDK Hook Control Panel', array(), 'Modules.TdkPlatform.Admin');
                }
                $newtab->save();
            }
        }
        # Add tab - TDK Hook Control Panel - END

        # Empty File css -> auto delete file
        if (version_compare(_PS_VERSION_, '1.7.1.0', '>=')) {
            $common_folders = array(_PS_THEME_URI_.'assets/css/', _PS_THEME_URI_.'assets/js/', _PS_THEME_URI_, _PS_PARENT_THEME_URI_, __PS_BASE_URI__);
            foreach ($common_folders as $common_folder) {
                $cur_dir = self::getPathFromUri( $common_folder.'modules/tdkplatform/css/positions/' );
                $position_css_files = @Tools::scandir($cur_dir, 'css');
                foreach ($position_css_files as $cur_file) {
                    if (filesize($cur_dir.$cur_file) === 0) {
                        Tools::deleteFile($cur_dir.$cur_file);
                    }
                }

                $cur_dir = self::getPathFromUri( $common_folder.'modules/tdkplatform/js/positions/' );
                $position_js_files = @Tools::scandir($cur_dir, 'js');
                foreach ($position_js_files as $cur_file) {
                    if (filesize($cur_dir.$cur_file) === 0) {
                        Tools::deleteFile($cur_dir.$cur_file);
                    }
                }

                $cur_dir = self::getPathFromUri( $common_folder.'modules/tdkplatform/css/profiles/' );
                $profile_css_files = @Tools::scandir($cur_dir, 'css');
                foreach ($profile_css_files as $cur_file) {
                    if (filesize($cur_dir.$cur_file) === 0) {
                        Tools::deleteFile($cur_dir.$cur_file);
                    }
                }

                $cur_dir = self::getPathFromUri( $common_folder.'modules/tdkplatform/js/profiles/' );
                $profile_js_files = @Tools::scandir($cur_dir, 'js');
                foreach ($profile_js_files as $cur_file) {
                    if (filesize($cur_dir.$cur_file) === 0) {
                        Tools::deleteFile($cur_dir.$cur_file);
                    }
                }
            }

            if (file_exists(_PS_MODULE_DIR_.'tdkplatform/tdkplatform.php') && Module::isInstalled('tdkplatform')) {
                // @tam_thoi hook vao 'displayBanner'
                require_once(_PS_MODULE_DIR_.'tdkplatform/tdkplatform.php');
                $instance_module = TdkPlatform::getInstance();
                $instance_module->registerHook('displayBanner');

            }
        }
        
        # FIX : update Prestashop by 1-Click module -> LOST HOOK
        $tdk_version = Configuration::get('TDK_CURRENT_VERSION');
        if ($tdk_version == false) {
            $ps_version = Configuration::get('PS_VERSION_DB');
            Configuration::updateValue('TDK_CURRENT_VERSION', $ps_version);
        }
        
        $instance_module->registerHook('displayBackOfficeHeader');

        //add some hook
		//TDK:: call override file
		if ($qs && $instance_module->getOverrides() != null) {
            // Install overrides
            try {
                $instance_module->installOverrides();
				Configuration::updateValue('TDKPLATFORM_OVERRIDED', 1);
            } catch (Exception $e) {
                $instance_module->_errors[] = Context::getContext()->getTranslator()->trans('Unable to install override: %s', array($e->getMessage()), 'Admin.Modules.Notification');
                $instance_module->uninstallOverrides();

                return false;
            }
        }
    }
    
    public static function processDeleteOldPosition()
    {
        $sql = 'SELECT header,content,footer,product FROM `'._DB_PREFIX_.'tdkplatform_profiles` GROUP BY id_tdkplatform_profiles';
        $result = Db::getInstance()->executeS($sql);
        $list_exits_position = array();
        foreach ($result as $val) {
            foreach ($val as $v) {
                if (!in_array($v, $list_exits_position) && $v) {
                    $list_exits_position[] = $v;
                }
            }
        }
        if ($list_exits_position) {
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'tdkplatform_positions` WHERE id_tdkplatform_positions NOT IN ('.pSQL(implode(',', $list_exits_position)).')';
            
            $list_delete_position = Db::getInstance()->executes($sql);
            foreach ($list_delete_position as $row) {
                $object = new TdkPlatformPositionsModel($row['id_tdkplatform_positions']);
                $object->delete();
                if ($object->position_key) {
                    Tools::deleteFile(_PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/tdkplatform/css/positions/'.$object->position.$object->position_key.'.css');
                    Tools::deleteFile(_PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/tdkplatform/js/positions/'.$object->position.$object->position_key.'.js');
                }
            }
        }
    }
    
    /**
     * Check is Release or Developing
     * Release      : load css in themes/THEME_NAME/modules/MODULE_NAME/ folder
     * Developing   : load css in themes/THEME_NAME/assets/css/ folder
     */
    public static function isRelease()
    {
        if (defined('_TDK_MODE_DEV_') && _TDK_MODE_DEV_ === true) {
            # CASE DEV
            return false;
        }
        
        # Release
        return true;
    }
    
    public static $path_css;
    public static function getFullPathCss($file, $directories = array())
    {
        if (self::$path_css) {
            $directories = self::$path_css;
        } else {
            /**
             * DEFAULT
             * => D:\localhost\prestashop\themes/base/
             * =>
             * => D:\localhost\prestashop\
             */
            $directories = array(_PS_THEME_DIR_, _PS_PARENT_THEME_DIR_, _PS_ROOT_DIR_);
            if (!self::isRelease()) {
                $directories = array(_PS_THEME_DIR_.'assets/css/',_PS_THEME_DIR_, _PS_PARENT_THEME_DIR_, _PS_ROOT_DIR_);
            }
        }
        
        foreach ($directories as $baseDir) {
            $fullPath = realpath($baseDir.'/'.$file);
            if (is_file($fullPath)) {
                return $fullPath;
            }
        }
        return false;
    }
    
    public static function getUriFromPath($fullPath)
    {
        $uri = str_replace(
            _PS_ROOT_DIR_,
            rtrim(__PS_BASE_URI__, '/'),
            $fullPath
        );

        return str_replace(DIRECTORY_SEPARATOR, '/', $uri);
    }
    
    /**
     * Live Theme Editor
     */
    public static function getFileList($path, $e = null, $nameOnly = false)
    {
        $output = array();
        $directories = glob($path.'*'.$e);
        if ($directories) {
            foreach ($directories as $dir) {
                $dir = basename($dir);
                if ($nameOnly) {
                    $dir = str_replace($e, '', $dir);
                }
                $output[$dir] = $dir;
            }
        }
        return $output;
    }
    
    /**
     * When install theme, still get old_theme
     */
    public static function getInstallationThemeName()
    {
        $theme_name = '';
        if (Tools::getValue('controller') == 'AdminThemes' && Tools::getValue('action') == 'enableTheme') {
            # Case install theme
            $theme_name = Tools::getValue('theme_name');
        } else if (Tools::getValue('controller') == 'AdminShop' && Tools::getValue('submitAddshop')) {
            # Case install theme
            $theme_name = Tools::getValue('theme_name');
        } else {
            # Case other
            $theme_name = TdkPlatformHelper::getThemeName();
        }
        return $theme_name;
    }
    
    static $id_shop;
    /**
     * FIX Install multi theme
     * TdkPlatformHelper::getIDShop();
     */
    public static function getIDShop()
    {
        if ((int)self::$id_shop) {
            $id_shop = (int)self::$id_shop;
		} else {
            $id_shop = (int)Context::getContext()->shop->id;
        }
        return $id_shop;
    }
    
    /*
     * get theme in SINGLE_SHOP or MULTI_SHOP
     * TdkPlatformHelper::getThemeName()
     */
    public static function getThemeName()
    {
        static $theme_name;
        if (!$theme_name) {
            # DEFAULT SINGLE_SHOP
            $theme_name = _THEME_NAME_;

            # GET THEME_NAME MULTI_SHOP
            if (Shop::getTotalShops(false, null) >= 2) {
                $id_shop = Context::getContext()->shop->id;

                $shop_arr = Shop::getShop($id_shop);
                if (is_array($shop_arr) && !empty($shop_arr)) {
                    $theme_name = $shop_arr['theme_name'];
                }
            }
        }
        
        return $theme_name;
    }
    
    public static function fullCopy( $source, $target )
    {
        if (is_dir($source)) {
            @mkdir($target);
            $d = dir($source);
            while (FALSE !== ( $name = $d->read())) {
                if ($name == '.' || $name == '..' ) {
                    continue;
                }
                $entry = $source . '/' . $name;
                if (is_dir($entry)) {
                    self::fullCopy($entry, $target . '/' . $name);
                    continue;
                }
                
                copy($entry, $target . '/' . $name);
            }

            $d->close();
        } else {
            copy($source, $target);
        }
    }
    
    public static function getTemplate($tpl_name, $override_folder = '')
    {
        $module_name = 'tdkplatform';
        $hook_name = TdkShortCodesBuilder::$hook_name;
        
        if (isset($override_folder) && file_exists(_PS_ALL_THEMES_DIR_._THEME_NAME_."/modules/$module_name/views/templates/hook/$override_folder/$tpl_name")) {
            $tpl_file = "views/templates/hook/$override_folder/$tpl_name";
        } elseif (file_exists(_PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/'.$module_name.'/views/templates/hook/'.$hook_name.'/'.$tpl_name) || file_exists(_PS_MODULE_DIR_.$module_name.'/views/templates/hook/'.$hook_name.'/'.$tpl_name)) {
            $tpl_file = 'views/templates/hook/'.$hook_name.'/'.$tpl_name;
        } elseif (file_exists(_PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/'.$module_name.'/views/templates/hook/'.$tpl_name) || file_exists(_PS_MODULE_DIR_.$module_name.'/views/templates/hook/'.$tpl_name)) {
            $tpl_file = 'views/templates/hook/'.$tpl_name;
        } else {
            $tpl_file = 'views/templates/hook/TdkGeneral.tpl';
        }
        
        return $tpl_file;
    }
    
    /**
     * get Full path in tpl
     */
    public static function getTplTemplate($tpl_name='', $override_folder = '')
    {
        $module_name = 'tdkplatform';
        $hook_name = TdkShortCodesBuilder::$hook_name;
        
        $path_theme = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/'.$module_name.'/views/templates/hook/';
        $path_module = _PS_MODULE_DIR_.$module_name.'/views/templates/hook/';
        
        if (file_exists($path_theme.$override_folder.'/'.$tpl_name)) {
            # THEMES / OVERRIDE
            $tpl_file = $path_theme.$override_folder.'/'.$tpl_name;
        } elseif (file_exists($path_module.$override_folder.'/'.$tpl_name)) {
            # MODULE / OVERRIDE
            $tpl_file = $path_module.$override_folder.'/'.$tpl_name;
        } elseif (file_exists($path_theme.$hook_name.'/'.$tpl_name)) {
            # THEME / HOOK_NAME
            $tpl_file = $path_theme.$hook_name.'/'.$tpl_name;
        } elseif (file_exists($path_module.$hook_name.'/'.$tpl_name)) {
            # MODULE / HOOK_NAME
            $tpl_file = $path_module.$hook_name.'/'.$tpl_name;
        } elseif (file_exists($path_theme.$tpl_name)) {
            # THEME / HOOK
            $tpl_file = $path_theme.$tpl_name;
        } elseif (file_exists($path_module.$tpl_name)) {
            # MODULE / HOOK
            $tpl_file = $path_module.$tpl_name;
        } elseif (file_exists($path_theme.'/TdkGeneral.tpl')) {
            # THEME / GENERATE
            $tpl_file = $path_theme.'/TdkGeneral.tpl';
        } else {
            # MODULE / GENERATE
            $tpl_file = $path_module.'/TdkGeneral.tpl';
        }
        return $tpl_file;
    }

    /**
     * Copy method from ROOT\src\Adapter\Assets\AssetUrlGeneratorTrait.php
     */
    public static function getPathFromUri($fullUri)
    {
        return _PS_ROOT_DIR_.str_replace(rtrim(__PS_BASE_URI__, '/'), '', $fullUri);
    }
    
    public static function getFontFamily($default=false)
    {
        if ($default == 'default') {
            return '';
        }
        $result = array(
            array( 'id' => '', 'name' => ''),
            array( 'id' => 'arial', 'name' => 'Arial'),
            array( 'id' => 'verdana', 'name' => 'Verdana, Geneva'),
            array( 'id' => 'trebuchet', 'name' => 'Trebuchet'),
            array( 'id' => 'georgia', 'name' => 'Georgia'),
            array( 'id' => 'times', 'name' => 'Times New Roman'),
            array( 'id' => 'tahoma', 'name' => 'Tahoma, Geneva'),
            array( 'id' => 'palatino', 'name' => 'Palatino'),
            array( 'id' => 'helvetica', 'name' => 'Helvetica'),
        );
        
        
        $google_font_cfg = Configuration::get( self::getConfigName('google_font'));
        if ($google_font_cfg) {
            $google_fonts = explode('__________', $google_font_cfg);
            foreach ($google_fonts as &$font) {
                $font = Tools::jsonDecode($font, true);
                $result[] = array(
                    'id' => $font['gfont_name'],
                    'name' => $font['gfont_name'],
                );
            }
        }
        
        return $result;
    }
    
    public static function getShortcodeTemplatePath( $file_name )
    {
        $path = _PS_MODULE_DIR_.'tdkplatform/views/templates/admin/shortcodes/' . $file_name;
        return $path;
    }
	
    //TDK:: correct shortcode
	public static function createShortCode($correct = false)
	{
		//TDK:: copy js to add shortcode to tinymce
		if (!file_exists(_PS_MODULE_DIR_.'tdkplatform/views/js/shortcode/backup/tinymce.inc.js'))
		{
			//TDK:: backup default file config for tinymce
			Tools::copy(_PS_ROOT_DIR_.'/js/admin/tinymce.inc.js', _PS_MODULE_DIR_.'tdkplatform/views/js/shortcode/backup/tinymce.inc.js');
		}
		
		@mkdir(_PS_ROOT_DIR_.'/js/admin/', 0755, true);
		//TDK:: copy new file config for tinymce
		Tools::copy(_PS_MODULE_DIR_.'tdkplatform/views/js/shortcode/tinymce.inc.js', _PS_ROOT_DIR_.'/js/admin/tinymce.inc.js');
		
		//TDK:: copy folder plugin of shortcode for tinymce
		@mkdir(_PS_ROOT_DIR_.'/js/tiny_mce/plugins/tdkplatform/', 0755, true);
		Tools::copy(_PS_MODULE_DIR_.'tdkplatform/views/js/shortcode/tdkplatform/index.php', _PS_ROOT_DIR_.'/js/tiny_mce/plugins/tdkplatform/index.php');
		Tools::copy(_PS_MODULE_DIR_.'tdkplatform/views/js/shortcode/tdkplatform/plugin.min.js', _PS_ROOT_DIR_.'/js/tiny_mce/plugins/tdkplatform/plugin.min.js');
		
		@mkdir(_PS_ROOT_DIR_.'/override/controllers/front/listing/', 0755, true);
		Tools::copy(_PS_ROOT_DIR_.'/override/controllers/front/index.php', _PS_ROOT_DIR_.'/override/controllers/front/listing/index.php');
		
		if ($correct && !Configuration::get('TDKPLATFORM_OVERRIDED'))
		{
			$instance_module = TdkPlatform::getInstance();
			$instance_module->installOverrides();
			Configuration::updateValue('TDKPLATFORM_OVERRIDED', 1);
		}
	}
	
	//TDK:: clone function from fontcontroller for assign global parmaters for shortcode
	public static function getTemplateVarUrls($context)
    {
		$ssl = false;
		
		if (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) {
            $ssl = true;
        }

        if (isset($useSSL)) {
            $ssl = $useSSL;
        } else {
            $useSSL = $ssl;
        }
		
        $http = Tools::getCurrentUrlProtocolPrefix();
        $base_url = $context->shop->getBaseURL(true, true);

        $urls = array(
            'base_url' => $base_url,
            'current_url' => $context->shop->getBaseURL(true, false).$_SERVER['REQUEST_URI'],
            'shop_domain_url' => $context->shop->getBaseURL(true, false),
        );

        $assign_array = array(
            'img_ps_url' => _PS_IMG_,
            'img_cat_url' => _THEME_CAT_DIR_,
            'img_lang_url' => _THEME_LANG_DIR_,
            'img_prod_url' => _THEME_PROD_DIR_,
            'img_manu_url' => _THEME_MANU_DIR_,
            'img_sup_url' => _THEME_SUP_DIR_,
            'img_ship_url' => _THEME_SHIP_DIR_,
            'img_store_url' => _THEME_STORE_DIR_,
            'img_col_url' => _THEME_COL_DIR_,
            'img_url' => _THEME_IMG_DIR_,
            'css_url' => _THEME_CSS_DIR_,
            'js_url' => _THEME_JS_DIR_,
            'pic_url' => _THEME_PROD_PIC_DIR_,
        );

        foreach ($assign_array as $assign_key => $assign_value) {
            if (substr($assign_value, 0, 1) == '/' || $ssl) {
                $urls[$assign_key] = $http.Tools::getMediaServer($assign_value).$assign_value;
            } else {
                $urls[$assign_key] = $assign_value;
            }
        }

        $pages = array();
        $p = array(
            'address', 'addresses', 'authentication', 'cart', 'category', 'cms', 'contact',
            'discount', 'guest-tracking', 'history', 'identity', 'index', 'my-account',
            'order-confirmation', 'order-detail', 'order-follow', 'order', 'order-return',
            'order-slip', 'pagenotfound', 'password', 'pdf-invoice', 'pdf-order-return', 'pdf-order-slip',
            'prices-drop', 'product', 'search', 'sitemap', 'stores', 'supplier',
        );
        foreach ($p as $page_name) {
            $index = str_replace('-', '_', $page_name);
            $pages[$index] = $context->link->getPageLink($page_name, $ssl);
        }
        $pages['register'] = $context->link->getPageLink('authentication', true, null, array('create_account' => '1'));
        $pages['order_login'] = $context->link->getPageLink('order', true, null, array('login' => '1'));
        $urls['pages'] = $pages;

        $urls['alternative_langs'] = self::getAlternativeLangsUrl($context);

        $urls['theme_assets'] = __PS_BASE_URI__.'themes/'.$context->shop->theme->getName().'/assets/';

        $urls['actions'] = array(
            'logout' => $context->link->getPageLink('index', true, null, 'mylogout'),
        );

        $imageRetriever = new ImageRetriever($context->link);
        $urls['no_picture_image'] =  $imageRetriever->getNoPictureImage($context->language);

        return $urls;
    }
	
	public static function getAlternativeLangsUrl($context)
    {
        $alternativeLangs = array();
        $languages = Language::getLanguages(true, $context->shop->id);

        if ($languages < 2) {
            // No need to display alternative lang if there is only one enabled
            return $alternativeLangs;
        }

        foreach ($languages as $lang) {
            $alternativeLangs[$lang['language_code']] = $context->link->getLanguageLink($lang['id_lang']);
        }
        return $alternativeLangs;
    }
	
}
