<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

class ProductController extends ProductControllerCore
{
    public function getTemplateVarProduct()
    {
        $product = parent::getTemplateVarProduct();

        if ((bool)Module::isEnabled('tdkplatform')) {
            $tdkplatform = Module::getInstanceByName('tdkplatform');
            if (strpos($product['description'], '[TdkSC') !== false && strpos($product['description'], '[/TdkSC]') !== false) {
                $product['description'] = $tdkplatform->buildShortCode($product['description']);
            };
            if (strpos($product['description_short'], '[TdkSC') !== false && strpos($product['description_short'], '[/TdkSC]') !== false) {
                $product['description_short'] = $tdkplatform->buildShortCode($product['description_short']);
            }
        }

        return $product;
    }
}
