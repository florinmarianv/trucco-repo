<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

class CategoryController extends CategoryControllerCore
{
    protected function getTemplateVarCategory()
    {
        $category = parent::getTemplateVarCategory();

        if ((bool)Module::isEnabled('tdkplatform')) {
            if (strpos($category['description'], '[TdkSC') !== false && strpos($category['description'], '[/TdkSC]') !== false) {
                $tdkplatform = Module::getInstanceByName('tdkplatform');
                $category['description'] = $tdkplatform->buildShortCode($category['description']);
            }
        }

        return $category;
    }
}
