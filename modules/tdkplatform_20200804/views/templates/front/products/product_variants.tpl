{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="highlighted-informations{if !$product.main_variants} no-variants{/if} hidden-sm-down">
	{block name='product_variants'}
	  {if $product.main_variants}
		{include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}
	  {/if}
	{/block}
  </div>