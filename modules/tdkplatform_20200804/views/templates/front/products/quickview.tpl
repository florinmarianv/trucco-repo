{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\front\products\file_tpl -->
<div class="quickview{if !$product.main_variants} no-variants{/if} hidden-sm-down">
<a
  href="#"
  class="quick-view btn TdkBtnProduct"
  data-link-action="quickview"
>
	<span class="tdk-quickview-bt-loading cssload-speeding-wheel"></span>
	<span class="tdk-quickview-bt-content">
		<i class="TdkIconBtnProduct icon-quick-view material-icons search">&#xE8B6;</i>
		<span class="TdkNameBtnProduct">{l s='Quick view' mod='tdkplatform'}</span>
	</span>
</a>
</div>
