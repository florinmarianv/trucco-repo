{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
{block name='product_accessories'}
  {if $accessories}
    <section class="product-accessories clearfix">
      <h3 class="h5 products-section-title">{l s='You might also like' d='Shop.Theme.Catalog'}</h3>
      <div class="products">
        <div class="slick-row">
			<div class="timeline-wrapper clearfix prepare"
				data-item="4" 
				data-xl="4" 
				data-lg="3" 
				data-md="3" 
				data-sm="2" 
				data-m="2"
			>
				{for $foo=1 to 4}			
					<div class="timeline-parent">
						<div class="timeline-item">
							<div class="animated-background">					
								<div class="background-masker content-top"></div>							
								<div class="background-masker content-second-line"></div>							
								<div class="background-masker content-third-line"></div>							
								<div class="background-masker content-fourth-line"></div>
							</div>
						</div>
					</div>
				{/for}
			</div>
			<div class="list-product-slick-carousel slick-slider slick-loading {if isset($productClassWidget)}{$productClassWidget|escape:'html':'UTF-8'}{/if}">
				{foreach from=$accessories item="product_accessory" name=products_accessories name=products_accessories}
					<div class="slick-slide{if $smarty.foreach.products_accessories.index == 0} first{/if}{if $smarty.foreach.products_accessories.last} last{/if}">
						<div class="item">
						  {block name='product_miniature'}
							{if isset($productProfileDefault) && $productProfileDefault}
								{* exits THEME_NAME/modules/tdkplatform/views/templates/front/profiles/profile_name.tpl -> load template*}
								{hook h='displayTdkProfileProduct' product=$product_accessory profile=$productProfileDefault}
							{else}
								{include file='catalog/_partials/miniatures/product.tpl' product=$product_accessory}
							{/if}
						  {/block}
						</div>
					</div>
				{/foreach}
			</div>
        </div>
      </div>
    </section>
	<script type="text/javascript">
	tdk_list_functions.push(function(){
		$('.product-accessories .list-product-slick-carousel').imagesLoaded( function() {
			$('.product-accessories .list-product-slick-carousel').slick(
				{			
					centerMode: false,
					centerPadding: '60px',
					dots: false,
					infinite: false,
					vertical: false,
					autoplay: false,
					pauseonhover: true,
					autoplaySpeed: 2000,
					arrows: true,
					rows: 1,
					slidesToShow: 4,
					slidesToScroll: 1,
					rtl: {if isset($IS_RTL) && $IS_RTL}true{else}false{/if},
					responsive: [
											{
						  breakpoint: 1200,
						  settings: {
							centerMode: false,
							centerPadding: '60px',
							slidesToShow: 4,
							slidesToScroll: 1,
						  }
						},
											{
						  breakpoint: 992,
						  settings: {
							centerMode: false,
							centerPadding: '60px',
							slidesToShow: 3,
							slidesToScroll: 1,
						  }
						},
											{
						  breakpoint: 768,
						  settings: {
							centerMode: false,
							centerPadding: '60px',
							slidesToShow: 3,
							slidesToScroll: 1,
						  }
						},
											{
						  breakpoint: 576,
						  settings: {
							centerMode: false,
							centerPadding: '60px',
							slidesToShow: 3,
							slidesToScroll: 1,
						  }
						},
											{
						  breakpoint: 575,
						  settings: {
							centerMode: false,
							centerPadding: '60px',
							slidesToShow: 2,
							slidesToScroll: 1,
						  }
						},
											{
						  breakpoint: 400,
						  settings: {
							centerMode: false,
							centerPadding: '60px',
							slidesToShow: 1,
							slidesToScroll: 1,
						  }
						},
										]
				}			
			);
			$('.product-accessories .list-product-slick-carousel').removeClass('slick-loading').addClass('slick-loaded').parents('.slick-row').addClass('hide-loading');
		});
	});
	</script>
  {/if}
{/block}