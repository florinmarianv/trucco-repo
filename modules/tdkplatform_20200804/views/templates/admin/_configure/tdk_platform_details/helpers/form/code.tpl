{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="col-md-12 code plist-element" data-element='code'>
    <div class="desc-box"><a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Drag me' mod='tdkplatform'}" class="group-action gaction-drag label-tooltip"><i class="icon-move"></i> </a> tpl code</div>
    <div class="pull-right">
        <a class="plist-code"><i class="icon-code"></i></a>
        <a class="plist-eremove"><i class="icon-trash"></i></a>
    </div>
    <div class="content-code">
        <textarea name="filecontent" rows="5" class="">{if isset($code)}{$code nofilter}{* HTML form , no escape necessary *}{/if}</textarea>
    </div>
</div>