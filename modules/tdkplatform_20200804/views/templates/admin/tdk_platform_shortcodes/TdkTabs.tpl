{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\admin\tdk_platform_shortcodes\TdkTabs -->
{if !isset($isSubTab)}
<div {if !isset($tdkInfo)}id="default_TdkTabs"{/if} class="widget-row clearfix TdkTabs{if isset($formAtts)} {$formAtts.form_id|escape:'html':'UTF-8'}{/if}" data-type='TdkTabs'>
    <div class="float-center-control text-center">
        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Drag to sort group' mod='tdkplatform'}" class="tab-action waction-drag label-tooltip"><i class="icon-move"></i> </a>
        <span>{l s='Widget Tab' mod='tdkplatform'}</span>
        
        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Edit Tabs' mod='tdkplatform'}" class="tab-action btn-edit label-tooltip" data-type="TdkTabs"><i class="icon-edit"></i></a>
        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Delete Tabs' mod='tdkplatform'}" class="tab-action btn-delete label-tooltip"><i class="icon-trash"></i></a>
        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Duplicate Tabs' mod='tdkplatform'}" class="tab-action btn-duplicate label-tooltip"><i class="icon-paste"></i></a>
        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Disable or Enable Tab' mod='tdkplatform'}" class="tab-action btn-status label-tooltip{if isset($formAtts.active) && !$formAtts.active} deactive{else} active{/if}"><i class="icon-ok"></i></a>
    </div>
{if !isset($tdkInfo)}
    <ul class="widget-container-heading nav nav-tabs" role="tablist">
        {for $foo=1 to 3}
            <li {if $foo ==3}id="default_tabnav"{/if} class="{if $foo==1}active{/if}">
                <a href="#tab{$foo|escape:'html':'UTF-8'}" role="tab" data-toggle="tab">{if $foo ==3}{l s='New Tab' mod='tdkplatform'}{else}{l s='Tab' mod='tdkplatform'} {$foo|escape:'html':'UTF-8'}{/if}</a></li>
        {/for}
        <li class="tab-button"><a href="javascript:void(0)" class="btn-add-tab"><i class="icon-plus"></i> {l s='Add' mod='tdkplatform'}</a></li>
    </ul>
    
    <div class="tab-content widget-container-content">
        {for $foo=1 to 3}
            <div {if $foo ==3}id="default_tabcontent"{else}id="tab{$foo|escape:'html':'UTF-8'}"{/if} class="tab-pane{if $foo==1} active{/if} widget-wrapper-content">
                <div class="text-center tab-content-control">
                    <span>{l s='Tab' mod='tdkplatform'}</span>
                    <a href="javascript:void(0)" class="tabcontent-action btn-new-widget label-tooltip" title=""><i class="icon-plus-sign"></i></a>
                    <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Edit Tab' mod='tdkplatform'}" class="tabcontent-action btn-edit label-tooltip" data-type="tdkSubTabs"><i class="icon-edit"></i></a>
                    <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Delete Tab' mod='tdkplatform'}" class="tabcontent-action btn-delete label-tooltip tab"><i class="icon-trash"></i></a>
                    <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Duplicate Tab' mod='tdkplatform'}" class="tabcontent-action btn-duplicate label-tooltip"><i class="icon-paste"></i></a>
                </div>
                <div class="subwidget-content">
                    
                </div>
            </div>
        {/for}
    </div>
{else}
    <ul class="widget-container-heading nav nav-tabs" role="tablist">
        {foreach from=$subTabContent item=subTab}
            <li class="">
                <a href="#{$subTab.id|escape:'html':'UTF-8'}" class="{$subTab.form_id|escape:'html':'UTF-8'}" role="tab" data-toggle="tab">
                    <span>{$subTab.title|escape:'html':'UTF-8'}</span>
                </a>
            </li>
        {/foreach}
        <li class="tab-button"><a href="javascript:void(0)" class="btn-add-tab"><i class="icon-plus"></i> {l s='Add' mod='tdkplatform'}</a></li>
    </ul>

    <div class="tab-content">
        {$tdkContent}{* HTML form , no escape necessary *}
    </div>
{/if}
</div>
{else}
    <div id="{$tabID|escape:'html':'UTF-8'}" class="tab-pane widget-wrapper-content">
        <div class="text-center tab-content-control">
            <span>{l s='Tab' mod='tdkplatform'}</span>
            <a href="javascript:void(0)" class="tabcontent-action btn-new-widget label-tooltip" title=""><i class="icon-plus-sign"></i></a>
            <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Edit Tab' mod='tdkplatform'}" class="tabcontent-action btn-edit label-tooltip" data-type="tdkSubTabs"><i class="icon-edit"></i></a>
            <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Delete Tab' mod='tdkplatform'}" class="tabcontent-action btn-delete label-tooltip tab"><i class="icon-trash"></i></a>
            <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Duplicate Tab' mod='tdkplatform'}" class="tabcontent-action btn-duplicate label-tooltip"><i class="icon-paste"></i></a>
        </div>
        <div class="subwidget-content">
            {$tdkContent}{* HTML form , no escape necessary *}
        </div>
    </div>
{/if}
