{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkManuFacturersCarousel -->
{if isset($formAtts.lib_has_error) && $formAtts.lib_has_error}
    {if isset($formAtts.lib_error) && $formAtts.lib_error}
        <div class="alert alert-warning tdk-lib-error">{$formAtts.lib_error}</div>
    {/if}
{else}
    <div class="block TdkBlock TdkManufacturersCarousel manufacturers_block exclusive {(isset($formAtts.class)) ? $formAtts.class : ''|escape:'html':'UTF-8'}">
        {($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            <div class="TdkGroupTitle">
        {/if}
            {if isset($formAtts.title)&&!empty($formAtts.title)}
                <h4 class="TdkTitleBlock">
                    {$formAtts.title|escape:'html':'UTF-8'}
                </h4>
            {/if}
            {if isset($formAtts.sub_title) && $formAtts.sub_title}
                <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
            {/if}
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            </div>
        {/if}
        <div class="TdkBlockContent">
            {if !empty($manufacturers)}
                {if $formAtts.carousel_type == "slickcarousel"}
                    {assign var=tdk_include_file value=$tdk_helper->getTplTemplate('manufacturers_slick_carousel.tpl', $formAtts['override_folder'])}
                    {include file=$tdk_include_file}
                {else}
                    {if $formAtts.carousel_type == "boostrap"}
                        {assign var=tdk_include_file value=$tdk_helper->getTplTemplate('manufacturers_carousel.tpl', $formAtts['override_folder'])}
                        {include file=$tdk_include_file}
                    {else}
                        {assign var=tdk_include_file value=$tdk_helper->getTplTemplate('manufacturers_owl_carousel.tpl', $formAtts['override_folder'])}
                        {include file=$tdk_include_file}
                    {/if}
                {/if}
            {else}
                <p class="alert alert-info">{l s='No manufacturer at this time.' mod='tdkplatform'}</p>
            {/if}
        </div>
        {($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
    </div>
{/if}