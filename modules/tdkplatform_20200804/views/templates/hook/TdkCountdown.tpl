{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkCountdown -->
{if isset($formAtts.lib_has_error) && $formAtts.lib_has_error}
    {if isset($formAtts.lib_error) && $formAtts.lib_error}
        <div class="alert alert-warning tdk-lib-error">{$formAtts.lib_error}</div>
    {/if}
{else}
    {if isset($formAtts.active) && $formAtts.active == 1}
        <div class="TdkWidgetCountDown {(isset($formAtts.class)) ? $formAtts.class : ''|escape:'html':'UTF-8'}" id="countdown-{$formAtts.form_id|escape:'html':'UTF-8'}">
            {if isset($formAtts.sub_title) && $formAtts.sub_title}
                <div class="TdkGroupTitle">
            {/if}
                {if isset($formAtts.title) && !empty($formAtts.title)}
                    <h4 class="TdkTitleBlock">
                        {$formAtts.title nofilter}{* HTML form , no escape necessary *}
                    </h4>
                {/if}
                {if isset($formAtts.sub_title) && $formAtts.sub_title}
                    <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
                {/if}
            {if isset($formAtts.sub_title) && $formAtts.sub_title}
                </div>
            {/if}
            {if isset($formAtts.description) && !empty($formAtts.description)}
                <div class="tdk-countdown-desc">{$formAtts.description nofilter}{* HTML form , no escape necessary *}</div>
            {/if}
            <ul class="tdk-countdown-time deal-clock lof-clock-11-detail list-inline"></ul>

            {if isset($formAtts.link) && $formAtts.link}
                <p class="tdk-countdown-link">
                    {if isset($formAtts.new_tab) && $formAtts.new_tab == 1}
                        <a href="{$formAtts.link|escape:'html':'UTF-8'}" target="_blank">{$formAtts.link_label|escape:'html':'UTF-8'}</a>
                    {/if}	
                    {if isset($formAtts.new_tab) && $formAtts.new_tab == 0}
                        <a href="{$formAtts.link|escape:'html':'UTF-8'}">{$formAtts.link_label|escape:'html':'UTF-8'}</a>
                    {/if}
                </p>
            {/if}
        </div>

        <script type="text/javascript">
            tdk_list_functions.push(function(){
                var text_d = '{l s='days' mod='tdkplatform'}';
                var text_h = '{l s='hours' mod='tdkplatform'}';
                var text_m = '{l s='min' mod='tdkplatform'}';
                var text_s = '{l s='sec' mod='tdkplatform'}';
                $(".lof-clock-11-detail").lofCountDown({
                    TargetDate:"{$formAtts.time_to|escape:'html':'UTF-8'}",
                    DisplayFormat:'<li class="tdkDay">%%D%%<span>'+text_d+'</span></li><li class="tdkHours">%%H%%<span>'+text_h+'</span></li><li class="tdkMonth">%%M%%<span>'+text_m+'</span></li><li class="tdkSecond">%%S%%<span>'+text_s+'</span></li>',
                });
            });
        </script>
    {/if}
{/if}