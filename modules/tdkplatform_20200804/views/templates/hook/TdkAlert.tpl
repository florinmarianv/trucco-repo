{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkAlert -->
<div id="alert-{$formAtts.form_id|escape:'html':'UTF-8'}" class="block TdkBlock TdkWidgetAlert">
	{($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
	{if isset($formAtts.sub_title) && $formAtts.sub_title}
        <div class="TdkGroupTitle">
    {/if}
		{if isset($formAtts.title) && !empty($formAtts.title)}
			<h4 class="TdkTitleBlock">
				{$formAtts.title|rtrim|escape:'html':'UTF-8'}
			</h4>
		{/if}
	    {if isset($formAtts.sub_title) && $formAtts.sub_title}
	        <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
	    {/if}
	{if isset($formAtts.sub_title) && $formAtts.sub_title}
        </div>
    {/if}
	<div class="alert {$formAtts.alert_type|escape:'html':'UTF-8'}">
	{if isset($formAtts.content_html)}
		{$formAtts.content_html nofilter}{* HTML form , no escape necessary *}
	{/if}
	</div>
	{($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
</div>