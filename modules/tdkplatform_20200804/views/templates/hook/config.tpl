{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="group-input group-{$tdk_cf} clearfix">
	<label class="col-sm-12 control-label">
			<i class="fa fa-tags"></i>
			{if $tdk_cf == 'profile'}
					{l s='Home version' mod='tdkplatform'}
			{else if $tdk_cf == 'header'}
					{l s='Header version' mod='tdkplatform'}
			{else if $tdk_cf == 'footer'}
					{l s='Footer version' mod='tdkplatform'}
			{else if $tdk_cf == 'product'}
					{l s='Product version' mod='tdkplatform'}
			{else if $tdk_cf == 'content'}
					{l s='Content Home' mod='tdkplatform'}
			{else if $tdk_cf == 'product_builder'}
					{l s='Product Builder' mod='tdkplatform'}
			{/if}
	</label>
	<div class="col-sm-12">
        {foreach from=$tdk_cfdata item=cfdata}
            {if isset($cfdata.friendly_url) && $cfdata.friendly_url != '' && ( $tdk_controller=='index' || $tdk_controller=='tdkplatformhome')}
                {* current_page is index.php, and this link is friendly_url  *}
                <a class="tdkconfig tdkconfig-{$tdk_cf}{if $cfdata.active} active{/if}" data-type="{$tdk_type}" data-id='{$cfdata.id}' data-enableJS="false" href="{$tdk_current_url}{$cfdata.friendly_url}.html">{$cfdata.name}</a>
            {else if ($tdk_controller=='index' && $tdk_cf=='profile')}
                {* current_page is index.php (rewrite), and this link is id  *}
                <a class="tdkconfig tdkconfig-{$tdk_cf}{if $cfdata.active} active{/if}" data-type="{$tdk_type}" data-id='{$cfdata.id}' data-enableJS="true" data-url="{$tdk_current_url}" href="{$tdk_current_url}{$cfdata.friendly_url}.html">{$cfdata.name}</a>
            {else}
                <a class="tdkconfig tdkconfig-{$tdk_cf}{if $cfdata.active} active{/if}" data-type="{$tdk_type}" data-id='{$cfdata.id}' data-enableJS="true" href="index.php?{$tdk_type}={$cfdata.id}">{$cfdata.name}</a>
            {/if}
        {/foreach}
	</div>
</div>