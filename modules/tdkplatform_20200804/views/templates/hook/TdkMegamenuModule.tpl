{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkMegamenuModule.tpl -->
{if isset($formAtts.lib_has_error) && $formAtts.lib_has_error}
    {if isset($formAtts.lib_error) && $formAtts.lib_error}
        <div class="alert alert-warning tdk-lib-error">{$formAtts.lib_error}</div>
    {/if}
{else}
<div id="memgamenu-{$formAtts.form_id|escape:'html':'UTF-8'}" class="TdkMegamenu">
	{if isset($content_megamenu)}
		{$content_megamenu nofilter}{* HTML form , no escape necessary *}
	{/if}
</div>
{/if}