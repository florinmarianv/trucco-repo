{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
{if $product}
	{if $colors}
		{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction}
			{if $product.specific_prices.reduction_type == 'percentage'}
					{math assign='sale' equation='x*100' x=$product.specific_prices.reduction}
			{else}
					{math assign='sale' equation='(x/y)*100' x=$product.specific_prices.reduction y=$product.price_without_reduction}
			{/if}
			{foreach from=$colors item=color key=k}	
				{if $k >= $sale }
					<div>
						{$color nofilter}{* HTML form , no escape necessary *}
					</div>
					{break}
				{/if}
			{/foreach}
		{/if}
	{/if}		
{/if}