{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkModule -->
{($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
{$tdkContent nofilter nofilter}{* HTML form , no escape necessary *}
{($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}