/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
* 
**/
function proccessData(id) {
    var list = $.trim($(id).val()).split(",");
    var result = "";
    var sep = "";
    for(var i = 0; i < list.length; i++) {
        if($.trim(list[i])) {
            result += sep + $.trim(list[i]);
            sep = ",";
        }
    }
    $(id).val(result);
}
$(document).ready(function() {
    $('.panel-content-builder').slideUp();
    $('span.open-content').click(function(){
        $(this).closest('.panel').find('.panel-content-builder').slideToggle();
    });
	
    // SHOW FULL input html
    $(".tdk-html-full").closest( ".col-lg-9.col-lg-offset-3" ).removeClass("col-lg-9 col-lg-offset-3");
	
	//TDK:: hide home config with theme of TDK Studio, redirect to profile page
	if ($('#psthemecusto').length && (typeof tdk_check_theme_name != 'undefined'))
	{		
		$('#psthemecusto .panel').hide();
		$('#psthemecusto').append('<div class="panel"><div class="panel-heading">'+tdk_profile_txt_redirect+': <a target="_blank" href="'+tdk_profile_url+'">'+tdk_profile_url+'</a></div></div>');
	}
});
function compareListHooks(oldHooks, newHooks) {
    //console.log(oldHooks);
    //console.log(newHooks);
    isEqual = true;
    if(oldHooks.length > 0 && newHooks.length > 0) {
        for(var i = 0; i < oldHooks.length; i++) {
            var isSubLook = false;
            for(var j = 0; j < newHooks.length; j++) {
                if(oldHooks[i] === newHooks[j]) {
                    newHooks.splice(j, 1);
                    //console.log(newHooks);
                    isSubLook = true;
                    break;
                }
            }
            if(!isSubLook) {
                return false;
            }
        }
    }
    return isEqual;
}

$(function() {

    // SHOW MENU 'TDK Module Configuration' - BEGIN
    if( (typeof(js_tdk_controller) != 'undefined') && (js_tdk_controller == 'module_configuration')){
        $('#subtab-AdminTdkPlatform').addClass('active');
        $('#subtab-AdminTdkPlatformModule').addClass('active');
        $('#subtab-AdminParentModulesSf').removeClass('active');
    }
    // SHOW MENU 'TDK Module Configuration' - END
    
    // Hide RESUME at left menu, BO
    if( (typeof(js_tdk_dev) != 'undefined') && (js_tdk_dev == 1)){
//        $('#tab-AdminDashboard').hide();
        $('.onboarding-navbar').hide();
    }
    
    $("#hook_header_old").val($("#HEADER_HOOK").val());
    $("#hook_content_old").val($("#CONTENT_HOOK").val());
    $("#hook_footer_old").val($("#FOOTER_HOOK").val());
    $("#hook_product_old").val($("#PRODUCT_HOOK").val());
    
    $(".list-all-hooks a").click(function() {
        var newHook = $.trim($(this).text());
        var id = $("#position-hook-select").val();
        id = id ? id : "#HEADER_HOOK";
        var listHook = $.trim($(id).val());
        if(listHook.search(newHook) < 0) {
            listHook += (listHook ? "," : "") + newHook;
            $(id).val(listHook);
        } else {
            alert("This hook is existed");
        }
    });
    $("#HEADER_HOOK, #CONTENT_HOOK, #FOOTER_HOOK, #PRODUCT_HOOK").focus(function() {
        var id = "#" + $(this).attr("id");
        $("#position-hook-select").val(id);
    });
    $("#btn-save-tdkplatform").click(function(e) {
        $isChange = false;
        proccessData("#HEADER_HOOK");
        proccessData("#CONTENT_HOOK");
        proccessData("#FOOTER_HOOK");
        proccessData("#PRODUCT_HOOK");
        // Check change config hooks
        var oldHook = $.trim($("#hook_header_old").val()).split(",");
        var currentHook = $.trim($("#HEADER_HOOK").val()).split(",");
        if(oldHook.length != currentHook.length || !compareListHooks(oldHook, currentHook)) {
            $isChange = true;
        }
        oldHook = $.trim($("#hook_content_old").val()).split(",");
        currentHook = $.trim($("#CONTENT_HOOK").val()).split(",");
        if(oldHook.length != currentHook.length || !compareListHooks(oldHook, currentHook)) {
            $isChange = true;
        }
        oldHook = $.trim($("#hook_footer_old").val()).split(",");
        currentHook = $.trim($("#FOOTER_HOOK").val()).split(",");
        if(oldHook.length != currentHook.length || !compareListHooks(oldHook, currentHook)) {
            $isChange = true;
        }
        oldHook = $.trim($("#hook_product_old").val()).split(",");
        currentHook = $.trim($("#PRODUCT_HOOK").val()).split(",");
        if(oldHook.length != currentHook.length || !compareListHooks(oldHook, currentHook)) {
            $isChange = true;
        }
        if($isChange) {
            if(!confirm($("#message_confirm").val())) {
                e.stopPropagation();
                return false;
            }
            $("#is_change").val("is_change");
        }
        
    });
    
    $("#modal_form").on("click", ".btn-savewidget", function() {
        // Is form add new module to tdkplatform
        if($("#modal_form").find(".form_tdk_module").length > 0) {
            // Validate select hook
            if(!$("#select-hook").val()) {
                alert($("#select-hook-error").val());
                return false;
            }
        }
    });
});