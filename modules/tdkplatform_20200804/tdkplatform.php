<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}
require_once(_PS_MODULE_DIR_.'tdkplatform/libs/Helper.php');
require_once(_PS_MODULE_DIR_.'tdkplatform/libs/TdkFrameworkHelper.php');
require_once(_PS_MODULE_DIR_.'tdkplatform/classes/TdkPlatformSetting.php');
require_once(_PS_MODULE_DIR_.'tdkplatform/classes/TdkPlatformModel.php');
require_once(_PS_MODULE_DIR_.'tdkplatform/classes/TdkPlatformProfilesModel.php');
require_once(_PS_MODULE_DIR_.'tdkplatform/classes/TdkPlatformProductsModel.php');
require_once(_PS_MODULE_DIR_.'tdkplatform/classes/TdkPlatformShortcodeModel.php');

class TdkPlatform extends Module
{
    protected $default_language;
    protected $languages;
    protected $theme_name;
    protected $data_index_hook;
    protected $profile_data;
    protected $hook_index_data;
    protected $profile_param;
    protected $path_resource;
    protected $product_active;
    protected $backup_dir;
    protected $header_content;
    
    protected $_confirmations = array();
    protected $_errors = array();
    protected $_warnings = array();

    public function __construct()
    {
        $this->name = 'tdkplatform';
        $this->tab = 'front_office_features';
        $this->version = '1.1.0';
        $this->author = 'TDK Studio';
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->need_instance = 0;
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;
        parent::__construct();
        
        $this->displayName = $this->l('TDK Platform');
        $this->description = $this->l('TDK Platform build content for your site.');
        $this->theme_name = _THEME_NAME_;
        $this->default_language = Language::getLanguage(Context::getContext()->language->id);
        $this->languages = Language::getLanguages();
        $this->path_resource = $this->_path.'views/';
        $this->redirectFriendUrl();
    }
    
    public function redirectFriendUrl()
    {
//        if (Context::getContext()->controller === null || (isset(Context::getContext()->controller->controller_type) && in_array(Context::getContext()->controller->controller_type, array('front', 'modulefront')))) {
        // NEED HOOK TO MODULEROUTES
        $tdk_live_edit = Tools::getValue('tdk_live_edit');
        if (empty($tdk_live_edit) &&  (Context::getContext()->controller === null || Context::getContext()->controller->controller_type != 'admin')) {
            $id_tdkplatform_profiles = Tools::getValue('id_tdkplatform_profiles');
            if (Configuration::get('PS_REWRITING_SETTINGS') && Tools::getIsset('id_tdkplatform_profiles') && $id_tdkplatform_profiles) {
                $profile_data = TdkPlatformProfilesModel::getActiveProfile('index');

                if (isset($profile_data['friendly_url']) && !empty($profile_data['friendly_url'])) {
                    require_once(_PS_MODULE_DIR_.'tdkplatform/libs/TdkFriendlyUrl.php');
                    $tdk_friendly_url = TdkFriendlyUrl::getInstance();
                    
                    $link = Context::getContext()->link;
                    $idLang = Context::getContext()->language->id;
                    $idShop = null;
                    $relativeProtocol = false;
                    
                    $url = $link->getBaseLink($idShop, null, $relativeProtocol).$tdk_friendly_url->getLangLink($idLang, null, $idShop).$profile_data['friendly_url'].'.html';
                    $tdk_friendly_url->canonicalRedirection($url);
                }
            }
        }
    }
    
    public static function getInstance()
    {
        static $_instance;
        if (!$_instance) {
            $_instance = new TdkPlatform();
        }
        return $_instance;
    }

    public function install()
    {
        require_once(_PS_MODULE_DIR_.$this->name.'/libs/setup.php');
        //TDK:: build shortcode
        TdkPlatformHelper::createShortCode();
        if (!parent::install() || !TdkPlatformSetup::installConfiguration() || !TdkPlatformSetup::createTables() || !TdkPlatformSetup::installModuleTab() || !$this->registerTDKHook()) {
            return false;
        }

        # NOT LOAD DATASAMPLE AGAIN
        Configuration::updateValue('TDK_INSTALLED_TDKPLATFORM', '1');
        
        # REMOVE FILE INDEX.PHP FOR TRANSLATE
        TdkPlatformSetup::processTranslateTheme();
               
        Configuration::updateValue('TDKPLATFORM_OVERRIDED', 1);

        return true;
    }

    public function uninstall()
    {
        require_once(_PS_MODULE_DIR_.$this->name.'/libs/setup.php');
        
        //TDK:: rollback default file config for tinymce
        Tools::copy(_PS_MODULE_DIR_.$this->name.'/views/js/shortcode/backup/tinymce.inc.js', _PS_ROOT_DIR_.'/js/admin/tinymce.inc.js');

        if (!parent::uninstall()|| !TdkPlatformSetup::uninstallModuleTab() || !TdkPlatformSetup::deleteTables() || !TdkPlatformSetup::uninstallConfiguration()) {
            return false;
        }
        //TDK:: remove overrider folder
        // $this->uninstallOverrides();
        Configuration::updateValue('TDKPLATFORM_OVERRIDED', 0);
        return true;
    }
    
    public function hookActionModuleRegisterHookAfter($params)
    {
        if (isset($params['hook_name']) && ($params['hook_name'] == 'header' || $params['hook_name']=='displayheader')) {
            if ($this->getConfig('MOVE_END_HEADER')) {
                $hook_name = 'header';
                $id_hook = Hook::getIdByName($hook_name);
                $id_module = $this->id;
                $id_shop = Context::getContext()->shop->id;
                
                // Get module position in hook
                $sql = 'SELECT MAX(`position`) AS position
                    FROM `'._DB_PREFIX_.'hook_module`
                    WHERE `id_hook` = '.(int)$id_hook.' AND `id_shop` = '.(int)$id_shop . ' AND id_module != '.(int) $id_module;
                if (!$position = Db::getInstance()->getValue($sql)) {
                    $position = 0;
                }

                $sql = 'UPDATE `'._DB_PREFIX_.'hook_module'.'` SET `position` =' . (int)($position + 1) . ' WHERE '
                                . '`id_module` = '.(int) $id_module
                                . ' AND `id_hook` = '.(int) $id_hook
                                . ' AND `id_shop` = '.(int) $id_shop;
                Db::getInstance()->execute($sql);
            }
        }
    }
    
    public function getContent()
    {
        $output = '';
        $this->backup_dir = str_replace('\\', '/', _PS_CACHE_DIR_.'backup/modules/tdkplatform/');
        
        if (Tools::isSubmit('installdemo')) {
            require_once(_PS_MODULE_DIR_.$this->name.'/libs/setup.php');
            TdkPlatformSetup::installSample();
        } else if (Tools::isSubmit('resetmodule')) {
            require_once(_PS_MODULE_DIR_.$this->name.'/libs/setup.php');
            TdkPlatformSetup::createTables(1);
        } else if (Tools::isSubmit('deleteposition')) {
            TdkPlatformHelper::processDeleteOldPosition();
            $this->_confirmations[] = 'POSITIONS NOT USE have been deleted successfully.';
        } else if (Tools::isSubmit('submitUpdateModule')) {
            TdkPlatformHelper::processCorrectModule();
            $this->_confirmations[] = 'Update and Correct Module is successful';
        } else if (Tools::isSubmit('backup')) {
            $this->processBackup();
            $this->_confirmations[] = 'Back-up to PHP file is successful';
        } else if (Tools::isSubmit('restore')) {
            $this->processRestore();
            $this->_confirmations[] = 'Restore Back-up File is successful';
        } else if (Tools::isSubmit('submitTdkPlatform')) {
            $load_owl = Tools::getValue('TDKPLATFORM_LOAD_OWL');
            $header_hook = Tools::getValue('TDKPLATFORM_HEADER_HOOK');
            $content_hook = Tools::getValue('TDKPLATFORM_CONTENT_HOOK');
            $footer_hook = Tools::getValue('TDKPLATFORM_FOOTER_HOOK');
            $product_hook = Tools::getValue('TDKPLATFORM_PRODUCT_HOOK');
            Configuration::updateValue('TDKPLATFORM_LOAD_OWL', (int)$load_owl);
            Configuration::updateValue('TDKPLATFORM_LOAD_SWIPER', Tools::getValue('TDKPLATFORM_LOAD_SWIPER'));
            Configuration::updateValue('TDKPLATFORM_LOAD_SLICK', Tools::getValue('TDKPLATFORM_LOAD_SLICK'));
            Configuration::updateValue('TDKPLATFORM_COOKIE_PROFILE', Tools::getValue('TDKPLATFORM_COOKIE_PROFILE'));
            Configuration::updateValue('TDKPLATFORM_HEADER_HOOK', $header_hook);
            Configuration::updateValue('TDKPLATFORM_CONTENT_HOOK', $content_hook);
            Configuration::updateValue('TDKPLATFORM_FOOTER_HOOK', $footer_hook);
            Configuration::updateValue('TDKPLATFORM_PRODUCT_HOOK', $product_hook);
//            Configuration::updateValue('TDKPLATFORM_LOAD_AJAX', Tools::getValue('TDKPLATFORM_LOAD_AJAX'));
            Configuration::updateValue('TDKPLATFORM_LOAD_STELLAR', Tools::getValue('TDKPLATFORM_LOAD_STELLAR'));
            Configuration::updateValue('TDKPLATFORM_LOAD_WAYPOINTS', Tools::getValue('TDKPLATFORM_LOAD_WAYPOINTS'));
            Configuration::updateValue('TDKPLATFORM_LOAD_INSTAFEED', Tools::getValue('TDKPLATFORM_LOAD_INSTAFEED'));
            Configuration::updateValue('TDKPLATFORM_LOAD_IMAGE360', Tools::getValue('TDKPLATFORM_LOAD_IMAGE360'));
            Configuration::updateValue('TDKPLATFORM_LOAD_IMAGEHOTPOT', Tools::getValue('TDKPLATFORM_LOAD_IMAGEHOTPOT'));
            Configuration::updateValue('TDKPLATFORM_LOAD_HTML5VIDEO', Tools::getValue('TDKPLATFORM_LOAD_HTML5VIDEO'));
            Configuration::updateValue('TDKPLATFORM_SAVE_MULTITHREARING', Tools::getValue('TDKPLATFORM_SAVE_MULTITHREARING'));
            Configuration::updateValue('TDKPLATFORM_SAVE_SUBMIT', Tools::getValue('TDKPLATFORM_SAVE_SUBMIT'));
            Configuration::updateValue('TDKPLATFORM_LOAD_PRODUCTZOOM', Tools::getValue('TDKPLATFORM_LOAD_PRODUCTZOOM'));

            Configuration::updateValue('TDKPLATFORM_LOAD_FULLPAGEJS', Tools::getValue('TDKPLATFORM_LOAD_FULLPAGEJS'));
            Configuration::updateValue('TDKPLATFORM_LOAD_PN', Tools::getValue('TDKPLATFORM_LOAD_PN'));
            Configuration::updateValue('TDKPLATFORM_LOAD_TRAN', Tools::getValue('TDKPLATFORM_LOAD_TRAN'));
            Configuration::updateValue('TDKPLATFORM_LOAD_IMG', Tools::getValue('TDKPLATFORM_LOAD_IMG'));
            Configuration::updateValue('TDKPLATFORM_LOAD_COUNT', Tools::getValue('TDKPLATFORM_LOAD_COUNT'));
//            Configuration::updateValue('TDKPLATFORM_LOAD_COLOR', Tools::getValue('TDKPLATFORM_LOAD_COLOR'));
            Configuration::updateValue('TDKPLATFORM_COLOR', Tools::getValue('TDKPLATFORM_COLOR'));
//            Configuration::updateValue('TDKPLATFORM_LOAD_ACOLOR', Tools::getValue('TDKPLATFORM_LOAD_ACOLOR'));
            Configuration::updateValue('TDKPLATFORM_PRODUCT_MAX_RANDOM', Tools::getValue('TDKPLATFORM_PRODUCT_MAX_RANDOM'));
            Configuration::updateValue('TDKPLATFORM_LOAD_COOKIE', Tools::getValue('TDKPLATFORM_LOAD_COOKIE'));

            // update extrafield
            $this->saveConfigExtrafields('TDKPLATFORM_PRODUCT_TEXTEXTRA', 'TDKPLATFORM_PRODUCT_EDITOREXTRA', 'product');
            $this->saveConfigExtrafields('TDKPLATFORM_CATEGORY_TEXTEXTRA', 'TDKPLATFORM_CATEGORY_EDITOREXTRA', 'category');
        }
        $create_profile_link = $this->context->link->getAdminLink('AdminTdkPlatformProfiles').'&addtdkplatform_profiles';
        $profile_link = $this->context->link->getAdminLink('AdminTdkPlatformProfiles');
        $position_link = $this->context->link->getAdminLink('AdminTdkPlatformPositions');
        $product_link = $this->context->link->getAdminLink('AdminTdkPlatformProducts');
        // $path_guide = _PS_MODULE_DIR_.$this->name.'/views/templates/admin/guide.tpl';
        // $guide_box = TdkPlatformSetting::buildGuide($this->context, $path_guide, 1);

        $module_link = $this->context->link->getAdminLink('AdminModules', false)
                .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules');
        $back_up_file = @Tools::scandir($this->backup_dir, 'php');
        arsort($back_up_file);
        $this->context->smarty->assign(array(
            // 'guide_box' => $guide_box,
            'create_profile_link' => $create_profile_link,
            'profile_link' => $profile_link,
            'position_link' => $position_link,
            'product_link' => $product_link,
            'module_link' => $module_link,
            'back_up_file' => $back_up_file,
            'backup_dir' => $this->backup_dir,
        ));
        $output = $this->generateTdkHtmlMessage();
        $output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
        Media::addJsDef(array('js_tdk_controller' => 'module_configuration'));
        return $output.$this->renderForm();
    }

    public function saveConfigExtrafields($ft, $fe, $type)
    {
        $field_text = array_unique(explode(',', Tools::getValue($ft)));
        $field_default = array('name', 'description', 'description_short', 'link_rewrite',  'meta_description', 'meta_keywords',  'meta_title', 'name', 'available_now', 'available_later', 'online_only', 'ean13', 'isbn', 'upc', 'ecotax', 'quantity', 'minimal_quantity', 'price', 'wholesale_price', 'unity', 'unit_price_ratio', 'additional_shipping_cost', 'reference', 'supplier_reference', 'location', 'width', 'height', 'depth', 'weight', 'out_of_stock', 'quantity_discount', 'customizable', 'uploadable_files', 'text_fields', 'active', 'redirect_type', 'id_type_redirected', 'available_date', 'show_condition', 'condition', 'show_price', 'indexed', 'cache_is_pack', 'cache_has_attachments', 'is_virtual', 'cache_default_attribute', 'date_add', 'date_upd', 'advanced_stock_management', 'pack_stock_type', 'state');
        $field_text_valid = array();
        foreach ($field_text as $k => $v) {
            // validate module
            unset($k);
            
            $v = str_replace(' ', '_', trim($v));
            $v = preg_replace('/[^A-Za-z0-9\_]/', '', $v);
            if ($v && !in_array($v, $field_default)) {
                $field_text_valid[] = $v;
            }
        }

        Configuration::updateValue($ft, implode(',', $field_text_valid));
        $this->processExtrafield($field_text_valid, $type, 'varchar(255)');

        $field_editor = array_unique(explode(',', Tools::getValue($fe)));
        $field_editor_valid = array();
        foreach ($field_editor as $k => $v) {
            // validate module
            unset($k);
            
            $v = str_replace(' ', '_', trim($v));
            $v = preg_replace('/[^A-Za-z0-9\_]/', '', $v);
            if ($v && !in_array($v, $field_text) && !in_array($v, $field_default)) {
                $field_editor_valid[] = $v;
            }
        }
            
        Configuration::updateValue($fe, implode(',', $field_editor_valid));
        $this->processExtrafield($field_editor_valid, $type, 'text');
    }
  
    public function processExtrafield($submit_fields, $type, $field_type)
    {
        $table = ($type == 'product')?_DB_PREFIX_.'tdkplatform_extrapro':_DB_PREFIX_.'tdkplatform_extracat';
        $id = ($type == 'product')?'id_product':'id_category';
        //$field_type = ($field_type == 'varchar(255)')?'VARCHAR(255) NULL;':'TEXT;';
        $files = array();
        //check table exist and return field
        $sql = 'SELECT * FROM information_schema.tables
                    WHERE table_schema = "'._DB_NAME_.'"
                    AND table_name = "'.pSQL($table).'"';
        $result = Db::getInstance()->getValue($sql);

        if (!empty($result)) {
            $sql = 'SHOW FIELDS FROM `'.pSQL($table) .'`';
            $result = Db::getInstance()->executeS($sql);
            foreach ($result as $value) {
                $files[$value['Field']] = $value['Type'];
            }
        } else {
            #select product layout
            $this->registerHook('actionObjectProductUpdateAfter');
            $this->registerHook('displayAdminProductsExtra');
            $this->registerHook('filterProductContent');
            #select category layout
            $this->registerHook('actionObjectCategoryUpdateAfter');
            $this->registerHook('displayBackOfficeCategory');
            $this->registerHook('filterCategoryContent');

            Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'.pSQL($table).'` (
                  `'.pSQL($id).'` int(11) unsigned NOT NULL,
                  `id_shop` int(11) unsigned NOT NULL DEFAULT \'1\',
                  `id_lang` int(10) unsigned NOT NULL,
                  PRIMARY KEY (`'.pSQL($id).'`, `id_shop`, `id_lang`)
                ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
            ');
        }

        //add field
        foreach ($submit_fields as $value) {
            if ($value && !array_key_exists($value, $files)) {
                $sql = 'ALTER TABLE `'.pSQL($table).'` ADD `'.pSQL($value).'` '.pSQL($field_type);
                //echo $sql.'<br>';
                Db::getInstance()->execute($sql);
                $files[$value] = $field_type;
            }
        }

        //delete field
        foreach ($files as $key => $value) {
            if (!in_array($key, $submit_fields) && $key != $id && $key != 'id_shop' && $key != 'id_lang' && $value == $field_type) {
                $sql = 'ALTER TABLE `'.pSQL($table).'` DROP `'.pSQL($key).'`';
                //echo $sql.'<br>';
                Db::getInstance()->execute($sql);
                unset($files[$key]);
            }
        }
        unset($files['id_product']);
        unset($files['id_category']);
        unset($files['id_shop']);
        unset($files['id_lang']);
        return implode(',', array_keys($files));
    }

    public function processRestore()
    {
        $file = Tools::getValue('backupfile');
        if (file_exists($this->backup_dir.$file)) {
            $query = $dataLang = '';
            require_once($this->backup_dir.$file);
            if (isset($query) && !empty($query)) {
                $query = str_replace('_DB_PREFIX_', _DB_PREFIX_, $query);
                $query = str_replace('_MYSQL_ENGINE_', _MYSQL_ENGINE_, $query);
                $query = str_replace('TDK_ID_SHOP', (int)Context::getContext()->shop->id, $query);
                $query = str_replace("\\'", "\'", $query);

                $db_data_settings = preg_split("/;\s*[\r\n]+/", $query);
                foreach ($db_data_settings as $query) {
                    $query = trim($query);
                    if (!empty($query)) {
                        if (!Db::getInstance()->Execute($query)) {
                            $this->_html['error'][] = 'Can not restore for '.$this->name;
                            return false;
                        }
                    }
                }

                if (isset($dataLang) && !empty($dataLang)) {
                    $languages = Language::getLanguages(true, Context::getContext()->shop->id);
                    foreach ($languages as $lang) {
                        if (isset($dataLang[Tools::strtolower($lang['iso_code'])])) {
                            $query = str_replace('_DB_PREFIX_', _DB_PREFIX_, $dataLang[Tools::strtolower($lang['iso_code'])]);
                            //if not exist language in list, get en
                        } else {
                            if (isset($dataLang['en'])) {
                                $query = str_replace('_DB_PREFIX_', _DB_PREFIX_, $dataLang['en']);
                            } else {
                                //firt item in array
                                foreach (array_keys($dataLang) as $key) {
                                    $query = str_replace('_DB_PREFIX_', _DB_PREFIX_, $dataLang[$key]);
                                    break;
                                }
                            }
                        }
                        $query = str_replace('_MYSQL_ENGINE_', _MYSQL_ENGINE_, $query);
                        $query = str_replace('TDK_ID_SHOP', (int)Context::getContext()->shop->id, $query);
                        $query = str_replace('TDK_ID_LANGUAGE', (int)$lang['id_lang'], $query);
                        $query = str_replace("\\\'", "\'", $query);

                        $db_data_settings = preg_split("/;\s*[\r\n]+/", $query);
                        foreach ($db_data_settings as $query) {
                            $query = trim($query);
                            if (!empty($query)) {
                                if (!Db::getInstance()->Execute($query)) {
                                    $this->_html['error'][] = 'Can not restore for data lang '.$this->name;
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function processBackup()
    {
        $install_folder = $this->backup_dir;
        
        if (!is_dir($install_folder)) {
            mkdir($install_folder, 0755, true);
        }
        $list_table = Db::getInstance()->executeS("SHOW TABLES LIKE '%tdkplatform%'");

        $create_table = '';
        $data_with_lang = '';
        $backup_file = $install_folder.$this->name.date('_Y_m_d_H_i_s').'.php';
        $fp = @fopen($backup_file, 'w');
        if ($fp === false) {
            die('Unable to create backup file '.addslashes($backup_file));
        }

        fwrite($fp, '<?php');
        fwrite($fp, "\n/* back up for module ".$this->name." */\n");

        $data_language = array();
        $list_lang = array();
        $languages = Language::getLanguages(true, Context::getContext()->shop->id);
        foreach ($languages as $lang) {
            $list_lang[$lang['id_lang']] = $lang['iso_code'];
        }

        foreach ($list_table as $table) {
            $table = current($table);
            $table_name = str_replace(_DB_PREFIX_, '_DB_PREFIX_', $table);
            // Skip tables which do not start with _DB_PREFIX_
            if (Tools::strlen($table) < Tools::strlen(_DB_PREFIX_) || strncmp($table, _DB_PREFIX_, Tools::strlen(_DB_PREFIX_)) != 0) {
                continue;
            }
            $schema = Db::getInstance()->executeS('SHOW CREATE' . ' TABLE `'.pSQL($table).'`');
            if (count($schema) != 1 || !isset($schema[0]['Table']) || !isset($schema[0]['Create Table'])) {
                fclose($fp);
                die($this->l('An error occurred while backing up. Unable to obtain the schema of').' '.$table);
            }
            $create_table .= 'DROP TABLE IF EXISTS `'.$table_name."`;\n".$schema[0]['Create Table'].";\n";

            if (strpos($schema[0]['Create Table'], '`id_shop`')) {
                $data = Db::getInstance()->query('SELECT * FROM `'.pSQL($schema[0]['Table']).'` WHERE `id_shop`='.(int)Context::getContext()->shop->id, false);
            } else {
                $data = Db::getInstance()->query('SELECT * FROM `'.pSQL($schema[0]['Table']).'`', false);
            }

            $sizeof = DB::getInstance()->NumRows();
            $lines = explode("\n", $schema[0]['Create Table']);

            if ($data && $sizeof > 0) {
                //if table is language
                $id_language = 0;
                if (strpos($schema[0]['Table'], 'lang') !== false) {
                    $data_language[$schema[0]['Table']] = array();
                    $i = 1;
                    while ($row = DB::getInstance()->nextRow($data)) {
                        $s = '(';
                        foreach ($row as $field => $value) {
                            if ($field == 'id_lang') {
                                $id_language = $value;
                                $tmp = "'".pSQL('TDK_ID_LANGUAGE', true)."',";
                            } else if ($field == 'ID_SHOP') {
                                $tmp = "'".pSQL('ID_SHOP', true)."',";
                            } else {
                                $tmp = "'".pSQL($value, true)."',";
                            }

                            if ($tmp != "'',") {
                                $s .= $tmp;
                            } else {
                                foreach ($lines as $line) {
                                    if (strpos($line, '`'.$field.'`') !== false) {
                                        if (preg_match('/(.*NOT NULL.*)/Ui', $line)) {
                                            $s .= "'',";
                                        } else {
                                            $s .= 'NULL,';
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                        if (!isset($list_lang[$id_language])) {
                            continue;
                        }

                        if (!isset($data_language[$schema[0]['Table']][Tools::strtolower($list_lang[$id_language])])) {
                            $data_language[$schema[0]['Table']][Tools::strtolower($list_lang[$id_language])] = 'INSERT INTO `'.$table_name."` VALUES\n";
                        }

                        $s = rtrim($s, ',');
                        if ($i % 200 == 0 && $i < $sizeof) {
                            $s .= ");\nINSERT INTO `".$table_name."` VALUES\n";
                        } else {
                            $s .= "),\n";
                        }
                        $data_language[$schema[0]['Table']][Tools::strtolower($list_lang[$id_language])] .= $s;
                    }
                } else {
                    //normal table
                    $create_table .= $this->createInsert($data, $table_name, $lines, $sizeof);
                }
            }
        }

        $create_table = str_replace('$', '\$', $create_table);
        $create_table = '$query = "'.$create_table;
        //foreach by table
        $tpl = array();

        fwrite($fp, $create_table."\";\n");
        if ($data_language) {
            foreach ($data_language as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    if (!isset($tpl[$key1])) {
                        $tpl[$key1] = Tools::substr($value1, 0, -2).";\n";
                    } else {
                        $tpl[$key1] .= Tools::substr($value1, 0, -2).";\n";
                    }
                }
            }
            foreach ($tpl as $key => $value) {
                if ($data_with_lang) {
                    $data_with_lang .= ',"'.$key.'"=>'.'"'.$value.'"';
                } else {
                    $data_with_lang .= '"'.$key.'"=>'.'"'.$value.'"';
                }
            }

            //delete base uri when export
            $data_with_lang = str_replace('$', '\$', $data_with_lang);
            $data_with_lang = '$dataLang = Array('.$data_with_lang;

            fwrite($fp, $data_with_lang.');');
        }
        fclose($fp);
    }
    
    /**
     * sub function of back-up database
     */
    public function createInsert($data, $table_name, $lines, $sizeof)
    {
        $data_no_lang = 'INSERT INTO `'.$table_name."` VALUES\n";
        $i = 1;
        while ($row = DB::getInstance()->nextRow($data)) {
            $s = '(';
            foreach ($row as $field => $value) {
                if ($field == 'ID_SHOP') {
                    $tmp = "'".pSQL('ID_SHOP', true)."',";
                } else {
                    $tmp = "'".pSQL($value, true)."',";
                }
                if ($tmp != "'',") {
                    $s .= $tmp;
                } else {
                    foreach ($lines as $line) {
                        if (strpos($line, '`'.$field.'`') !== false) {
                            if (preg_match('/(.*NOT NULL.*)/Ui', $line)) {
                                $s .= "'',";
                            } else {
                                $s .= 'NULL,';
                            }
                            break;
                        }
                    }
                }
            }
            $s = rtrim($s, ',');
            if ($i % 200 == 0 && $i < $sizeof) {
                $s .= ");\nINSERT INTO `".$table_name."` VALUES\n";
            } elseif ($i < $sizeof) {
                $s .= "),\n";
            } else {
                $s .= ");\n";
            }
            $data_no_lang .= $s;

            ++$i;
        }
        return $data_no_lang;
    }

    public function renderForm()
    {
        $list_all_hooks = $this->renderListAllHook(TdkPlatformSetting::getHook('all'));
        $list_header_hooks = (Configuration::get('TDKPLATFORM_HEADER_HOOK')) ?
                Configuration::get('TDKPLATFORM_HEADER_HOOK') : implode(',', TdkPlatformSetting::getHook('header'));
        $list_content_hooks = (Configuration::get('TDKPLATFORM_CONTENT_HOOK')) ?
                Configuration::get('TDKPLATFORM_CONTENT_HOOK') : implode(',', TdkPlatformSetting::getHook('content'));
        $list_footer_hooks = (Configuration::get('TDKPLATFORM_FOOTER_HOOK')) ?
                Configuration::get('TDKPLATFORM_FOOTER_HOOK') : implode(',', TdkPlatformSetting::getHook('footer'));
        $list_product_hooks = (Configuration::get('TDKPLATFORM_PRODUCT_HOOK')) ?
                Configuration::get('TDKPLATFORM_PRODUCT_HOOK') : implode(',', TdkPlatformSetting::getHook('product'));
        $form_general = array(
            'legend' => array(
                'title' => $this->l('General Settings'),
                'icon' => 'icon-cogs'
            ),
            'input' => array(
                array(
                    'type' => 'html',
                    'name' => 'default_html',
                    'name' => 'dump_name',
                    'html_content' => '<div class="alert alert-info tdk-html-full">'
                    .$this->l('Loading JS and CSS library').'</div>',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Load Jquery Stellar Library'),
                    'name' => 'TDKPLATFORM_LOAD_STELLAR',
                    'desc' => $this->l('This script is use for parallax. If you load it in other plugin please turn it off'),
                    'is_bool' => true,
                    'values' => TdkPlatformSetting::returnYesNo(),
                    'default' => '0',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Load Owl Carousel Library'),
                    'name' => 'TDKPLATFORM_LOAD_OWL',
                    'desc' => $this->l('This script is use for Carousel. If you load it in other plugin please turn it off'),
                    'is_bool' => true,
                    'values' => TdkPlatformSetting::returnYesNo(),
                    'default' => '0',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Load Swiper Carousel Library'),
                    'name' => 'TDKPLATFORM_LOAD_SWIPER',
                    'desc' => $this->l('This script is use for Carousel. If you load it in other plugin please turn it off'),
                    'is_bool' => true,
                    'values' => TdkPlatformSetting::returnYesNo(),
                    'default' => '0',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Load Waypoints Library'),
                    'name' => 'TDKPLATFORM_LOAD_WAYPOINTS',
                    'desc' => $this->l('This script is use for Animated. If you load it in other plugin please turn it off'),
                    'is_bool' => true,
                    'values' => TdkPlatformSetting::returnYesNo(),
                    'default' => '0',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Load Instafeed Library'),
                    'name' => 'TDKPLATFORM_LOAD_INSTAFEED',
                    'default' => 0,
                    'values' => TdkPlatformSetting::returnYesNo(),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Load Video HTML5 Library'),
                    'name' => 'TDKPLATFORM_LOAD_HTML5VIDEO',
                    'default' => 0,
                    'values' => TdkPlatformSetting::returnYesNo(),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Load Full Page Library'),
                    'name' => 'TDKPLATFORM_LOAD_FULLPAGEJS',
                    'default' => 0,
                    'values' => TdkPlatformSetting::returnYesNo(),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Load Image360 Library'),
                    'name' => 'TDKPLATFORM_LOAD_IMAGE360',
                    'default' => 0,
                    'values' => TdkPlatformSetting::returnYesNo(),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Load Image Hotpot Library'),
                    'name' => 'TDKPLATFORM_LOAD_IMAGEHOTPOT',
                    'default' => 0,
                    'values' => TdkPlatformSetting::returnYesNo(),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Load Cookie Library'),
                    'name' => 'TDKPLATFORM_LOAD_COOKIE',
                    'default' => 0,
                    'values' => TdkPlatformSetting::returnYesNo(),
                    'desc' => $this->l('Yes : Load library JS jquery.cooki-plugin.js'),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Load Product Zoom Library'),
                    'name' => 'TDKPLATFORM_LOAD_PRODUCTZOOM',
                    'default' => 1,
                    'values' => TdkPlatformSetting::returnYesNo(),
                ),
                array(
                    'type' => 'html',
                    'name' => 'default_html',
                    'name' => 'dump_name',
                    'html_content' => '<div class="alert alert-info tdk-html-full">'
                    .$this->l('Functions').'</div>',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Save Profile Multithrearing'),
                    'name' => 'TDKPLATFORM_SAVE_MULTITHREARING',
                    'default' => 0,
                    'values' => TdkPlatformSetting::returnYesNo(),
                    'desc' => $this->l('Yes - use AJAX SUBMIT, not load page again, keep your data safe'),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Save Profile Submit'),
                    'name' => 'TDKPLATFORM_SAVE_SUBMIT',
                    'default' => 0,
                    'values' => TdkPlatformSetting::returnYesNo(),
                    'desc' => $this->l('Yes - use Normal SUBMIT and load page again. No - use AJAX SUBMIT, not load page again.'),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Save profile and postion id to cookie'),
                    'name' => 'TDKPLATFORM_COOKIE_PROFILE',
                    'default' => 0,
                    'desc' => $this->l('That is only for demo, please turn off it in live site'),
                    'values' => TdkPlatformSetting::returnYesNo(),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Random Product'),
                    'desc' => $this->l('Number of time create random product when using Prestashop_CACHED and showing product carousel has order by RANDOM'),
                    'name' => 'TDKPLATFORM_PRODUCT_MAX_RANDOM',
                    'class' => '',
                    'default' => 2,
                ),
                array(
                    'type' => 'html',
                    'name' => 'default_html',
                    'name' => 'dump_name',
                    'html_content' => '<div class="alert alert-info tdk-html-full">'
                    .$this->l('AJAX SETTINGS').
                    '<input type="hidden" id="position-hook-select"/></div>',
                ),
//                array(
//                    'type' => 'switch',
//                    'label' => $this->l('Use Ajax Feature'),
//                    'name' => 'TDKPLATFORM_LOAD_AJAX',
//                    'default' => 1,
//                    'values' => TdkPlatformSetting::returnYesNo(),
//                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('AJAX Show Category Qty - Enable'),
                    'name' => 'TDKPLATFORM_LOAD_PN',
                    'default' => 0,
                    'values' => TdkPlatformSetting::returnYesNo(),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('AJAX Show Category Qty - Code'),
                    'name' => 'TDKPLATFORM_LOAD_TPN',
                    'cols' => 100,
                    'hint' => $this->l('Add this CODE to THEME_NAME/modules/ps_categorytree/views/templates/hook/ ps_categorytree.tpl file'),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('AJAX Show More Product Image - Enable'),
                    'name' => 'TDKPLATFORM_LOAD_TRAN',
                    'default' => 0,
                    'values' => TdkPlatformSetting::returnYesNo(),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('AJAX Show More Product Image - Code'),
                    'name' => 'TDKPLATFORM_LOAD_TTRAN',
                    'cols' => 100,
                    'hint' => $this->l('Add this CODE to THEME_NAME/templates/catalog/_partials/miniatures/product.tpl file'),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('AJAX Show Multiple Product Images - Enable'),
                    'name' => 'TDKPLATFORM_LOAD_IMG',
                    'default' => 0,
                    'values' => TdkPlatformSetting::returnYesNo(),
                ),
                array(
                    'type' => 'textarea',
//                    'label' => $this->l('You can add this code in tpl file of module you want to show Multiple Product Image'),
                    'label' => $this->l('AJAX Show Multiple Product Images - Code'),
                    'name' => 'TDKPLATFORM_LOAD_TIMG',
                    'cols' => 100,
                    'hint' => $this->l('Add this CODE to THEME_NAME/templates/catalog/_partials/miniatures/product.tpl file'),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('AJAX Show Count Down Product - Enable'),
                    'name' => 'TDKPLATFORM_LOAD_COUNT',
                    'default' => 0,
                    'values' => TdkPlatformSetting::returnYesNo(),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('AJAX Show Count Down Product  - Code'),
                    'name' => 'TDKPLATFORM_LOAD_TCOUNT',
                    'cols' => 100,
                    'hint' => $this->l('Add this CODE to THEME_NAME/templates/catalog/_partials/miniatures/product.tpl file'),
                ),
//                array(
//                    'type' => 'switch',
//                    'label' => $this->l('Show Discount Color'),
//                    'name' => 'TDKPLATFORM_LOAD_ACOLOR',
//                    'default' => 1,
//                    'values' => TdkPlatformSetting::returnYesNo(),
//                ),
//                array(
//                    'type' => 'textarea',
//                    'label' => $this->l('You can add this code in tpl file of module you want to show color discount'),
//                    'name' => 'TDKPLATFORM_LOAD_TCOLOR',
//                    'cols' => 100,
//                ),
//                array(
//                    'type' => 'textarea',
//                    'label' => $this->l('For color (Ex: 10:#ff0000,20:#152ddb,40:#ffee001) '),
//                    'name' => 'TDKPLATFORM_LOAD_COLOR',
//                    'cols' => 100
//                ),
//                array(
//                    'type' => 'textarea',
//                    'label' => $this->l('If you want my script run fine with Layered navigation block.
//                                Please copy to override file modules/blocklayered/blocklayered.js to folder
//                                themes/TEMPLATE_NAME/js/modules/blocklayered/blocklayered.js.
//                                Then find function reloadContent(params_plus).'),
//                    'name' => 'TDKPLATFORM_LOAD_RTN',
//                    'cols' => 100
//                ),
//                array(
//                    'type' => 'textarea',
//                    'label' => $this->l('For color (Ex: 10:#ff0000,20:#152ddb,40:#ffee001)'),
//                    'name' => 'TDKPLATFORM_COLOR',
//                    'default' => '',
//                    'cols' => 100
//                ),
                array(
                    'type' => 'html',
                    'name' => 'default_html',
                    'name' => 'dump_name',
                    'html_content' => '<br/><div class="alert alert-info tdk-html-full">'
                    .$this->l('Setting hook in positions (This setting will apply for all profiles).')
                    .'<div class="list-all-hooks">'.$this->l('Default all hooks: [').$list_all_hooks.' ]</div>'
                    .'</div>',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Hooks in header'),
                    'name' => 'TDKPLATFORM_HEADER_HOOK',
                    'class' => '',
                    'default' => $list_header_hooks
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Hooks in content'),
                    'name' => 'TDKPLATFORM_CONTENT_HOOK',
                    'class' => '',
                    'default' => $list_content_hooks
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Hooks in footer'),
                    'name' => 'TDKPLATFORM_FOOTER_HOOK',
                    'class' => '',
                    'default' => $list_footer_hooks
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Hooks in product-footer'),
                    'name' => 'TDKPLATFORM_PRODUCT_HOOK',
                    'class' => '',
                    'default' => $list_product_hooks
                ),
                array(
                    'type' => 'html',
                    'name' => 'default_html',
                    'name' => 'html_content_de',
                    'html_content' => '<input type="hidden" name="hook_header_old" id="hook_header_old"/>
                                                <input type="hidden" name="hook_content_old" id="hook_content_old"/>
                                                <input type="hidden" name="hook_footer_old" id="hook_footer_old"/>
                                                <input type="hidden" name="hook_product_old" id="hook_product_old"/>
                                                <input type="hidden" name="is_change" id="is_change" value=""/>
                                                <input type="hidden" id="message_confirm" value="'
                    .$this->l('The hook is changing. Click OK will save new config hooks and will
                                                                    REMOVE ALL current data widget. Are you sure?').'"/>',
                ),
            ),
            'submit' => array(
                'id' => 'btn-save-tdkplatform',
                'title' => $this->l('Save'),
            )
        );
     
        $form_extrafield = array(

            'legend' => array(
                'title' => $this->l('Extrafield Settings'),
                'icon' => 'icon-cogs'
            ),
            'input' => array(
                array(
                    'type' => 'html',
                    'name' => 'default_html',
                    'name' => 'dump_name',
                    'html_content' => '<br/><div class="alert alert-info tdk-html-full">'
                    .$this->l('This config is only apply for advance user.')
                    .'<div class="list-all-hooks">We will create new fileds for category and product, then you can use it to show in layout</div>'
                    .'<div class="list-all-hooks">Have 2 type of database: 1. text and 2. editor</div>'
                    .'<div class="list-all-hooks">When you change data of this field, all data of extrafied will lost</div>'
                    .'</div>',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Product Extra Text Field'),
                    'name' => 'TDKPLATFORM_PRODUCT_TEXTEXTRA',
                    'class' => '',
                    'desc'  => $this->l('Do not contain space and special charactor. Example: sub-name, sub-title'),
                    'default' => ''
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Product Extra Editor Field'),
                    'name' => 'TDKPLATFORM_PRODUCT_EDITOREXTRA',
                    'class' => '',
                    'desc'  => $this->l('Do not contain space and special charactor. Example: sub-name, sub-title'),
                    'default' => ''
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Category Extra Editor Field'),
                    'name' => 'TDKPLATFORM_CATEGORY_TEXTEXTRA',
                    'class' => '',
                    'desc'  => $this->l('Do not contain space and special charactor. Example: sub-name, sub-title'),
                    'default' => ''
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Category Extra Editor Field'),
                    'name' => 'TDKPLATFORM_CATEGORY_EDITOREXTRA',
                    'class' => '',
                    'desc'  => $this->l('Do not contain space and special charactor. Example: sub-name, sub-title'),
                    'default' => ''
                )
            ),
            'submit' => array(
                'id' => 'btn-save-extrafield',
                'title' => $this->l('Save'),
            )
        );
        $fields_form = array(
            'form' => $form_general
        );
        $fields_form1 = array(
            'form' => $form_extrafield
        );
        $data = $this->getConfigFieldsValues($form_general, $form_extrafield);
        // Check existed the folder root store resources of module
        $path_img = TdkPlatformHelper::getImgThemeDir();
        if (!file_exists($path_img)) {
            mkdir($path_img, 0755, true);
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
                Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitTdkPlatform';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
                .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $data,
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        return $helper->generateForm(array($fields_form, $fields_form1));
    }

    private function renderListAllHook($arr)
    {
        $html = '';
        if ($arr) {
            foreach ($arr as $item) {
                $html .= "<a href='javascript:;'>$item</a>";
            }
        }
        return $html;
    }

    public function hookTdkPlatformConfig($param)
    {
        $cf = $param['configName'];
        if ($cf == 'profile' || $cf == 'header' || $cf == 'footer' || $cf == 'content' || $cf == 'product' || $cf == 'product_builder') {
            #GET DETAIL PROFILE
            $cache_name = 'tdkplatformConfig'.'|'.$param['configName'];
            $cache_id = $this->getCacheId($cache_name);
            if (!$this->isCached('module:tdkplatform/views/templates/hook/config.tpl', $cache_id)) {
                $tdk_type = $cf;

                if ($cf == 'profile') {
                    $tdk_type = 'id_tdkplatform_profiles';
                } else if ($cf == 'product_builder') {
                    $tdk_type = 'plist_key';
                }
                $this->smarty->assign(
                    array(
                        'tdk_cfdata' => $this->getConfigData($cf),
                        'tdk_cf' => $cf,
                        'tdk_type' => $tdk_type,
                        'tdk_controller' => TdkPlatformHelper::getPageName(),
                        'tdk_current_url' => Context::getContext()->link->getPageLink('index', true),
                    )
                );
            }
            return $this->display(__FILE__, 'config.tpl', $cache_id);
        }

        if (!$this->product_active) {
            $this->product_active = TdkPlatformProductsModel::getActive();
        }
        if ($cf == 'productClass') {
            // validate module
            return $this->product_active['class'];
        }
        if ($cf == 'productKey') {
            $tpl_file = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/tdkplatform/views/templates/front/profiles/' . $this->product_active['plist_key'].'.tpl';
            if (is_file($tpl_file)) {
                return $this->product_active['plist_key'];
            }
            return;
        }

        if ($cf == 'productLayout') {
            $id_product = Tools::getValue('id_product');
            if ($id_product) {
                $id_shop = Context::getContext()->shop->id;
                $sql = 'SELECT page from `'._DB_PREFIX_.'tdkplatform_page` where id_product = \''.(int)$id_product.'\' AND id_shop = \''.(int)$id_shop.'\'';
                $layout = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
                return $layout;
            } else {
                return '';
            }
        }
        if ($cf == 'categoryLayout') {
            $id_category = Tools::getValue('id_category');
            if ($id_category) {
                $id_shop = Context::getContext()->shop->id;
                $sql = 'SELECT page from `'._DB_PREFIX_.'tdkplatform_page` where id_category = \''.(int)$id_category.'\' AND id_shop = \''.(int)$id_shop.'\'';
                $layout = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
                return $layout;
            } else {
                return '';
            }
        }
        //TDK:: get class of category layout
        if ($cf == 'classCategoryLayout') {
            $id_category = Tools::getValue('id_category');
            if ($id_category) {
                $id_shop = Context::getContext()->shop->id;
                $sql = 'SELECT pr.`class` from `'._DB_PREFIX_.'tdkplatform_page` pa INNER JOIN `'._DB_PREFIX_.'tdkplatform_products` pr ON (pa.`page` = pr.`plist_key`) where pa.id_category = \''.(int)$id_category.'\' AND pa.id_shop = \''.(int)$id_shop.'\'';
                $class_category_layout = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
                return $class_category_layout;
            } else {
                return '';
            }
        }
    }

    public function getConfigData($cf)
    {
        if ($cf == 'profile') {
            $id_lang = (int)Context::getContext()->language->id;
            $sql = 'SELECT p.`id_tdkplatform_profiles` AS `id`, p.`name`, ps.`active`, pl.friendly_url FROM `'._DB_PREFIX_.'tdkplatform_profiles` p '
                    .' INNER JOIN `'._DB_PREFIX_.'tdkplatform_profiles_shop` ps ON (ps.`id_tdkplatform_profiles` = p.`id_tdkplatform_profiles`)'
                    .' INNER JOIN `'._DB_PREFIX_.'tdkplatform_profiles_lang` pl ON (pl.`id_tdkplatform_profiles` = p.`id_tdkplatform_profiles`) AND pl.id_lang='.(int)$id_lang
                    .' WHERE ps.id_shop='.(int)Context::getContext()->shop->id;
        } else if ($cf == 'product_builder') {
            $sql = 'SELECT p.`plist_key` AS `id`, p.`name`, ps.`active`'
                    .' FROM `'._DB_PREFIX_.'tdkplatform_products` p '
                    .' INNER JOIN `'._DB_PREFIX_.'tdkplatform_products_shop` ps '
                    .' ON (ps.`id_tdkplatform_products` = p.`id_tdkplatform_products`)'
                    .' WHERE ps.id_shop='.(int)Context::getContext()->shop->id;
        } else {
            $sql = 'SELECT p.`id_tdkplatform_positions` AS `id`, p.`name`'
                    .' FROM `'._DB_PREFIX_.'tdkplatform_positions` p '
                    .' INNER JOIN `'._DB_PREFIX_.'tdkplatform_positions_shop` ps '
                    .' ON (ps.`id_tdkplatform_positions` = p.`id_tdkplatform_positions`)'
                    .' WHERE p.`position` = \''.PSQL($cf).'\' AND ps.id_shop='.(int)Context::getContext()->shop->id;
        }
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        foreach ($result as &$val) {
            if ($cf == 'profile') {
                $val['active'] = 0;
                if ($val['id'] == $this->profile_data['id_tdkplatform_profiles']) {
                    $val['active'] = 1;
                }
            } else if ($cf == 'product_builder') {
                if (Tools::getIsset('plist_key')) {
                    $val['active'] = 0;
                    if ($val['id'] == Tools::getValue('plist_key')) {
                        $val['active'] = 1;
                    }
                }
            } else {
                $val['active'] = 0;
                if (Tools::getIsset($cf)) {
                    if ($val['id'] == Tools::getValue($cf)) {
                        $val['active'] = 1;
                    }
                } else {
                    if ($val['id'] == $this->profile_data[$cf]) {
                        $val['active'] = 1;
                    }
                }
            }
        }
        return $result;
    }

    public function getConfigFieldsValues($form_general, $form_extrafield)
    {
        $this->context->controller->addCss(TdkPlatformHelper::getCssDir().'style.css');
        $result = array();
       
        foreach ($form_general['input'] as $form) {
            //$form['name'] = isset($form['name']) ? $form['name'] : '';
            if (Configuration::hasKey($form['name'])) {
                $result[$form['name']] = Tools::getValue($form['name'], Configuration::get($form['name']));
            } else {
                $result[$form['name']] = Tools::getValue($form['name'], isset($form['default']) ? $form['default'] : '');
            }
        }
       
        foreach ($form_extrafield['input'] as $form) {
            //$form['name'] = isset($form['name']) ? $form['name'] : '';
            if (Configuration::hasKey($form['name'])) {
                $result[$form['name']] = Configuration::get($form['name']);
            } else {
                $result[$form['name']] = Tools::getValue($form['name'], isset($form['default']) ? $form['default'] : '');
            }
        }

        $result['TDKPLATFORM_LOAD_TCOUNT'] = '<div class="tdk-more-cdown" data-idproduct="{$product.id_product}"></div>';
        $result['TDKPLATFORM_LOAD_TTRAN'] = '<span class="product-additional" data-idproduct="{$product.id_product}"></span>';
        $result['TDKPLATFORM_LOAD_TIMG'] = '<div class="tdk-more-info" data-idproduct="{$product.id_product}"></div>';
        $result['TDKPLATFORM_LOAD_TPN'] = '<span id="tdk-cat-{$node.id}" style="display:none" class="tdk-qty tdk-cat-{$node.id} badge pull-right" data-str="{l s=\' item(s)\' d=\'Shop.Theme.Catalog\'}"></span>';

        return $result;
    }

    /**
     * Widget TdkTab
     */
    public function fontContent($assign, $tpl_name)
    {
        // Setting live edit mode
        $assign['tdkLiveEdit'] = '';
        $assign['tdkLiveEditEnd'] = '';
        $is_live = Tools::getIsset('tdk_live_edit') ? Tools::getValue('tdk_live_edit') : '';
        if ($is_live) {
            $live_token = Tools::getIsset('liveToken') ? Tools::getValue('liveToken') : '';
            if (!$this->checkLiveEditAccess($live_token)) {
                Tools::redirect('index.php?controller=404');
            }
            $cookie = new Cookie('url_live_back');
            $url_edit_profile = $cookie->variable_name;

            $id_profile = TdkPlatformProfilesModel::getIdProfileFromRewrite();
            $cookie = new Cookie('tdk_id_profile');
            if (!$id_profile) {
                if ($cookie->variable_name) {
                    $url_edit_profile .= '&id_tdkplatform_profiles='.$cookie->variable_name;
                }
            } else {
                $id_profile = '';
                // Restor id_profile to cookie
                $cookie = new Cookie('tdk_id_profile');
                $cookie->setExpire(time() + 60 * 60);
                $cookie->variable_name = $id_profile;
                $cookie->write();
                $url_edit_profile .= '&id_tdkplatform_profiles='.$id_profile;
            }
            $assign['urlEditProfile'] = $url_edit_profile;
            $assign['isLive'] = $is_live;
            $assign['tdkLiveEdit'] = '<div class="cover-live-edit"><a class="link-to-back-end" href="';
            if (isset($assign['formAtts']) && isset($assign['formAtts']['form_id']) && $assign['formAtts']['form_id']) {
                // validate module
                $assign['tdkLiveEdit'] .= $url_edit_profile.'#'.$assign['formAtts']['form_id'];
            } else {
                $assign['tdkLiveEdit'] .= $url_edit_profile;
            }
            $assign['tdkLiveEdit'] .= '" target="_blank"><i class="icon-pencil"></i> <span>Edit</span></a>';
            $assign['tdkLiveEditEnd'] = '</div>';
        }
        
        if ($assign) {
            foreach ($assign as $key => $ass) {
                $this->smarty->assign(array($key => $ass));
            }
        }
        $override_folder = '';
        if (isset($assign['formAtts']['override_folder']) && $assign['formAtts']['override_folder'] != '') {
            $override_folder = $assign['formAtts']['override_folder'];
        }
        $tpl_file = TdkPlatformHelper::getTemplate($tpl_name, $override_folder);
        $content = $this->display(__FILE__, $tpl_file);
        return $content;
    }

    public function checkLiveEditAccess($live_token)
    {
        $cookie = new Cookie('tdk_token');
        return $live_token === $cookie->variable_name;
    }

    /**
     * $page_number = 0, $nb_products = 10, $count = false, $order_by = null, $order_way = null
     */
    public function getProductsFont($params)
    {
        $id_lang = $this->context->language->id;
        $context = Context::getContext();
        //assign value from params
        $p = isset($params['page_number']) ? $params['page_number'] : 1;
        if ($p < 0) {
            $p = 1;
        }
        $n = isset($params['nb_products']) ? $params['nb_products'] : 10;
        if ($n < 1) {
            $n = 10;
        }
        $order_by = isset($params['order_by']) ? Tools::strtolower($params['order_by']) : 'position';
        $order_way = isset($params['order_way']) ? $params['order_way'] : 'ASC';
        $random = false;
        if ($order_way == 'random') {
            $random = true;
        }
        $get_total = isset($params['get_total']) ? $params['get_total'] : false;
        $order_by_prefix = false;
        if ($order_by == 'id_product' || $order_by == 'date_add' || $order_by == 'date_upd') {
            $order_by_prefix = 'product_shop';
        } else if ($order_by == 'reference') {
            $order_by_prefix = 'p';
        } else if ($order_by == 'name') {
            $order_by_prefix = 'pl';
        } elseif ($order_by == 'manufacturer') {
            $order_by_prefix = 'm';
            $order_by = 'name';
        } elseif ($order_by == 'position') {
            $order_by = 'date_add';
            $order_by_prefix = 'product_shop';
//            $order_by_prefix = 'cp';
        }
        if ($order_by == 'price') {
            $order_by = 'orderprice';
        }
        $active = 1;
        if (!Validate::isBool($active) || !Validate::isOrderBy($order_by)) {
            die(Tools::displayError());
        }
        //build where
        $where = '';
        $sql_join = '';
        $sql_group = '';

        $value_by_categories = isset($params['value_by_categories']) ? $params['value_by_categories'] : 0;
        if ($value_by_categories) {
            $id_categories = isset($params['categorybox']) ? $params['categorybox'] : '';
            if (isset($params['category_type']) && $params['category_type'] == 'default') {
                $where .= ' AND product_shop.`id_category_default` '.(strpos($id_categories, ',') === false ?
                                '= '.(int)$id_categories : 'IN ('.$id_categories.')');
            } else {
                $sql_join .= ' INNER JOIN '._DB_PREFIX_.'category_product cp ON (cp.id_product= p.`id_product` )';
                
                $where .= ' AND cp.`id_category` '.(strpos($id_categories, ',') === false ?
                                '= '.(int)$id_categories : 'IN ('.$id_categories.')');
                $sql_group = ' GROUP BY p.id_product';
            }
        }
        $value_by_supplier = isset($params['value_by_supplier']) ? $params['value_by_supplier'] : 0;
        if ($value_by_supplier && isset($params['supplier'])) {
            $id_suppliers = $params['supplier'];
            $where .= ' AND p.id_supplier '.(strpos($id_suppliers, ',') === false ? '= '.(int)$id_suppliers : 'IN ('.$id_suppliers.')');
        }
        $value_by_product_id = isset($params['value_by_product_id']) ? $params['value_by_product_id'] : 0;
        if ($value_by_product_id && isset($params['product_id'])) {
            $temp = explode(',', $params['product_id']);
            foreach ($temp as $key => $value) {
                // validate module
                $temp[$key] = (int)$value;
            }

            $product_id = implode(',', array_map('intval', $temp));
            $where .= ' AND p.id_product '.(strpos($product_id, ',') === false ? '= '.(int)$product_id : 'IN ('.pSQL($product_id).')');
        }

        $value_by_manufacture = isset($params['value_by_manufacture']) ? $params['value_by_manufacture'] : 0;
        if ($value_by_manufacture && isset($params['manufacture'])) {
            $id_manufactures = $params['manufacture'];
            $where .= ' AND p.id_manufacturer '.(strpos($id_manufactures, ',') === false ? '= '.(int)$id_manufactures : 'IN ('.pSQL($id_manufactures).')');
        }
        $product_type = isset($params['product_type']) ? $params['product_type'] : '';
        $value_by_product_type = isset($params['value_by_product_type']) ? $params['value_by_product_type'] : 0;
        if ($value_by_product_type && $product_type == 'new_product') {
            $where .= ' AND product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ?
                                            (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'"';
        }
        //home feature
        if ($value_by_product_type && $product_type == 'home_featured') {
            $ids = array();
            $category = new Category(Context::getContext()->shop->getCategory(), (int)Context::getContext()->language->id);
            $result = $category->getProducts((int)Context::getContext()->language->id, 1, $n*($p+1), 'position');   // Load more product $n*$p, hidden
            foreach ($result as $product) {
                $ids[$product['id_product']] = 1;
            }
            $ids = array_keys($ids);
            sort($ids);
            $ids = count($ids) > 0 ? implode(',', $ids) : 'NULL';
            $where .= ' AND p.`id_product` IN ('.$ids.')';
        }
        //special or price drop
        if ($value_by_product_type && $product_type == 'price_drop') {
            $current_date = date('Y-m-d H:i:s');
            $id_address = $context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
            $ids = Address::getCountryAndState($id_address);
            $id_country = $ids['id_country'] ? $ids['id_country'] : Configuration::get('PS_COUNTRY_DEFAULT');
            $id_country = (int)$id_country;
            $ids_product = SpecificPrice::getProductIdByDate($context->shop->id, $context->currency->id, $id_country, $context->customer->id_default_group, $current_date, $current_date, 0, false);
            $tab_id_product = array();
            foreach ($ids_product as $product) {
                if (is_array($product)) {
                    $tab_id_product[] = (int)$product['id_product'];
                } else {
                    $tab_id_product[] = (int)$product;
                }
            }
            $where .= ' AND p.`id_product` IN ('.((is_array($tab_id_product) && count($tab_id_product)) ? implode(', ', $tab_id_product) : 0).')';
        }
        
        $sql = 'SELECT p.*, product_shop.*, p.`reference`, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity,
                product_attribute_shop.id_product_attribute,
                product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity,
                pl.`description`, pl.`description_short`, pl.`available_now`,
                pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`,
                image_shop.`id_image`,
                il.`legend`, m.`name` AS manufacturer_name, cl.`name` AS category_default,
                DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(),
                INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'
                DAY)) > 0 AS new, product_shop.price AS orderprice';

        if ($value_by_product_type && $product_type == 'best_sellers') {
            $sql .= ' FROM `'._DB_PREFIX_.'product_sale` ps';
            $sql .= ' LEFT JOIN `'._DB_PREFIX_.'product` p ON ps.`id_product` = p.`id_product`';
        } else {
            $sql .= ' FROM `'._DB_PREFIX_.'product` p';
        }
        
        $sql .= ' INNER JOIN '._DB_PREFIX_.'product_shop product_shop ON (product_shop.id_product = p.id_product AND product_shop.id_shop = '.(int)$context->shop->id.')
          LEFT JOIN '._DB_PREFIX_.'product_attribute_shop product_attribute_shop        ON p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop='.(int)$context->shop->id.'
            '.ProductCore::sqlStock('p', 'product_attribute_shop', false, $context->shop).'
          LEFT JOIN '._DB_PREFIX_.'category_lang cl ON (product_shop.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
          LEFT JOIN '._DB_PREFIX_.'product_lang pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
          LEFT JOIN '._DB_PREFIX_.'image_shop image_shop    ON (image_shop.id_product = p.id_product AND image_shop.id_shop = '.(int)$context->shop->id.' AND image_shop.cover=1)
          LEFT JOIN '._DB_PREFIX_.'image_lang il    ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
          LEFT JOIN '._DB_PREFIX_.'manufacturer m   ON m.`id_manufacturer` = p.`id_manufacturer`';
        $sql .= $sql_join;

        $sql .= ' WHERE product_shop.`id_shop` = '.(int)$context->shop->id.'
                AND product_shop.`active` = 1
                AND product_shop.`visibility` IN ("both", "catalog")'
                .$where;

        $sql .= $sql_group;

        if ($random === true) {
    //            $sql .= ' ORDER BY product_shop.date_add '.(!$get_total ? ' LIMIT '.(int)$n : '');
            $sql .= ' ORDER BY RAND() '.(!$get_total ? ' LIMIT '.(int)$n : '');
        } else {
            $sql .= ' ORDER BY '.(!empty($order_by_prefix) ? pSQL($order_by_prefix).'.' : '').'`'.bqSQL($order_by).'` '.pSQL($order_way)
                    .(!$get_total ? ' LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n : '');
        }

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if ($order_by == 'orderprice') {
            Tools::orderbyPrice($result, $order_way);
        }
        if (!$result) {
            return array();
        }
        /* Modify SQL result */
        $this->context->controller->addColorsToProductList($result);
        return Product::getProductsProperties($id_lang, $result);
    }

    //TDK:: register hook back-end
    public function hookActionAdminControllerSetMedia()
    {
        Media::addJsDef(
            array(
                'tdkplatform_module_dir' => $this->_path,
                // 'tdkplatform_listshortcode_url' => $this->context->link->getAdminLink('AdminTdkPlatformShortcode').'&get_listshortcode',
            )
        );
        Configuration::updateValue('shortcode_url_add', $this->context->link->getAdminLink('AdminTdkPlatformShortcode'));
        
        $this->autoRestoreSampleData();
        //TDK:: auto update module
        $this->autoUpdateTDKModule();
    }

    public function hookdisplayHeader()
    {
        TdkPlatformHelper::autoUpdateModule();
        if (isset(Context::getContext()->controller->controller_type) && in_array(Context::getContext()->controller->controller_type, array('front', 'modulefront'))) {
            # WORK AT FRONTEND
            TdkPlatformHelper::loadShortCode(_PS_THEME_DIR_);

            $this->profile_data = TdkPlatformProfilesModel::getActiveProfile('index');
            $this->profile_param = Tools::jsonDecode($this->profile_data['params'], true);
            $this->setFullwidthHook();

            # FIX 1.7
            TdkPlatformHelper::setGlobalVariable($this->context);
        }
        
        
        if (Configuration::get('TDKPLATFORM_LOAD_WAYPOINTS')) {
            $uri = TdkPlatformHelper::getCssDir().'animate.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            
            $uri = TdkPlatformHelper::getJsDir().'waypoints.min.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
        }
        if (Configuration::get('TDKPLATFORM_LOAD_INSTAFEED')) {
            $uri = TdkPlatformHelper::getJsDir().'instafeed.min.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
        }
        if (Configuration::get('TDKPLATFORM_LOAD_STELLAR')) {
            $uri = TdkPlatformHelper::getJsDir().'jquery.stellar.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
        }
        if (Configuration::get('TDKPLATFORM_LOAD_OWL')) {
            $uri = TdkPlatformHelper::getCssDir().'owl.carousel.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            $uri = TdkPlatformHelper::getCssDir().'owl.theme.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            
            $uri = TdkPlatformHelper::getJsDir().'owl.carousel.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
        }

        if (Configuration::get('TDKPLATFORM_LOAD_SWIPER')) {
            $uri = TdkPlatformHelper::getCssDir().'swiper.min.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            
            $uri = TdkPlatformHelper::getJsDir().'swiper.min.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
        }

        //TDK:: add jquery plugin images loaded
        $uri = TdkPlatformHelper::getJsDir().'imagesloaded.pkgd.min.js';
        $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));

        //slick
        $uri = TdkPlatformHelper::getJsDir().'slick.js';
        $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));

        $uri = TdkPlatformHelper::getCssDir().'slick-theme.css';
        $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));

        $uri = TdkPlatformHelper::getCssDir().'slick.css';
        $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));

        // zoom
        if (Configuration::get('TDKPLATFORM_LOAD_PRODUCTZOOM')) {
            $uri = TdkPlatformHelper::getJsDir().'jquery.elevateZoom-3.0.8.min.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
        }

        $product_list_image = Configuration::get('TDKPLATFORM_LOAD_IMG');
        $product_one_img = Configuration::get('TDKPLATFORM_LOAD_TRAN');
        $category_qty = Configuration::get('TDKPLATFORM_LOAD_PN');
        $productCdown = Configuration::get('TDKPLATFORM_LOAD_COUNT');
//        $productColor = Configuration::get('TDKPLATFORM_LOAD_ACOLOR');
        $productColor = false;
        $ajax_enable = $product_list_image || $product_one_img || $category_qty || $productCdown || $productColor;
        $this->smarty->assign(array(
            'ajax_enable' => $ajax_enable,
            'product_list_image' => $product_list_image,
            'product_one_img' => $product_one_img,
            'category_qty' => $category_qty,
            'productCdown' => $productCdown,
            'productColor' => $productColor
        ));
        $this->context->controller->addJqueryPlugin('fancybox');
        if ($productCdown) {
            $uri = TdkPlatformHelper::getJsDir().'countdown.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 80));
        }
        if ($product_list_image) {
            $this->context->controller->addJqueryPlugin(array('scrollTo', 'serialScroll'));
        }
            
        // add js for html5 youtube video
        if (Configuration::get('TDKPLATFORM_LOAD_HTML5VIDEO')) {
            $uri = TdkPlatformHelper::getCssDir().'mediaelementplayer.min.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            
            $uri = TdkPlatformHelper::getJsDir().'mediaelement-and-player.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
        }
        //add js,css for full page js
        if (Configuration::get('TDKPLATFORM_LOAD_FULLPAGEJS')) {
            $uri = TdkPlatformHelper::getCssDir().'jquery.fullPage.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            
            $uri = TdkPlatformHelper::getJsDir().'jquery.fullPage.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
        }
        // add js, css for Image360
        if (Configuration::get('TDKPLATFORM_LOAD_IMAGE360')) {
            $uri = TdkPlatformHelper::getCssDir().'TdkImage360.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            
            $uri = TdkPlatformHelper::getJsDir().'TdkImage360.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
            $this->context->controller->addJqueryUI('ui.slider');
        }
        // add js, css for ImageHotPot
        if (Configuration::get('TDKPLATFORM_LOAD_IMAGEHOTPOT')) {
            $uri = TdkPlatformHelper::getCssDir().'TdkImageHotspot.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            
            $uri = TdkPlatformHelper::getJsDir().'TdkImageHotspot.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
        }
        // add js Cookie : jquery.cooki-plugin.js
        if (Configuration::get('TDKPLATFORM_LOAD_COOKIE')) {
            $this->context->controller->addJqueryPlugin('cooki-plugin');
        }

        $uri = TdkPlatformHelper::getCssDir().'styles.css';
        $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            
        $uri = TdkPlatformHelper::getJsDir().'script.js';
        $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
        if (!$this->product_active) {
            $this->product_active = TdkPlatformProductsModel::getActive();
        }
        $this->smarty->smarty->assign(array('productClassWidget' => $this->product_active['class']));
        
        $tpl_file = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/tdkplatform/views/templates/front/profiles/' . $this->product_active['plist_key'].'.tpl';
        
        if (is_file($tpl_file)) {
            $this->smarty->smarty->assign(array('productProfileDefault' => $this->product_active['plist_key']));
        }
        // In the case not exist: create new cache file for template
        if (!$this->isCached('header.tpl', $this->getCacheId('displayHeader'))) {
            if (!$this->hook_index_data) {
                $model = new TdkPlatformModel();
                $this->hook_index_data = $model->getAllItems($this->profile_data, 1, $this->default_language['id_lang']);
            }
            $this->smarty->assign(array(
                'homeSize' => Image::getSize(ImageType::getFormattedName('home')),
                'mediumSize' => Image::getSize(ImageType::getFormattedName('medium'))
            ));
        }
        
        # TDKTEMCP
        $isRTL = $this->context->language->is_rtl;
        $tdkRTL = $this->context->language->is_rtl;
        if ($tdkRTL && version_compare(Configuration::get('PS_VERSION_DB'), '1.7.3.0', '>=')) {
            $directory = _PS_ALL_THEMES_DIR_._THEME_NAME_;
            $allFiles = Tools::scandir($directory, 'css', '', true);
            $rtl_file = false;
            foreach ($allFiles as $key => $file) {
                if (Tools::substr(rtrim($file, '.css'), -4) == '_rtl') {
                    $rtl_file = true;
                    break;
                }
            }
            
            if ($rtl_file) {
                $tdkRTL = false; // to remove class RTL
//                $this->context->controller->unregisterStylesheet('theme-rtl');
                @unlink(_PS_ALL_THEMES_DIR_._THEME_NAME_.'/assets/css/rtl_rtl.css');  // Remove file rtl_rtl.css
                $this->context->controller->registerStylesheet('theme-rtl', '/assets/css/rtl.css', array('media' => 'all', 'priority' => 900));
            }
        }
//        $id_shop = $this->context->shop->id;
//        $helper = TdkFrameworkHelper::getInstance();
        
        $this->themeCookieName = $this->getConfigName('PANEL_CONFIG');
        $panelTool = $this->getConfig('PANELTOOL');
        $backGroundValue = '';
        
        //TDK:: get product detail layout
        $list_productdetail_layout = array();

        if ($panelTool) {
            # ENABLE PANEL TOOL
            $uri = TdkPlatformHelper::getCssDir().'colorpicker/css/colorpicker.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            $uri = TdkPlatformHelper::getCssDir().'paneltool.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            
            $uri = TdkPlatformHelper::getJsDir().'colorpicker/js/colorpicker.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
            $uri = TdkPlatformHelper::getJsDir().'paneltool.js';
            $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
            $this->context->controller->addJqueryPlugin('cooki-plugin');
            
            $skin = $this->getPanelConfig('default_skin');
            $layout_mode = $this->getPanelConfig('layout_mode');
            $enable_fheader = (int)$this->getPanelConfig('enable_fheader');
            
            $backGroundValue = array(
                'attachment' => array('scroll', 'fixed', 'local', 'initial', 'inherit'),
                'repeat' => array('repeat', 'repeat-x', 'repeat-y', 'no-repeat', 'initial', 'inherit'),
                'position' => array('left top', 'left center', 'left bottom', 'right top', 'right center', 'right bottom', 'center top', 'center center', 'center bottom')
            );

            //TDK:: check table exist
            $check_table = Db::getInstance()->executeS('SELECT table_name FROM INFORMATION_SCHEMA.tables WHERE TABLE_SCHEMA = "'._DB_NAME_.'" AND TABLE_NAME = "'._DB_PREFIX_.'tdkplatform_details"');

            if (count($check_table) > 0) {
                if (Tools::getValue('id_product')) {
                        $id_product = Tools::getValue('id_product');
                } else {
                    $sql = 'SELECT p.`id_product`
                            FROM `'._DB_PREFIX_.'product` p
                            '.Shop::addSqlAssociation('product', 'p').'
                    AND product_shop.`visibility` IN ("both", "catalog")
                    AND product_shop.`active` = 1
                    ORDER BY p.`id_product` ASC';
                    $first_product = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
                    $id_product = $first_product['id_product'];
                }
                $sql = 'SELECT a.*
                        FROM `'._DB_PREFIX_.'tdkplatform_details` as a
                        INNER JOIN `'._DB_PREFIX_.'tdkplatform_details_shop` ps ON (ps.`id_tdkplatform_details` = a.`id_tdkplatform_details`) WHERE ps.id_shop='.(int)$this->context->shop->id;
                $list_productdetail = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

                if ($id_product && count($list_productdetail) > 1) {
                    foreach ($list_productdetail as $key => $list_productdetail_item) {
                        $product_layout_link = '';
                        $product_layout_link = $this->context->link->getProductLink($id_product, null, null, null, null, null, (int)Product::getDefaultAttribute((int)$id_product));
                        $product_layout_link = str_replace('.html', '.html?layout='.$list_productdetail_item['plist_key'], $product_layout_link);
                        $list_productdetail[$key]['product_layout_link'] = $product_layout_link;
                    }
                    $list_productdetail_layout = $list_productdetail;
                }
            }
        } else {
            $skin = $this->getConfig('default_skin');
            $layout_mode = $this->getConfig('layout_mode');
            $enable_fheader = $this->getConfig('enable_fheader');
            if (TdkPlatformHelper::getPageName() == 'category') {
                $this->context->controller->addJqueryPlugin('cooki-plugin');
            }
        }
        
//        if ($this->getConfig('ENABLE_CUSTOMFONT')) {
//            # CUSTOM FONT
//            $uri = TdkPlatformHelper::getCssDir().'fonts-cuttom.css';
//            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
//        }
        if ($this->getConfig('ENABLE_LOADFONT')) {
            # CUSTOM FONT
            $uri = TdkPlatformHelper::getCssDir().'fonts-cuttom2.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
        }
        
        $layout_width_val = '';
        $layout_width = $this->getConfig('layout_width');
        if (trim($layout_width) != 'auto' && trim($layout_width) != '') {
            $layout_width = (int)$layout_width;
            $layout_width_val = '<style type="text/css">.container{max-width:'.$layout_width.'px}</style>';
            if (is_numeric($layout_width)) {
                # validate module
                $layout_width_val .= '<script type="text/javascript">layout_width = '.$layout_width.';</script>';
            }
        }
        
        $load_css_type = $this->getConfig('load_css_type');
        $css_skin = array();
        $css_custom = array();
        if ($load_css_type) {
            # Load Css With Prestashop Standard - YES
            if (!$this->getConfig('enable_responsive')) {
                $uri = TdkPlatformHelper::getCssDir().'non-responsive.css';
                $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            }
            
            # LOAD SKIN CSS IN MODULE
            $uri = TdkPlatformHelper::getCssDir().'skins/'.$skin.'/skin.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            $uri = TdkPlatformHelper::getCssDir().'skins/'.$skin.'/custom-rtl.css';
            $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            
            # LOAD CUSTOM CSS
            if ($this->context->getMobileDevice() != false && !$this->getConfig('enable_responsive')) {
                $uri = TdkPlatformHelper::getCssDir().'mobile.css';
                $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            }
            
            # LOAD POSITIONS AND PROFILES
            $this->loadResouceForProfile();

            # LOAD PATTERN
            if ($profile = $this->getConfig('c_profile')) {
                $uri = TdkPlatformHelper::getCssDir().'patterns/'.$profile.'.css';
                $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
            }
        } else {
            # Load Css With Prestashop Standard - NO
            if (!$this->getConfig('enable_responsive')) {
                $uri = TdkPlatformHelper::getCssDir().'non-responsive.css';
                $skinFileUrl = TdkPlatformHelper::getFullPathCss($uri);
                if ($skinFileUrl !== false) {
                    $css_skin[] = '<link rel="stylesheet" href="'.TdkPlatformHelper::getUriFromPath($skinFileUrl).'" type="text/css" media="all" />';
                }
            }
            
            # LOAD SKIN CSS IN TPL
            $uri = TdkPlatformHelper::getCssDir().'skins/'.$skin.'/skin.css';
            $skinFileUrl = TdkPlatformHelper::getFullPathCss($uri);
            if ($skinFileUrl !== false) {
                $css_skin[] = '<link rel="stylesheet" id="tdk-dynamic-skin-css" href="'.TdkPlatformHelper::getUriFromPath($skinFileUrl).'" type="text/css" media="all" />';
            }
            $uri = TdkPlatformHelper::getCssDir().'skins/'.$skin.'/custom-rtl.css';
            $skinFileUrl = TdkPlatformHelper::getFullPathCss($uri);
            if ($tdkRTL && $skinFileUrl !== false) {
                $css_skin[] = '<link rel="stylesheet" id="tdk-dynamic-skin-css-rtl" href="'.TdkPlatformHelper::getUriFromPath($skinFileUrl).'" type="text/css" media="all" />';
            }
            
            # LOAD CUSTOM CSS
            if ($this->context->getMobileDevice() != false && !$this->getConfig('enable_responsive')) {
                $uri = TdkPlatformHelper::getCssDir().'mobile.css';
                $skinFileUrl = TdkPlatformHelper::getFullPathCss($uri);
                if ($skinFileUrl !== false) {
                    $css_skin[] = '<link rel="stylesheet" href="'.TdkPlatformHelper::getUriFromPath($skinFileUrl).'" type="text/css" media="all" />';
                }
            }
            
            # LOAD POSITIONS AND PROFILES
            $this->loadResouceForProfile();
            
            # LOAD PATTERN
            if ($profile = $this->getConfig('c_profile')) {
                $uri = TdkPlatformHelper::getCssDir().'patterns/'.$profile.'.css';
                $skinFileUrl = TdkPlatformHelper::getFullPathCss($uri);
                if ($skinFileUrl !== false) {
                    $css_skin[] = '<link rel="stylesheet" href="'.TdkPlatformHelper::getUriFromPath($skinFileUrl).'" type="text/css" media="all" />';
                }
            }
        }
        //TDK:: add class of detail to body
        $TDK_CLASS_DETAIL = '';
        if (Tools::getValue('id_product')) {
            $id_product = Tools::getValue('id_product');
            $id_shop = Context::getContext()->shop->id;
            //TDK:: get layout of product detail by parameter in URL
            if (Tools::getValue('layout')) {
                $sql = 'SELECT a.`class_detail` FROM `'._DB_PREFIX_.'tdkplatform_details` AS a INNER JOIN `'._DB_PREFIX_.'tdkplatform_details_shop` AS b ON a.`id_tdkplatform_details` = b.`id_tdkplatform_details' .'` WHERE b.`id_shop` = "'.(int)$id_shop.'" AND a.`plist_key` = "'.pSQL(Tools::getValue('layout')).'"';
                $class_detail = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
                if ($class_detail) {
                    $TDK_CLASS_DETAIL = $class_detail;
                }
            } else {
                //layout
                $sql = 'SELECT page from `'._DB_PREFIX_.'tdkplatform_page` where id_product = \''.(int)$id_product.'\' AND id_shop = \''.(int)$id_shop.'\'';
                $layout = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
                //get default
                if (!$layout) {
                    $sql = 'SELECT a.`class_detail` FROM `'._DB_PREFIX_.'tdkplatform_details` AS a INNER JOIN `'._DB_PREFIX_.'tdkplatform_details_shop` AS b ON a.`id_tdkplatform_details` = b.`id_tdkplatform_details' .'` WHERE b.`id_shop` = "'.(int)$id_shop.'" AND b.`active` = 1';
                    $class_detail = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
                    $TDK_CLASS_DETAIL = $class_detail;
                } else {
                    $sql = 'SELECT a.`class_detail` FROM `'._DB_PREFIX_.'tdkplatform_details` AS a INNER JOIN `'._DB_PREFIX_.'tdkplatform_details_shop` AS b ON a.`id_tdkplatform_details` = b.`id_tdkplatform_details' .'` WHERE b.`id_shop` = "'.(int)$id_shop.'" AND a.`plist_key` = "'.pSQL($layout).'"';
                    $class_detail = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
                    $TDK_CLASS_DETAIL = $class_detail;
                }
            }
        }
        $ps = array(
            'TDK_THEMENAME' => _THEME_NAME_,
			'TDK_PROFILE_KEY' => $this->profile_data['profile_key'],
            'TDK_CLASS_DETAIL' => $TDK_CLASS_DETAIL,
            // 'TDK_PANELTOOL' => $panelTool,
            'TDK_SUBCATEGORY' => $this->getConfig('subcategory'),
            'TDK_DEFAULT_SKIN' => $skin,
            'TDK_LAYOUT_MODE' => $layout_mode,
            'BACKGROUNDVALUE' => $backGroundValue,
            'LAYOUT_WIDTH' => $layout_width_val,
            'LOAD_CSS_TYPE' => $load_css_type,
            'TDK_CSS' => $css_custom,
            'TDK_SKIN_CSS' => $css_skin,
            'IS_RTL' => $isRTL,
            'TDK_RTL' => $tdkRTL,
            'USE_PTABS' => $this->getConfig('ENABLE_PTAB'),
            'USE_FHEADER' => $enable_fheader,
            'TDK_COOKIE_THEME' => $this->themeCookieName,
            'TDK_BACKTOP' => $this->getConfig('backtop'),
            'TdkPlatformHelper' => TdkPlatformHelper::getInstance(),
            'tdkConfiguration' => new Configuration(),
            'list_productdetail_layout' => $list_productdetail_layout,
        );

        Media::addJsDefL('TDK_COOKIE_THEME', $this->themeCookieName);
        $this->context->smarty->assign($ps);

        $page_name = TdkPlatformHelper::getPageName();
        $page = $this->smarty->smarty->getVariable('page')->value;
        if (isset($this->profile_data['meta_title']) && $this->profile_data['meta_title'] && $page_name == 'index') {
            $page['meta']['title'] = $this->profile_data['meta_title'];
        }
        if (isset($this->profile_data['meta_description']) && $this->profile_data['meta_description'] && $page_name == 'index') {
            $page['meta']['description'] = $this->profile_data['meta_title'];
        }
        if (isset($this->profile_data['meta_keywords']) && $this->profile_data['meta_keywords'] && $page_name == 'index') {
            $page['meta']['keywords'] = $this->profile_data['meta_title'];
        }
        $this->smarty->smarty->assign('page', $page);

        # REPLACE LINK FOR MULILANGUAGE
        $controller = Dispatcher::getInstance()->getController();
        if ($controller == 'tdkplatformhome') {
            Media::addJsDef(array('tdkprofile_multilang_url' => TdkPlatformProfilesModel::getAllProfileRewrite($this->profile_data['id_tdkplatform_profiles'])));
        }

        if ($controller == 'sitemap') {
            $profile_model = new TdkPlatformProfilesModel();
            $profiles = $profile_model->getAllProfileByShop();
            foreach ($profiles as $key => $profile) {
                if (!isset($profile['friendly_url']) || !$profile['friendly_url']) {
                    unset($profiles[$key]);
                }
            }
            $this->smarty->smarty->assign('simtdk_tdk_profiles', $profiles);
        }

        $this->header_content = $this->display(__FILE__, 'header.tpl');
        return $this->header_content;
    }

    //TDK:: build shortcode by hook
    public function hookDisplayTdkSC($params)
    {
        if (isset($params['sc_key']) && $params['sc_key'] != '') {
            return $this->processShortCode($params['sc_key']);
        }
    }

    //TDK:: build shortcode by embedded in content
    public function buildShortCode($content)
    {
        //TDK: remove tag p in content
        if (Tools::substr(trim($content), 0, 3) == '<p>') {
            $content = Tools::substr(trim($content), 3);
        }
        if (Tools::substr(trim($content), -4) == '</p>') {
            $content = Tools::substr(trim($content), 0, -4);
        }
        //TDK:: validate module
        $result = preg_replace_callback(
            '~\[TdkSC(.*?)\[\/TdkSC\]~',
            function ($matches_tmp) {
                preg_match_all("~sc_key=(.*?)\]~", $matches_tmp[1], $tmp);
                return self::processShortCode($tmp[1][0]);
            },
            $content
        );
        return $result;
    }

    //DONGN:: get list short code for tinymce
    public function getListShortCodeForEditor()
    {
        $this->smarty->smarty->assign(array(
            'js_dir' => _PS_JS_DIR_,
            'tdkplatform_module_dir' => $this->_path,
            'shortcode_url_add' => Configuration::get('shortcode_url_add').'&addtdkplatform_shortcode',
            'shortcode_url' => Configuration::get('shortcode_url_add'),
            'list_shortcode' => TdkPlatformShortcodeModel::getListShortCode(),
        ));
        return $this->display(__FILE__, 'list_shortcode.tpl');
    }

    private function processShortCode($shortcode_key)
    {
        $disable_cache = false;
        if (!Configuration::get('PS_SMARTY_CACHE')) {
            $disable_cache = true;
        }

        $cache_id = $this->getCacheId('tdkshortcode', $shortcode_key);
        if ($disable_cache || !$this->isCached('module:tdkplatform/views/templates/hook/tdkplatform.tpl', $cache_id)) {
            $shortcode_html = '';
            $shortcode_obj = TdkPlatformShortcodeModel::getShortCode($shortcode_key);
            if (isset($shortcode_obj['id_tdkplatform']) && $shortcode_obj['id_tdkplatform'] != '' && $shortcode_obj['id_tdkplatform'] != 0) {
                $shortcode_code = TdkPlatformShortcodeModel::getAllItems($shortcode_obj['id_tdkplatform'], 1);
                
                if (!empty($shortcode_code)) {
                    if (empty(TdkShortCodesBuilder::$shortcode_tags)) {
                        TdkPlatformHelper::loadShortCode(_PS_THEME_DIR_);
                    }
                    
                    TdkPlatformHelper::setGlobalVariable($this->context, true);
                    
                    // TdkShortCodesBuilder::$is_front_office = 1;
                    // TdkShortCodesBuilder::$is_gen_html = 1;
                    // TdkShortCodesBuilder::$profile_param = array();
                    $tdk_helper = new TdkShortCodesBuilder();
                    // TdkShortCodesBuilder::$hook_name = 'tdkshortcode';
                    
                    $shortcode_html = $tdk_helper->parse($shortcode_code['tdkshortcode']);
                }
            }
            $this->smarty->assign(array('tdkContent' => $shortcode_html));
        }
        return $this->display(__FILE__, 'tdkplatform.tpl', $cache_id);
    }

    private function processHook($hook_name, $params = 'null')
    {
        $disable_cache_hook = isset($this->profile_param['disable_cache_hook']) ? $this->profile_param['disable_cache_hook'] : TdkPlatformSetting::getCacheHook(3);
        $disable_cache = false;
        if (isset($disable_cache_hook[$hook_name]) && $disable_cache_hook[$hook_name]) {
            $disable_cache = true;
        }
        if (Tools::isSubmit('submitNewsletter')) {
            $disable_cache = true;
        }
        if (!Configuration::get('PS_SMARTY_CACHE')) {
            $disable_cache = true;
        }
        $is_live = Tools::getIsset('tdk_live_edit') ? Tools::getValue('tdk_live_edit') : '';
        if ($is_live) {
            $disable_cache = true;
        }

        $cache_id = $this->getCacheId($hook_name);
        $cover_hook_live = '';
        if ($disable_cache || !$this->isCached('tdkplatform.tpl', $cache_id)) {
            if ($disable_cache) {
                $cache_id = null;
            }
            if ($is_live) {
                $token = Tools::getIsset('tdk_edit_token') ? Tools::getValue('tdk_edit_token') : '';
                $admin_dir = Tools::getIsset('ad') ? Tools::getValue('ad') : '';
                $controller = 'AdminTdkPlatformHome';
                $id_lang = Context::getContext()->language->id;
                $id_profile = TdkPlatformProfilesModel::getIdProfileFromRewrite();
                $params = array('token' => $token, 'id_tdkplatform_profiles' => $id_profile);
                $current_link = _PS_BASE_URL_.__PS_BASE_URI__;
                $url_design_layout = $current_link.$admin_dir.'/'.Dispatcher::getInstance()->createUrl($controller, $id_lang, $params, false);
                $cover_hook_live = '<div class="cover-hook"><a title="'.$this->l('Click to edit').'" class="lnk-hook" href="'
                        .$url_design_layout.'#'.$hook_name.'" target="_blank">Hook: '.Tools::strtoupper($hook_name).'</a></div>';
            }
            $model = new TdkPlatformModel();
            if (!$this->hook_index_data) {
                $this->hook_index_data = $model->getAllItems($this->profile_data, 1, $this->default_language['id_lang']);
            }
            if (!isset($this->hook_index_data[$hook_name]) || trim($this->hook_index_data[$hook_name]) == '') {
                # NOT DATA BUT SET VARIABLE TO SET CACHE
                $this->smarty->assign(array('tdkContent' => ''));
                return $cover_hook_live.$this->display(__FILE__, 'tdkplatform.tpl', $cache_id);
            }
            $tdk_content = $model->parseData($hook_name, $this->hook_index_data[$hook_name], $this->profile_param);
            if ($is_live) {
                $tdk_content = '<div class="tdk-cover-hook">'.$tdk_content.'</div>';
            }
            $this->smarty->assign(array('tdkContent' => $tdk_content));
        }
        return $cover_hook_live.$this->display(__FILE__, 'tdkplatform.tpl', $cache_id);
    }
    
    public function hookDisplayBanner($params)
    {
        return $this->processHook('displayBanner', $params);
    }

    public function hookDisplayNav1($params)
    {
        return $this->processHook('displayNav1', $params);
    }

    public function hookDisplayNav2($params)
    {
        return $this->processHook('displayNav2', $params);
    }

    public function hookDisplayNavFullWidth($params)
    {
        return $this->processHook('displayNavFullWidth', $params);
    }

    public function hookDisplayTop($params)
    {
        return $this->processHook('displayTop', $params);
    }

    public function hookDisplaySlideshow($params)
    {
        return $this->processHook('displaySlideshow', $params);
    }

    public function hookDisplayRightColumn($params)
    {
        return $this->processHook('displayRightColumn', $params);
    }

    public function hookDisplayLeftColumn($params)
    {
        return $this->processHook('displayLeftColumn', $params);
    }

    public function hookDisplayHome($params)
    {
        return $this->processHook('displayHome', $params);
    }

    public function hookDisplayFooterBefore($params)
    {
        return $this->processHook('displayFooterBefore', $params);
    }

    public function hookDisplayFooter($params)
    {
        return $this->processHook('displayFooter', $params);//.$this->header_content;
    }

    public function hookDisplayFooterAfter($params)
    {
        return $this->processHook('displayFooterAfter', $params);
    }

    public function hookDisplayFooterProduct($params)
    {
        return $this->processHook('displayFooterProduct', $params);
    }

    public function hookDisplayRightColumnProduct($params)
    {
        return $this->processHook('displayRightColumnProduct', $params);
    }

    public function hookDisplayLeftColumnProduct($params)
    {
        return $this->processHook('displayLeftColumnProduct', $params);
    }
    
    public function hookdisplayProductButtons($params)
    {
        return $this->processHook('displayProductButtons', $params);
    }
    
    public function hookDisplayReassurance($params)
    {
        return $this->processHook('displayReassurance', $params);
    }
    
    public function hookDisplayTdkProfileProduct($params)
    {
        TdkPlatformHelper::setGlobalVariable($this->context);
        $html = '';
        $tpl_file = '';
        
        if (isset($params['ony_global_variable'])) {
            # {hook h='displayTdkProfileProduct' ony_global_variable=true}
            return $html;
        }

        if (!isset($params['product'])) {
            return 'Not exist product to load template';
        } else if (isset($params['profile'])) {
            # {hook h='displayTdkProfileProduct' product=$product profile=$productProfileDefault}
            $tpl_file = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/tdkplatform/views/templates/front/profiles/' . $params['profile'].'.tpl';
        } else if (isset($params['load_file'])) {
            # {hook h='displayTdkProfileProduct' product=$product load_file='templates/catalog/_partials/miniatures/product.tpl'}
            $tpl_file = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/' . $params['load_file'];
        } else if (isset($params['typeProduct'])) {
            //TDK:: load default product tpl when do not have product profile
            if ($params['product']['productLayout'] != '') {
                $tpl_file = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/tdkplatform/views/templates/front/details/' . $params['product']['productLayout'].'.tpl';
            } else {
                $tpl_file = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/templates/catalog/product.tpl';
            }
        }

        if (empty($tpl_file)) {
            return 'Not exist profile to load template';
        }

        Context::getContext()->smarty->assign(array(
            'product' => $params['product'],
        ));
        $html .= Context::getContext()->smarty->fetch($tpl_file);
        return $html;
    }

    public function hookActionShopDataDuplication()
    {
        $this->clearHookCache();
    }

    /**
     * Register hook again to after install/change any theme
     */
    public function hookActionObjectShopUpdateAfter()
    {
        // Retrieve hooks used by the module
//        $sql = 'SELECT `id_hook` FROM `'._DB_PREFIX_.'hook_module` WHERE `id_module` = '.(int)$this->id;
//        $result = Db::getInstance()->executeS($sql);
//        foreach ($result as $row) {
//            $this->unregisterHook((int)$row['id_hook']);
//            $this->unregisterExceptions((int)$row['id_hook']);
//        }
    }
    
    /**
     * FIX BUG 1.7.3.3 : install theme lose hook displayHome, displayTdkProfileProduct
     * because ajax not run hookActionAdminBefore();
     */
    public function autoRestoreSampleData()
    {
        if (Hook::isModuleRegisteredOnHook($this, 'actionAdminBefore', (int)Context::getContext()->shop->id)) {
            $theme_manager = new stdclass();
            $theme_manager->theme_manager = 'theme_manager';
            $this->hookActionAdminBefore(array(
                'controller' => $theme_manager,
            ));
        }
    }
    
    /**
     * Run only one when install/change Theme_of_TDK Studio
     */
    public function hookActionAdminBefore($params)
    {
        $this->unregisterHook('actionAdminBefore');
        if (isset($params) && isset($params['controller']) && isset($params['controller']->theme_manager)) {
            // Validate : call hook from theme_manager
        } else {
            // Other module call this hook -> duplicate data
            return;
        }
        
        
        # FIX : update Prestashop by 1-Click module -> NOT NEED RESTORE DATABASE
        $tdk_version = Configuration::get('TDK_CURRENT_VERSION');
        if ($tdk_version != false) {
            $ps_version = Configuration::get('PS_VERSION_DB');
            $versionCompare =  version_compare($tdk_version, $ps_version);
            if ($versionCompare != 0) {
                // Just update Prestashop
                Configuration::updateValue('TDK_CURRENT_VERSION', $ps_version);
                return;
            }
        }
        
        
        # WHENE INSTALL THEME, INSERT HOOK FROM DATASAMPLE IN THEME
        $hook_from_theme = false;
        if (file_exists(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php')) {
            require_once(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php');
            $sample = new TDKDatasample();
            if ($sample->processHook($this->name)) {
                $hook_from_theme = true;
            };
        }
        
        # INSERT HOOK FROM MODULE_DATASAMPLE
        if ($hook_from_theme == false) {
            $this->registerTDKHook();
        }
        
        # WHEN INSTALL MODULE, NOT NEED RESTORE DATABASE IN THEME
        $install_module = (int)Configuration::get('TDK_INSTALLED_TDKPLATFORM', 0);
        if ($install_module) {
			Configuration::updateValue('TDK_INSTALLED_TDKPLATFORM', '0');    // next : allow restore sample
			//TDK:: fix for case can not install sample when install theme (can not get theme name to find directory folder)
			if ((int)Configuration::get('TDK_INSTALLED_SAMPLE_TDKPLATFORM', 0)) {				
				return;
			}           
        }
        
        # INSERT DATABASE FROM THEME_DATASAMPLE
        if (file_exists(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php')) {
            require_once(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php');
            $sample = new TDKDatasample();
            $sample->processImport($this->name);
        }
        
        # REMOVE FILE INDEX.PHP FOR TRANSLATE
        if (file_exists(_PS_MODULE_DIR_.'tdkplatform/libs/setup.php')) {
            require_once(_PS_MODULE_DIR_.'tdkplatform/libs/setup.php');
            TdkPlatformSetup::processTranslateTheme();
        }
    }
    
    protected function getCacheId($hook_name = null, $shortcode_key = null)
    {
        $cache_array = array();

        $cache_array[] = $this->name;
        if (TdkPlatformProfilesModel::getIdProfileFromRewrite() && !$shortcode_key) {
            $cache_array[] = 'profile_'.TdkPlatformProfilesModel::getIdProfileFromRewrite();
            // $cache_array[] = 'tdk_profile_'.TdkPlatformProfilesModel::getIdProfileFromRewrite();
        // } else {
            // $cache_array[] = $this->name;
        }
        $cache_array[] = $hook_name;
        if ($this->profile_param && isset($this->profile_param[$hook_name]) && $this->profile_param[$hook_name]) {
            //$cache_array[] = $hook_name;
            $current_page = TdkPlatformHelper::getPageName();
            //show ocurrentPagenly in controller
            if (isset($this->profile_param[$hook_name][$current_page])) {
                $cache_array[] = $current_page;
                if ($current_page != 'index' && $cache_id = TdkPlatformSetting::getControllerId($current_page, $this->profile_param[$hook_name][$current_page])) {
                    $cache_array[] = $cache_id;
                }
            } elseif (isset($this->profile_param[$hook_name]['productCarousel'])) {
                $random = round(rand(1, max(Configuration::get('TDKPLATFORM_PRODUCT_MAX_RANDOM'), 1)));
                $cache_array[] = "p_carousel_$random";
            } else if (isset($this->profile_param[$hook_name]['exception']) && in_array($cache_array, $this->profile_param[$hook_name]['exception'])) {
                //show but not in controller
                $cache_array[] = $current_page;
            }
        }
        if (Configuration::get('PS_SSL_ENABLED')) {
            $cache_array[] = 'SSL_'.(int)Tools::usingSecureMode();
        }
        if (Shop::isFeatureActive()) {
            $cache_array[] = 'shop_'.(int)$this->context->shop->id;
        }
        if (Group::isFeatureActive()) {
            $cache_array[] = 'c_group_'.(int)GroupCore::getCurrent()->id;
        }
        if (Language::isMultiLanguageActivated()) {
            $cache_array[] = 'la_'.(int)$this->context->language->id;
        }
        if (Currency::isMultiCurrencyActivated()) {
            $cache_array[] = 'curcy_'.(int)$this->context->currency->id;
        }
        $cache_array[] = 'ctry_'.(int)$this->context->country->id;
        if (Tools::getValue('plist_key')&& Tools::getIsset('tdkpanelchange')) {
            $cache_array[] = 'plist_key_'.Tools::getValue('plist_key');
        }
        if (Tools::getValue('header') && Tools::getIsset('tdkpanelchange') && (in_array($hook_name, TdkPlatformSetting::getHook('header')) || $hook_name == 'tdkplatformConfig|header')) {
            $cache_array[] = 'header_'.Tools::getValue('header');
        }
        if (Tools::getValue('content')&& Tools::getIsset('tdkpanelchange') && (in_array($hook_name, TdkPlatformSetting::getHook('content')) || $hook_name == 'tdkplatformConfig|content')) {
            $cache_array[] = 'content_'.Tools::getValue('content');
        }
        if (Tools::getValue('product')&& Tools::getIsset('tdkpanelchange') && (in_array($hook_name, TdkPlatformSetting::getHook('product')) || $hook_name == 'tdkplatformConfig|product')) {
            $cache_array[] = 'product_'.Tools::getValue('product');
        }
        if (Tools::getValue('footer') && Tools::getIsset('tdkpanelchange') && (in_array($hook_name, TdkPlatformSetting::getHook('footer')) || $hook_name == 'tdkplatformConfig|footer')) {
            $cache_array[] = 'footer_'.Tools::getValue('footer');
        }
        
        //TDK:: update cache for shortcode
        if ($shortcode_key) {
            $cache_array[] = 'shortcodekey_'.$shortcode_key;
        }
        
        return implode('|', $cache_array);
    }

    /**
     * Overide function isCached of Module.php
     * @param type $template
     * @param type $cache_id
     * @param type $compile_id
     * @return boolean
     */
    // public function isCached($template, $cache_id = null, $compile_id = null)
    // {
        // if (Tools::getIsset('live_edit')) {
            // return false;
        // }
        // Tools::enableCache();
        // $is_cached = $this->getCurrentSubTemplate($this->getTemplatePath($template), $cache_id, $compile_id);
        // $is_cached = $is_cached->isCached($this->getTemplatePath($template), $cache_id, $compile_id);
        // Tools::restoreCacheSettings();
        // return $is_cached;
    // }

    /**
     * This function base on the function getCurrentSubTemplate of Module.php (not overide)
     * @param type $template
     * @param type $cache_id
     * @param type $compile_id
     * @return type
     */
    // protected function getCurrentSubTemplate($template, $cache_id = null, $compile_id = null)
    // {
        // if (!isset($this->current_subtemplate[$template.'_'.$cache_id.'_'.$compile_id])) {
            // $this->current_subtemplate[$template.'_'.$cache_id.'_'.$compile_id] = $this->context->smarty->createTemplate($this->getTemplatePath($template), $cache_id, $compile_id, $this->smarty);
        // }
        // return $this->current_subtemplate[$template.'_'.$cache_id.'_'.$compile_id];
    // }

    /**
     * Overide function display of Module.php
     * @param type $file
     * @param type $template
     * @param null $cache_id
     * @param type $compile_id
     * @return type
     */
    public function display($file, $template, $cache_id = null, $compile_id = null)
    {
        if (($overloaded = Module::_isTemplateOverloadedStatic(basename($file, '.php'), $template)) === null) {
            return sprintf($this->l('No template found "%s"'), $template);
        } else {
            if (Tools::getIsset('live_edit')) {
                $cache_id = null;
            }
            $this->smarty->assign(array(
                'module_dir' => __PS_BASE_URI__.'modules/'.basename($file, '.php').'/',
                'module_template_dir' => ($overloaded ? _THEME_DIR_ : __PS_BASE_URI__).'modules/'.basename($file, '.php').'/',
                'allow_push' => $this->allow_push
            ));
            if ($cache_id !== null) {
                Tools::enableCache();
            }
            $result = $this->getCurrentSubTemplate($template, $cache_id, $compile_id)->fetch();
            if ($cache_id !== null) {
                Tools::restoreCacheSettings();
            }
            $this->resetCurrentSubTemplate($template, $cache_id, $compile_id);
            return $result;
        }
    }

    public function clearHookCache()
    {
        $this->_clearCache('tdkplatform.tpl', $this->name);
    }
    
    //TDK:: add clear cache for shortcode
    public function clearShortCodeCache($shortcode_key)
    {
        $cache_id = $this->getCacheId('tdkshortcode', $shortcode_key);
        
        $this->_clearCache('tdkplatform.tpl', $cache_id);
    }

    public function hookCategoryAddition()
    {
        $this->clearHookCache();
    }

    public function hookCategoryUpdate()
    {
        $this->clearHookCache();
    }

    public function hookCategoryDeletion()
    {
        $this->clearHookCache();
    }

    public function hookAddProduct()
    {
        $this->clearHookCache();
    }

    public function hookUpdateProduct()
    {
        $this->clearHookCache();
    }

    public function hookDeleteProduct()
    {
        $this->clearHookCache();
    }

    public function hookDisplayBackOfficeHeader()
    {
        TdkPlatformHelper::autoUpdateModule();
        if (method_exists($this->context->controller, 'addJquery')) {
            // validate module
            $this->context->controller->addJquery();
        }
        //TDK:: fix home page config with theme of TDK Studio, redirect to profile page
        if (get_class($this->context->controller) == 'AdminPsThemeCustoConfigurationController' && _THEME_NAME_ != 'default') {
            Media::addJsDef(
                array(
                    'tdk_profile_url' => $this->context->link->getAdminLink('AdminTdkPlatformProfiles'),
                    'tdk_profile_txt_redirect' => $this->l('You are using a theme from TDK Studio. Please access this link to use this feature easily'),
                    'tdk_check_theme_name' => _THEME_NAME_
                )
            );
        }
        $this->context->controller->addCss(TdkPlatformHelper::getCssAdminDir().'admin/style.css');
        $this->context->controller->addJS(TdkPlatformHelper::getJsAdminDir().'admin/setting.js');
        if (!TdkPlatformHelper::isRelease()) {
            Media::addJsDef(array('js_tdk_dev' => 1));
        }
    }

    public function loadResouceForProfile()
    {
        $profile = $this->profile_data;
        $arr = array();
        if ($profile['header']) {
            $arr[] = $profile['header'];
        }
        if ($profile['content']) {
            $arr[] = $profile['content'];
        }
        if ($profile['footer']) {
            $arr[] = $profile['footer'];
        }
        if ($profile['product']) {
            $arr[] = $profile['product'];
        }
        if (count($arr) > 0) {
            $model = new TdkPlatformProfilesModel();
            $list_positions = $model->getPositionsForProfile($arr);
            if ($list_positions) {
                foreach ($list_positions as $item) {
                    $name = $item['position'].$item['position_key'];
                    
                    $uri = TdkPlatformHelper::getCssDir().'positions/'.$name.'.css';
                    $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
                    
                    $uri = TdkPlatformHelper::getJsDir().'positions/'.$name.'.js';
                    $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
                }
            }
        }
        $uri = TdkPlatformHelper::getCssDir().'profiles/'.$profile['profile_key'].'.css';
        $this->context->controller->registerStylesheet(sha1($uri), $uri, array('media' => 'all', 'priority' => 8000));
        
        $uri = TdkPlatformHelper::getJsDir().'profiles/'.$profile['profile_key'].'.js';
        $this->context->controller->registerJavascript(sha1($uri), $uri, array('position' => 'bottom', 'priority' => 8000));
    }

    public function getProfileData()
    {
        return $this->profile_data;
    }

    public function setFullwidthHook()
    {
        if (isset(Context::getContext()->controller->controller_type) && in_array(Context::getContext()->controller->controller_type, array('front', 'modulefront'))) {
            # frontend
            $page_name = TdkPlatformHelper::getPageName();
            if ($page_name == 'index' || $page_name == 'tdkplatformhome') {
                $this->context->smarty->assign(array(
                    'fullwidth_hook' => isset($this->profile_param['fullwidth_index_hook']) ? $this->profile_param['fullwidth_index_hook'] : TdkPlatformSetting::getIndexHook(3),
                ));
            } else {
                $this->context->smarty->assign(array(
                    'fullwidth_hook' => isset($this->profile_param['fullwidth_other_hook']) ? $this->profile_param['fullwidth_other_hook'] : TdkPlatformSetting::getOtherHook(3),
                ));
            }
        }
    }

    /**
     * Get Grade By product
     *
     * @return array Grades
     */
    public static function getGradeByProducts($list_product)
    {
        $validate = Configuration::get('PRODUCT_COMMENTS_MODERATE');
        $id_lang = (int)Context::getContext()->language->id;

        return (Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
        SELECT pc.`id_product_comment`, pcg.`grade`, pccl.`name`, pcc.`id_product_comment_criterion`, pc.`id_product`
        FROM `'._DB_PREFIX_.'product_comment` pc
        LEFT JOIN `'._DB_PREFIX_.'product_comment_grade` pcg ON (pcg.`id_product_comment` = pc.`id_product_comment`)
        LEFT JOIN `'._DB_PREFIX_.'product_comment_criterion` pcc ON (pcc.`id_product_comment_criterion` = pcg.`id_product_comment_criterion`)
        LEFT JOIN `'._DB_PREFIX_.'product_comment_criterion_lang` pccl ON (pccl.`id_product_comment_criterion` = pcg.`id_product_comment_criterion`)
        WHERE pc.`id_product` in ('.pSQL($list_product).')
        AND pccl.`id_lang` = '.(int)$id_lang.
                        ($validate == '1' ? ' AND pc.`validate` = 1' : '')));
    }

    /**
     * Return number of comments and average grade by products
     *
     * @return array Info
     */
    public static function getGradedCommentNumber($list_product)
    {
        $validate = (int)Configuration::get('PRODUCT_COMMENTS_MODERATE');

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
        SELECT COUNT(pc.`id_product`) AS nbr, pc.`id_product`
        FROM `'._DB_PREFIX_.'product_comment` pc
        WHERE `id_product` in ('.pSQL($list_product).')'.($validate == '1' ? ' AND `validate` = 1' : '').'
        AND `grade` > 0 GROUP BY pc.`id_product`');
        return $result;
    }

    public static function getByProduct($id_product)
    {
        $id_lang = (int)Context::getContext()->language->id;

        if (!Validate::isUnsignedId($id_product) || !Validate::isUnsignedId($id_lang)) {
            die(Tools::displayError());
        }
        $alias = 'p';
        $table = '';
        // check if version > 1.5 to add shop association
        if (version_compare(_PS_VERSION_, '1.5', '>')) {
            $table = '_shop';
            $alias = 'ps';
        }
        return Db::getInstance()->executeS('
            SELECT pcc.`id_product_comment_criterion`, pccl.`name`
            FROM `'._DB_PREFIX_.'product_comment_criterion` pcc
            LEFT JOIN `'._DB_PREFIX_.'product_comment_criterion_lang` pccl
                ON (pcc.id_product_comment_criterion = pccl.id_product_comment_criterion)
            LEFT JOIN `'._DB_PREFIX_.'product_comment_criterion_product` pccp
                ON (pcc.`id_product_comment_criterion` = pccp.`id_product_comment_criterion` AND pccp.`id_product` = '.(int)$id_product.')
            LEFT JOIN `'._DB_PREFIX_.'product_comment_criterion_category` pccc
                ON (pcc.`id_product_comment_criterion` = pccc.`id_product_comment_criterion`)
            LEFT JOIN `'._DB_PREFIX_.'product'.bqSQL($table).'` '.bqSQL($alias).'
                ON ('.bqSQL($alias).'.id_category_default = pccc.id_category AND '.bqSQL($alias).'.id_product = '.(int)$id_product.')
            WHERE pccl.`id_lang` = '.(int)$id_lang.'
            AND (
                pccp.id_product IS NOT NULL
                OR ps.id_product IS NOT NULL
                OR pcc.id_product_comment_criterion_type = 1
            )
            AND pcc.active = 1
            GROUP BY pcc.id_product_comment_criterion
        ');
    }

    public function hookProductMoreImg($list_pro)
    {
        $id_lang = Context::getContext()->language->id;
        //get product info
        $product_list = $this->getProducts($list_pro, $id_lang);

        $this->smarty->assign(array(
            'homeSize' => Image::getSize(ImageType::getFormattedName('home')),
            'mediumSize' => Image::getSize(ImageType::getFormattedName('medium'))
        ));

        $obj = array();
        foreach ($product_list as $product) {
            $this->smarty->assign('product', $product);
            $obj[] = array('id' => $product['id_product'], 'content' => ($this->display(__FILE__, 'product.tpl')));
        }
        return $obj;
    }

    public function hookProductOneImg($list_pro)
    {
        $protocol_link = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? 'https://' : 'http://';
        $use_ssl = ((isset($this->ssl) && $this->ssl && Configuration::get('PS_SSL_ENABLED')) || Tools::usingSecureMode()) ? true : false;
        $protocol_content = ($use_ssl) ? 'https://' : 'http://';
        $link = new Link($protocol_link, $protocol_content);

        $id_lang = Context::getContext()->language->id;
        $where = ' WHERE i.`id_product` IN ('.$list_pro.') AND (ish.`cover`=0 OR ish.`cover` IS NULL) AND ish.`id_shop` = '.Context::getContext()->shop->id;
        $order = ' ORDER BY i.`id_product`,`position`';
        $limit = ' LIMIT 0,1';
        //get product info
        $list_img = $this->getAllImages($id_lang, $where, $order, $limit);
        $saved_img = array();
        $obj = array();
        $this->smarty->assign(array(
            'homeSize' => Image::getSize(ImageType::getFormattedName('home')),
            'mediumSize' => Image::getSize(ImageType::getFormattedName('medium')),
            'smallSize' => Image::getSize(ImageType::getFormattedName('small'))
        ));

        $image_name = 'home';
        $image_name .= '_default';
        foreach ($list_img as $product) {
            if (!in_array($product['id_product'], $saved_img)) {
                $obj[] = array(
                    'id' => $product['id_product'],
                    'content' => ($link->getImageLink($product['link_rewrite'], $product['id_image'], $image_name)),
                    'name' => $product['name'],
                    );
            }
            $saved_img[] = $product['id_product'];
        }
        return $obj;
    }

    public function hookProductCdown($tdk_pro_cdown)
    {
        $id_lang = Context::getContext()->language->id;
        $product_list = $this->getProducts($tdk_pro_cdown, $id_lang);
        $obj = array();
        foreach ($product_list as $product) {
            $this->smarty->assign('product', $product);
            $obj[] = array('id' => $product['id_product'], 'content' => ($this->display(__FILE__, 'cdown.tpl')));
        }
        return $obj;
    }

    public function hookProductColor($tdk_pro_color)
    {
        $id_lang = Context::getContext()->language->id;
        $colors = array();
        $tdk_customajax_color = Configuration::get('TDKPLATFORM_COLOR');
        if ($tdk_customajax_color) {
            $arrs = explode(',', $tdk_customajax_color);
            foreach ($arrs as $arr) {
                $items = explode(':', $arr);
                $colors[$items[0]] = $items[1];
            }
        }
        $this->smarty->assign(array(
            'colors' => $colors,
        ));
        $product_list = $this->getProducts($tdk_pro_color, $id_lang);
        $obj = array();
        foreach ($product_list as $product) {
            $this->smarty->assign('product', $product);
            $obj[] = array('id' => $product['id_product'], 'content' => ($this->display(__FILE__, 'color.tpl')));
        }
        return $obj;
    }
    
    public function hookModuleRoutes($params)
    {
        $routes = array();
        $model = new TdkPlatformProfilesModel();
        $allProfileArr = $model->getAllProfileByShop();
        foreach ($allProfileArr as $allProfileItem) {
            if (isset($allProfileItem['friendly_url']) && $allProfileItem['friendly_url']) {
                $routes['module-tdkplatform-'.$allProfileItem['friendly_url']] = array(
                    'controller' => 'tdkplatformhome',
                    'rule' => $allProfileItem['friendly_url'].'.html',
                    'keywords' => array(
                    ),
                    'params' => array(
                        'fc' => 'module',
                        'module' => 'tdkplatform'
                    )
                );
            }
        }
        return $routes;
    }

    public function getProducts($product_list, $id_lang, $colors = array())
    {
        $context = Context::getContext();
        $id_address = $context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
        $ids = Address::getCountryAndState($id_address);
        $id_country = ($ids['id_country'] ? $ids['id_country'] : Configuration::get('PS_COUNTRY_DEFAULT'));
        $sql = 'SELECT p.*, product_shop.*, pl.* , m.`name` AS manufacturer_name, s.`name` AS supplier_name,sp.`id_specific_price`
                                FROM `'._DB_PREFIX_.'product` p
                                '.Shop::addSqlAssociation('product', 'p').'
                                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
                                LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
                                LEFT JOIN `'._DB_PREFIX_.'supplier` s ON (s.`id_supplier` = p.`id_supplier`)
                                LEFT JOIN `'._DB_PREFIX_.'specific_price` sp ON (sp.`id_product` = p.`id_product`
                                                AND sp.`id_shop` IN(0, '.(int)$context->shop->id.')
                                                AND sp.`id_currency` IN(0, '.(int)$context->currency->id.')
                                                AND sp.`id_country` IN(0, '.(int)$id_country.')
                                                AND sp.`id_group` IN(0, '.(int)$context->customer->id_default_group.')
                                                AND sp.`id_customer` IN(0, '.(int)$context->customer->id.')
                                                AND sp.`reduction` > 0
                                        )
                                WHERE pl.`id_lang` = '.(int)$id_lang.
                ' AND p.`id_product` in ('.$product_list.')';
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if ($product_list) {
            $tmp_img = array();
            $cover_img = array();
            $where = ' WHERE i.`id_product` IN ('.$product_list.') AND ish.`id_shop` = '.Context::getContext()->shop->id;
            $order = ' ORDER BY i.`id_product`,`position`';

            switch (Configuration::get('TDK_MINFO_SORT')) {
                case 'position2':
                    break;
                case 'random':
                    $order = ' ORDER BY RAND()';
                    break;
                default:
                    $order = ' ORDER BY i.`id_product`,`position` DESC';
            }

            $list_img = $this->getAllImages($id_lang, $where, $order);
            foreach ($list_img as $val) {
                $tmp_img[$val['id_product']][$val['id_image']] = $val;
                if ($val['cover'] == 1) {
                    $cover_img[$val['id_product']] = $val['id_image'];
                }
            }
        }
        $now = date('Y-m-d H:i:s');
        $finish = $this->l('Expired');
        foreach ($result as &$val) {
            $time = false;
            if (isset($tmp_img[$val['id_product']])) {
                $val['images'] = $tmp_img[$val['id_product']];
                $val['id_image'] = $cover_img[$val['id_product']];
            } else {
                $val['images'] = array();
            }

            $val['specific_prices'] = self::getSpecificPriceById($val['id_specific_price']);
            if (isset($val['specific_prices']['from']) && $val['specific_prices']['from'] > $now) {
                $time = strtotime($val['specific_prices']['from']);
                $val['finish'] = $finish;
                $val['check_status'] = 0;
                $val['lofdate'] = Tools::displayDate($val['specific_prices']['from']);
            } elseif (isset($val['specific_prices']['to']) && $val['specific_prices']['to'] > $now) {
                $time = strtotime($val['specific_prices']['to']);
                $val['finish'] = $finish;
                $val['check_status'] = 1;
                $val['lofdate'] = Tools::displayDate($val['specific_prices']['to']);
            } elseif (isset($val['specific_prices']['to']) && $val['specific_prices']['to'] == '0000-00-00 00:00:00') {
                $val['js'] = 'unlimited';
                $val['finish'] = $this->l('Unlimited');
                $val['check_status'] = 1;
                $val['lofdate'] = $this->l('Unlimited');
            } else if (isset($val['specific_prices']['to'])) {
                $time = strtotime($val['specific_prices']['to']);
                $val['finish'] = $finish;
                $val['check_status'] = 2;
                $val['lofdate'] = Tools::displayDate($val['specific_prices']['from']);
            }
            if ($time) {
                $val['js'] = array(
                    'month' => date('m', $time),
                    'day' => date('d', $time),
                    'year' => date('Y', $time),
                    'hour' => date('H', $time),
                    'minute' => date('i', $time),
                    'seconds' => date('s', $time)
                );
            }
        }
        unset($colors);
        return Product::getProductsProperties($id_lang, $result);
    }

    public static function getSpecificPriceById($id_specific_price)
    {
        if (!SpecificPrice::isFeatureActive()) {
            return array();
        }

        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
                        SELECT *
                        FROM `'._DB_PREFIX_.'specific_price` sp
                        WHERE `id_specific_price` ='.(int)$id_specific_price);

        return $res;
    }

    public function getAllImages($id_lang, $where, $order)
    {
        $id_shop = Context::getContext()->shop->id;
        $sql = 'SELECT DISTINCT i.`id_product`, ish.`cover`, i.`id_image`, il.`legend`, i.`position`,pl.`link_rewrite`, pl.`name`
                                FROM `'._DB_PREFIX_.'image` i
                                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (i.`id_product` = pl.`id_product`) AND pl.`id_lang` = '.(int)$id_lang.'
                                LEFT JOIN `'._DB_PREFIX_.'image_shop` ish ON (ish.`id_image` = i.`id_image` AND ish.`id_shop` = '.(int)$id_shop.')
                                LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')'.pSql($where).' '.pSQL($order);
        return Db::getInstance()->executeS($sql);
    }

    // show category and tags of product
    public function hookdisplayProductInformation($params)
    {
        $return = '';
        $product_id = $params['product']->id;
        $category_id = $params['product']->id_category_default;
        $cat = new Category($category_id, $this->context->language->id);
        $product_tags = Tag::getProductTags($product_id);
        $product_tags = $product_tags[(int)$this->context->cookie->id_lang];
        $return .= '<div class =category>Category: <a href="'.$this->context->link->getCategoryLink($category_id, $cat->link_rewrite).'">'.$cat->name.'</a>.</div>';
        $return .= '<div class="producttags clearfix">';
        $return .= 'Tag: ';
        if ($product_tags && count($product_tags) > 1) {
            $count = 0;
            foreach ($product_tags as $tag) {
                $return .= '<a href="'.$this->context->link->getPageLink('search', true, null, "tag=$tag").'">'.$tag.'</a>';
                if ($count < count($product_tags) - 1) {
                    $return .= ',';
                } else {
                    $return .= '.';
                }
                $count++;
            }
        }
        $return .= '</div>';
        return $return;
    }
    
    /**
     * alias from TdkPlatformHelper::getConfig()
     */
    public function getConfigName($name)
    {
        return TdkPlatformHelper::getConfigName($name);
    }
    
    /**
     * alias from TdkPlatformHelper::getConfig()
     */
    public function getConfig($name)
    {
        return TdkPlatformHelper::getConfig($name);
    }
    
    /**
     * get Value of configuration based on actived theme
     */
    public function getPanelConfig($key, $default = '', $id_lang = null)
    {
        if (Tools::getIsset($key)) {
            # validate module
            return Tools::getValue($key);
        }

        $cookie = TdkFrameworkHelper::getCookie();
        
        if (isset($cookie[$this->themeCookieName.'_'.$key])) {
            return $cookie[$this->themeCookieName.'_'.$key];
        }

        unset($default);
        return Configuration::get($this->getConfigName($key), $id_lang);
    }

    public function generateTdkHtmlMessage()
    {
        $html = '';
        if (count($this->_confirmations)) {
            foreach ($this->_confirmations as $string) {
                $html .= $this->displayConfirmation($string);
            }
        }
        if (count($this->_errors)) {
            $html .= $this->displayError($this->_errors);
        }
        if (count($this->_warnings)) {
            $html .= $this->displayWarning($this->_warnings);
        }
        return $html;
    }

    /**
     * Common method
     * Resgister all hook for module
     */
    public function registerTDKHook()
    {
        $res = true;
        $res &= $this->registerHook('header');
        $res &= $this->registerHook('actionShopDataDuplication');
        $res &= $this->registerHook('displayBackOfficeHeader');
        $res &= $this->registerHook('moduleroutes');
        foreach (TdkPlatformSetting::getHook('all') as $value) {
            $res &= $this->registerHook($value);
        }
        # register hook to show when paging
        $this->registerHook('tdkplatformConfig');
        
        # register hook to show category and tags of product
        $this->registerHook('displayProductInformation');
        
        # register hook again to after install/change theme
        $this->registerHook('actionObjectShopUpdateAfter');
        
        # Multishop create new shop
        $this->registerHook('actionAdminShopControllerSaveAfter');
        
        $this->registerHook('displayProductButtons');
        $this->registerHook('displayReassurance');
        $this->registerHook('displayTdkProfileProduct');
        # MoveEndHeader
        $this->registerHook('actionModuleRegisterHookAfter');
        #select product layout
        $this->registerHook('actionObjectProductUpdateAfter');
        $this->registerHook('displayAdminProductsExtra');
        #select category layout
        $this->registerHook('actionObjectCategoryUpdateAfter');
        $this->registerHook('displayBackOfficeCategory');

        //TDK:: register hook for tdkshortcode
        $this->registerHook('displayTdkSC');
        $this->registerHook('actionAdminControllerSetMedia');
        //TDK:: register hook for product detail builder
        $this->registerHook('displayTdkProductMoreInfo');
        return $res;
    }
    
    /**
     * @Action Create new shop, choose theme then auto restore datasample.
     */
    public function hookActionAdminShopControllerSaveAfter($param)
    {
        if (Tools::getIsset('controller') !== false && Tools::getValue('controller') == 'AdminShop'
                && Tools::getIsset('submitAddshop') !== false && Tools::getValue('submitAddshop')
                && Tools::getIsset('theme_name') !== false && Tools::getValue('theme_name')) {
            $shop = $param['return'];
            
            if (file_exists(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php')) {
                require_once(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php');
                $sample = new TDKDatasample();
                TdkPlatformHelper::$id_shop = $shop->id;
                $sample->_id_shop = $shop->id;
                $sample->processImport('tdkplatform');
            }
        }
    }

    public function hookDisplayBackOfficeCategory($params)
    {
        
        if (Validate::isLoadedObject($category = new Category((int)Tools::getValue('id_category')))) {
            // validate module
            unset($category);
            
            $id_shop = Context::getContext()->shop->id;
            
            $category_layouts = array();
            $id_category = Tools::getValue('id_category');
            $layouts = _PS_THEME_DIR_.'modules/tdkplatform/views/templates/front/profiles/';
            if (is_dir($layouts)) {
                //TDK:: fix get list product list via database
                $sql = 'SELECT * FROM '._DB_PREFIX_.'tdkplatform_products p
                        INNER JOIN '._DB_PREFIX_.'tdkplatform_products_shop ps on(p.id_tdkplatform_products = ps.id_tdkplatform_products) WHERE ps.id_shop='.(int)$id_shop;
                $category_layouts = Db::getInstance()->executeS($sql);
            }

            $extrafied = array();
            $data_fields = array();

            if (Configuration::get('TDKPLATFORM_CATEGORY_TEXTEXTRA') || Configuration::get('TDKPLATFORM_CATEGORY_EDITOREXTRA')) {
                $sql = 'SHOW FIELDS FROM `'._DB_PREFIX_.'tdkplatform_extracat' .'`';
                $result = Db::getInstance()->executeS($sql);

                $rows = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'tdkplatform_extracat' .'` WHERE id_category="'.(int)$id_category.'" AND id_shop="'.(int)$id_shop.'"');

                foreach ($result as $value) {
                    if ($value['Field'] != 'id_category' && $value['Field'] != 'id_shop' && $value['Field'] != 'id_lang') {
                        $extrafied[$value['Field']] = $value['Type'];
                        foreach ($rows as $row) {
                            $data_fields[$value['Field']][$row['id_lang']] = $row[$value['Field']];
                        }
                    }
                }
            }

            $this->context->smarty->assign(array(
                'category_layouts' => $category_layouts,
                'tdkextras' => $extrafied,
                'id_lang_default' => $this->default_language['id_lang'],
                'languages' => $this->languages,
                'data_fields' => $data_fields,
                'current_layout' => Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT page from `'._DB_PREFIX_.'tdkplatform_page` where id_category = \''.(int)$id_category.'\' AND id_shop = \''.(int)$id_shop.'\'')
            ));

            return $this->display(__FILE__, 'categoryExtra.tpl');
        }
    }

    public function hookActionObjectCategoryUpdateAfter($params)
    {
        $id_category = Tools::getValue('id_category');
        //TDK:: fix when change status category at category list (BO)
        if (isset($id_category) && $id_category) {
            $tdklayout = Tools::getValue('tdklayout');
            $id_shop = Context::getContext()->shop->id;
            $sql = 'SELECT page from `'._DB_PREFIX_.'tdkplatform_page` where id_category = \''.(int)$id_category.'\' AND id_shop = \''.(int)$id_shop.'\'';
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
           
            if ($result) {
                if ($tdklayout == 'default') {
                    Db::getInstance()->execute('DELETE from `'._DB_PREFIX_.'tdkplatform_page` where id_category = \''.(int)$id_category.'\' and id_shop=\''.(int)$id_shop.'\'');
                } else {
                    Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'tdkplatform_page` set page = \''.pSQL($tdklayout).'\' where id_category = \''.(int)$id_category.'\' and id_shop=\''.(int)$id_shop.'\'');
                }
            } else {
                if ($tdklayout != 'default') {
                    Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'tdkplatform_page` (`id_product`,`id_category`,`page`,`id_shop`) VALUES (0,'.(int)$id_category.',\''.pSQL($tdklayout).'\','.(int)$id_shop.')');
                }
            }
            if (Configuration::get('TDKPLATFORM_CATEGORY_TEXTEXTRA') || Configuration::get('TDKPLATFORM_CATEGORY_EDITOREXTRA')) {
                //save for extrafield
                $sql = 'SHOW FIELDS FROM `'._DB_PREFIX_.'tdkplatform_extracat' .'`';
                $result = Db::getInstance()->executeS($sql);
                $extrafied = array();
                foreach ($result as $value) {
                    if ($value['Field'] != 'id_category' && $value['Field'] != 'id_shop' && $value['Field'] != 'id_lang') {
                        $extrafied[] = $value['Field'];
                    }
                }
                if ($extrafied) {
                    foreach ($this->languages as $lang) {
                        $checkExist =  Db::getInstance()->getValue('SELECT COUNT(*) FROM `'._DB_PREFIX_.'tdkplatform_extracat' .'` WHERE id_shop="'.(int)$id_shop.'" AND id_category = "'.(int)$id_category.'" AND id_lang="'.(int)$lang['id_lang'].'"');
                        if ($checkExist) {
                            $sql = '';
                            foreach ($extrafied as $value) {
                                $sql .= (($sql=='')?'':',').pSQL($value).'="'.pSQL(Tools::getValue($value.'_'.$lang['id_lang'])).'"';
                            }
                            $sql = 'UPDATE `'._DB_PREFIX_.'tdkplatform_extracat' .'` SET '.$sql.' WHERE id_shop="'.(int)$id_shop.'" AND id_category = "'.(int)$id_category.'" AND id_lang="'.(int)$lang['id_lang'].'"';
                        } else {
                            $sql = 'INSERT INTO `'._DB_PREFIX_.'tdkplatform_extracat' .'` (`id_category`, `id_shop`, `id_lang`';
                            $sql1 = ' VALUES ("'.(int)$id_category.'","'.(int)$id_shop.'","'.(int)$lang['id_lang'].'"';
                            foreach ($extrafied as $value) {
                                $sql .= ',`'.$value.'`';
                                $sql1 .= ',"'.((Tools::getValue($value.'_'.$lang['id_lang'])=='')? pSQL(Tools::getValue($value.'_'.$this->default_language['id_lang'])) : pSQL(Tools::getValue($value.'_'.$lang['id_lang']))).'"';
                            }
                            $sql = $sql.')'.$sql1.')';
                        }
                        //echo $sql.'<br/>';
                        Db::getInstance()->execute($sql);
                    }
                }
            }
        }
        //die;
    }

    public function hookfilterCategoryContent($params)
    {
        $id_category = Tools::getValue('id_category');
        $id_shop = Context::getContext()->shop->id;

        if (Configuration::get('TDKPLATFORM_CATEGORY_TEXTEXTRA') || Configuration::get('TDKPLATFORM_CATEGORY_EDITOREXTRA')) {
            $rows = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'tdkplatform_extracat' .'` WHERE id_category="'.(int)$id_category.'" AND id_shop="'.(int)$id_shop.'" AND id_lang="'.(int)$this->default_language['id_lang'].'"');
            foreach ($rows as $value) {
                foreach ($value as $k => $v) {
                    if ($k != 'id_category' && $k != 'id_shop' && $k != 'id_lang') {
                        //TDK:: check shortcode in extra field
                        if (strpos($v, '[TdkSC') !== false && strpos($v, '[/TdkSC]') !== false) {
                            $params['object'][$k] = $this->buildShortCode($v);
                        } else {
                            $params['object'][$k] = $v;
                        }
                    }
                }
            }
        }

        //layout
        $sql = 'SELECT page from `'._DB_PREFIX_.'tdkplatform_page` where id_category = \''.(int)$id_category.'\' AND id_shop = \''.(int)$id_shop.'\'';
        $layout = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        $params['object']['categoryLayout'] = $layout;

        return $params;
    }

    public function hookActionObjectProductUpdateAfter($params)
    {
        $id_product = Tools::getValue('id_product');

        //TDK:: fix when change status product at product list (BO)
        if (isset($id_product) && $id_product) {
            $tdklayout = Tools::getValue('tdklayout');
            $id_shop = Context::getContext()->shop->id;
            $sql = 'SELECT * from `'._DB_PREFIX_.'tdkplatform_page` where id_product = '.(int)$id_product.' AND id_shop = '.(int)$id_shop;
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
            
            if ($result) {
                if ($tdklayout == 'default') {
                    Db::getInstance()->execute('DELETE from `'._DB_PREFIX_.'tdkplatform_page` where id_product = '.(int)$id_product.' and id_shop='.(int)$id_shop);
                } else {
                    Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'tdkplatform_page` set page = "'.pSQL($tdklayout).'" where id_product = '.(int)$id_product.' and id_shop='.(int)$id_shop);
                }
            } else {
                if ($tdklayout != 'default') {
                    Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'tdkplatform_page` (`id_product`,`id_category`,`page`,`id_shop`) VALUES ('.(int)$id_product.',0,\''.$tdklayout.'\','.(int)$id_shop.')');
                }
            }
        
            if (Configuration::get('TDKPLATFORM_PRODUCT_TEXTEXTRA') || Configuration::get('TDKPLATFORM_PRODUCT_EDITOREXTRA')) {
                //save for extrafield
                $sql = 'SHOW FIELDS FROM `'._DB_PREFIX_.'tdkplatform_extrapro' .'`';
                $result = Db::getInstance()->executeS($sql);
                $extrafied = array();
                foreach ($result as $value) {
                    if ($value['Field'] != 'id_product' && $value['Field'] != 'id_shop' && $value['Field'] != 'id_lang') {
                        $extrafied[] = $value['Field'];
                    }
                }
                if ($extrafied) {
                    //$form = Tools::getValue('form');
                    $tdk_pro_extra = Tools::getValue('tdk_pro_extra');
                    foreach ($this->languages as $lang) {
                        $checkExist =  Db::getInstance()->getValue('SELECT COUNT(*) FROM `'._DB_PREFIX_.'tdkplatform_extrapro' .'` WHERE id_shop="'.(int)$id_shop.'" AND id_product = "'.(int)$id_product.'" AND id_lang="'.(int)$lang['id_lang'].'"');
                        if ($checkExist) {
                            $sql = '';
                            foreach ($extrafied as $value) {
                                $sql .= (($sql=='')?'':',').pSQL($value).'="'.pSQL($tdk_pro_extra[$value][$lang['id_lang']]).'"';
                            }
                            $sql = 'UPDATE `'._DB_PREFIX_.'tdkplatform_extrapro' .'` SET '.$sql.' WHERE id_shop="'.(int)$id_shop.'" AND id_product = "'.(int)$id_product.'" AND id_lang="'.(int)$lang['id_lang'].'"';
                        } else {
                            $sql = 'INSERT INTO `'._DB_PREFIX_.'tdkplatform_extrapro' .'` (`id_product`, `id_shop`, `id_lang`';
                            $sql1 = ' VALUES ("'.$id_product.'","'.(int)$id_shop.'","'.(int)$lang['id_lang'].'"';
                            foreach ($extrafied as $value) {
                                $sql .= ',`'.$value.'`';
                                $sql1 .= ',"'.pSQL(($tdk_pro_extra[$value][$lang['id_lang']]=='')?pSQL($tdk_pro_extra[$value][$this->default_language['id_lang']]) : pSQL($tdk_pro_extra[$value][$lang['id_lang']])).'"';
                            }
                            $sql = $sql.')'.$sql1.')';
                        }
                        //echo $sql;
                        Db::getInstance()->execute($sql);
                    }
                }
            }
        }
    }

    public function hookDisplayAdminProductsExtra($params)
    {
        if (Validate::isLoadedObject($product = new Product((int)$params['id_product']))) {
            // validate module
            unset($product);
            
            $id_shop = Context::getContext()->shop->id;
            $extrafied = array();
            $data_fields = array();

            if (Configuration::get('TDKPLATFORM_PRODUCT_TEXTEXTRA') || Configuration::get('TDKPLATFORM_PRODUCT_EDITOREXTRA')) {
                $sql = 'SHOW FIELDS FROM `'._DB_PREFIX_.'tdkplatform_extrapro' .'`';
                $result = Db::getInstance()->executeS($sql);
                
                $rows = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'tdkplatform_extrapro' .'` WHERE id_product="'.(int)$params['id_product'].'" AND id_shop="'.(int)$id_shop.'"');

                foreach ($result as $value) {
                    if ($value['Field'] != 'id_product' && $value['Field'] != 'id_shop' && $value['Field'] != 'id_lang') {
                        $extrafied[$value['Field']] = $value['Type'];
                        foreach ($rows as $row) {
                            $data_fields[$value['Field']][$row['id_lang']] = $row[$value['Field']];
                        }
                    }
                }
            }
//            $product_layouts = array();
            $sql = 'SELECT a.`plist_key`, a.`name` FROM `'._DB_PREFIX_.'tdkplatform_details` AS a INNER JOIN `'._DB_PREFIX_.'tdkplatform_details_shop` AS b ON a.`id_tdkplatform_details` = b.`id_tdkplatform_details' .'` WHERE b.id_shop= "'.(int)$id_shop.'"';
            $list = Db::getInstance()->executeS($sql);

            $this->context->smarty->assign(array(
                'product_layouts' => $list,
//                'default_plist' => $default_plist,
                'id_product' => (int)Tools::getValue('id_product'),
                'tdkextras' => $extrafied,
                'languages' => $this->languages,
                'data_fields' => $data_fields,
                'default_language' => $this->default_language,
                'current_layout' => Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT page from `'._DB_PREFIX_.'tdkplatform_page` where id_product = \''.(int)$params['id_product'].'\' AND id_shop = \''.(int)$id_shop.'\'')
            ));
            
            return $this->display(__FILE__, 'productExtra.tpl');
        }
    }

    public function hookfilterProductContent($params)
    {
        $id_product = Tools::getValue('id_product');
        $id_shop = Context::getContext()->shop->id;
        //extra fields
        if (Configuration::get('TDKPLATFORM_PRODUCT_TEXTEXTRA') || Configuration::get('TDKPLATFORM_PRODUCT_EDITOREXTRA')) {
            $rows = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'tdkplatform_extrapro' .'` WHERE id_product="'.(int)$id_product.'" AND id_shop="'.(int)$id_shop.'" AND id_lang="'.(int)$this->default_language['id_lang'].'"');
            foreach ($rows as $value) {
                foreach ($value as $k => $v) {
                    if ($k != 'id_product' && $k != 'id_shop' && $k != 'id_lang') {
                        //TDK:: check shortcode in extra field
                        if (strpos($v, '[TdkSC') !== false && strpos($v, '[/TdkSC]') !== false) {
                            $params['object'][$k] = $this->buildShortCode($v);
                        } else {
                            $params['object'][$k] = $v;
                        }
                    }
                }
            }
        }
        //TDK:: get layout of product detail by parameter in URL
        if (Tools::getValue('layout')) {
            $sql = 'SELECT a.`plist_key` FROM `'._DB_PREFIX_.'tdkplatform_details` AS a INNER JOIN `'._DB_PREFIX_.'tdkplatform_details_shop` AS b ON a.`id_tdkplatform_details` = b.`id_tdkplatform_details' .'` WHERE b.`id_shop` = "'.(int)$id_shop.'" AND a.`plist_key` = "'.pSQL(Tools::getValue('layout')).'"';
            $layout = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
            if ($layout) {
                $params['object']['productLayout'] = $layout;
                return $params;
            }
        }
        //layout
        $sql = 'SELECT page from `'._DB_PREFIX_.'tdkplatform_page` where id_product = \''.(int)$id_product.'\' AND id_shop = \''.(int)$id_shop.'\'';
        $layout = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        //get default
        if (!$layout) {
            $sql = 'SELECT a.`plist_key` FROM `'._DB_PREFIX_.'tdkplatform_details` AS a INNER JOIN `'._DB_PREFIX_.'tdkplatform_details_shop` AS b ON a.`id_tdkplatform_details` = b.`id_tdkplatform_details' .'` WHERE b.`id_shop` = "'.(int)$id_shop.'" AND b.`active` = 1';
            $layout = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        }
        $params['object']['productLayout'] = $layout;
        return $params;
    }
    //TDK:: add hook for product detail builder
    public function hookdisplayTdkProductMoreInfo($params)
    {
        $product = $params['product'];
        $lang_id = Context::getContext()->language->id;
        $link = Context::getContext()->link;
        $id_manufacturer = $product['id_manufacturer'];
        $id_supplier = $product['id_supplier'];
        $product_manufacturer = array();
        $product_supplier = array();
        $list_categories = array();
        if ($id_manufacturer) {
            $manufacturer_obj = new Manufacturer((int) $id_manufacturer, $lang_id);
            $product_manufacturer['id_manufacturer'] = $manufacturer_obj->id;
            $product_manufacturer['manufacturer_name'] = $manufacturer_obj->name;
            $product_manufacturer['manufacturer_url'] = $link->getManufacturerLink($manufacturer_obj->id, $manufacturer_obj->link_rewrite);
        }
        if ($id_supplier) {
            $supplier_obj = new Supplier((int) $id_supplier, $lang_id);
            $product_supplier['id_supplier'] = $supplier_obj->id;
            $product_supplier['supplier_name'] = $supplier_obj->name;
            $product_supplier['supplier_url'] = $link->getSupplierLink($supplier_obj->id, $supplier_obj->link_rewrite);
        }
        $list_categories = $this->getProductCategoriesFull($product['id'], $lang_id);
        if (count($list_categories) > 0) {
            foreach ($list_categories as $k => $list_categories_item) {
                $list_categories[$k]['category_url'] = $link->getCategoryLink($list_categories_item['id_category'], $list_categories_item['link_rewrite']);
            }
        }
        $templateVars = array(
            'tdk_product_more_info' => $product,
            'product_manufacturer' => $product_manufacturer,
            'product_supplier' => $product_supplier,
            'product_categories' => $list_categories,
        );
        $this->context->smarty->assign($templateVars);
        return $this->display(__FILE__, 'tdk_product_more_info.tpl');
    }
    //TDK:: clone function from class Product
    public function getProductCategoriesFull($id_product = '', $id_lang = null)
    {
        if (!$id_lang) {
            $id_lang = Context::getContext()->language->id;
        }
        $ret = array();
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT cp.`id_category`, cl.`name`, cl.`link_rewrite` FROM `'._DB_PREFIX_.'category_product` cp
            LEFT JOIN `'._DB_PREFIX_.'category` c ON (c.id_category = cp.id_category)
            LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (cp.`id_category` = cl.`id_category`'.Shop::addSqlRestrictionOnLang('cl').')
            '.Shop::addSqlAssociation('category', 'c').'
            WHERE cp.`id_product` = '.(int)$id_product.' AND c.`active` = 1 AND cl.`id_lang` = '.(int)$id_lang);
        foreach ($row as $val) {
            $ret[$val['id_category']] = $val;
        }
        return $ret;
    }
    //TDK:: auto update version
    public function autoUpdateTDKModule()
    {
        //TDK:: add file more info for product detail
        if (!Configuration::hasKey('TDKPLATFORM_CURRENT_VERSION') || (Configuration::hasKey('TDKPLATFORM_CURRENT_VERSION') && version_compare(Configuration::get('TDKPLATFORM_CURRENT_VERSION'), '1.0.1', '<'))) {
            $this->registerHook('displayTdkProductMoreInfo');
            Module::upgradeModuleVersion($this->name, '1.0.1');
            Configuration::updateValue('TDKPLATFORM_CURRENT_VERSION', '1.0.1');
        }
		
		//TDK:: uninstall tab
		if (!Configuration::hasKey('TDKPLATFORM_CURRENT_VERSION') || (Configuration::hasKey('TDKPLATFORM_CURRENT_VERSION') && version_compare(Configuration::get('TDKPLATFORM_CURRENT_VERSION'), '1.1.0', '<'))) {			
            $id = Tab::getIdFromClassName('AdminTdkPlatformThemeEditor');
			if ($id) {
				$tab = new Tab($id);
				$tab->delete();
			}
            Module::upgradeModuleVersion($this->name, '1.1.0');
            Configuration::updateValue('TDKPLATFORM_CURRENT_VERSION', '1.1.0');
        }		
    }
}
