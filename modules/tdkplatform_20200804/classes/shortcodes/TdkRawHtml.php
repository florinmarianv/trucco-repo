<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class TdkRawHtml extends TdkShortCodeBase
{
    public $name = 'TdkRawHtml';
    public $for_module = 'manage';

    public function getInfo()
    {
        return array('label' => $this->l('Raw Html'), 'position' => 3, 'desc' => $this->l('You can put raw html'),
            'icon_class' => 'icon-html5', 'tag' => 'content structure');
    }

    public function getConfigList()
    {
        $inputs = array(
            array(
                'type' => 'text',
                'name' => 'title',
                'label' => $this->l('Title'),
                'lang' => 'true',
                'default' => '',
            ),
            array(
                'type' => 'textarea',
                'name' => 'sub_title',
                'label' => $this->l('Sub Title'),
                'lang' => true,
                'values' => '',
                'autoload_rte' => false,
                'default' => '',
            ),
            array(
                'type' => 'text',
                'name' => 'class',
                'label' => $this->l('CSS Class'),
                'default' => ''
            ),
            array(
                'type' => 'textarea',
                'name' => 'content_html',
                'class' => 'tdk_html_raw raw-'.time(),
                'rows' => '50',
                'lang' => true,
                'label' => $this->l('Raw html'),
                'values' => '',
                'default' => "<div>\n</div>"
            ),
        );
        return $inputs;
    }
}
