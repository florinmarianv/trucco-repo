<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class TdkAccordions extends TdkShortCodeBase
{
    public $name = 'TdkAccordions';

    public function getInfo()
    {
        return array('label' => $this->l('Accordions'),
            'position' => 5, 'desc' => $this->l('You can put widget in accordions'),
            'icon_class' => 'icon icon-align-justify',
            'tag' => 'content');
    }

    public function getConfigList($sub_tab = 0)
    {
        if (Tools::getIsset('subTab') || $sub_tab) {
            $input = array(
                array(
                    'type' => 'text',
                    'name' => 'title',
                    'label' => $this->l('Title'),
                    'lang' => 'true',
                    'values' => '',
                ),
                array(
                    'type' => 'textarea',
                    'name' => 'sub_title',
                    'label' => $this->l('Sub Title'),
                    'lang' => true,
                    'values' => '',
                    'autoload_rte' => false,
                    'default' => ''
                ),
                array(
                    'type' => 'text',
                    'name' => 'id',
                    'label' => $this->l('ID Accordion'),
                    'values' => ''
                )
            );
            $this->name = 'TdkSubAccordion';
        } else {
            $input = array(
                array(
                    'type' => 'text',
                    'name' => 'title',
                    'label' => $this->l('Title'),
                    'lang' => 'true',
                    'default' => '',
                ),
                array(
                    'type' => 'textarea',
                    'name' => 'sub_title',
                    'label' => $this->l('Sub Title'),
                    'lang' => true,
                    'values' => '',
                    'autoload_rte' => false,
                    'default' => ''
                ),
                array(
                    'type' => 'text',
                    'name' => 'class',
                    'label' => $this->l('CSS Class'),
                    'default' => '',
                ),
//                array(
//                    'type' => 'select',
//                    'label' => $this->l('Accordion Type'),
//                    'name' => 'accordion_type',
//                    'options' => array(
//                        'query' => array(
//                            array(
//                                'id' => 'vertical',
//                                'name' => $this->l('Vertical'),
//                            ),
//                            array(
//                                'id' => 'horizontal',
//                                'name' => $this->l('Horizontal'),
//                            )
//                        ),
//                        'id' => 'id',
//                        'name' => 'name'
//                    ),
//                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Showing Type'),
                    'name' => 'active_type',
                    'class' => 'form-action',
                    'default' => 'hideall',
                    'options' => array(
                        'query' => array(
                            array(
                                'id' => 'set',
                                'name' => $this->l('Set active'),
                            ),
                            array(
                                'id' => 'showall',
                                'name' => $this->l('Show all'),
                            ),
                            array(
                                'id' => 'hideall',
                                'name' => $this->l('Hide all'),
                            )
                        ),
                        'id' => 'id',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'text',
                    'name' => 'active_accordion',
                    'label' => $this->l('Active Accordion'),
                    'default' => '1',
                    'form_group_class' => 'active_type_sub active_type-set',
                ),
            );
        }
        return $input;
    }

    /**
     * overide in tabs module
     */
    public function adminContent($atts, $content = null, $tag_name = null, $is_gen_html = null)
    {
        $this->preparaAdminContent($atts, $tag_name);
        if ($is_gen_html) {
            $assign = array();
            $w_info = $this->getInfo();
            $w_info['name'] = $this->name;
            if ($tag_name == 'TdkAccordion') {
                $assign['isSubTab'] = 1;
                $w_info['name'] = 'TdkAccordion';
            } else {
                //TDK:: check correct wrapper TdkAccordion
                $assign['isWrapper'] = 1;
                preg_match_all('/ parent_id="([^\"]+)"{0,1}/i', $content, $matches, PREG_OFFSET_CAPTURE);
                if (isset($matches['1']['0']['0'])) {
                    $atts['id'] = $matches['1']['0']['0'];
                }
            }
            $assign['formAtts'] = $atts;
            $assign['tdkInfo'] = $w_info;
            $assign['tdkContent'] = TdkShortCodesBuilder::doShortcode($content);
            $controller = new AdminTdkPlatformShortcodesController();
            return $controller->adminContent($assign, $this->name.'.tpl');
        } else {
            TdkShortCodesBuilder::doShortcode($content);
        }
    }

    /**
     * overide in tabs module
     */
    public function fontContent($atts, $content = null, $tag_name = null, $is_gen_html = null)
    {
        $is_active = $this->isWidgetActive(array('formAtts' => $atts));
        if (!$is_active) {
            return '';
        }
        
        foreach ($atts as $key => $val) {
            if (strpos($key, 'content') !== false || strpos($key, 'link') !== false || strpos($key, 'url') !== false || strpos($key, 'alt') !== false || strpos($key, 'tit') !== false || strpos($key, 'name') !== false || strpos($key, 'desc') !== false || strpos($key, 'itemscustom') !== false) {
                $atts[$key] = str_replace($this->str_search, $this->str_relace_html, $val);
                if (strpos($atts[$key], '_TDK_IMG_DIR') !== false) {
                    // validate module
                    $atts[$key] = str_replace('_TDK_IMG_DIR/', $this->theme_img_module, $atts[$key]);
                }
            }
        }
        // validate module
        unset($is_gen_html);
        $assign = $w_info = array();
        $w_info['name'] = $this->name;
        if ($tag_name == 'TdkAccordion') {
            $assign['isSubTab'] = 1;
            $w_info['name'] = 'TdkAccordion';
        } else {
            //TDK:: check correct wrapper TdkAccordion
            $assign['isWrapper'] = 1;
            preg_match_all('/ parent_id="([^\"]+)"{0,1}/i', $content, $matches, PREG_OFFSET_CAPTURE);
            if (isset($matches['1']['0']['0'])) {
                $atts['id'] = $matches['1']['0']['0'];
            }
            
            if (!isset($atts['active_accordion'])) {
                $active_tab = 0;
                // validate module
                unset($active_tab);
            } else {
                # SET ACTIVE
                if (isset($atts['title']) && !empty($atts['title'])) {
                    $atts['active_accordion'] = $atts['active_accordion'] + 1;
                }
            }
        }
        $content = TdkShortCodesBuilder::doShortcode($content);
        $assign['tdkContent'] = $content;
        $assign['formAtts'] = $atts;
        $module = TdkPlatform::getInstance();
        return $module->fontContent($assign, $this->name.'.tpl');
    }
}
