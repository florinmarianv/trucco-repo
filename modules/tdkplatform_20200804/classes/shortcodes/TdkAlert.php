<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class TdkAlert extends TdkShortCodeBase
{
    public $name = 'TdkAlert';
    public $for_module = 'manage';

    public function getInfo()
    {
        return array('label' => $this->l('Alert'),
            'position' => 5,
            'desc' => $this->l('Alert Message box'),
            'icon_class' => 'icon-info-sign',
            'tag' => 'content');
    }

    public function getConfigList()
    {
        $types = array(
            array(
                'value' => 'alert-success',
                'text' => $this->l('Alert Success')
            ),
            array(
                'value' => 'alert-info',
                'text' => $this->l('Alert Info')
            ),
            array(
                'value' => 'alert-warning',
                'text' => $this->l('Alert Warning')
            ),
            array(
                'value' => 'alert-danger',
                'text' => $this->l('Alert Danger')
            )
        );
        $inputs = array(
            array(
                'type' => 'text',
                'name' => 'title',
                'label' => $this->l('Title'),
                'desc' => $this->l('Auto hide if leave it blank'),
                'lang' => 'true',
                'form_group_class' => 'tdkrow_general',
                'default' => ''
            ),
            array(
                'type' => 'textarea',
                'name' => 'sub_title',
                'label' => $this->l('Sub Title'),
                'lang' => true,
                'values' => '',
                'autoload_rte' => false,
                'default' => ''
            ),
            array(
                'type' => 'textarea',
                'lang' => true,
                'label' => $this->l('Content'),
                'name' => 'content_html',
                'cols' => 40,
                'rows' => 10,
                'value' => true,
                'lang' => true,
                'default' => '',
                'autoload_rte' => true,
            ),
            array(
                'type' => 'select',
                'label' => $this->l('Alert Type'),
                'name' => 'alert_type',
                'options' => array('query' => $types,
                    'id' => 'value',
                    'name' => 'text'),
                'default' => '1',
                'desc' => $this->l('Select a alert style')
            )
        );
        return $inputs;
    }
}
