/**
*  @copyright   2017 Webmecanik
*  @author      Webmecanik
*  @link        http://www.webmecanik.com
 */

;(function () {
	"use strict";

	var mauticConfiguration = {

		init: function () {

			// Formulaire de config oauth
			mauticConfiguration.$oauthForm = $('form.mautic_oauth');
			if (mauticConfiguration.$oauthForm.length === 1) {

				// Faut-il afficher le bouton de test de l'API ?
				var $hiddenFlag = mauticConfiguration.$oauthForm.find('#WEBMECANIK_SHOW_CHECK_BUTTON');
				if ($hiddenFlag.length > 0) {
					mauticConfiguration.checkBtn($hiddenFlag);
				}
			}

			// Formulaire de gestion du matching
			mauticConfiguration.$matchingForm = $('form.mautic_matching');
			if (mauticConfiguration.$matchingForm.length > 0) {
				mauticConfiguration.matchingForm.init();
			}
		},


		// Gestion du bouton CHECK OAUTH
		checkBtn: function ($hiddenFlag) {

			// Insertion du bouton dans le DOM
			var $lastFormGroup = mauticConfiguration.$oauthForm.find('.form-group:not(.hide):last'),
				$newFormGroup,
				$checkBtn,
				strings = $hiddenFlag.val().split('§'),
				buttonLabel = strings[0],
				successMessage = strings[1],
				failedMessage = strings[2];

			$lastFormGroup.clone().insertAfter($lastFormGroup);
			$newFormGroup = $lastFormGroup.next();
			$newFormGroup.children('label').removeClass('required').text('');
			$newFormGroup.children('div').html('<input type="button" value="" class="btn" />');
			$checkBtn = $newFormGroup.find('input[type=button]');
			$checkBtn.val(buttonLabel);
			$checkBtn.data('success', successMessage);
			$checkBtn.data('failed', failedMessage);

			// Clic sur le bouton : appel AJAX
			$checkBtn.click(function () {

				// Désactivation du bouton et nettoyage du message précédent éventuel
				$checkBtn.prop('disabled', true);
				$checkBtn.next('.response').remove();

                // Action passée en GET et en POST, pour la compatibilité avec Prestashop 1.6 et 1.7
				$.post(location.href + '&ajax&action=CheckOauth', {action: 'CheckOauth'}, function(ret) {

					// Message de succès ou d'échec et ré-activation du bouton
					var message = $checkBtn.data(ret.status == 1 ? 'success' : 'failed');
					$checkBtn.after('<strong class="response" style="margin-left:1em;">' + message + '</strong>');
					$checkBtn.prop('disabled', false);

				}, 'json');
			});


			// En cas de modification de l'un des champs de config, le bouton CHECK doit disparaitre

			var $configFields = mauticConfiguration.$oauthForm.find('input[type="text"]');

			$configFields.each(function () {
				var $input = $(this);
				$input.data('initial-value', $input.val());
			});

			$configFields.keyup(function (){
				mauticConfiguration.$oauthForm.trigger('UPDATE_CHECK_BTN_VISIBILITY');
			});

			mauticConfiguration.$oauthForm.on('UPDATE_CHECK_BTN_VISIBILITY', function () {

				$checkBtn.show();

				$configFields.each(function () {
					if ($(this).val() != $(this).data('initial-value')) {
						$checkBtn.hide()
					}
				});
			});

		},


		// Formulaire de gestion du matching
		matchingForm: {

			init: function () {

				// Afficher / masquer le champ "nom du nouveau segment" en fonction du choix pour le segment
				mauticConfiguration.$matchingForm.on('UPDATE_NEW_SEGMENT_FIELD', function () {

					var currentSegment = mauticConfiguration.$matchingForm.find('[name="WEBMECANIK_MATCHING_SEGMENT"]').val(),
						isNewSegment = (currentSegment == -1),
						$formGroup = mauticConfiguration.$matchingForm.find('[name="WEBMECANIK_MATCHING_NEW_SEGMENT"]').closest('.form-group');

					if (isNewSegment) {
						$formGroup.show();
					}
					else {
						$formGroup.hide();
					}
				});
				mauticConfiguration.$matchingForm.find('[name="WEBMECANIK_MATCHING_SEGMENT"]').change(function () {
					mauticConfiguration.$matchingForm.trigger('UPDATE_NEW_SEGMENT_FIELD');
				}).trigger('change');

				// Insertion du bouton "Ajouter" dans le DOM
				var $lastFormGroup = mauticConfiguration.$matchingForm.find('.form-group:not(.hide):last'),
					$newFormGroup,
					$addBtn,
					strings = mauticConfiguration.$matchingForm.find('[name="WEBMECANIK_MATCHING_STRINGS"]').val().split('§'),
					buttonLabel = strings[0];

				$lastFormGroup.clone().insertAfter($lastFormGroup);
				$newFormGroup = $lastFormGroup.next();
				$newFormGroup.children('label').removeClass('required').text('');
				$newFormGroup.children('div').html('<input type="button" value="" class="btn" />');
				$addBtn = $newFormGroup.find('input[type=button]');
				$addBtn.val(buttonLabel);

				// Clic sur le bouton "Ajouter"
				$addBtn.click(function () {

					// Lecture de la catégorie et du segment choisis
					var $form = mauticConfiguration.$matchingForm,
						categoryId = $form.find('[name="WEBMECANIK_MATCHING_CAT"]').val(),
						segmentId = $form.find('[name="WEBMECANIK_MATCHING_SEGMENT"]').val(),
						$associationsField = $form.find('[name="WEBMECANIK_MATCHING_ASSOCIATIONS"]'),
						tmpList = JSON.parse($associationsField.val()),
						segmentName,
						datas;

					if (tmpList === false) {
						tmpList = [];
					}

					// Nouvelle association
					datas = {
						categoryId: categoryId,
						segmentId: segmentId
					};

					// S'il s'agit d'un nouveau segment, on mémorise son nom
					if (segmentId < 0) {
						datas.segmentName = $form.find('[name="WEBMECANIK_MATCHING_NEW_SEGMENT"]').val();

						// Si le nom n'est pas saisi, STOP
						if (datas.segmentName.length < 2) {
							return;
						}
					}

					// Sauvegarde de la liste des associations à jour dans un champ caché
					// Puis mise à jour du DOM
					tmpList.push(datas);
					$associationsField.val(JSON.stringify(tmpList));
					$form.trigger('REFRESH_ASSSOCIATIONS_LIST');

					// Reset du formulaire d'ajout
					$form.find('[name="WEBMECANIK_MATCHING_CAT"] option:selected').prop('selected', false);
					$form.find('[name="WEBMECANIK_MATCHING_SEGMENT"] option:selected').prop('selected', false).parent('select').trigger('change');
					$form.find('[name="WEBMECANIK_MATCHING_NEW_SEGMENT"]').val('');
				});

				// Event : mettre à jour la liste HTML des associations à partir du champ caché contenant les données
				mauticConfiguration.$matchingForm.on('REFRESH_ASSSOCIATIONS_LIST', function () {
					var $form = mauticConfiguration.$matchingForm,
						$associationsWrapper = $form.find('#associationsWrapper'),
						associations = JSON.parse($form.find('[name="WEBMECANIK_MATCHING_ASSOCIATIONS"]').val()),
						html = "<ul class='list-group'>";

					// Lors du 1er appel, on crée le conteneur HTML pour la liste
					if ($associationsWrapper.length === 0) {
						$form.find('.panel-heading:first').after('<div id="associationsWrapper"></div>');
						$associationsWrapper = $form.find('#associationsWrapper');
					}

					// Construction de la liste HTML
					for(var k=0; k<associations.length; k++) {
						var association = associations[k],
							category = mauticConfiguration.matchingForm.getCategory(association.categoryId),
							categoryName = category.fullName,
							segmentName;

						if (association.segmentId < 0) {
							segmentName = association.segmentName;
						}
						else {
							segmentName = mauticConfiguration.matchingForm.getSegmentName(association.segmentId);
						}

						html += "<li class='list-group-item' data-index='" + k + "'>" +
									"<i class='delete material-icons' style='float:right; margin-top:-8px; cursor:pointer;'>delete</i>" +
									categoryName + " : <strong>" + segmentName + "</strong>" +
								"</li>";
					}
					html += "</ul>";

					// Mise à jour du DOM
					$associationsWrapper.html(html);

				}).trigger('REFRESH_ASSSOCIATIONS_LIST');

				// Suppression d'un élément de la liste
				mauticConfiguration.$matchingForm.on('click', '#associationsWrapper ul li .delete', function () {
					var k = $(this).closest('li').data('index'),
						$form = mauticConfiguration.$matchingForm,
						$associations = $form.find('[name="WEBMECANIK_MATCHING_ASSOCIATIONS"]'),
						associations = JSON.parse($associations.val());

					// Suppression de l'élément dans le champ caché
					delete associations[k];
					associations = associations.filter(function(val){return val});
					$associations.val(JSON.stringify(associations));

					// Mise à jour de la liste dans le DOM
					$form.trigger('REFRESH_ASSSOCIATIONS_LIST');
				});
			},


			// Liste complète des catégories
			getCategories: function () {

				var rawCategories = mauticConfiguration.$matchingForm.find('[name="WEBMECANIK_MATCHING_ALL_CATEGORIES"]').val(),
					categories = JSON.parse(rawCategories);

				return categories;
			},


			// Informations sur une catégorie
			getCategory: function (categoryId) {
				var categories = mauticConfiguration.matchingForm.getCategories();
				return categoryId in categories ? categories[categoryId] : '';
			},


			// Liste complète des segments
			getSegments: function () {

				var rawSegments = mauticConfiguration.$matchingForm.find('[name="WEBMECANIK_MATCHING_ALL_SEGMENTS"]').val(),
					segments = JSON.parse(rawSegments);

				return segments;
			},


			// Recherche du nom d'un segment d'après son ID
			getSegmentName: function (segmentId) {
				var segments = mauticConfiguration.matchingForm.getSegments();
				return segmentId in segments ? segments[segmentId] : 'SEGMENT NOT FOUND';
			}
		}
	};

	$(document).ready(mauticConfiguration.init);

})(jQuery);
