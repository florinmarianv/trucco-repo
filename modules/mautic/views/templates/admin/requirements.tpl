{*
*
*  @copyright   2017 Webmecanik
*  @author      Webmecanik
*  @link        http://www.webmecanik.com
*}

<div class="panel">
	<h3>{l s='1. Requirements' mod='mautic'}</h3>
	<ol>
		<li>
            {l s='In your Mautic account, in Configuration > API settings : activer API' mod='mautic'}
		</li>
		<li>
            {l s='From API credentials, add a new OAuth2 entry' mod='mautic'}

            {l s='using the redirection URL' mod='mautic'} <strong>{$homeUrl|escape:'html':'UTF-8'}</strong>
		</li>
	</ol>
</div>
