<?php
/**
*  PrestaShop Module : sync to Mautic solution
*
*  @author    Webmecanik
*  @copyright 2017 Webmecanik
*  @license   Proprietary
*  @link      http://www.webmecanik.com
*/

class Mautic extends Module
{
    // Dépendances de cette classe
    protected $services;

    protected $logger;

    // Compteur d'instances
    private static $instancesCounter = 0;

    public function __construct()
    {
        // Installation de l'auto-loader de classes pour le module
        // Appelé une seule fois
        self::$instancesCounter++;
        if (self::$instancesCounter === 1) {
            $this->installAutoloader();
        }

        $this->name = 'mautic';
        $this->tab = 'advertising_marketing';
        $this->version = '1.4.0';
        $this->author = 'Webmecanik';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6');
        $this->dependencies = array();
        $this->bootstrap = true;
        $this->module_key = '6129563193515c13320f8993c7085828';

        parent::__construct();

        $this->displayName = $this->l('Mautic sync');
        $this->description = $this->l(
            'Prestashop contacts synchronisation to Mautic marketing automation software.'
        );

        $this->confirmUninstall = $this->l('Do you want to remove this add-on ?');

        if (_PS_VERSION_ >= '1.7.0.0') {
            $this->logger = new PrestaShop\PrestaShop\Adapter\LegacyLogger();
        }

        // Dépendances
        $apiManagerInstance = new MauticApiManager($this);
        $matchingInstance = new MauticMatching($apiManagerInstance);
        $this->services = (object) array(
            'apiManager' => $apiManagerInstance,
            'matching' => $matchingInstance,
            'sync' => new MauticSync($this)
        );
    }


    // Autoloader spécifique pour les dépendances du module
    private function installAutoloader()
    {
        spl_autoload_register(function ($class) {

            // Recherche dans 'vendor'
            $filename = dirname(__FILE__) . "/vendor/" . str_replace("\\", '/', $class) . ".php";
            if (file_exists($filename)) {
                include_once($filename);
                return class_exists($class);
            }

            // Recherche dans 'lib'
            $classSegments = explode("\\", $class);
            $className = array_pop($classSegments);
            $filename = dirname(__FILE__) . "/lib/" . $className . ".php";
            if (file_exists($filename)) {
                include_once($filename);
                return class_exists($class);
            }
            return false;
        });
    }


    public function getApiManagerService()
    {
        return $this->services->apiManager;
    }


    public function getMatchingService()
    {
        return $this->services->matching;
    }

    /**
     * @return MauticSync
     */
    public function getSyncService()
    {
        return $this->services->sync;
    }


    public function install()
    {
        $response = parent::install() && $this->registerHook('newOrder') && $this->registerHook('backOfficeHeader');

        if (_PS_VERSION_ >= '1.7.0.0') {
            $response = $response && $this->registerHook('displayCustomerIdentityForm');
        }
        return $response;
    }


    public function uninstall()
    {
        return parent::uninstall();
    }


    // Page "Configure" du module, appelée pour l'affichage ET la soumission du formulaire
    public function getContent()
    {
        $output = '';
        // SUBMIT EVENT : Enregistrement des clés oAuth
        // Puis obtention d'un token
        if (Tools::isSubmit('submit_oauth_config')) {
            $baseUrl = trim(Tools::getValue('WEBMECANIK_BASE_URL'));
            $clientKey = trim(Tools::getValue('WEBMECANIK_CLIENT_KEY'));
            $clientSecret = trim(Tools::getValue('WEBMECANIK_CLIENT_SECRET'));

            if (empty($baseUrl)) {
                $output .= $this->displayError($this->l('Please write your Mautic instance URL.'));
            } elseif (empty($clientKey) || empty($clientSecret)) {
                $errorMessage = $this->l('Please write your OAuth keys given by Mautic.');
                $output .= $this->displayError($errorMessage);
            } else {
                // Test de l'URL saisie
                $testUrl = rtrim($baseUrl, '/').'/oauth/v2/authorize';
                $isUrlValid = (filter_var($testUrl, FILTER_VALIDATE_URL) !== false);
                $headers = $isUrlValid ? @get_headers($testUrl) : false;
                if ($headers !== false) {
                    if (strpos($headers[0], '200') === false &&
                        strpos($headers[0], '301') === false &&
                        strpos($headers[0], '302') === false
                    ) {
                        $headers = false;
                    }
                }
                if ($headers === false) {
                    $output .= $this->displayError($this->l('Please write your Mautic instance URL.'));
                } else {
                    // Sauvegarde oAuth config
                    $this->services->apiManager->saveOauthConfig($baseUrl, $clientKey, $clientSecret);

                    // Etablissement oAuth (phase 1, qui redirige le navigateur vers Mautic)
                    $this->log('Init oAuth step 1');
                    $this->services->apiManager->authorizeStep1();
                }
            }
        }

        // OAUTH EVENT : déclenché après la phase 1 de oauth2. Il faut ensuite demander un token.
        if (Tools::getValue('state') && Tools::getValue('code')) {
            try {
                $this->log('Init oAuth step 2');
                $this->services->apiManager->authorizeStep2();

                $this->log('Init oAuth : SUCCESS');
                $output .= $this->displayConfirmation(
                    $this->l('The connection between Prestashop and Mautic has been establish with success.').
                    $this->l('You can test your access to continue your configuration.')
                );
            } catch (\Mautic\Exception\IncorrectParametersReturnedException $e) {
                $this->log('Init oAuth : FAILED', true);
                $errorMessage = $this->l('The authorisation code has expired. Please re-try.');
                $output .= $this->displayError($errorMessage);
            }
        }

        // SUBMIT EVENT : Enregistrement du matching
        if (Tools::isSubmit('submit_matching')) {
            // Lecture des associations
            $rawAssociations = trim(Tools::getValue('WEBMECANIK_MATCHING_ASSOCIATIONS'));
            $associations = Tools::jsonDecode($rawAssociations, true);

            // Phase 1 : création des nouveaux segments éventuels
            $hasErrors = false;
            foreach ($associations as $a => $association) {
                if ($association['segmentId'] < 0) {
                    $newSegment = $this->services->matching->createNewSegment($association['segmentName']);

                    if ($newSegment === false) {
                        $output .= $this->displayError($this->l('Impossible to establish connection with Mautic.'));
                        $hasErrors = true;
                        $this->log('Create segment FAILED (API auth error) : '.$association['segmentName'], true);
                        break;
                    } elseif (array_key_exists('error', $newSegment)) {
                        $errorMessage = $newSegment['error']['message'];
                        $output .= $this->displayError($errorMessage);
                        $this->log('Create segment FAILED : '.$association['segmentName'].' : '.$errorMessage, true);
                        $hasErrors = true;
                        break;
                    } else {
                        // Le nouveau segment a été créé : on revient sur le cas général
                        $associations[$a]['segmentId'] = $newSegment['list']['id'];
                        $output .= $this->displayConfirmation(
                            sprintf(
                                $this->l('The segment "%s" has been created with success.'),
                                $association['segmentName']
                            )
                        );
                        $this->log('Create segment SUCCESS : '.$association['segmentName']);
                        unset($associations[$a]['segmentName']);
                    }
                }
            }

            // Phase 2 : sauvegarde du matching
            if (!$hasErrors) {
                $this->services->matching->saveMatching($associations);

                $output .= $this->displayConfirmation($this->l(
                    'Categories and segments matching has been saved with success.'
                ));
            }
        }

        // Notice d'explication
        $output .= $this->getConfigInfos();

        // Formulaire : configuration oAuth
        $oauthConfig = $this->services->apiManager->getOauthConfig();
        $tokenExists = $this->services->apiManager->tokenExists();
        $output .= $this->buildConfigForm($oauthConfig, $tokenExists);

        // Formulaire : matching des catégories avec les segments
        $segments = $this->services->matching->getSegments();
        if ($segments !== false) {
            $flattenCategories = $this->services->matching->getFlattenCategories();
            $associations = $this->services->matching->getMatching();

            $output .= $this->buildMatchingForm($flattenCategories, $segments, $associations);
        }

        $output .= $this->buildContactFieldsMatchingForm();
        return $output;
    }


    // AJAX : vérification de la connexion oauth
    public function ajaxProcessCheckOauth()
    {
        $status = $this->services->apiManager->checkAuth();

        $this->log('Check oAuth : '.($status == 1 ? 'SUCCESS' : 'FAILED'), $status == 0);

        echo Tools::jsonEncode(array(
            'status' => (int)$status
        ));
        exit;
    }


    public function hookBackOfficeHeader()
    {
        // Script JS nécessaire à la page de config
        $this->context->controller->addJquery();
        $this->context->controller->addJS($this->_path.'/views/js/configurationPage.js');
    }


    // HOOK : appelé après la validation d'une commande
    public function hookActionValidateOrder($params)
    {
        if (array_key_exists('customer', $params)) {
            $customerId = $params['customer']->id;

            $this->log('Trigger order validation customerId='.$customerId);

            if ($customerId > 0) {
                $customer = new \Customer($customerId);

                // Synchronisation des infos générales du client
                $mauticContactId = $this->services->sync->syncCustomerDatas($customer);
                if ($mauticContactId !== false) {
                    $this->log('Customer datas sync mauticContactId='.$mauticContactId.' SUCCESS');

                    // Synchronisation des segments
                    $response = $this->services->sync->syncCustomerSegments($customerId, $mauticContactId);
                    $this->log(
                        'Customer Segments sync mauticContactId='.$mauticContactId.' '.
                        ($response ? 'SUCCESS' : 'FAILED'),
                        !$response
                    );

                    // Synchronisation des flags "newsletter" et "offres partenaires (champ optin)"
                    $response = $this->services->sync->syncCustomerOptinFlags($customer, $mauticContactId);
                    $this->log(
                        'Customer OptIn flag sync mauticContactId='.$mauticContactId.' '.
                        ($response ? 'SUCCESS' : 'FAILED'),
                        !$response
                    );
                } else {
                    $this->log('Customer sync FAILED', true);
                }
            }
        }
    }


    // HOOK : affichage du compte client
    // Déclenche une mise à jour du flag newsletter
    // Uniquement pour Presta 1.7+
    public function hookDisplayCustomerIdentityForm($params)
    {
        if (array_key_exists('cart', $params)) {
            $customerId = $params['cart']->id_customer;

            if ($customerId > 0) {
                $customer = new \Customer($customerId);

                $mauticContactId = $this->services->apiManager->getContactIdByEmail($customer->email);

                if ($mauticContactId > 0) {
                    $response = $this->services->sync->syncCustomerOptinFlags($customer, $mauticContactId);
                    $this->log(
                        'Customer OptIn flag sync mauticContactId='.$mauticContactId.' '.
                        ($response ? 'SUCCESS' : 'FAILED'),
                        !$response
                    );
                }
            }
        }
    }

    // Notice d'utilisation
    private function getConfigInfos()
    {
        $this->context->smarty->assign(array('homeUrl' => _PS_BASE_URL_.__PS_BASE_URI__));
        return $this->display(dirname(__FILE__), 'views/templates/admin/requirements.tpl');
    }

    // Formulaire de config du module
    private function buildConfigForm($oauthConfig, $tokenExists)
    {
        $helper = $this->getFormHelperInstance('mautic_oauth', 'submit_oauth_config');

        $fields_form = array(
            0 => array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l('2. Mautic connexion')
                    ),
                    'input' => array(
                        array(
                            'type' => 'text',
                            'label' => $this->l('URL root of Mautic'),
                            'name' => 'WEBMECANIK_BASE_URL',
                            'size' => 128,
                            'required' => true
                        ),
                        array(
                            'type' => 'text',
                            'label' => $this->l('Public key'),
                            'name' => 'WEBMECANIK_CLIENT_KEY',
                            'size' => 64,
                            'required' => true
                        ),
                        array(
                            'type' => 'text',
                            'label' => $this->l('Secret key'),
                            'name' => 'WEBMECANIK_CLIENT_SECRET',
                            'size' => 64,
                            'required' => true
                        )
                    ),
                    'submit' => array(
                        'title' => $this->l('Save and establish connection'),
                        'class' => 'btn btn-default button pull-right'
                    )
                )
            )
        );

        // Si un token est défini, on demande à JS d'afficher et de gérer le bouton "CHECK OAUTH"
        if ($tokenExists) {
            array_push(
                $fields_form[0]['form']['input'],
                array(
                    'type' => 'hidden',
                    'name' => 'WEBMECANIK_SHOW_CHECK_BUTTON'
                )
            );
            $helper->fields_value['WEBMECANIK_SHOW_CHECK_BUTTON'] = $this->l('Test connection').'§'.
            $this->l('Connection establish with success').'§'.
            $this->l('Connection failed');
        }

        // Remplissage des valeurs
        $helper->fields_value['WEBMECANIK_BASE_URL'] = $oauthConfig['baseUrl'];
        $helper->fields_value['WEBMECANIK_CLIENT_KEY'] = $oauthConfig['clientKey'];
        $helper->fields_value['WEBMECANIK_CLIENT_SECRET'] = $oauthConfig['clientSecret'];

        return $helper->generateForm($fields_form);
    }


    private function buildContactFieldsMatchingForm()
    {
        $helper = $this->getFormHelperInstance('fields_matching', 'submit_fields_matching');

        if (Tools::isSubmit('submit_fields_matching')) {
            foreach (array_keys($this->getSyncService()->getFields()->getFieldsFromPrestashopWithPrefix()) as $field) {
                Configuration::updateValue($field, Tools::getValue($field));
            }
        }

        $fields_form = array(
            0 => array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l('4. Contact fields mapping')
                    ),
                    'submit' => array(
                        'title' => $this->l('Save'),
                        'class' => 'btn btn-default button pull-right'
                    )
                )
            )
        );

        $fieldsFromPrestashop = $this->getSyncService()->getFields()->getFieldsFromPrestashop();
        foreach ($fieldsFromPrestashop as $fieldFromPrestashop) {

            $alias = $this->name.$fieldFromPrestashop;

            $helper->fields_value[$alias] = Configuration::get($alias,  null, null, null, $this->getSyncService()->getFields()->getDefaultField($fieldFromPrestashop));

            // If we are using special custom field
            switch ($alias) {
                case $this->name.'tags':
                    $this->getTagsOptions($fields_form);
                    continue 2;
                    break;
            }

            $fields_form[0]['form']['input'][] = array(
                'type' => 'select',
                'label' => $this->l($fieldFromPrestashop),
                'name' => $alias,
                'options' => array(
                    'query' => $this->getSyncService()->getFields()->getMauticContactFields(),
                    'id' => 'alias',
                    'name' => 'label',
                ),
            );

        }

        return $helper->generateForm($fields_form);

    }

    /**
     * @param $fields_form
     */
    private function getTagsOptions(&$fields_form)
    {
        $productsTagFormat = array();
        $productsTagFormat[]= [
            'key'=>'',
            'value'=> $this->l('-skip-'),
        ];
        $productsTagFormat[]= [
            'key'=>'id',
            'value'=> $this->l('Product ID'),
        ];
        $productsTagFormat[]= [
            'key'=>'name',
            'value'=> $this->l('Product name'),
        ];
        $productsTagFormat[]= [
            'key'=>'id_name',
            'value'=> $this->l('Product ID') .' + '. $this->l('Product name'),
        ];
        $productsTagFormat[]= [
            'key'=>'category_name',
            'value'=> $this->l('Product Category') .' + '. $this->l('Product name'),
        ];

        $productsTagFormat[]= [
            'key'=>'category_name_id',
            'value'=> $this->l('Product Category') .' + '. $this->l('Product name') .' + '. $this->l('Product ID'),
        ];

        $fields_form[0]['form']['input'][] = array(
            'type' => 'select',
            'label' => $this->l('Tags'),
            'name' => $this->name.'tags',
            'options' => array(
                'id' => 'key',
                'name' => 'value',
                'query' => $productsTagFormat
            )
        );
    }


    // Formulaire pour le matching des catégories et des segments
    private function buildMatchingForm($flattenCategories, $segments, $associations)
    {
        $helper = $this->getFormHelperInstance('mautic_matching', 'submit_matching');

        // Mise en forme de la liste des catégories pour le SELECT
        $categoriesOptions = array();
        foreach ($flattenCategories as $categoryId => $category) {
            $prefix = '';
            for ($i=0; $i < $category['depth']; $i++) {
                $prefix .= '&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            $categoriesOptions[] = array(
                'option_key' => $categoryId,
                'option_value' => trim($prefix.' '.$category['name'])
            );
        }

        // Mise en forme de la liste des segments pour le SELECT
        $segmentsOptions = array();
        $segmentsOptions[] = array(
            'option_key' => -1,
            'option_value' => $this->l('New segment')
        );
        foreach ($segments as $segmentId => $segmentName) {
            $segmentsOptions[] = array(
                'option_key' => $segmentId,
                'option_value' => $segmentName
            );
        }

        $fields_form = array(
            0 => array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l('3. Prestashop categories and Mautic segments association')
                    ),
                    'input' => array(
                        array(
                            'type' => 'select',
                            'label' => $this->l('Article categories'),
                            'name' => 'WEBMECANIK_MATCHING_CAT',
                            'required' => true,
                            'options' => array(
                                'id' => 'option_key',
                                'name' => 'option_value',
                                'query' => $categoriesOptions
                            )
                        ),
                        array(
                            'type' => 'select',
                            'label' => $this->l('Mautic segment associated'),
                            'name' => 'WEBMECANIK_MATCHING_SEGMENT',
                            'required' => true,
                            'options' => array(
                                'id' => 'option_key',
                                'name' => 'option_value',
                                'query' => $segmentsOptions
                            )
                        ),
                        array(
                            'type' => 'text',
                            'label' => $this->l('New segment name'),
                            'name' => 'WEBMECANIK_MATCHING_NEW_SEGMENT',
                            'size' => 64,
                            'required' => true
                        ),
                        array(
                            'type' => 'hidden',
                            'name' => 'WEBMECANIK_MATCHING_STRINGS'
                        ),
                        array(
                            'type' => 'hidden',
                            'name' => 'WEBMECANIK_MATCHING_ALL_CATEGORIES'
                        ),
                        array(
                            'type' => 'hidden',
                            'name' => 'WEBMECANIK_MATCHING_ALL_SEGMENTS'
                        ),
                        array(
                            'type' => 'hidden',
                            'name' => 'WEBMECANIK_MATCHING_ASSOCIATIONS'
                        )
                    ),
                    'submit' => array(
                        'title' => $this->l('Save'),
                        'class' => 'btn btn-default button pull-right'
                    )
                )
            )
        );

        $helper->fields_value['WEBMECANIK_MATCHING_CAT'] = -1;
        $helper->fields_value['WEBMECANIK_MATCHING_SEGMENT'] = -1;
        $helper->fields_value['WEBMECANIK_MATCHING_NEW_SEGMENT'] = "";

        // Textes traductibles utilisés par le formulaire
        $helper->fields_value['WEBMECANIK_MATCHING_STRINGS'] = $this->l('Add');

        // Transmission à JS de la liste des catégories et des segments
        $helper->fields_value['WEBMECANIK_MATCHING_ALL_CATEGORIES'] = Tools::jsonEncode($flattenCategories);
        $helper->fields_value['WEBMECANIK_MATCHING_ALL_SEGMENTS'] = Tools::jsonEncode($segments);

        // Liste des associations déjà établies
        $helper->fields_value['WEBMECANIK_MATCHING_ASSOCIATIONS'] = Tools::jsonEncode($associations);

        return $helper->generateForm($fields_form);
    }


    // Initialisation d'un formulaire Prestashop, commune à tous les formulaires du panneau de config
    private function getFormHelperInstance($formSlug, $submitAction)
    {
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $formSlug;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name; /* URI courante */
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->show_toolbar = false;
        $helper->submit_action = $submitAction;

        return $helper;
    }

    public function log($message, $isError = false)
    {
        if (_PS_VERSION_ >= '1.7.0.0') {
            if ($isError) {
                $this->logger->error('WEBMECANIK : '.$message.' : '.date('H:i:s'));
            } else {
                $this->logger->info('WEBMECANIK : '.$message.' : '.date('H:i:s'));
            }
        } else {
            $severity = $isError ? 3 : 1;
            \Logger::addLog('WEBMECANIK : '.$message.' : '.date('H:i:s'), $severity);
        }
    }
}
