<?php
/*
 * Gestion du matching entre les catégories Prestashop et les segments Mautic
 *
 */

class MauticMatching
{
	protected $_services;
	protected $_segmentsApi;

	// Injection de dépendances : cette classe a besoin de l'API Mautic
	public function __construct(MauticApiManager $apiManager)
	{
		$this->_segmentsApi = false;

		$this->_services = (object)array(
			'apiManager' => $apiManager
		);
	}


	// Retourne la liste des catégoories Prestashop sous forme d'un flatten tree
	public function getFlattenCategories()
	{
		$categories = \Category::getNestedCategories();
		return $this->_flattenCategories($categories);
	}


	// Retourne tous les segments présents dans Mautic, ou false en cas d'erreur
	public function getSegments()
	{
		$segmentsApi = $this->_getSegmentsApi();
		if ($segmentsApi === false) {
			return false;
		}

		// Lecture de tous les segments sous la forme ID => NAME
		$rawSegments = $segmentsApi->getList('', 0, 999);
		if (isset($rawSegments['error']) || !isset($rawSegments['lists'])) {
			return false;
		}
		$segments = array();
		foreach($rawSegments['lists'] as $segment) {

			$segments[ $segment['id'] ] = $segment['name'];
		}
		return $segments;
	}


	// Création d'un nouveau segment dans Mautic
	public function createNewSegment($newSegmentName)
	{
		$segmentsApi = $this->_getSegmentsApi();
		if ($segmentsApi === false) {
			return false;
		}

		$newSegment = $segmentsApi->create(array(
			'name' => $newSegmentName,
			'isPublished' => 1
		));

		return $newSegment;
	}


	// Sauvegarde du matching : liste d'associations entre des segmentId et des categoryId
	public function saveMatching($associations)
	{
		\Configuration::updateValue('WEBMECANIK_MATCHING', serialize($associations));
	}


	// Retourne le matching mémorisé dans la config
	public function getMatching()
	{
		return unserialize(\Configuration::get('WEBMECANIK_MATCHING'));
	}


	// Applique le mapping à une liste de catégories.
	// Retourne une liste de segments
	public function getSegmentsFromCategories($categoriesIds)
	{
		// Lecture de la config
		$associations = $this->getMatching();
		if (empty($associations)) {
			return array();
		}

		// Recherche des segments distincts associés aux catégories
		$segmentsIds = array();
		foreach($associations as $association) {
			if (in_array($association['categoryId'], $categoriesIds)) {
				$segmentsIds[] = $association['segmentId'];
			}
		}

		return $segmentsIds;
	}


	// Ajoute un contact à un segment
	public function addContactToSegment($contactId, $segmentId)
	{
		$segmentApi = $this->_getSegmentsApi();
		if ($segmentApi !== false) {
			$response = $segmentApi->addContact($segmentId, $contactId);
			return isset($response['success']);
		}
		return false;
	}


	// Retourne le endpoint "segments" de l'API Mautic, ou false si la connexion échoue
	private function _getSegmentsApi()
	{
		if ($this->_segmentsApi !== false) {
			return $this->_segmentsApi;
		}

		// Tentative de connexion à l'API 'segments'
		if ( !$this->_services->apiManager->tokenExists()) {
			return false;
		}
		$this->_segmentsApi = $this->_services->apiManager->getApiEndpoint('segments');
		return $this->_segmentsApi;
	}


	// Algo récursif pour aplatir l'arbre des catégories
	private function _flattenCategories($categories, $depth = 0, $path = '')
	{
		$flattenCategories = array();

		foreach($categories as $category) {

			$categoryId = $category['id_category'];

			$fullName = trim($path.' > '.$category['name']);

			$flattenCategories[$categoryId] = array(
				'id' => $categoryId,
				'name' => $category['name'],
				'fullName' => $fullName,
				'depth' => $depth
			);

			if ( !empty($category['children'])) {
				$flattenCategories += $this->_flattenCategories($category['children'], $depth + 1, $fullName);
			}
		}

		return $flattenCategories;
	}

}
