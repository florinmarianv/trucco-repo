<?php
/*
 * Ce fichier contient tout le nécessaire pour gérer la connexion oAuth2 avec Mautic :
 * - Sauvegarde des clés oauth et des token
 * - Obtention ou rafraîchissement d'un token
 * - Appel de l'API Mautic
 *
 * Dépendances : /vendor/Psr et /vendor/Mautic
 *
 */

class MauticApiManager
{
    private $module;

    public function __construct($module)
    {
        $this->module = $module;
    }

	// Retourne l'URL de Mautic et les clés oAuth : clientKey et le clientSecret
	public function getOauthConfig()
	{
		$settings = $this->_getOauthSettings();
		return array(
			'baseUrl' => isset($settings['baseUrl']) ? $settings['baseUrl'] : '',
			'clientKey' => isset($settings['clientKey']) ? $settings['clientKey'] : '',
			'clientSecret' => isset($settings['clientSecret']) ? $settings['clientSecret'] : '',
			'callback' => $this->_getCallbackUrl(),
			'version' => 'OAuth2'
		);
	}

	// Mémorise la config oAuth (sans les token, juste les clés)
	public function saveOauthConfig($baseUrl, $clientKey, $clientSecret)
	{
		$settings = $this->getOauthConfig();

		$settings['baseUrl'] = rtrim(trim($baseUrl), '/');
		$settings['clientKey'] = trim($clientKey);
		$settings['clientSecret'] = trim($clientSecret);

		$this->_setOauthSettings($settings);
	}

	// Force l'obtention d'un token, même s'il en existe déjà un.
	// Attention, cette méthode redirige vers Mautic avec la fonction header().
	// Rien ne doit donc s'afficher avant, et il ne faut pas l'appeler en AJAX.
	public function authorizeStep1()
	{
		$settings = $this->_getOauthSettings();

		if (empty($settings['baseUrl']) || empty($settings['clientKey']) || empty($settings['clientSecret'])) {
			return false;
		}

		// Paramètres minimaux
		unset($settings['accessToken']);
		unset($settings['accessTokenExpires']);
		unset($settings['refreshToken']);

		// Sauvegarde ce cette config pour éviter d'avoir d'anciens token qui persistent
		$this->_setOauthSettings($settings);

		$auth = \Mautic\Auth\ApiAuth::initiate($settings);
		$auth->validateAccessToken();
	}

	// Phase 2 de oAuth2 : à partir du code obtenu à la phase 1, appel CURL pour avoir un token
	public function authorizeStep2()
	{
		$settings = $this->_getOauthSettings();

		$auth = \Mautic\Auth\ApiAuth::initiate($settings);
		if ($auth->validateAccessToken()) {

			// Sauvegarde du token, du token refresh et de la date d'expiration
			$datas = $auth->getAccessTokenData();
			$this->_saveToken($datas);

			return true;
		}

		return false;
	}

	// Vérifie si la config mémorisée contient un token
	// (ne vérifie pas s'il fonctionne)
	public function tokenExists()
	{
		$settings = $this->_getOauthSettings();
        if(isset($settings['accessToken']) && !empty($settings['accessToken'])){
            if ($this->getApiEndpoint('contacts')) {
                return true;
            }
        }
        return false;
	}

	// Retourne un objet Mautic pour manipuler l'un des concepts de l'API
	public function getApiEndpoint($endpoint)
	{
		$settings = $this->_getOauthSettings();
		$auth = \Mautic\Auth\ApiAuth::initiate($settings);

		try {
			if ($auth->validateAccessToken()) {

				// Sauvegarde du token car il a pu être renouvellé par validateAccessToken
				$datas = $auth->getAccessTokenData();
				$this->_saveToken($datas);

				$apiEndpoint = \Mautic\MauticApi::getContext(
				    $endpoint,
				    $auth,
				    $settings['baseUrl'] . '/api/'
				);

				return $apiEndpoint;
			}
		}
		catch(Exception $e) {
            $this->module->log($e->getMessage());
			return false;
		}

		return false;
	}

	// Teste le fonctionnement de l'API avec le token connu
	public function checkAuth()
	{
		$contactsApi = $this->getApiEndpoint('contacts');
		if ($contactsApi === false) {
			return false;
		}

		// Appel simple à l'API pour tester son fonctionnement
		$list = $contactsApi->getList('', 0, 1);

		return !isset($list['error']);
	}


	// Recherche d'un contact dans Mautic d'après son email
	public function getContactIdByEmail($email)
	{
		$contactsApi = $this->getApiEndpoint('contacts');
		if ($contactsApi !== false) {

			$response = $contactsApi->getList('email:'.$email, 0, 1);
			if (isset($response['contacts'])) {

				// Si le contact existe déjà...
				if ( !empty($response['contacts'])) {
				    $contact = reset($response['contacts']);
                    if (!empty($contact)) {
					    $mauticContactId = $contact['id'];
					    return $mauticContactId;
                    }
                    return -1;
				}
				else {
					return -1;
				}
			}
		}

		return false;
	}


	// Met à jour ou crée (si mautic id vaut -1) un contact dans Mautic. Retourne le ID ou -1 en cas d'erreur.
	public function updateOrCreateContact($mauticContactId, $mauticContactDatas, $logger = null)
	{
		$contactsApi = $this->getApiEndpoint('contacts');
		if ($contactsApi !== false) {

			if ($mauticContactId > 0) {
				$response = $contactsApi->edit($mauticContactId, $mauticContactDatas);
			}
			else {
				$response = $contactsApi->create($mauticContactDatas);
			}

            if ($logger) {
                $logger->log(base64_encode(serialize($response)));
            }

			if (isset($response['contact']['id'])) {
				return $response['contact']['id'];
			}
		}
        return $response;
	}


	public function setDoNotContactFlag($mauticContactId, $doNotContact)
	{
		$contactsApi = $this->getApiEndpoint('contacts');
		if ($contactsApi === false) {
			return false;
		}

		if ($doNotContact) {
			$response = $contactsApi->addDnc($mauticContactId, 'email', \Mautic\Api\Contacts::MANUAL, null, 'Prestashop sync');
		}
		else {
			$response = $contactsApi->removeDnc($mauticContactId, 'email');
		}

        // Réponses positives :
        // L'object contact complet
        // ou une erreur 400, qui signifie qu'il n'y a pas eu de changement
		return (
            isset($response['contact']['id']) ||
            (isset($response['error']['code']) && $response['error']['code'] == 400)
        );
	}


	// Enregistre le token, son expiration et le refresh_token
	private function _saveToken($datas)
	{
		$settings = $this->_getOauthSettings();

		$settings['accessToken'] = $datas['access_token'];
		$settings['accessTokenExpires'] = $datas['expires'];
		$settings['refreshToken'] = $datas['refresh_token'];

		$this->_setOauthSettings($settings);
	}




	//////// Dépendant de Prestashop : à adpater pour ré-utiliser dans un autre projet

	// Lecture de la config
	private function _getOauthSettings()
	{
		$settings = unserialize(\Configuration::get('WEBMECANIK_OAUTH_SETTINGS'));
		if (!$settings) {
			$settings = array();
		}
		return $settings;
	}

	// Ecriture de la config
	private function _setOauthSettings($settings)
	{
		\Configuration::updateValue('WEBMECANIK_OAUTH_SETTINGS', serialize($settings));
	}

    // URL de callback pour la phase 1 de oAuth2
	private function _getCallbackUrl()
	{
        $callback = \Context::getContext()->link->getAdminLink('AdminModules', true).'&configure=mautic';

        // Compatibilité Prestashop 1.6
        if (substr($callback, 0, 4) !== 'http') {
            $admin = explode(DIRECTORY_SEPARATOR,_PS_ADMIN_DIR_);
            $admin = array_slice($admin, -1);
            $admin_folder = array_pop($admin);
            $admin_url = rtrim(_PS_BASE_URL_.__PS_BASE_URI__.$admin_folder, '/').'/';
            $callback = $admin_url.$callback;
        }

        return $callback;
	}
}
