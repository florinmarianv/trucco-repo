<?php
/*
 * Synchronisation des clients Prestashop avec Mautic
 *
 */

class MauticSync
{
	protected $_module;

    private $_contactApi;

    /** @var Fields  */
    private $_fields;

    // Injection de dépendances
	public function __construct(\Mautic $module)
	{
		$this->_module = $module;
        $this->_fields = new Fields($this);
	}

	// Synchronise un client avec le contact Mautic correspondant
	// Si le contact n'existe pas, il est créé
	// Retourne l'ID du contact Mautic, ou false en cas d'erreur
	public function syncCustomerDatas(\Customer $customer)
	{
        $mapper = new Mapper($this->getFields()->getFieldsToMapping());
        $mauticContactDatas = $mapper->getFieldsValuesToMapping();
        $this->_module->log($customer->id.': '.base64_encode(serialize($mauticContactDatas)));

		// Le client existe-t-il déjà dans Mautic ?
        // Si pas trouvé retourne -1, si erreur API retourne false
		$mauticContactId = $this->_module->getApiManagerService()->getContactIdByEmail($customer->email);

		// Mise à jour ou création
		if ($mauticContactId !== false) {
			$mauticContactId = $this->_module->getApiManagerService()->updateOrCreateContact($mauticContactId, $mauticContactDatas, $this->_module);
            if (is_array($mauticContactId) && isset($mauticContactId['errors'][0]['message'])) {
                $this->_module->log($mauticContactId['errors'][0]['message']);
                return false;
            }
		}
		return ($mauticContactId > 0) ? $mauticContactId : false;
	}


	// Ajoute un contact Mautic aux segments associés avec sa dernière commande passée
	public function syncCustomerSegments($prestashopCustomerId, $mauticContactId)
	{
		// Recherche de la dernière commande du client, sans tenir compte du statut de cette commande
        /** @var  $orders */
		$orders = \Order::getCustomerOrders($prestashopCustomerId);
		if ( empty($orders)) {
			return false;
		}
		$lastOrderId = $orders[0]['id_order'];

		// Recherche des catégories liées à cette commande
		$categoriesIds = $this->_getAllCategoriesFromOrderIncludingAncestors($lastOrderId);

		// Transposition des catégories en segments mappés
		$segmentsIds = $this->_module->getMatchingService()->getSegmentsFromCategories($categoriesIds);

		// Ajout du contact aux segments
        $successCounter = 0;
		if ( !empty($segmentsIds)) {
			foreach($segmentsIds as $segmentId) {

				$response = $this->_module->getMatchingService()->addContactToSegment($mauticContactId, $segmentId);
				if ($response) {
					$successCounter++;
				}
			}
        }

        if ($successCounter < count($segmentsIds)) {
            $this->_module->log('Please check segments mapping, some of them do not exist anymore', true);
        }

        // Réponse négative si tous les segments échouent
		return ($successCounter > 0);
	}


	// Si le client ne souhaite ni newsletter, ni offres partenaires, il est placé en "DO NOT CONTACT" dans Mautic
	// Sinon il est ressorti du "DO NOT CONTACT"
	public function syncCustomerOptinFlags($customer, $mauticContactId)
	{
		$doNotContact = ($customer->newsletter == 0 && $customer->optin == 0);
		return $this->_module->getApiManagerService()->setDoNotContactFlag($mauticContactId, $doNotContact);
	}


	// Retourne les ID de toutes les catégories de tous les articles d'une commande
	// En incluant les ancêtres de chaque catégorie
	protected function _getAllCategoriesFromOrderIncludingAncestors($orderId)
	{
		// Articles contenus dans cette commande
		$products = \OrderDetail::getList($orderId);

		$categoriesIds = array();
		foreach($products as $product) {

			// Parcours des catégories de cet article
			$categories = \Product::getProductCategories($product['product_id']);
			foreach($categories as $categoryId) {
				$categoriesIds[$categoryId] = true;

				// Parcours des catégories parentes jusqu'à la racine
				$tmpCat = new \Category($categoryId);
                $ancestors = $tmpCat->getParentsCategories();

				foreach($ancestors as $ancestor) {
					$categoriesIds[$ancestor['id_category']] = true;
				}
			}
		}

		// Dédoublonnage
		return array_keys($categoriesIds);
	}

	// Conversion d'un tableau associatif de champs Prestashop vers les équivalents Mautic
	protected function _applyFieldsMapping($fields)
	{
		// Table de conversion Prestashop => Mautic
		$mapping = array(
			'email' => 'email',
			'firstname' => 'firstname',
			'lastname' => 'lastname',
			'company' => 'company',
			'address1' => 'address1',
			'address2' => 'address2',
			'postcode' => 'zipcode',
			'city' => 'city',
			'country' => 'country',
			'phone' => 'phone',
			'phone_mobile' => 'mobile'
		);

		// Application du mapping
		// Les champs n'ayant pas de conversion sont ignorés
		$mauticFields = array();
		foreach($fields as $prestashopKey => $value) {

			if (array_key_exists($prestashopKey, $mapping)) {

				$mauticKey = $mapping[ $prestashopKey ];
				$mauticFields[ $mauticKey ] = $value;
			}
		}

		return $mauticFields;
	}

    /**
     * @return Mautic
     */
    public function getModule()
    {
        return $this->_module;
    }

    /**
     * @return \Mautic\Api\Contacts
     * @throws Exception
     */
    public function _getContactApi()
    {
        if ($this->_contactApi) {
            return $this->_contactApi;
        }
        // Tentative de connexion à l'API 'segments'
        if ( !$this->_module->getApiManagerService()->tokenExists()) {
            throw new Exception('Token doesn\'t exist or is invalid. Check the logs');
        }

        $this->_contactApi = $this->_module->getApiManagerService()->getApiEndpoint('contacts');
        return $this->_contactApi;
    }

    /**
     * @return Fields
     */
    public function getFields()
    {
        return $this->_fields;
    }

}
