<?php
/*
 * Fields from Prestashop and Mautic
 *
 */

class Fields
{
    /**
     * @var MauticSync
     */
    private $mauticSync;

    /**
     * @var Mautic
     */
    private $_module;

    /** @var  array */
    private $mauticContactFields;

    /**
     * Fields constructor.
     *
     * @param MauticSync|WmkSync $mauticSync
     */
    public function __construct($mauticSync)
    {

        $this->mauticSync = $mauticSync;
        $this->_module = $this->mauticSync->getModule();
    }

    /**
     * Return configurated mapped
     *
     * @return array
     */
    public function getFieldsToMapping()
    {
        $fields = $this->getFieldsFromPrestashopWithPrefix();
        $ret = array();
        foreach (array_keys($fields) as $field) {
            $fieldWithoutPrefix = str_replace($this->_module->name, '', $field);
            if ($value = \Configuration::get($field, null, null, null, $this->mauticSync->getFields()->getDefaultField($fieldWithoutPrefix))) {
                $ret[$fieldWithoutPrefix] = $value;
            }
        }
        return $ret;
    }


    /**
     * Add module name prefix to prevent duplicate configuration name
     *
     * @return array
     */
    public function getFieldsFromPrestashopWithPrefix()
    {
        $ret = array();
        foreach ($this->getFieldsFromPrestashop() as $field) {
            $ret[$this->_module->name.$field] = $field;
        }
        return $ret;
    }

    /**
     * Get all prestashop field available to mapping
     * Merge AddressFormat::getValidateFields array and manual list
     *
     * @return array
     */
    public function getFieldsFromPrestashop()
    {
        $prestashopFieldsMapping = AddressFormat::getValidateFields('Address');
        $prestashopFieldsMapping = array_merge($prestashopFieldsMapping, array('country', 'id', 'email','id_gender', 'birthday', 'newsletter', 'optin', 'website', 'first_purchase_date', 'last_purchase_date', 'last_purchase_amount', 'total_purchase_amount', 'last_year_purchase_amount', 'total_purchases', 'tags'));
        $this->getAdditionalSyncFields($prestashopFieldsMapping);
        return $prestashopFieldsMapping;
    }

    /**
     * @return array
     */
    public function getMauticContactFields()
    {
        // just load one time
        if (!empty($this->mauticContactFields)) {
            return $this->mauticContactFields;
        }

        try {
            $this->mauticContactFields = $this->mauticSync->_getContactApi()->getFieldList();
            // just object = lead
            foreach ($this->mauticContactFields as $key=>$mauticContactField) {
                if (isset($mauticContactField['object']) && $mauticContactField['object'] != 'lead') {
                    unset($this->mauticContactFields[$key]);
                }
            }
            // add first empty line to skip
            array_unshift($this->mauticContactFields, array('alias' => '', 'label' => $this->_module->l('-skip-')));
            return $this->mauticContactFields;

        } catch (Exception $exception) {

        }
        return [];
    }

    /**
     * Get default field by alias
     *
     * @param $field
     *
     * @return string
     */
    public function getDefaultField($field)
    {
        $array  = array_combine($this->defaultEnabledFields(), $this->defaultEnabledFields());
        if (isset($array[$field])) {
            return $array[$field];
        }

        return '';
    }

    /**
     * Fields synchronized before plugin version 1.2.3
     *
     * @return array
     */
    private function defaultEnabledFields()
    {
        return array('email', 'firstname', 'lastname', 'company', 'address1', 'address2', 'postcode', 'city', 'country', 'phone', 'phone_mobile');
    }


    /**
     * If we need add aditional default fields
     *
     * @param $fields
     */
    private function getAdditionalSyncFields(&$fields)
    {
        $module = Module::getInstanceByName('jashnewsletter');
        if ($module && $module->active) {
            array_push($fields, 'jashnewsletter');
        }

        $module = Module::getInstanceByName('loyalty');
        if ($module && $module->active) {
            array_push($fields, 'loyalty');
        }
    }
}
