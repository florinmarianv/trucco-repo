<?php

/*
 * Synchronisation des clients Prestashop avec Mautic
 *
 */

class Mapper
{
    /**
     * @var array
     */
    private $orders;

    /** @var array */
    private $firstOrder;

    /** @var array */
    private $lastOrder;

    /** @var Context */
    private $context;

    /**
     * @var Customer
     */
    private $customer;

    /** @var  array */
    private $fieldsToMapping;

    /**
     * DataOrders constructor.
     *
     * @param array $fieldsToMapping
     */
    public function __construct($fieldsToMapping)
    {
        $this->context         = Context::getContext();
        $this->customer        = $this->context->customer;
        $this->fieldsToMapping = $fieldsToMapping;
        $this->orders          = $this->getCustomerOrders();
        $this->lastOrder       = reset($this->orders);
        $this->firstOrder      = end($this->orders);
    }


    /**
     * @param object|array $data
     */
    public function getFieldsValuesToMapping()
    {
        $customer     = $this->customer;
        $default_lang = (int) \Configuration::get('PS_LANG_DEFAULT');
        $addresses    = $customer->getAddresses($default_lang);
        $datas = $this->objectToArray($customer);

        if (!empty($addresses)) {
            $address = $addresses[0];
            $datas   = array_merge($datas, $address);
        }
        // convert object to array
        $mapped = [];
        foreach ($this->fieldsToMapping as $prestashopField => $mauticField) {
            switch ($prestashopField) {
                case 'first_purchase_date':
                    $mapped[$mauticField] = $this->getFromFirstOrder('date_add');
                    break;
                case 'last_purchase_date':
                    $mapped[$mauticField] = $this->getFromLastOrder('date_add');
                    break;
                case 'last_purchase_amount':
                    $mapped[$mauticField] = $this->getFromLastOrder('total_paid');
                    break;
                case 'last_year_purchase_amount':
                    $mapped[$mauticField] = $this->getSumFromOrders('total_paid', $this->getOrdersFromLastYear());
                    break;
                case 'last_year_purchase_total':
                    $mapped[$mauticField] = $this->getCountFromOrders($this->getOrdersFromLastYear());
                    break;
                case 'total_purchase_amount':
                    $mapped[$mauticField] = $this->getSumFromOrders('total_paid');
                    break;
                case 'total_purchases':
                    $mapped[$mauticField] = $this->getCountFromOrders();
                    break;
                case 'tags':
                    $mapped[$prestashopField] = $this->getTags();
                    break;
                case 'jashnewsletter':
                    $mapped[$mauticField] = $this->getJashNewsletterUserSubscriptionsFromEmail($customer->email);
                    break;
                case 'loyalty':
                    $mapped[$mauticField] = $this->getLoyalityPoints($customer);
                    break;
                default:
                    if (isset($datas[$prestashopField])) {
                        $mapped[$mauticField] = $datas[$prestashopField];
                    }
                    break;
            }
        }
        return $mapped;
    }

    /**
     * @param $customer
     *
     * @return false|string|null
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    private function getLoyalityPoints($customer)
    {
        // Actual points from approved points
        $sql           = 'SELECT SUM(l.points) FROM '._DB_PREFIX_.'loyalty l WHERE
l.id_customer = '.(int) $customer->id.' AND l.id_loyalty_state IN (2, 4)  ';
        $currentPoints = Db::getInstance()->getValue($sql);

        // Loyality points from last order
        $order              = new Order($this->getFromLastOrder('id_order'));
        $pointsFromNewOrder = LoyaltyModule::getOrderNbPoints($order);

        return ($currentPoints + $pointsFromNewOrder);
    }


    /**
     * @param $email
     *
     * @return array
     */
    private function getJashNewsletterUserSubscriptionsFromEmail($email)
    {
        $sql = 'SELECT `id_mailing_list` ' .
            'FROM  `' . _DB_PREFIX_ . 'mailing_list_subscriber` ' .
            'WHERE `email` = \'' . pSQL($email) . '\' ' .
            'AND `id_shop` = ' . Shop::getContextShopID() . ' ' .
            'AND `confirmation_token` IS NULL';

        $result = array();
        foreach (Db::getInstance()->executeS($sql) as $row) {
            $result[] = $row['id_mailing_list'];
        }

        return $result;
    }

    /**
     * @return array
     */
    private function getTags()
    {
        $tags = [];
        if (empty($this->fieldsToMapping['tags'])) {
            return $tags;
        }
        $orderEntity = new Order($this->getFromLastOrder('id_order'));
        $products = $orderEntity->getProducts(false, false, false, false);
        foreach ($products as $product) {
            switch ($this->fieldsToMapping['tags']) {
                case "id":
                    $tags[] = $product['id_product'];
                    break;
                case "name":
                    $tags[] = $product['product_name'];
                    break;
                case "id_name":
                    $tags[] = $product['id_product'].' | '.$product['product_name'];
                    break;
                case "category_name":
                case "category_name_id":
                    if ($category = new Category($product['id_category_default'])) {
                        if ($this->fieldsToMapping['tags'] == 'category_name_id') {
                            $tags[] = $category->getName().' | '.$product['product_name'].' | '.$product['product_id'];
                        }else{
                            $tags[] = $category->getName().' | '.$product['product_name'];
                        }
                    }
                    break;
            }
        }
        return $tags;
    }


    /**
     * @param $object
     *
     * @return array
     */
    private function objectToArray($object)
    {
        if (is_object($object)) {
            return json_decode(json_encode(($object)), true);
        } elseif (is_array($object)) {
            return $object;
        }

        return [];
    }

    /**
     * @return array
     */
    private function getOrdersFromLastYear()
    {
        $orders = $this->orders;
        foreach ($orders as $key => $order) {
            if ($order['date_add'] < date('Y-m-d H:i:s', strtotime('-1 year'))) {
                unset($orders[$key]);
            }
        }

        return $orders;
    }

    /**
     * @param array $orders
     *
     * @return int
     */
    private function getCountFromOrders($orders = [])
    {
        if (empty($orders)) {
            $orders = $this->orders;
        }

        return count($orders);
    }

    /**
     * @param string $column
     *
     * @param array  $orders
     *
     * @return number
     */
    private function getSumFromOrders($column, $orders = [])
    {
        if (empty($orders)) {
            $orders = $this->orders;
        }

        return array_sum(array_column($orders, $column));
    }

    /**
     * @param string $column
     */
    private function getFromFirstOrder($column)
    {
        if (isset($this->firstOrder[$column])) {
            return $this->firstOrder[$column];
        }
    }

    /**
     * @param string $column
     */
    private function getFromLastOrder($column)
    {
        if (isset($this->lastOrder[$column])) {
            return $this->lastOrder[$column];
        }
    }

    /**
     * @return array
     */
    private function getCustomerOrders()
    {
        $allOrders = $orders = \Order::getCustomerOrders($this->customer->id);
        $lastOrder = reset($allOrders);
        foreach ($orders as $key=>$order) {
            if (empty($order['valid'])) {
                unset($orders[$key]);
            }
        }

        if (!is_array($orders)) {
            $orders = [];
        }
        // last order as first
        array_unshift($orders, $lastOrder);
        return $orders;
    }

}
