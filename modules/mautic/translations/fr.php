<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{mautic}prestashop>mautic_8feb3009a995c5f981c53490e8c48b72'] = 'Mautic sync';
$_MODULE['<{mautic}prestashop>mautic_2c8fdb5742ca8d4640aae61a87330cef'] = 'Synchronisation des contacts Prestashop vers la solution de marketing automation Mautic.';
$_MODULE['<{mautic}prestashop>mautic_4a324a0257ab146b944ccc77d116c5dc'] = 'Voulez-vous désinstaller ce module ?';
$_MODULE['<{mautic}prestashop>mautic_5d39afe8ae76dd572bc781b38b29bdb8'] = 'Veuillez renseigner l\'URL de votre instance Mautic.';
$_MODULE['<{mautic}prestashop>mautic_b4074ac8d44c9b4b5b7905963ba91cba'] = 'Veuillez renseigner les clés oAuth fournies par Mautic.';
$_MODULE['<{mautic}prestashop>mautic_94110bd6e75f29db0168c885da6d97f0'] = 'La connexion entre Prestashop et Mautic a été établie avec succès. ';
$_MODULE['<{mautic}prestashop>mautic_a1a8bb8ccec3ebec5359eddf8c6b3386'] = 'Vous pouvez tester l\'accès et poursuivre la configuration.';
$_MODULE['<{mautic}prestashop>mautic_c61e32a4491dacf15fc01ea90a4539a6'] = 'Le code d\'autorisation a expiré. Veuillez ré-essayer.';
$_MODULE['<{mautic}prestashop>mautic_b77b146352b182d49e00c4189b0d2f94'] = 'Impossible de communiquer avec Mautic.';
$_MODULE['<{mautic}prestashop>mautic_2e7ea564e843c56e90b87c76a3546739'] = 'Le segment \"%s\" a été créé avec succès.';
$_MODULE['<{mautic}prestashop>mautic_95bdcc15d7583852bc46146044cce383'] = '2. Connexion avec Mautic';
$_MODULE['<{mautic}prestashop>mautic_3efa8454b94566b85e33dcacf3d15517'] = 'URL racine de Mautic';
$_MODULE['<{mautic}prestashop>mautic_ed6445336472aef39084720adcf903b9'] = 'Clé publique';
$_MODULE['<{mautic}prestashop>mautic_952bf87c967660b7bbd4e1eb08cefc92'] = 'Clé secrète';
$_MODULE['<{mautic}prestashop>mautic_505ed83e12e2b73412b81d63207ed314'] = 'Enregistrer et établir la connexion';
$_MODULE['<{mautic}prestashop>mautic_39838db537c95bc8df810c5601601d5b'] = 'Tester la connexion';
$_MODULE['<{mautic}prestashop>mautic_33f10afde0f34428c3ba28282d0e65c5'] = 'Connexion établie avec succès';
$_MODULE['<{mautic}prestashop>mautic_1be415f041c0d8f2e10093a7b4236694'] = 'Echec de la connexion';
$_MODULE['<{mautic}prestashop>mautic_0170047f48620c53f542f317a60fe99c'] = 'Nouveau segment';
$_MODULE['<{mautic}prestashop>mautic_c0b854c7bcb879aa9ba8843f34812244'] = '3. Association des catégories d\'articles aux segments de Mautic';
$_MODULE['<{mautic}prestashop>mautic_4830ddc617900e8c9dba08d5da78609d'] = 'Catégorie d\'articles';
$_MODULE['<{mautic}prestashop>mautic_31cdf3289d6eed02381bbc65c260bdd9'] = 'Segment Mautic associé';
$_MODULE['<{mautic}prestashop>mautic_732d24e3445f2ef5ccb55ff958ce1bfb'] = 'Nom du nouveau segment';
$_MODULE['<{mautic}prestashop>mautic_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{mautic}prestashop>mautic_ec211f7c20af43e742bf2570c3cb84f9'] = 'Ajouter';
$_MODULE['<{mautic}prestashop>requirements_fda6e21117ff54d0dd2d488bdf1a9e4d'] = '1. Pré-requis';
$_MODULE['<{mautic}prestashop>requirements_241a61f5d4cace29761313a31ee6abe7'] = 'Dans Mautic, depuis Configuration > API Settings : activer API';
$_MODULE['<{mautic}prestashop>requirements_fc9773f9b5584f55e952c7a10af5d27f'] = 'Puis depuis API Credentials ajouter une nouvelle entrée de type OAuth2';
$_MODULE['<{mautic}prestashop>requirements_612f1b892ee4e9c81dee055c62603867'] = 'avec comme URL de redirection';
