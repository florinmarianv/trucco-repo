<?php
/**
* 2014-2017 Crezzur
*
* LICENSE
* You are not allowed to share this code and or files. All rights reserved Crezzur
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade our products to newer
* versions in the future. If you wish to customize our products for your
* needs please contact us for more information.
*
*  @author    Crezzur <info@crezzur.com>
*  @copyright 2014-2017 Jaimy Aerts
*  @license   All rights reserved
*  International Registered Trademark & Property of Crezzur
*/
if (!defined('_PS_VERSION_')) {
    exit;
}

class DataSwitchPro extends Module
{
    private $html = '';
    public function __construct()
    {
        $this->name       = 'dataswitchpro';
        $this->tab        = 'front_office_features';
        $this->version    = '1.0.4';
        $this->author     = 'Jaimy A.';
        $this->module_key = '';
        $this->bootstrap  = true;
        parent::__construct();
        $this->displayName = $this->l('DataSwitchPro');
        $this->description = $this->l('Automatic on/off switch for categories, sub-categories and products.');
        $this->ps_versions_compliancy = array(
            'min' => '1.6',
            'max' => _PS_VERSION_
        );
    }

    public function install($delete_params = true)
    {
        if (!parent::install() || !$this->registerHook('header') || !$this->registerHook('productfooter') ) {
            return false;
        }
        if ($delete_params) {
            if (!$this->installDb()) {
                return false;
            }
        }
        return true;
    }

    public function installDb()
    {
        return (Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'dataswitch_sync
        (`switch_group`   varchar(250), last_switch TIMESTAMP, `update_time` int(30), `active` int(30))
        default CHARSET=utf8') && Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'dataswitch_sync
        (switch_group, last_switch, update_time, active)
        VALUES ("categories", "2000-01-01 00:00:00", 3, 1), ("products", "2000-01-01 00:00:00", 3, 1)'));
    }

    public function uninstall($delete_params = true)
    {
        if (!parent::uninstall()) {
            return false;
        }
        if ($delete_params) {
            if (!$this->uninstallDB()) {
                return false;
            }
        }
        return true;
    }

    private function uninstallDb()
    {
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'dataswitch_sync`');
        return true;
    }
    
    public function reset()
    {
        if (!$this->uninstall(false)) {
            return false;
        }
        if (!$this->install(false)) {
            return false;
        }
        return true;
    }

    public function hookHeader()
    {
        $sql = Db::getInstance()->ExecuteS('SELECT switch_group, last_switch, update_time, active
        FROM '._DB_PREFIX_.'dataswitch_sync');
        foreach ($sql as $key) {
            if ($key['active'] == 1 && strtotime($key['last_switch']) <= strtotime('-'.$key['update_time'].' minutes')
            && $key['switch_group'] == 'products') {
                Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'dataswitch_sync set last_switch=CURRENT_TIMESTAMP
                WHERE switch_group = "products"');
                Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'product_shop as ps JOIN '._DB_PREFIX_.'stock_available
                as sa ON sa.quantity<1 AND out_of_stock!=1 AND id_product_attribute=0 SET active=0 WHERE ps.id_product=sa.id_product');
                Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'product_shop as ps JOIN '._DB_PREFIX_.'stock_available
                as sa ON sa.quantity>0 AND id_product_attribute=0 SET active=1 WHERE ps.id_product=sa.id_product');
                Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'product_shop as ps JOIN '._DB_PREFIX_.'stock_available
                as sa ON sa.out_of_stock=1 AND id_product_attribute=0 SET active=1 WHERE ps.id_product=sa.id_product');
            }

            if ($key['active'] == 1 && strtotime($key['last_switch']) <= strtotime('-'.$key['update_time'].' minutes')
            && $key['switch_group'] == 'categories') {
                Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'category as c SET c.active=0 WHERE c.id_category
                IN (SELECT tmp.id_category FROM (SELECT id_category FROM '._DB_PREFIX_.'category) as tmp
                WHERE tmp.id_category NOT IN (SELECT cp.id_category FROM '._DB_PREFIX_.'category_product as cp
                GROUP BY cp.id_category))');
                Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'dataswitch_sync set last_switch=CURRENT_TIMESTAMP
                WHERE switch_group = "categories"');
            }
        }
    }
    
    public function hookProductFooter()
    {
        return $this->display(__FILE__, 'header.tpl');
    }
    public function getContent()
    {
        $this->html = '';
        if (Tools::getValue('saveSettings')) {
            $this->html .= '<p style="color:red">'.$this->postProcess().'</p>';
            $this->html .= $this->displayConfirmation($this->l('Settings updated'));
        }
        $this->html .= $this->displaySettings($_SERVER['REQUEST_URI']);
        return $this->html;
    }

    private function displaySettings()
    {
        $products_no = '';
        $categories_no = '';
        $update_time = Db::getInstance()->getValue('SELECT update_time FROM '._DB_PREFIX_.'dataswitch_sync');
        $categories_active = Db::getInstance()->getValue('
        SELECT active FROM '._DB_PREFIX_.'dataswitch_sync WHERE switch_group = "categories"');
        $products_active = Db::getInstance()->getValue('
        SELECT active FROM '._DB_PREFIX_.'dataswitch_sync WHERE switch_group = "products"');

        if ($categories_active == '1') {
            $categories_yes = 'checked="checked"';
        } else {
            $categories_no = 'checked="checked"';
        }
        if ($products_active == '1') {
            $products_yes = 'checked="checked"';
        } else {
            $products_no = 'checked="checked"';
        }


        $this->context->smarty->assign('p_yes', $products_yes);
        $this->context->smarty->assign('p_no', $products_no);
        $this->context->smarty->assign('c_yes', $categories_yes);
        $this->context->smarty->assign('c_no', $categories_no);
        $this->context->smarty->assign('update_time', $update_time);
        $this->context->smarty->assign('url', $_SERVER['REQUEST_URI']);
        $txt = $this->display(__FILE__, 'views/templates/admin/displaysettings.tpl');
        return $txt;
    }

    public function postProcess()
    {
        if (Tools::getValue('update_time') != null) {
            Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'dataswitch_sync SET update_time =
            '.Tools::getValue('update_time'));
        }
        if (Tools::getValue('categories_data') != null) {
            Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'dataswitch_sync SET active =
            '.Tools::getValue('categories_data').' WHERE switch_group = "categories"');
        }
        if (Tools::getValue('products_data') != null) {
            Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'dataswitch_sync SET active =
            '.Tools::getValue('products_data').' WHERE switch_group = "products"');
        }
    }
}
