{*
* 2014-2017 Crezzur
*
* LICENSE
* You are not allowed to share this code and or files. All rights reserved Crezzur
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade our products to newer
* versions in the future. If you wish to customize our products for your
* needs please contact us for more information.
*
*  @author    Crezzur <info@crezzur.com>
*  @copyright 2014-2017 Jaimy Aerts
*  @license   All rights reserved
*  International Registered Trademark & Property of Crezzur
**}

<div class="row">
<div class="col-lg-12">
<form id="product_form" name ="product_form" action="{$url|escape:'htmlall':'UTF-8'}"
method="post" class="defaultForm form-horizontal AdminTags" enctype="multipart/form-data">

<div class="panel" id="fieldset_0">
<div class="panel-heading"><i class="icon-cogs"></i>&nbsp;DataSwitchPro - {l s='Settings' mod='dataswitchpro'}</div>

<div class="form-group">
<div id="conf_id_PS_TOKEN_ENABLE">
<label class="control-label col-lg-3">{l s='Categories' mod='dataswitchpro'}</label>
<div class="col-lg-9">
<span class="switch prestashop-switch fixed-width-lg">

<input type="radio" name="categories_data" id="categories_yes" value="1" {$c_yes|escape:'htmlall':'UTF-8'}/>
<label for="categories_yes" class="radioCheck">{l s='Yes' mod='dataswitchpro'}</label>

<input type="radio" name="categories_data" id="categories_no" value="0" {$c_no|escape:'htmlall':'UTF-8'}/>
<label for="categories_no" class="radioCheck">{l s='No' mod='dataswitchpro'}</label>

<a class="slide-button btn"></a>
</span>
</div>
<div class="col-lg-9 col-lg-offset-3">
<div class="help-block">{l s='Enable or disable automatic on/off switch for categories' mod='dataswitchpro'}.</div>
</div>
</div>
</div>

<div class="form-group">
<div id="conf_id_PS_TOKEN_ENABLE">
<label class="control-label col-lg-3">{l s='Products' mod='dataswitchpro'}</label>
<div class="col-lg-9">
<span class="switch prestashop-switch fixed-width-lg">

<input type="radio" name="products_data" id="products_yes" value="1" {$p_yes|escape:'htmlall':'UTF-8'}/>
<label for="products_yes" class="radioCheck">{l s='Yes' mod='dataswitchpro'}</label>

<input type="radio" name="products_data" id="products_no" value="0" {$p_no|escape:'htmlall':'UTF-8'}/>
<label for="products_no" class="radioCheck">{l s='No' mod='dataswitchpro'}</label>

<a class="slide-button btn"></a>
</span>
</div>
<div class="col-lg-9 col-lg-offset-3">
<div class="help-block">{l s='Enable or disable automatic on/off switch for products' mod='dataswitchpro'}.</div>
</div>
</div>
</div>

<div class="form-group"><label class="control-label col-lg-3">Switch Timer</label><div class="col-lg-9 ">
<input type="text" name="update_time" id="update_time" value="{$update_time|escape:'htmlall':'UTF-8'}"
class="fixed-width-xs" required="required"/>
<p class="help-block">{l s='Minutes between each synchronization' mod='dataswitchpro'}. ({l s='example: 120 = 2 hours' mod='dataswitchpro'})</p></div></div>

<!-- /.form-wrapper -->
<div class="panel-footer">
<button type="submit" value="{l s='Save' mod='dataswitchpro'}" id="tag_form_submit_btn" 
name ="saveSettings" class="btn btn-default pull-right">
<i class="process-icon-save"></i> {l s='Save' mod='dataswitchpro'}
</button>
</div>

</div>
</div>
</form>
</div>
</div>