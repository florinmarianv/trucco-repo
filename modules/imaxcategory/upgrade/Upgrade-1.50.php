<?php

function upgrade_module_1_50($module) {
    $sqls = array();
    $sqls[] = '
            ALTER TABLE `' . _DB_PREFIX_ . 'imaxCategoryMapeo`
                ADD COLUMN `tipoMapeo` TINYINT NULL DEFAULT "0" AFTER `catDestino`,
                ADD COLUMN `cambiarCatDefecto` TINYINT NULL DEFAULT "0" AFTER `tipoMapeo`,
                ADD COLUMN `desasociarCat` TINYINT NULL DEFAULT "0" AFTER `cambiarCatDefecto`,
                ADD COLUMN `position` INT NULL AFTER `cambiarCatDefecto`';
    
    $imaxCambiarCatDefecto = Configuration::getGlobalValue($module->sufijo.'imaxCambiarCatDefecto');
    $imaxEliminarAsociaciones = Configuration::getGlobalValue($module->sufijo.'imaxEliminarAsociaciones');
    $imaxAccionCopiado = Configuration::getGlobalValue($module->sufijo.'imaxAccionCopiado');
    
    $sqls[] = 'UPDATE `' . _DB_PREFIX_ . 'imaxCategoryMapeo` SET tipoMapeo= '.(int)$imaxAccionCopiado.', cambiarCatDefecto='.(int)$imaxAccionCopiado.', desasociarCat='.$imaxEliminarAsociaciones;    
    
    foreach ($sqls AS $sql) {
        if (!Db::getInstance()->execute($sql)) {
            return false;
        }
    }

    if (!@copy(dirname(__FILE__) . '/../imaxcategoryCron.php', $directorioAdmin . '/imaxcategoryCron.php')) {
        return false;
    }
    if (is_file(dirname(__FILE__) . '/../imaxcategoryCron.php.imax') && !@copy(dirname(__FILE__) . '/../imaxcategoryCron.php.imax', $directorioAdmin . '/imaxcategoryCron.php.imax')) {
        return false;
    }

    return true;
}
