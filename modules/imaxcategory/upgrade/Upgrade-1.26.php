<?php
function upgrade_module_1_26($module)
{	
    $directorioAdmin = getcwd();
	if (!@copy(dirname(__FILE__).'/../imaxcategoryExcel.php', $directorioAdmin.'/imaxcategoryExcel.php')) 
	{
            return false;
    }
	if (is_file(dirname(__FILE__).'/../imaxcategoryExcel.php.imax') && !@copy(dirname(__FILE__).'/../imaxcategoryExcel.php.imax', $directorioAdmin.'/imaxcategoryExcel.php.imax')) 
	{
            return false;
    }
    
    if (!@copy(dirname(__FILE__).'/../imaxcategoryCron.php', $directorioAdmin.'/imaxcategoryCron.php')) 
	{
            return false;
    }
    if (is_file(dirname(__FILE__).'/../imaxcategoryCron.php.imax') && !@copy(dirname(__FILE__).'/../imaxcategoryCron.php.imax', $directorioAdmin.'/imaxcategoryCron.php.imax')) 
	{
            return false;
    }
    
    if (!@copy(dirname(__FILE__).'/../imaxcategoryCronProductosExcel.php', $directorioAdmin.'/imaxcategoryCronProductosExcel.php')) {
            return false;
	}			
    if (is_file(dirname(__FILE__).'/../imaxcategoryCronProductosExcel.php.imax') && !@copy(dirname(__FILE__).'/../imaxcategoryCronProductosExcel.php.imax', $directorioAdmin.'/imaxcategoryCronProductosExcel.php.imax')) {
            return false;
	}	
    
    return true;
}