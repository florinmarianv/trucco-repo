<?php
function upgrade_module_1_20($module)
{	
    $sql = array();
    $sql[] = 'ALTER TABLE `'._DB_PREFIX_.'imaxCategoryimport` ALTER `catsAdicionales` DROP DEFAULT;';
    $sql[] = 'ALTER TABLE `'._DB_PREFIX_.'imaxCategoryimport` CHANGE COLUMN `catsAdicionales` `catsAdicionales` TEXT NOT NULL AFTER `catDefecto`;';
    foreach($sql AS $s) {
       if(!DB::getInstance()->execute($s)) {
           return false;
       }
    }
    return true;
}