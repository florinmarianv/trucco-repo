<?php

function upgrade_module_1_30($module) {
    $hooks = array();
    $hooks[] = 'actionObjectProductUpdateBefore';
    foreach($hooks AS $hook) {
        if(!$module->registerHook($hook)) {
            return false;
        }
    }
    return true;
}
