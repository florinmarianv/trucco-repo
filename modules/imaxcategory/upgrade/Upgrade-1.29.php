<?php

function upgrade_module_1_29($module) {
    $truncate = false;
    $sql = array();

    $sql[] = 'ALTER TABLE `ps_imaxCategoryMapeoProductos` DROP INDEX `idProducto_catDestino_idMapeo`;';
    $sql[] = 'ALTER TABLE `' . _DB_PREFIX_ . 'imaxCategoryMapeoProductos` ALTER `idMapeo` DROP DEFAULT;';
    $sql[] = 'ALTER TABLE `' . _DB_PREFIX_ . 'imaxCategoryMapeoProductos` '
            . ' CHANGE COLUMN `idMapeo` `idMapeo` INT(10) NOT NULL AFTER `catDestino`, '
            . '	ADD UNIQUE INDEX `idProducto_idMapeo` (`idProducto`, `idMapeo`); ';
    $sqlTruncate = 'TRUNCATE TABLE `' . _DB_PREFIX_ . 'imaxCategoryMapeoProductos`';
    
    try {
        foreach ($sql AS $s) {
            if (!DB::getInstance()->execute($s)) {
                $truncate = true;
            }
        }
    } catch (Exception $exc) {
        if (DB::getInstance()->execute($sqlTruncate)) {
            foreach ($sql AS $s) {
                if (!DB::getInstance()->execute($s)) {
                    return false;
                }
            }
        } else {
            return false;
        }
    }
    
    $directorioAdmin = getcwd();
    if (!@copy(dirname(__FILE__) . '/../imaxcategoryExcel.php', $directorioAdmin . '/imaxcategoryExcel.php')) {
        return false;
    }
    if (is_file(dirname(__FILE__) . '/../imaxcategoryExcel.php.imax') && !@copy(dirname(__FILE__) . '/../imaxcategoryExcel.php.imax', $directorioAdmin . '/imaxcategoryExcel.php.imax')) {
        return false;
    }

    if (!@copy(dirname(__FILE__) . '/../imaxcategoryCron.php', $directorioAdmin . '/imaxcategoryCron.php')) {
        return false;
    }
    if (is_file(dirname(__FILE__) . '/../imaxcategoryCron.php.imax') && !@copy(dirname(__FILE__) . '/../imaxcategoryCron.php.imax', $directorioAdmin . '/imaxcategoryCron.php.imax')) {
        return false;
    }

    if (!@copy(dirname(__FILE__) . '/../imaxcategoryCronProductosExcel.php', $directorioAdmin . '/imaxcategoryCronProductosExcel.php')) {
        return false;
    }
    if (is_file(dirname(__FILE__) . '/../imaxcategoryCronProductosExcel.php.imax') && !@copy(dirname(__FILE__) . '/../imaxcategoryCronProductosExcel.php.imax', $directorioAdmin . '/imaxcategoryCronProductosExcel.php.imax')) {
        return false;
    }

    return true;
}
