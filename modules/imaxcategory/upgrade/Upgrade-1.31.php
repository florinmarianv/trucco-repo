<?php

function upgrade_module_1_31($module) {
    $hooks = array();
    $hooks[] = 'actionObjectProductUpdateBefore';
    foreach($hooks AS $hook) {
        if(!$module->registerHook($hook)) {
            return false;
        }
    }
    
        $directorioAdmin = getcwd();
        if (!@copy(dirname(__FILE__).'/../imaxcategoryExcel.php', $directorioAdmin.'/imaxcategoryExcel.php')) 
        {
                $module->_errors[] = $module->l('Se ha producido un error al copiar el archivo excel a la carpeta de administracion');
                return false;
        }
        if (is_file(dirname(__FILE__).'/../imaxcategoryExcel.php.imax') && !@copy(dirname(__FILE__).'/../imaxcategoryExcel.php.imax', $directorioAdmin.'/imaxcategoryExcel.php.imax')) 
        {
                $module->_errors[] = $module->l('Se ha producido un error al copiar el archivo Excel ofuscado a la carpeta de administracion');
                return false;
        }

        if (!@copy(dirname(__FILE__).'/../imaxcategoryCron.php', $directorioAdmin.'/imaxcategoryCron.php')) 
        {
                $module->_errors[] = $module->l('Se ha producido un error al copiar el archivo de cron a la carpeta de administracion');
                return false;
        }
        if (is_file(dirname(__FILE__).'/../imaxcategoryCron.php.imax') && !@copy(dirname(__FILE__).'/../imaxcategoryCron.php.imax', $directorioAdmin.'/imaxcategoryCron.php.imax')) 
        {
                $module->_errors[] = $module->l('Se ha producido un error al copiar el archivo de cron ofuscado a la carpeta de administracion');
                return false;
        }

        if (!@copy(dirname(__FILE__).'/../imaxcategoryCronProductosExcel.php', $directorioAdmin.'/imaxcategoryCronProductosExcel.php')) 
        {
                $module->_errors[] = $module->l('Se ha producido un error al copiar el archivo excel de productos a la carpeta de administracion');
                return false;
        }			
        if (is_file(dirname(__FILE__).'/../imaxcategoryCronProductosExcel.php.imax') && !@copy(dirname(__FILE__).'/../imaxcategoryCronProductosExcel.php.imax', $directorioAdmin.'/imaxcategoryCronProductosExcel.php.imax'))
        {
                $module->_errors[] = $module->l('Se ha producido un error al copiar el archivo excel de productos ofuscado a la carpeta de administracion');
                return false;
        }	

        return true;
    }
