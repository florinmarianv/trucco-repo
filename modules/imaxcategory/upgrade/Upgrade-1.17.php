<?php
function upgrade_module_1_17($module)
{	
	$sql = 'ALTER TABLE `'._DB_PREFIX_.'imaxCategoryMapeoProductos` ADD UNIQUE INDEX `idProducto_catDestino_idMapeo` (`idProducto`, `catDestino`, `idMapeo`);';
	DB::getInstance()->execute($sql);
	return true;
}