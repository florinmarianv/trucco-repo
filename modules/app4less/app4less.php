<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2015 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class App4less extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'app4less';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0.0';
        $this->author = 'WebImpacto Consulting S.L.';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('App4Less');
        $this->description = $this->l('Configure your Shop to download your APP in mobile device.');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('APP4LESS_LIVE_MODE', false);

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('actionAuthentication') &&
            $this->registerHook('actionCustomerAccountAdd') &&
            $this->registerHook('displayFooter') &&
            $this->registerHook('displayHeader') &&
            $this->registerHook('displayTop');
    }

    public function uninstall()
    {
        Configuration::deleteByName('APP4LESS_LIVE_MODE');
        Configuration::deleteByName('APP4LESS_NAME');
        Configuration::deleteByName('APP4LESS_ANDROID');
        Configuration::deleteByName('APP4LESS_IOS');
        Configuration::deleteByName('APP4LESS_WINDOWS_PHONE');
        Configuration::deleteByName('APP4LESS_REQUESTED_WITH');
        Configuration::deleteByName('APP4LESS_TEXT');
        Configuration::deleteByName('APP4LESS_GROUP');
        Configuration::deleteByName('APP4LESS_LOGO');


        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitApp4lessModule')) == true) {
            $this->postProcess();
        }

        $isCorrect = true;
        $error = null;
        if (((bool)Tools::isSubmit('submitApp4lessModuleIOS')) == true) {
            $isCorrect = $this->postProcess2();
            if (!$isCorrect) {
                $this->error = $this->l('An error occurred. Is missing "#" or "." .');
                $error = "<div class='error'>$this->l('An error occurred. Is missing \"#\" or \".\" .')</div>";
            }
        }

        $this->context->smarty->assign('module_dir', $this->_path);
        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm().$error.$this->renderForm2();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    public function renderForm()
    {

        $groups = Group::getGroups((int)Configuration::get('PS_LANG_DEFAULT'));
        $image_sufix = '';

        if (Shop::isFeatureActive()) {
            $id_shop = Shop::getContextShopID();
            $image_sufix = $id_shop ? '-' . $id_shop : '';
        }

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'description' => $this->l('This block displays in the footer a link to the APP Download page only for mobile devices.').'<br/><br/>'.
                    $this->l('You must enter the link of your APP in every mobile device (iOS, Android, Windows Phone)').'<br/>'.
                    $this->l('If you dont have APP leave empty the Link'),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('APP Name'),
                        'name' => 'APP4LESS_NAME',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Android Link'),
                        'name' => 'APP4LESS_ANDROID',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('iOs Link'),
                        'name' => 'APP4LESS_IOS'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Windows Phone Link'),
                        'name' => 'APP4LESS_WINDOWS_PHONE'
                    ),
                    array(
                        'type' => 'text',
                        'desc' => $this->l('Leave Blank if you don\'t know this value'),
                        'label' => $this->l('APP Header Requested With'),
                        'name' => 'APP4LESS_REQUESTED_WITH'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Text Visible Mobile'),
                        'name' => 'APP4LESS_TEXT'
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('APP Customer Group'),
                        'name' => 'APP4LESS_GROUP',
                        'required' => true,
                        'options' => array(
                            'query' => $groups,
                            'id' => 'id_group',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'file',
                        'label' => $this->l('Logo'),
                        'desc' => $this->l('The recommended dimensions are 70 x 70px.'),
                        'name' => 'APP4LESS_LOGO',
                        'thumb' => $this->_path.'/images/logoApp' . $image_sufix . '.png',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'name' => 'submitApp4lessModule'
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table =  $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'APP4LESS_NAME' => Configuration::get('APP4LESS_NAME'),
            'APP4LESS_ANDROID' => Configuration::get('APP4LESS_ANDROID'),
            'APP4LESS_IOS' => Configuration::get('APP4LESS_IOS'),
            'APP4LESS_WINDOWS_PHONE' => Configuration::get('APP4LESS_WINDOWS_PHONE'),
            'APP4LESS_REQUESTED_WITH' => Configuration::get('APP4LESS_HEADER_REQUESTED_WITH'),
            'APP4LESS_TEXT' => Configuration::get('APP4LESS_TEXT'),
            'APP4LESS_GROUP' => Configuration::get('APP4LESS_GROUP'),
            'APP4LESS_LOGO' => Configuration::get('APP4LESS_LOGO'),

        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();
        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }

        $type = Tools::strtolower(Tools::substr(strrchr($_FILES['APP4LESS_LOGO']['name'], '.'), 1));
        $imagesize = $_FILES['APP4LESS_LOGO']['size'];
        if (isset($_FILES['APP4LESS_LOGO']) && $imagesize &&
            in_array($type, array('jpg', 'gif', 'jpeg', 'png'))){

            $temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
            $salt = sha1(microtime());
            $image_sufix = '';

            if (Shop::isFeatureActive()) {
                $id_shop = Shop::getContextShopID();
                $image_sufix = $id_shop ? '-' . $id_shop : '';
            }

            if ($error = ImageManager::validateUpload($_FILES['APP4LESS_LOGO']))
                $errors[] = $error;
            elseif (!$temp_name || !move_uploaded_file($_FILES['APP4LESS_LOGO']['tmp_name'], $temp_name))
                return false;
            elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/images/logoApp' . $image_sufix . '.png', null, null, $type))
                $errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));
            if (isset($temp_name))
                @unlink($temp_name);



        }


    }

    public function renderForm2()
    {
        $groups = Group::getGroups((int)Configuration::get('PS_LANG_DEFAULT'));
        $image_sufix = '';

        if (Shop::isFeatureActive()) {
            $id_shop = Shop::getContextShopID();
            $image_sufix = $id_shop ? '-' . $id_shop : '';
        }

        $fields_form2 = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings IOS Movement Element'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Class or ID of element to control the coordinates. Put it with . or #'),
                        'name' => 'APP4LESS_COORD_IOS',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Class or ID of element to move. Put it with . or #'),
                        'name' => 'APP4LESS_MOVEMENT_IOS',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'name' => 'submitApp4lessModuleIOS'
                )
            ),
        );

        $helper2 = new HelperForm();
        $helper2->show_toolbar = false;
        $helper2->table =  "movement_ios";
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper2->default_form_language = $lang->id;
        $helper2->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper2->identifier = $this->identifier;
        $helper2->submit_action = 'submitModuleIOS';
        $helper2->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper2->token = Tools::getAdminTokenLite('AdminModules');

        $helper2->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues2(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper2->generateForm(array($fields_form2));
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues2()
    {
        return array(
            'APP4LESS_COORD_IOS' => Configuration::get('APP4LESS_COORD_IOS'),
            'APP4LESS_MOVEMENT_IOS' => Configuration::get('APP4LESS_MOVEMENT_IOS'),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess2()
    {
        $form_values = $this->getConfigFormValues2();
        foreach (array_keys($form_values) as $key) {

            if (strpos(Tools::getValue($key), '#') !== false || strpos(Tools::getValue($key), '.') !== false) {
            } else {
                return false;
            }
            Configuration::updateValue($key, Tools::getValue($key));
        }
        return true;
    }
    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }



    public function hookDisplayFooter()
    {

        if(isset($_COOKIE['noAPPpopup']) && (int)$_COOKIE['noAPPpopup']>=2){
            return false;
        }

        require_once $this->local_path.'classes/MobileDetect.php';
        if (!class_exists('Mobile_Detect')) { return 'classic'; }

        $detect = new app4less_classes\Mobile_Detect();
        $link = '';
        $image_sufix = '';

        if ($detect->isAndroidOS()){
            $link = Configuration::get('APP4LESS_ANDROID');
        }
        if ($detect->isiOS()){
            $link = Configuration::get('APP4LESS_IOS');
        }
        if ($detect->isWindowsPhoneOS()){
            $link = Configuration::get('APP4LESS_WINDOWS_PHONE');
        }

        if (Shop::isFeatureActive()) {
            $id_shop = Shop::getContextShopID();
            $image_sufix = $id_shop ? '-' . $id_shop : '';
        }

        if(!$this->isApp4Less()){
            if($link){
                $this->context->smarty->assign('app4less_text', Configuration::get('APP4LESS_TEXT'));
                $this->context->smarty->assign('app4less_name', Configuration::get('APP4LESS_NAME'));
                $this->context->smarty->assign('app4less_link', $link);
                $this->context->smarty->assign('app4less_folder', $this->_path);
                $this->context->smarty->assign('app4less_img_sufix', $image_sufix);
                $output = $this->context->smarty->fetch($this->local_path.'views/templates/front/app_advertisement.tpl');
                return $output;
            }
        }
    }

    public function hookDisplayHeader()
    {
        require_once $this->local_path.'classes/MobileDetect.php';
        if (!class_exists('Mobile_Detect')) { return 'classic'; }

        $detect = new app4less_classes\Mobile_Detect();

        $this->context->smarty->assign(array(
            'app4less_is_enabled' => Module::isEnabled('app4less'),
            'android_app_link' => Configuration::get('APP4LESS_ANDROID'),
            'ios_app_link' => Configuration::get('APP4LESS_IOS'),
            'is_App4Less' => $this->isApp4Less(),
            'is_iOS' => $detect->isiOS()
        ));
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');

        if($this->isApp4Less()) {
            $this->context->controller->addJS($this->_path . '/views/js/front_app4less.js');
            $this->context->controller->addCSS($this->_path . '/views/css/front_app4less.css');
        }

        Media::addJsDef(array ('topidentifier' => Configuration::get('APP4LESS_COORD_IOS'),
            'elementArrow' => Configuration::get('APP4LESS_MOVEMENT_IOS')));
    }

    public function hookDisplayTop()
    {

    }

    public function hookActionAuthentication($params){
        $customer = null;
        if(isset($params['cookie']->id_customer)){
            $customer = new Customer($params['cookie']->id_customer);
        }
        if(isset($params['customer'])){
            $customer = $params['customer'];
        }

        $this->registerInAPPGroup($customer);

    }

    public function hookActionCustomerAccountAdd($params){
        $customer = null;
        if(isset($params['newCustomer'])){
            $customer = $params['newCustomer'];
        }

        $this->registerInAPPGroup($customer);
    }

    public function registerInAPPGroup($customer){
        if($this->isApp4Less()){
            $group_id = Configuration::get('APP4LESS_GROUP');
            if($customer && Validate::isLoadedObject($customer)){
                $customer->addGroups(array($group_id));
            }
        }
    }

    public function isApp4Less(){
        $isApp4Less = false;

        if (!function_exists('getallheaders')) {
            function getallheaders()
            {
                $headers = array();
                foreach ($_SERVER as $name => $value) {
                    if (substr($name, 0, 5) == 'HTTP_') {
                        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                    }
                }
                return $headers;
            }
        }

        $headers = getallheaders();

        if(isset($headers['X-Requested-With']) &&
            (strpos($headers['X-Requested-With'],'reskyt')==true
                || strpos($headers['X-Requested-With'],'app4less') ==true
                || (Configuration::get('APP4LESS_REQUESTED_WITH') ? strpos($headers['X-Requested-With'],Configuration::get('APP4LESS_REQUESTED_WITH')) : false) == true)){

            Context::getContext()->cookie->app4less = true;
            $isApp4Less = true;

        }

        else if(isset($headers['Referer']) &&
            (strpos($headers['Referer'],'app.center3d')==true
                || strpos($headers['Referer'],'app.reskyt')==true
                || strpos($headers['Referer'],'app.app4less') ==true
                || (Configuration::get('APP4LESS_REQUESTED_WITH') ? strpos($headers['Referer'],Configuration::get('APP4LESS_REQUESTED_WITH')) : false) == true)){

            Context::getContext()->cookie->app4less = true;
            $isApp4Less = true;
        }
        else if(Context::getContext()->cookie->app4less == true) {
            $isApp4Less = true;
        }

        if($isApp4Less){
            $date_of_expiry = time() + 60 * 60 * 24 * 20 ;
            setcookie( "isApp4Less", "1", $date_of_expiry, "/");
        }

        return $isApp4Less;
    }
}
