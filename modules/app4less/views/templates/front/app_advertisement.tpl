{*
* 2016 WebImpacto Consulting S.L.
*
*  @author    WebImpacto Consulting S.L. <info@webimpacto.es>
*  @copyright 2014-2016 WebImpacto Consulting S.L.
*}
<div id="app_advertise_fixed">
    <div onclick="location.href='{$app4less_link}'" id="app_image">
        <img src="{$app4less_folder}/images/logoApp{$app4less_img_sufix}.png?ver=1" alt="{$shop.name|escape} - {l s='APP - Descargar' mod='app4less'}" width="70" height="70">
    </div>
    <div onclick="location.href='{$app4less_link}'" id="app_text">
        <strong>{$app4less_name}</strong>
        <br/>
        <span class="">{$app4less_text}</span>
        <br/>
        <a class="btn btn-primary" href="#" title="{$shop.name|escape} - {l s='APP - Descargar' mod='app4less'}">{l s='Descargar' mod='app4less'}</a>
    </div>
    <div id="APPpopupClose" onclick="javascript:closePopUpApp()" style="margin-top: 0px; width: 25px; text-align: center; float: right; position: absolute; font-size: 28px; z-index: 10;line-height: 30px;">
    x
    </div>
</div>
