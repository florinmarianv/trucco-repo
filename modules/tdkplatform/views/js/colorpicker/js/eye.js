/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
* 
**/
(function($){
	var EYE = window.EYE = function() {
		var _registered = {
			init: []
		};
		return {
			init: function() {
				$.each(_registered.init, function(nr, fn){
					fn.call();
				});
			},
			extend: function(prop) {
				for (var i in prop) {
					if (prop[i] != undefined) {
						this[i] = prop[i];
					}
				}
			},
			register: function(fn, type) {
				if (!_registered[type]) {
					_registered[type] = [];
				}
				_registered[type].push(fn);
			}
		};
	}();
	$(EYE.init);
})(jQuery);
