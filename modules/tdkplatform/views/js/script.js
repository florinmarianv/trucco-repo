/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
* 
**/

/**
 * Start block functions common for front-end
 */
(function ($) {
    $.TdkCustomAjax = function () {
        this.tdkData = 'tdkajax=1';
    };
    $.TdkCustomAjax.prototype = {
        processAjax: function () {

            var myElement = this;
            
            if (tdkOption.category_qty && $(".tdk-qty").length)
                myElement.getCategoryList();
            else if ($(".tdk-qty").length)
                $(".tdk-qty").remove();
            
            if (tdkOption.product_list_image && $(".tdk-more-info").length)
                myElement.getProductListImage();
            else if ($(".tdk-more-info").length)
                $(".tdk-more-info").remove();
            
            if (tdkOption.product_one_img && $(".product-additional").length)
                myElement.getProductOneImage();
            else if ($(".product-additional").length)
                $(".product-additional").remove();
            
            if (tdkOption.productCdown && $(".tdk-more-cdown").length)
                myElement.getProductCdownInfo();
            else if ($(".tdk-more-cdown").length)
                $(".tdk-more-cdown").remove();
            
            if (tdkOption.productCdown && $(".tdk-more-color").length)
                myElement.getProductColorInfo();
            else if ($(".tdk-more-color").length)
                $(".tdk-more-color").remove();
            
            //find class tdk-count-wishlist-compare
            if($('.tdk-total-wishlist').length || $('.tdk-total-compare').length)
            {
                myElement.getCountWishlistCompare();
            }
            
            if (myElement.tdkData != "tdkajax=1") {
                $.ajax({
                    type: 'POST',
                    headers: {"cache-control": "no-cache"},
                    url: prestashop.urls.base_url + 'modules/tdkplatform/tdkajax.php' + '?rand=' + new Date().getTime(),
                    async: true,
                    cache: false,
                    dataType: "json",
                    data: myElement.tdkData,
                    success: function (jsonData) {
                        if (jsonData) {
                            if (jsonData.cat) {
                                for (i = 0; i < jsonData.cat.length; i++) {
                                    var str = jsonData.cat[i].total;
                                    var label = $(".tdk-qty.tdk-cat-" + jsonData.cat[i].id_category).data("str");
                                    if(typeof label != "undefined") {
                                        str += "<span>" + label + "</span>";
                                    }
                                    $(".tdk-qty.tdk-cat-" + jsonData.cat[i].id_category).html(str);
                                    $(".tdk-qty.tdk-cat-" + jsonData.cat[i].id_category).show();
                                }
								
                                $('.tdk-qty').each(function(){
                                        if($(this).html() == '')
                                        {
                                                $(this).html('0');
                                                $(this).show();
                                        }
                                })
                            }
                            if (jsonData.product_list_image) {
                                var listProduct = new Array();
                                for (i = 0; i < jsonData.product_list_image.length; i++) {
                                    listProduct[jsonData.product_list_image[i].id] = jsonData.product_list_image[i].content;
                                }

                                $(".tdk-more-info").each(function () {
                                    $(this).html(listProduct[$(this).data("idproduct")]);
                                });
                                addEffectProducts();
                            }

                            if (jsonData.pro_cdown) {
                                var listProduct = new Array();
                                for (i = 0; i < jsonData.pro_cdown.length; i++) {
                                    listProduct[jsonData.pro_cdown[i].id] = jsonData.pro_cdown[i].content;
                                }

                                $(".tdk-more-cdown").each(function () {
                                    $(this).html(listProduct[$(this).data("idproduct")]);
                                });
                            }

                            if (jsonData.pro_color) {
                                var listProduct = new Array();
                                for (i = 0; i < jsonData.pro_color.length; i++) {
                                    listProduct[jsonData.pro_color[i].id] = jsonData.pro_color[i].content;
                                }

                                $(".tdk-more-color").each(function () {
                                    $(this).html(listProduct[$(this).data("idproduct")]);
                                });
                            }
                                                        
                            if (jsonData.product_one_img) {
                                var listProductImg = new Array();
								var listProductName = new Array();
                                for (i = 0; i < jsonData.product_one_img.length; i++) {
                                    listProductImg[jsonData.product_one_img[i].id] = jsonData.product_one_img[i].content;
									listProductName[jsonData.product_one_img[i].id] = jsonData.product_one_img[i].name;
                                }
								
								iw = 360;
								ih = 360;
								if (typeof tdkOption.homeWidth !== 'undefined') {
										iw = tdkOption.homeWidth;
										ih = tdkOption.homeheight;
								}else{
										iw = $('.product_img_link .img-fluid').first().attr('width');
										ih = $('.product_img_link .img-fluid').first().attr('height');
								}
                                $(".product-additional").each(function () {
                                    if (listProductImg[$(this).data("idproduct")]) {
                                        var str_image = listProductImg[$(this).data("idproduct")];
                                        if ($(this).data("image-type")) {
                                            src_image = str_image.replace('home_default',$(this).data("image-type"));
                                        }else{
                                            src_image = str_image.replace('home_default', 'home_default');
                                        }
										var name_image = listProductName[$(this).data("idproduct")];
                                        $(this).html('<img class="img-fluid" title="'+name_image+'" alt="'+name_image+'" src="' + src_image + '" width="'+iw+'" height="'+ih+'"/>');
                                    }
                                });
                                //addEffOneImg();
                            }
                            
                            //wishlist 
                            if (jsonData.wishlist_products)
                            {
								$('.tdk-total-wishlist').data('wishlist-total',jsonData.wishlist_products);
                                $('.tdk-total-wishlist').text(jsonData.wishlist_products);
                            }
                            else
                            {
								$('.tdk-total-wishlist').data('wishlist-total',0);
                                $('.tdk-total-wishlist').text('0');
                            }
                            
                            //compare
                            if (jsonData.compared_products)
                            {
								$('.tdk-total-compare').data('compare-total',jsonData.compared_products);
                                $('.tdk-total-compare').text(jsonData.compared_products);
                            }
                            else
                            {
								$('.tdk-total-compare').data('compare-total',0);
                                $('.tdk-total-compare').text(0);
                            }

                        }
                    },
                    error: function () {
                    }
                });
            }
        },
        
        //check get number product of wishlist compare
        getCountWishlistCompare: function()
        {
            this.tdkData += '&wishlist_compare=1';
        },
        
        getCategoryList: function () {
            //get category id
            var tdkCatList = "";
            $(".tdk-qty").each(function () {
                if($(this).data("id")){
                    if (tdkCatList)
                        tdkCatList += "," + $(this).data("id");
                    else
                        tdkCatList = $(this).data("id");
                }else{
                    if (tdkCatList)
                        tdkCatList += "," + $(this).attr("id");
                    else
                        tdkCatList = $(this).attr("id");
                }
            });

            if (tdkCatList) {
                tdkCatList = tdkCatList.replace(/tdk-cat-/g, "");
                this.tdkData += '&cat_list=' + tdkCatList;
            }
            return false;
        },
        getProductListImage: function () {
            var tdkProInfo = "";
            $(".tdk-more-info").each(function () {
				//TDK:: fix bug, not have attribute
				if (typeof $(this).data("idproduct") != "undefined")
				{
					if (!tdkProInfo)
						tdkProInfo += $(this).data("idproduct");
					else
						tdkProInfo += "," + $(this).data("idproduct");
				}
            });
            if (tdkProInfo) {
                this.tdkData += '&product_list_image=' + tdkProInfo;
            }
            return false;
        },
        getProductCdownInfo: function () {
            var tdkProCdown = "";
            $(".tdk-more-cdown").each(function () {
                if (!tdkProCdown)
                    tdkProCdown += $(this).data("idproduct");
                else
                    tdkProCdown += "," + $(this).data("idproduct");
            });
            if (tdkProCdown) {
                this.tdkData += '&pro_cdown=' + tdkProCdown;
            }
            return false;
        },
        getProductColorInfo: function () {
            var tdkProColor = "";
            $(".tdk-more-color").each(function () {
                if (!tdkProColor)
                    tdkProColor += $(this).data("idproduct");
                else
                    tdkProColor += "," + $(this).data("idproduct");
            });
            if (tdkProColor) {
                this.tdkData += '&pro_color=' + tdkProColor;
            }
            return false;
        },
        getProductOneImage: function () {
            //tranditional image
            var tdkAdditional = "";
            $(".product-additional").each(function () {
                if (!tdkAdditional)
                    tdkAdditional += $(this).data("idproduct");
                else
                    tdkAdditional += "," + $(this).data("idproduct");
            });
            if (tdkAdditional) {
                this.tdkData += '&product_one_img=' + tdkAdditional;
            }
            return false;
        }
    };
}(jQuery));

function addJSProduct(currentProduct) {
// http://demos.flesler.com/jquery/serialScroll/
//    if (typeof serialScroll == 'function') { 
        $('.thumbs_list_' + currentProduct).serialScroll({
            items: 'li:visible',
            prev: '.view_scroll_left_' + currentProduct,
            next: '.view_scroll_right_' + currentProduct,
            axis: 'y',
            offset: 0,
            start: 0,
            stop: true,
            duration: 700,
            step: 1,
            lazy: true,
            lock: false,
            force: false,
            cycle: false,
			onBefore: function( e, elem, $pane, $items, pos ){
				//TDK:: update status for button
				if( pos == 0 )
				{
					$('.view_scroll_left_' + currentProduct).addClass('disable');					
				}
                else if( pos == $items.length -1 )
				{
					$('.view_scroll_right_' + currentProduct).addClass('disable');
				}
				else
				{
					$('.view_scroll_left_' + currentProduct).removeClass('disable');
					$('.view_scroll_right_' + currentProduct).removeClass('disable');
				}
			},
        });
        $('.thumbs_list_' + currentProduct).trigger('goto', 1);// SerialScroll Bug on goto 0 ?
        $('.thumbs_list_' + currentProduct).trigger('goto', 0);
 //   }   
}
function addEffectProducts(){
    
    if(typeof(tdkOption) != 'undefined' && tdkOption.product_list_image){
        $(".tdk-more-info").each(function() {
            addJSProduct($(this).data("idproduct"));
        });
        addEffectProduct();
    }
}

function addEffectProduct() {
    var speed = 800;
    var effect = "easeInOutQuad";

    //$(".products_block .carousel-inner .ajax_block_product:first-child").mouseenter(function() {
        //$(".products_block .carousel-inner").css("overflow", "inherit");
    //});
    //$(".carousel-inner").mouseleave(function() {
        //$(".carousel-inner").css("overflow", "hidden");
    //});

    $(".tdk-more-info").each(function() {
        var tdk_preview = this;
        $(tdk_preview).find(".tdk-hover-image").each(function() {
            $(this).mouseover(function() {
                var big_image = $(this).attr("rel");
                var imgElement = $(tdk_preview).parent().find(".product-thumbnail img").first();
				//TDK:: hover product image gallery change second product image
				if ($(tdk_preview).parent().find(".product-thumbnail .product-additional img").first().length);
				{
					imgElement = $(tdk_preview).parent().find(".product-thumbnail .product-additional img").first();
				}
                if (!imgElement.length) {
                    imgElement = $(tdk_preview).parent().find(".product_image img").first();
                }
				
                if (imgElement.length) {
                    $(imgElement).stop().animate({opacity: 0}, {duration: speed, easing: effect});
                    $(imgElement).first().attr("src", big_image);
                    $(imgElement).first().attr("data-rel", big_image);
                    $(imgElement).stop().animate({opacity: 1}, {duration: speed, easing: effect});
                }
				//TDK:: change class when hover another image
				if (!$(this).hasClass('shown'))
				{
					$(tdk_preview).find('.shown').removeClass('shown');
					$(this).parent().addClass('shown');
				}
            });
        });

		//TDK:: click image in product image thumb to show product gallery
		/*
        $('.thickbox-ajax-'+$(this).data("idproduct")).fancybox({
            helpers: {
                overlay: {
                    locked: false
                }
            },
            'hideOnContentClick': true,
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic'
        });
		*/
		//TDK:: click image in product image thumb to show quickview
		$('.thickbox-ajax-'+$(this).data("idproduct")).click(function(){
			$(tdk_preview).parents('.product-miniature').find('.quick-view').trigger('click');
			return false;
		});
    });
}

function addEffOneImg() {
    var speed = 800;
    var effect = "easeInOutQuad";

    $(".product-additional").each(function() {
        if ($(this).find("img").length) {
            var tdk_hover_image = $(this).parent().find("img").first();
            var tdk_preview = $(this);
            $(this).parent().mouseenter(function() {
                $(this).find("img").first().stop().animate({opacity: 0}, {duration: speed, easing: effect});
                $(tdk_preview).stop().animate({opacity: 1}, {duration: speed, easing: effect});
            });
            $(this).parent().mouseleave(function() {
                $(this).find("img").first().stop().animate({opacity: 1}, {duration: speed, easing: effect});
                $(tdk_preview).stop().animate({opacity: 0}, {duration: speed, easing: effect});
            });
        }
    });
}
function log(message) {
    console.log(message);
}

//TDK:: action animation
function activeAnimation()
{
	$(".has-animation").each(function() {
		onScrollInit($(this));
	});
}

function onScrollInit(items) {
    items.each(function() {
        var osElement = $(this);
        var animation = $(osElement).data("animation");
        var osAnimationDelay = $(osElement).data("animation-delay");
		var osAnimationDuration = $(osElement).data("animation-duration");
		var osAnimationIterationCount = $(osElement).data("animation-iteration-count");
		var osAnimationInfinite = $(osElement).data("animation-infinite");
		if (osAnimationInfinite == 1)
		{
			var loop_animation = 'infinite';
		}
		else
		{
			var loop_animation = osAnimationIterationCount;
		}
        osElement.css({
            "-webkit-animation-delay": osAnimationDelay,
            "-moz-animation-delay": osAnimationDelay,
            "animation-delay": osAnimationDelay,
			"-webkit-animation-duration": osAnimationDuration,
            "-moz-animation-duration": osAnimationDuration,
            "animation-duration": osAnimationDuration,
			"-webkit-animation-iteration-count": loop_animation,
            "-moz-animation-iteration-count": loop_animation,
            "animation-iteration-count": loop_animation,
        });
        
        osElement.waypoint(function() {		
			if (osElement.hasClass('has-animation'))
			{					
				osElement.addClass('animated '+ animation).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){					
					$(this).removeClass('has-animation animated ' +animation);					
				});			
			}            
	
			this.destroy();
        }, {
            triggerOnce: true,
            offset: '100%'
        });
    });
}
/**
 * End block functions common for front-end
 */


/**
 * End block for module tdk_gmap
 */
function synSize(name) {
    var obj = $("#" + name);
    var div = $(obj).closest(".gmap-cover");
    var gmap = $(div).find(".gmap");
    $(obj).height($(gmap).height());
    //console.log($(gmap).height());
}

$(function() {
    /**
     * Start block for module tdk_product_list
     */
    $(".btn-show-more").click(function() {
        var page = parseInt($(this).data('page'));
		var use_animation = parseInt($(this).data('use-animation'));
        var btn = $(this);
        var config = $(this).closest(".TdkProductList").find(".tdkconfig").val();

        // FIX 1.7
        btn.data('reset-text', btn.html() );
        btn.html(  btn.data('loading-text') );
		
        $.ajax({
            headers: {"cache-control": "no-cache"},
            url: prestashop.urls.base_url + 'modules/tdkplatform/tdkajax.php',
            async: true,
            cache: false,
            dataType: "Json",
            data: {"config": config, "p": page, "use_animation": use_animation, "page_name": prestashop.page.page_name},
            success: function(response) {
                var boxCover = $(btn).closest(".box-show-more");
                if(!response.is_more) {
                    $(boxCover).removeClass("open").fadeOut();
                }
                if(response.html) {
                    $(boxCover).prev().append(response.html);
                }
                $(btn).data("page", (page + 1));
				
				if (typeof $.TdkCustomAjax !== "undefined" && $.isFunction($.TdkCustomAjax)) {
					var tdkCustomAjax = new $.TdkCustomAjax();
					tdkCustomAjax.processAjax();
				}
				
				//TDK:: class function of tdktoolkit
				callTdkToolkit();
				
				//TDK:: re call run animation
				activeAnimation();
				
            }
        }).always(function () {
            // FIX 1.7
            btn.html(  btn.data('reset-text') );
        });
    });
    /**
     * End block for module tdk_product_list
     */
    /**
     * Start block for module tdk_image
     */
	 //TDK:: call active anmation
    activeAnimation();
    /**
     * End block for module tdk_image
     */
});
(function ($) {

    window.addRule = function (selector, styles, sheet) {

        styles = (function (styles) {
            if (typeof styles === "string") return styles;
            var clone = "";
            for (var prop in styles) {
                if (styles.hasOwnProperty(prop)) {
                    var val = styles[prop];
                    prop = prop.replace(/([A-Z])/g, "-$1").toLowerCase(); // convert to dash-case
                    clone += prop + ":" + (prop === "content" ? '"' + val + '"' : val) + "; ";
                }
            }
            return clone;
        }(styles));
        


        try {
            sheet = sheet || document.styleSheets[document.styleSheets.length - 1];
            if (sheet.insertRule)
            {
                // if(!(navigator.userAgent.toLowerCase().indexOf('firefox') > -1))		// FIX work on firefox
                            // {
                    if(sheet.cssRules!==null && sheet.cssRules.length!== 0)
                        sheet.insertRule(selector + " {" + styles + "}", sheet.cssRules.length);
                // }
            }
            else if (sheet.addRule) sheet.addRule(selector, styles);
        }
        catch(err) {
            // way 2 : https://stackoverflow.com/questions/28930846/how-to-use-cssstylesheet-insertrule-properly
            var style = (function() {
                var style = document.createElement("style");
                style.appendChild(document.createTextNode(""));
                document.head.appendChild(style);
                return style;
            })();
            style.sheet.insertRule(selector + " {" + styles + "}", 0);
            
            
        }
//        window.console.log(sheet);
//        window.console.log(selector);
//        window.console.log(styles);
        return this;

    };

    if ($) $.fn.addRule = function (styles, sheet) {
        addRule(this.selector, styles, sheet);
        return this;
    };

}(this.jQuery || this.Zepto));
function tdkPopupForm(){
	var profile_key = $('body').data('profile-key');
    if($.cookie('tdknewletter-'+profile_key))
	{
		return;
	}
    $.fancybox({
            'content' : $(".tdk-popup").html(),
            'wrapCSS' : 'tdk-popup-clone',
            afterClose: function (event, ui) {
				
            },
			beforeShow: function (event, ui) {
                // this.inner.append("<div class='turnoff-popup-wrapper text-center'><input id='turnoff-popup-bt' name='turnoff-popup-bt' class='turnoff-popup' type='checkbox'><label for='turnoff-popup-bt'>"+turnoff_popup_text+"</label></div>");			
				$('.block_newsletter').append("<div class='turnoff-popup-wrapper text-center'><input id='turnoff-popup-bt' name='turnoff-popup-bt' class='turnoff-popup' type='checkbox'><label for='turnoff-popup-bt'>"+turnoff_popup_text+"</label></div>");
				$('.turnoff-popup').click(function(){
					if (!$(this).hasClass('active'))
					{
						$.cookie('tdknewletter-'+profile_key, '1');
						$(this).addClass('active');
					}
					else
					{
						$.cookie('tdknewletter-'+profile_key, null);
						$(this).removeClass('active');
					}
				})
            }
    });
}

//set background and parallax
$(document).ready(function(){
    //show popup
    if($('.tdk-popup').length){        
		//TDK:: only display popup at homepage
		if($('body').attr('id') == 'index')
		{
			tdkPopupForm();
		}
    }

    $(".has-bg.bg-fullwidth").each(function(){
        id = "#"+$(this).attr("id");
        bg = $(this).data("bg");
        $(id + ":before").addRule({
            background: bg
        });
    });
    //stela
    if (typeof stellar !== 'undefined' && stellar)
            $.stellar({horizontalScrolling:false});

    //mouse
    currentPosX = [];
    currentPosY = [];
    $("div[data-mouse-parallax-strength]").each(function(){
        currentPos = $(this).css("background-position");
        if (typeof currentPos == "string")
        {
            currentPosArray = currentPos.split(" ");
        }else
        {
            currentPosArray = [$(this).css("background-position-x"),$(this).css("background-position-y")];
        }
        currentPosX[$(this).data("mouse-parallax-rid")] = parseFloat(currentPosArray[0]);
        currentPosY[$(this).data("mouse-parallax-rid")] = parseFloat(currentPosArray[1]);
        $(this).mousemove(function(e){
            newPosX = currentPosX[$(this).data("mouse-parallax-rid")];
            newPosY = currentPosY[$(this).data("mouse-parallax-rid")];
            if($(this).data("mouse-parallax-axis") != "axis-y"){
                mparallaxPageX = e.pageX - $(this).offset().left;
                if($(this).hasClass("full-bg-screen"))
                {
                    mparallaxPageX = mparallaxPageX - 1000;
                }
                newPosX = (mparallaxPageX * $(this).data("mouse-parallax-strength") * -1) + newPosX;
            }
            if($(this).data("mouse-parallax-axis") !="axis-x"){
                mparallaxPageY = e.pageY - $(this).offset().top;
                newPosY = mparallaxPageY * $(this).data("mouse-parallax-strength") * -1;
            }
            $(this).css("background-position",newPosX+"px "+newPosY+"px");
        });
    });

    var ytIframeId; var ytVideoId;
    function onYouTubeIframeAPIReady() {
        $("div.iframe-youtube-api-tag").each(function(){
            ytIframeId = $(this).attr("id");
            ytVideoId = $(this).data("youtube-video-id");

            new YT.Player(ytIframeId, {
                videoId: ytVideoId,
                width: "100%",
                height: "100%",
                playerVars :{autoplay:1,controls:0,disablekb:1,fs:0,cc_load_policy:0,
                            iv_load_policy:3,modestbranding:0,rel:0,showinfo:0,start:0},
                events: {
                    "onReady": function(event){
                        event.target.mute();
                        setInterval(
                            function(){event.target.seekTo(0);},
                            (event.target.getDuration() - 1) * 1000
                        );
                    }
                }
            });
        });
    }
    onYouTubeIframeAPIReady();
    
    if (typeof MediaElementPlayer !== 'undefined') {
        //add function for html5 youtube video
        var player1 = new MediaElementPlayer('#special-youtube-video1');
        var player2 = new MediaElementPlayer('#special-youtube-video2');
        if(player1)
        {
            var auto_find = setInterval(function(){
                if($('#video-1 .mejs-overlay-play').html())
                {
                    $('#video-1 .mejs-overlay-play>.mejs-overlay-button').before('<div class="video-name">'+$('#special-youtube-video1').data('name')+'</div>');
                    $('#video-1 .mejs-overlay-play').append('<div class="video-description">Watch video and <span>subscribe us<span></div>');   
                    clearInterval(auto_find);
                }
            }, 500);
        }
        
        if(player2)
        {
            var auto_find1 = setInterval(function(){        
                if($('#video-2 .mejs-overlay-play').html())
                {
                    $('#video-2 .mejs-overlay-play>.mejs-overlay-button').before('<div class="video-name">'+$('#special-youtube-video2').data('name')+'</div>');
                    $('#video-2 .mejs-overlay-play').append('<div class="video-description">Watch video and <span>subscribe us<span></div>');   
                    clearInterval(auto_find1);              
                }
            }, 500);
        }
    }
    //js for select header, footer, content in demo
    current_url = window.location.href;
    $('.tdkconfig').each(function(){
        var enableJS = $(this).data('enablejs');
        if(enableJS == false){
            return;
        }
        var tdkchange = '&tdkpanelchange';
        var current_url = $(this).data('url');
        if( !current_url ){
            current_url = window.location.href;
            current_url = current_url.replace(tdkchange, "");
        }
        
        var param = $(this).data('type');
        var value = $(this).data('id');
        var re = new RegExp("([?|&])" + param + "=.*?(&|$)","i");
        if (current_url.match(re))
            $(this).attr('href', current_url.replace(re,'$1' + param + "=" + value + '$2') + tdkchange);
        else{
            if(current_url.indexOf('?') == -1)
                $(this).attr('href', current_url + '?' + param + "=" + value + tdkchange);
            else
                 $(this).attr('href', current_url + '&' + param + "=" + value + tdkchange);
                
        }
    });
	
	//TDK:: fix owl carousel in tab load delay when resize.
	$(window).resize(function(){
		if($('.tab-pane .owl-carousel').length)
		{
			$('.tab-pane .owl-carousel').each(function(index, element){
				if(!$(element).parents('.tab-pane').hasClass('active') && typeof ($(element).data('owlCarousel')) !== "undefined")
				{
					var w_owl_active_tab = $(element).parents('.tab-pane').siblings('.active').find('.owl-carousel').width();
			
					$(element).width(w_owl_active_tab);
					$(element).data('owlCarousel').updateVars();
					$(element).width('100%');
				}
			});
		}
	})
	
	//TDK:: loading owl carousel fake item
	var check_window_w = parseInt($(window).width());
	
	if (check_window_w >= 992 && check_window_w < 1200)
	{	   
	   addClassLoading('lg');
	}
	else if (check_window_w >= 768 && check_window_w < 992)
	{	  
	   addClassLoading('md');
	}
	else if (check_window_w >= 576 && check_window_w < 768)
	{	   
	   addClassLoading('sm');
	}
	else if (check_window_w < 576)
	{	   
	   addClassLoading('m');
	}
	else
	{		
		addClassLoading('xl');
	}
	

    // // Slick Carousel
    // $( ".slick-carousel-parent" ).each(function( index ) {
    //     var slick_config = $(this).data("slick-config");
    //     if(slick_config){

    //         $(this).on('init', function() {
    //             $(this).css("display", "block");
    //         });
            
    //         $(this).slick({
    //             dots: parseInt(slick_config.dot)?true:false,
    //             //infinite: parseInt(slick_config.dot)?false:true,
    //             infinite: parseInt(slick_config.infinite)?true:false,
    //             vertical: parseInt(slick_config.vertical)?true:false,
    //             autoplay: parseInt(slick_config.autoplay)?true:false,
    //             pauseonhover: parseInt(slick_config.autoplay)?true:false,
    //             autoplaySpeed: 2000,
    //             arrows: parseInt(slick_config.arrows)?true:false,
    //             row: parseInt(slick_config.row),
    //             slidesToShow: parseInt(slick_config.slidestoshow),
    //             slidesToScroll: parseInt(slick_config.slidestoscroll),
    //             responsive: [
    //                 {
    //                   breakpoint: 1024,
    //                   settings: {
    //                     row: parseInt(slick_config.row_1024),
    //                     slidesToShow: parseInt(slick_config.slidestoshow_1024),
    //                   }
    //                 },
    //                 {
    //                   breakpoint: 768,
    //                   settings: {
    //                     row: parseInt(slick_config.row_768),
    //                     slidesToShow: parseInt(slick_config.slidestoshow_768),
    //                   }
    //                 },
    //                 {
    //                   breakpoint: 600,
    //                   settings: {
    //                     row: parseInt(slick_config.row_600),
    //                     slidesToShow: parseInt(slick_config.slidestoshow_600),
    //                   }
    //                 },
    //                 {
    //                   breakpoint: 450,
    //                   settings: {
    //                     row: parseInt(slick_config.row_450),
    //                     slidesToShow: parseInt(slick_config.slidestoshow_450),
    //                   }
    //                 }
    //             ]
    //         });
            
    //         $(this).on('init', function(slick){
    //             console.log("123");
    //         });

    //     }
    // });

});

//TDK:: add class loading by width of window
function addClassLoading($type)
{
	$('.timeline-wrapper').each(function(){		
		if ($(this).data($type) <= $(this).data('item'))
		{			
			var num_remove_item = $(this).data('item') - $(this).data($type);
			if ($(this).data($type) < $(this).data('item'))
			{
				var num_remove_item = $(this).data('item') - $(this).data($type);
				$(this).find('.timeline-parent').slice(-num_remove_item).remove();
			}
			
			if (12%$(this).data($type) == 0)
			{
				if ($type == 'm')
				{
					$(this).find('.timeline-parent').addClass('col-xs-'+12/$(this).data($type));
				}
				else
				{
					$(this).find('.timeline-parent').addClass('col-'+$type+'-'+12/$(this).data($type));
				}
			}
			else
			{				
				$(this).find('.timeline-parent').css({'width': 100/$(this).data($type)+'%', 'float': 'left'})
			}
				
			$('.timeline-wrapper').removeClass('prepare');
		}
	})
}

//TDK:: call function from tdktoolkit
function callTdkToolkit()
{
	if(typeof (tdkBtCart) != 'undefined')
	{		
		tdkBtCart();
	}
	if(typeof (tdkSelectAttr) != 'undefined')
	{
		tdkSelectAttr();		
	}
	if(typeof (TdkWishlistButtonAction) != 'undefined')
	{	
		TdkWishlistButtonAction();		
	}
	if(typeof (TdkCompareButtonAction) != 'undefined')
	{		
		TdkCompareButtonAction();
	}
	if(typeof (actionQuickViewLoading) != 'undefined')
	{
		actionQuickViewLoading();
	}
}

function SetOwlCarouselFirstLast(el){
    el.find(".owl-item").removeClass("first");
    el.find(".owl-item.active").first().addClass("first");

    el.find(".owl-item").removeClass("last");
    el.find(".owl-item.active").last().addClass("last");
}
/**
 * List functions will run when document.ready()
 */
$(document).ready(function()
{
    if(typeof (tdk_list_functions) != 'undefined')
    {
        for (var i = 0; i < tdk_list_functions.length; i++) {
            tdk_list_functions[i]();
        }
    }
});

/**
 * List functions will run when window.load()
 */
$(window).load(function(){
	if(typeof (tdk_list_functions_loaded) != 'undefined')
    {
        for (var i = 0; i < tdk_list_functions_loaded.length; i++) {
            tdk_list_functions_loaded[i]();
        }
    }
})

$(document).ready(function(){
    // REPLACE URL IN BLOCKLANGUAGES
    if(typeof tdkprofile_multilang_url != "undefined") {
         $.each(tdkprofile_multilang_url, function(index, profile){
            var url_search = prestashop.urls.base_url + profile.iso_code;
            var url_change = prestashop.urls.base_url + profile.iso_code + '/' + profile.friendly_url + '.html';
            
            //console.log(url_change);
                        
			//TDK:: update for module blockgrouptop and default
			if ($('#tdk_block_top').length)
			{
				var parent_o = $('#tdk_block_top .language-selector');
			}
			else
			{
				var parent_o = $('.language-selector-wrapper');
			}
			
			parent_o.find('li a').each(function(){
				
				var lang_href = $(this).attr('href');
				
				if(lang_href.indexOf(url_search) > -1 )
				{
					//window.console.log('--' + url_change);
					$(this).attr('href', url_change);
					//window.console.log(url_change);
				}
			});
			
        });
    }
    
	//TDK:: update for module blockgrouptop and default
	if ($('#tdk_block_top').length)
	{
		var parent_o_currency = $('#tdk_block_top .currency-selector');
	}
	else
	{
		var parent_o_currency = $('.currency-selector');
	}
    
    // REPLACE URL IN BLOCKLANGUAGES
	parent_o_currency.find('li a').each(function(){
		
        var url_link = $(this).attr('href');
        var id_currency = getParamFromURL("id_currency", url_link);
        var SubmitCurrency = getParamFromURL("SubmitCurrency", url_link);
        
        var current_url = window.location.href;
		//TDK:: fix for only product page, url has #
		if (prestashop.page.page_name == 'product')
		{			
			var current_url = prestashop.urls.current_url;		
		}
        var current_url = removeParamFromURL('SubmitCurrency',current_url);
        var current_url = removeParamFromURL('id_currency',current_url);
        
        if(current_url.indexOf('?') == -1){
            var new_url = current_url + '?SubmitCurrency=' + SubmitCurrency + "&id_currency=" +id_currency;
            $(this).attr('href', new_url);
        }
        else{
            var new_url = current_url + '&SubmitCurrency=' + SubmitCurrency + "&id_currency=" +id_currency;
            $(this).attr('href', new_url);
        }
    });
    
    
    prestashop.on('updateProductList', function() {
        // FIX BUG : FILTER PRODUCT NOT SHOW MORE IMAGE
        if (typeof $.TdkCustomAjax !== "undefined" && $.isFunction($.TdkCustomAjax)) {
            var tdkCustomAjax = new $.TdkCustomAjax();
            tdkCustomAjax.processAjax();
        }
    });
	
	//TDK:: auto get link product and add to demo link on megamenu to show multi product detail
	if ($('.demo-product-detail a').length && prestashop.page.page_name != 'product')
	{
		$.ajax({
			type: 'POST',
			headers: {"cache-control": "no-cache"},
			url: prestashop.urls.base_url + 'modules/tdkplatform/tdkajax.php' + '?rand=' + new Date().getTime(),
			async: true,
			cache: false,
			dataType: "json",
			data: {"action": "get-product-link"},
			success: function (jsonData) {				
				if (jsonData)
				{
					$('.demo-product-detail a').each(function(){						
						var original_url = $(this).attr('href');
						var layout_key = original_url.substring(original_url.indexOf('?layout='));
						
						var new_url = jsonData.replace('.html', '.html'+layout_key);
						$(this).attr('href',new_url).addClass('updated');
					});
				}
				
			},
			error: function () {
			}
		});
	}
});

function removeParamFromURL(key, sourceURL) {

    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";

    
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        if(params_arr.length > 0){
            rtn = rtn + "?" + params_arr.join("&");
        }
    }
    return rtn;
}

function getParamFromURL(key, sourceURL) {

    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";

    if (queryString !== "") {
        params_arr = queryString.split("&");
        
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                return params_arr[i].split("=")[1];
            }
        }
    }
    return false;
}