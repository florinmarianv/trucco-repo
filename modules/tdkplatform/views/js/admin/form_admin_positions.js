/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
* 
**/

$(document).ready(function() {
    $('.tdk_delete_position').each(function(){
        
        $(this).closest('a').attr('href',"javascript:void(0);");
        
        $('<input>', {
            type: 'hidden',
            id : 'tdk_delete_position',
            name: 'tdk_delete_position',
            value: '0'
        }).appendTo( $(this).parent() );
        
        $(this).closest('a').click(function(){
            if (confirm(tdk_confirm_text)){
                $('#tdk_delete_position').val('1');
                $(this).closest('form').attr('action', tdk_form_submit);
                $(this).closest('form').submit();
            }
        });
    });
});