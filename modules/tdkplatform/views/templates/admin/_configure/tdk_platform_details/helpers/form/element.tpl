{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="widget-row col-md-12 {$eItem.file} plist-element" data-element='{$eItem.file}'{if isset($configElement)} data-form='{$configElement}'{/if}><a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Drag me' mod='tdkplatform'}" class="group-action gaction-drag label-tooltip"><i class="icon-move"></i> </a>
    <a class="show-postion" href="#postion_layout" title="Click to see postion">
    {$eItem.name|escape:'html':'UTF-8'}
    </a>
    {if isset($eItem.icon)}<i class="{$eItem.icon}"></i>{/if}
    <div class="pull-right">
        {if isset($eItem.config)}
        <a href="javascript:void(0)" data-config="{$eItem.config}" title="{l s='Config element' mod='tdkplatform'}" class="element-config" data-type="TdkColumn" data-for=".column-row"><i class="icon-pencil"></i></a>
        {/if}
        <a class="plist-eremove"><i class="icon-trash"></i></a>
        <a class="plist-eedit" data-element='{$eItem.file}'><i class="icon-edit"></i></a>
    </div>
</div>