{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\admin\configure -->
{*
{$guide_box}
*}
<div class="panel">
	<h3><i class="icon icon-book"></i> {l s='Documentation' mod='tdkplatform'}</h3>
	<p>
        &raquo; {l s='Before Start You can click here to read guide' mod='tdkplatform'} :
        <ul>
            <li><a href="https://tdk-studio.com/guide/prestashop/" target="_blank">{l s='Read Guide' mod='tdkplatform'}</a></li>
        </ul>
		&raquo; {l s='You can start with TDK Platform following steps' mod='tdkplatform'} :
		<ul>
			<li><a href="{$create_profile_link|escape:'html':'UTF-8'}" target="_blank">{l s='Create new Profile' mod='tdkplatform'}</a></li>
		</ul>
		&raquo; {l s='Others management function:' mod='tdkplatform'}
		<ul>
			<li><a href="{$profile_link|escape:'html':'UTF-8'}" target="_blank">{l s='Manager Profile' mod='tdkplatform'}</a>
				<span> - {l s='This function enables you to manage all profiles in the module. This function is useful when you\'re building plans before the home interface changes, the product page for the event discounts, holidays ... by changing the options Default profile' mod='tdkplatform'}</span>
			</li>
			<li><a href="{$position_link|escape:'html':'UTF-8'}" target="_blank">{l s='Manager Position' mod='tdkplatform'}</a>
				<span> - {l s='This function enables you to manage all of the position of all profiles. This function is useful when you have multiple profiles' mod='tdkplatform'}</span>
			</li>
			<li><a href="{$product_link|escape:'html':'UTF-8'}" target="_blank">{l s='Manager Product List Builder' mod='tdkplatform'}</a>
				<span> - {l s='A function to help you design the details of the composition of the products displayed in the list of products on the website.' mod='tdkplatform'}</span>
			</li>
		</ul>
	</p>
</div>
<div class="panel">
	<h3>
        <i class="icon icon-credit-card"></i> <span class="open-content">{l s='Sample Data' mod='tdkplatform'}</span>
        </h3>
        <div class="panel-content-builder">
            <p>
            <strong>{l s='Here is my module page builder!' mod='tdkplatform'}</strong><br />
            {l s='Thanks to PrestaShop, now I have a great module.' mod='tdkplatform'}<br />
            {l s='You can configure it using the following configuration form.' mod='tdkplatform'}
            </p>
            <div class="alert alert-info">
                {l s='You can click here to import demo data' mod='tdkplatform'}
            </div>
            <a class="btn btn-default btn-primary" onclick="javascript:return confirm('{l s='Are you sure you want to install demo?' mod='tdkplatform'}')" href="{$module_link|escape:'html':'UTF-8'}&installdemo=1"><i class="icon-AdminTools"></i> {l s='Install Demo Data' mod='tdkplatform'}</a>
            <br/><br/>
            <div class="alert alert-info">
                {l s='You can download demo image in' mod='tdkplatform'}<br/>
                {l s='Then you can unzip and copy folder tdkplatform to Root/themes/THEME_NAME/assets/img/modules' mod='tdkplatform'}
            </div>
            <a class="btn btn-default btn-primary" href="http://demothemes.info/prestashop/tdkplatform/tdkplatform.zip"><i class="icon-AdminCatalog"></i> {l s='Demo Image' mod='tdkplatform'}</a><br/>
						<br/><br/>
						{*<div class="alert alert-info">
                {l s='You can reset database' mod='tdkplatform'}<br/>
            </div>*}
						{*<a class="btn btn-default btn-primary" style="background-color:red" onclick="javascript:return confirm('{l s='Are you sure you want to reset data? All database will lost' mod='tdkplatform'}')" href="{$module_link|escape:'html':'UTF-8'}&resetmodule=1"><i class="icon-AdminTools"></i> {l s='Reset Data' mod='tdkplatform'}</a>*}
        </div>
</div>
<div class="panel">
	<h3>
        <i class="icon icon-credit-card"></i> <span class="open-content">{l s='Back-up and Update' mod='tdkplatform'}</span>
        </h3>
        <div class="panel-content-builder">
            <div class="alert alert-info">
                {l s='Please click back-up button to back-up database' mod='tdkplatform'}
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <a class="btn btn-default" href="{$module_link|escape:'html':'UTF-8'}&backup=1">
                        <i class="icon-AdminParentPreferences"></i> {l s='Back-up to PHP file' mod='tdkplatform'}
                    </a>                
                </div>
                
            </div>
            <hr/><br/>
            <div class="alert alert-info">
                {l s='You can select a file by date backup to restore data' mod='tdkplatform'}
            </div>
            <div class="row">
                <form class="defaultForm form-horizontal" action="{$module_link|escape:'html':'UTF-8'}" method="post" enctype="multipart/form-data" novalidate="">
                    <div class="col-sm-12">
                        {if $back_up_file}
                            <select name="backupfile" style="width:50%">
                            {foreach from=$back_up_file item=file name=Modulefile}
                                <option value="{$file|escape:'html':'UTF-8'}">{$file|escape:'html':'UTF-8'}</option>
                            {/foreach}
                            </select>
                        {else}
                            <i style="color:red">{l s='No file to select in : ' mod='tdkplatform'}{$backup_dir}</i>
                        {/if}
                        <br/>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-default" name="restore">
                        {l s='Restore from PHP file' mod='tdkplatform'}
                        </button>
                    </div>
                </form>
            </div>
            <hr/><br/>
            <div class="alert alert-warning">
                {l s='Delete position do not use (fix error when create profile)' mod='tdkplatform'}
            </div>
            <a class="btn btn-default" onclick="javascript:return confirm('{l s='Are you sure you want to Delete do not use position. Please back-up all thing before?' mod='tdkplatform'}')" href="{$module_link|escape:'html':'UTF-8'}&deleteposition=1">
                <i class="icon-AdminParentPreferences"></i> {l s='Delete do not use position' mod='tdkplatform'}</a>
            
        </div>
</div>