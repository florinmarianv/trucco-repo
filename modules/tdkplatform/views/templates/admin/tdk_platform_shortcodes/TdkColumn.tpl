{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\admin\tdk_platform_shortcodes\TdkColumn -->
<div {if !isset($tdkInfo)}id="default_column"{/if} class="column-row{if !isset($tdkInfo)} col-sp-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12{/if}{if isset($colClass)} {$colClass|replace:'.':'-'|escape:'html':'UTF-8'}{/if}{if isset($formAtts)} {$formAtts.form_id|escape:'html':'UTF-8'}{if isset($formAtts.active) && !$formAtts.active} deactive{else} active{/if}{/if}">
	<div class="cover-column">
		<div class="column-controll-top">
			<a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Drag to sort Column' mod='tdkplatform'}" class="column-action caction-drag label-tooltip"><i class="icon-move"></i> </a>
			&nbsp;
			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
					<span>{l s='Column' mod='tdkplatform'}</span> <span class="caret"></span>
				</button>
				<ul class="dropdown-menu for-column-row" role="menu">
					<li><a href="javascript:void(0)" title="{l s='Add new Widget' mod='tdkplatform'}" class="column-action btn-new-widget "><i class="icon-plus-sign"></i> {l s='Add new Widget' mod='tdkplatform'}</a></li>
					<li><a href="javascript:void(0)" title="{l s='Edit Column' mod='tdkplatform'}" class="column-action btn-edit " data-type="TdkColumn" data-for=".column-row"><i class="icon-pencil"></i> {l s='Edit Column' mod='tdkplatform'}</a></li>
					<li><a href="javascript:void(0)" title="{l s='Delete Column' mod='tdkplatform'}" class="column-action btn-delete "><i class="icon-trash"></i> {l s='Delete Column' mod='tdkplatform'}</a></li>
					<li><a href="javascript:void(0)" title="{l s='Duplicate Group' mod='tdkplatform'}" class="column-action btn-duplicate "><i class="icon-paste"></i> {l s='Duplicate Column' mod='tdkplatform'}</a></li>
					<li><a href="javascript:void(0)" title="{l s='Disable or Enable Column' mod='tdkplatform'}" class="column-action btn-status {if isset($formAtts.active) && !$formAtts.active} deactive{else} active{/if}"><i class="icon-ok"></i> {l s='Disable or Enable Column' mod='tdkplatform'}</a></li>
				</ul>
			</div> 
			<div class="btn-group animation-section">
				<button type="button" class="btn btn-default animation-button">
					<i class="icon-magic"></i>
					<span class="animation-status" data-text-default="{l s='Animation' mod='tdkplatform'}" data-text-infinite="{l s='Infinite' mod='tdkplatform'}">{l s='Animation' mod='tdkplatform'}</span>
				</button>
				<div class="form-horizontal animation-wrapper column-animation-wrapper">
					<div class="form-group">
						<label class="control-label col-lg-5">
							{l s='Select Animation' mod='tdkplatform'}
						</label>
						<div class="col-lg-7">
							<select name="animation" class="animation_select fixed-width-xl">
								{if isset($listAnimation)}
									{foreach $listAnimation as $listAnimation_val}
										<optgroup label="{$listAnimation_val.name}">
											{foreach $listAnimation_val.query as $option}
												<option value="{$option.id}">{$option.name}</option>
											{/foreach}
										</optgroup>
									{/foreach}
								{/if}
							</select>
						</div>
					</div>
					<div class="form-group animate_sub">
						<div class="col-lg-10 col-lg-offset-2">
							<div class="animationSandbox">Prestashop.com</div>								
						</div>
					</div>
					<div class="form-group animate_sub">
						<label class="control-label col-lg-5">
							{l s='Delay' mod='tdkplatform'} ({l s='Default' mod='tdkplatform'}: 1)
						</label>
						<div class="col-lg-7">						
							<div class="input-group fixed-width-xs">
								<input name="animation_delay" value="1" class="fixed-width-xs animation_delay" type="text">
								<span class="input-group-addon">{l s='s' mod='tdkplatform'}</span>							
							</div>						
						</div>
					</div>
					<div class="form-group animate_sub">
						<label class="control-label col-lg-5">
							{l s='Duration' mod='tdkplatform'} ({l s='Default' mod='tdkplatform'}: 1)
						</label>
						<div class="col-lg-7">						
							<div class="input-group fixed-width-xs">
								<input name="animation_duration" value="1" class="fixed-width-xs animation_duration" type="text">
								<span class="input-group-addon">{l s='s' mod='tdkplatform'}</span>							
							</div>						
						</div>
					</div>
					<div class="form-group animate_sub animate_loop">
						<label class="control-label col-lg-5">
							{l s='Iteration count' mod='tdkplatform'} ({l s='Default' mod='tdkplatform'}: 1)
						</label>
						<div class="col-lg-7">						
							<div class="input-group fixed-width-xs">
								<input name="animation_iteration_count" value="1" class="fixed-width-xs animation_iteration_count" type="text">
								<span class="input-group-addon">{l s='times' mod='tdkplatform'}</span>							
							</div>							
						</div>
					</div>
					<div class="form-group animate_sub">
						<div class="col-lg-7 col-lg-offset-5">
							<div class="checkbox">
								<label for="animation_infinite">
									<input name="animation_infinite" class="checkbox-group animation_infinite" value="1" type="checkbox">{l s='Infinite' mod='tdkplatform'}
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-12">
							<button type="button" class="btn btn-primary pull-right btn-save-animation">{l s='Save' mod='tdkplatform'}</button>
							<button type="button" class="btn btn-default pull-right animate-it animate_sub">{l s='Animate demo' mod='tdkplatform'}</button>						
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="column-controll-right pull-right">
			<a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Reduce size' mod='tdkplatform'}" class="column-action btn-change-colwidth" data-value="-1"><i class="icon-minus-sign-alt"></i></a>
			<div class="btn-group">
				<button type="button" class="btn" tabindex="-1" data-toggle="dropdown">
					<span class="width-val tdk-w-6"></span>
				</button>
				<ul class="dropdown-menu dropdown-menu-right">
					{foreach from=$widthList item=itemWidth}
					<li class="col-{$itemWidth|replace:'.':'-'|escape:'html':'UTF-8'}">
						<a class="change-colwidth" data-width="{$itemWidth|replace:'.':'-'|escape:'html':'UTF-8'}" href="javascript:void(0);" tabindex="-1">                                          
							<span data-width="{$itemWidth|replace:'.':'-'|escape:'html':'UTF-8'}" class="width-val tdk-w-{if $itemWidth|strpos:"."|escape:'html':'UTF-8'}{$itemWidth|replace:'.':'-'|escape:'html':'UTF-8'}{else}{$itemWidth|escape:'html':'UTF-8'}{/if}">{$itemWidth|escape:'html':'UTF-8'}/12 - ( {math equation="x/y*100" x=$itemWidth y=12 format="%.2f"} % )</span>
						</a>
					</li>
					{/foreach}
				</ul>
			</div>
			<a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Increase size' mod='tdkplatform'}" class="column-action btn-change-colwidth" data-value="1"><i class="icon-plus-sign-alt"></i></a>
		</div>
		<div class="column-content">
			{if isset($tdkInfo)}{$tdkContent}{* HTML form , no escape necessary *}{/if}
		</div>
		<div class="column-controll-bottom">
			<a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Add new Widget' mod='tdkplatform'}" class="column-action btn-new-widget label-tooltip"><i class="icon-plus-sign"></i></a>
			<a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Edit Column' mod='tdkplatform'}" class="column-action btn-edit label-tooltip" data-type="TdkColumn"><i class="icon-pencil"></i></a>
		</div>
	</div>
</div>