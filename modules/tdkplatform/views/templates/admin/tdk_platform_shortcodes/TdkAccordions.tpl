{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\admin\tdk_platform_shortcodes\TdkAccordions -->
{if !isset($isSubTab)}
<div id="{if !isset($tdkInfo)}default_TdkAccordions{else}{$formAtts.id|escape:'html':'UTF-8'}{/if}" class="widget-row clearfix TdkAccordions{if isset($formAtts)} {$formAtts.form_id|escape:'html':'UTF-8'}{/if}" data-type='TdkAccordions'>
    <div class="float-center-control text-center">
        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Drag to sort accordion' mod='tdkplatform'}" class="accordions-action waction-drag label-tooltip"><i class="icon-move"></i> </a>
        <span>{l s='Widget Accordions' mod='tdkplatform'}</span>
        
        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Edit Accordions' mod='tdkplatform'}" class="accordions-action btn-edit label-tooltip " data-type="TdkAccordions"><i class="icon-edit"></i></a>
        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Delete Accordions' mod='tdkplatform'}" class="accordions-action btn-delete label-tooltip"><i class="icon-trash"></i></a>
        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Duplicate Accordions' mod='tdkplatform'}" class="accordions-action btn-duplicate label-tooltip"><i class="icon-paste"></i></a>
        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Disable or Enable Accordions' mod='tdkplatform'}" class="accordions-action btn-status label-tooltip{if isset($formAtts.active) && !$formAtts.active} deactive{else} active{/if}"><i class="icon-ok"></i></a>
    </div>
    <div class="panel-group" id="{if isset($formAtts.id)}{$formAtts.id|escape:'html':'UTF-8'}{else}accordion{/if}">
        {if !isset($formAtts.form_id)}
        {for $foo=1 to 2}
            <div class="panel panel-default">
                <div class="panel-heading widget-container-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse{$foo|escape:'html':'UTF-8'}">Accordion {$foo|escape:'html':'UTF-8'}</a>
                    </h4>
                </div>
                <div id="collapse{$foo|escape:'html':'UTF-8'}" class="panel-collapse collapse in widget-container-content">
                    <div class="panel-body">
                        <div class="text-center tab-content-control">
                            <span>{l s='Accordion' mod='tdkplatform'}</span>
                            <a href="javascript:void(0)" class="tabcontent-action accordion btn-new-widget label-tooltip" title=""><i class="icon-plus-sign"></i></a>
                            <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Edit Tab' mod='tdkplatform'}" class="tabcontent-action accordions btn-edit label-tooltip" data-type="tdkSubAccordions"><i class="icon-edit"></i></a>
                            <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Delete Tab' mod='tdkplatform'}" class="tabcontent-action accordions btn-delete label-tooltip"><i class="icon-trash"></i></a>
                            <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Duplicate Tab' mod='tdkplatform'}" class="tabcontent-action accordions btn-duplicate label-tooltip"><i class="icon-paste"></i></a>
                        </div>
                        <div class="subwidget-content">

                        </div>
                    </div>
                </div>
            </div>    
        {/for}
        {else}
            {$tdkContent}{* HTML form , no escape necessary *}
        {/if}
            <a href="javascript:void(0)" class="btn-add-accordion"><i class="icon-plus"></i> {l s='Add' mod='tdkplatform'}</a>
    </div>
</div>    
{else}
        <div class="panel panel-default">
            <div class="panel-heading widget-container-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" class="{$formAtts.form_id|escape:'html':'UTF-8'}" data-parent="#{$formAtts.parent_id|escape:'html':'UTF-8'}" href="#{$formAtts.id|escape:'html':'UTF-8'}">{$formAtts.title|escape:'html':'UTF-8'}</a>
                </h4>
            </div>
            <div id="{$formAtts.id|escape:'html':'UTF-8'}" class="panel-collapse collapse widget-wrapper-content widget-container-content">
                <div class="panel-body">
                    <div class="text-center tab-content-control">
                        <span>{l s='Content' mod='tdkplatform'}</span>
                        <a href="javascript:void(0)" class="tabcontent-action accordion btn-new-widget label-tooltip" title=""><i class="icon-plus-sign"></i></a>
                        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Edit Tab' mod='tdkplatform'}" class="tabcontent-action accordions btn-edit label-tooltip" data-type="tdkSubAccordions"><i class="icon-edit"></i></a>
                        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Delete Tab' mod='tdkplatform'}" class="tabcontent-action accordions btn-delete label-tooltip"><i class="icon-trash"></i></a>
                        <a href="javascript:void(0)" data-toggle="tooltip" title="{l s='Duplicate Tab' mod='tdkplatform'}" class="tabcontent-action accordions btn-duplicate label-tooltip"><i class="icon-paste"></i></a>
                    </div>
                    <div class="subwidget-content">
                        {$tdkContent}{* HTML form , no escape necessary *}
                    </div>
                </div>
            </div>
        </div> 
{/if}