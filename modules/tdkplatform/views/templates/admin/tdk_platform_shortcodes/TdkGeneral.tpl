{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\admin\tdk_platform_shortcodes\TdkGeneral -->
<div {if !isset($tdkInfo)}id="default_widget"{/if} class="widget-row clearfix{if isset($tdkInfo)} {$tdkInfo.name|escape:'html':'UTF-8'}{if isset($tdkInfo.icon_class)} widget-icon{/if}{/if}{if isset($formAtts)} {$formAtts.form_id|escape:'html':'UTF-8'}{/if}" {if isset($tdkInfo)}data-type="{$tdkInfo.name|escape:'html':'UTF-8'}"{/if}>
	{if isset($formAtts)}
	<a id="{$formAtts.form_id|escape:'html':'UTF-8'}" name="{$formAtts.form_id|escape:'html':'UTF-8'}"></a>
	{/if}
    <div class="widget-controll-top pull-right">
        <a href="javascript:void(0)" title="{l s='Drag to sort Widget' mod='tdkplatform'}" class="widget-action waction-drag label-tooltip"><i class="icon-move"></i> </a>
        <a href="javascript:void(0)" title="{l s='Disable or Enable Column' mod='tdkplatform'}" class="widget-action btn-status{if isset($formAtts.active) && !$formAtts.active} deactive{else} active{/if} label-tooltip"><i class="{if isset($formAtts.active) && !$formAtts.active}icon-remove{else}icon-ok{/if}"></i></a>
        <a href="javascript:void(0)" title="{l s='Edit Widget' mod='tdkplatform'}" class="widget-action btn-edit label-tooltip" {if isset($tdkInfo)}data-type="{$tdkInfo.name|escape:'html':'UTF-8'}"{/if}><i class="icon-pencil"></i></a>
        <a href="javascript:void(0)" title="{l s='Duplicate Widget' mod='tdkplatform'}" class="widget-action btn-duplicate label-tooltip"><i class="icon-paste"></i></a>
        <a href="javascript:void(0)" title="{l s='Delete Column' mod='tdkplatform'}" class="widget-action btn-delete label-tooltip"><i class="icon-trash"></i></a>
    </div>
    <div class="widget-content">
        <img class="w-img" width="16" src="{$moduleDir|escape:'html':'UTF-8'}tdkplatform/logo.gif" title="{l s='Appolo Widget' mod='tdkplatform'}" alt="{l s='Appolo Widget' mod='tdkplatform'}"/>
        <i class="icon w-icon{if isset($tdkInfo) && isset($tdkInfo.icon_class)} {$tdkInfo.icon_class|escape:'html':'UTF-8'}{/if}"></i>
        <a href="javascript:void(0);" title="" class="widget-title">{if isset($formAtts.title) && $formAtts.title}{$formAtts.title|rtrim|escape:'html':'UTF-8'} - {/if} {if isset($tdkInfo)}{$tdkInfo.label|escape:'html':'UTF-8'}{/if}</a>
    </div>
</div>