{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\admin\tdk_platform_home\home -->
{if isset($errorText) && $errorText}
<div class="error alert alert-danger">
    {$errorText|escape:'html':'UTF-8'}
</div>
{/if}
{if isset($errorSubmit) && $errorSubmit}
<div class="error alert alert-danger">
    {$errorSubmit|escape:'html':'UTF-8'}
</div>
{/if}

<div id="home_wrapper" class="default">
    <div class="position-cover row">
    {include file='./position.tpl'}
    </div>
</div>

<div id="tdk_loading" class="tdk-loading">
    <div class="spinner">
        <div class="cube1"></div>
        <div class="cube2"></div>
    </div>
</div>
{include file="$tplPath/tdk_platform_shortcode/home_form.tpl"}
<script type="text/javascript">
		{addJsDef imgModuleLink=$imgModuleLink}
		{addJsDef tdkAjaxShortCodeUrl=$ajaxShortCodeUrl}
		{addJsDef tdkAjaxHomeUrl=$ajaxHomeUrl}
		{addJsDef tdkImgController=$imgController}
		
    $(document).ready(function(){
        var $tdkHomeBuilder = $(document).tdkPlatform();
        $tdkHomeBuilder.process('{$dataForm}{* HTML form , no escape necessary *}','{$shortcodeInfos}{* HTML form , no escape necessary *}','{$languages}{* HTML form , no escape necessary *}');
        $tdkHomeBuilder.ajaxShortCodeUrl = tdkAjaxShortCodeUrl;
        $tdkHomeBuilder.ajaxHomeUrl = tdkAjaxHomeUrl;
        $tdkHomeBuilder.lang_id = '{$lang_id|escape:'html':'UTF-8'}';
        $tdkHomeBuilder.imgController = tdkImgController;        
    });
</script>