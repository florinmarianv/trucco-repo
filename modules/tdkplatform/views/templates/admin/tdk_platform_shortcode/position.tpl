{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\admin\tdk_platform_home\position -->
<div class="position-area">
	<div class="hook-wrapper tdkshortcode {if isset($data_shortcode_content.tdkshortcode.class)}{$data_shortcode_content.tdkshortcode.class|escape:'html':'UTF-8'}{/if}" data-hook="tdkshortcode">
		<div class="hook-top">
			<div class="pull-left hook-desc"></div>
			<div class="hook-info text-center">
				<a href="javascript:;" tabindex="0" class="open-group label-tooltip" title="{l s='Expand Hook' mod='tdkplatform'}" id="tdkshortcode" name="tdkshortcode">
					{l s='Shortcode Content' mod='tdkplatform'} <i class="icon-circle-arrow-down"></i>
				</a>
			</div>
		</div>
		<div class="hook-content">
			{if isset($data_shortcode_content.tdkshortcode.content)}
			{$data_shortcode_content.tdkshortcode.content}{* HTML form , no escape necessary *}
			{/if}
			<div class="hook-content-footer text-center">
				<a href="javascript:void(0)" tabindex="0" class="btn-new-widget-group" title="{l s='Add Widget in new Group' mod='tdkplatform'}" data-container="body" data-toggle="popover" data-placement="top" data-trigger="focus">
					<i class="icon-plus"></i>
				</a>
			</div>
		</div>
	</div>
</div>
