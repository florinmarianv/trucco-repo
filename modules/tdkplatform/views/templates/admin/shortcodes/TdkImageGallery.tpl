{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="tdk-info" style="display: none;"></div>
{literal}
<script>
$(document).off("click", ".btn-create-folder");
$(document).on("click", ".btn-create-folder", function(){

    var data = "&ajax=1&action=callmethod&type_shortcode=TdkImageGallery&method_name=CreateDir&path=" + $("#path").val();
    var url = $globalthis.ajaxShortCodeUrl;
    var html = '';

    $(".tdk-info").html("");
    $(".tdk-info").removeClass("alert-success alert-danger");
    $(".tdk-info").hide();

    $("#tdk_loading").show();
    $.ajax({
        type: "POST",
        headers: {"cache-control": "no-cache"},
        url: url,
        async: true,
        cache: false,
        data: data,
        dataType: "json",
        success: function (jsonData) {
            $("#tdk_loading").hide();
            if (jsonData.success) {
                if(typeof jsonData.img_dir == "undefined")
                    jsonData.img_dir = "";
                html = jsonData.information + "<p style=\"font-weight: bold;\">" + jsonData.img_dir + "</p>";

                $(".tdk-info").addClass("alert alert-success").html(html).slideDown();
            }
            if (jsonData.hasError) {
                if(typeof jsonData.img_dir == "undefined")
                    jsonData.img_dir = "";
                html = jsonData.error + "<p style=\"font-weight: bold;\">" + jsonData.img_dir + "</p>";

                $(".tdk-info").addClass("alert alert-danger").html(html).slideDown();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#tdk_loading").hide();
        }
    });

});
</script>
{/literal}