{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="form-group">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="{l s='Layout Type' mod='tdkplatform'}">
        {l s='Layout Type' mod='tdkplatform'}
        </span>
      </label>
    <div class="col-lg-9">
      <select id="tdklayout" name="tdklayout" class="custom-select">
          <option value="default">{l s='default' mod='tdkplatform'}</option>
          {foreach $category_layouts as $tdklayout}
          <option value="{$tdklayout['plist_key']}" {if $tdklayout['plist_key'] == $current_layout}selected="selected"{/if}>{$tdklayout['name']}</option>
          {/foreach}
      </select>
      <br/>
      <div class="alert alert-info" role="alert">
        <i class="material-icons">help</i>
        <p class="alert-text">
          1. {l s='Create layout file in TDK Platform > TDK product list builder' mod='tdkplatform'}<br>
          2. {l s='Use code $category.categorylayout to get layout' mod='tdkplatform'}<br>
          3. {l s='Example code download in' mod='tdkplatform'} <a href="#" title="example">{l s='Here' mod='tdkplatform'}</a>
          <br>
        </p>
      </div>
    </div>

    {foreach from=$tdkextras key=tdkextrak item=tdkextrav}
      <div class="form-group">
         <label class="control-label col-lg-3">{$tdkextrak}</label>
         <div class="col-lg-9">
            <div class="form-group">
              {foreach from=$languages item=language}
                  {if $languages|count > 1}
                  <div class="row">
                    <div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
                      <div class="col-lg-9">
                  {/if}
                  {if $tdkextrav == 'varchar(255)'}
                      <input id="{$tdkextrak}_{$language.id_lang|intval}" type="text"  name="{$tdkextrak}_{$language.id_lang|intval}" value="{if $data_fields}{$data_fields[$tdkextrak][$language.id_lang]}{/if}"/>
                  {else}
                      <textarea name="{$tdkextrak}_{$language.id_lang|intval}" rows="2" class="rte autoload_rte">{if $data_fields}{$data_fields[$tdkextrak][$language.id_lang]|escape}{/if}</textarea>
                  {/if}
                  {if $languages|count > 1}
              </div>
              <div class="col-lg-2">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  {$language.iso_code}
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  {foreach from=$languages item=language}
                  <li><a href="javascript:hideOtherLanguage({$language.id_lang});" tabindex="-1">{$language.name}</a></li>
                  {/foreach}
                </ul>
              </div>
            </div>
          </div>
          {/if}
          {/foreach}
            </div>
            <p class="alert-text">Use $category.{$tdkextrak} to get value in category.tpl file</p> 
          </div>
      </div>
    {/foreach}
</div>