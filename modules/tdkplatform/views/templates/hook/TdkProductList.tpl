{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkProductList -->
{if !isset($tdkAjax)}
    <div class="block TdkBlock TdkWidgetProductList {$formAtts.class|escape:'html':'UTF-8'}">
		{($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
		<input type="hidden" class="tdkconfig" value='{$tdkPConfig nofilter}{* HTML form , no escape necessary *}'/>
		{if isset($formAtts.sub_title) && $formAtts.sub_title}
            <div class="TdkGroupTitle">
        {/if}
            {if isset($formAtts.title) && !empty($formAtts.title)}
        		<h4 class="TdkTitleBlock">
        			{$formAtts.title|escape:'html':'UTF-8'}
        		</h4>
    		{/if}
            {if isset($formAtts.sub_title) && $formAtts.sub_title}
                <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
            {/if}
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            </div>
        {/if}
{/if}
{if isset($products) && $products}
    {if !isset($tdkAjax)}
    <!-- Products list -->
    <ul{if isset($id) && $id} id="{$id|intval}"{/if} class="product_list grid row{if isset($class) && $class} {$class|escape:'html':'UTF-8'}{/if} {if isset($productClassWidget)}{$productClassWidget|escape:'html':'UTF-8'}{/if}">
    {/if}
        {foreach from=$products item=product name=products}
            <li class="ajax_block_product{if isset($formAtts.use_animation) && $formAtts.use_animation} has-animation{/if} product_block{if $scolumn_xl == 5} col-xl-2-4 {else} col-xl-{12/$scolumn_xl|intval}{/if}{if $scolumn_lg == 5} col-lg-2-4 {else} col-lg-{12/$scolumn_lg|intval}{/if}{if $scolumn_md == 5} col-md-2-4 {else} col-md-{12/$scolumn_md|intval}{/if}{if $scolumn_sm == 5} col-sm-2-4 {else} col-sm-{12/$scolumn_sm|intval}{/if}{if $scolumn_xs == 5} col-xs-2-4 {else} col-xs-{12/$scolumn_xs|intval}{/if}{if $scolumn_sp == 5} col-sp-2-4 {else} col-sp-{12/$scolumn_sp|intval}{/if} {if $smarty.foreach.products.first}first_item{elseif $smarty.foreach.products.last}last_item{/if}"{if isset($formAtts.use_animation) && $formAtts.use_animation} data-animation="fadeInUp" data-animation-delay="{$smarty.foreach.products.iteration*100}ms" data-animation-duration="2s" data-animation-iteration-count="1"{/if}>
                    {if isset($product_item_path)}
                        {include file="$product_item_path"}
                    {/if}
            </li>
        {/foreach}
    {if !isset($tdkAjax)}
    </ul>
    <!-- End Products list -->
    {if isset($formAtts.use_showmore) && $formAtts.use_showmore && $products|@count >= $formAtts.nb_products}
    <div class="box-show-more open">
        <a href="javascript:void(0)" class="btn btn-default btn-show-more" data-use-animation="{if isset($formAtts.use_animation) && $formAtts.use_animation}1{else}0{/if}" data-page="{$p|intval}" data-loading-text="{l s='Loading...' mod='tdkplatform'}">
            <span>{l s='Show me more' mod='tdkplatform'}</span></a>
    </div>
    {/if}

    </div>
	{($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
    {/if}
	{else}
</div>
{/if}
