{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkFontAwesome -->
{($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
	{if isset($formAtts.sub_title) && $formAtts.sub_title}
        <div class="TdkGroupTitle">
    {/if}
		{if isset($formAtts.title) && !empty($formAtts.title)}
			<h4 class="TdkTitleBlock">
				{$formAtts.title|escape:'html':'UTF-8'}
			</h4>
		{/if}
		{if isset($formAtts.sub_title) && $formAtts.sub_title}
		    <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
		{/if}
	{if isset($formAtts.sub_title) && $formAtts.sub_title}
        </div>
    {/if}
{if isset($formAtts.font_name) && $formAtts.font_name}
	<i class="fa 
		{if isset($formAtts.font_size)}{$formAtts.font_size|escape:'html':'UTF-8'}{/if} 
		{if isset($formAtts.font_type)}{$formAtts.font_type|escape:'html':'UTF-8'}{/if} 
		{if isset($formAtts.is_spin)}{$formAtts.is_spin|escape:'html':'UTF-8'}{/if} 
		{$formAtts.font_name|escape:'html':'UTF-8'}"></i>
{/if}
{($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
