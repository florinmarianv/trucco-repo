{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkImageGalleryProduct -->
<div class="widget col-lg-12 col-md-6 col-sm-6 col-xs-6 col-sp-12">
	{($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
    <!-- {$smallimage|escape:'html':'UTF-8'}     -->
    {if isset($images)}
    <div class="widget-images widgetImageGalleryProduct block">
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            <div class="TdkGroupTitle">
        {/if}
            {if isset($formAtts.title)&&!empty($formAtts.title)}
                <h4 class="TdkTitleBlock">
                    {$formAtts.title|escape:'html':'UTF-8'}
                </h4>
            {/if}
            {if isset($formAtts.sub_title) && $formAtts.sub_title}
                <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
            {/if}
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            </div>
        {/if}
        <div class="TdkBlockContent clearfix">
            <div class="images-list clearfix">    
                <div class="row">
                {foreach from=$images item=image name=images}
                    <div class="image-item {if $columns == 5} col-md-2-4 {else} col-md-{12/$columns|intval}{/if} col-xs-12">
                        <a class="fancybox" rel="tdkgallery{$formAtts.form_id|escape:'html':'UTF-8'}" href= "{$link->getImageLink($image.link_rewrite, $image.id_image, $thickimage)|escape:'html':'UTF-8'}">
                            <img class="replace-2x img-fluid" src="{$link->getImageLink($image.link_rewrite, $image.id_image, $smallimage)|escape:'html':'UTF-8'}" alt=""/>
                    	</a>
                    </div>
                {/foreach}
                </div>
            </div>
        </div>
    </div>
	{($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
    <script type="text/javascript">
        tdk_list_functions.push(function(){
        $(".fancybox").fancybox({
            openEffect : 'none',
            closeEffect : 'none'
        });
    });
    </script>
    {/if} 
</div>