{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
 <!-- @file modules\tdkplatform\views\templates\hook\tdk_product_more_info -->
<div class="product-more-info">
	{if count($product_categories) > 0}
		<div class="product-category">
			<span>{l s='Categories' mod='tdkplatform'}</span>
			<div class="list-info-product">
				{foreach $product_categories as $key => $product_categories_item}
					<a href="{$product_categories_item.category_url}">{$product_categories_item.name}</a>
				{/foreach}
			</div>
		</div>
	{/if}
	
	{if count($product_manufacturer) > 0}
		<div class="product-brand">
			<span>{l s='Brand' mod='tdkplatform'}</span>	
			<div class="list-info-product">	
				<a href="{$product_manufacturer.manufacturer_url}">{$product_manufacturer.manufacturer_name}</a>
			</div>	
		</div>
	{/if}
	
	{if count($product_supplier) > 0}
		<div class="product-supplier">
			<span>{l s='Supplier' mod='tdkplatform'}</span>
			<div class="list-info-product">
				<a href="{$product_supplier.supplier_url}">{$product_supplier.supplier_name}</a>
			</div>	
		</div>
	{/if}
	
	{if isset($product.reference_to_display)}
		<div class="product-reference">
			<span class="label">{l s='Reference' mod='tdkplatform'} </span>
			<div class="list-info-product">
				<span itemprop="sku">{$product.reference_to_display}</span>
			</div>
		</div>
    {/if}
</div>