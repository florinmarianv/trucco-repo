{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\header -->

<script>
    /**
     * List functions will run when document.ready()
     */
    var tdk_list_functions = [];
    /**
     * List functions will run when window.load()
     */
    var tdk_list_functions_loaded = [];

    /**
     * List functions will run when document.ready() for theme
     */
    
    var products_list_functions = [];
</script>


{if isset($ajax_enable) && $ajax_enable}
<script type='text/javascript'>
    var tdkOption = {
        category_qty:{if $category_qty}{$category_qty|escape:'html':'UTF-8'}{else}0{/if},
        product_list_image:{if $product_list_image}{$product_list_image|escape:'html':'UTF-8'}{else}0{/if},
        product_one_img:{if $product_one_img}{$product_one_img|escape:'html':'UTF-8'}{else}0{/if},
        productCdown: {if $productCdown}{$productCdown|escape:'html':'UTF-8'}{else}0{/if},
        productColor: {if $productColor}{$productColor|escape:'html':'UTF-8'}{else}0{/if},
        homeWidth: {if $homeSize}{$homeSize.width|escape:'html':'UTF-8'}{else}0{/if},
        homeheight: {if $homeSize}{$homeSize.height|escape:'html':'UTF-8'}{else}0{/if},
	}

    tdk_list_functions.push(function(){
        if (typeof $.TdkCustomAjax !== "undefined" && $.isFunction($.TdkCustomAjax)) {
            var tdkCustomAjax = new $.TdkCustomAjax();
            tdkCustomAjax.processAjax();
        }
    });
</script>
{/if}