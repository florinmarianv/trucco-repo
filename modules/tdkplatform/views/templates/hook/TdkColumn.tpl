{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkColumn -->
<div{if isset($formAtts.id) && $formAtts.id} id="{$formAtts.id|escape:'html':'UTF-8'}"{/if}
    class="{(isset($formAtts.class)) ? $formAtts.class : ''|escape:'html':'UTF-8'} {(isset($formAtts.animation) && $formAtts.animation != 'none'|escape:'html':'UTF-8') ? 'has-animation' : ''}"
	{if isset($formAtts.animation) && $formAtts.animation != 'none'} data-animation="{$formAtts.animation|escape:'html':'UTF-8'}"
	{if isset($formAtts.animation_delay) && $formAtts.animation_delay != ''} data-animation-delay="{$formAtts.animation_delay|escape:'html':'UTF-8'}" {/if}
	{if isset($formAtts.animation_duration) && $formAtts.animation_duration != ''} data-animation-duration="{$formAtts.animation_duration|escape:'html':'UTF-8'}" {/if}
	{if isset($formAtts.animation_iteration_count) && $formAtts.animation_iteration_count != ''} data-animation-iteration-count="{$formAtts.animation_iteration_count|escape:'html':'UTF-8'}" {/if}
	{if isset($formAtts.animation_infinite) && $formAtts.animation_infinite != ''} data-animation-infinite="{$formAtts.animation_infinite|escape:'html':'UTF-8'}" {/if}
	{/if}
    >
    {if isset($formAtts.sub_title) && $formAtts.sub_title}
        <div class="TdkGroupTitleColumn">
    {/if}
        {if isset($formAtts.title) && $formAtts.title}
            <h4 class="TdkTitleBlock TdkTitleColumn">{$formAtts.title nofilter}{* HTML form , no escape necessary *}</h4>
        {/if}
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            <div class="subTitleWidget TdkSubTitleColumn">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
        {/if}
    {if isset($formAtts.sub_title) && $formAtts.sub_title}
        </div>
    {/if}
    {if isset($formAtts.content_html)}
        {$formAtts.content_html|escape:'html':'UTF-8' nofilter}{* HTML form , no escape necessary *}
    {else}
        {$tdkContent nofilter}{* HTML form , no escape necessary *}
    {/if}
</div>