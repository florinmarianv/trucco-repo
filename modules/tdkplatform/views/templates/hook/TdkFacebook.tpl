{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkFacebook -->
 <div class="TdkWidgetFacebook TdkBlock block">
	{($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
	<div id="fb-root"></div>
    {if isset($formAtts.sub_title) && $formAtts.sub_title}
        <div class="TdkGroupTitle">
    {/if}
        {if isset($formAtts.title) && $formAtts.title}
            <h4 class="TdkTitleBlock">{$formAtts.title|escape:'html':'UTF-8'}</h4>
        {/if}
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
        {/if}
    {if isset($formAtts.sub_title) && $formAtts.sub_title}
        </div>
    {/if}
    {if isset($formAtts.page_url) && $formAtts.page_url}
<script type="text/javascript">
tdk_list_functions.push(function(){
    // Check avoid include duplicate library Facebook SDK
    if($("#facebook-jssdk").length == 0) {
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }
});
</script>
    
    <div class="fb-like-box" data-href="{$formAtts.page_url|escape:'html':'UTF-8'}" 
		{if isset($formAtts.height) && $formAtts.height}data-height={$formAtts.height|escape:'html':'UTF-8'}{/if}
		{if isset($formAtts.width) && $formAtts.width}data-width={$formAtts.width|escape:'html':'UTF-8'}{/if}
    	data-colorscheme="{$formAtts.target|escape:'html':'UTF-8'}" 
    	data-show-faces="{if $formAtts.show_faces}true{else}false{/if}" 
    	data-header="{if $formAtts.show_header}true{else}false{/if}" 
    	data-stream="{if $formAtts.show_stream}true{else}false{/if}" 
    	data-show-border="{if $formAtts.show_border}true{else}false{/if}">
    </div>
    {/if}
	{($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
</div>