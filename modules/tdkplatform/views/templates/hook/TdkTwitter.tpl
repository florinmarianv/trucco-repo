{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
 <!-- @file modules\tdkplatform\views\templates\hook\TdkTwitter -->
{if isset($formAtts.twidget_id) && $formAtts.twidget_id}
{if !isset($formAtts.accordion_type) || $formAtts.accordion_type == 'full'}{* Default : always full *}
<div class="block TdkBlock TdkWidgetTwitter {(isset($formAtts.class)) ? $formAtts.class : ''|escape:'html':'UTF-8'}">
	{($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
    {if isset($formAtts.sub_title) && $formAtts.sub_title}
        <div class="TdkGroupTitle">
    {/if}
        {if isset($formAtts.title) && $formAtts.title}
            <h4 class="TdkTitleBlock">{$formAtts.title|escape:'html':'UTF-8'}</h4>
        {/if}
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
        {/if}
	{if isset($formAtts.sub_title) && $formAtts.sub_title}
        </div>
    {/if}
    <div class="TdkBlockContent">
		<div id="tdk-twitter{$formAtts.twidget_id|escape:'html':'UTF-8'}" class="tdk-twitter">
            <a class="twitter-timeline" href="https://twitter.com/{$formAtts.username|escape:'html':'UTF-8'}"
                data-width="{$formAtts.width|intval}"
                data-height="{$formAtts.height|intval}"
            	data-dnt="true"
            	data-widget-id="{$formAtts.twidget_id|escape:'html':'UTF-8'}"
            	{if isset($formAtts.link_color) && $formAtts.link_color}data-link-color="{$formAtts.link_color|escape:'html':'UTF-8'}"{/if}
            	{if isset($formAtts.border_color) && $formAtts.border_color}data-border-color="{$formAtts.border_color|escape:'html':'UTF-8'}"{/if}
            	{if isset($formAtts.count) && $formAtts.count}data-tweet-limit="{$formAtts.count|intval}"{/if}
            	data-show-replies="{if isset($formAtts.show_replies) && $formAtts.show_replies}true{else}false{/if}"
            	data-chrome="{if isset($formAtts.transparent) && !$formAtts.transparent} transparent{/if} 
            				{if isset($formAtts.show_scrollbar) && !$formAtts.show_scrollbar} noscrollbar{/if}
            				{if isset($formAtts.show_border) && !$formAtts.show_border} noborders{/if}
            				{if isset($formAtts.show_header) && !$formAtts.show_header} noheader{/if}
            				{if isset($formAtts.show_footer) && !$formAtts.show_footer} nofooter{/if}"
        	>{l s='Tweets by' mod='tdkplatform'} {$formAtts.username|escape:'html':'UTF-8'}</a>
		</div>
	</div>
	{($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
</div>
{elseif isset($formAtts.accordion_type) && ($formAtts.accordion_type == 'accordion' || $formAtts.accordion_type == 'accordion_small_screen')}{* Case : full or accordion*}
    
<div class="block TdkBlock TdkWidgetTwitter block-toggler {(isset($formAtts.class)) ? $formAtts.class : ''|escape:'html':'UTF-8'}{if $formAtts.accordion_type == 'accordion_small_screen'} accordion_small_screen{/if}">
	{($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
    {if isset($formAtts.title) && $formAtts.title}
    <div class="title clearfix" data-target="#tdk-twitter{$formAtts.twidget_id|escape:'html':'UTF-8'}" data-toggle="collapse">
        <h4 class="TdkTitleBlock">{$formAtts.title|escape:'html':'UTF-8'}</h4>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
    </div>
    {/if}
    {if isset($formAtts.sub_title) && $formAtts.sub_title}
        <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
    {/if}
	
    <div class="TdkBlockContent">
		<div id="tdk-twitter{$formAtts.twidget_id|escape:'html':'UTF-8'}" class="tdk-twitter collapse">
            <a class="twitter-timeline" href="https://twitter.com/{$formAtts.username|escape:'html':'UTF-8'}"
                data-width="{$formAtts.width|intval}"
                data-height="{$formAtts.height|intval}"
            	data-dnt="true"
            	data-widget-id="{$formAtts.twidget_id|escape:'html':'UTF-8'}"
            	{if isset($formAtts.link_color) && $formAtts.link_color}data-link-color="{$formAtts.link_color|escape:'html':'UTF-8'}"{/if}
            	{if isset($formAtts.border_color) && $formAtts.border_color}data-border-color="{$formAtts.border_color|escape:'html':'UTF-8'}"{/if}
            	{if isset($formAtts.count) && $formAtts.count}data-tweet-limit="{$formAtts.count|intval}"{/if}
            	data-show-replies="{if isset($formAtts.show_replies) && $formAtts.show_replies}true{else}false{/if}"
            	data-chrome="{if isset($formAtts.transparent) && !$formAtts.transparent} transparent{/if} 
            				{if isset($formAtts.show_scrollbar) && !$formAtts.show_scrollbar} noscrollbar{/if}
            				{if isset($formAtts.show_border) && !$formAtts.show_border} noborders{/if}
            				{if isset($formAtts.show_header) && !$formAtts.show_header} noheader{/if}
            				{if isset($formAtts.show_footer) && !$formAtts.show_footer} nofooter{/if}"
        	>{l s='Tweets by' mod='tdkplatform'} {$formAtts.username|escape:'html':'UTF-8'}</a>
		</div>
	</div>
	{($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
</div>
{/if}
<script>
	tdk_list_functions.push(function(){
        // Check avoid include duplicate library Facebook SDK
        if($("#twitter-wjs").length == 0) {
            window.twttr = (function (d, s, id) {
                var t, js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id; js.src= "https://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);
                return window.twttr || (t = { _e: [], ready: function (f) { t._e.push(f) } });
            }(document, "script", "twitter-wjs"));
        }
    
		var tdk_flag_twitter_set_css = 1;
		$('#tdk-twitter{$formAtts.twidget_id}').bind("DOMSubtreeModified", function() {
			tdk_flag_twitter_set_css++;
			
			var isRun = 10;
			
                        {*var is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
                        var is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
                        var is_firefox = navigator.userAgent.indexOf('Firefox') > -1;*}
                        var is_safari = navigator.userAgent.indexOf("Safari") > -1;
                        {*var is_opera = navigator.userAgent.toLowerCase().indexOf("op") > -1;
                        if ((is_chrome)&&(is_safari)) {is_safari=false;}
                        if ((is_chrome)&&(is_opera)) {is_chrome=false;}                        *}
			if(window.chrome || is_safari){
				isRun = 5;
			}
            
			if(tdk_flag_twitter_set_css == isRun)
			{
				// Run only one time
				
				$('#tdk-twitter{$formAtts.twidget_id} iframe').ready(function() {

					{if (isset($formAtts.border_color) && $formAtts.border_color) && isset($formAtts.show_border) && $formAtts.show_border }
						// SHOW BORDER COLOR
						$('#tdk-twitter{$formAtts.twidget_id} iframe').css('border', '1px solid {$formAtts.border_color}');
					{/if}
					{if (isset($formAtts.name_color) && $formAtts.name_color) }
						$('#tdk-twitter{$formAtts.twidget_id} iframe').contents().find('.TweetAuthor-name.Identity-name.customisable-highlight').css('color', '{$formAtts.name_color}');
					{/if}
					{if (isset($formAtts.mail_color) && $formAtts.mail_color) }
						$('#tdk-twitter{$formAtts.twidget_id} iframe').contents().find('.TweetAuthor-screenName.Identity-screenName').css('color', '{$formAtts.mail_color}');
					{/if}

					var head = jQuery("#tdk-twitter{$formAtts.twidget_id} iframe").contents().find("head");

					var css = {literal}'<style type="text/css">\n\{/literal}
					{if (isset($formAtts.text_color) && $formAtts.text_color) }\n\
						body { color : {$formAtts.text_color} ; }\n\
					{/if}\n\
					{if (isset($formAtts.link_color) && $formAtts.link_color) }\n\
						.timeline-Tweet-text a { color : {$formAtts.link_color} ; }\n\
					{/if}\n\
							</style>';
					$(head).append(css);
				});
			
			}
		});	
	});
</script>
{/if}