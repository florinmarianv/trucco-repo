{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkLiveEdit -->
{if isset($isLive)}
	{if isset($isEnd)}
	</div>
	{else}
	<div class="cover-live-edit">
		{if isset($formAtts) && isset($formAtts.form_id) && $formAtts.form_id}
		<a class='link-to-back-end' href="{$urlEditProfile}{*full link can not escape*}#{$formAtts.form_id|escape:'html':'UTF-8'}" target="_blank">
		{else}
		<a class='link-to-back-end' href="{$urlEditProfile}{*full link can not escape*}" target="_blank">
		{/if}
			<i class="icon-pencil"></i> <span>{l s='Edit' mod='tdkplatform'}</span>
		</a>
	{/if}
{/if}