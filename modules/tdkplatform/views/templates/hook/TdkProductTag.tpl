{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkProductTag.tpl -->
{if !isset($formAtts.accordion_type) || $formAtts.accordion_type == 'full'}{* Default : always full *}
    <div {if isset($formAtts.form_id) && $formAtts.form_id} id="{$formAtts.form_id|escape:'html':'UTF-8' nofilter}"{/if}
        {if isset($formAtts.class)} class="block TdkBlock TdkWidgetProductTag {$formAtts.class|escape:'html':'UTF-8'}"{/if}>
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            <div class="TdkGroupTitle">
        {/if}
            {if isset($formAtts.title) && $formAtts.title}
                <h4 class="TdkTitleBlock">{$formAtts.title|escape:'html':'UTF-8' nofilter}{* HTML form , no escape necessary *}</h4>
            {/if}
            {if isset($formAtts.sub_title) && $formAtts.sub_title}
                <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
            {/if}
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            </div>
        {/if}
        <span class="TdkBlockContent">
            {if isset($formAtts.tags) &&  $formAtts.tags}
                {foreach from=$formAtts.tags item=tag name=myLoop}
                    <a href="{$tdklink->getPageLink('search', true, NULL, "tag={$tag.name|urlencode}")|escape:'html'}" title="{l s='More about' mod='tdkplatform'} {$tag.name|escape:html:'UTF-8'}" class="{$tag.class} {if $smarty.foreach.myLoop.last}last_item{elseif $smarty.foreach.myLoop.first}first_item{else}item{/if}">{$tag.name|escape:html:'UTF-8'}</a>
                {/foreach}
            {else}
                {l s='No tags have been specified yet.' mod='tdkplatform'}
            {/if}
        </span>
    </div>
{elseif isset($formAtts.accordion_type) && ($formAtts.accordion_type == 'accordion' || $formAtts.accordion_type == 'accordion_small_screen')}{* Case : full or accordion*}
    <div {if isset($formAtts.form_id) && $formAtts.form_id} id="{$formAtts.form_id|escape:'html':'UTF-8' nofilter}"{/if}
        class="{if isset($formAtts.class)}block block-toggler {$formAtts.class|escape:'html':'UTF-8'}{/if}{if $formAtts.accordion_type == 'accordion_small_screen'} accordion_small_screen{/if}">
        {if isset($formAtts.title) && $formAtts.title}
            <div class="title clearfix" data-target="#TdkProductTag_{$formAtts.form_id|escape:'html':'UTF-8'}" data-toggle="collapse">
                <h4 class="TdkTitleBlock">
                    {$formAtts.title|escape:'html':'UTF-8' nofilter}{* HTML form , no escape necessary *}
                </h4>
                <span class="float-xs-right">
                  <span class="navbar-toggler collapse-icons">
                    <i class="material-icons add">&#xE313;</i>
                    <i class="material-icons remove">&#xE316;</i>
                  </span>
                </span>
            </div>
        {/if}
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
        {/if}
        <div class="TdkBlockContent">
            {if isset($formAtts.tags) &&  $formAtts.tags}
                <ul class="collapse" id="TdkProductTag_{$formAtts.form_id|escape:'html':'UTF-8'}">
                {foreach from=$formAtts.tags item=tag name=myLoop}
                    <li><a href="{$tdklink->getPageLink('search', true, NULL, "tag={$tag.name|urlencode}")|escape:'html'}" title="{l s='More about' mod='tdkplatform'} {$tag.name|escape:html:'UTF-8'}" class="{$tag.class} {if $smarty.foreach.myLoop.last}last_item{elseif $smarty.foreach.myLoop.first}first_item{else}item{/if}">{$tag.name|escape:html:'UTF-8'}</a></li>
                {/foreach}
                </ul>
            {else}
                {l s='No tags have been specified yet.' mod='tdkplatform'}
            {/if}
        </div>
    </div> 
{/if}