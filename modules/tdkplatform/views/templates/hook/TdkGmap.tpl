{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkGmap -->
{if ($page_name != 'stores' || $formAtts.stores == 1) && ($page_name != 'sitemap' || $formAtts.sitemap == 1)}
<div id="google-maps-{$formAtts.form_id|escape:'html':'UTF-8'}" class="block TdkBlock TdkWidgetGmap">
	{($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
    {if isset($formAtts.sub_title) && $formAtts.sub_title}
        <div class="TdkGroupTitle">
    {/if}
        {if isset($formAtts.title) && !empty($formAtts.title)}
            <h4 class="TdkTitleBlock">
            	{$formAtts.title|escape:'html':'UTF-8'}
            </h4>
        {/if}
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
        {/if}
    {if isset($formAtts.sub_title) && $formAtts.sub_title}
        </div>
    {/if}
    <div class="gmap-cover {if $hasListStore}display-list-store{else}not-display-list-store{/if}" style="width: 100%; 
     height:{if isset($formAtts.height) && $formAtts.height}{$formAtts.height}{else}100%;{/if}; clear:both;">
    	{if $hasListStore}
    	<div class="gmap-content col-lg-9 col-md-8 col-sm-8 col-xs-6">
    	{else}
    	<div class="gmap-content">
    	{/if}
            <div id="map-canvas-{$formAtts.form_id|escape:'html':'UTF-8'}" class="gmap" style="min-width:100px; min-height:100px;
            	width:{if isset($formAtts.width) && $formAtts.width}{$formAtts.width|escape:'html':'UTF-8'}{else}100%;{/if}; 
            	height:{if isset($formAtts.height) && $formAtts.height}{$formAtts.height|escape:'html':'UTF-8'}{else}100%;{/if};"></div>
    	</div>
		{if $hasListStore}
    	<div class="gmap-stores-content col-lg-3 col-md-4 col-sm-4 col-xs-6" style="height: 100%">
    		<div id="gmap-stores-list-{$formAtts.form_id|escape:'html':'UTF-8'}">
        		<ul></ul>
        	</div>
    	</div>
    	{/if}
    </div>
	{($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
    
	<script type="text/javascript">
		tdk_list_functions.push(function(){
			$('<script>')
			.attr('type', 'text/javascript')
			.attr('src', 'https://maps.googleapis.com/maps/api/js?key={if isset($formAtts.gkey) && $formAtts.gkey}{$formAtts.gkey}{/if}&callback=initTdkMap')
			.appendTo('head');
		});
	</script>
	
    <script type="text/javascript">
        
        var tdkGMap = {$tdkGMap nofilter};{* HTML form , no escape necessary *}
		var marker_list_{$formAtts.form_id} = {$marker_list nofilter};{* HTML form , no escape necessary *}
		var marker_center = {$marker_center nofilter};{* HTML form , no escape necessary *}
    	var markers_{$formAtts.form_id|escape:'html':'UTF-8'} = [];

    	function displayAMarker(data, obj, id) {
    		var m = markers_{$formAtts.form_id|escape:'html':'UTF-8'}[id];
    		google.maps.event.trigger(m, 'click');
    	}
    	function initializeListStore(data, name) {
    		var obj = $("#" + name + " ul");
    		synSize(name);
    		for(var i = 0; i < data.length; i++) {
    			var s = data[i];
    			obj.append("<li class='item-gmap-store' marker-id='" + i + "'" 
    					+ "onclick='return displayAMarker(marker_list_{$formAtts.form_id|escape:'html':'UTF-8'}, this, " + i + ");'>"
    					+ "<strong><b><span class='icon-map-marker'></span> "
    					+ s.name + "</b></strong><br/><text>" + s.address + "</text>");
    		}
    	}
        function initTdkMap(){
            initializeGmap('',
                    marker_list_{$formAtts.form_id|escape:'html':'UTF-8'}, 
                    markers_{$formAtts.form_id|escape:'html':'UTF-8'}, 
                    "map-canvas-{$formAtts.form_id|escape:'html':'UTF-8'}", 
                    {$formAtts.zoom|escape:'html':'UTF-8'});

            if("{$hasListStore|escape:'html':'UTF-8'}".length > 0) {
                initializeListStore(
                        marker_list_{$formAtts.form_id|escape:'html':'UTF-8'}, 
                        "gmap-stores-list-{$formAtts.form_id|escape:'html':'UTF-8'}");
            }

        }

    // CODE HERE not write in *.js, compatility with Chrome
    function initializeGmap(map, data, markers, nameGmap, zoom)
    {
        map = new google.maps.Map(document.getElementById(nameGmap), {
            center: new google.maps.LatLng(marker_center.latitude, marker_center.longitude),
            zoom: zoom,
            mapTypeId: 'roadmap'
        });

        if(data.length>0)
        {
            setTimeout(createMarkers(map, markers, data), 1500);
        }
        else
        {
            markers[0] = new google.maps.Marker({
                position: new google.maps.LatLng(marker_center.latitude, marker_center.longitude),
                animation: google.maps.Animation.DROP,
                map: map,
            });
        }
    };

    function createMarkers(map, markers, data) {
        // dataMarkers
        for (var i = 0; i < data.length; i++) {
            var obj = data[i];
            var lg = parseFloat(obj.longitude);
            var lt = parseFloat(obj.latitude);
            var name = obj.name;
            var address = obj.address;
            var other = obj.other;
            var id_store = obj.id_store;
            var has_store_picture = obj.has_store_picture;

            var latlng = new google.maps.LatLng(lt, lg);
            var html = "<div style='min-width:200px;'><b>" + name + "</b><br/>" + address;
            html += (has_store_picture ? "<br /><br /><p><img src='" + tdkGMap.img_store_dir + parseInt(id_store) + ".jpg' alt='' /></p>" : "");
            html += other + "<a href='http://maps.google.com/maps?saddr=&daddr=" + latlng + "' target='_blank'>" + tdkGMap.translation_5 +"<\/a>";
            html += "</div>";

            var infowindow = new google.maps.InfoWindow({
                content: "loading..."
            });

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lt, lg),
                animation: google.maps.Animation.DROP,
                map: map,
                icon: tdkGMap.img_ps_dir + tdkGMap.logo_store,
                title: obj.name,
                html: html
            });

            google.maps.event.addListener(marker, "click", function () {
                infowindow.setContent(this.html);
                infowindow.open(map, this);
            });
            markers[i] = marker;
        }
    }
	</script>
</div>
{/if}