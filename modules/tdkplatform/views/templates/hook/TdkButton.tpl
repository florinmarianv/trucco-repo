{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkButton -->
{($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
{if isset($formAtts.sub_title) && $formAtts.sub_title}
    <div class="TdkGroupTitle">
{/if}
	{if isset($formAtts.title) && !empty($formAtts.title)}
		<h4 class="TdkTitleBlock">
			{$formAtts.title|escape:'html':'UTF-8'}
		</h4>
	{/if}
	{if isset($formAtts.sub_title) && $formAtts.sub_title}
	    <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
	{/if}
{if isset($formAtts.sub_title) && $formAtts.sub_title}
    </div>
{/if}
{if isset($formAtts.name) && $formAtts.name}
	{if isset($formAtts.url) && $formAtts.url}
	<a href="{$formAtts.url}{*full link can not escape*}" {if isset($formAtts.is_blank) && $formAtts.is_blank}target="_blank"{/if}>
	{/if}
		<span class="btn {(isset($formAtts.class)) ? $formAtts.class : ''|escape:'html':'UTF-8'} {$formAtts.button_type|escape:'html':'UTF-8'} {$formAtts.button_size|escape:'html':'UTF-8'} {if isset($formAtts.is_block) && $formAtts.is_block} btn-block{/if}">{$formAtts.name|escape:'html':'UTF-8'}</span>
	{if isset($formAtts.url) && $formAtts.url}
	</a>
	{/if}
{/if}
{($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
