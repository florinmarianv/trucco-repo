{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\TdkGeneral -->
<div{if isset($formAtts.id) && $formAtts.id} id="{$formAtts.id|escape:'html':'UTF-8' nofilter}{* HTML form , no escape necessary *}"{/if}
    {if isset($formAtts.class)} class="block TdkBlock {$formAtts.class|escape:'html':'UTF-8'}"{/if}>
	{($tdkLiveEdit)?$tdkLiveEdit:'' nofilter}{* HTML form , no escape necessary *}
    {if isset($formAtts.sub_title) && $formAtts.sub_title}
        <div class="TdkGroupTitle">
    {/if}
        {if isset($formAtts.title) && $formAtts.title}
            <h4 class="TdkTitleBlock">{$formAtts.title|escape:'html':'UTF-8' nofilter}{* HTML form , no escape necessary *}</h4>
        {/if}
        {if isset($formAtts.sub_title) && $formAtts.sub_title}
            <div class="subTitleWidget">{$formAtts.sub_title nofilter}{* HTML form , no escape necessary *}</div>
        {/if}
    {if isset($formAtts.sub_title) && $formAtts.sub_title}
        </div>
    {/if}
    {if isset($formAtts.content_html)}
        {$formAtts.content_html nofilter}{* HTML form , no escape necessary *}
    {else}
        {$tdkContent nofilter}{* HTML form , no escape necessary *}
    {/if}
	{($tdkLiveEditEnd)?$tdkLiveEditEnd:'' nofilter}{* HTML form , no escape necessary *}
</div>