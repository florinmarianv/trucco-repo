{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<!-- @file modules\tdkplatform\views\templates\hook\BlogItem -->
<div class="blog-container TdkBlogContainer" itemscope itemtype="https://schema.org/Blog">
    <div class="blogPostImage">
        <div class="blogImage">
            <a class="blogLinkImage" href="{$blog.link|escape:'html':'UTF-8'}" title="{$blog.title|escape:'html':'UTF-8'}" itemprop="url">
				{if isset($formAtts.btdkblogs_sima) && $formAtts.btdkblogs_sima}
					<img class="img-fluid" src="{if (isset($blog.preview_thumb_url) && $blog.preview_thumb_url != '')}{$blog.preview_thumb_url}{else}{$blog.preview_url}{/if}{*full url can not escape*}" 
						alt="{if !empty($blog.legend)}{$blog.legend|escape:'html':'UTF-8'}{else}{$blog.title|escape:'html':'UTF-8'}{/if}" 
						title="{if !empty($blog.legend)}{$blog.legend|escape:'html':'UTF-8'}{else}{$blog.title|escape:'html':'UTF-8'}{/if}" 
						{if isset($formAtts.btdkblogs_width)}width="{$formAtts.btdkblogs_width|escape:'html':'UTF-8'}" {/if}
						{if isset($formAtts.btdkblogs_height)} height="{$formAtts.btdkblogs_height|escape:'html':'UTF-8'}"{/if}
						itemprop="image" />
				{/if}
            </a>
        </div>
    </div>
    <div class="blogPostContent">
        {if isset($formAtts.show_title) && $formAtts.show_title}
        	<h5 class="blogTitle" itemprop="name"><a href="{$blog.link}{*full url can not escape*}" title="{$blog.title|escape:'html':'UTF-8'}">{$blog.title|strip_tags:'UTF-8'|truncate:80:'...'}</a></h5>
        {/if}
        <div class="blogMeta">
			{if isset($formAtts.btdkblogs_saut) && $formAtts.btdkblogs_saut}
				<span class="blogAuthor">
					<span class="icon-author">{l s='Author' mod='tdkplatform'}:</span> {if $blog.author_name != ''}{$blog.author_name|escape:'html':'UTF-8'}{else}{$blog.author|escape:'html':'UTF-8'}{/if}
				</span>
			{/if}		
			{if isset($formAtts.btdkblogs_scat) && $formAtts.btdkblogs_scat}
				<span class="blogCat"> 
					<span class="icon-list">{l s='In' mod='tdkplatform'}</span> 
					<a href="{$blog.category_link}{*full url can not escape*}" title="{$blog.category_title|escape:'html':'UTF-8'}">{$blog.category_title|escape:'html':'UTF-8'}</a>
				</span>
			{/if}
			{if isset($formAtts.btdkblogs_scre) && $formAtts.btdkblogs_scre}
				<span class="blogCalendar">
					<span class="icon-calendar"> {l s='On' mod='tdkplatform'} </span> 
					<time class="date" datetime="{strtotime($blog.date_add)|date_format:"%Y"}{*convert to date time*}">		
						{assign var='blog_date' value=strtotime($blog.date_add)|date_format:"%A"}
						{l s=$blog_date mod='tdkplatform'},	<!-- day of week -->
						{assign var='blog_month' value=strtotime($blog.date_add)|date_format:"%B"}
						{l s=$blog_month mod='tdkplatform'}
						{assign var='blog_date_add' value=strtotime($blog.date_add)|date_format:"%d"}<!-- day of month -->
						{assign var='blog_day' value=strtotime($blog.date_add)|date_format:"%e"}
						{l s=$blog_day mod='tdkplatform'}
						{assign var='blog_daycount' value=$formAtts.tdk_blog_helper->string_ordinal($blog_date_add)}
						{l s=$blog_daycount mod='tdkplatform'},
						{assign var='blog_year' value=strtotime($blog.date_add)|date_format:"%Y"}						
						{l s=$blog_year mod='tdkplatform'}	<!-- year -->
					</time>
				</span>
			{/if}
			{if isset($formAtts.btdkblogs_scoun) && $formAtts.btdkblogs_scoun && isset($blog.comment_count)}
				<span class="blogNbcomment">
					<span class="icon-comment"> {l s='Comment' mod='tdkplatform'}:</span> {$blog.comment_count|intval}
				</span>
			{/if}
			
			{if isset($formAtts.btdkblogs_shits) && $formAtts.btdkblogs_shits}
				<span class="blogHits">
					<span class="icon-hits"> {l s='Hits' mod='tdkplatform'}:</span> {$blog.hits|intval}
				</span>	
			{/if}
		</div>
		{if isset($formAtts.show_desc) && $formAtts.show_desc}
	        <p class="blogDesc" itemprop="description">
	            {$blog.description|strip_tags:'UTF-8'|truncate:160:'...'}
	        </p>
        {/if}
    </div>
	
	<div class="hidden-xl-down hidden-xl-up datetime-translate">
		{l s='Sunday' mod='tdkplatform'}
		{l s='Monday' mod='tdkplatform'}
		{l s='Tuesday' mod='tdkplatform'}
		{l s='Wednesday' mod='tdkplatform'}
		{l s='Thursday' mod='tdkplatform'}
		{l s='Friday' mod='tdkplatform'}
		{l s='Saturday' mod='tdkplatform'}
		
		{l s='January' mod='tdkplatform'}
		{l s='February' mod='tdkplatform'}
		{l s='March' mod='tdkplatform'}
		{l s='April' mod='tdkplatform'}
		{l s='May' mod='tdkplatform'}
		{l s='June' mod='tdkplatform'}
		{l s='July' mod='tdkplatform'}
		{l s='August' mod='tdkplatform'}
		{l s='September' mod='tdkplatform'}
		{l s='October' mod='tdkplatform'}
		{l s='November' mod='tdkplatform'}
		{l s='December' mod='tdkplatform'}
		
		{l s='st' mod='tdkplatform'}
		{l s='nd' mod='tdkplatform'}
		{l s='rd' mod='tdkplatform'}
		{l s='th' mod='tdkplatform'}
	</div>
</div>
