{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{if $product}
	<ul class="deal-clock TdkDealClock lof-clock-{$product.id_product|intval}-detail list-inline">
		{if isset($product.js) && $product.js == 'unlimited'}<div class="lof-labelexpired">{l s='Unlimited' mod='tdkplatform'}</div>{/if}
	</ul>
	{if isset($product.js) && $product.js != 'unlimited'}
		<script type="text/javascript">
			{literal}
			jQuery(document).ready(function($){{/literal}
				var text_d = '{l s='days' mod='tdkplatform'}';
				var text_h = '{l s='hours' mod='tdkplatform'}';
				var text_m = '{l s='min' mod='tdkplatform'}';
				var text_s = '{l s='sec' mod='tdkplatform'}';
				var text_title = '{l s='Ends in' mod='tdkplatform'}';
				$(".lof-clock-{$product.id_product|intval}-detail").lofCountDown({literal}{{/literal}
					TargetDate:"{$product.js.month|escape:'html':'UTF-8'}/{$product.js.day|escape:'html':'UTF-8'}/{$product.js.year|escape:'html':'UTF-8'} {$product.js.hour|escape:'html':'UTF-8'}:{$product.js.minute|escape:'html':'UTF-8'}:{$product.js.seconds|escape:'html':'UTF-8'}",
					DisplayFormat:'<li class="tdkTitle">'+text_title+'</li><li class="tdkDay">%%D%%<span>'+text_d+'</span></li><li class="tdkHours">%%H%%<span>'+text_h+'</span></li><li class="tdkMonth">%%M%%<span>'+text_m+'</span></li><li class="tdkSeconds">%%S%%<span>'+text_s+'</span></li>',
					FinishMessage: "{$product.finish|escape:'html':'UTF-8'}"
				{literal}
				});
			});
			{/literal}
		</script>
	{/if}
{/if}