{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}
<div class="col-md-12">
  <div class="row">
    <div class="col-md-12">
      <h2>{l s='Layout Type' mod='tdkplatform'}</h2>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 form-group">
      <select id="tdklayout" name="tdklayout" class="form-control select2-hidden-accessible" data-toggle="select2" tabindex="-1" aria-hidden="true">
      	<option value="default">{l s='Global Layout' mod='tdkplatform'}</option>
      	{foreach $product_layouts as $tdklayout}
        <option value="{$tdklayout.plist_key}" {if $tdklayout.plist_key == $current_layout}selected="selected"{/if}>{$tdklayout.name}</option>
        {/foreach}
      </select>
      <br>
      <br>
      <div class="alert alert-info" role="alert">
        <i class="material-icons">help</i>
        <p class="alert-text">
          1. {l s='Please select layout to show product' mod='tdkplatform'}<br/>
          2. {l s='Then you can use variable' mod='tdkplatform'}<br/>
          $product.productLayout<br/>
          3. {l s='To select product layout in file product.tpl' mod='tdkplatform'}<br/>
          <br>
        </p>
      </div>
    </div>
  </div>
	
  {foreach from=$tdkextras key=tdkextrak item=tdkextrav}
  <div class="row">
    <div class="col-md-12 form-group">
      <fieldset class="form-group">
        <label>{$tdkextrak}</label>
        <div class="translations tabbable" id="form_tab_hooks_meta_title">
          <div class="translationsFields tab-content">
            {foreach from=$languages item=language}
              <div class="translationsFields-form_tab_hooks_{$tdkextrak}_{$language.id_lang} tab-pane translation-field translation-label-{$language.iso_code} {if $language.id_lang == $default_language.id_lang}show active{/if}">
                {if $tdkextrav == 'varchar(255)'}
                <input type="text" id="form_tab_hooks_{$tdkextrak}_{$language.id_lang}" name="tdk_pro_extra[{$tdkextrak}][{$language.id_lang}]" class="form-control" value="{$data_fields[$tdkextrak][$language.id_lang]}"/>
                {else}
                <textarea name="tdk_pro_extra[{$tdkextrak}][{$language.id_lang}]" rows="2" class="rte autoload_rte">{$data_fields[$tdkextrak][$language.id_lang]|escape}</textarea>
                {/if}
              </div>
            {/foreach}
          </div>
        </div>
        <p class="alert-text">Use $product.{$tdkextrak} to get value in product.tpl file</p> 
      </fieldset>
    </div>
  </div>  
  {/foreach}
</div>
