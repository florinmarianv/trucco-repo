<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
include_once(dirname(__FILE__).'/tdkplatform.php');

$module = TdkPlatform::getInstance();

//TDK:: get product link for demo multi product detail
if (Tools::getValue('action') == 'get-list-shortcode') {
    $result = '';
    $result = $module->getListShortCodeForEditor();
    die($result);
}
