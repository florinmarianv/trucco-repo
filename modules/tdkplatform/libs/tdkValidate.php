<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}


class Tdkblog
{
    
}

class TdkSliderLayer
{
    
}

class TdkSlideShow
{
    
}

class TdkLessParser
{
    
}

class Tdkmegamenu
{
    
}

class ProductListingPresenter
{
    
}

class ImageRetriever
{
    
}

class PriceFormatter
{
    
}

class ProductColorsRetriever
{
    
}

class TdkBlogHelper
{
    public static function getInstance()
    {
    }
}

class TdkBlogConfig
{
    public static function getInstance()
    {
    }
}


