<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class AdminTdkPlatformController extends ModuleAdminControllerCore
{
    public static $shortcode_lang;
    public static $lang_id;
    public static $language;
    public $error_text = '';
    public $theme_name;

    public function __construct()
    {
        $url = 'index.php?controller=adminmodules&configure=tdkplatform&token='.Tools::getAdminTokenLite('AdminModules')
                .'&tab_module=Home&module_name=tdkplatform';
        Tools::redirectAdmin($url);
        $this->bootstrap = true;
        $this->className = 'Configuration';
        $this->table = 'configuration';
        $this->theme_name = _THEME_NAME_;
        parent::__construct();
    }
}
