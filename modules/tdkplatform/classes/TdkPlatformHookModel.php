<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

require_once(_PS_MODULE_DIR_.'tdkplatform/classes/TdkPlatformProfilesModel.php');

class TdkPlatformHookModel
{
    public $profile_data;
    public $profile_param;
    public $hook;

    public function create()
    {
        $this->profile_data = TdkPlatformProfilesModel::getActiveProfile('index');
        $this->profile_param = Tools::jsonDecode($this->profile_data['params'], true);
        $this->fullwidth_index_hook = $this->fullwidthIndexHook();
        $this->fullwidth_other_hook = $this->fullwidthOtherHook();
        return $this;
    }

    public function fullwidthIndexHook()
    {
        return isset($this->profile_param['fullwidth_index_hook']) ? $this->profile_param['fullwidth_index_hook'] : TdkPlatformSetting::getIndexHook(3);
    }

    public function fullwidthOtherHook()
    {
        return isset($this->profile_param['fullwidth_other_hook']) ? $this->profile_param['fullwidth_other_hook'] : TdkPlatformSetting::getOtherHook(3);
    }

    public function fullwidthHook($hook_name, $page)
    {
        if ($page == 'index') {
            // validate module
            return isset($this->fullwidth_index_hook[$hook_name]) ? $this->fullwidth_index_hook[$hook_name] : 0;
        } else {
            # other page
            return isset($this->fullwidth_other_hook[$hook_name]) ? $this->fullwidth_other_hook[$hook_name] : 0;
        }
    }
}
