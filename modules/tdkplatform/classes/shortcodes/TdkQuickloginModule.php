<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class TdkQuickloginModule extends TdkShortCodeBase
{
    public $name = 'TdkQuickloginModule';
    public $for_module = 'manage';

    public function getInfo()
    {
        return array('label' => $this->l('Quicklogin Module'), 'position' => 3, 'desc' => $this->l('You set quick login or social login form from tdkquicklogin module'),
            'icon_class' => 'icon icon-chevron-right', 'tag' => 'content');
    }

    public function getConfigList()
    {
        if (Module::isInstalled('tdkquicklogin') && Module::isEnabled('tdkquicklogin')) {
            include_once(_PS_MODULE_DIR_.'tdkquicklogin/tdkquicklogin.php');
            $module = new Tdkquicklogin();
            $select_list_type = array();

            foreach ($module->list_type as $key => $value) {
                    $select_list_type[] = array('id' => $key, 'name' => $value);
            }

            $select_list_layout = array();

            foreach ($module->list_layout as $key => $value) {
                    $select_list_layout[] = array('id' => $key, 'name' => $value);
            }
            $inputs = array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Select type'),
                    'name' => 'quicklogin_type',
                    'options' => array(
                        'query' => $select_list_type,
                        'id' => 'id',
                        'name' => 'name'
                    ),

                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select layout'),
                    'name' => 'quicklogin_layout',
                    'options' => array(
                        'query' => $select_list_layout,
                        'id' => 'id',
                        'name' => 'name'
                    ),

                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Show Social Login'),
                    'name' => 'quicklogin_sociallogin',
                    'options' => array(
                        'query' => array(
                            array(
                                    'id' => 'enable',
                                    'name' => $this->l('Yes'),
                            ),
                            array(
                                    'id' => 'disable',
                                    'name' => $this->l('No'),
                        )),
                        'id' => 'id',
                        'name' => 'name'
                    ),
                ),
            );
        } else {
            $inputs = array(
                array(
                    'type' => 'html',
                    'name' => 'default_html',
                    'html_content' => '<div class="alert alert-warning">'.
                    $this->l('"Tdkquicklogin module" Module must be installed and enabled before using.').
                    '</div><br/><h4><center>You can take this module at TDK Studio</center></h4>'
                )
            );
        }
        return $inputs;
    }

    public function prepareFontContent($assign, $module = null)
    {
        if (Module::isInstalled('tdkquicklogin') && Module::isEnabled('tdkquicklogin')) {
            $assign['formAtts']['isEnabled'] = true;
            include_once(_PS_MODULE_DIR_.'tdkquicklogin/tdkquicklogin.php');
            $module = new Tdkquicklogin();
            
            $assign['content_quicklogin'] = $module->processHookCallBack($assign['formAtts']['quicklogin_type'], $assign['formAtts']['quicklogin_layout'], $assign['formAtts']['quicklogin_sociallogin']);
        } else {
            // validate module
            $assign['formAtts']['isEnabled'] = false;
            $assign['formAtts']['lib_has_error'] = true;
            $assign['formAtts']['lib_error'] = 'Can not show Tdkquicklogin via TdkPlatform. Please enable Tdkquicklogin module.';
        }
        return $assign;
    }
}
