{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

{if isset($action)}
    {if $action == 'printStar'}
        <span rel="{$id_gformrequest|intval}" class="giconstar {if isset($star) && $star} gstarred {/if} label-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="{if isset($star) && $star}{l s='Starred' mod='gformbuilderpro'}{else}{l s='Not starred' mod='gformbuilderpro'}{/if}" data-notstar="{l s='Not starred' mod='gformbuilderpro'}" data-iststar="{l s='Starred' mod='gformbuilderpro'}"  ><i class="icon-star"></i></span>
    {elseif $action == 'printShortcode'}
        <input type="text" value="{$shortcode|escape:'html':'UTF-8'}" />
    {elseif $action == 'printSubject'}
        <span class="gform_subject {if !isset($viewed) || $viewed != 1}gunread{/if}">{$subject|escape:'html':'UTF-8'}</span>
    {elseif $action == 'printFrontlink'}
        <a href="{$url_rewrite|escape:'html':'UTF-8'}" target="_blank" class="btn btn-default"><i class="icon-external-link"></i></a>
    {elseif $action == 'exportFormsToXml'}   
        {if $type == 'form'} 
        {literal}
        <?xml version="1.0" encoding="UTF-8"?>
            <gforms>
        {/literal}
        {else}
        <datafields>
        {/if}
                {if isset($gforms) && $gforms}
                    {foreach $gforms as $key=>$gform}
                        {if $type == 'form'} 
                        <gform id="{$key|intval}">
                        {else}
                        <field id="{$key|intval}">
                        {/if}
                            {foreach $fields as $_key=>$field}
                                {if isset($gform[$_key]) && isset($field.lang) && $field.lang == 1}
                                    {foreach $gform[$_key] as $idlang=>$data_lang}
                                        {if isset($langs_iso[$idlang])}
                                            {if isset($field.validate) && $field.validate == 'isCleanHtml'}
                                                <{$_key|escape:'html':'UTF-8'} lang="{$langs_iso[$idlang]|escape:'html':'UTF-8'}">{if $data_lang !=''}<![CDATA[{$data_lang nofilter}]]>{else}{/if}</{$_key|escape:'html':'UTF-8'}>{* Html content. No need escape.*}
                                            {else}
                                                <{$_key|escape:'html':'UTF-8'}  lang="{$langs_iso[$idlang]|escape:'html':'UTF-8'}">{if $data_lang !=''}<![CDATA[{$data_lang|escape:'html':'UTF-8'}]]>{else}{/if}</{$_key|escape:'html':'UTF-8'}>
                                            {/if}
                                        {/if}
                                    {/foreach}
                                {else}
                                    {if isset($field.validate) && $field.validate == 'isCleanHtml'}
                                        <{$_key|escape:'html':'UTF-8'}>{if isset($gform[$_key]) && $gform[$_key] !=''}<![CDATA[{$gform[$_key] nofilter}]]>{else}{/if}</{$_key|escape:'html':'UTF-8'}>{* Html content. No need escape.*}
                                    {else}
                                        <{$_key|escape:'html':'UTF-8'}>{if isset($gform[$_key]) && $gform[$_key] !=''}{$gform[$_key]|escape:'html':'UTF-8'}{else}{/if}</{$_key|escape:'html':'UTF-8'}>
                                    {/if}
                                {/if}
                            {/foreach}
                            {if isset($type) && $type == 'form' && isset($gform['datafields']) && $gform['datafields'] !=''}
                                {$gform['datafields'] nofilter}{* xml content. No need escape.*}
                            {/if}
                        {if $type == 'form'} 
                        </gform>
                        {else}
                        </field>
                        {/if}
                    {/foreach}
                {/if}
            {if $type == 'form'} 
            </gforms>
            {else}
            </datafields>
            {/if}
    {elseif $action == 'import_export_form'}
        <div class="gimport_export_form">
            <div class="col-lg-6" style="display:none;">
                <form action="" method="POST" name="gexport_form">
                    <input type="hidden" name="submitGexport_form" value="1"/>
                    <input name="gid_forms" type="hidden" value="" id="gid_forms" autocomplete="off" />
                    <div class="export_warrning alert alert-danger" role="alert">
                        <p class="alert-text">{l s='You must select at least one element to export.' mod='gformbuilderpro'}</p>
                    </div>
                </form>
            </div>
            <div class="col-lg-6">
                <div  class="panel">
                    <div class="panel-heading"><i class="icon-upload"></i> {l s='Import Forms' mod='gformbuilderpro'}</div>
                    
                    <form action="" method="POST" name="gimport_form" enctype="multipart/form-data" >
                        <input type="hidden" name="submitGimport_form" value="1"/>
                        <div class="form-group">
                    		<label class="control-label col-lg-3">{l s='File' mod='gformbuilderpro'}</label>
                            <div class="col-lg-9">
                                <div class="form-group">
                                	<div class="col-sm-12">
                                		<input id="file" type="file" name="zipfile" class="hide" />
                                		<div class="dummyfile input-group">
                                			<span class="input-group-addon"><i class="icon-file"></i></span>
                                			<input id="file-name" type="text" name="zipfile" readonly="" />
                                			<span class="input-group-btn">
                                				<button id="file-selectbutton" type="button" name="submitAddGForm" class="btn btn-default">
                                					<i class="icon-folder-open"></i> {l s='Add file' mod='gformbuilderpro'}
                                                </button>
                                            </span>
                                		</div>
                                	</div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">				
							<label class="control-label col-lg-3">{l s='Delete old form' mod='gformbuilderpro'}</label>
                            <div class="col-lg-9">
                                <span class="switch prestashop-switch fixed-width-lg">
							        <input type="radio" name="delete_old_form" id="delete_old_form_on" value="1"  />
    								<label for="delete_old_form_on">{l s='Yes' mod='gformbuilderpro'}</label>
    								<input type="radio" name="delete_old_form" id="delete_old_form_off" value="0" checked="checked" />
    								<label for="delete_old_form_off">{l s='No' mod='gformbuilderpro'}</label>
    								<a class="slide-button btn"></a>
    							</span>
                            </div>
				        </div>
                        <div class="form-group">				
							<label class="control-label col-lg-3">{l s='Override old form' mod='gformbuilderpro'}</label>
                            <div class="col-lg-9">
                                <span class="switch prestashop-switch fixed-width-lg">
							        <input type="radio" name="override_old_form" id="override_old_form_on" value="1"  />
    								<label for="override_old_form_on">{l s='Yes' mod='gformbuilderpro'}</label>
    								<input type="radio" name="override_old_form" id="override_old_form_off" value="0" checked="checked" />
    								<label for="override_old_form_off">{l s='No' mod='gformbuilderpro'}</label>
    								<a class="slide-button btn"></a>
    							</span>
                            </div>
				        </div>
                        {literal}
                        <script type="text/javascript">
                        	$(document).ready(function(){
                        		$('#file-selectbutton').click(function(e) {
                        			$('#file').trigger('click');
                        		});
                        		$('#file-name').click(function(e) {
                        			$('#file').trigger('click');
                        		});
                        		$('#file-name').on('dragenter', function(e) {
                        			e.stopPropagation();
                        			e.preventDefault();
                        		});
                        		$('#file-name').on('dragover', function(e) {
                        			e.stopPropagation();
                        			e.preventDefault();
                        		});
                        		$('#file-name').on('drop', function(e) {
                        			e.preventDefault();
                        			var files = e.originalEvent.dataTransfer.files;
                        			$('#file')[0].files = files;
                        			$(this).val(files[0].name);
                        		});
                        		$('#file').change(function(e) {
                        			if ($(this)[0].files !== undefined)
                        			{
                        				var files = $(this)[0].files;
                        				var name  = '';
                        
                        				$.each(files, function(index, value) {
                        					name += value.name+', ';
                        				});
                        
                        				$('#file-name').val(name.slice(0, -2));
                        			}
                        			else // Internet Explorer 9 Compatibility
                        			{
                        				var name = $(this).val().split(/[\\/]/);
                        				$('#file-name').val(name[name.length-1]);
                        			}
                        		});
                        	});
                        </script>
                        {/literal}
                        <div style="clear:both;"></div>
                        <div class="panel-footer">
                            <button type="submit" value="1" id="gformbuilderpro_form_import" class="btn btn-default pull-right">
    							<i class="process-icon-upload"></i> {l s='Import' mod='gformbuilderpro'}
    						</button>
                        </div>
                    </form>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    {elseif $action == 'printRequest'}
        {if isset($showrequest) && $showrequest}
            <a href="#gformrequest_quickview_{$id_gformrequest|intval}" rel="{$id_gformrequest|intval}" class="btn btn-default gformrequest_quickview"><i class="icon-eye"></i></a>
            <div style="display:none;">
                <div id="gformrequest_quickview_{$id_gformrequest|intval}" class="gformrequest_quickview_box bootstrap">
                    <div class="form-group">
                		<label class="control-label col-lg-12">#{$id_gformrequest|intval}: {$subject|escape:'html':'UTF-8'}</label>
                	</div><div style="clear:both;"></div><hr />
                    {$request nofilter}{* $request is html content, no need to escape*}
                    <div class="panel-footer">
            			<a href="{$link_request|escape:'html':'UTF-8'}#replyform" class="btn btn-primary pull-right" target="_blank"><i class="icon-reply"></i>{l s='Reply' mod='gformbuilderpro'}</a>
                    </div>
                </div>
            </div>
        {/if}
    {/if}
{/if}