<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

include_once('../../config/config.inc.php');
include_once('../../init.php');
require_once(_PS_MODULE_DIR_.'tdkmegamenu/tdkmegamenu.php');
$context = Context::getContext();
$module = new Tdkmegamenu();
$id_shop = Tools::getValue('id_shop') ? Tools::getValue('id_shop') : 0;
echo $module->renderwidget($id_shop);
die;
