{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<div class="navbar navbar-default">
	<nav id="mainmenutop" class="megamenu" role="navigation">
		<div class="navbar-header">
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				{$tree}
			</div>
		</div>
	</nav>
</div>
