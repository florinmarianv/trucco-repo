{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{$html}
{if $successful == 1}
	<div class="bootstrap">
		<div class="alert alert-success megamenu-alert">
			<button data-dismiss="alert" class="close" type="button">×</button>
			{l s='Successfully' mod='tdkmegamenu'}
		</div>
	</div>
{/if}
<div class="col-lg-12"> 
	<div class="" style="float: right">
		<div class="pull-right">
			<a href="{$live_editor_url}" class="btn btn-danger">{l s='Live Edit Tools' mod='tdkmegamenu'}</a>
               {l s='To Make Rich Content For Megamenu' mod='tdkmegamenu'}
		</div>
	</div>
</div>
 
<div class="tab-content row">
	<div class="tab-pane active" id="megamenu">
	
		<div class="col-md-4">
			<div class="panel panel-default">
				<h3 class="panel-title">{l s='Group: ' mod='tdkmegamenu'}{$current_group_title}{l s=' - Type: ' mod='tdkmegamenu'}{$current_group_type}</h3>
				<div class="panel-content">{l s='To sort orders or update parent-child, you drap and drop expected menu.' mod='tdkmegamenu'}
					<hr>
					<p>
						<input type="button" value="{l s='New Menu Item' mod='tdkmegamenu'}" id="addcategory" data-loading-text="{l s='Processing ...' mod='tdkmegamenu'}" class="btn btn-danger" name="addcategory">
						<a href="{$admin_widget_link}" class="tdk-modal-action btn btn-modeal btn-success btn-info">{l s='List Widget' mod='tdkmegamenu'}</a>
					</p>
					
					<hr>										
					{$tree}
				</div>
			</div>
		</div>
		<div class="col-md-8">
			{$helper_form}
		</div>
		<script type="text/javascript">
			var addnew ="{$addnew}"; 
			var action="{$action}";
			$("#content").PavMegaMenuList({
				action:action,
				addnew:addnew
			});
		</script>
	</div>
</div>
<script>
	$('#myTab a[href="#profile"]').tab('show');
</script>