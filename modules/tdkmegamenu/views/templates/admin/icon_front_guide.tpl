{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{l s='Check list of icons and class name in here ' mod='tdkmegamenu'}
	<a href="https://design.google.com/icons/" target="_blank">https://design.google.com/icons/</a>
{l s=' Material Font or ' mod='tdkmegamenu'}
	<a href="http://fontawesome.io/icons/" target="_blank">http://fontawesome.io/icons/</a>
{l s=' Awesome Font or your icon class... ' mod='tdkmegamenu'}
	<hr/>
{l s='Special example (Required) for Material Font by Google' mod='tdkmegamenu'}
    <br/>
&lt;i class="material-icons"&gt;&amp;#xE855;&lt;/i&gt;
	<br/>
{l s='OR' mod='tdkmegamenu'}
	<br/>
&lt;i class="material-icons"&gt;alarm&lt;/i&gt;
	<hr/>
{l s='Example for other fonts (Awesome Font...)' mod='tdkmegamenu'}
	<br/>
&lt;i class="fa fa-arrows"&gt;&lt;/i&gt;
	<br/>
{l s='OR' mod='tdkmegamenu'}
	<br/>
fa fa-arrows