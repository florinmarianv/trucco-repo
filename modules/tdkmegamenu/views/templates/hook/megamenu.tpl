{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{if $group_type && $group_type == 'horizontal'}
	<nav data-megamenu-id="{$megamenu_id}" class="tdk-megamenu cavas_menu navbar navbar-default {if $show_cavas && $show_cavas == 1}enable-canvas{else}disable-canvas{/if} {if $group_class && $group_class != ''}{$group_class}{/if}" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="icon-show-menu navbar-toggler hidden-lg-up" data-toggle="collapse" data-target=".megamenu-off-canvas-{$megamenu_id}">
					<span class="sr-only">{l s='Toggle navigation' mod='tdkmegamenu'}</span>
		
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					
				</button>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			{*
			<div id="tdk-top-menu" class="collapse navbar-collapse navbar-ex1-collapse">{$boostrapmenu|escape:'html':'UTF-8'}</div>
			*}
			<div class="tdk-top-menu collapse navbar-toggleable-md megamenu-off-canvas megamenu-off-canvas-{$megamenu_id}">{$boostrapmenu|escape:'html':'UTF-8' nofilter}{* HTML form , no escape necessary *}</div>
	</nav>
	<script type="text/javascript">{literal}
	// <![CDATA[				
			// var type="horizontal";
			// checkActiveLink();
			// checkTarget();
			list_menu_tmp.id = {/literal}{$megamenu_id}{literal};
			list_menu_tmp.type = 'horizontal';
	// ]]>
	{/literal}
	{if $show_cavas && $show_cavas == 1}
		{literal}						
				// offCanvas();
				// var show_cavas = 1;
				// console.log('testaaa');
				// console.log(show_cavas);
				list_menu_tmp.show_cavas =1;
			
		{/literal}
		{else}
		{literal}
				// var show_cavas = 0;
				list_menu_tmp.show_cavas =0;	
		{/literal}
	{/if}
		{literal}
		list_menu_tmp.list_tab = list_tab;
		list_menu.push(list_menu_tmp);
		list_menu_tmp = {};	
		list_tab = {};
		{/literal}
	</script>
{else}
	<div data-megamenu-id="{$megamenu_id}" class="tdk-verticalmenu {if $group_class && $group_class != ''}{$group_class}{/if}">
		<h4 class="title_block verticalmenu-button">
			<span class="icon-show-menu">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</span>
			<span class="title-verticalmenu">{$group_title}</span>
		</h4>
		
		<div class="box-content block_content{if $group_type == 'vertical' && $type_sub == 'drilldown'} is-drilldown{/if}">
			<div class="verticalmenu" role="navigation">{$boostrapmenu|escape:'html':'UTF-8' nofilter}{* HTML form , no escape necessary *}</div>
		</div>
	</div>
	<script type="text/javascript">
		{literal}
			// var type="vertical";	
			
			list_menu_tmp.id = {/literal}{$megamenu_id}{literal};			
			list_menu_tmp.type = 'vertical';
			list_menu_tmp.list_tab = list_tab;
			list_menu.push(list_menu_tmp);
			list_menu_tmp = {};
			list_tab = {};
		{/literal}		
	</script>
	
	
{/if}
