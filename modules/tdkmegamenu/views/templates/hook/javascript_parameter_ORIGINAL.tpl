{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<script type="text/javascript">
	{literal}
	var FancyboxI18nClose = "{/literal}{l s='Close' mod='tdkmegamenu'}{literal}";
	var FancyboxI18nNext = "{/literal}{l s='Next' mod='tdkmegamenu'}{literal}";
	var FancyboxI18nPrev = "{/literal}{l s='Previous' mod='tdkmegamenu'}{literal}";
	var current_link = "{/literal}{$current_link}{literal}";		
	var currentURL = window.location;
	currentURL = String(currentURL);
	currentURL = currentURL.replace("https://","").replace("http://","").replace("www.","").replace( /#\w*/, "" );
	current_link = current_link.replace("https://","").replace("http://","").replace("www.","");
	var text_warning_select_txt = "{/literal}{l s='Please select One to remove?' mod='tdkmegamenu'}";{literal}
	var text_confirm_remove_txt = "{/literal}{l s='Are you sure to remove footer row?' mod='tdkmegamenu'}";{literal}
	var close_bt_txt = "{/literal}{l s='Close' mod='tdkmegamenu'}";{literal}
	var list_menu = [];
	var list_menu_tmp = {};
	var list_tab = [];
	var isHomeMenu = 0;
	{/literal}
</script>