{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<!-- {$smallimage}	 -->
{if isset($images)}
<div class="widget-images block">
	{if isset($widget_heading)&&!empty($widget_heading)}
	<h4 class="title_block">
		{$widget_heading}
	</h4>
	{/if}
	<div class="block_content clearfix">
			<div class="images-list clearfix">	
			<div class="row">
				{foreach from=$images item=image name=images}
					<div class="image-item {if $columns == 5} col-md-2-4 {else} col-md-{12/$columns}{/if} col-xs-12">
						<a class="fancybox" rel="tdkgallery{$id_tdkmegamenu_widgets}" href= "{$link->getImageLink($image.link_rewrite, $image.id_image, $thickimage)}">
							<img class="replace-2x img-fluid" src="{$link->getImageLink($image.link_rewrite, $image.id_image, $smallimage)}" alt=""/>
						</a>
					</div>
				{/foreach}
			</div>
		</div>
	</div>
</div>
{/if} 
