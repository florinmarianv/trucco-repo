{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can not resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{if isset($images) && $images}
<div class="widget-images">
	{if isset($widget_heading)&&!empty($widget_heading)}
	<div class="menu-title">
		{$widget_heading}
	</div>
	{/if}
	<div class="widget-inner clearfix">
		<div class="images-list clearfix">	
			<div>
			{foreach from=$images item=image name=images}
				
				{if $smarty.foreach.images.index == 0 || $smarty.foreach.images.index % $columns == 0}
					<div class="row">
				{/if}
					<div class="image-item {if 12 % $columns == 0}col-md-{12/$columns}{/if} col-xs-12">
						<img class="replace-2x img-fluid" src="{$image}" alt=""/>
					</div>
				{if $smarty.foreach.images.index % $columns == $columns-1 || $smarty.foreach.images.last}
					</div>
				{/if}
			{/foreach}
			</div>
		</div>
	</div>
</div>
{/if} 