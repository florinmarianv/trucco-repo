<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

$path = dirname(_PS_ADMIN_DIR_);

include_once($path.'/config/config.inc.php');
include_once($path.'/init.php');


$res = (bool)Db::getInstance()->execute('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkmegamenu` (
      `id_tdkmegamenu` int(11) NOT NULL AUTO_INCREMENT,
      `id_group` int(11) NOT NULL,
      `image` varchar(255) NOT NULL,
      `id_parent` int(11) NOT NULL,
      `sub_with` varchar(255) NOT NULL,
      `is_group` tinyint(1) NOT NULL,
      `width` varchar(255) DEFAULT NULL,
      `submenu_width` varchar(255) DEFAULT NULL,
      `submenu_colum_width` varchar(255) DEFAULT NULL,
      `item` varchar(255) DEFAULT NULL,
      `item_parameter` varchar(255) DEFAULT NULL,
      `colums` varchar(255) DEFAULT NULL,
      `type` varchar(255) NOT NULL,
      `is_content` tinyint(1) NOT NULL,
      `show_title` tinyint(1) NOT NULL,
      `level_depth` smallint(6) NOT NULL,
      `active` tinyint(1) NOT NULL,
      `position` int(11) NOT NULL,
      `submenu_content` text NOT NULL,
      `show_sub` tinyint(1) NOT NULL,
      `target` varchar(25) DEFAULT NULL,
	  `show_pro_num` tinyint(1) DEFAULT NULL,
      `privacy` smallint(6) DEFAULT NULL,
      `position_type` varchar(25) DEFAULT NULL,
      `menu_class` varchar(255) DEFAULT NULL,
      `content` text,
      `icon_class` varchar(255) DEFAULT NULL,
      `level` int(11) NOT NULL,
      `left` int(11) NOT NULL,
      `right` int(11) NOT NULL,
      `submenu_catids` text,
      `is_cattree` tinyint(1) DEFAULT \'1\',
      `date_add` datetime DEFAULT NULL,
      `date_upd` datetime DEFAULT NULL,
      `groupBox` varchar(255) DEFAULT "all",
      `params_widget` LONGTEXT,
      PRIMARY KEY (`id_tdkmegamenu`)
    ) ENGINE='._MYSQL_ENGINE_.'  DEFAULT CHARSET=utf8;
');
$res &= (bool)Db::getInstance()->execute('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkmegamenu_lang` (
      `id_tdkmegamenu` int(11) NOT NULL,
      `id_lang` int(11) NOT NULL,
      `title` varchar(255) DEFAULT NULL,
      `text` varchar(255) DEFAULT NULL,
        `url` varchar(255) DEFAULT NULL,
      `description` text,
      `content_text` text,
      `submenu_content_text` text,
      PRIMARY KEY (`id_tdkmegamenu`,`id_lang`)
    ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;
');

$res &= (bool)Db::getInstance()->execute('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkmegamenu_shop` (
      `id_tdkmegamenu` int(11) NOT NULL DEFAULT \'0\',
      `id_shop` int(11) NOT NULL DEFAULT \'0\',
      PRIMARY KEY (`id_tdkmegamenu`,`id_shop`)
    ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;
');

$res &= (bool)Db::getInstance()->execute('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkmegamenu_widgets`(
      `id_tdkmegamenu_widgets` int(11) NOT NULL AUTO_INCREMENT,
      `name` varchar(250) NOT NULL,
      `type` varchar(250) NOT NULL,
      `params` LONGTEXT,
      `id_shop` int(11) unsigned NOT NULL,
      `key_widget` int(11)  NOT NULL,
      PRIMARY KEY (`id_tdkmegamenu_widgets`,`id_shop`)
    ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;
');

$res &= (bool)Db::getInstance()->execute('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkmegamenu_group`(
        `id_tdkmegamenu_group` int(11) NOT NULL AUTO_INCREMENT,
        `id_shop` int(10) unsigned NOT NULL,
        `hook` varchar(64) DEFAULT NULL,
        `position` int(11) NOT NULL,
        `active` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
        `params` text NOT NULL,
        
        `active_tdk` tinyint(1) DEFAULT NULL,
        `randkey` varchar(255) DEFAULT NULL,
        `form_id` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`id_tdkmegamenu_group`,`id_shop`)
    ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;
');

$res &= (bool)Db::getInstance()->execute('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tdkmegamenu_group_lang` (
      `id_tdkmegamenu_group` int(11) NOT NULL,
      `id_lang` int(11) NOT NULL,
      `title` varchar(255) DEFAULT NULL,
      `title_fo` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`id_tdkmegamenu_group`,`id_lang`)
    ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;
');

// $res &= (bool)Db::getInstance()->execute('
    // INSERT INTO `'._DB_PREFIX_.'tdkmegamenu`(`id_group`,`image`,`id_parent`,`is_group`,`colums`) VALUES(0,\'\', 0, 1, 1)
// ');

// $rows = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT id_tdkmegamenu FROM `'._DB_PREFIX_.'tdkmegamenu`');

// if (count($rows) <= 0) {
    // # validate module : not use this in this file
    // $languages = Language::getLanguages(false);
    // foreach ($languages as $lang) {
        // $res &= (bool)Db::getInstance()->execute('
            // INSERT INTO `'._DB_PREFIX_.'tdkmegamenu_lang`(`id_tdkmegamenu`,`id_lang`,`title`) VALUES(1, '.(int)$lang['id_lang'].', \'Root\')
        // ');
    // }
    // # validate module : not use this in this file
    // $context = Context::getContext();
    // $res &= (bool)Db::getInstance()->execute('
        // INSERT INTO `'._DB_PREFIX_.'tdkmegamenu_shop`(`id_tdkmegamenu`,`id_shop`) VALUES( 1, '.(int)$context->shop->id.' )
    // ');
// }

/* install sample data */
$rows = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT id_tdkmegamenu FROM `'._DB_PREFIX_.'tdkmegamenu`');
if (count($rows) <= 0 && file_exists(_PS_MODULE_DIR_.'tdkmegamenu/install/sample.php')) {
    # validate module
    include_once(_PS_MODULE_DIR_.'tdkmegamenu/install/sample.php');
}
/* END REQUIRED */
