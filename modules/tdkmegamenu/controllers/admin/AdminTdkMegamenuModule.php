<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class AdminTdkMegamenuModuleController extends ModuleAdminControllerCore
{

    public function __construct()
    {
        parent::__construct();
        if (!Tools::getValue('exportgroup') && !Tools::getValue('exportwidgets')) {
            if (Configuration::get('TDKMEGAMENU_GROUP_DE') && Configuration::get('TDKMEGAMENU_GROUP_DE') != '') {
                $url = 'index.php?controller=AdminModules&configure=tdkmegamenu&editgroup=1&id_group='.Configuration::get('TDKMEGAMENU_GROUP_DE').'&tab_module=front_office_features&module_name=tdkmegamenu&token='.Tools::getAdminTokenLite('AdminModules');
            } else {
                $url = 'index.php?controller=AdminModules&configure=tdkmegamenu&tab_module=front_office_features&module_name=tdkmegamenu&token='.Tools::getAdminTokenLite('AdminModules');
            }
            Tools::redirectAdmin($url);
        }
    }
    
    public function postProcess()
    {
        //TDK:: export group process
        if (Tools::getValue('exportgroup')) {
            $languages = Language::getLanguages();
            $group = TdkmegamenuGroup::getGroupByID(Tools::getValue('id_group'));
            $obj_group = new TdkmegamenuGroup(Tools::getValue('id_group'));
            foreach ($languages as $lang) {
                # module validation
                $group['title'][$lang['iso_code']] = $obj_group->title[$lang['id_lang']];
                $group['title_fo'][$lang['iso_code']] = $obj_group->title_fo[$lang['id_lang']];
            }
            //TDK:: add list menu of group
            $menus = $this->getMenusByGroup(Tools::getValue('id_group'));
            $language_field = array('title', 'text', 'url', 'description', 'content_text', 'submenu_content_text');
           
            $lang_list = array();
            foreach ($languages as $lang) {
                # module validation
                $lang_list[$lang['id_lang']] = $lang['iso_code'];
            }
            
            $list_widgets = '';
            
            foreach ($menus as $menus_item) {
                if (Tools::getValue('widgets')) {
                    if ($menus_item['params_widget'] != '') {
                        $list_widgets .= $this->module->base64Decode($menus_item['params_widget']);
                    }
                } else {
                    $menus_item['params_widget'] = '';
                }
                foreach ($menus_item as $key => $value) {
                    if ($key == 'id_lang') {
                        $curent_lang = $lang_list[$value];
                        continue;
                    }
                    if (in_array($key, $language_field)) {
                        $group['list_menu'][$menus_item['id_tdkmegamenu']][$key][$curent_lang] = $value;
                    } else {
                        # module validation
                        $group['list_menu'][$menus_item['id_tdkmegamenu']][$key] = $value;
                    }
                }
            }
            
            if (Tools::getValue('widgets')) {
                $widget_include = 'with_widgets';
            } else {
                // $group['params_widget'] = '';
                $widget_include = 'without_widgets';
            }
            //TDK:: add list menu of group
            $group['list_widget'] = array();
            if ($list_widgets != '' && Tools::getValue('widgets')) {
                // $group_widget = $this->module->base64Decode($group['params_widget']);
                $model = new TdkWidget();
                $widget_shop = $model->getWidgets();
                if (count($widget_shop) > 0) {
                    foreach ($widget_shop as $key => $widget_shop_item) {
                        if (strpos($list_widgets, 'wid-'.$widget_shop_item['key_widget']) !== false) {
                            $params_widget = $this->module->base64Decode($widget_shop_item['params']);
                            foreach ($languages as $lang) {
                                # module validation
                                if (strpos($params_widget, '_'.$lang['id_lang'].'"') !== false) {
                                    $params_widget = str_replace('_'.$lang['id_lang'].'"', '_'.$lang['iso_code'].'"', $params_widget);
                                }
                            }
                            $widget_shop_item['params'] = $this->module->base64Encode($params_widget);
                            $group['list_widget'][] = $widget_shop_item;
                        }
                    }
                }
            }
            header('Content-Type: plain/text');
            header('Content-Disposition: Attachment; filename=export_megamenu_group_'.Tools::getValue('id_group').'_'.$widget_include.'_'.time().'.txt');
            header('Pragma: no-cache');
            die($this->module->base64Encode(Tools::jsonEncode($group)));
        }
        
        //TDK:: export widgets process
        if (Tools::getValue('exportwidgets')) {
            $languages = Language::getLanguages();
            $model = new TdkWidget();
            $widget_shop = $model->getWidgets();
            
            $widgets = array();
            if (count($widget_shop) > 0) {
                foreach ($widget_shop as $key => $widget_shop_item) {
                    $params_widget = $this->module->base64Decode($widget_shop_item['params']);
                    foreach ($languages as $lang) {
                        # module validation
                        if (strpos($params_widget, '_'.$lang['id_lang'].'"') !== false) {
                            $params_widget = str_replace('_'.$lang['id_lang'].'"', '_'.$lang['iso_code'].'"', $params_widget);
                        }
                    }
                    
                    $widget_shop_item['params'] = $this->module->base64Encode($params_widget);
                    $widgets[] = $widget_shop_item;
                }
            }
            
            header('Content-Type: plain/text');
            header('Content-Disposition: Attachment; filename=export_widgets_'.time().'.txt');
            header('Pragma: no-cache');
            die($this->module->base64Encode(Tools::jsonEncode($widgets)));
        }

        parent::postProcess();
    }
    
    //TDK:: get all data of menu by group
    public function getMenusByGroup($id_group)
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                    SELECT btm.*, btml.*
                    FROM '._DB_PREFIX_.'tdkmegamenu btm
                    LEFT JOIN '._DB_PREFIX_.'tdkmegamenu_lang btml ON (btm.id_tdkmegamenu = btml.id_tdkmegamenu)
                    WHERE btm.id_group = '.(int)$id_group.'
                    ORDER BY btm.id_parent ASC');
    }
}
