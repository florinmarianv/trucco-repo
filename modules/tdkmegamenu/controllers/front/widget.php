<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class TdkmegamenuWidgetModuleFrontController extends ModuleFrontController
{
    public $php_self;

    public function init()
    {
        parent::init();

        require_once($this->module->getLocalPath().'tdkmegamenu.php');
    }

    public function initContent()
    {
        $this->php_self = 'widget';
        parent::initContent();

        $module = new Tdkmegamenu();

        echo $module->renderwidget();
        die;
    }
}
