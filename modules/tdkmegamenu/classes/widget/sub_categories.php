<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class TdkWidgetSubcategories extends TdkWidgetBase
{
    public $name = 'sub_categories';
    public $for_module = 'all';

    public function getWidgetInfo()
    {
        return array('label' => $this->l('Sub Categories In Parent'), 'explain' => $this->l('Show List Of Categories Links Of Parent'));
    }

    public function renderForm($args, $data)
    {
        # validate module
        unset($args);
        $helper = $this->getFormHelper();

        $this->fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Widget Form.'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Parent Category ID'),
                    'name' => 'category_id',
                    'default' => '6',
                ),
            ),
            'buttons' => array(
                array(
                    'title' => $this->l('Save And Stay'),
                    'icon' => 'process-icon-save',
                    'class' => 'pull-right',
                    'type' => 'submit',
                    'name' => 'saveandstaytdkwidget'
                ),
                array(
                    'title' => $this->l('Save'),
                    'icon' => 'process-icon-save',
                    'class' => 'pull-right',
                    'type' => 'submit',
                    'name' => 'savetdkwidget'
                ),
            )
        );

        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues($data),
            'languages' => Context::getContext()->controller->getLanguages(),
            'id_language' => $default_lang
        );

        return $helper->generateForm($this->fields_form);
    }

    public function renderContent($args, $setting)
    {
        
        # validate module
        unset($args);
        $t = array(
            'category_id' => '',
        );
        $setting = array_merge($t, $setting);

        $category = new Category($setting['category_id'], $this->langID);
        //TDK:: check if category id not exists
        if ($category->id != '') {
            $subCategories = $category->getSubCategories($this->langID);
            $setting['subcategories'] = $subCategories;
        } else {
            $setting['subcategories'] = array();
        }
        $setting['title'] = $category->name;
        $setting['cat'] = $category;
        $output = array('type' => 'sub_categories', 'data' => $setting);
        
        return $output;
    }

    /**
     * 0 no multi_lang
     * 1 multi_lang follow id_lang
     * 2 multi_lnag follow code_lang
     */
    public function getConfigKey($multi_lang = 0)
    {
        if ($multi_lang == 0) {
            return array(
                'category_id',
            );
        } elseif ($multi_lang == 1) {
            return array(
            );
        } elseif ($multi_lang == 2) {
            return array(
            );
        }
    }
}
