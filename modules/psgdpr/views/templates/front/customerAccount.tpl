{**
 * 2007-2020 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="psgdpr-link" href="{$front_controller}">
    <span class="link-item text-center">
        <svg id="Outline" height="38" viewBox="0 0 512 512" width="38" xmlns="http://www.w3.org/2000/svg"><path d="m456 88h-216v-8a40.045 40.045 0 0 0 -40-40h-144a40.045 40.045 0 0 0 -40 40v352a40.045 40.045 0 0 0 40 40h400a40.045 40.045 0 0 0 40-40v-304a40.045 40.045 0 0 0 -40-40zm24 344a24.028 24.028 0 0 1 -24 24h-400a24.028 24.028 0 0 1 -24-24v-352a24.028 24.028 0 0 1 24-24h144a24.028 24.028 0 0 1 24 24v64a8 8 0 0 0 8 8h224a24.028 24.028 0 0 1 24 24zm0-287.978a39.788 39.788 0 0 0 -24-8.022h-216v-32h216a24.028 24.028 0 0 1 24 24z"/><path d="m283.337 277.305a40 40 0 1 0 -54.465-.087c-29.225 11.594-50.79 41.352-52.859 77.131a8 8 0 0 0 1.463 5.09 96.29 96.29 0 0 0 157.048 0 8 8 0 0 0 1.463-5.09c-2.064-35.693-23.532-65.391-52.65-77.044zm-27.187-53.305a24 24 0 1 1 -24 24 24.028 24.028 0 0 1 24-24zm-.15 160a80.412 80.412 0 0 1 -63.8-31.545c3.379-36.367 30.93-64.455 63.8-64.455s60.421 28.088 63.8 64.455a80.412 80.412 0 0 1 -63.8 31.545z"/></svg><br>

        {l s='GDPR - Personal data' mod='psgdpr'}
    </span>
</a>
