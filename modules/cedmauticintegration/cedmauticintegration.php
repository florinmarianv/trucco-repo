<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
include_once dirname(__FILE__) . '/classes/Properties.php';
include_once dirname(__FILE__) . '/classes/ConnectionManager.php';
include_once dirname(__FILE__) . '/classes/ExportCustomers.php';
if (_PS_VERSION_ < '1.7') {
    include_once(_PS_SWIFT_DIR_ . 'swift_required.php');
}

class CedMauticIntegration extends Module
{
    public $mauticContactFields = array();
    public $prestashopContactFields = array();

    public function __construct()
    {

        $this->name = 'cedmauticintegration';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Cedcommerce';
        $this->need_instance = 0;
        $this->module_key = '9a41fada096fc603616f3974d872d643';
        $this->ps_versions_compliancy = array(
            'min' => '1.6',
            'max' => _PS_VERSION_
        );


        $this->bootstrap = true;
        parent::__construct();


        $this->displayName = $this->l('Mautic Integration');
        $this->description = $this->l('Integrates Prestashop with Mautic');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!ConfigurationCore::get('mautic_integration_mauticapi_integration_enable') ||
            !ConfigurationCore::get('mautic_integration_mauticapi_integration_oauth_type') ||
            !ConfigurationCore::get('mautic_integration_mautic_abandoned_cart_time') ||
            !ConfigurationCore::get('mautic_integration_mautic_rfm_settings_rfm_fields')) {
            $this->warning = $this->l('Some default configuration missing');
        }
    }


    public function install()
    {
        if (ShopCore::isFeatureActive()) {
            ShopCore::setContext(ShopCore::CONTEXT_ALL);
        }
        $this->createMauticTab();

        $this->createCedmauticTable();
        $this->insertDataInCedMauticTable();
        if (!parent::install() ||
            !Configuration::updateValue('mautic_integration_mauticapi_integration_enable', '1') ||
            !Configuration::updateValue('mautic_integration_mauticapi_integration_oauth_type', 'Oauth2') ||
            !Configuration::updateValue('mautic_integration_mautic_abandoned_cart_time', '30') ||
            !Configuration::updateValue('mautic_integration_mautic_property_groups_customer_group', '1') ||
            !Configuration::updateValue('mautic_integration_mautic_property_groups_shopping_cart_fields', '1') ||
            !Configuration::updateValue('mautic_integration_mautic_property_groups_abandoned_cart', '1') ||
            !Configuration::updateValue('mautic_integration_mautic_property_groups_feedback', '1') ||
            !Configuration::updateValue(
                'mautic_integration_mautic_completed_order_status',
                $this->defaultCompOrderStatus()
            ) ||
            !Configuration::updateValue(
                'mautic_integration_mautic_customer_export_type',
                Properties::EXPORT_TYPE_OBSERVER
            ) ||
            !Configuration::updateValue('mautic_integration_mauticapi_integration_cron_secure_key', 'abcde') ||
            !Configuration::updateValue(
                'mautic_integration_mautic_rfm_settings_rfm_fields',
                '{"rfm_at_5":{"recency":"30","frequency":"20","monetary":"1000"},"from_rfm_4":{"recency":"31",
                "frequency":"10","monetary":"750"},"to_rfm_4":{"recency":"90","frequency":"20","monetary":"1000"},
                "from_rfm_3":{"recency":"91","frequency":"5","monetary":"500"},"to_rfm_3":{"recency":"180",
                "frequency":"10","monetary":"750"},"from_rfm_2":{"recency":"181","frequency":"2","monetary":"250"},
                "to_rfm_2":{"recency":"365","frequency":"5","monetary":"500"},
                "rfm_at_1":{"recency":"365","frequency":"2","monetary":"250"}}'
            )
        ) {
            return false;
        }
        $this->registerHook('backOfficeHeader');
        $this->registerHook('actionValidateOrder');
        $this->registerHook('actionCustomerAccountAdd');
        $this->registerHook('actionCustomerAccountUpdate');
        $this->registerHook('actionOrderStatusPostUpdate');
        $this->registerHook('actionOrderEdited');
        $this->registerHook('actionValidateCustomerAddressForm');
        $this->mauticLog();
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !ConfigurationCore::deleteByName('mautic_integration_mauticapi_integration_enable') ||
            !ConfigurationCore::deleteByName('mautic_integration_mauticapi_integration_oauth_type') ||
            !ConfigurationCore::deleteByName('mautic_integration_mautic_abandoned_cart_time') ||
            !ConfigurationCore::deleteByName('mautic_integration_mautic_rfm_settings_rfm_fields') ||
            !ConfigurationCore::deleteByName('mautic_integration_mauticapi_integration_connection_established') ||
            !ConfigurationCore::deleteByName('mautic_integration_mautic_property_groups_customer_group') ||
            !ConfigurationCore::deleteByName('mautic_integration_mautic_property_groups_shopping_cart_fields') ||
            !ConfigurationCore::deleteByName('mautic_integration_mautic_property_groups_abandoned_cart') ||
            !ConfigurationCore::deleteByName('mautic_integration_mautic_property_groups_feedback') ||
            !ConfigurationCore::deleteByName('mautic_integration_mautic_completed_order_status') ||
            !ConfigurationCore::deleteByName('mautic_integration_mautic_customer_export_type') ||
            !ConfigurationCore::deleteByName('mautic_integration_mauticapi_integration_cron_secure_key')
        ) {
            return false;
        }

        $dropCedMauticTableQuery = 'DROP TABLE ' . _DB_PREFIX_ . 'ced_mautic';
        Db::getInstance()->execute($dropCedMauticTableQuery);
        $this->uninstallTabs('AdminConfiguration');
        $this->uninstallTabs('AdminMauticSetting');
        $this->uninstallTabs('AdminCreateSingleProperty');
        $this->uninstallTabs('AdminExportCustomers');
        $this->uninstallTabs('cedmauticintegration');
        $this->unregisterHook('backOfficeHeader');
        $this->unregisterHook('actionValidateOrder');
        $this->unregisterHook('actionCustomerAccountAdd');
        $this->unregisterHook('actionCustomerAccountUpdate');
        $this->unregisterHook('actionOrderStatusPostUpdate');
        $this->unregisterHook('actionOrderEdited');
        $this->unregisterHook('actionValidateCustomerAddressForm');
        return true;
    }


    public function createMauticTab()
    {
        $this->installTabs(
            'cedmauticintegration',
            'Mautic Integration',
            0
        );
        $this->installTabs(
            'AdminConfiguration',
            'Configuration',
            (int)TabCore::getIdFromClassName('cedmauticintegration')
        );
        $this->installTabs(
            'AdminMauticSetting',
            'Settings',
            (int)TabCore::getIdFromClassName('cedmauticintegration')
        );
        $this->installTabs(
            'AdminCreateSingleProperty',
            'Create Single Property',
            (int)-1
        );
        $this->installTabs(
            'AdminExportCustomers',
            'Export Customers',
            (int)-1
        );
    }

    /* install tabs
 *
 */
    public function installTabs($class_name, $tab_name, $parent_id)
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = $class_name;
        $tab->name = array();
        foreach (LanguageCore::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $tab_name;
        }
        if ($parent_id == 0 && _PS_VERSION_ >= '1.7') {
            $tab->id_parent = (int)Tab::getIdFromClassName('SELL');
            $tab->icon = 'flight';
        } else {
            $tab->id_parent = $parent_id;
        }
        $tab->module = $this->name;
        return $tab->add();
    }

    /* Uninstall tabs
   *
   */
    public function uninstallTabs($class_name)
    {
        $id_tab = (int)Tab::getIdFromClassName($class_name);
        if ($id_tab) {
            $tab = new Tab($id_tab);
            return $tab->delete();
        } else {
            return false;
        }
    }

    public function getContent()
    {
        $output = null;
        $errors = 0;
        if (Tools::isSubmit('submitcedmauticintegration')) {
            $enable = (string)(Tools::getValue('mautic_integration_mauticapi_integration_enable'));
            ConfigurationCore::updateValue('mautic_integration_mauticapi_integration_enable', $enable);
            if ($enable == 1) {
                $oauthType = (string)(Tools::getValue('mautic_integration_mauticapi_integration_oauth_type'));

                if ($this->getMauticConfig('oauth_type') != $oauthType) {
                    $this->setMauticConfig('connection_established', 0);
                }

                ConfigurationCore::updateValue('mautic_integration_mauticapi_integration_oauth_type', $oauthType);
                $mauticUrl = (string)(Tools::getValue('mautic_integration_mauticapi_integration_mautic_url'));

                if ($oauthType == 'Oauth2') {
                    $clientid = (string)(Tools::getValue('mautic_integration_mauticapi_integration_client_id'));
                    $clientSecret = (string)(Tools::getValue('mautic_integration_mauticapi_integration_client_secret'));

                    if (!$clientid || empty($clientid) || !$clientSecret ||
                        empty($clientSecret) || !$mauticUrl || empty($mauticUrl)) {
                        $output .= $this->displayError($this->l('Mautic credentials can\'t be empty.'));
                        $errors = 1;
                    } elseif (!filter_var($mauticUrl, FILTER_VALIDATE_URL)) {
                        $output .= $this->displayError($this->l('Enter valid Mautic url.'));
                        $errors = 1;
                    } else {
                        if ($this->getMauticConfig('client_id') != $clientid ||
                            $this->getMauticConfig('client_secret') != $clientSecret ||
                            $this->getMauticConfig('mautic_url') != $mauticUrl) {
                            $this->setMauticConfig('connection_established', 0);
                        }

                        ConfigurationCore::updateValue('mautic_integration_mauticapi_integration_client_id', $clientid);
                        ConfigurationCore::updateValue(
                            'mautic_integration_mauticapi_integration_client_secret',
                            $clientSecret
                        );
                        ConfigurationCore::updateValue(
                            'mautic_integration_mauticapi_integration_mautic_url',
                            $mauticUrl
                        );
                        $output .= $this->displayConfirmation($this->l('Mautic credentails updated.'));
                    }
                } elseif ($oauthType == 'Basic') {
                    $username = (string)(Tools::getValue('mautic_integration_mauticapi_integration_mautic_username'));
                    $password = (string)(Tools::getValue('mautic_integration_mauticapi_integration_mautic_password'));
                    if (!$username || empty($username) || !$password ||
                        empty($password) || !$mauticUrl || empty($mauticUrl)) {
                        $output .= $this->displayError($this->l("Mautic credentials can't be empty."));
                        $errors = 1;
                    } elseif (!filter_var($mauticUrl, FILTER_VALIDATE_URL)) {
                        $output .= $this->displayError($this->l('Enter valid Mautic url.'));
                        $errors = 1;
                    } else {
                        if ($this->getMauticConfig('mautic_username') != $username ||
                            $this->getMauticConfig('mautic_password') != $password ||
                            $this->getMauticConfig('mautic_url') != $mauticUrl) {
                            $this->setMauticConfig('connection_established', 0);
                        }
                        ConfigurationCore::updateValue(
                            'mautic_integration_mauticapi_integration_mautic_username',
                            $username
                        );
                        ConfigurationCore::updateValue(
                            'mautic_integration_mauticapi_integration_mautic_password',
                            $password
                        );
                        ConfigurationCore::updateValue(
                            'mautic_integration_mauticapi_integration_mautic_url',
                            $mauticUrl
                        );
                        $output .= $this->displayConfirmation($this->l('Mautic credentails updated'));
                    }
                }

                ConfigurationCore::updateValue(
                    'mautic_integration_mautic_property_groups_customer_group',
                    (string)Tools::getValue('mautic_integration_mautic_property_groups_customer_group')
                );

                ConfigurationCore::updateValue(
                    'mautic_integration_mautic_property_groups_shopping_cart_fields',
                    (string)Tools::getValue('mautic_integration_mautic_property_groups_shopping_cart_fields')
                );

                ConfigurationCore::updateValue(
                    'mautic_integration_mautic_property_groups_abandoned_cart',
                    (string)Tools::getValue('mautic_integration_mautic_property_groups_abandoned_cart')
                );

                ConfigurationCore::updateValue(
                    'mautic_integration_mautic_property_groups_feedback',
                    (string)Tools::getValue('mautic_integration_mautic_property_groups_feedback')
                );

                if (ConfigurationCore::get('mautic_integration_mautic_property_groups_feedback')) {
                    ConfigurationCore::updateValue(
                        'mautic_integration_mautic_completed_order_status',
                        (string)Tools::getValue('mautic_integration_mautic_completed_order_status')
                    );
                }

                ConfigurationCore::updateValue(
                    'mautic_integration_mautic_abandoned_cart_cron_time',
                    (string)Tools::getValue('mautic_integration_mautic_abandoned_cart_cron_time')
                );

                $abandonedCartTime = (int)(string)(Tools::getValue(
                    'mautic_integration_mautic_abandoned_cart_time'
                ));

                if (!$abandonedCartTime || empty($abandonedCartTime) || $abandonedCartTime == '') {
                    $output .= $this->displayError($this->l("Abandoned cart time can't be null "));
                    $errors = 1;
                } elseif ($abandonedCartTime <= 0) {
                    $output .= $this->displayError($this->l('Abandoned cart time should be greater than 0.'));
                    $errors = 1;
                } else {
                    ConfigurationCore::updateValue(
                        'mautic_integration_mautic_abandoned_cart_time',
                        $abandonedCartTime
                    );
                }

                ConfigurationCore::updateValue(
                    'mautic_integration_mautic_customer_export_type',
                    (string)Tools::getValue('mautic_integration_mautic_customer_export_type')
                );

                if (ConfigurationCore::get('mautic_integration_mautic_customer_export_type') ==
                    Properties::EXPORT_TYPE_CRON) {
                    ConfigurationCore::updateValue(
                        'mautic_integration_mautic_customer_export_cron_time',
                        (string)Tools::getValue('mautic_integration_mautic_customer_export_cron_time')
                    );
                }

                $data = array();
                $data['rfm_at_5']['recency'] = (string)(Tools::getValue('rfm_at_5_recency'));
                $data['rfm_at_5']['frequency'] = (string)(Tools::getValue('rfm_at_5_frequency'));
                $data['rfm_at_5']['monetary'] = (string)(Tools::getValue('rfm_at_5_monetary'));
                $data['from_rfm_4']['recency'] = (string)(Tools::getValue('from_rfm_4_recency'));
                $data['to_rfm_4']['recency'] = (string)(Tools::getValue('to_rfm_4_recency'));
                $data['from_rfm_4']['frequency'] = (string)(Tools::getValue('from_rfm_4_frequency'));
                $data['to_rfm_4']['frequency'] = (string)(Tools::getValue('to_rfm_4_frequency'));
                $data['from_rfm_4']['monetary'] = (string)(Tools::getValue('from_rfm_4_monetary'));
                $data['to_rfm_4']['monetary'] = (string)(Tools::getValue('to_rfm_4_monetary'));
                $data['from_rfm_3']['recency'] = (string)(Tools::getValue('from_rfm_3_recency'));
                $data['to_rfm_3']['recency'] = (string)(Tools::getValue('to_rfm_3_recency'));
                $data['from_rfm_3']['frequency'] = (string)(Tools::getValue('from_rfm_3_frequency'));
                $data['to_rfm_3']['frequency'] = (string)(Tools::getValue('to_rfm_3_frequency'));
                $data['from_rfm_3']['monetary'] = (string)(Tools::getValue('from_rfm_3_monetary'));
                $data['to_rfm_3']['monetary'] = (string)(Tools::getValue('to_rfm_3_monetary'));
                $data['from_rfm_2']['recency'] = (string)(Tools::getValue('from_rfm_2_recency'));
                $data['to_rfm_2']['recency'] = (string)(Tools::getValue('to_rfm_2_recency'));
                $data['from_rfm_2']['frequency'] = (string)(Tools::getValue('from_rfm_2_frequency'));
                $data['to_rfm_2']['frequency'] = (string)(Tools::getValue('to_rfm_2_frequency'));
                $data['from_rfm_2']['monetary'] = (string)(Tools::getValue('from_rfm_2_monetary'));
                $data['to_rfm_2']['monetary'] = (string)(Tools::getValue('to_rfm_2_monetary'));
                $data['rfm_at_1']['recency'] = (string)(Tools::getValue('rfm_at_1_recency'));
                $data['rfm_at_1']['frequency'] = (string)(Tools::getValue('rfm_at_1_frequency'));
                $data['rfm_at_1']['monetary'] = (string)(Tools::getValue('rfm_at_1_monetary'));

                $break = 0;
                foreach ($data as $key => $value) {
                    foreach ($value as $subValue) {
                        if (!$subValue || empty($subValue)) {
                            $output .= $this->displayError($this->l('Please fill all the textboxes of RFM ratings.'));
                            $errors = 2;
                            $break = 1;
                            break;
                        } elseif ($subValue < 0) {
                            $output .= $this->displayError($this->l('RFM ratings values should be greater than 0'));
                            $errors = 2;
                            $break = 1;
                            break;
                        }
                    }
                }

                $rfmData = json_encode($data);
                if ($break === 0) {
                    ConfigurationCore::updateValue('mautic_integration_mautic_rfm_settings_rfm_fields', $rfmData);
                    $output .= $this->displayConfirmation($this->l('RFM settings table data updated.'));
                }

                $allValues = Tools::getAllValues();
                $mappingTable = array();
                $mappingValues = array();
                foreach ($allValues as $key => $value) {
                    if (strpos($key, 'mautic_contact_fields_') === 0) {
                        $mappingRowId = explode('mautic_contact_fields_', $key);
                        $mappingTable[$mappingRowId[1]]['mautic'] = $value;
                    } elseif (strpos($key, 'prestashop_customer_fields_') === 0) {
                        $mappingRowId = explode('prestashop_customer_fields_', $key);
                        $mappingTable[$mappingRowId[1]]['prestashop'] = $value;
                    }
                }

                foreach ($mappingTable as $key => $value) {
                    if ($mappingTable[$key]['mautic'] != "" && $mappingTable[$key]['prestashop'] != "") {
                        $mappingValues[$mappingTable[$key]['mautic']] = $mappingTable[$key]['prestashop'];
                    }
                }

                $mappingData = json_encode($mappingValues);
                ConfigurationCore::updateValue('mautic_integration_mautic_fields_mapping_mapping_table', $mappingData);
                $output .= $this->displayConfirmation($this->l('Field mapping table data updated.'));

                $cronSecureKey = (string)(Tools::getValue('mautic_integration_mauticapi_integration_cron_secure_key'));
                if (!$cronSecureKey || empty($cronSecureKey) || $cronSecureKey == "") {
                    $output .= $this->displayError($this->l("Cron secure key can't be empty."));
                    $errors = 3;
                } else {
                    ConfigurationCore::updateValue(
                        'mautic_integration_mauticapi_integration_cron_secure_key',
                        $cronSecureKey
                    );
                    $output .= $this->displayConfirmation($this->l('Cron secure key updated.'));
                }
            }
            if ($errors === 0) {
                $output = $this->displayConfirmation($this->l('Settings updated.'));
            }
        }

        $this->context->smarty->assign(
            array(
                'observer' => Properties::EXPORT_TYPE_OBSERVER,
                'cron' => Properties::EXPORT_TYPE_CRON
            )
        );

        $this->authorizeLink();
        $this->getCodeLink();
        $this->getMauticContactFields();
        $this->getPrestashopContactFields();

        $content = $this->context->smarty->fetch(_PS_MODULE_DIR_ .
            'cedmauticintegration/views/templates/admin/configure.tpl');

        $this->context->smarty->assign(
            array(
                'content' => $content
            )
        );

        return $output . $this->displayForm() . $content;
    }

    public function displayForm()
    {
        // Get default language
        $defaultLang = (int)ConfigurationCore::get('PS_LANG_DEFAULT');
        $base_url = ContextCore::getContext()->shop->getBaseURL(true);
        $cron_secure_key = ConfigurationCore::get('mautic_integration_mauticapi_integration_cron_secure_key');
        if (!$cron_secure_key) {
            $cron_secure_key = "";
        }
        $abandonedCartTime = ConfigurationCore::get('mautic_integration_mautic_abandoned_cart_time');
        $mauticUrl = ConfigurationCore::get('mautic_integration_mauticapi_integration_mautic_url');
        $redirectUrl = $this->context->link->getModuleLink('cedmauticintegration', 'getcode');
        $rfmData = ConfigurationCore::get('mautic_integration_mautic_rfm_settings_rfm_fields');
        $data = json_decode($rfmData, true);

        if ($this->getMauticConfig('connection_established')) {
            $authorize = 'Re-Authenticate';
            $basicInstall = 'Re-Install';
        } else {
            $authorize = 'Authenticate';
            $basicInstall = 'Install';
        }

        $this->context->smarty->assign(
            array(
                'data' => $data,
                'mauticUrl' => $mauticUrl,
                'authorize' => $authorize,
                'basicInstall' => $basicInstall,
                'abandonedCartTime' => $abandonedCartTime
            )
        );
        // Init Fields form array
        $fieldsForm = array();
        $fieldsForm[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Mautic Configuration'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Enable Mautic Integration'),
                    'name' => 'mautic_integration_mauticapi_integration_enable',
                    'required' => true,
                    'options' => array(
                        'query' => array(
                            array(
                                'id_option' => 1,
                                'name' => 'Yes'    
                            ),
                            array(
                                'id_option' => 0,
                                'name' => 'No'
                            ),
                        ),
                        'id' => 'id_option',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Authentication Type'),
                    'name' => 'mautic_integration_mauticapi_integration_oauth_type',
                    'required' => true,
                    'options' => array(
                        'query' => array(
                            array(
                                'id_option' => 'Oauth2',
                                'name' => 'Oauth2'
                            ),
                        ),
                        'id' => 'id_option',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Username'),
                    'name' => 'mautic_integration_mauticapi_integration_mautic_username',
                    'size' => 20,
                    'required' => true
                ),
                array(
                    'type' => 'password',
                    'label' => $this->l('Password'),
                    'name' => 'mautic_integration_mauticapi_integration_mautic_password',
                    'size' => 20,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Client ID'),
                    'name' => 'mautic_integration_mauticapi_integration_client_id',
                    'size' => 50,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Client Secret'),
                    'name' => 'mautic_integration_mauticapi_integration_client_secret',
                    'size' => 50,
                    'required' => true
                ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Mautic URL'),
                    'name' => 'mautic_integration_mauticapi_integration_mautic_url',
                    'size' => 50,
                    'required' => true,
                    'html_content' => $this->context->smarty->fetch(_PS_MODULE_DIR_ .
                        'cedmauticintegration/views/templates/admin/mauticurl.tpl'),
                    'desc' => 'Mautic URL of your app should be https://mauticurl.com.'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Redirect URI'),
                    'name' => 'mautic_integration_mauticapi_integration_redirect_url',
                    'size' => 50,
                    'required' => true,
                    'readonly' => true,
                    'desc' =>'Redirect URI of your app should be www.yourdomain.com/module/cedmauticintegration/getcode'
                ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Authorize'),
                    'name' => 'mautic_integration_mauticapi_integration_oauth2_authorize',
                    'html_content' => $this->context->smarty->fetch(_PS_MODULE_DIR_ .
                        'cedmauticintegration/views/templates/admin/authorize.tpl')
                ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Install'),
                    'name' => 'mautic_integration_mauticapi_integration_basic_authorize',
                    'html_content' => $this->context->smarty->fetch(_PS_MODULE_DIR_ .
                        'cedmauticintegration/views/templates/admin/installbutton.tpl')
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-primary pull-right submitMauticConfiguration',
                'name' => 'submitMauticConfiguration'
            )
        );

        $fieldsForm[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Manage Property Groups'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Customer group'),
                    'name' => 'mautic_integration_mautic_property_groups_customer_group',
                    'required' => true,
                    'options' => array(
                        'query' => array(
                            array(
                                'id_option' => 1,
                                'name' => 'Enable'
                            ),
                            array(
                                'id_option' => 0,
                                'name' => 'Disable'
                            ),
                        ),
                        'id' => 'id_option',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Shipping and Billing Address'),
                    'name' => 'mautic_integration_mautic_property_groups_shopping_cart_fields',
                    'required' => true,
                    'options' => array(
                        'query' => array(
                            array(
                                'id_option' => 1,
                                'name' => 'Enable'
                            ),
                            array(
                                'id_option' => 0,
                                'name' => 'Disable'
                            ),
                        ),
                        'id' => 'id_option',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Abandoned Cart'),
                    'name' => 'mautic_integration_mautic_property_groups_abandoned_cart',
                    'required' => true,
                    'options' => array(
                        'query' => array(
                            array(
                                'id_option' => 1,
                                'name' => 'Enable'
                            ),
                            array(
                                'id_option' => 0,
                                'name' => 'Disable'
                            ),
                        ),
                        'id' => 'id_option',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Order Item Feedback'),
                    'name' => 'mautic_integration_mautic_property_groups_feedback',
                    'required' => true,
                    'options' => array(
                        'query' => array(
                            array(
                                'id_option' => 1,
                                'name' => 'Enable'
                            ),
                            array(
                                'id_option' => 0,
                                'name' => 'Disable'
                            ),
                        ),
                        'id' => 'id_option',
                        'name' => 'name'
                    ),
                    'desc' => $this->l('Enable customer properties you want to export.', array())
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select status for completed order'),
                    'name' => 'mautic_integration_mautic_completed_order_status',
                    'required' => true,
                    'options' => array(
                        'query' => $this->getOrderStatusList(),

                        'id' => 'id_option',
                        'name' => 'name'
                    )
                ),
            )
        );

        $fieldsForm[2]['form'] = array(
            'legend' => array(
                'title' => $this->l('Abandoned Cart'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Abandoned Cart Cron Time'),
                    'name' => 'mautic_integration_mautic_abandoned_cart_cron_time',
                    'required' => true,
                    'options' => array(
                        'query' => $this->setCronOptions(),
                        'id' => 'id_option',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Abandoned Cart Time(in minutes)'),
                    'name' => 'mautic_integration_mautic_abandoned_cart_time',
                    'required' => true,
                    'html_content' => $this->context->smarty->fetch(_PS_MODULE_DIR_ .
                        'cedmauticintegration/views/templates/admin/abandonedcarttime.tpl'),
                    'desc' => 'After how many minute of inactive cart we should treat it as abandoned.'
                )
            )
        );

        $fieldsForm[3]['form'] = array(
            'legend' => array(
                'title' => $this->l('Customer Export Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Export Type'),
                    'name' => 'mautic_integration_mautic_customer_export_type',
                    'required' => true,
                    'options' => array(
                        'query' => array(
                            array(
                                'id_option' => Properties::EXPORT_TYPE_OBSERVER,
                                'name' => 'Immediately'
                            ),
                            array(
                                'id_option' => Properties::EXPORT_TYPE_CRON,
                                'name' => 'Cron based'
                            ),
                        ),
                        'id' => 'id_option',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Cron Time'),
                    'name' => 'mautic_integration_mautic_customer_export_cron_time',
                    'required' => true,
                    'options' => array(
                        'query' => $this->setCronOptions(),
                        'id' => 'id_option',
                        'name' => 'name'
                    )
                )
            )
        );

        $rfmSettingTpl = $this->context->smarty->fetch(_PS_MODULE_DIR_ .
            'cedmauticintegration/views/templates/admin/rfmsetting.tpl');

        $fieldsForm[4]['form'] = array(
            'legend' => array(
                'title' => $this->l('Rfm Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'html',
                    'name' => 'mautic_integration_mautic_rfm_settings_rfm_fields',
                    'required' => true,
                    'html_content' => $rfmSettingTpl
                )
            )
        );

        $this->context->smarty->assign(
            array(
                'mappingRowValues' => $this->setMappingRows(),
                'mauticContactFields' => $this->mauticContactFields,
                'prestashopContactFields' => $this->prestashopContactFields
            )
        );

        $mappingTable = $this->context->smarty->fetch(_PS_MODULE_DIR_ .
            'cedmauticintegration/views/templates/admin/mappingtable.tpl');

        $fieldsForm[5]['form'] = array(
            'legend' => array(
                'title' => $this->l('Fields Mapping'),
            ),
            'input' => array(
                array(
                    'type' => 'html',
                    'name' => 'mautic_integration_mautic_fields_mapping_mapping_table',
                    'required' => true,
                    'html_content' => $mappingTable,
                    'desc' => $this->l('Mautic authentication is required to get the data of Mautic contact 
                    fields.', array()),

                )
            )
        );

        $this->context->smarty->assign(
            array(
                'base_url' => $base_url,
                'cron_secure_key' => $cron_secure_key,
                'abandoned_cart_time' => $this->getCronTime('mautic_integration_mautic_abandoned_cart_cron_time'),
                'customer_export_time' => $this->getCronTime('mautic_integration_mautic_customer_export_cron_time')
            )
        );

        $cronUrls = $this->context->smarty->fetch(_PS_MODULE_DIR_ .
            'cedmauticintegration/views/templates/admin/cronurls.tpl');

        $fieldsForm[6]['form'] = array(
            'legend' => array(
                'title' => $this->l('CRON INFO'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Cron Secure Key'),
                    'name' => 'mautic_integration_mauticapi_integration_cron_secure_key',
                    'size' => 50,
                    'required' => true
                ),
                array(
                    'type' => 'html',
                    'name' => 'mautic_integration_mauticapi_integration_cron_urls',
                    'required' => true,
                    'html_content' => $cronUrls,
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-primary pull-right',
                'name' => 'submitCronSecureKey'
            )
        );

        $helper = new HelperFormCore();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminControllerCore::$currentIndex . '&configure=' . $this->name;

        // Language
        $helper->default_form_language = $defaultLang;
        $helper->allow_employee_form_lang = $defaultLang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit' . $this->name;

        // Load current value
        $helper->fields_value['mautic_integration_mauticapi_integration_enable'] =
            ConfigurationCore::get('mautic_integration_mauticapi_integration_enable');

        $helper->fields_value['mautic_integration_mauticapi_integration_oauth_type'] =
            ConfigurationCore::get('mautic_integration_mauticapi_integration_oauth_type');

        $helper->fields_value['mautic_integration_mauticapi_integration_client_id'] =
            ConfigurationCore::get('mautic_integration_mauticapi_integration_client_id');

        $helper->fields_value['mautic_integration_mauticapi_integration_client_secret'] =
            ConfigurationCore::get('mautic_integration_mauticapi_integration_client_secret');

        $helper->fields_value['mautic_integration_mauticapi_integration_mautic_username'] =
            ConfigurationCore::get('mautic_integration_mauticapi_integration_mautic_username');

        $helper->fields_value['mautic_integration_mauticapi_integration_mautic_password'] =
            ConfigurationCore::get('mautic_integration_mauticapi_integration_mautic_password');

        $helper->fields_value['mautic_integration_mauticapi_integration_redirect_url'] = $redirectUrl;

        $helper->fields_value['mautic_integration_mauticapi_integration_cron_secure_key'] =
            ConfigurationCore::get('mautic_integration_mauticapi_integration_cron_secure_key');

        $helper->fields_value['mautic_integration_mautic_property_groups_customer_group'] =
            ConfigurationCore::get('mautic_integration_mautic_property_groups_customer_group');

        $helper->fields_value['mautic_integration_mautic_property_groups_shopping_cart_fields'] =
            ConfigurationCore::get('mautic_integration_mautic_property_groups_shopping_cart_fields');

        $helper->fields_value['mautic_integration_mautic_property_groups_abandoned_cart'] =
            ConfigurationCore::get('mautic_integration_mautic_property_groups_abandoned_cart');

        $helper->fields_value['mautic_integration_mautic_property_groups_feedback'] =
            ConfigurationCore::get('mautic_integration_mautic_property_groups_feedback');

        $helper->fields_value['mautic_integration_mautic_completed_order_status'] =
            ConfigurationCore::get('mautic_integration_mautic_completed_order_status');

        $helper->fields_value['mautic_integration_mautic_abandoned_cart_cron_time'] =
            ConfigurationCore::get('mautic_integration_mautic_abandoned_cart_cron_time');

        $helper->fields_value['mautic_integration_mautic_abandoned_cart_time'] =
            ConfigurationCore::get('mautic_integration_mautic_abandoned_cart_time');

        $helper->fields_value['mautic_integration_mautic_customer_export_type'] =
            ConfigurationCore::get('mautic_integration_mautic_customer_export_type');

        $helper->fields_value['mautic_integration_mautic_customer_export_cron_time'] =
            ConfigurationCore::get('mautic_integration_mautic_customer_export_cron_time');

        return $helper->generateForm($fieldsForm);
    }

    public function createCedmauticTable()
    {
        Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'ced_mautic (
			  `entity_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `entity_type` VARCHAR(100) NOT NULL,
			  `code` VARCHAR(100) NOT NULL,
			  `name` VARCHAR(100) NOT NULL,
			  `is_required` BIT NOT NULL ,
			  `mautic_id` int NOT NULL ,
			  UNIQUE (`entity_type`,`code`)
			) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;'
        );
    }

    public function insertDataInCedMauticTable()
    {
        $notRequiredProperties = array('ced_ship_add_line_1', 'ced_ship_add_line_2', 'ced_ship_city', 'ced_ship_state',
            'ced_ship_post_code', 'ced_ship_country', 'ced_prod_2_img_url', 'ced_prod_2_name', 'ced_prod_2_price',
            'ced_prod_2_url', 'ced_prod_3_img_url', 'ced_prod_3_name', 'ced_prod_3_price', 'ced_prod_3_url',
            'ced_abncart_prod_html', 'ced_abncart_prods_count', 'ced_abncart_total');

        $insertDataQuery = 'INSERT INTO ' . _DB_PREFIX_ . 'ced_mautic(`entity_type`,`code`,`name`,`is_required`,
        `mautic_id`)VALUES ';
        $allGroups = Properties::allGroups();
        foreach ($allGroups as $group) {
            $allGroupProperties = Properties::allProperties($group['name']);
            foreach ($allGroupProperties as $property) {
                if (in_array($property['alias'], $notRequiredProperties)) {
                    $insertDataQuery .= '("properties","' . $property['alias'] . '","' . $property['label'] . '",0,0),';
                } else {
                    $insertDataQuery .= '("properties","' . $property['alias'] . '","' . $property['label'] . '",1,0),';
                }
            }
        }

        $notRequiredSegments = array('ced-newsletter-subs');
        $allSegments = Properties::getMauticSegments();
        foreach ($allSegments as $segment) {
            if (in_array($segment['alias'], $notRequiredSegments)) {
                $insertDataQuery .= '("segments","' . $segment['alias'] . '","' . $segment['name'] . '",0,0),';
            } else {
                $insertDataQuery .= '("segments","' . $segment['alias'] . '","' . $segment['name'] . '",1,0),';
            }
        }
        $insertQuery = trim($insertDataQuery, ",");
        Db::getInstance()->execute($insertQuery);
    }

    public function authorizeLink()
    {
        if (!$this->getMauticConfig('client_id') ||
            !$this->getMauticConfig('client_secret') ||
            !$this->getMauticConfig('mautic_url')) {
            $this->context->smarty->assign(
                array(
                    'oauth2_authorize' => false,
                    'error_message' => "Please fill Mautic credentials.",
                    'oauth2_authorize_link' => "",
                )
            );
        } else {
            $state = 'cedcommerce';
            $url = $this->getMauticConfig('mautic_url') . '/oauth/v2/authorize?client_id=' .
                $this->getMauticConfig('client_id') . '&grant_type=authorization_code&redirect_uri=' .
                $this->getRedirectUrl() . '&response_type=code&state=' . $state . '';
            $this->context->smarty->assign(
                array(
                    'oauth2_authorize' => true,
                    'error_message' => "",
                    'oauth2_authorize_link' => $url,
                )
            );
        }
    }

    public function getCodeLink()
    {
        if (!$this->getMauticConfig('mautic_username') ||
            !$this->getMauticConfig('mautic_password') ||
            !$this->getMauticConfig('mautic_url')) {
            $this->context->smarty->assign(
                array(
                    'basic_authorize' => false,
                    'error_message' => "Please fill Mautic credentials.",
                    'basic_authorize_link' => "",
                )
            );
        } else {
            $this->context->smarty->assign(
                array(
                    'basic_authorize' => true,
                    'error_message' => "",
                    'basic_authorize_link' => $this->getRedirectUrl(),
                )
            );
        }
    }

    public function getMauticContactFields()
    {
        $mauticContactFields = array();
        if ($this->getMauticConfig('connection_established')) {
            $connectionManager = new ConnectionManager();
            $response = $connectionManager->getListOfFields();
            if (isset($response['status_code']) && $response['status_code'] == 200) {
                $newResponse = json_decode($response['response'], true);
                if (isset($newResponse['total']) && $newResponse['total'] > 0) {
                    $fields = $newResponse['fields'];
                    foreach ($fields as $field) {
                        if (strpos($field['alias'], 'ced_') !== 0 && !$this->isExcluded($field['alias'])) {
                            if (isset($field['alias'])) {
                                $value = $field['alias'];
                            } else {
                                $value = $field['label'];
                            }
                            $label = $field['label'];
                            $mauticContactFields[] = array('label' => $label, 'value' => $value);
                        }
                    }
                }
            }
        }
        $this->mauticContactFields = $mauticContactFields;
        $this->context->smarty->assign(
            array(
                'mauticContactFields' => $this->mauticContactFields,
            )
        );
    }

    public function getPrestashopContactFields()
    {
        $prestashopContactFields = Db::getInstance()->executeS(
            'DESCRIBE ' . _DB_PREFIX_ . 'customer;'
        );
        foreach ($prestashopContactFields as $contactField) {
            if (isset($contactField['Field'])) {
                $this->prestashopContactFields[] = $contactField['Field'];
            }
        }
        $this->context->smarty->assign(
            array(
                'prestashopContactFields' => $this->prestashopContactFields,
            )
        );
    }

    public function getMauticConfig($field)
    {
        if (!$field) {
            return null;
        }
        $path = 'mautic_integration_mauticapi_integration_' . $field;
        return ConfigurationCore::get($path);
    }

    public function setMauticConfig($field, $value)
    {
        if (!$field) {
            return null;
        }
        $path = 'mautic_integration_mauticapi_integration_' . $field;
        return ConfigurationCore::updateValue($path, $value);
    }

    public function getRedirectUrl()
    {
        return $this->context->link
            ->getModuleLink('cedmauticintegration', 'getcode');
    }

    public function isExcluded($attributeCode)
    {
        $excludedFields = array('email', 'firstname', 'lastname');
        if (in_array($attributeCode, $excludedFields)) {
            return true;
        } else {
            return false;
        }
    }

    public function setMappingRows()
    {
        $mappingValue = array();
        $mappingTable = json_decode(
            Configuration::get('mautic_integration_mautic_fields_mapping_mapping_table'),
            true
        );
        if (!$mappingTable) {
            $mappingTable = array();
        }

        foreach ($mappingTable as $key => $value) {
            $randomNumber = rand(1, 1000000);
            $mappingValue[$randomNumber]['mautic'] = $key;
            $mappingValue[$randomNumber]['prestashop'] = $value;
        }
        return $mappingValue;
    }

    public function setCronOptions()
    {
        $options = array(
            array(
                'id_option' => Properties::ONCE_IN_A_DAY,
                'name' => 'Once in a day'
            ),
            array(
                'id_option' => Properties::TWICE_A_DAY,
                'name' => 'Twice a day'
            ),
            array(
                'id_option' => Properties::FOUR_TIMES_A_DAY,
                'name' => 'Four times a day'
            ),
            array(
                'id_option' => Properties::EVERY_HOUR,
                'name' => 'Every hour'
            )
        );

        return $options;
    }

    public function getOrderStatusList()
    {
        $langId = (int)Configuration::get('PS_LANG_DEFAULT');
        $orderStateList = OrderStateCore::getOrderStates($langId);
        $options = array();
        foreach ($orderStateList as $orderState) {
            $options[] = array('id_option' => $orderState['id_order_state'], 'name' => $orderState['name']);
        }
        return $options;
    }

    public function defaultCompOrderStatus()
    {
        $langId = (int)Configuration::get('PS_LANG_DEFAULT');
        $orderStateList = OrderStateCore::getOrderStates($langId);
        $stausId = 0;
        foreach ($orderStateList as $orderState) {
            if ($orderState['name'] == 'Delivered') {
                $stausId = $orderState['id_order_state'];
            }
        }
        return $stausId;
    }

    public function getCronTime($path)
    {
        if ($path == 'mautic_integration_mautic_abandoned_cart_cron_time') {
            if (!ConfigurationCore::get('mautic_integration_mautic_property_groups_abandoned_cart')) {
                return 'Disabled';
            }
        }

        if ($path == 'mautic_integration_mautic_customer_export_cron_time') {
            if (ConfigurationCore::get('mautic_integration_mautic_customer_export_type') ==
                Properties::EXPORT_TYPE_OBSERVER) {
                return 'Disabled';
            }
        }

        $value = ConfigurationCore::get($path);
        if ($value == Properties::ONCE_IN_A_DAY) {
            return 'Once in a day';
        } elseif ($value == Properties::TWICE_A_DAY) {
            return 'Twice a day';
        } elseif ($value == Properties::FOUR_TIMES_A_DAY) {
            return 'Four times a day';
        } elseif ($value == Properties::EVERY_HOUR) {
            return 'Every hour';
        } elseif ($value == Properties::EVERY_FIVE_MINUTES) {
            return 'Every five minutes';
        }
        return 'No time selected';
    }

    public function mauticLog()
    {
        try {
            $domain = ContextCore::getContext()->shop->getBaseURL(true);
            $merchantmail = (ConfigurationCore::get('PS_SHOP_EMAIL'));
            $customers = CustomerCore::getCustomers();
            if (is_array($customers)) {
                $customerCount = count($customers);
            } else {
                $customerCount = 0;
            }

            $this->context->smarty->assign(
                array(
                    'domain' => $domain,
                    'merchant_mail' => $merchantmail,
                    'installation' => date('Y-m-d H:i:s'),
                    'customer_count' => $customerCount
                )
            );

            $body = $this->context->smarty->fetch(_PS_MODULE_DIR_ .
                'cedmauticintegration/views/templates/admin/mauticlog.tpl');

            $to_email = 'ankurverma@cedcoss.com';
            $subject = 'New Installation of Mautic PrestaShop';
            $message = \Swift_Message::newInstance();

            $message
                ->setFrom($merchantmail)
                ->setTo($to_email)
                ->setSubject($subject)
                ->setBody($body);
            $swift = \Swift_Mailer::newInstance(\Swift_MailTransport::newInstance());
            $swift->send($message);
        } catch (\Exception $e) {
            $connectionManager = new ConnectionManager();
            $connectionManager->createLog(
                'error in sending mail:-' . $e->getMessage(),
                '',
                '',
                ''
            );
        }
    }

    public function hookBackOfficeHeader()
    {
        if (_PS_VERSION_ < '1.7') {
            $this->context->controller->addCSS($this->_path . 'views/css/mautic-config.css');
        }
    }

    public function hookActionValidateOrder($params)
    {
        $customerId = array();
        if ($this->getMauticConfig('enable') &&
            ConfigurationCore::get('mautic_integration_mautic_customer_export_type') ==
            Properties::EXPORT_TYPE_OBSERVER) {
            $connectionManager = new ConnectionManager();
            $exportProperties = new ExportCustomers();
            if (isset($params['customer']->id)) {
                $customerId[] = $params['customer']->id;
                try {
                    $exportProperties->exportCustomerOrder($customerId);
                } catch (\Exception $e) {
                    $connectionManager->createLog(
                        'Error in hookActionValidateOrder:- ' . $e->getMessage(),
                        '',
                        '',
                        ''
                    );
                }
            }
        }
    }

    public function hookActionCustomerAccountAdd($params)
    {
        $customerId = array();
        if ($this->getMauticConfig('enable') &&
            ConfigurationCore::get('mautic_integration_mautic_customer_export_type') ==
            Properties::EXPORT_TYPE_OBSERVER) {
            $connectionManager = new ConnectionManager();
            $exportCustomers = new ExportCustomers();
            if (isset($params['newCustomer']->id)) {
                $customerId[] = $params['newCustomer']->id;
                try {
                    $exportCustomers->exportCustomerAccount($customerId);
                } catch (\Exception $e) {
                    $connectionManager->createLog(
                        'Error in hookActionCustomerAccountAdd:- ' . $e->getMessage(),
                        '',
                        '',
                        ''
                    );
                }
            }
        }
    }

    public function hookActionCustomerAccountUpdate($params)
    {
        $customerId = array();
        if ($this->getMauticConfig('enable') &&
            ConfigurationCore::get('mautic_integration_mautic_customer_export_type') ==
            Properties::EXPORT_TYPE_OBSERVER) {
            $connectionManager = new ConnectionManager();
            $exportCustomers = new ExportCustomers();
            if (isset($params['customer']->id)) {
                $customerId[] = $params['customer']->id;
                try {
                    $exportCustomers->exportCustomerAccount($customerId);
                } catch (\Exception $e) {
                    $connectionManager->createLog(
                        'Error in hookActionCustomerAccountUpdate:- ' . $e->getMessage(),
                        '',
                        '',
                        ''
                    );
                }
            }
        }
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        $customerId = array();
        if ($this->getMauticConfig('enable') &&
            ConfigurationCore::get('mautic_integration_mautic_customer_export_type') ==
            Properties::EXPORT_TYPE_OBSERVER) {
            $connectionManager = new ConnectionManager();
            $exportCustomers = new ExportCustomers();
            if (isset($params['cart']->id_customer)) {
                $customerId[] = $params['cart']->id_customer;
                try {
                    $exportCustomers->exportCustomerOrder($customerId);
                } catch (\Exception $e) {
                    $connectionManager->createLog(
                        'Error in hookActionOrderStatusPostUpdate:- ' . $e->getMessage(),
                        '',
                        '',
                        ''
                    );
                }
            }
        }
    }

    public function hookActionOrderEdited($params)
    {
        $customerId = array();
        if ($this->getMauticConfig('enable') &&
            ConfigurationCore::get('mautic_integration_mautic_customer_export_type') ==
            Properties::EXPORT_TYPE_OBSERVER) {
            $connectionManager = new ConnectionManager();
            $exportCustomers = new ExportCustomers();
            if (isset($params['order'])) {
                $orderFromObserver = (array)$params['order'];
                $orderFromObserver['id_order'] = $orderFromObserver['id'];
            }
            if (isset($params['order']->id_customer)) {
                $customerId[] = $params['order']->id_customer;
                try {
                    $exportCustomers->exportCustomerOrder($customerId);
                } catch (\Exception $e) {
                    $connectionManager->createLog(
                        'Error in hookActionOrderEdited:- ' . $e->getMessage(),
                        '',
                        '',
                        ''
                    );
                }
            }
        }
    }

    public function hookActionValidateCustomerAddressForm($params)
    {
        $customerId = array();
        if ($this->getMauticConfig('enable') &&
            ConfigurationCore::get('mautic_integration_mautic_customer_export_type') ==
            Properties::EXPORT_TYPE_OBSERVER) {
            $connectionManager = new ConnectionManager();
            $exportCustomers = new ExportCustomers();
            if (isset($params['cookie']->id_customer)) {
                $customerId[] = $params['cookie']->id_customer;
                try {
                    $exportCustomers->exportCustomerAccount($customerId);
                } catch (\Exception $e) {
                    $connectionManager->createLog(
                        'Error in hookActionValidateCustomerAddressForm:- ' . $e->getMessage(),
                        '',
                        '',
                        ''
                    );
                }
            }
        }
    }
}
