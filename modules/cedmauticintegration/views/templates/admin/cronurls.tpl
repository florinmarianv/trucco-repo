<!--
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the End User License Agreement(EULA)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://cedcommerce.com/license-agreement.txt
*
* @author    CedCommerce Core Team <connect@cedcommerce.com>
* @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
* @license   http://cedcommerce.com/license-agreement.txt
* @category  Ced
* @package   CedMauticIntegration
*/
-->
<div style="overflow-x:auto">
<table class="table" id='mautic_integration_mauticapi_integration_cron_urls'>
    <thead>
    <tr>
        <th><strong>Cron Name</strong></th>
        <th><strong>Cron URL</strong></th>
        <th><strong>Recommended Time</strong></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Abandoned Carts Data Export</td>
        <td scope="row">{$base_url|escape:'htmlall':'UTF-8'}modules/cedmauticintegration/mauticintegration-abandoned-cart-cron.php?secure_key={$cron_secure_key|escape:'htmlall':'UTF-8'}</td>
        <td>{$abandoned_cart_time|escape:'htmlall':'UTF-8'}</td>
    </tr>
    <tr>
        <td>Customer Data Export</td>
        <td scope="row">{$base_url|escape:'htmlall':'UTF-8'}modules/cedmauticintegration/mauticintegration-export-customer-cron.php?secure_key={$cron_secure_key|escape:'htmlall':'UTF-8'}</td>
        <td>{$customer_export_time|escape:'htmlall':'UTF-8'}</td>
    </tr>
    </tbody>
</table>
</div>