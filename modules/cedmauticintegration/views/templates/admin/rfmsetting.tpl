<!--
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the End User License Agreement(EULA)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://cedcommerce.com/license-agreement.txt
*
* @author    CedCommerce Core Team <connect@cedcommerce.com>
* @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
* @license   http://cedcommerce.com/license-agreement.txt
* @category  Ced
* @package   CedMauticIntegration
*/
-->
<style>
    td {
        border-left: 0.1rem dashed #d6d6d6;
        border-right: 0.1rem dashed #d6d6d6;
    }
    tbody tr:first-child {
        border-top: 1px solid #d6d6d6;
    }
    tbody tr:last-child {
        border-bottom: 1px solid #d6d6d6;
    }
    tr td:first-child {
        border-left: 1px solid #d6d6d6;
    }
    tr td:last-child {
        border-right: 1px solid #d6d6d6;
    }
    tr:nth-child(even) {
        background-color: #f2f2f2
    }

    .data-grid-th {
        background-clip: padding-box;
        color: #ffffff;
        padding: 1rem 1rem;
        position: relative;
        vertical-align: middle;
        background-color: #514943;
        border: 0.1rem solid #8a837f;
        border-left-color: transparent;
        font-weight: 600;
        text-align: left;
    }

</style>
<div style="overflow-x:auto;">
<tr id='row_mautic_integration_mautic_rfm_settings_rfm_fields'>
    <td class='value' style='padding: 0; width: 100%;overflow-x:auto;'>
        <table class='data-grid rfmgrid-wrap'>
            <thead>
            <tr><th class='data-grid-th'><span>Rating</span></th>
                <th class='data-grid-th'><span>Recency</span><p><span>
                            (days since last order)</span></p></th>
                <th class='data-grid-th'><span>Frequency</span><p><span>
                            (total orders placed)</span></p></th>
                <th class='data-grid-th'><span>Monetary</span><p>
                        <span>(total money spent)</span></p></th>
            </tr>
            </thead>
            <tbody>
            <tr class='data-row'>
                <td style='vertical-align: middle;text-align: center;padding:2rem;'><b>5</b></td>
                <td style='padding: 2rem;'>
                    <div class='input-wrap'>
                            <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                            Less Than:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;' type='number'
                               value='{$data['rfm_at_5']['recency']|escape:'htmlall':'UTF-8'}'
                        name='rfm_at_5_recency'/>
                    </div>
                </td>
                <td style='padding: 2rem;'>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           More Than:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;' type='number'
                               value='{$data['rfm_at_5']['frequency']|escape:'htmlall':'UTF-8'}'
                        name='rfm_at_5_frequency'/></div>
                </td>
                <td style='padding: 2rem;'>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           More Than:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['rfm_at_5']['monetary']|escape:'htmlall':'UTF-8'}'
                        name='rfm_at_5_monetary'/></div>
                </td>
            </tr>

            <tr class='data-row _odd-row'>
                <td style='vertical-align: middle;text-align: center;padding: 2rem;'><b>4</b></td>
                <td style='padding: 2rem;'>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           From:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['from_rfm_4']['recency']|escape:'htmlall':'UTF-8'}'
                        name='from_rfm_4_recency'/>
                    </div>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           To:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['to_rfm_4']['recency']|escape:'htmlall':'UTF-8'}'
                        name='to_rfm_4_recency'/></div>
                </td>
                <td style='padding: 2rem;'>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           From:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['from_rfm_4']['frequency']|escape:'htmlall':'UTF-8'}'
                        name='from_rfm_4_frequency'/>
                    </div>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           To:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['to_rfm_4']['frequency']|escape:'htmlall':'UTF-8'}'
                        name='to_rfm_4_frequency'/></div>
                </td>
                <td style='padding: 2rem;'>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           From:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['from_rfm_4']['monetary']|escape:'htmlall':'UTF-8'}'
                        name='from_rfm_4_monetary'/></div>

                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           To:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['to_rfm_4']['monetary']|escape:'htmlall':'UTF-8'}'
                        name='to_rfm_4_monetary'/></div>
                </td>
            </tr>

            <tr class='data-row'>
                <td style='vertical-align: middle;text-align: center;padding: 1rem;'><b>3</b></td>
                <td style='padding: 2rem;'>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           From:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['from_rfm_3']['recency']|escape:'htmlall':'UTF-8'}'
                        name='from_rfm_3_recency'/>
                    </div>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           To:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['to_rfm_3']['recency']|escape:'htmlall':'UTF-8'}'
                        name='to_rfm_3_recency'/></div>
                </td>
                <td style='padding: 2rem;'>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           From:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['from_rfm_3']['frequency']|escape:'htmlall':'UTF-8'}'
                        name='from_rfm_3_frequency'/>
                    </div>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           To:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['to_rfm_3']['frequency']|escape:'htmlall':'UTF-8'}'
                        name='to_rfm_3_frequency'/></div>
                </td>
                <td style='padding: 2rem;'>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           From:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['from_rfm_3']['monetary']|escape:'htmlall':'UTF-8'}'
                        name='from_rfm_3_monetary'/>
                    </div>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           To:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['to_rfm_3']['monetary']|escape:'htmlall':'UTF-8'}'
                        name='to_rfm_3_monetary'/></div>
                </td>
            </tr>

            <tr class='data-row _odd-row'>
                <td style='vertical-align: middle;text-align: center;padding: 1rem;'><b>2</b></td>
                <td style='padding: 2rem;'><div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           From:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['from_rfm_2']['recency']|escape:'htmlall':'UTF-8'}'
                        name='from_rfm_2_recency'/></div>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           To:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['to_rfm_2']['recency']|escape:'htmlall':'UTF-8'}'
                        name='to_rfm_2_recency'/></div></td>
                <td style='padding: 2rem;'><div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           From:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['from_rfm_2']['frequency']|escape:'htmlall':'UTF-8'}'
                        name='from_rfm_2_frequency'/></div>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           To:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['to_rfm_2']['frequency']|escape:'htmlall':'UTF-8'}'
                        name='to_rfm_2_frequency'/></div></td>
                <td style='padding: 2rem;'><div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           From:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['from_rfm_2']['monetary']|escape:'htmlall':'UTF-8'}'
                        name='from_rfm_2_monetary'/></div>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           To:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['to_rfm_2']['monetary']|escape:'htmlall':'UTF-8'}'
                        name='to_rfm_2_monetary'/></div></td>
            </tr>

            <tr>
                <td style='vertical-align: middle;text-align: center;padding: 2rem;'><b>1</b></td>
                <td style='padding: 2rem;'>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           More Than:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['rfm_at_1']['recency']|escape:'htmlall':'UTF-8'}'
                        name='rfm_at_1_recency'/></div></td>
                <td style='padding: 2rem;'>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           Less Than:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['rfm_at_1']['frequency']|escape:'htmlall':'UTF-8'}'
                        name='rfm_at_1_frequency'/></div></td>
                <td style='padding: 2rem;'>
                    <div class='input-wrap'>
                           <span class='' style='font-size:12px;font-weight: normal;float: left;min-width: 70px;'>
                           Less Than:</span>
                        <input class="form-control" style='width: calc(100% - 80px);margin-left: 5px;margin-bottom:10px;'
                               type='number' value='{$data['rfm_at_1']['monetary']|escape:'htmlall':'UTF-8'}'
                        name='rfm_at_1_monetary'/></div></td>
            </tr>
            </tbody>
        </table>
    </td></tr>
</div>