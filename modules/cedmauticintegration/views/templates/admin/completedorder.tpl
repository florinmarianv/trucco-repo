<!--
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the End User License Agreement(EULA)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://cedcommerce.com/license-agreement.txt
*
* @author    CedCommerce Core Team <connect@cedcommerce.com>
* @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
* @license   http://cedcommerce.com/license-agreement.txt
* @category  Ced
* @package   CedMauticIntegration
*/
-->
<div class="" style="position: relative; left: 0px; top: 0px;" data-slot="separator">
    <hr></div>
<div data-slot="text"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td style="border-bottom: 1px solid #f4f4f4; text-align: center;" width="33%">
                <strong>Item</strong>
            </td>
            <td style="border-bottom: 1px solid #f4f4f4; text-align: center" width="33%">
                <strong>Image</strong>
            </td>
            <td style="border-bottom: 1px solid #f4f4f4; text-align: center" width="33%">
                <strong> Feedback Link </strong>
            </td>
        </tr>

{foreach from=$lastCompletedOrderItems key=key item=type}
        <tr>
            <td width="33%" style="text-align: center">
                <strong>{$type.name|escape:'htmlall':'UTF-8'}
                </strong>
            </td>
            <td width="33%" style="text-align: center">
                <img src="{$type.image|escape:'quotes':'UTF-8'}" width="100px" height="100px"/>
            </td>
            <td width="33%" style="text-align: center">
                <a href = "{$type.url|escape:'quotes':'UTF-8'}" target="_blank"> Feedback for {$type.name|escape:'htmlall':'UTF-8'}</a>
            </td>
        </tr>
{/foreach}