<!--
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the End User License Agreement(EULA)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://cedcommerce.com/license-agreement.txt
*
* @author    CedCommerce Core Team <connect@cedcommerce.com>
* @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
* @license   http://cedcommerce.com/license-agreement.txt
* @category  Ced
* @package   CedMauticIntegration
*/
-->
<div class="" style="position: relative; left: 0px; top: 0px;" data-slot="separator"><hr></div>
<div data-slot="text">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td style="border-bottom: 1px solid #f4f4f4;" width="20%">
                    <strong>Item</strong>
            </td>
            <td style="border-bottom: 1px solid #f4f4f4;" width="20%">
                <strong>Image</strong>
            </td>
            <td style="border-bottom: 1px solid #f4f4f4;" width="20%">
                    <strong>Qty</strong>
            </td>
            <td style="border-bottom: 1px solid #f4f4f4;" width="20%">
                    <strong>Cost</strong>
            </td>
            <td style="border-bottom: 1px solid #f4f4f4;" width="20%">
                    <strong>Total</strong>
            </td>
        </tr>

{foreach from=$cartProducts key=key item=type}
        <tr>
            <td width="20%">
                <a href="{$type.url|escape:'quotes':'UTF-8'}" target="_blank"><strong>{$type.name|escape:'htmlall':'UTF-8'}</strong></a>
            </td>
            <td width="20%">
                <img src="{$type.image|escape:'quotes':'UTF-8'}" width="100px" height="100px"/>
            </td>
            <td width="20%">
                {$type.qty|escape:'htmlall':'UTF-8'}
            </td>
            <td width="20%">
                {$currencySymbol|escape:'htmlall':'UTF-8'}{$type.price|escape:'htmlall':'UTF-8'}
            </td>
            <td width="20%">
                {$currencySymbol|escape:'htmlall':'UTF-8'}{$type.total|escape:'htmlall':'UTF-8'}
            </td>
        </tr>
{/foreach}
        </tbody>
    </table>
</div>
<div class="" style="position: relative; left: 0px; top: 0px;" data-slot="separator"><hr></div>
