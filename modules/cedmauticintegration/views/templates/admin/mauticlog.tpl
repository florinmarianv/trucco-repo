<!--
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the End User License Agreement(EULA)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://cedcommerce.com/license-agreement.txt
*
* @author    CedCommerce Core Team <connect@cedcommerce.com>
* @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
* @license   http://cedcommerce.com/license-agreement.txt
* @category  Ced
* @package   CedMauticIntegration
*/
-->
<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td class="email-heading">
                        <h1>You have a new installation for Mautic PrestaShop.</h1>
                        <p> Please review your admin panel."</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <h4>Merchant Domain: {$domain|escape:'quotes':'UTF-8'}</h4>
        </td>
    </tr>
    <tr>
        <td>
            <h4>Merchant Email: {$merchant_mail|escape:'quotes':'UTF-8'}</h4>
        </td>
    </tr>
    <tr>
        <td>
            <h4>Installation Date: {$installation|escape:'htmlall':'UTF-8'}</h4>
        </td>
    </tr>
    <tr>
        <td>
            <h4>No Of Customers: {$customer_count|escape:'htmlall':'UTF-8'}</h4>
        </td>
    </tr>
</table>