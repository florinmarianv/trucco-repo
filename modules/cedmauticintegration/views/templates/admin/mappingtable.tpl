<!--
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the End User License Agreement(EULA)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://cedcommerce.com/license-agreement.txt
*
* @author    CedCommerce Core Team <connect@cedcommerce.com>
* @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
* @license   http://cedcommerce.com/license-agreement.txt
* @category  Ced
* @package   CedMauticIntegration
*/
-->
<table id='mapping_table'>
    <thead>
    <tr>
        <th>Mautic Contact Fields</th>
        <th>PrestaShop Customer Fields</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$mappingRowValues key=mappingKey item=mappingValue}
        <tr>
        <tr id='{$mappingKey|escape:'htmlall':'UTF-8'}'>
        <td>
            <select name='mautic_contact_fields_{$mappingKey|escape:'htmlall':'UTF-8'}'>
                {foreach from=$mauticContactFields key=mauticKey item=mauticValue} {
                    {if $mappingValue.mautic == $mauticValue.value}
                        <option value='{$mauticValue.value|escape:'htmlall':'UTF-8'}' selected>{$mauticValue.label|escape:'htmlall':'UTF-8'}</option>
                    {else}
                        <option value='{$mauticValue.value|escape:'htmlall':'UTF-8'}'>{$mauticValue.label|escape:'htmlall':'UTF-8'}</option>
                    {/if}
                {/foreach}
            </select>
        </td>
        <td>
            <select name='prestashop_customer_fields_{$mappingKey|escape:'htmlall':'UTF-8'}'>
                {foreach from=$prestashopContactFields key=prestaKey item=prestaValue} {
                    {if $mappingValue.prestashop == $prestaValue}
                        <option value='{$prestaValue|escape:'htmlall':'UTF-8'}' selected>{$prestaValue|escape:'htmlall':'UTF-8'}</option>
                    {else}
                        <option value='{$prestaValue|escape:'htmlall':'UTF-8'}'>{$prestaValue|escape:'htmlall':'UTF-8'}</option>
                    {/if}
                {/foreach}
            </select>
        </td>
        <td>
            <input type='button' class='btn btn-info' id='deletemappingrow-{$mappingKey|escape:'htmlall':'UTF-8'}'
                   data-rowid='{$mappingKey|escape:'htmlall':'UTF-8'}' value='Del' onclick='deleteRow(this);'/>
        </td>
        </tr>
    {/foreach}
    </tbody>
    <tfoot>
    <tr>
        <td colspan='3' style="border:none">
            <button type='button' class='btn btn-primary' id='addmappingrow'>
                <span>Add</span>
            </button>
        </td>
    </tr>
    </tfoot>
</table>
