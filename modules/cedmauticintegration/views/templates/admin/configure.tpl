<!--
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the End User License Agreement(EULA)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://cedcommerce.com/license-agreement.txt
*
* @author    CedCommerce Core Team <connect@cedcommerce.com>
* @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
* @license   http://cedcommerce.com/license-agreement.txt
* @category  Ced
* @package   CedMauticIntegration
*/
-->
<style>
    #mapping_table th, #mapping_table td {
        padding: 10px;
    }
    .data-grid-th{
        text-align: center;
    }
    .bootstrap th {
         text-align: center;
    }
    .fixed-width-xl {
        width : 75%;
    }
    input[type = 'text'], input[type = 'number'], select{
        width : 75% !important;
    }
    input[type="password"] {
        width : 74% !important;
    }
    label {
        font-weight: bold !important;
    }
</style>

<script>
     $(document).ready(function () {

         $('select').removeClass('fixed-width-xl');
         $('input[type="password"]').parent().removeClass('fixed-width-lg');

        $('#mautic_integration_mauticapi_integration_basic_authorize').on('click', function () {
            basicInstall();
        });

        function basicInstall() {
            {if $basic_authorize == true}
            left = ($(window).width() / 2) - (550 / 2);
            top = ($(window).height() / 2) - (550 / 2);
            window.open('{$basic_authorize_link|escape:'quotes'}', 'popup', "width=400, height=450, top=" + top + ", left=" + left);
            {else}
            $('#mautic_integration_mauticapi_integration_basic_authorize').after('<p class="ced-wish-loading fa fa-spinner" style="color:red;margin-left:2px">{$error_message|escape:'htmlall':'UTF-8'}</p>');

            {/if}
        }

         $('#mautic_integration_mauticapi_integration_oauth2_authorize').on('click', function () {
             generateCode();
         });

         function generateCode() {
             {if $oauth2_authorize == true}
             left = ($(window).width() / 2) - (550 / 2);
             top = ($(window).height() / 2) - (550 / 2);
             window.open('{$oauth2_authorize_link|escape:'quotes'}', 'popup', "width=400, height=450, top=" + top + ", left=" + left);
             {else}
             $('#mautic_integration_mauticapi_integration_oauth2_authorize').after('<p class="ced-wish-loading fa fa-spinner" style="color:red;margin-left:2px">{$error_message|escape:'htmlall':'UTF-8'}</p>');

         {/if}
         }

         ToggleEnableDisable();

         //ToggleOauthType();

         $('#addmappingrow').click(function () {
             let next_row = $("#mapping_table tbody").find("tr").length + 1;
             let new_row = "";
             let row_id = Math.random();
             new_row += "<tr id='"+row_id+"'><td><select name='mautic_contact_fields_"+row_id+"'>" +
                     {foreach from=$mauticContactFields key=key item=contactField}
                    "<option value='{$contactField.value|escape:'htmlall':'UTF-8'}'>{$contactField.label|escape:'htmlall':'UTF-8'}</option>" +
                     {/foreach}
                 "</select></td>" +
                 "<td><select name='prestashop_customer_fields_"+row_id+"'>" +
                     {foreach from=$prestashopContactFields key=key item=contactField}
                 "<option value='{$contactField|escape:'htmlall':'UTF-8'}'>{$contactField|escape:'htmlall':'UTF-8'}</option>" +
                     {/foreach}
                 "</select></td>" +
                 "<td><input type='button' class='btn btn-info' id='deletemappingrow-"+row_id+"' data-rowid='"+row_id+"' value='Del' onclick='deleteRow(this);'/> </td></tr>";
             $("#mapping_table tbody").append(new_row);
         });
         $('#deletemappingrow').click(function () {
             let rowId = this.data('rowid');
             $('#'+rowId+'').remove();
         });

         $('#mautic_integration_mauticapi_integration_enable').change(function () {
             ToggleEnableDisable();
         });

         $('#mautic_integration_mauticapi_integration_oauth_type').change(function () {
             ToggleOauthType();
         });

         $('#mautic_integration_mautic_property_groups_feedback').change(function () {
             ToggleCompletedOrderStatus();
         });

         $('#mautic_integration_mautic_property_groups_abandoned_cart').change(function () {
             ToggleAbandonedCartFieldset();
         });

         $('#mautic_integration_mautic_customer_export_type').change(function () {
             ToggleExportCustomerCronTime();
         });

         function ToggleEnableDisable() {
             if ($('#mautic_integration_mauticapi_integration_enable').val()==0) {
                 $('#mautic_integration_mauticapi_integration_oauth_type').parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_mautic_username').parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_mautic_password').parent().parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_client_id').parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_client_secret').parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_redirect_url').parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_oauth2_authorize').parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_basic_authorize').parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_mautic_url').parent().parent().hide();
                 $('#fieldset_1_1').hide();
                 $('#fieldset_2_2').hide();
                 $('#fieldset_3_3').hide();
                 $('#fieldset_4_4').hide();
                 $('#fieldset_5_5').hide();
                 $('#fieldset_6_6').hide();
                 $('.submitMauticConfiguration').show();
             } else if($('#mautic_integration_mauticapi_integration_enable').val()==1) {
                 ToggleOauthType();
                 $('#mautic_integration_mauticapi_integration_oauth_type').parent().parent().show();
                 $('#mautic_integration_mauticapi_integration_mautic_url').parent().parent().show();
                 $('#fieldset_1_1').show();
                 ToggleCompletedOrderStatus();
                 ToggleAbandonedCartFieldset();
                 $('#fieldset_3_3').show();
                 ToggleExportCustomerCronTime();
                 $('#fieldset_4_4').show();
                 $('#fieldset_5_5').show();
                 $('#fieldset_6_6').show();
                 $('.submitMauticConfiguration').hide();
             }
         }

         function ToggleOauthType() {
             if ($('#mautic_integration_mauticapi_integration_oauth_type').val()=='Oauth2') {
                 $('#mautic_integration_mauticapi_integration_mautic_username').parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_mautic_password').parent().parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_basic_authorize').parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_client_id').parent().parent().show();
                 $('#mautic_integration_mauticapi_integration_client_secret').parent().parent().show();
                 $('#mautic_integration_mauticapi_integration_redirect_url').parent().parent().show();
                 $('#mautic_integration_mauticapi_integration_oauth2_authorize').parent().parent().show();

             } else if($('#mautic_integration_mauticapi_integration_oauth_type').val()=='Basic') {
                 $('#mautic_integration_mauticapi_integration_mautic_username').parent().parent().show();
                 $('#mautic_integration_mauticapi_integration_mautic_password').parent().parent().parent().show();
                 $('#mautic_integration_mauticapi_integration_basic_authorize').parent().parent().show();
                 $('#mautic_integration_mauticapi_integration_client_id').parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_client_secret').parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_redirect_url').parent().parent().hide();
                 $('#mautic_integration_mauticapi_integration_oauth2_authorize').parent().parent().hide();
             }
         }

         function ToggleCompletedOrderStatus() {
             if($('#mautic_integration_mautic_property_groups_feedback').val() == 0) {
                 $('#mautic_integration_mautic_completed_order_status').parent().parent().hide();
             } else if($('#mautic_integration_mautic_property_groups_feedback').val() == 1) {
                 $('#mautic_integration_mautic_completed_order_status').parent().parent().show();
             }
         }

         function ToggleAbandonedCartFieldset() {
             if($('#mautic_integration_mautic_property_groups_abandoned_cart').val() == 0) {
                 $('#fieldset_2_2').hide();
             } else if($('#mautic_integration_mautic_property_groups_abandoned_cart').val() == 1) {
                 $('#fieldset_2_2').show();
             }
         }

         function ToggleExportCustomerCronTime() {
             if($('#mautic_integration_mautic_customer_export_type').val() == '{$observer}') {
                 $('#mautic_integration_mautic_customer_export_cron_time').parent().parent().hide();
             } else if($('#mautic_integration_mautic_customer_export_type').val() == '{$cron}') {
                 $('#mautic_integration_mautic_customer_export_cron_time').parent().parent().show();
             }
         }

     });

     function deleteRow(obj) {
         $(obj).closest("tr").remove();
     }

</script>
