<!--
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the End User License Agreement(EULA)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://cedcommerce.com/license-agreement.txt
*
* @author    CedCommerce Core Team <connect@cedcommerce.com>
* @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
* @license   http://cedcommerce.com/license-agreement.txt
* @category  Ced
* @package   CedMauticIntegration
*/
-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<style>
    * {
        box-sizing: border-box
    }

    .bootstrap h2 {
        font-family: sans-serif;
    }

    .tab {
        float: left;
        border: 1px solid #ccc;
        background-color: white;
        width: 25%;
        /* height: 300px;*/
        margin-right: 20px;
        margin-bottom: 30px;
    }

    /* Style the buttons inside the tab */
    .tab button {
        display: block;
        background-color: inherit;
        color: black;
        padding: 22px 16px;
        width: 100%;
        border: none;
        outline: none;
        text-align: left;
        cursor: pointer;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current "tab button" class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        background-color: white;
        float: left;
        padding: 0px 12px;
        border: 1px solid #ccc;
        width: 70%;
        border-left: none;
        max-height: 550px;
        overflow-y: scroll;
        overflow-x: hidden;
    }


    /*.connection-status {*/
        /*background: #ffffff none repeat scroll 0 0;*/
        /*border-radius: 3px;*/
        /*border: 1px solid #e3e3e3;*/
        /*max-width: 250px;*/
        /*overflow: hidden;*/
        /*padding: 20px 20px 50px;*/
        /*position: relative;*/
        /*text-align: center;*/
    /*}*/

    .connection-status .icon i {
        border-radius: 50%;
        font-size: 22px;
        height: 50px;
        line-height: 50px;
        margin-top: 15px;
        text-align: center;
        width: 50px;
    }

    .connection-status .item-title {
        display: block;
        margin: 25px 0;
    }

    .connection-status .item-description {
        bottom: 0;
        color: #ffffff;
        display: block;
        font-weight: bold;
        left: 0;
        line-height: 50px;
        /*position: absolute;*/
        text-align: center;
        width: 100%;
    }

    .connection-status.connected .icon i {
        background: #efffe5 none repeat scroll 0 0;
        border: 1px solid #92dd5a;
        color: #92dd5a;
    }

    .connection-status.connected .item-description {
        background: #92dd5a none repeat scroll 0 0;
    }

    .connection-status.disconnected .icon i {
        background: #FFF2F3 none repeat scroll 0 0;
        border: 1px solid #eb505d;
        color: #eb505d;
    }

    .connection-status.disconnected .item-description {
        background: #eb505d none repeat scroll 0 0;
    }

    .export-btn .btn.btn-info {
        background: #eb5202 none repeat scroll 0 0;
        border-radius: 2px;
        color: #ffffff;
        display: inline-block;
        margin: 3%;
        /*float: right;*/
        padding: 12px 30px;
        text-align: center;
        text-decoration: none;
    }

    .contect-table-wrapper {
        max-height: 550px;
        overflow-y: auto;
        overflow-x: hidden;
    }

    .contect-table-wrapper .fa-check {
        color: #4dae4f;
    }

    .tabcontent table {
        width: 100%;
    }

    .tabcontent th {
        background-color: black;
        color: white;
        padding: 10px;
        text-align: left;
    }

    .tabcontent td {
        padding: 10px;
        font-size: 15px;
    }

</style>


<div class="bootstrap error-message">
</div>

<h2>Manage Mautic Settings</h2>

<div class="tab">
    <div class="row">
        <div class="col-12 col-md-12">
            <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen" style="font-size: 110%">General Information</button>
            <button class="tablinks" onclick="openCity(event, 'Paris')" style="font-size: 110%">Contact Properties</button>
            <button class="tablinks" onclick="openCity(event, 'Tokyo')" style="font-size: 110%">Segments</button>
        </div>
    </div>
</div>

<div id="London" class="tabcontent" style="overflow-y: hidden;">
    <div class="row" style="margin-top: 6%">
        <div class="col-12 col-md-12">
            <h3 style="margin-left: 2%">General Information</h3>
        </div>
        <div class="col-12 col-md-12">
            <div class="control">
                <div class="row">
                {if $status && $enabled}

                    <div class="col-12 col-md-12" style="margin-left: 30%; width: 40%;">
                        <div class="connection-status connected" style="border: 1px solid #e3e3e3;">
                            <div class="row" >
                                <div class="col-12 col-md-12 text-center">
                            <div class="icon"><i class="fa fa-check"></i></div>
                                </div>
                                <div class="col-12 col-md-12 text-center">
                            <span class="item-title">Connection Status</span>
                                </div>
                                <div class="col-12 col-md-12">
                            <span class="item-description">Connected</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 text-center">
                        <div class="export-btn">
                            <a class="btn btn-info" href="{$export_customer_link|escape:'quotes'}">
                                <span class="item-description">Export Customers</span>
                            </a>
                        </div>
                    </div>
                {else}
                    <div class="col-12 col-md-12" style="margin-left: 30%; width: 40%; margin-bottom: 2%;">
                        <div class="connection-status disconnected" style="border: 1px solid #e3e3e3;">
                            <div class="row" >
                                <div class="col-12 col-md-12 text-center">
                            <div class="icon"><i class="fa fa-close"></i></div>
                                </div>
                                <div class="col-12 col-md-12 text-center">
                            <span class="item-title">Connection Status</span>
                                </div>
                                <div class="col-12 col-md-12">
                            <span class="item-description">Not Connected</span>
                                </div>
                            </div>
                        </div>
                    </div>

                {/if}
                </div>
            </div>
        </div>
    </div>
</div>

<div id="Paris" class="tabcontent">
    {if $status && $enabled}
        <table>
            <thead>
            <tr>
                <th>Contact Property Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$properties key=key item=type}
                <tr>
                    <td> {$type.name|escape:'htmlall':'UTF-8'}</td>
                    <td>{if $type.mautic_id==0}
                            <button type="button" title="create"
                                    class="btn btn-outline-primary property-create"
                                    id="{$type.code|escape:'htmlall':'UTF-8'}" data-property-code="{$type.code|escape:'htmlall':'UTF-8'}">Create
                            </button>
                        {else}
                            <i class="fa fa-check"></i>
                        {/if}
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    {else}
        <div class="col-12 col-md-12" style="margin-left: 30%; width: 40%; margin-bottom: 2%;margin-top: 10px;">
            <div class="connection-status disconnected" style="border: 1px solid #e3e3e3;">
                <div class="row" >
                    <div class="col-12 col-md-12 text-center">
                        <div class="icon"><i class="fa fa-close"></i></div>
                    </div>
                    <div class="col-12 col-md-12 text-center">
                        <span class="item-title">Connection Status</span>
                    </div>
                    <div class="col-12 col-md-12">
                        <span class="item-description">Not Connected</span>
                    </div>
                </div>
            </div>
        </div>
    {/if}

</div>

<div id="Tokyo" class="tabcontent">
    {if $status && $enabled}
        <table>
            <thead>
            <tr>
                <th>Segments Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$segments key=key item=type}
                <tr>
                    <td> {$type.name|escape:'htmlall':'UTF-8'}</td>
                    <td>{if $type.mautic_id==0}
                            <button type="button" title="create"
                                    class="btn btn-outline-primary segment-create"
                                    id="{$type.code|escape:'htmlall':'UTF-8'}" data-segment-code="{$type.code|escape:'htmlall':'UTF-8'}">Create
                            </button>
                        {else}
                            <i class="fa fa-check"></i>
                        {/if}
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    {else}
        <div class="col-12 col-md-12" style="margin-left: 30%; width: 40%; margin-bottom: 2%;margin-top: 10px;">
            <div class="connection-status disconnected" style="border: 1px solid #e3e3e3;">
                <div class="row" >
                    <div class="col-12 col-md-12 text-center">
                        <div class="icon"><i class="fa fa-close"></i></div>
                    </div>
                    <div class="col-12 col-md-12 text-center">
                        <span class="item-title">Connection Status</span>
                    </div>
                    <div class="col-12 col-md-12">
                        <span class="item-description">Not Connected</span>
                    </div>
                </div>
            </div>
        </div>
    {/if}
</div>


<script>


    function openCity(evt, cityName) {
        let i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();


    hideSuccessMessage();
    hideErrorMessage();
    $(document).ready(function () {
        $('.ajax-response').hide();
        $('.segment-create').click(function () {
            singleSegmentCreate($(this).data('segment-code'));
        });

        $('.property-create').click(function () {
            singlePropertyCreate($(this).data('property-code'));
        });

        function singlePropertyCreate(code) {
            $.ajax({
                url: '{$create_single_property_link|escape:'quotes'}',
                data: "alias=" + code + "&type=properties",
                datatype: JSON,
                type: 'POST'
            }).done(function (transport) {
                transport = JSON.parse(transport);
                if (transport.status === 'true') {
                    $('#' + code).parent().html('<i class="fa fa-check"></i>');
                    if (transport.hasOwnProperty('success')) {
                        $msg = ' <div class="alert alert-success">\n' +
                            ' <button type="button" class="close" data-dismiss="alert">×</button>\n' +
                            transport.success +
                            '</div>';
                        $('.error-message').html($msg);
                        //$('.ajax-response').show().html(transport.success).removeClass('danger').addClass('success');
                    }
                } else {
                    if (transport.hasOwnProperty('error')) {
                        $msg = '<div class="alert alert-danger">\n' +
                            '<button type="button" class="close" data-dismiss="alert">×</button>\n' +
                            transport.error +
                            '</div>';
                        $('.error-message').html($msg);
                        // $('.ajax-response').show().html(transport.error).removeClass('success').addClass('danger');
                    }
                }
                console.log(transport);
                //location.reload(true);
            })
        }

        function singleSegmentCreate(code) {
            $.ajax({
                url: '{$create_single_property_link|escape:'quotes'}',
                data: "alias=" + code + "&type=segments",
                datatype: JSON,
                type: 'POST'
            }).done(function (transport) {
                transport = JSON.parse(transport);
                if (transport.status === 'true') {
                    $('#' + code).parent().html('<i class="fa fa-check"></i>');
                    if (transport.hasOwnProperty('success')) {
                        $msg = ' <div class="alert alert-success">\n' +
                            ' <button type="button" class="close" data-dismiss="alert">×</button>\n' +
                            transport.success +
                            '</div>';
                        $('.error-message').html($msg);
                        //  $('.ajax-response').show().html(transport.success).removeClass('danger').addClass('success');
                    }
                } else {
                    if (transport.hasOwnProperty('error')) {
                        $msg = '<div class="alert alert-danger">\n' +
                            '<button type="button" class="close" data-dismiss="alert">×</button>\n' +
                            transport.error +
                            '</div>';
                        $('.error-message').html($msg);
                        //  $('.ajax-response').show().html(transport.error).removeClass('success').addClass('danger');
                    }
                }
                console.log(transport);
                //location.reload(true);
            });
        }
    });

    function hideSuccessMessage() {
        setTimeout(function () {
            $('.success').hide();
        }, 3000);
    }

    function hideErrorMessage() {
        setTimeout(function () {
            $('.error').hide();
        }, 3000);
    }

</script>