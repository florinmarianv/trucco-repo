<!--
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the End User License Agreement(EULA)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://cedcommerce.com/license-agreement.txt
*
* @author    CedCommerce Core Team <connect@cedcommerce.com>
* @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
* @license   http://cedcommerce.com/license-agreement.txt
* @category  Ced
* @package   CedMauticIntegration
*/
-->
<style>
    #mautic-responses {
        padding: 0;
    }
    #mautic-responses .success {
        background: #e0efd8 none repeat scroll 0 0;
        border: 1px solid #d1e0c9;
        border-radius: 2px;
        color: #3c753e;
        padding: 10px;
    }
    #mautic-responses .warning {
        background: #FDF8E4 none repeat scroll 0 0;
        border: 1px solid #ece7d3;
        border-radius: 2px;
        color: #976D3B;
        padding: 10px;
    }
    #mautic-responses .warning {
        background: #FDF8E4 none repeat scroll 0 0;
        border: 1px solid #ece7d3;
        border-radius: 2px;
        color: #976D3B;
        padding: 10px;
    }
    #mautic-responses .error {
        background: #f2dedf none repeat scroll 0 0;
        border: 1px solid #e1cdce;
        border-radius: 2px;
        color: #a94341;
        padding: 10px;
    }
</style>

{if ($connection_status) || $oauth_type == 'Basic'}
    <h1 id="response-heading"></h1>
    <ul style="list-style: none" id="mautic-responses">
        <li class="success">
            <span>Token Generated Successfully.</span>
        </li>
        <li id="mautic-properties" class="warning">
            <span>Properties Creation is in process, Please do not close the window.</span>
            <i class="fa fa-spinner fa-spin" style="font-size:25px"></i>
        </li>
    </ul>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>

            $(document).ready(function () {

                $.ajax({
                    url: '{$export_property_url|escape:'htmlall':'UTF-8'}',
                    data: "field=properties",
                    datatype: JSON,
                    type: 'POST'
                }).done(function (transport) {
                    transport = JSON.parse(transport);
                    if (transport['errors']) {
                        $errorMessage = '<li class="error">' + transport['errors'] + '</li>';
                        console.log(transport);
                        jQuery('#mautic-responses').html($errorMessage);
                    } else {
                        console.log(transport);
                        $('#mautic-properties').html(transport[0]);
                        $('#mautic-properties').removeClass('warning');
                        $('#mautic-properties').addClass('success');
                        var appendHtml = '<li id="mautic-segments" class="warning">' + transport[1] + '</li>';
                        $('#mautic-responses').append(appendHtml);

                        $.ajax({
                            url: '{$export_property_url|escape:'htmlall':'UTF-8'}',
                            data: "field=segments",
                            datatype: JSON,
                            type: 'POST'
                        }).done(function (transport) {
                            transport = JSON.parse(transport);
                            if (transport['errors']) {
                                $errorMessage = '<li class="error">' + transport['errors'] + '</li>';
                                $('#mautic-responses').html($errorMessage);
                            } else {
                                console.log(transport);
                                $('#mautic-segments').html(transport[0]);
                                $('#mautic-segments').removeClass('warning');
                                $('#mautic-segments').addClass('success');
                                $('#response-heading').html(transport[1]);
                               closeWindow();
                            }
                        });
                    }
                });



                function closeWindow() {
                    setTimeout(function () {
                        window.close();
                    }, 3000);
                }
            });

    </script>
{else}
    <ul style="list-style: none" id="mautic-responses">
        <li class="error">Authorization denied, invalid credentials.</li>
    </ul>
{/if}