<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

include_once dirname(__FILE__) . '/../../config/config.inc.php';
include_once dirname(__FILE__) . '/classes/ConnectionManager.php';
include_once dirname(__FILE__) . '/classes/Properties.php';
include_once dirname(__FILE__) . '/classes/ExportCustomers.php';

if (!Tools::getIsset('secure_key')
    || Tools::getValue('secure_key') !=
    Configuration::get('mautic_integration_mauticapi_integration_cron_secure_key')) {
    die('Cron Secure key does not match');
}
if (Configuration::get('mautic_integration_mauticapi_integration_enable') &&
    Configuration::get('mautic_integration_mauticapi_integration_connection_established') &&
    Configuration::get('mautic_integration_mautic_customer_export_type') == Properties::EXPORT_TYPE_CRON) {
    $connectionManager = new ConnectionManager();
    $exportCustomers = new ExportCustomers();
    try {
        $exportCustomers->exportCustomersByCron();
        die('Customers exported successfully.');
    } catch (\Exception $e) {
        $connectionManager->createLog('Exception in customer export by cron:-' . $e->getMessage(), '', '', '');
        die($e->getMessage());
    }
} else {
    die('Connection is closed.');
}
