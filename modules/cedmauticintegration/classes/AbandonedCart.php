<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

include_once 'Properties.php';
include_once 'ConnectionManager.php';
include_once 'ExportPropertiesAndSegments.php';

class AbandonedCart extends Module
{
    public $properties;
    public $connectionManager;
    public $exportProperties;
    public $productObject;
    public $carts_id_lang;

    public function __construct()
    {
        parent::__construct();
        $this->properties = new Properties();
        $this->connectionManager = new ConnectionManager();
        $this->exportProperties = new ExportPropertiesAndSegments();
    }

    /**
     * @param $carts
     * @param $properties
     * @param $arr
     * @return mixed
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function getAbandonedCartDetails($carts, $arr)
    {
        if ($carts == null || !$this->connectionManager->isCustomerGroupEnabled('abandoned_cart')) {
            $arr['ced_abncart_stat'] = 'no';
            $arr['ced_abncart_prod_html'] = " ";
            $arr['ced_abncart_total'] = 0;
            return $arr;
        }
        $this->carts_id_lang = reset($carts)['id_lang'];
        $properties = $this->properties->allProperties('abandoned_cart');
        $key = 0;
        $cartProductsCount = 0;
        $cartTotal = 0;
        $cart_products = array();

        foreach ($carts as $cart) {
            foreach ($cart['cart_product'] as $cartItem) {
                $cartProduct = $this->getProductData($cartItem['id_product']);
                if (is_array($cartProduct->name)) {
                    $cartProduct->name = reset($cartProduct->name);
                }
                $cart_products[$key]["name"] = $cartProduct->name;

                $cartCategory = Category::getLinkRewrite($cartProduct->id_category_default, $this->carts_id_lang);
                $cart_products[$key]["url"] = $this->getProductUrl($cartProduct, $cartCategory);
                $cart_products[$key]["image"] = $this->getImageUrl($cartProduct);
                $cart_products[$key]["price"] = $cartProduct->price;
                $cart_products[$key]["qty"] = $cartItem['quantity'];
                $cart_products[$key]["total"] = $cartItem['quantity'] * $cartProduct->price;
                $cartProductsCount += $cartItem['quantity'];
                $cartTotal += $cart_products[$key]["total"];
                $key++;
            }
            $currency = new Currency((int)$cart['id_currency']);
        }

        foreach ($properties as $property) {
            if ($this->exportProperties->canSetProperty($property['alias'], CedMautic::TYPE_PROPERTY)) {
                switch ($property['alias']) {
                    case 'ced_abncart_stat':
                        $arr['ced_abncart_stat'] = 'yes';
                        break;

                    case 'ced_abncart_prod_html':
                        if (isset($cart_products)) {
                            $arr['ced_abncart_prod_html'] = $this->getProductsHtml($cart_products, $currency);
                        }
                        break;

                    case 'ced_abncart_prods_count':
                        $arr['ced_abncart_prods_count'] = $cartProductsCount;
                        break;

                    case 'ced_abncart_total':
                        $arr['ced_abncart_total'] = $cartTotal;
                        break;
                }
            }
        }
        return $arr;
    }

    public function getProductsHtml($cart_products, $currency)
    {
        $currencySymbol = "";
        if ($currency) {
            $currencySymbol = $currency->sign;
        }
        $products_html = "";

        $this->context->smarty->assign(
            array(
                'cartProducts' => $cart_products,
                'currencySymbol' => $currencySymbol
            )
        );

        if (count($cart_products)) {
            $products_html = $this->context->smarty->fetch(_PS_MODULE_DIR_ .
                'cedmauticintegration/views/templates/admin/abandonedcart.tpl');
        }

        return $products_html;
    }

    public function getProductData($productId)
    {
        if (isset($this->productObject[$productId])) {
            return $this->productObject[$productId];
        } else {
            $this->productObject[$productId] = new Product((int)$productId);
            return $this->productObject[$productId];
        }
    }

    public function getImageUrl($product)
    {
        $productId = $product->id;
        $image = Image::getCover($productId);
        $link = new Link();
        if (is_array($product->link_rewrite)) {
            $product->link_rewrite = reset($product->link_rewrite);
        }
        $imageUrl = $link->getImageLink($product->link_rewrite, $image['id_image']);
        return $imageUrl;
    }

    public function getProductUrl($product, $category)
    {
        $link = new Link();
        $productUrl = $link->getProductLink($product, null, $category);
        return $productUrl;
    }
}
