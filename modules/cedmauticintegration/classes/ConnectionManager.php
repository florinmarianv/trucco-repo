<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

include_once 'CedMautic.php';
include_once 'Properties.php';

class ConnectionManager extends Module
{
    public $properties;
    public $oauth2AccessToken = '';
    public $oauth2RefreshToken = '';
    public $oauth2TokenExpiresIn = '';
    public $clientId = '';
    public $clientSecret = '';
    public $mauticUrl = '';
    public $redirecturl = '';
    public $oauth2Code = '';
    public $mauticUsername = "";
    public $mauticPassword = "";
    public $oauthType = "";

    /**
     * ConnectionManager constructor.
     * @throws PrestaShopDatabaseException
     */
    public function __construct()
    {
        parent::__construct();
        $this->properties = new Properties();
        $this->prepareConfiguration();
    }


    /**
     * Prepare the configuration for Mautic
     *
     * @throws PrestaShopDatabaseException
     */
    public function prepareConfiguration()
    {
        $this->mauticUrl = $this->getMauticConfig('mautic_url');
        $this->redirecturl = $this->getRedirectUrl();
        $this->oauthType = $this->getMauticConfig('oauth_type');
        if ($this->oauthType == 'Oauth2') {
            $this->clientId = $this->getMauticConfig('client_id');
            $this->clientSecret = $this->getMauticConfig('client_secret');
            $this->oauth2AccessToken = $this->getMauticConfig('oauth2_access_token');
            $this->oauth2RefreshToken = $this->getMauticConfig('oauth2_refresh_token');
            $this->oauth2Code = $this->getMauticConfig('oauth2_code');
            $this->oauth2TokenExpiresIn = $this->getMauticConfig('oauth2_token_expires_in');
        } elseif ($this->oauthType == 'Basic') {
            $this->mauticUsername = $this->getMauticConfig('mautic_username');
            $this->mauticPassword = $this->getMauticConfig('mautic_password');
        }
    }

    /**
     * @param $code
     * @return array
     */
    public function getToken($code)
    {
        if ($this->clientId == null || $this->clientSecret == null || $this->mauticUrl == null) {
            return array('success' => false, 'message' => 'Please Fill Mautic Credential(s).');
        }
        $params = array(
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'grant_type' => 'authorization_code',
            'redirect_uri' => $this->redirecturl,
            'code' => $code
        );

        $post_params = $this->prepareQueryArguments($params);
        $endpoint = '/oauth/v2/token';
        $headers = array();
        $tokenResponse = $this->postRequest($endpoint, $post_params, $headers, "");
        $response = json_decode($tokenResponse['response'], true);
        if ($tokenResponse['status_code'] == 200 && $response != null) {
            if (isset($response['expires_in'])) {
                $this->setMauticConfig('oauth2_token_expires_in', time() + $response['expires_in'] - 5);
                $this->oauth2TokenExpiresIn = time() + $response['expires_in'] - 5;
            }
            if (isset($response['refresh_token'])) {
                $this->setMauticConfig('oauth2_refresh_token', $response['refresh_token']);
                $this->oauth2RefreshToken = $response['refresh_token'];
            }
            if (isset($response['access_token'])) {
                $this->setMauticConfig('oauth2_access_token', $response['access_token']);
                $this->oauth2AccessToken = $response['access_token'];
                return array('success' => true, 'data' => $response['access_token']);
            }
            if (isset($response['errors'])) {
                return $response;
            }
        } else {
            return array('success' => false, 'message' => 'Some Problem Occured');
        }
    }

    /**
     * @param null $token
     * @return bool|mixed|null|string
     */
    public function validateToken($token = null)
    {
        if ($token === null) {
            $token = $this->oauth2AccessToken;
        }
        $expiry_time = $this->oauth2TokenExpiresIn;
        if (time() < $expiry_time) {
            $this->oauth2AccessToken = $token;
        } else {
            $refresh_token = $this->refreshToken();
            if ($refresh_token && $refresh_token['success']) {
                $this->oauth2AccessToken = $refresh_token['data'];
            } else {
                $this->oauth2AccessToken = false;
            }
        }
        return $this->oauth2AccessToken;
    }

    /**
     * @return array
     */
    public function refreshToken()
    {
        if ($this->clientId == null || $this->clientSecret == null || $this->oauth2RefreshToken == null ||
            $this->mauticUrl == null) {
            return array('success' => false, 'message' => 'Please Fill Mautic Credential(s).');
        }
        $params = array(
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->oauth2RefreshToken,
            'redirect_uri' => $this->redirecturl

        );

        $post_params = $this->prepareQueryArguments($params);
        $endpoint = '/oauth/v2/token';
        $headers = array();
        $tokenResponse = $this->postRequest($endpoint, $post_params, $headers, "");
        $response = json_decode($tokenResponse['response'], true);
        if ($tokenResponse['status_code'] == 200 && $response != null) {
            if (isset($response['expires_in'])) {
                $this->setMauticConfig('oauth2_token_expires_in', time() + $response['expires_in'] - 5);
                $this->oauth2TokenExpiresIn = time() + $response['expires_in'] - 5;
            }
            if (isset($response['refresh_token'])) {
                $this->setMauticConfig('oauth2_refresh_token', $response['refresh_token']);
                $this->oauth2RefreshToken = $response['refresh_token'];
            }
            if (isset($response['access_token'])) {
                $this->setMauticConfig('oauth2_access_token', $response['access_token']);
                $this->oauth2AccessToken = $response['access_token'];
                return array('success' => true, 'data' => $response['access_token']);
            } else {
                return array('success' => false, 'message' => 'some problem occured');
            }
        } else {
            return array('success' => false, 'message' => 'Some Problem Occured');
        }
    }

    /**
     * @param $endpoint
     * @param $get_params
     * @return array
     */
    public function getRequest($endpoint, $get_params, $headers, $authType)
    {
        if (!empty($get_params)) {
            $url = $this->mauticUrl . $endpoint . '?' . $this->prepareQueryArguments($get_params);
        } else {
            $url = $this->mauticUrl . $endpoint;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_HTTPAUTH, $authType);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $response = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $decodedResponse = json_decode($response, true);
        if (isset($decodedResponse['errors'])) {
            $this->createLog($url, $response, json_encode($get_params), $status_code);
        }

        return array(
            'status_code' => $status_code,
            'response' => $response,
        );
    }

    /**
     * @param $endpoint
     * @param $post_params
     * @param $headers
     * @param $authType
     * @return array
     */
    public function postRequest($endpoint, $post_params, $headers, $authType)
    {
        if ($this->mauticUrl == null) {
            return array('success' => false, 'message' => 'Please Fill Mautic Credential(s).');
        }
        $url = $this->mauticUrl . $endpoint;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_HTTPAUTH, $authType);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $response = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $decodedResponse = json_decode($response, true);
        if (isset($decodedResponse['errors'])) {
            $this->createLog($url, $response, $post_params, $status_code);
        }

        return array(
            'status_code' => $status_code,
            'response' => $response
        );
    }

    /**
     * @param $endpoint
     * @param $params
     * @return array
     */
    public function createRequest($method, $endpoint, $params)
    {
        if ($this->oauthType == null) {
            return array('success' => false, 'message' => 'Please Fill Mautic Credential(s).');
        }

        if ($this->oauthType == 'Oauth2') {
            if ($this->clientId == null || $this->clientSecret == null || $this->mauticUrl == null) {
                return array('success' => false, 'message' => 'Please Fill Mautic Credential(s).');
            }
            if ($method == 'POST') {
                $params['access_token'] = $this->validateToken();
                $post_params = http_build_query($params);
                $headers = array();
                $authType = "";
                $response = $this->postRequest($endpoint, $post_params, $headers, $authType);
                return $response;
            } elseif ($method == 'GET') {
                $headers = array();
                $authType = "";
                $params['access_token'] = $this->validateToken();
                $response = $this->getRequest($endpoint, $params, $headers, $authType);
                return $response;
            }
        } elseif ($this->oauthType == 'Basic') {
            if ($this->mauticUsername == null || $this->mauticPassword == null) {
                return array('success' => false, 'message' => 'Please Fill Mautic Credential(s).');
            }
            $credentials = $this->mauticUsername . ':' . $this->mauticPassword;
            // add basic code here, modify encoded credentials
            $encodedCredentials = $credentials;
            $headers = array('Authorization: Basic ' . $encodedCredentials);
            $authType = 'CURLAUTH_BASIC';
            if ($method == 'POST') {
                $post_params = http_build_query($params);
                $response = $this->postRequest($endpoint, $post_params, $headers, $authType);
                return $response;
            } elseif ($method == 'GET') {
                $response = $this->getRequest($endpoint, $params, $headers, $authType);
                return $response;
            }
        }
    }

    /**
     * @return array
     */
    public function getListOfFields()
    {
        $params = array();
        $endpoint = '/api/fields/contact';
        $method = 'GET';
        $params['limit'] = 200;
        $response = $this->createRequest($method, $endpoint, $params);
        return $response;
    }

    public function getListOfSegments()
    {
        $params = array();
        $endpoint = '/api/segments';
        $method = 'GET';
        $params['limit'] = 200;
        $response = $this->createRequest($method, $endpoint, $params);
        return $response;
    }

    /**
     * @return mixed|null
     */
    public function getClientId()
    {
        return $this->getMauticConfig('client_id');
    }

    /**
     * @return mixed|null
     */
    public function getClientSecret()
    {
        return $this->getMauticConfig('client_secret');
    }

    /**
     * @return mixed|null
     */
    public function getMauticUrl()
    {
        return $this->getMauticConfig('mautic_url');
    }

    /**
     * @return mixed|null
     */
    public function getCode()
    {
        return $this->getMauticConfig('oauth2_code');
    }

    /**
     * @return mixed|null
     */
    public function getAccessToken()
    {
        return $this->getMauticConfig('oauth2_access_token');
    }

    /**
     * @return mixed|null
     */
    public function getRefreshToken()
    {
        return $this->getMauticConfig('oauth2_refresh_token');
    }

    /**
     * @return mixed|null
     */
    public function getExpiryTime()
    {
        return $this->getMauticConfig('oauth2_token_expires_in');
    }


    public function getRedirectUrl()
    {
        $redirectUrl = $this->context->link->getModuleLink('cedmauticintegration', 'getcode');
        return urlencode($redirectUrl);
    }

    public function getModuleStatus()
    {
        return $this->getMauticConfig('enable');
    }

    /**
     * @param $field
     * @return mixed|null
     */
    public function getMauticConfig($field)
    {
        if (!$field) {
            return null;
        }
        $path = 'mautic_integration_mauticapi_integration_' . $field;
        return Configuration::get($path);
    }

    public function setMauticConfig($field, $value)
    {
        if (!$field) {
            return null;
        }
        $path = 'mautic_integration_mauticapi_integration_' . $field;
        return Configuration::updateValue($path, $value);
    }

    public function isCustomerGroupEnabled($field)
    {
        if (!$field) {
            return null;
        }

        $path = 'mautic_integration_mautic_property_groups_' . $field;
        return Configuration::get($path);
    }

    /**
     * @param $args
     * @return string
     */
    public function prepareQueryArguments($args, $returnArray = false, $query = array(), $key = '')
    {
        foreach ($args as $k => $v) {
            if (is_array($v)) {
                $query = $this->prepareQueryArguments($v, true, $query, $k);
            } else {
                if ($key) {
                    $k = $key;
                }
                $query[] = $k . '=' . $v;
            }
        }
        return $returnArray ? $query : implode('&', $query);
    }

    /**
     * @param $url
     * @param $response
     * @param array $postParams
     */
    public function createLog($url, $response, $postParams = array(), $statuscode = 0)
    {

        $log = "URL: " . $url . PHP_EOL .
            "PostParam: " . json_encode($postParams) . PHP_EOL .
            "status:" . $statuscode . PHP_EOL .
            "Response: " . $response . PHP_EOL;
        $outputDir = _PS_MODULE_DIR_ . 'cedmauticintegration/logs/';
        if (!is_dir($outputDir)) {
            mkdir($outputDir, 0777, true);
        }
        try {
            $outputFile = _PS_MODULE_DIR_ . 'cedmauticintegration/logs/mautic_api.log';
            $fp = fopen($outputFile, 'a');
            fwrite($fp, $log);
            fclose($fp);
        } catch (\Exception $e) {
        }
    }
}
