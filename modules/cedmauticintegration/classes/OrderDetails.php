<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

include_once 'Properties.php';
include_once 'ConnectionManager.php';
include_once 'ExportPropertiesAndSegments.php';

class OrderDetails extends Module
{
    public $properties;
    public $connectionManager;
    public $exportProperties;
    public $orderDetails;
    public $lastOrderDetails;
    public $firstOrderDetails;
    public $lastCompletedOrderDetails;
    public $productObject;
    public $categoryName;
    public $countryName;
    public $carrier;
    public $orderStatus;
    public $stateName;
    public $order_id_lang;

    public function __construct()
    {
        parent::__construct();
        $this->properties = new Properties();
        $this->connectionManager = new ConnectionManager();
        $this->exportProperties = new ExportPropertiesAndSegments();
    }

    public function orderDetailsToExport($customer, $orders, $completedOrder, $arr)
    {
        $this->orderDetails = array();
        $this->lastOrderDetails = array();
        $this->firstOrderDetails = array();
        $this->lastCompletedOrderDetails = array();

        $shoppingCartProperties = $this->properties->allProperties('shopping_cart_fields');

        if ($this->connectionManager->isCustomerGroupEnabled('shopping_cart_fields')) {
            $arr = $this->getShoppingCartFields($customer, $orders, $shoppingCartProperties, $arr);
        }

        if ($orders != null) {
            $this->order_id_lang = reset($orders)['id_lang'];
            $this->setOrderDetails($orders);
            $this->setLastOrderDetails(end($orders));
            $this->setFirstOrderDetails(reset($orders));
            $orderProperties = $this->properties->allProperties('order');
            $arr = $this->getOrderGroupDetails($orderProperties, $arr);
            $lastProductProperties = $this->properties->allProperties('last_products_bought');
            $arr = $this->getLastProductsBought($lastProductProperties, $arr);
            $categoriesBoughtProperties = $this->properties->allProperties('categories_bought');
            $arr = $this->getCategoriesBought($categoriesBoughtProperties, $arr);
            $rfmProperties = $this->properties->allProperties('rfm_fields');
            $arr = $this->getRfmFields($rfmProperties, $arr);
            $skusProperties = $this->properties->allProperties('skus_bought');
            $arr = $this->getSkusBought($skusProperties, $arr);
            $feedback = $this->connectionManager->isCustomerGroupEnabled('feedback');
            if ($feedback && $completedOrder != null) {
                $this->setLastCompletedOrder($completedOrder);
                $feedbackProperties = $this->properties->allProperties('order_feedback');
                $arr = $this->getLastCompletedOrder($feedbackProperties, $arr);
            } else {
                $arr['ced_last_comp_order_num'] = 0;
                $arr['ced_feedback_html'] = " ";
            }
        } else {
            $arr['ced_order_monetary'] = 1;
            $arr['ced_order_frequency'] = 1;
            $arr['ced_order_recency'] = 1;
        }

        return $arr;
    }

    /**
     * @param $orders
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function setOrderDetails($orders)
    {
        $this->orderDetails['totalProductsBought'] = 0;
        $this->orderDetails['totalOrderValues'] = 0;
        $this->orderDetails['productsBought'] = array();
        $this->orderDetails['totalOrders'] = count($orders);

        foreach ($orders as $order) {
            foreach ($order['order_detail'] as $orderItem) {
                array_push(
                    $this->orderDetails['productsBought'],
                    $orderItem['product_name']
                );
                $this->orderDetails['totalProductsBought'] += $orderItem['product_quantity'];
                $productId = $orderItem['product_id'];
                $product = array();
                $product[$productId] = $this->getProductData($orderItem['product_id']);
                $category = $this->getCategoryName($order['id_lang'], $product[$productId]->id_category_default);
                $this->orderDetails['categoriesBought'][$category] = $category;
                $sku = $product[$productId]->reference;
                $this->orderDetails['skusBought'][$sku] = $sku;
            }
            $this->orderDetails['totalOrderValues'] += $order['total_paid'];
        }
    }

    /**
     * @param $lastOrder
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function setLastOrderDetails($lastOrder)
    {
        $this->lastOrderDetails['lastTotalProductsBought'] = 0;
        $this->lastOrderDetails['lastProductsBought'] = array();
        $this->lastOrderDetails['trackingNumber'] = 0;
        $this->lastOrderDetails['carrierCode'] = '';
        $this->lastOrderDetails['carrierCode'] = $this->getCarrierCode($lastOrder['id_carrier']);
        if (isset($lastOrder['shipping_number'])) {
            $this->lastOrderDetails['trackingNumber'] = $lastOrder['shipping_number'];
        }
        $this->orderDetails['lastThreeProducts']['last'] = array();

        $this->lastOrderDetails['lastOrderStatus'] =
            $this->getOrderStatus($lastOrder['id_lang'], $lastOrder['current_state']);
        $this->lastOrderDetails['lastOrderNumber'] = $lastOrder['id_order'];
        $this->lastOrderDetails['lastOrderValue'] = (float)$lastOrder['total_paid'];
        $this->lastOrderDetails['lastOrderDate'] = date('Y-m-d H:i:s', strtotime($lastOrder['date_add']));

        foreach ($lastOrder['order_detail'] as $orderItem) {
            $this->lastOrderDetails['lastTotalProductsBought'] += $orderItem['product_quantity'];
            array_push(
                $this->lastOrderDetails['lastProductsBought'],
                $orderItem['product_name']
            );
            $this->setLastProduct($orderItem);
            $productId = $orderItem['product_id'];
            $product = array();
            $product[$productId] = $this->getProductData((int)$productId);
            $category = $this->getCategoryName($lastOrder['id_lang'], (int)$product[$productId]->id_category_default);
            $this->lastOrderDetails['lastCategoriesBought'][$category] = $category;

            $sku = $product[$productId]->reference;
            $this->lastOrderDetails['lastSkusBought'][$sku] = $sku;
        }

        if (!empty($this->orderDetails['lastThreeProducts']['last'])) {
            $this->orderDetails['lastProduct'] = $this->getProductData($this->
            orderDetails['lastThreeProducts']['last']['product_id']);
        }
    }

    /**
     * @param $firstOrder
     */
    public function setFirstOrderDetails($firstOrder)
    {
        $this->firstOrderDetails['firstOrderValue'] = (float)$firstOrder['total_paid'];
        $this->firstOrderDetails['firstOrderDate'] = date('Y-m-d H:i:s', strtotime($firstOrder['date_add']));
    }

    public function setLastCompletedOrder($lastCompletedOrder)
    {
        $this->lastCompletedOrderDetails['ced_last_comp_order_date'] =
            date('Y-m-d H:i:s', strtotime($lastCompletedOrder['date_upd']));
        $this->lastCompletedOrderDetails['ced_last_comp_order_num'] = $lastCompletedOrder['id_order'];

        $key = 0;
        $completedOrder = array();
        foreach ($lastCompletedOrder['order_detail'] as $orderItem) {
            $orderProduct = $this->getProductData($orderItem['product_id']);
            $completedOrder[$key]['name'] = $orderProduct->name;
            if (is_array($completedOrder[$key]['name'])) {
                $completedOrder[$key]['name'] = reset($completedOrder[$key]['name']);
            }
            $image = $this->getImageUrl($orderProduct);
            $completedOrder[$key]['image'] = $image;
            $orderProductCategory = Category::getLinkRewrite(
                $orderProduct->id_category_default,
                $lastCompletedOrder['id_lang']
            );
            $completedOrder[$key]["url"] = $this->getProductUrl($orderProduct, $orderProductCategory);
            $key++;
        }

        $this->context->smarty->assign(
            array(
                'lastCompletedOrderItems' => $completedOrder
            )
        );

        $feedbackFormHtml = " ";
        if (count($completedOrder)) {
            $feedbackFormHtml = $this->context->smarty->fetch(_PS_MODULE_DIR_ .
                'cedmauticintegration/views/templates/admin/completedorder.tpl');
        }

        $this->lastCompletedOrderDetails['ced_feedback_html'] = $feedbackFormHtml;
    }

    /**
     * @param $orderItem
     */
    public function setLastProduct($orderItem)
    {
        if (empty($this->orderDetails['lastThreeProducts']['last']) || isset($this->orderDetails['lastThreeProducts']
                ['last']['id_order_detail']) && $this->orderDetails['lastThreeProducts']['last']['id_order_detail'] <
            $orderItem['id_order_detail']) {
            $this->orderDetails['lastThreeProducts']['last'] = $orderItem;
        }
    }

    public function getProductData($productId)
    {
        if (isset($this->productObject[$productId])) {
            return $this->productObject[$productId];
        } else {
            $this->productObject[$productId] = new Product((int)$productId);
            return $this->productObject[$productId];
        }
    }

    /**
     * @param $properties
     * @param $arr
     * @return mixed
     */
    public function getOrderGroupDetails($properties, $arr)
    {
        foreach ($properties as $property) {
            if ($this->exportProperties->canSetProperty($property['alias'], CedMautic::TYPE_PROPERTY)) {
                switch ($property['alias']) {
                    case 'ced_last_order_stat':
                        if (isset($this->lastOrderDetails['lastOrderStatus'])) {
                            $arr['ced_last_order_stat'] = $this->lastOrderDetails['lastOrderStatus'];
                        }
                        break;

                    case 'ced_last_order_track_num':
                        if (isset($this->lastOrderDetails['trackingNumber'])) {
                            $arr['ced_last_order_track_num'] = $this->lastOrderDetails['trackingNumber'];
                        }
                        break;

                    case 'ced_last_order_track_url':
                        if (isset($this->lastOrderDetails['carrierCode'])) {
                            $arr['ced_last_order_track_url'] = $this->lastOrderDetails['carrierCode'];
                        }
                        break;

                    case 'ced_last_order_ship_date':
                        if (isset($this->lastOrderDetails['lastOrderShipDate'])) {
                            $arr['ced_last_order_ship_date'] = $this->lastOrderDetails['lastOrderShipDate'];
                        }
                        break;

                    case 'ced_last_order_num':
                        if (isset($this->lastOrderDetails['lastOrderNumber'])) {
                            $arr['ced_last_order_num'] = $this->lastOrderDetails['lastOrderNumber'];
                        }
                        break;
                }
            }
        }

        return $arr;
    }

    /**
     * @param $properties
     * @param $arr
     * @return mixed
     */
    public function getLastProductsBought($properties, $arr)
    {
        foreach ($properties as $property) {
            if ($this->exportProperties->canSetProperty($property['alias'], CedMautic::TYPE_PROPERTY)) {
                switch ($property['alias']) {
                    case 'ced_last_products':
                        if (!empty($this->lastOrderDetails['lastProductsBought'])) {
                            $arr['ced_last_products'] = implode(';', $this->lastOrderDetails['lastProductsBought']);
                        }
                        break;

                    case 'ced_last_num_of_prod':
                        if (isset($this->lastOrderDetails['lastTotalProductsBought']) &&
                            $this->lastOrderDetails['lastTotalProductsBought'] > 0) {
                            $arr['ced_last_num_of_prod'] = $this->lastOrderDetails['lastTotalProductsBought'];
                        }
                        break;

                    case 'ced_products':
                        if (!empty($this->orderDetails['productsBought'])) {
                            $arr['ced_products'] = implode(';', $this->orderDetails['productsBought']);
                        }
                        break;

                    case 'ced_total_products_num':
                        if (isset($this->orderDetails['totalProductsBought']) &&
                            $this->orderDetails['totalProductsBought'] > 0) {
                            $arr['ced_total_products_num'] = $this->orderDetails['totalProductsBought'];
                        }
                        break;

                    case 'ced_prod_1_img_url':
                        if (!empty($this->orderDetails['lastThreeProducts']['last'])) {
                            $imageUrl = $this->getImageUrl($this->orderDetails['lastProduct']);
                            $arr['ced_prod_1_img_url'] = $imageUrl;
                        }
                        break;

                    case 'ced_prod_1_name':
                        if (!empty($this->orderDetails['lastThreeProducts']['last'])) {
                            $arr['ced_prod_1_name'] = $this->orderDetails['lastThreeProducts']['last']['product_name'];
                        }
                        break;

                    case 'ced_prod_1_price':
                        if (!empty($this->orderDetails['lastThreeProducts']['last'])) {
                            $arr['ced_prod_1_price'] =
                                $this->orderDetails['lastThreeProducts']['last']['total_price_tax_incl'];
                        }
                        break;

                    case 'ced_prod_1_url':
                        if (!empty($this->orderDetails['lastThreeProducts']['last'])) {
                            $product = $this->orderDetails['lastProduct'];
                            $category = Category::getLinkRewrite($product->id_category_default, $this->order_id_lang);
                            $arr['ced_prod_1_url'] = $this->getProductUrl($product, $category);
                        }
                        break;
                }
            }
        }

        return $arr;
    }

    /**
     * @param $properties
     * @param $arr
     * @return mixed
     */
    public function getRfmFields($properties, $arr)
    {
        foreach ($properties as $property) {
            if ($this->exportProperties->canSetProperty($property['alias'], CedMautic::TYPE_PROPERTY)) {
                switch ($property['alias']) {
                    case 'ced_total_val_of_orders':
                        if ($this->orderDetails['totalOrderValues'] > 0) {
                            $arr['ced_total_val_of_orders'] = $this->orderDetails['totalOrderValues'];
                        }
                        break;

                    case 'ced_avg_order_value':
                        if ($this->orderDetails['totalOrderValues'] > 0) {
                            $arr['ced_avg_order_value'] =
                                $this->orderDetails['totalOrderValues'] / $this->orderDetails['totalOrders'];
                        }
                        break;

                    case 'ced_total_orders':
                        $arr['ced_total_orders'] = $this->orderDetails['totalOrders'];
                        break;

                    case 'ced_first_order_val':
                        $arr['ced_first_order_val'] = $this->firstOrderDetails['firstOrderValue'];
                        break;

                    case 'ced_first_order_date':
                        $arr['ced_first_order_date'] = $this->firstOrderDetails['firstOrderDate'];
                        break;

                    case 'ced_last_order_val':
                        $arr['ced_last_order_val'] = $this->lastOrderDetails['lastOrderValue'];
                        break;

                    case 'ced_last_order_date':
                        $arr['ced_last_order_date'] = $this->lastOrderDetails['lastOrderDate'];
                        break;

                    case 'ced_avg_days_bt_orders':
                        $arr['ced_avg_days_bt_orders'] = $this->getAvgDays();
                        break;

                    case 'ced_order_monetary':
                        if ($this->orderDetails['totalOrderValues'] > 0) {
                            $monetaryRating = $this->getRating('monetary', $this->orderDetails['totalOrderValues']);
                            $arr['ced_order_monetary'] = (int)$monetaryRating;
                        }
                        break;

                    case 'ced_order_frequency':
                        $frequencyRating = $this->getRating('frequency', $this->orderDetails['totalOrders']);
                        $arr['ced_order_frequency'] = (int)$frequencyRating;
                        break;

                    case 'ced_order_recency':
                        $recencyDateDiff = $this->getRecencyDateDiff();
                        $recencyRating = $this->getRating('recency', $recencyDateDiff);
                        $arr['ced_order_recency'] = (int)$recencyRating;
                        break;
                }
            }
        }

        return $arr;
    }

    public function getCategoriesBought($properties, $arr)
    {
        foreach ($properties as $property) {
            if ($this->exportProperties->canSetProperty($property['alias'], CedMautic::TYPE_PROPERTY)) {
                switch ($property['alias']) {
                    case 'ced_categories':
                        if (isset($this->orderDetails['categoriesBought'])) {
                            $arr['ced_categories'] =
                                implode(';', array_values($this->orderDetails['categoriesBought']));
                        }
                        break;

                    case 'ced_last_categories':
                        if (isset($this->lastOrderDetails['lastCategoriesBought'])) {
                            $arr['ced_last_categories'] =
                                implode(';', array_values($this->lastOrderDetails['lastCategoriesBought']));
                        }
                        break;
                }
            }
        }

        return $arr;
    }

    /**
     * @param $properties
     * @param $arr
     * @return mixed
     */
    public function getSkusBought($properties, $arr)
    {
        foreach ($properties as $property) {
            if ($this->exportProperties->canSetProperty($property['alias'], CedMautic::TYPE_PROPERTY)) {
                switch ($property['alias']) {
                    case 'ced_skus':
                        if (isset($this->orderDetails['skusBought'])) {
                            $arr['ced_skus'] = implode(';', array_values($this->orderDetails['skusBought']));
                        }
                        break;

                    case 'ced_last_skus':
                        if (isset($this->lastOrderDetails['lastSkusBought'])) {
                            $arr['ced_last_skus'] =
                                implode(';', array_values($this->lastOrderDetails['lastSkusBought']));
                        }
                        break;
                }
            }
        }

        return $arr;
    }

    public function getLastCompletedOrder($properties, $arr)
    {
        foreach ($properties as $property) {
            if ($this->exportProperties->canSetProperty(
                $property['alias'],
                Cedmautic::TYPE_PROPERTY
            )) {
                switch ($property['alias']) {
                    case 'ced_feedback_html':
                        if (isset($this->lastCompletedOrderDetails['ced_feedback_html'])) {
                            $arr['ced_feedback_html'] = $this->lastCompletedOrderDetails['ced_feedback_html'];
                        }
                        break;

                    case 'ced_last_comp_order_date':
                        if (isset($this->lastCompletedOrderDetails['ced_last_comp_order_date'])) {
                            $arr['ced_last_comp_order_date'] =
                                $this->lastCompletedOrderDetails['ced_last_comp_order_date'];
                        }
                        break;

                    case 'ced_last_comp_order_num':
                        if (isset($this->lastCompletedOrderDetails['ced_last_comp_order_num'])) {
                            $arr['ced_last_comp_order_num'] =
                                $this->lastCompletedOrderDetails['ced_last_comp_order_num'];
                        }
                        break;
                }
            }
        }
        return $arr;
    }

    /**
     * @param $category
     * @return mixed
     */
    public function getCategoryName($langId, $categoryId)
    {
        if (!isset($this->categoryName[$langId][$categoryId])) {
            $categories = Category::getSimpleCategories($langId);
            foreach ($categories as $category) {
                $this->categoryName[$langId][$category['id_category']] = $category;
            }
        }
        if (isset($this->categoryName[$langId][$categoryId]['name'])) {
            return $this->categoryName[$langId][$categoryId]['name'];
        }
        return "";
    }

    public function getImageUrl($product)
    {
        $productId = $product->id;
        $image = Image::getCover($productId);
        $link = new Link();
        if (is_array($product->link_rewrite)) {
            $product->link_rewrite = reset($product->link_rewrite);
        }
        $imageUrl = $link->getImageLink($product->link_rewrite, $image['id_image']);
        return $imageUrl;
    }

    public function getAvgDays()
    {
        $firstOrderDate = date('Y-m-d', strtotime($this->firstOrderDetails['firstOrderDate']));
        $lastOrderDate = date('Y-m-d', strtotime($this->lastOrderDetails['lastOrderDate']));
        $dateDiff = date_diff(date_create($lastOrderDate), date_create($firstOrderDate))->format('%a');
        $avgDays = $dateDiff / $this->orderDetails['totalOrders'];
        return $avgDays;
    }

    public function getRecencyDateDiff()
    {
        $currentTime = date('Y-m-d H:i:s', time());
        $time = date('Y-m-d', strtotime($currentTime));
        $recencyDateDiff = date_diff(date_create($time), date_create($this->lastOrderDetails['lastOrderDate']))
            ->format('%a');
        return $recencyDateDiff;
    }

    public function getRating($keyword, $value)
    {
        $data = Configuration::get('mautic_integration_mautic_rfm_settings_rfm_fields');
        $rfmRating = json_decode($data, true);

        switch ($keyword) {
            case 'recency':
                if ($value <= $rfmRating['rfm_at_5'][$keyword]) {
                    return 5;
                } elseif ($value >= $rfmRating['from_rfm_4'][$keyword] && $value <= $rfmRating['to_rfm_4'][$keyword]) {
                    return 4;
                } elseif ($value >= $rfmRating['from_rfm_3'][$keyword] && $value <= $rfmRating['to_rfm_3'][$keyword]) {
                    return 3;
                } elseif ($value >= $rfmRating['from_rfm_2'][$keyword] && $value <= $rfmRating['to_rfm_2'][$keyword]) {
                    return 2;
                } else {
                    return 1;
                }
                break;

            case 'frequency':
                if ($value >= $rfmRating['rfm_at_5'][$keyword]) {
                    return 5;
                } elseif ($value <= $rfmRating['from_rfm_4'][$keyword] && $value >= $rfmRating['to_rfm_4'][$keyword]) {
                    return 4;
                } elseif ($value <= $rfmRating['from_rfm_3'][$keyword] && $value >= $rfmRating['to_rfm_3'][$keyword]) {
                    return 3;
                } elseif ($value <= $rfmRating['from_rfm_2'][$keyword] && $value >= $rfmRating['to_rfm_2'][$keyword]) {
                    return 2;
                } else {
                    return 1;
                }
                break;

            case 'monetary':
                if ($value >= $rfmRating['rfm_at_5'][$keyword]) {
                    return 5;
                } elseif ($value <= $rfmRating['from_rfm_4'][$keyword] && $value >= $rfmRating['to_rfm_4'][$keyword]) {
                    return 4;
                } elseif ($value <= $rfmRating['from_rfm_3'][$keyword] && $value >= $rfmRating['to_rfm_3'][$keyword]) {
                    return 3;
                } elseif ($value <= $rfmRating['from_rfm_2'][$keyword] && $value >= $rfmRating['to_rfm_2'][$keyword]) {
                    return 2;
                } else {
                    return 1;
                }
                break;

            default:
                return 0;
        }
    }

    /**
     * @param $customer
     * @param $orders
     * @param $properties
     * @param $arr
     * @return mixed
     */
    public function getShoppingCartFields($customer, $orders, $properties, $arr)
    {
        $billingAddress = array();
        $shippingAddress = array();
        if ($orders != null) {
            $lastOrder = end($orders);
            $shippingAddressId = $lastOrder['id_address_invoice'];
            $shippingAddress = new Address((int)$shippingAddressId);
            $billingAddressId = $lastOrder['id_address_delivery'];
            $billingAddress = new Address((int)$billingAddressId);
        } else {
            $addressId = Address::getFirstCustomerAddressId($customer['id_customer']);
            if ($addressId) {
                $shippingAddress = $billingAddress = new Address((int)$addressId);
            }
        }

        foreach ($properties as $property) {
            if ($this->exportProperties->canSetProperty($property['alias'], CedMautic::TYPE_PROPERTY)) {
                switch ($property['alias']) {
                    case 'ced_ship_add_line_1':
                        if (!empty($shippingAddress) && isset($shippingAddress->address1)) {
                            $arr['ced_ship_add_line_1'] = $shippingAddress->address1;
                        }
                        break;

                    case 'ced_ship_add_line_2':
                        if (!empty($shippingAddress) && isset($shippingAddress->address2)) {
                            $arr['ced_ship_add_line_2'] = $shippingAddress->address2;
                        }
                        break;

                    case 'ced_ship_city':
                        if (!empty($shippingAddress) && isset($shippingAddress->city)) {
                            $arr['ced_ship_city'] = $shippingAddress->city;
                        }
                        break;

                    case 'ced_ship_state':
                        if (!empty($shippingAddress) && isset($shippingAddress->id_state)
                            && $shippingAddress->id_state) {
                            $stateId = $shippingAddress->id_state;
                            $arr['ced_ship_state'] = $this->getStateName($stateId);
                        }
                        break;

                    case 'ced_ship_post_code':
                        if (!empty($shippingAddress) && isset($shippingAddress->postcode)) {
                            $arr['ced_ship_post_code'] = $shippingAddress->postcode;
                        }
                        break;

                    case 'ced_ship_country':
                        if (!empty($shippingAddress) && isset($shippingAddress->id_country)
                            && $shippingAddress->id_country) {
                            $countryId = $shippingAddress->id_country;
                            $shippingCountryName = $this->getCountry($customer['id_lang'], $countryId);
                            $arr['ced_ship_country'] = $shippingCountryName;
                        }
                        break;

                    case 'ced_bill_add_line_1':
                        if (!empty($billingAddress) && isset($billingAddress->address1)) {
                            $arr['ced_bill_add_line_1'] = $billingAddress->address1;
                        }
                        break;

                    case 'ced_bill_add_line_2':
                        if (!empty($billingAddress) && isset($billingAddress->address2)) {
                            $arr['ced_bill_add_line_2'] = $billingAddress->address2;
                        }
                        break;

                    case 'ced_bill_city':
                        if (!empty($billingAddress) && isset($billingAddress->city)) {
                            $arr['ced_bill_city'] = $billingAddress->city;
                        }
                        break;

                    case 'ced_bill_state':
                        if (!empty($billingAddress) && isset($billingAddress->id_state)
                            && $billingAddress->id_state) {
                            $stateId = $billingAddress->id_state;
                            $arr['ced_bill_state'] = $this->getStateName($stateId);
                        }
                        break;

                    case 'ced_bill_post_code':
                        if (!empty($billingAddress) && isset($billingAddress->postcode)) {
                            $arr['ced_bill_post_code'] = $billingAddress->postcode;
                        }
                        break;

                    case 'ced_bill_country':
                        if (!empty($billingAddress) && isset($billingAddress->id_country)
                            && $billingAddress->id_country) {
                            $countryId = $billingAddress->id_country;
                            $billingCountry = $this->getCountry($customer['id_lang'], $countryId);
                            $arr['ced_bill_country'] = $billingCountry;
                        }
                        break;
                }
            }
        }
        return $arr;
    }

    public function getAddressLine2($addressLine1, $address)
    {
        $addressLines = str_replace(
            $addressLine1,
            "",
            $address
        );
        $addressLine2 = str_replace("\n", " ", $addressLines);
        return $addressLine2;
    }

    /**
     * @param $countryCode
     * @return mixed
     */
    public function getCountry($langId, $countryId)
    {
        if (!isset($this->countryName[$langId][$countryId])) {
            $this->countryName[$langId][$countryId] = Country::getNameById($langId, $countryId);
        }
        return $this->countryName[$langId][$countryId];
    }

    public function getCarrierCode($carrierId)
    {
        if (isset($this->carrier[$carrierId])) {
            return $this->carrier[$carrierId];
        } else {
            $carrier = new Carrier((int)$carrierId);
            $this->carrier[$carrierId] = $carrier->name;
            return $this->carrier[$carrierId];
        }
    }

    public function getOrderStatus($langId, $orderStateId)
    {
        if (!isset($this->orderStatus[$langId][$orderStateId])) {
            $orderStateList = OrderState::getOrderStates($langId);
            foreach ($orderStateList as $orderState) {
                $this->orderStatus[$langId][$orderState['id_order_state']] = $orderState;
            }
        }

        if (isset($this->orderStatus[$langId][$orderStateId])) {
            return $this->orderStatus[$langId][$orderStateId]['name'];
        }

        return "";
    }

    public function getProductUrl($product, $category)
    {
        $link = new Link();
        $productUrl = $link->getProductLink($product, null, $category);
        return $productUrl;
    }

    public function getStateName($stateId)
    {
        if (!isset($this->stateName[$stateId])) {
            $this->stateName[$stateId] = State::getNameById($stateId);
        }
        return $this->stateName[$stateId];
    }

    public function getCompletedOrderStatus()
    {
        return Configuration::get('mautic_integration_mautic_completed_order_status');
    }
}
