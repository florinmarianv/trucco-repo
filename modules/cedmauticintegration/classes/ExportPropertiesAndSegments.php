<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

include_once 'CedMautic.php';
include_once 'Properties.php';
include_once 'ConnectionManager.php';

class ExportPropertiesAndSegments extends Module
{
    public $properties;
    public $connectionManager;
    public $requiredProperties = array();

    public function __construct()
    {
        parent::__construct();
        $this->properties = new Properties();
        $this->connectionManager = new ConnectionManager();
        $this->getPropertiesFromDb();
    }

    /**
     * Create the contact property
     * @return mixed
     * @throws PrestaShopDatabaseException
     */
    public function createProperties()
    {
        $this->getPropertiesFromDb();
        $endpoint = '/api/fields/contact/new';
        $method = 'POST';
        $groups = Properties::allGroups();
        foreach ($groups as $group) {
            $properties = $this->properties->allProperties($group['name']);

            foreach ($properties as $property) {
                if ($this->isReuiredProperty(
                    $property['alias'],
                    CedMautic::TYPE_PROPERTY
                )) {
                    $contactResponse = $this->connectionManager->createRequest($method, $endpoint, $property);
                    if ($contactResponse['status_code'] == 201) {
                        $response = json_decode($contactResponse['response'], true);
                        if ($response != null && !isset($response['errors']) && isset($response['field'])) {
                            $this->saveMauticId(
                                $response['field'],
                                CedMautic::TYPE_PROPERTY
                            );
                        }
                    } elseif ($contactResponse['status_code'] == 200) {
                        $response = json_decode($contactResponse['response'], true);
                        if (isset($response['errors'])) {
                            return $response;
                        }
                    }
                }
            }
        }
    }

    /**
     * Create segment
     * @return mixed
     * @throws PrestaShopDatabaseException
     */
    public function createSegments()
    {
        $this->getPropertiesFromDb();
        $endpoint = '/api/segments/new';
        $method = 'POST';
        $segments = Properties::getMauticSegments();
        foreach ($segments as $segment) {
            if ($this->isReuiredProperty(
                $segment['alias'],
                CedMautic::TYPE_SEGMENT
            )) {
                $segmentResponse = $this->connectionManager->createRequest($method, $endpoint, $segment);
                if ($segmentResponse['status_code'] == 201) {
                    $response = json_decode($segmentResponse['response'], true);
                    if ($response != null && !isset($response['errors']) && isset($response['list'])) {
                        $this->saveMauticId($response['list'], CedMautic::TYPE_SEGMENT);
                    }
                } elseif ($segmentResponse['status_code'] == 200) {
                    $response = json_decode($segmentResponse['response'], true);
                    if (isset($response['errors'])) {
                        return $response;
                    }
                }
            }
        }

        $this->connectionManager->setMauticConfig('connection_established', 1);
    }

    /**
     * @param $propertyCode
     */
    public function createSingleProperty($propertyCode)
    {
        $endpoint = '/api/fields/contact/new';
        $method = 'POST';
        $groups = $this->properties->allGroups();
        foreach ($groups as $group) {
            $properties = $this->properties->allProperties($group['name']);
            if (isset($properties[$propertyCode])) {
                $property = $properties[$propertyCode];
                $contactResponse = $this->connectionManager->createRequest($method, $endpoint, $property);
                $response = json_decode($contactResponse['response'], true);
                if ($contactResponse['status_code'] == 201) {
                    if ($response != null && !isset($response['errors']) && isset($response['field'])) {
                        $this->saveMauticId(
                            $response['field'],
                            CedMautic::TYPE_PROPERTY
                        );
                    }
                }
                break;
            }
        }
        return $response;
    }

    /**
     * @param $segmentCode
     */
    public function createSingleSegment($segmentCode)
    {
        $endpoint = '/api/segments/new';
        $method = 'POST';
        $segments = $this->properties->getMauticSegments();
        if (isset($segments[$segmentCode])) {
            $segment = $segments[$segmentCode];
            $segmentResponse = $this->connectionManager->createRequest($method, $endpoint, $segment);
            $response = json_decode($segmentResponse['response'], true);
            if ($segmentResponse['status_code'] == 201) {
                if ($response != null && !isset($response['errors']) && isset($response['list'])) {
                    $this->saveMauticId($response['list'], CedMautic::TYPE_SEGMENT);
                }
            }
        }
        return $response;
    }

    /**
     * @throws PrestaShopDatabaseException
     */
    public function getPropertiesFromDb()
    {
        $allProperties = CedMautic::getAllData();
        foreach ($allProperties as $property) {
            $this->requiredProperties[$property['entity_type']][$property['code']] = $property;
        }
    }

    /**
     * @param $propertyCode
     * @param $entityType
     * @return bool
     */
    public function isReuiredProperty($propertyCode, $entityType)
    {
        if (isset($this->requiredProperties[$entityType][$propertyCode])) {
            $property = $this->requiredProperties[$entityType][$propertyCode];
            if ($property['mautic_id'] == 0 && $property['is_required']) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * @param $propertyCode
     * @param $entityType
     * @return bool
     */
    public function canSetProperty($propertyCode, $entityType)
    {
        if (isset($this->requiredProperties[$entityType][$propertyCode])) {
            $property = $this->requiredProperties[$entityType][$propertyCode];
            if ($property['mautic_id'] != 0 && $property['is_required']) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * @param $response
     * @param $entityType
     */
    public function saveMauticId($response, $entityType)
    {
        if (isset($this->requiredProperties[$entityType][$response['alias']])) {
            $id = $this->requiredProperties[$entityType][$response['alias']]['entity_id'];
            $cedMautic = new CedMautic();
            $cedMauticObj = array();
            $cedMauticObj['entity_id'] = $id;
            $cedMauticObj['is_required'] = true;
            $cedMauticObj['mautic_id'] = $response['id'];
            try {
                $cedMautic->updateById($cedMauticObj);
            } catch (\Exception $e) {
                $this->displayError($this->l($e->getMessage()));
            }
        }
    }
}
