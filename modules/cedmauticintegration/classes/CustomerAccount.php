<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

include_once 'Properties.php';
include_once 'ExportPropertiesAndSegments.php';
include_once 'ConnectionManager.php';

class CustomerAccount extends Module
{
    public $properties;
    public $exportProperties;
    public $groupName;
    public $connectionManager;

    public function __construct()
    {
        parent::__construct();
        $this->properties = new Properties();
        $this->connectionManager = new ConnectionManager();
        $this->exportProperties = new ExportPropertiesAndSegments();
    }

    public function customerAccountDetailsToExport($customer, $arr)
    {
        $customerGroupProperties = $this->properties->allProperties('customer_group');
        $arr['firstname'] = $customer['firstname'];
        $arr['lastname'] = $customer['lastname'];
        if ($this->connectionManager->isCustomerGroupEnabled('customer_group')) {
            $arr = $this->getCustomerGroupDetails($customer, $customerGroupProperties, $arr);
        }
        $arr = $this->setMappingFields($customer, $arr);
        return $arr;
    }

    public function getCustomerGroupDetails($customer, $properties, $arr)
    {
        foreach ($properties as $property) {
            if ($this->exportProperties->canSetProperty($property['alias'], CedMautic::TYPE_PROPERTY)) {
                switch ($property['alias']) {
                    case 'ced_customer_group':
                        if (isset($customer['id_default_group'])) {
                            $arr['ced_customer_group'] =
                                $this->getCustomerGroup($customer['id_lang'], $customer['id_default_group']);
                        }
                        break;

                    case 'ced_newsletter_subs':
                        if (isset($customer['email'])) {
                            $arr['ced_newsletter_subs'] = $this->getNewsLetterSubscription($customer['newsletter']);
                        }
                        break;

                    case 'ced_customer_cart_id':
                        $arr['ced_customer_cart_id'] = $customer['id_customer'];
                        break;
                }
            }
        }

        return $arr;
    }

    public function setMappingFields($customer, $arr)
    {
        if (json_decode($this->getMappingFields())) {
            $mappingArray = json_decode($this->getMappingFields());
            foreach ($mappingArray as $key => $value) {
                if ($customer[$value] && $key != "") {
                    $arr[$key] = $customer[$value];
                }
            }
        }
        return $arr;
    }

    public function getMappingFields()
    {
        return Configuration::get('mautic_integration_mautic_fields_mapping_mapping_table');
    }

    public function getCustomerGroup($langId, $groupId)
    {
        if (!isset($this->groupName[$langId][$groupId])) {
            $groups = Group::getGroups($langId);
            foreach ($groups as $group) {
                $this->groupName[$langId][$group['id_group']] = $group;
            }
        }
        if (isset($this->groupName[$langId][$groupId]['name'])) {
            return $this->groupName[$langId][$groupId]['name'];
        }

        return "";
    }

    public function getNewsLetterSubscription($newsletter)
    {
        if ($newsletter) {
            return "yes";
        } else {
            return "no";
        }
    }
}
