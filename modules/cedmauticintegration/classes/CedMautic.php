<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

class CedMautic extends ObjectModel
{
    const TYPE_PROPERTY = 'properties';
    const TYPE_SEGMENT = 'segments';

    public static $definition = array(
        'table' => 'ced_mautic',
        'primary' => 'entity_id',
        'fields' => array(
            'entity_type' => array('type' => self::TYPE_STRING, 'size' => 100),
            'code' => array('type' => self::TYPE_STRING, 'size' => 100),
            'name' => array('type' => self::TYPE_STRING, 'size' => 100),
            'is_required' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'mautic_id' => array('type' => self::TYPE_INT)
        ),
    );

    /**
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getAllData()
    {
        $sql = "Select * From " . _DB_PREFIX_ . "ced_mautic";
        $result = Db::getInstance()->executeS($sql);
        if (!is_array($result)) {
            $result = array();
        }
        return $result;
    }

    /**
     * @param $code
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getDataByType($type)
    {
        $sql = "Select * From " . _DB_PREFIX_ . "ced_mautic c Where c.`entity_type`='" . $type . "'";
        $result = Db::getInstance()->executeS($sql);
        if (!is_array($result)) {
            $result = array();
        }
        return $result;
    }

    public function updateById($obj)
    {
        $db = Db::getInstance();
        if (isset($obj['is_required']) && isset($obj['mautic_id']) && isset($obj['entity_id'])) {
            $db->update(
                'ced_mautic',
                array(
                    'is_required' => (int)$obj['is_required'],
                    'mautic_id' => (int)$obj['mautic_id'],
                ),
                'entity_id=' . (int)$obj['entity_id']
            );
        }
    }
}
