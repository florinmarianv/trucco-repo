<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

include_once 'ConnectionManager.php';
include_once 'OrderDetails.php';
include_once 'AbandonedCart.php';
include_once 'CustomerAccount.php';

class ExportCustomers extends Module
{
    const EXPORT_DEFAULT = 'default';
    const EXPORT_CUSTOMER_ACCOUNT = 'customer_account';
    const EXPORT_CUSTOMER_ORDER = 'customer_order';
    const EXPORT_CUSTOMER_CART = 'customer_cart';
    const EXPORT_CUSTOMER_BY_CRON = 'export_by_cron';
    const ENDPOINT = '/api/contacts/new';
    const METHOD = 'POST';

    public $connectionManager;
    public $orderDetails;
    public $abandonedCart;
    public $customerAccount;

    public function __construct()
    {
        $this->connectionManager = new ConnectionManager();
        $this->orderDetails = new OrderDetails();
        $this->abandonedCart = new AbandonedCart();
        $this->customerAccount = new CustomerAccount();
        $this->feedback = $this->connectionManager->isCustomerGroupEnabled('feedback');
    }

    /**
     * @param $customerIds
     * @return array
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function createBulkContacts($customerIds)
    {
        $status = $this->orderDetails->getCompletedOrderStatus();
        $errors = array();

        $allCustomers = $this->getCustomersByArrayOfIds($customerIds);

        $allOrders = $this->getOrdersByArrayOfCustomerIds($customerIds);

        $allCartsData = $this->getCartsDataByArrayOfCustomerIds($customerIds);

        $ordersList = array();
        $lastCompletedOrder = array();
        foreach ($allOrders as $order) {
            $ordersList[$order['id_customer']][$order['id_order']] = $order;
            if ($this->feedback) {
                if ($order['current_state'] == $status) {
                    if (isset($lastCompletedOrder[$order['id_customer']]) &&
                        $lastCompletedOrder[$order['id_customer']]['date_upd'] < $order
                        ['date_upd']) {
                        $lastCompletedOrder[$order['id_customer']] = $order;
                    } elseif (!isset($lastCompletedOrder[$order['id_customer']])) {
                        $lastCompletedOrder[$order['id_customer']] = $order;
                    }
                }
            }
        }

        $cartList = array();
        foreach ($allCartsData as $cart) {
            if (!empty($cart['cart_product'])) {
                $cartList[$cart['id_customer']][$cart['id_cart']] = $cart;
            }
        }
        foreach ($allCustomers as $customer) {
            $arr = array();
            if (isset($ordersList[$customer['id_customer']])) {
                $orders = $ordersList[$customer['id_customer']];
            } else {
                $orders = null;
            }

            if (isset($lastCompletedOrder[$customer['id_customer']])) {
                $completedOrder = $lastCompletedOrder[$customer['id_customer']];
            } else {
                $completedOrder = null;
            }

            if (isset($cartList[$customer['id_customer']])) {
                $carts = $cartList[$customer['id_customer']];
            } else {
                $carts = null;
            }
            $arr = $this->getContactDetails($customer, $orders, $completedOrder, $carts, $arr, $this::EXPORT_DEFAULT);

            $contactResponse = $this->connectionManager->createRequest($this::METHOD, $this::ENDPOINT, $arr);
            if (isset($contactResponse['status']) && $contactResponse['status'] === false) {
                $errors[$customer['email']] = array(
                    'email' => $customer['email'],
                    'id' => $customer['id_customer'],
                    'errors' => isset($contactResponse['message']) ? $contactResponse['message'] : 'Unkown Error'
                );
            } elseif (isset($contactResponse['status_code']) &&
                !in_array($contactResponse['status_code'], array(200, 201))) {
                $msg = array();
                $response = json_decode($contactResponse['response'], true);
                if (isset($response['errors'])) {
                    foreach ($response['errors'] as $error) {
                        $msg[] = $error['message'];
                    }
                }
                $errors[$customer['email']] = array(
                    'email' => $customer['email'],
                    'id' => $customer['id_customer'],
                    'errors' => $msg
                );
            }
        }
        return $errors;
    }

    public function exportCustomerAccount($customerIds)
    {
        $allCustomers = $this->getCustomersByArrayOfIds($customerIds);

        $allCartsData = $this->getCartsDataByArrayOfCustomerIds($customerIds);

        $cartList = array();
        foreach ($allCartsData as $cart) {
            if (!empty($cart['cart_product'])) {
                $cartList[$cart['id_customer']][$cart['id_cart']] = $cart;
            }
        }
        foreach ($allCustomers as $customer) {
            $arr = array();

            if (isset($cartList[$customer['id_customer']])) {
                $carts = $cartList[$customer['id_customer']];
            } else {
                $carts = null;
            }
            $arr = $this->getContactDetails(
                $customer,
                null,
                null,
                $carts,
                $arr,
                $this::EXPORT_CUSTOMER_ACCOUNT
            );

            $this->connectionManager->createRequest($this::METHOD, $this::ENDPOINT, $arr);
        }
    }

    public function exportCustomerOrder($customerIds)
    {
        $status = $this->orderDetails->getCompletedOrderStatus();

        $allCustomers = $this->getCustomersByArrayOfIds($customerIds);

        $allOrders = $this->getOrdersByArrayOfCustomerIds($customerIds);

        $allCartsData = $this->getCartsDataByArrayOfCustomerIds($customerIds);

        $ordersList = array();
        $lastCompletedOrder = array();
        foreach ($allOrders as $order) {
            $ordersList[$order['id_customer']][$order['id_order']] = $order;
            if ($this->feedback) {
                if ($order['current_state'] == $status) {
                    if (isset($lastCompletedOrder[$order['id_customer']]) &&
                        $lastCompletedOrder[$order['id_customer']]['date_upd'] < $order
                        ['date_upd']) {
                        $lastCompletedOrder[$order['id_customer']] = $order;
                    } elseif (!isset($lastCompletedOrder[$order['id_customer']])) {
                        $lastCompletedOrder[$order['id_customer']] = $order;
                    }
                }
            }
        }

        $cartList = array();
        foreach ($allCartsData as $cart) {
            if (!empty($cart['cart_product'])) {
                $cartList[$cart['id_customer']][$cart['id_cart']] = $cart;
            }
        }
        foreach ($allCustomers as $customer) {
            $arr = array();
            if (isset($ordersList[$customer['id_customer']])) {
                $orders = $ordersList[$customer['id_customer']];
            } else {
                $orders = null;
            }

            if (isset($lastCompletedOrder[$customer['id_customer']])) {
                $completedOrder = $lastCompletedOrder[$customer['id_customer']];
            } else {
                $completedOrder = null;
            }

            if (isset($cartList[$customer['id_customer']])) {
                $carts = $cartList[$customer['id_customer']];
            } else {
                $carts = null;
            }
            $arr = $this->getContactDetails(
                $customer,
                $orders,
                $completedOrder,
                $carts,
                $arr,
                $this::EXPORT_CUSTOMER_ORDER
            );

            $this->connectionManager->createRequest($this::METHOD, $this::ENDPOINT, $arr);
        }
    }

    public function exportCustomerCart()
    {
        $allIds = array();
        $carts = $this->getAbandonedCarts();
        $cartList = array();
        foreach ($carts as $cart) {
            if (!empty($cart['cart_product'])) {
                $cartList[$cart['id_customer']][$cart['id_cart']] = $cart;
            }
            array_push($allIds, $cart['id_customer']);
        }

        $allCustomers = $this->getCustomersByArrayOfIds($allIds);

        foreach ($allCustomers as $customer) {
            $arr = array();
            if (isset($cartList[$customer['id_customer']])) {
                $carts = $cartList[$customer['id_customer']];
            } else {
                $carts = null;
            }
            $arr = $this->getContactDetails(
                $customer,
                null,
                null,
                $carts,
                $arr,
                $this::EXPORT_CUSTOMER_CART
            );

            $this->connectionManager->createRequest($this::METHOD, $this::ENDPOINT, $arr);
        }
    }

    public function exportCustomersByCron()
    {
        $allIds = $this->getAllIdsForCron();

        $status = $this->orderDetails->getCompletedOrderStatus();

        $allCustomers = $this->getCustomersByArrayOfIds($allIds);

        $allOrders = $this->getOrdersByArrayOfCustomerIds($allIds);

        $allCartsData = $this->getCartsDataByArrayOfCustomerIds($allIds);

        $ordersList = array();
        $lastCompletedOrder = array();
        foreach ($allOrders as $order) {
            $ordersList[$order['id_customer']][$order['id_order']] = $order;
            if ($this->feedback) {
                if ($order['current_state'] == $status) {
                    if (isset($lastCompletedOrder[$order['id_customer']]) &&
                        $lastCompletedOrder[$order['id_customer']]['date_upd'] < $order
                        ['date_upd']) {
                        $lastCompletedOrder[$order['id_customer']] = $order;
                    } elseif (!isset($lastCompletedOrder[$order['id_customer']])) {
                        $lastCompletedOrder[$order['id_customer']] = $order;
                    }
                }
            }
        }

        $cartList = array();
        foreach ($allCartsData as $cart) {
            if (!empty($cart['cart_product'])) {
                $cartList[$cart['id_customer']][$cart['id_cart']] = $cart;
            }
        }
        foreach ($allCustomers as $customer) {
            $arr = array();
            if (isset($ordersList[$customer['id_customer']])) {
                $orders = $ordersList[$customer['id_customer']];
            } else {
                $orders = null;
            }

            if (isset($lastCompletedOrder[$customer['id_customer']])) {
                $completedOrder = $lastCompletedOrder[$customer['id_customer']];
            } else {
                $completedOrder = null;
            }

            if (isset($cartList[$customer['id_customer']])) {
                $carts = $cartList[$customer['id_customer']];
            } else {
                $carts = null;
            }
            $arr = $this->getContactDetails(
                $customer,
                $orders,
                $completedOrder,
                $carts,
                $arr,
                $this::EXPORT_CUSTOMER_BY_CRON
            );

             $this->connectionManager->createRequest($this::METHOD, $this::ENDPOINT, $arr);
        }
    }

    public function getContactDetails($customer, $orders, $completedOrder, $carts, $arr, $exportType)
    {
        $arr['email'] = $customer['email'];
        if ($exportType == $this::EXPORT_CUSTOMER_ACCOUNT) {
            $arr = $this->customerAccount->customerAccountDetailsToExport($customer, $arr);
        } elseif ($exportType == $this::EXPORT_CUSTOMER_ORDER) {
            $arr = $this->orderDetails->orderDetailsToExport($customer, $orders, $completedOrder, $arr);
        } elseif ($exportType == $this::EXPORT_CUSTOMER_BY_CRON) {
            $arr = $this->customerAccount->customerAccountDetailsToExport($customer, $arr);
            $arr = $this->orderDetails->orderDetailsToExport($customer, $orders, $completedOrder, $arr);
        } elseif ($exportType == $this::EXPORT_DEFAULT) {
            $arr = $this->customerAccount->customerAccountDetailsToExport($customer, $arr);
            $arr = $this->orderDetails->orderDetailsToExport($customer, $orders, $completedOrder, $arr);
        }
        $arr = $this->abandonedCart->getAbandonedCartDetails($carts, $arr);
        return $arr;
    }

    /**
     * @param $customerIds
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public function getCustomersByArrayOfIds($customerIds)
    {
        $result = array();
        if (!empty($customerIds)) {
            $query = "Select * From " . _DB_PREFIX_ . "customer c Where c.`id_customer` IN ("
                . implode(',', $customerIds) . ")";

            $result = Db::getInstance()->executeS($query);
        }

        if (!is_array($result)) {
            $result = array();
        }

        return $result;
    }

    /**
     * @param $customerIds
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public function getOrdersByArrayOfCustomerIds($customerIds)
    {
        $orders = array();
        $orderIds = array();
        if (!empty($customerIds)) {
            $orderQuery = "Select * From " . _DB_PREFIX_ . "orders o Where o.`id_customer` IN ("
                . implode(',', $customerIds) . ")";

            $orders = Db::getInstance()->executeS($orderQuery);


            if (!is_array($orders)) {
                $orders = array();
            }

            $orderIdsQuery = "Select `id_order` from " . _DB_PREFIX_ . "orders o Where o.`id_customer` IN ("
                . implode(',', $customerIds) . ")";
            $orderIds = Db::getInstance()->executeS($orderIdsQuery);

            if (!is_array($orderIds)) {
                $orderIds = array();
            }

            foreach ($orderIds as $key => $value) {
                if (isset($value['id_order'])) {
                    $orderIds[$key] = $value['id_order'];
                }
            }
        }

        if (!empty($orderIds)) {
            $orderDetailsQuery = "Select * From " . _DB_PREFIX_ . "order_detail od Where od.`id_order` IN ("
                . implode(',', $orderIds) . ")";

            $orderDetails = Db::getInstance()->executeS($orderDetailsQuery);

            if (!is_array($orderDetails)) {
                $orderDetails = array();
            }

            foreach ($orders as $key => $order) {
                $orders[$key]['order_detail'] = array();
                foreach ($orderDetails as $detail) {
                    if ($order['id_order'] == $detail['id_order']) {
                        array_push($orders[$key]['order_detail'], $detail);
                    }
                }
            }
        }
        return $orders;
    }

    /**
     * @param $customerIds
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public function getCartsDataByArrayOfCustomerIds($customerIds)
    {
        $abandonedCartTime = Configuration::get('mautic_integration_mautic_abandoned_cart_time');
        $time = date('Y-m-d H:i:s ', strtotime('-' . $abandonedCartTime . ' minutes'));

        $cartsQuery = "Select * From " . _DB_PREFIX_ . "cart c Where c.`id_cart` NOT IN (SELECT id_cart FROM "
            . _DB_PREFIX_ . "orders) And c.`id_customer` IN (" . implode(',', $customerIds) . ") And c.`date_upd` <='"
            . $time . "' And c.`id_cart` IN (SELECT id_cart FROM " . _DB_PREFIX_ . "cart_product)";

        $carts = Db::getInstance()->executeS($cartsQuery);

        if (!is_array($carts)) {
            $carts = array();
        }

        $cartIdsQuery = "Select `id_cart` from " . _DB_PREFIX_ . "cart c where c.`id_cart` NOT IN (SELECT id_cart FROM "
            . _DB_PREFIX_ . "orders) And c.`id_customer` IN (" . implode(',', $customerIds) . ") And c.`date_upd` <= '"
            . $time . "' And c.`id_cart` IN (SELECT id_cart FROM " . _DB_PREFIX_ . "cart_product)";

        $cartIds = Db::getInstance()->executeS($cartIdsQuery);

        if (!is_array($cartIds)) {
            $cartIds = array();
        }

        foreach ($cartIds as $key => $value) {
            if (isset($value['id_cart'])) {
                $cartIds[$key] = $value['id_cart'];
            }
        }
        if (!empty($cartIds)) {
            $cartProductsQuery = " Select * from " . _DB_PREFIX_ . "cart_product c where c.`id_cart` IN ("
                . implode(',', $cartIds) . ")";

            $cartProducts = Db::getInstance()->executeS($cartProductsQuery);

            if (!is_array($cartProducts)) {
                $cartProducts = array();
            }

            foreach ($carts as $key => $cart) {
                $carts[$key]['cart_product'] = array();
                foreach ($cartProducts as $product) {
                    if ($cart['id_cart'] == $product['id_cart']) {
                        array_push($carts[$key]['cart_product'], $product);
                    }
                }
            }
        }
        return $carts;
    }

    /**
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public function getAbandonedCarts()
    {
        $abandonedCartTime = Configuration::get('mautic_integration_mautic_abandoned_cart_time');
        $cronTime = Configuration::get('mautic_integration_mautic_abandoned_cart_cron_time');
        if ($cronTime == Properties::ONCE_IN_A_DAY) {
            $time = 1440;
        } elseif ($cronTime == Properties::TWICE_A_DAY) {
            $time = 720;
        } elseif ($cronTime == Properties::FOUR_TIMES_A_DAY) {
            $time = 360;
        } elseif ($cronTime == Properties::EVERY_HOUR) {
            $time = 60;
        } elseif ($cronTime == Properties::EVERY_FIVE_MINUTES) {
            $time = 5;
        }
        $addedTime = $time + $abandonedCartTime;
        $from = date('Y-m-d H:i:s ', strtotime('-' . $addedTime . ' minutes'));
        $to = date('Y-m-d H:i:s ', strtotime('-' . $abandonedCartTime . ' minutes'));

        $cartsQuery = "Select * From " . _DB_PREFIX_ . "cart c Where c.`id_cart` NOT IN (SELECT id_cart FROM "
            . _DB_PREFIX_ . "orders) And c.`date_upd` >= '" . $from . "' And c.`date_upd` <= '" . $to . "'";
        $carts = Db::getInstance()->executeS($cartsQuery);

        if (!is_array($carts)) {
            $carts = array();
        }

        $cartIdsQuery = "Select `id_cart` from " . _DB_PREFIX_ . "cart c where c.`id_cart` NOT IN (SELECT id_cart FROM "
            . _DB_PREFIX_ . "orders) And c.`date_upd` >= '" . $from . "' And c.`date_upd` <= '" . $to . "'";

        $cartIds = Db::getInstance()->executeS($cartIdsQuery);

        if (!is_array($cartIds)) {
            $cartIds = array();
        }

        foreach ($cartIds as $key => $value) {
            if (isset($value['id_cart'])) {
                $cartIds[$key] = $value['id_cart'];
            }
        }

        if (!empty($cartIds)) {
            $cartProductsQuery = " Select * from " . _DB_PREFIX_ . "cart_product c where c.`id_cart` IN ("
                . implode(',', $cartIds) . ")";

            $cartProducts = Db::getInstance()->executeS($cartProductsQuery);

            if (!is_array($cartProducts)) {
                $cartProducts = array();
            }

            foreach ($carts as $key => $cart) {
                $carts[$key]['cart_product'] = array();
                foreach ($cartProducts as $product) {
                    if ($cart['id_cart'] == $product['id_cart']) {
                        array_push($carts[$key]['cart_product'], $product);
                    }
                }
            }
        }

        return $carts;
    }

    public function getAllIdsForCron()
    {
        $cronTime = Configuration::get('mautic_integration_mautic_customer_export_cron_time');
        if ($cronTime == Properties::ONCE_IN_A_DAY) {
            $time = 1440;
        } elseif ($cronTime == Properties::TWICE_A_DAY) {
            $time = 720;
        } elseif ($cronTime == Properties::FOUR_TIMES_A_DAY) {
            $time = 360;
        } elseif ($cronTime == Properties::EVERY_HOUR) {
            $time = 60;
        } elseif ($cronTime == Properties::EVERY_FIVE_MINUTES) {
            $time = 5;
        }

        $from = date('Y-m-d H:i:s ', strtotime('-' . $time . ' minutes'));
        $to = date('Y-m-d H:i:s ', time());

        $customerOrderIdsQuery = "Select `id_customer` from " . _DB_PREFIX_ . "orders o Where o.`date_upd` >=
        '" . $from . "' And o.`date_upd`<='" . $to . "'";
        $customerOrderIds = Db::getInstance()->executeS($customerOrderIdsQuery);

        if (!is_array($customerOrderIds)) {
            $customerOrderIds = array();
        }

        foreach ($customerOrderIds as $key => $value) {
            if (isset($value['id_customer'])) {
                $customerOrderIds[$key] = $value['id_customer'];
            }
        }

        $customerIdsQuery = "Select `id_customer` From " . _DB_PREFIX_ . "customer c Where c.`date_upd` >=
        '" . $from . "' And c.`date_upd`<='" . $to . "'";

        $customerIds = Db::getInstance()->executeS($customerIdsQuery);

        if (!is_array($customerIds)) {
            $customerIds = array();
        }

        foreach ($customerIds as $key => $value) {
            if (isset($value['id_customer'])) {
                $customerIds[$key] = $value['id_customer'];
            }
        }
        $allIds = array_merge($customerOrderIds, $customerIds);
        return $allIds;
    }
}
