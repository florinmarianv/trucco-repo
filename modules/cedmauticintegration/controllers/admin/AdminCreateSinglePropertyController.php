<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

include_once dirname(__FILE__) . '/../../classes/CedMautic.php';
include_once dirname(__FILE__) . '/../../classes/ConnectionManager.php';
include_once dirname(__FILE__) . '/../../classes/ExportPropertiesAndSegments.php';

class AdminCreateSinglePropertyController extends ModuleAdminControllerCore
{
    public function __construct()
    {
        $this->bootstrap = true;
        parent::__construct();
    }

    public function renderList()
    {
        parent::renderList();
        $exportProperties = new ExportPropertiesAndSegments();
        $propertyCode = Tools::getValue('alias');
        $type = Tools::getValue('type');
        $response = array('status' => 'false');
        if ($type != null && $propertyCode != null) {
            if ($type == CedMautic::TYPE_PROPERTY) {
                $propertyResponse = $exportProperties->createSingleProperty($propertyCode);
                if (isset($propertyResponse['errors'])) {
                    $response = array('status' => 'false', 'error' => 'Some problem occured.');
                } elseif (isset($propertyResponse['field'])) {
                    $this->context->smarty->assign('success', 'Property created successfully.');
                    $response = array('status' => 'true', 'success' => 'Property created successfully.');
                } else {
                    $response = array('status' => 'false', 'error' => 'Some problem occured.');
                }
            } elseif ($type == CedMautic::TYPE_SEGMENT) {
                $segnmentResponse = $exportProperties->createSingleSegment($propertyCode);
                if (isset($segnmentResponse['errors'])) {
                    $response = array('status' => 'false', 'error' => 'Some problem occured.');
                } elseif (isset($segnmentResponse['list'])) {
                    $response = array('status' => 'true', 'success' => 'Segment created successfully.');
                } else {
                    $response = array('status' => 'false', 'error' => 'Some problem occured.');
                }
            }
        }
        die(json_encode($response));
    }

    public function initContent()
    {
        parent::initContent();
        $exportProperties = new ExportPropertiesAndSegments();
        $propertyCode = Tools::getValue('alias');
        $type = Tools::getValue('type');
        $response = array('status' => 'false');
        if ($type != null && $propertyCode != null) {
            if ($type == CedMautic::TYPE_PROPERTY) {
                $propertyResponse = $exportProperties->createSingleProperty($propertyCode);
                if (isset($propertyResponse['errors'])) {
                    $this->context->smarty->assign('error', 'Some problem occured.');
                    $response = array('status' => 'false');
                } elseif (isset($propertyResponse['field'])) {
                    $this->context->smarty->assign('success', 'Property created successfully.');
                    $response = array('status' => 'true');
                } else {
                    $this->context->smarty->assign('error', 'Some problem occured.');
                    $response = array('status' => 'false');
                }
            } elseif ($type == CedMautic::TYPE_SEGMENT) {
                $segnmentResponse = $exportProperties->createSingleSegment($propertyCode);
                if (isset($segnmentResponse['errors'])) {
                    $this->context->smarty->assign('error', 'Some problem occured.');
                    $response = array('status' => 'false');
                } elseif (isset($segnmentResponse['list'])) {
                    $this->context->smarty->assign('success', 'Property created successfully.');
                    $response = array('status' => 'true');
                } else {
                    $this->context->smarty->assign('error', 'Some problem occured.');
                    $response = array('status' => 'false');
                }
            }
        }
        die(json_encode($response));
    }
}
