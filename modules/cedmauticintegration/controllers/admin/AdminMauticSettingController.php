<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

include_once dirname(__FILE__) . '/../../classes/CedMautic.php';

class AdminMauticSettingController extends ModuleAdminControllerCore
{
    public function __construct()
    {
        $this->bootstrap = true;
        parent::__construct();
    }

    public function initContent()
    {
        parent::initContent();
        $properties = CedMautic::getDataByType(CedMautic::TYPE_PROPERTY);
        $segments = CedMautic::getDataByType(CedMautic::TYPE_SEGMENT);
        if (_PS_VERSION_ >= '1.7') {
            $adminBaseUrl = '';
        } else {
            $adminBaseUrl = Tools::getShopDomain(true, true) . __PS_BASE_URI__ . basename(_PS_ADMIN_DIR_) . '/';
        }
        $this->context->smarty->assign(
            array(
                'properties' => $properties,
                'segments' => $segments,
                'export_customer_link' => $adminBaseUrl . $this->context->link->getAdminLink('AdminExportCustomers'),
                'create_single_property_link' => $adminBaseUrl . $this->context->link
                        ->getAdminLink('AdminCreateSingleProperty'),
                'status' => Configuration::get('mautic_integration_mauticapi_integration_connection_established'),
                'enabled' => Configuration::get('mautic_integration_mauticapi_integration_enable')
            )
        );

        $content = $this->context->smarty->fetch(_PS_MODULE_DIR_ .
            'cedmauticintegration/views/templates/admin/mauticsetting.tpl');
        $this->context->smarty->assign(
            array(
                'content' => $content
            )
        );
    }
}
