<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

include_once dirname(__FILE__) . '/../../classes/ConnectionManager.php';
include_once dirname(__FILE__) . '/../../classes/ExportCustomers.php';

class AdminExportCustomersController extends ModuleAdminController
{
    const CHUNK_SIZE = 5;
    public $session;

    public function __construct()
    {
        $this->bootstrap = true;
        parent::__construct();
    }

    public function renderList()
    {
        parent::renderList();
    }

    /**
     * @throws PrestaShopException
     * @throws SmartyException
     */
    public function initContent()
    {

        parent::initContent();
        $exportCustomers = new ExportCustomers();
        // case 1 ajax request for chunk processing
        $batchId = Tools::getValue('batchid');
        $customerIds = Tools::getValue('customerIds');
        $searchString = ',';
        if ($customerIds !== false && $customerIds != null && $customerIds != "") {
            if (strpos($customerIds, $searchString) !== -1 && strpos($customerIds, $searchString)) {
                $customerIds = explode(',', $customerIds);
            } else {
                $customerIds = array((int)$customerIds);
            }
        }
        if ($batchId !== false && $customerIds !== false && is_array($customerIds)) {
            try {
                $response = $exportCustomers->createBulkContacts($customerIds);
            } catch (\Exception $e) {
                $resultJson = array(
                    'error' => count($customerIds) . " Customer Export Failed",
                    'exceptionmessages' => array('errors' => $e->getMessage()),
                );
                die(json_encode($resultJson));
            }
            if (empty($response)) {
                $resultJson = array(
                    'success' => count($customerIds) . " Customer Exported Successfully",
                    'messages' => "Customer Exported Successfully"
                );
                die(json_encode($resultJson));
            } else {
                $resultJson = array(
                    'error' => count($customerIds) . " Customer Export Failed",
                    'messages' => $response,
                );
                die(json_encode($resultJson));
            }
        }

        $customerIds = $this->getAllCustomerIds();

        if (count($customerIds) == 0) {
            $this->displayError($this->l('No customer found.'));
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminMauticSetting'));
        }
        $batches = count(array_chunk($customerIds, self::CHUNK_SIZE));
        $this->context->smarty->assign(
            array(
                'errorImg' => __PS_BASE_URI__ . 'modules/cedmauticintegration/views/img/fam_bullet_error.svg',
                'successImg' => __PS_BASE_URI__ . 'modules/cedmauticintegration/views/img/fam_bullet_success.svg',
                'loaderImg' => __PS_BASE_URI__ . 'modules/cedmauticintegration/views/img/rule-ajax-loader.gif',
                'export_link' => $this->context->link->getAdminLink('AdminExportCustomers'),
                'settings_page_link' => $this->context->link->getAdminLink('AdminMauticSetting'),
                'total' => $batches,
                'customerIds' => json_encode($customerIds)
            )
        );

        $content = $this->context->smarty->fetch(_PS_MODULE_DIR_ .
            'cedmauticintegration/views/templates/admin/export.tpl');
        $this->context->smarty->assign(
            array(
                'content' => $content
            )
        );
    }

    /**
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public function getAllCustomerIds()
    {
        $sql = "Select `id_customer` from " . _DB_PREFIX_ . "customer";
        $customerIds = Db::getInstance()->executeS($sql);
        if (!is_array($customerIds)) {
            $customerIds = array();
        }
        foreach ($customerIds as $key => $value) {
            if (isset($value['id_customer'])) {
                $customerIds[$key] = $value['id_customer'];
            }
        }
        return $customerIds;
    }
}
