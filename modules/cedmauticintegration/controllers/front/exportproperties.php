<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

include_once dirname(__FILE__).'/../../classes/CedMautic.php';
include_once dirname(__FILE__).'/../../classes/ConnectionManager.php';
include_once dirname(__FILE__).'/../../classes/ExportPropertiesAndSegments.php';
class CedMauticintegrationExportPropertiesModuleFrontController extends ModuleFrontController
{
    public $connectionManager;

    public $exportProperties;

    public $mauticContactFields=array();

    public $mauticSegments=array();

    /**
     * cedmauticintegrationexportpropertiesModuleFrontController constructor.
     * @throws PrestaShopDatabaseException
     */
    public function __construct()
    {
        parent::__construct();
        $this->connectionManager = new ConnectionManager();
        $this->exportProperties = new ExportPropertiesAndSegments();
    }

    /**
     * @throws PrestaShopDatabaseException
     */
    public function initContent()
    {
        parent::initContent();
        $message = array();
        $getListResponse = $this->connectionManager->getListOfFields();
        $authenticationResponse = json_decode($getListResponse['response'], true);
        if ($getListResponse['status_code'] == 200) {
            if ($authenticationResponse) {
                if (isset($authenticationResponse['errors'])) {
                    foreach ($authenticationResponse['errors'] as $error) {
                        if (in_array($error['code'], array(400, 401))) {
                            $message =array('errors' => 'Authorization denied, invalid credentials.');
                            break;
                        } elseif (isset($error['message'])) {
                            $message = array('errors' => $error['message']);
                            break;
                        }
                    }
                    $this->connectionManager->setMauticConfig('connection_established', 0);
                    $response = json_encode($message);
                    die($response);
                } else {
                    $this->updateMauticPropertiesInDatabase($authenticationResponse);
                }
            }
        } elseif (!$authenticationResponse) {
            $this->connectionManager->setMauticConfig('connection_established', 0);
            $message = array('errors' => 'Authorization denied, invalid credentials.');
            $response = json_encode($message);
            die($response);
        }
        $param = Tools::getValue('field');
        if ($param == 'properties') {
            $propertiesResponse = $this->exportProperties->createProperties();
            if (isset($propertiesResponse['errors'])) {
                foreach ($propertiesResponse['errors'] as $error) {
                    if (in_array($error['code'], array(400, 401))) {
                        $message = array('errors' => 'Authorization denied, invalid credentials.');
                        break;
                    } elseif (isset($error['message'])) {
                        $message = array('errors' => $error['message']);
                        break;
                    }
                }
            } else {
                $message =array('0' => 'Properties has been created.',
                    '1' => 'Segment creation is in process, please do not close window');
            }
        } elseif ($param == 'segments') {
            $segmentResponse = $this->exportProperties->createSegments();
            if (isset($segmentResponse['errors'])) {
                foreach ($segmentResponse['errors'] as $error) {
                    if (in_array($error['code'], array(400, 401))) {
                        $message = array('errors' => 'Authorization denied, invalid credentials.');
                        break;
                    } elseif (isset($error['message'])) {
                        $message = array('errors' => $error['message']);
                        break;
                    }
                }
            } else {
                $message = array('0' => 'Segments Created Successfully',
                    '1' => 'You can now close window.');
            }
        }
        $response = json_encode($message);
        die($response);
    }

    public function updateMauticPropertiesInDatabase($mauticFields)
    {
        if (isset($mauticFields['total']) && $mauticFields['total'] > 0) {
            $fields = $mauticFields['fields'];
            foreach ($fields as $field) {
                if (strpos($field['alias'], 'ced_') === 0) {
                    $this->mauticContactFields[$field['alias']] = $field;
                }
            }
        }
        $type = CedMautic::TYPE_PROPERTY;
        if (isset($this->exportProperties->requiredProperties[$type])) {
            foreach ($this->exportProperties->requiredProperties[$type] as $property) {
                if (isset($this->mauticContactFields[$property['code']]) &&
                    $property['mautic_id'] != $this->mauticContactFields[$property['code']]['id']) {
                    $this->exportProperties->saveMauticId(
                        $this->mauticContactFields[$property['code']],
                        $type
                    );
                } elseif (!isset($this->mauticContactFields[$property['code']]) &&
                    $property['mautic_id'] != 0) {
                    $data = array();
                    $data['alias'] = $property['code'];
                    $data['id'] = 0;
                    $this->exportProperties->saveMauticId($data, $type);
                }
            }
        }
        $getSegmentListResponse = $this->connectionManager->getListOfSegments();
        if ($getSegmentListResponse['status_code'] == 200) {
            $segmentResponse = json_decode($getSegmentListResponse['response'], true);
            if ($segmentResponse != null) {
                if (!isset($segmentResponse['errors'])) {
                    $this->updateMauticSegmentsInDatabase($segmentResponse);
                }
            }
        }
    }

    public function updateMauticSegmentsInDatabase($mauticSegments)
    {
        if (isset($mauticSegments['total']) && $mauticSegments['total'] > 0) {
            $segments = $mauticSegments['lists'];
            foreach ($segments as $segment) {
                if (strpos($segment['alias'], 'ced-') === 0) {
                    $this->mauticSegments[$segment['alias']] = $segment;
                }
            }
        }
        $type = CedMautic::TYPE_SEGMENT;
        if (isset($this->exportProperties->requiredProperties[$type])) {
            foreach ($this->exportProperties->requiredProperties[$type] as $property) {
                if (isset($this->mauticSegments[$property['code']]) &&
                    $property['mautic_id'] != $this->mauticSegments[$property['code']]['id']) {
                    $this->exportProperties->saveMauticId(
                        $this->mauticSegments[$property['code']],
                        $type
                    );
                } elseif (!isset($this->mauticSegments[$property['code']]) &&
                    $property['mautic_id'] != 0) {
                    $data = array();
                    $data['alias'] = $property['code'];
                    $data['id'] = 0;
                    $this->exportProperties->saveMauticId($data, $type);
                }
            }
        }
    }
}
