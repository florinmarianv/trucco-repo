<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 * @category  Ced
 * @package   CedMauticIntegration
 */

include_once dirname(__FILE__) . '/../../classes/ConnectionManager.php';

class CedMauticintegrationGetCodeModuleFrontController extends ModuleFrontController
{
    /**
     * @throws PrestaShopException
     */
    public function initContent()
    {
        parent::initContent();
        $connectionManager = new ConnectionManager();
        $params = Tools::getAllValues();
        if ($connectionManager->oauthType == 'Oauth2') {
            if (isset($params['state']) && isset($params['code'])) {
                if ($params['state'] == 'cedcommerce') {
                    $code = Tools::getValue('code');
                    $response = $connectionManager->getToken($code);
                    if ($response) {
                        if (isset($response['errors'])) {
                            $connectionManager->setMauticConfig('connection_established', 0);
                        } else {
                            $connectionManager->setMauticConfig('connection_established', 1);
                        }
                    } else {
                        $connectionManager->setMauticConfig('connection_established', 0);
                    }
                } else {
                    echo Tools::displayError('Values should be greater than 0');
                    $connectionManager->setMauticConfig('connection_established', 0);
                }
            } else {
                $connectionManager->setMauticConfig('connection_established', 0);
            }
        }
        $this->context->smarty->assign(
            array(
                'connection_status' => $connectionManager->getMauticConfig(
                    'connection_established'
                ),
                'oauth_type' => $connectionManager->getMauticConfig('oauth_type'),
                'export_property_url' => $this->context->link
                    ->getModuleLink('cedmauticintegration', 'exportproperties'),
            )
        );

        if (_PS_VERSION_ >= '1.7') {
            $this->setTemplate('module:cedmauticintegration/views/templates/front/getcode.tpl');
        } else {
            $this->display_column_left = false;
            $this->display_column_right = false;
            $this->display_footer = false;
            $this->display_header = false;
            $this->setTemplate('getcode.tpl');
        }
    }
}
