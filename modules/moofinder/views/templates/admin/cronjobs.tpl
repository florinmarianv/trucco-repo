{*
 * NOTICE OF LICENSE
 *
 * This software is protected under Spanish law. Any distribution of this software
 * will be prosecuted.
 *
 * @author    Waizabú <code@waizabu.com>
 * @copyright Copyright (c) 2020 All Rights Reserved
 *
*}

<div class="alert alert-info">
    <h4>{l s='Cron jobs to regenerate feeds' mod='moofinder'}</h4>
    <dl>
        {foreach from=$fullFeedUrls item=feedUrl key=shop}
            <dt>{l s='Urls for ' mod='moofinder'} [{$shop|escape:'htmlall':'UTF-8'}]</dt>
            <dl>
                {foreach from=$feedUrl item=feed}
                    <dd>[{$feed.lang|escape:'htmlall':'UTF-8'}]
                        <a href="{html_entity_decode($feed.url|escape:'htmlall':'UTF-8')}"
                           target="_blank">
                           {$feed.url|escape:'htmlall':'UTF-8'}
                        </a>
                    </dd>
                {/foreach}
            </dl>
        {/foreach}
    </dl>
</div>
