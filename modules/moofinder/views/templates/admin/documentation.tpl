{*
 * NOTICE OF LICENSE
 *
 * This software is protected under Spanish law. Any distribution of this software
 * will be prosecuted.
 *
 * @author    Waizabú <code@waizabu.com>
 * @copyright Copyright (c) 2020 All Rights Reserved
 *
*}
<div class="row">
    <div class="col-sm-6" style="padding: 20px  40px">
        <img style="max-width: 300px; margin:auto; display: block; height: auto" src="{$module_dir|escape:'html':'UTF-8'}views/img/moofinder.png"/>
        <hr>
        <h4 class="text-center"><strong>{l s='Blazing fast searches without monthly fees' mod='moofinder'}</strong>
        </h4>
        <p>
            {l s='Moofinder is not a service. It is a module that once installed on your shop brings to use an extremely fast search engine.' mod='moofinder'}
        </p>
        <p>
            {l s='Since Moofinder is built to work on top layers of servers, it is probably the fastest search engine you can ever have.' mod='moofinder'}
        </p>
        <p>
            {l s='Among the benefits are:' mod='moofinder'}
        </p>
        <ul>
            <li>{l s='Increase your sales by reducing time needed to convert.' mod='moofinder'}</li>
            <li>{l s='Reduce the load of your site.' mod='moofinder'}</li>
            <li>{l s='Improve the user experience.' mod='moofinder'}</li>
        </ul>
    </div>
    <div class="col-sm-6" style="padding: 20px 40px">
        {if $alertTime || $alertSize}
            <div class="alert alert-danger">{l s='Your server configuration is not optimal for generating the feeds' mod='moofinder'}</div>
        {/if}

        <h2>{l s='Steps to configure this module:' mod='moofinder'}</h2>
        <ol>
            <li>
                <p><strong>{l s='First, you must generate the index product.' mod='moofinder'}</strong>
                    {l s='If you’ve already done that, you can ignore this step.' mod='moofinder'}
                    <br>
                    {l s='These are the links by shop:' mod='moofinder'}
                </p>
                <dl>
                    {foreach from=$searchUrls item=searchUrl}
                        <dd style="text-overflow:ellipsis; overflow: hidden;white-space: nowrap">
                            <a href="{html_entity_decode($searchUrl.url|escape:'htmlall':'UTF-8')}"
                               target="_blank">
                                <i class="icon-external-link-sign"></i>
                                [{html_entity_decode($searchUrl.shop|escape:'htmlall':'UTF-8')}] -
                                {l s='Re-build the entire index' mod='moofinder'}
                            </a>
                        </dd>
                    {/foreach}
                </dl>
            </li>
            <li><p><strong>{l s='Configure the settings that modules needed.' mod='moofinder'}</strong>
                    {l s='These settings are found in the second tab.' mod='moofinder'}</p></li>
            <li>
                <p><strong>{l s='Generate the feed words and feed products.' mod='moofinder'}</strong>
                    {l s='If you’ve already done that, you can ignore this step.' mod='moofinder'}
                    <br>
                    {l s='These are the links by shop and language:' mod='moofinder'}
                </p>

                <dl>
                    {foreach from=$fullFeedUrls item=feedUrl key=shop}
                        <dt>{l s='Urls for ' mod='moofinder'} [{$shop|escape:'htmlall':'UTF-8'}]</dt>
                        <dl>
                            {foreach from=$feedUrl item=feed}
                                <dd style="text-overflow:ellipsis; overflow: hidden;white-space: nowrap">
                                    [{$feed.lang|escape:'htmlall':'UTF-8'}]
                                    <a href="{html_entity_decode($feed.url|escape:'htmlall':'UTF-8')}"
                                       target="_blank">
                                        <i class="icon-external-link-sign"></i> {$feed.url|escape:'htmlall':'UTF-8'}
                                    </a>
                                </dd>
                            {/foreach}
                        </dl>
                    {/foreach}
                </dl>
            </li>
            <li><p>{l s='When the feeds are generated, the module send you an email with result.' mod='moofinder'}</p>
            </li>
            <li><p><strong>{l s='Schedule cron jobs to regenerate feeds.' mod='moofinder'}</strong>
                    {l s='These cron jobs are found in third tab.' mod='moofinder'}</p></li>
            {if !empty($testUrl)}
                <li><p>{l s='Finally, you can test Moofinder in the next link:' mod='moofinder'}</p>
                    <dl>
                        <dd><a href="{html_entity_decode($testUrl|escape:'htmlall':'UTF-8')}"
                               target="_blank"><i
                                        class="icon-external-link-sign"></i> {$testUrl|escape:'htmlall':'UTF-8'}</a>
                        </dd>
                    </dl>
                </li>
            {/if}
        </ol>
        <hr>
        <div>
            <h2>{l s='Server configuration:' mod='moofinder'}</h2>

            <p><strong>{l s='Max. execution time:' mod='moofinder'} </strong>
                <span {if $alertTime}style="color:red"{/if}> {$currentTime|escape:'htmlall':'UTF-8'}</span>
                {if $alertTime}<span> ({$minTime|escape:'htmlall':'UTF-8'} {l s='recomended' mod='moofinder'})</span>{/if}
            </p>
            <p><strong>{l s='Memory limit:' mod='moofinder'} </strong>
                <span {if $alertSize}style="color:red"{/if}> {$currentSize|escape:'htmlall':'UTF-8'}</span>
                {if $alertSize}<span> ({$minSize|escape:'htmlall':'UTF-8'} {l s='bytes recomended' mod='moofinder'})</span>{/if}
            </p>
        </div>
        <hr>
        <div class="text-center">
            {if !empty($logUrl)}
                <a href="{$module_dir|escape:'html':'UTF-8'}{html_entity_decode($logUrl|escape:'htmlall':'UTF-8')}"
                   class="btn btn-default"
                   target="_blank">
                    <i class="icon-cloud-download"></i>
                    {l s='Download log' mod='moofinder'}
                </a>
            {/if}
            {if !empty($errorLogUrl)}
                <a href="{$module_dir|escape:'html':'UTF-8'}{html_entity_decode($errorLogUrl|escape:'htmlall':'UTF-8')}"
                   class="btn btn-default"
                   target="_blank">
                    <i class="icon-cloud-download"></i>
                    {l s='Download error log' mod='moofinder'}
                </a>
            {/if}
        </div>
    </div>
</div>

