{*
 * NOTICE OF LICENSE
 *
 * This software is protected under Spanish law. Any distribution of this software
 * will be prosecuted.
 *
 * @author    Waizabú <code@waizabu.com>
 * @copyright Copyright (c) 2020 All Rights Reserved
 *
*}
<!-- Moofinder -->
<script type="text/javascript">
    {literal}
    window['moofinder'] = {
        moduleDir:  {/literal} '{$moduleDir|escape:'html':'UTF-8'}' {literal},
        dataDir:  {/literal} '{$dataDir|escape:'html':'UTF-8'}' {literal},
        shardsDir:  {/literal} '{$shardsDir|escape:'html':'UTF-8'}' {literal},
        input:  {/literal} '{$input|escape:'html':'UTF-8'}' {literal},
        imageSize:  {/literal} '{$imageSize|escape:'html':'UTF-8'}' {literal},
        imageHeight:  {/literal} '{$imageHeight|escape:'html':'UTF-8'}' {literal},
        imageWidth:  {/literal} '{$imageWidth|escape:'html':'UTF-8'}' {literal},
        imageUrl:  {/literal} '{$imageUrl|escape:'html':'UTF-8'}' {literal},
        imageDefault:  {/literal} '{$imageDefault|escape:'html':'UTF-8'}' {literal},
        productUrl:  {/literal} '{$productUrl|escape:'html':'UTF-8'}' {literal},
        moofinderTest:  {/literal} '{$moofinderTest|escape:'html':'UTF-8'}' {literal},
        moofinderTestWord:  {/literal} '{$moofinderTestWord|escape:'html':'UTF-8'}' {literal},
        pageSize:  {/literal} '{$pageSize|escape:'html':'UTF-8'}' {literal},
        depth:  {/literal} '{$depth|escape:'html':'UTF-8'}' {literal},
        exactMatch:  {/literal} '{$exactMatch|escape:'html':'UTF-8'}' {literal}
    };
    {/literal}
</script>
<template id="mfd-product-result">
    <div class="card">
        <a href="{literal}{{productUrl}}{/literal}">
            <img class="card-img-top"
                 alt="{literal}{{name}}{/literal}"
                 src="{literal}{{link}}{/literal}"
                 height="{literal}{{imageHeight}}{/literal}"
                 width="{literal}{{imageWidth}}{/literal}"
                 data-holder-rendered="true"/>
            <div class="card-header">
                <div class="card-title">
                    <span class="name">{literal}{{name}}{/literal}</span>
                    <span class="reference">{literal} #{{reference}}{/literal}</span></div>
            </div>
            <div class="price" data-id="{literal}{{id_product}}{/literal}">
                <div class="load">
                    <div></div>
                </div>
            </div>
        </a>
    </div>
</template>
<template id="mfd-no-result">
    <li class="no-results">
        {l s='No results were found for your search' mod='moofinder'}
    </li>
</template>
<div id="moo-finder">
    <div class="moo-finder-close"></div>
    <ul class="mfd-results">
    </ul>
    <div class="moo-finder-footer">
        <a href="https://moofinder.com/"
           target="_blank"><img src="{$moduleDir}/views/img/moofinder-simple-sm.png"
                                style="max-height: 20px;" alt="MooFinder search engine"></a></div>
</div>
<!-- /Moofinder -->
