<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Waizabu <code@waizabu.com>
 * @copyright E.Alamo 2020
 * @license   Propietary All rights reserved
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_2_0($object)
{
    Configuration::updateGlobalValue('MFD_' . 'DEPTH', 3);
    Configuration::updateGlobalValue('MFD_' . 'EXACT_MATCH', 0);
}
