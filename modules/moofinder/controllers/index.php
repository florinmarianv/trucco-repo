<?php
/**
 * NOTICE OF LICENSE
 *
 * This software is protected under Spanish law. Any distribution of this software
 * will be prosecuted.
 *
 * @author    Waizabú <code@waizabu.com>
 * @copyright Copyright (c) 2020 All Rights Reserved
 */
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');

header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

header('Location: ../');
exit;
