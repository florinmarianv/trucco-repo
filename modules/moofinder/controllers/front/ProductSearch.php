<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Waizabu <code@waizabu.com>
 * @copyright E.Alamo 2020
 * @license   Propietary All rights reserved
 */

require_once(_PS_MODULE_DIR_ . 'moofinder/exceptions/MoofinderException.php');

if (!defined('_PS_VERSION_')) {
    exit;
}


/**
 * Class MoofinderProductSearchModuleFrontController
 */
class MoofinderProductSearchModuleFrontController extends ModuleFrontController
{
    /**
     * Product ID to redirect it | Product IDs to get prices
     * @var mixed
     */
    public $id;

    /**
     * MoofinderProductSearchModuleFrontController constructor.
     * @throws MoofinderException
     * @throws PrestaShopException
     */
    public function __construct()
    {
        $this->id = Tools::getValue('id');
        if (!$this->id) {
            throw new MoofinderException('Invalid id');
        }
        parent::__construct();
    }

    /**
     * MoofinderProductSearchModuleFrontController constructor.
     * @throws MoofinderException
     * @throws PrestaShopException
     */
    public function init()
    {
        parent::init();
        $this->postProcess();
        die;
    }

    /**
     * @throws PrestaShopException
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {
        if (Tools::getValue('prices')) {
            echo $this->getPrices();
        } else {
            $this->redirectProduct();
        }
    }


    /**
     * @return false|string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    protected function getPrices()
    {
        $prices = [];
        if (!is_object($this->context->cart)) {
            $cart = new Cart();
            $cart->id_lang = (int)$this->context->cookie->id_lang;
            $cart->id_currency = (int)$this->context->cookie->id_currency;
            $cart->id_guest = (int)$this->context->cookie->id_guest;
            $cart->id_shop_group = (int)$this->context->shop->id_shop_group;
            $cart->id_shop = $this->context->shop->id;
            if ($this->context->cookie->id_customer) {
                $cart->id_customer = (int)$this->context->cookie->id_customer;
                $cart->id_address_delivery = (int)Address::getFirstCustomerAddressId($cart->id_customer);
                $cart->id_address_invoice = (int)$cart->id_address_delivery;
            } else {
                $cart->id_address_delivery = 0;
                $cart->id_address_invoice = 0;
            }

            // Needed if the merchant want to give a free product to every visitors
            $this->context->cart = $cart;
            CartRule::autoAddToCart($this->context);
        }

        foreach ($this->id as $id) {
            $priceStatic = Product::getPriceStatic((int)$id);
            if (version_compare(_PS_VERSION_, '1.7.6', '>=')) {
                $prices[$id] = $this->context->currentLocale->formatPrice($priceStatic, $this->context->getContext()->currency->iso_code);
            } else {
                $prices[$id] = Tools::displayPrice($priceStatic, $this->context->cookie->id_currency);
            }
        }

        return json_encode($prices);
    }


    /**
     * @throws PrestaShopException
     */
    protected function redirectProduct()
    {
        if (!$this->context) {
            $this->context = Context::getContext();
        }
        $link = $this->context->link->getProductLink($this->id);
        Tools::redirect($link);
    }
}
