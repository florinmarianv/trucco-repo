<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Waizabu <code@waizabu.com>
 * @copyright E.Alamo 2020
 * @license   Propietary All rights reserved
 */

require_once(_PS_MODULE_DIR_ . 'moofinder/services/FeedGeneratorService.php');
require_once(_PS_MODULE_DIR_ . 'moofinder/exceptions/MoofinderException.php');

/**
 * Class MoofinderFeedGeneratorModuleFrontController
 * @property Moofinder $module
 */
class MoofinderFeedGeneratorModuleFrontController extends ModuleFrontController
{
    /**
     * @var array
     */
    public $errors = [];
    /**
     * @var array
     */
    protected $productIdsProcessed = array();
    /**
     * @var array
     */
    protected $productIds = array();
    /**
     * @var int
     */
    protected $limit = 500;
    /**
     * @var int
     */
    protected $lastProcessed = 0;
    /**
     * @var int
     */
    protected $lastWordProcessed = 0;
    /**
     * @var int
     */
    protected $lastProductProcessed = 0;
    /**
     * @var bool
     */
    protected $full = true;
    /**
     * @var int
     */
    protected $langId = 0;
    /**
     * @var int start time
     */
    protected $startTime;
    /**
     * @var int end time
     */
    protected $endTime;
    /**
     * @var int start memory
     */
    protected $startMemory;
    /**
     * @var int end memory
     */
    protected $endMemory;
    /**
     * @var array Shop
     */
    protected $shopId;
    /**
     * @var FeedGeneratorService
     */
    protected $feedGenerator;
    /**
     * @var integer
     */
    protected $lastProductId;

    /**
     * MoofinderFeedGeneratorModuleFrontController constructor.
     * @throws MoofinderException
     * @throws PrestaShopDatabaseException
     */
    public function __construct()
    {
        $token = Configuration::getGlobalValue(Moofinder::PREFIX_CONFIGURATION . 'EXECUTION_TOKEN');
        if (Tools::getValue('token') != $token) {
            throw new MoofinderException('Invalid token');
        }
        parent::__construct();
        try {
            $this->startTime = time();
            $this->startMemory = memory_get_usage();
            $this->shopId = Tools::getValue('id_shop');
            if (!$this->shopId) {
                $this->shopId = $this->context->shop->id;
            }
            $this->langId = Tools::getValue('id_lang');
            if (!$this->langId) {
                $this->langId = $this->context->language->id;
            }

            $w = Configuration::get(
                Moofinder::PREFIX_CONFIGURATION . 'LAST_WORD',
                $this->langId,
                null,
                $this->shopId
            );
            $p = Configuration::get(
                Moofinder::PREFIX_CONFIGURATION . 'LAST_PRODUCT',
                $this->langId,
                null,
                $this->shopId
            );

            $dir = (int)$this->shopId . DIRECTORY_SEPARATOR . (int)$this->langId . DIRECTORY_SEPARATOR;

            $this->feedGenerator = new FeedGeneratorService($dir);

            if (Tools::getValue('full') === 1 || (!$w && !$p)) {
                $this->full = true;
                $this->updateLastProcessed(null);
                Configuration::updateValue(
                    Moofinder::PREFIX_CONFIGURATION . 'LAST_WORD',
                    [$this->langId => null],
                    false,
                    null,
                    $this->shopId
                );
                Configuration::updateValue(
                    Moofinder::PREFIX_CONFIGURATION . 'LAST_PRODUCT',
                    [$this->langId => null],
                    false,
                    null,
                    $this->shopId
                );
            } else {
                $this->initLastProcessed($w, $p);
            }
            $this->postProcess();
            $this->endTime = time() - $this->startTime;
            $this->endMemory = memory_get_usage() - $this->startMemory;
            FeedGeneratorService::log(
                'Feed generated successfully. Duration: ' .
                $this->endTime . 'seconds; Memory: ' .
                $this->endMemory . 'bytes'
            );
            if ($this->errors || $this->feedGenerator->errors) {
                $this->errors = array_merge($this->errors, $this->feedGenerator->errors);
                $result = $this->module->l('Feed generated width errors:' . PHP_EOL . $this->errorText());
                echo $result;
                MoofinderException::errorLog($result);
            } else {
                echo $this->module->l('Feed generated successfully.');
            }
            die;
        } catch (\Exception $e) {
            throw new MoofinderException(
                $this->module->l("Errors generating Feed: ") . $e->getMessage(),
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * @param $value
     */
    protected function updateLastProcessed($value)
    {
        $this->lastProcessed = $value;
    }

    /**
     * @param $word
     * @param $product
     */
    protected function initLastProcessed($word, $product)
    {
        if ($word) {
            $this->lastWordProcessed = $word;
            $this->updateLastProductProcessed(null);
            $prefix = $this->feedGenerator->getPrefix($this->lastWordProcessed, FeedGeneratorService::WORDS);
            $value = Db::getInstance()
                ->getValue(
                    'SELECT count(*)
                FROM ' . _DB_PREFIX_ . 'search_word sw 
                WHERE sw.id_lang = ' . (int)$this->langId . '
                AND sw.id_shop = ' . (int)$this->shopId . '
                AND sw.word < "' . $prefix . '"
                ORDER BY sw.word ASC'
                );
        } else {
            $this->lastProductProcessed = (string)$product;
            $this->updateLastWordProcessed(null);
            $prefix = $this->feedGenerator->getPrefix(
                $this->lastProductProcessed,
                FeedGeneratorService::PRODUCTS
            );
            $search = str_pad($prefix, Tools::strlen($this->lastProductProcessed), '0');
            $value = Db::getInstance()
                ->getValue('SELECT count(*) FROM ' . _DB_PREFIX_ . 'product p
				' . Shop::addSqlAssociation('product', 'p', true, null, true) . '
			    LEFT JOIN ' . _DB_PREFIX_ . 'product_lang pl
				    ON p.id_product = pl.id_product AND pl.id_lang = ' . (int)$this->langId . '
			
				WHERE product_shop.`indexed` = 1
				AND product_shop.`visibility` IN ("both", "search")
				AND product_shop.`active` = 1
				AND pl.`id_shop` = product_shop.`id_shop`
				AND product_shop.`id_product` < ' . (int)$search . '
				AND product_shop.`id_shop` = ' . (int)$this->shopId . '
				ORDER BY product_shop.`id_product` ASC ');
        }
        $this->lastProcessed = (int)$value;
    }

    /**
     * @param $productId
     */
    protected function updateLastProductProcessed($productId)
    {
        $this->lastProductProcessed = $productId;
        Configuration::updateValue(
            Moofinder::PREFIX_CONFIGURATION . 'LAST_PRODUCT',
            [$this->langId => $productId],
            false,
            null,
            $this->shopId
        );
    }

    /**
     * @param $word
     */
    protected function updateLastWordProcessed($word)
    {
        $this->lastWordProcessed = $word;
        Configuration::updateValue(
            Moofinder::PREFIX_CONFIGURATION . 'LAST_WORD',
            [$this->langId => $word],
            false,
            null,
            $this->shopId
        );
    }

    /**
     * @throws MoofinderException
     * @throws PrestaShopDatabaseException
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {
        ob_start();
        $this->runTasksCrons();
        ob_end_clean();
    }

    /**
     * @throws MoofinderException
     * @throws PrestaShopDatabaseException
     */
    protected function runTasksCrons()
    {
        FeedGeneratorService::log('Init Feed Generator', false);
        if ($this->lastProductProcessed) {
            $result = $this->generateProductsFeed();
        } else {
            $result = $this->generateWordsFeed();
        }
        if ($result) {
            $this->sendEmail();
        } else {
            $this->errors[] = $this->module->l('Error occurred generating Feed');
        }
    }

    /**
     * @throws MoofinderException
     * @throws PrestaShopDatabaseException
     */
    protected function generateProductsFeed()
    {
        $refresh = ($this->lastProductProcessed) ? false : true;
        $products = $this->getProducts();
        if (!empty($products)) {
            if (!$this->feedGenerator->generateShards($products, FeedGeneratorService::PRODUCTS, $refresh)) {
                throw new MoofinderException(
                    $this->module->l('Error generating products feed.') . ' ' . $this->feedGenerator->errorText()
                );
            } else {
                $this->updateLastProcessed($this->lastProcessed + $this->limit);
                $this->updateLastProductProcessed($this->lastProductProcessed);

                return $this->generateProductsFeed();
            }
        } else {
            if (!$this->feedGenerator->saveLast(FeedGeneratorService::PRODUCTS)) {
                throw new MoofinderException(
                    $this->module->l('Error generating products feed.') . ' ' . $this->feedGenerator->errorText()
                );
            }
            $this->updateLastProcessed(null);
            $this->updateLastProductProcessed(null);

            return true;
        }
    }

    /**
     * @return array
     * @throws MoofinderException
     */
    protected function getProducts()
    {
        $sql = 'SELECT p.id_product,  pl.name, p.reference, i.id_image FROM ' . _DB_PREFIX_ . 'product p
				' . Shop::addSqlAssociation('product', 'p', true, null, true) . '
			    LEFT JOIN ' . _DB_PREFIX_ . 'product_lang pl
				    ON p.id_product = pl.id_product AND pl.id_lang = ' . (int)$this->langId . '
				LEFT JOIN `' . _DB_PREFIX_ . 'image` i
			        ON i.`id_product` = p.`id_product` AND i.`cover`= 1
				LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` ish
			        ON i.`id_image` = ish.`id_image`
			        AND ish.`id_shop` = ' . (int)$this->shopId . '
				WHERE product_shop.`indexed` = 1
				AND product_shop.`visibility` IN ("both", "search")
				AND product_shop.`active` = 1
				AND pl.`id_shop` = product_shop.`id_shop`
				AND product_shop.`id_shop` = ' . (int)$this->shopId . '
				ORDER BY product_shop.`id_product` ASC 
				LIMIT ' . (int)$this->limit . ' OFFSET ' . (int)$this->lastProcessed;

        $res = Db::getInstance()->executeS($sql, true);

        $products = array();
        foreach ($res as $element) {
            if (isset($element['id_product'])) {
                $id = $element['id_product'];
                $products[$id] = $element;
            } else {
                $this->errors[] = $this->module->l('Product without id.');
            }
        }
        ksort($products);
        $keyproducts = array_keys($products);
        $this->lastProductProcessed = end($keyproducts);

        return $products;
    }

    /**
     * @throws MoofinderException
     * @throws PrestaShopDatabaseException
     */
    protected function generateWordsFeed()
    {
        $refresh = ($this->lastWordProcessed) ? false : true;
        $words = $this->getWords();

        if (empty($words)) {
            if (!$this->feedGenerator->saveLast(FeedGeneratorService::WORDS)) {
                throw new MoofinderException(
                    $this->module->l('Error generating words feed.') . ' ' . $this->feedGenerator->errorText()
                );
            }
            $this->updateLastProcessed(0);
            $this->updateLastWordProcessed(null);

            return $this->generateProductsFeed();
        } else {
            if ($this->feedGenerator->generateShards($words, FeedGeneratorService::WORDS, $refresh)) {
                $this->updateLastProcessed($this->lastProcessed + $this->limit);
                $this->updateLastWordProcessed($this->lastWordProcessed);

                return $this->generateWordsFeed();
            } else {
                throw new MoofinderException(
                    $this->module->l('Error generating words feed.') . ' ' . $this->feedGenerator->errorText()
                );
            }
        }
    }

    /**
     * @throws PrestaShopDatabaseException
     * @throws Exception
     */
    protected function getWords()
    {
        $wordsShorted = Db::getInstance()
            ->executeS(
                'SELECT sw.id_word, sw.word
                FROM ' . _DB_PREFIX_ . 'search_word sw 
                WHERE sw.id_lang = ' . (int)$this->langId . '
                AND sw.id_shop = ' . (int)$this->shopId . '
                ORDER BY sw.word ASC 
                LIMIT ' . (int)$this->limit . ' OFFSET ' . (int)$this->lastProcessed,
                true,
                false
            );
        FeedGeneratorService::log('OFFSET:' . $this->lastProcessed);

        if (empty($wordsShorted)) {
            return array();
        }
        $wordsIds = array();
        foreach ($wordsShorted as $word) {
            $wordsIds[] = $word['id_word'];
        }
        $words = array();
        $res = Db::getInstance()->executeS(
            'SELECT si.id_word, si.id_product 
                    FROM ' . _DB_PREFIX_ . 'search_index si
                    LEFT JOIN ' . _DB_PREFIX_ . 'product p ON p.id_product = si.id_product
                    ' . Shop::addSqlAssociation('product', 'p', true, null, true) . '
                    WHERE product_shop.`indexed` = 1
                    AND product_shop.`visibility` IN ("both", "search")
                    AND product_shop.`active` = 1
                    AND product_shop.`id_shop` = ' . (int)$this->shopId . '
                    AND si.id_word in  ("' . implode('","', $wordsIds) . '")
                    ORDER BY si.id_word ASC,  si.id_product ASC',
            true,
            false
        );
        $fullWords = array();
        foreach ($res as $fullWord) {
            if (!isset($fullWords[$fullWord['id_word']])) {
                $fullWords[$fullWord['id_word']] = [];
            }
            $fullWords[$fullWord['id_word']][] = $fullWord['id_product'];
        }

        foreach ($wordsShorted as $wordShorted) {
            if (isset($fullWords[$wordShorted['id_word']])) {
                $word = $wordShorted['word'];
                foreach ($fullWords[$wordShorted['id_word']] as $productId) {
                    $this->addProduct($words, $word, $productId);
                }
            }
        }

        $keywords = array_keys($words);
        $this->lastWordProcessed = end($keywords);
        FeedGeneratorService::log('words indexed');

        return $words;
    }

    /**
     * @param $words
     * @param $word
     * @param $productId
     */
    protected function addProduct(&$words, $word, $productId)
    {
        if (!isset($words[$word])) {
            $words[$word] = array((int)$productId);
        } else {
            if ($productId - 1 === $this->lastProductId) {
                $value = end($words[$word]);
                $key = key($words[$word]);
                $sum = 1;
                if (mb_strpos($value, '+')) {
                    list($value, $sum) = explode('+', $value);
                    $sum++;
                }
                $words[$word][$key] = $value . '+' . $sum;
            } else {
                $words[$word][] = (int)$productId;
            }
        }
        $this->lastProductId = (int)$productId;
    }

    /**
     * @throws MoofinderException
     */
    protected function sendEmail()
    {
        $to = Configuration::getGlobalValue(Moofinder::PREFIX_CONFIGURATION . 'EMAIL');

        if (empty($to)) {
            return true;
        }
        $text = $this->module->getEmailText();
        $subject = Configuration::get('PS_SHOP_NAME') . " - " . $text['subject'];
        $errorContent = '';
        if ($this->errors) {
            $errorContent .= '<ul>';
            foreach ($this->errors as $error) {
                $errorContent .= '<li>' . $error . '</li>';
            }
            $errorContent .= '</ul>';
        }
        $content = "<html>
				<head>
				<title>" . $subject . "</title>
				<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
				</head>
				<body>
				<table width='650' align='left' cellpadding='0' cellspacing='0'>
				  <tr>
					<td height='15'><h3>" . ($this->errors ? $text['content'] : $text['errors']) .
            "</h3>" . ($this->errors ? $errorContent : '') . "


</td>
				  </tr>
				  <tr>
					<td height='15'>" . $text['footer'] .
            "</td>
				  </tr>
				</table>
				</body>
				</html>";
        $from = Configuration::get('PS_SHOP_EMAIL');
        if (!Validate::isEmail($from)) {
            $from = null;
        }
        if (!Validate::isEmail($to)) {
            $to = null;
        }
        if (!$from || !$to) {
            $this->error[] = $this->module->l('You must specify valid shop email and valid module email.');

            return false;
        }

        $smtpChecked = Configuration::get('PS_MAIL_METHOD');
        if ($smtpChecked == 3) {
            return true;
        }
        $smtpServer = Configuration::get('PS_MAIL_SERVER');
        $smtpLogin = $from;
        $smtpPassword = Configuration::get('PS_MAIL_PASSWD');
        $smtpPassword = str_replace(
            array('&lt;', '&gt;', '&quot;', '&amp;'),
            array('<', '>', '"', '&'),
            Tools::htmlentitiesUTF8($smtpPassword)
        );
        $smtpPort = Configuration::get('PS_MAIL_SMTP_PORT');
        $smtpEncryption = Configuration::get('PS_MAIL_SMTP_ENCRYPTION');
        $type = 'text/html';
        $result = Mail::sendMailTest(
            ($smtpChecked == 2),
            Tools::htmlentitiesUTF8($smtpServer),
            Tools::htmlentitiesUTF8($content),
            Tools::htmlentitiesUTF8($subject),
            Tools::htmlentitiesUTF8($type),
            Tools::htmlentitiesUTF8($to),
            Tools::htmlentitiesUTF8($from),
            Tools::htmlentitiesUTF8($smtpLogin),
            $smtpPassword,
            Tools::htmlentitiesUTF8($smtpPort),
            Tools::htmlentitiesUTF8($smtpEncryption)
        );
        if ($result !== true) {
            $this->errors[] = $this->module->l('Error sending email.') . ' ' . $result;
        }
    }

    /**
     * Get string with all errors
     * @return string
     */
    public function errorText()
    {
        $str = '';
        foreach ($this->errors as $error) {
            $str .= $error . PHP_EOL;
        }

        return $str;
    }
}
