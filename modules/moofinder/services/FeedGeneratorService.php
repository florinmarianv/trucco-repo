<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Waizabu <code@waizabu.com>
 * @copyright E.Alamo 2020
 * @license   Propietary All rights reserved
 */

require_once(_PS_MODULE_DIR_ . 'moofinder/exceptions/MoofinderException.php');

/**
 * Class FeedGeneratorService
 * @property array $data
 * @property string $dir
 * @property boolean $refresh
 */
class FeedGeneratorService
{
    /**
     * Shortcut for `Any-Latin; Latin-ASCII; [\u0080-\uffff] remove` transliteration rule.
     *
     * The rule is loose,
     * letters will be transliterated with the characters of Basic Latin Unicode Block.
     * For example:
     * `获取到 どちら Українська: ґ,є, Српска: ђ, њ, џ! ¿Español?`
     *  will be transliterated to
     * `huo qu dao dochira Ukrainska: g,e, Srpska: d, n, d! Espanol?`.
     *
     * Used in [[transliterate()]].
     * For detailed information see [unicode normalization forms]
     * (http://unicode.org/reports/tr15/#Normalization_Forms_Table)
     * @see   http://unicode.org/reports/tr15/#Normalization_Forms_Table
     */
    const TRANSLITERATE_LOOSE = 'Any-Latin; Latin-ASCII; [\u0080-\uffff] remove';
    /**
     * Log filename
     */
    const FILENAME_LOG = 'feed_generator.log';
    /**
     * Log Path
     */
    const FILE_LOG = _PS_MODULE_DIR_ . 'moofinder/logs/' . self::FILENAME_LOG;
    /**
     * Log file opening text
     */
    const OPEN_TEXT_FILE_LOG = 'MOOFINDER_GENERATOR';
    /**
     * Maximum logging runs
     */
    const MAX_INIT_LOGS = 5;
    /**
     * Constants with cron jobs types
     */
    const WORDS = 1;
    const PRODUCTS = 2;
    /**
     * @var mixed Either a [[\Transliterator]], or a string from which a [[\Transliterator]] can be built
     * for transliteration. Used by [[transliterate()]] when intl is available. Defaults to [[TRANSLITERATE_LOOSE]]
     * @see https://secure.php.net/manual/en/transliterator.transliterate.php
     */
    public static $transliterator = self::TRANSLITERATE_LOOSE;
    /**
     * @var array fallback map for transliteration used by [[transliterate()]] when intl isn't available.
     */
    public static $transliteration = [
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
        'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
        'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
        'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
        'ß' => 'ss',
        'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
        'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
        'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
        'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
        'ÿ' => 'y',
    ];
    /**
     * @var
     */
    public $data;
    /**
     * @var
     */
    public $dir;
    /**
     * @var
     */
    public $refresh;

    /**
     * @var
     */
    public $buffer = [];

    /**
     * @var
     */
    public static $initLog = false;

    /**
     * @var
     */
    public static $resourceLog;

    /**
     * @var
     */
    public $currentShard = null;
    /**
     * @var
     */
    public $errors = [];
    /**
     * Max depth levels generated on sharding
     */
    private $max_depth = 3;
    /**
     * @var
     */
    private $totalFiles = 0;

    /**
     * @param string $dir
     * @throws MoofinderException
     */
    public function __construct($dir)
    {
        if (empty($dir)) {
            throw new MoofinderException('Directory is required');
        }
        $this->max_depth = Configuration::get(Moofinder::PREFIX_CONFIGURATION . "DEPTH");
        $this->dir = $dir;
        self::log(self::OPEN_TEXT_FILE_LOG);
    }

    /**
     * Get string with all errors
     * @return string
     */
    public function errorText()
    {
        $str = '';
        foreach ($this->errors as $error) {
            $str .= $error . PHP_EOL;
        }
        return $str;
    }

    /**
     * Generates shards for each word or for each products
     * @param array $data the array with dictionaries data:
     *                    - Words dictionaries: relation between words and products
     *                    where key is the word and value is the product ids array
     *                    - Products dictionaries: product information where key is the id and value is the product
     *                    information
     * @param int $cronType
     * @param bool $refresh
     * @return bool
     * @throws MoofinderException
     */
    public function generateShards($data, $cronType, $refresh)
    {
        if (!is_array($data)) {
            $this->errors[] = 'Data is not an array';
            return false;
        }
        $this->data = $data;
        $this->refresh = $refresh;
        if (in_array($cronType, array_keys(self::getTasksCronTypes()))) {
            $outputDir = self::getTasksCronTypes()[$cronType] . $this->dir;
        } else {
            $this->errors[] = 'Cron type not valid';
            return false;
        }
        if ($refresh) {
            self::log("Remove $outputDir");
            self::removeDirectory($outputDir);
        }

        return $this->shards($outputDir, $cronType);
    }

    /**
     * @return array
     */
    public static function getTasksCronTypes()
    {
        return [
            FeedGeneratorService::WORDS => _PS_MODULE_DIR_ . 'moofinder/data/words/',
            FeedGeneratorService::PRODUCTS => _PS_MODULE_DIR_ . 'moofinder/data/products/'
        ];
    }

    /**
     * Write message in log file
     * @param $message
     * @throws Exception
     */
    public static function log($message)
    {
        $object = (new \DateTime())->format('Y-m-d H:i:s') . ' ' . $message . PHP_EOL;
        if (!self::$initLog) {
            $filesize = filesize(self::FILE_LOG);
            if ($filesize) {
                $content = Tools::file_get_contents(self::FILE_LOG);
                $inits = explode(self::OPEN_TEXT_FILE_LOG, $content);
                if (sizeof($inits) > self::MAX_INIT_LOGS) {
                    unset($inits[0]);
                    unset($inits[1]);
                    $firstLine = self::OPEN_TEXT_FILE_LOG;
                    $content = $firstLine . implode(self::OPEN_TEXT_FILE_LOG, $inits);

                    file_put_contents(self::FILE_LOG, $content);
                }
            }
            $object = $message . PHP_EOL;
            self::$initLog = true;
        }
        if (!self::$resourceLog) {
            self::$resourceLog = fopen(self::FILE_LOG, 'a');
        }
        fwrite(self::$resourceLog, $object);
    }

    /**
     * Removes a directory (and all its content) recursively.
     * @param string $dir the directory to be deleted recursively.
     */
    public static function removeDirectory($dir)
    {
        if (!is_dir($dir)) {
            return;
        }
        if (!is_link($dir)) {
            if (!($handle = opendir($dir))) {
                return;
            }
            while (($file = readdir($handle)) !== false) {
                if ($file === '.' || $file === '..') {
                    continue;
                }
                $path = $dir . DIRECTORY_SEPARATOR . $file;
                if (is_dir($path)) {
                    static::removeDirectory($path);
                } else {
                    unlink($path);
                }
            }
            closedir($handle);
        }
        if (is_link($dir)) {
            @unlink($dir);
        } else {
            @rmdir($dir);
        }
    }


    /**
     * Generates shards for each word
     * @param $outputDir
     * @param $cronType
     * @throws MoofinderException
     */
    private function shards($outputDir, $cronType)
    {
        $dictsAvailable = array();
        foreach ($this->data as $key => $value) {
            $dictionaryName = $this->getPrefix($key, $cronType);
            if ($dictionaryName === false && $cronType == self::WORDS) {
                continue;
            }
            if ($this->currentShard !== null && $this->currentShard !== $dictionaryName) {
                if (!$this->saveFile($outputDir, $dictsAvailable)) {
                    return false;
                }
            }
            $content = [$key => $value];
            if (isset($this->buffer[$dictionaryName])) {
                $this->buffer[$dictionaryName] = array_replace_recursive($this->buffer[$dictionaryName], $content);
            } else {
                $this->buffer[$dictionaryName] = $content;
            }
            $this->currentShard = $dictionaryName;
        }
        return true;
    }

    /**
     * Save the last shard
     * @param $cronType
     * @return false
     * @throws MoofinderException
     */
    public function saveLast($cronType)
    {
        if (in_array($cronType, array_keys(self::getTasksCronTypes()))) {
            $outputDir = self::getTasksCronTypes()[$cronType] . $this->dir;
            $dictsAvailable = array();
            if (!$this->saveFile($outputDir, $dictsAvailable)) {
                return false;
            }
            $this->currentShard = null;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Generate prefix of a specific word or product id
     * @param $value
     * @param $cronType
     * @return array|false|int
     */
    public function getPrefix($value, $cronType)
    {
        if ($cronType == self::WORDS) {
            $value = self::slug($value);
        }
        $pieces = str_split($value);
        $length = $this->max_depth;
        if ($cronType == self::PRODUCTS) {
            $count = count($pieces);
            if ($count < $length) {
                $length = $count;
            }
        } else {
            $count = count($pieces);
            if ($count < $length) {
                return false;
            }
        }
        $pieces = array_slice($pieces, 0, $length);
        return implode("", $pieces);
    }

    /**
     * Returns a string with all spaces converted to given replacement,
     * non word characters removed and the rest of characters transliterated.
     *
     * If intl extension isn't available uses fallback that converts latin characters only
     * and removes the rest. You may customize characters map via $transliteration property
     * of the helper.
     *
     * @param string $string An arbitrary string to convert
     * @return string The converted string.
     */
    public static function slug($string)
    {
        $replacement = '-';
        $parts = explode($replacement, static::transliterate($string));

        $replaced = array_map(function ($element) use ($replacement) {
            $element = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $element);

            return preg_replace('/[=\s—–-]+/u', $replacement, $element);
        }, $parts);

        $string = trim(implode($replacement, $replaced), $replacement);
        $string = preg_replace('#' . preg_quote($replacement) . '+#', $replacement, $string);

        return Tools::strtolower($string);
    }


    /**
     * Returns transliterated version of a string.
     *
     * If intl extension isn't available uses fallback that converts latin characters only
     * and removes the rest. You may customize characters map via $transliteration property
     * of the helper.
     *
     * @param string $string input string
     * @param string|\Transliterator $transliterator either a [[\Transliterator]] or a string
     *                                               from which a [[\Transliterator]] can be built.
     * @return string
     * @since 2.0.7 this method is public.
     */
    public static function transliterate($string, $transliterator = null)
    {
        if (static::hasIntl() && function_exists('transliterator_transliterate')) {
            if ($transliterator === null) {
                $transliterator = static::$transliterator;
            }

            return transliterator_transliterate($transliterator, $string);
        }

        return strtr($string, static::$transliteration);
    }

    /**
     * @return bool if intl extension is loaded
     */
    protected static function hasIntl()
    {
        return extension_loaded('intl');
    }

    /**
     * Creates a new directory.
     *
     * This method is similar to the PHP `mkdir()` function except that
     * it uses `chmod()` to set the permission of the created directory
     * in order to avoid the impact of the `umask` setting.
     *
     * @param string $path path of the directory to be created.
     * @param int $mode the permission to be set for the created directory.
     * @param bool $recursive whether to create parent directories if they do not exist.
     * @return bool whether the directory is created successfully
     */
    public function createDirectory($path, $mode = 0775, $recursive = true)
    {
        if (is_dir($path)) {
            return true;
        }
        $parentDir = dirname($path);
        // recurse if parent dir does not exist and we are not at the root of the file system.
        if ($recursive && !is_dir($parentDir) && $parentDir !== $path) {
            if (!$this->createDirectory($parentDir, $mode, true)) {
                return false;
            }
        }
        try {
            if (!mkdir($path, $mode)) {
                return false;
            }
        } catch (\Exception $e) {
            if (!is_dir($path)) {
                $this->errors[] = "Failed to create directory \"$path\": " . $e->getMessage();
                return false;
            }
        }
        try {
            return chmod($path, $mode);
        } catch (\Exception $e) {
            $this->errors[] = "Failed to change permissions for directory \"$path\": " . $e->getMessage();
            return false;
        }
    }

    /**
     * Decodes JSONP
     * @param $jsonp
     * @param bool $assoc
     * @return mixed
     */
    private function jsonpDecode($jsonp, $assoc = false)
    {
        if ($jsonp[0] !== '[' && $jsonp[0] !== '{') { // we have JSONP
            $jsonp = Tools::substr($jsonp, strpos($jsonp, '('));
        }

        return json_decode(trim($jsonp, '();'), $assoc);
    }

    /**
     * Update dictionary with shards saved
     * @param $outputDir
     * @param array $dictsAvailable
     */
    protected function updateDictionary($outputDir, array $dictsAvailable)
    {
        $dictsCurrentContent = [];
        if (file_exists($outputDir . "/dicts.js")) {
            $dict = Tools::file_get_contents($outputDir . "/dicts.js");
            if (!empty($dict)) {
                $dictsCurrentContent = $this->jsonpDecode($dict, true);
            }
        }
        $dfo = fopen($outputDir . "/dicts.js", "w");
        $content = json_encode(array_values(array_unique(array_merge($dictsCurrentContent, $dictsAvailable))));
        fwrite($dfo, "callback($content)");
        fclose($dfo);
    }

    /**
     * Save current shard file
     * @param $outputDir
     * @param array $dictsAvailable
     * @return boolean
     * @throws Exception
     */
    /**
     * Save current shard file
     * @param $outputDir
     * @param array $dictsAvailable
     * @return boolean
     * @throws Exception
     */
    private function saveFile($outputDir, array &$dictsAvailable)
    {
        $dir = implode(DIRECTORY_SEPARATOR, str_split($this->currentShard));
        $targetDir = $outputDir . $dir;
        if (!in_array($this->currentShard, $dictsAvailable)) {
            $dictsAvailable[] = $this->currentShard;
        }
        $targetFile = $targetDir . DIRECTORY_SEPARATOR . $this->currentShard . ".js";
        if (!$this->createDirectory($targetDir)) {
            return false;
        }
        $content = $this->buffer[$this->currentShard];
        $this->totalFiles++;
        if (file_exists($targetFile)) {
            $currentFileData = $this->jsonpDecode(Tools::file_get_contents($targetFile), true);
            if (empty($currentFileData)) {
                $currentFileData = [];
            }
            $content = json_encode(array_replace_recursive($content, $currentFileData));
        } else {
            $content = json_encode($content);
        }
        $fp = fopen($targetFile, 'w');
        if (!$fp) {
            $this->errors[] = 'Error creating file: ' . $this->currentShard;
            return false;
        }
        if (!fwrite($fp, "callback_$this->currentShard($content)")) {
            $this->errors[] = 'Error writing file: ' . $this->currentShard;
            return false;
        }
        fclose($fp);
        unset($this->buffer[$this->currentShard]);
        self::log("Saved shard $targetFile");
        self::log("Total Files $this->totalFiles");
        $this->updateDictionary($outputDir, $dictsAvailable);
        return true;
    }
}
