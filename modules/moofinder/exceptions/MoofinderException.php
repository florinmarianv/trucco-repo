<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Waizabu <code@waizabu.com>
 * @copyright E.Alamo 2020
 * @license   Propietary All rights reserved
 */

/**
 * Class MoofinderException
 */
class MoofinderException extends \Exception
{
    /**
     * Log filename
     */
    const FILENAME_LOG = 'feed_generator.error.log';

    /**
     * Log Path
     */
    const ERROR_FILE_LOG = _PS_ROOT_DIR_ . _MODULE_DIR_ . 'moofinder/logs/' . self::FILENAME_LOG;

    /**
     * MoofinderException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $output = (new \DateTime())->format('Y-m-d H:i:s') .
            ' Code: ' . $this->code .
            ' Message: ' . $this->message;
        self::errorLog($output . PHP_EOL);
    }

    /**
     * Prints object information into error log
     *
     * @param mixed $object
     * @return void
     * @see error_log()
     */
    public static function errorLog($object)
    {
        if (version_compare(_PS_VERSION_, '1.6.1', '<')) {
            error_log($object, 3, self::ERROR_FILE_LOG);
        } else {
            Tools::error_log($object . PHP_EOL, 3, self::ERROR_FILE_LOG);
        }
    }
}
