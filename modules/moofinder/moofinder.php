<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Waizabu <code@waizabu.com>
 * @copyright E.Alamo 2020
 * @license   Propietary All rights reserved
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(_PS_MODULE_DIR_ . 'moofinder/services/FeedGeneratorService.php');
require_once(_PS_MODULE_DIR_ . 'moofinder/exceptions/MoofinderException.php');

class Moofinder extends Module
{
    const PREFIX_CONFIGURATION = 'MFD_';

    const EXACT_MATCH_NAME = "EXACT_MATCH";

    /**
     * Average execution time per word in seconds
     */
    const WORD_AVERAGE_TIME = 0.001;
    /**
     * Average memory size per word in bytes
     */
    const WORD_AVERAGE_SIZE = 840;
    /**
     * Average execution time per product in seconds
     */
    const PRODUCT_AVERAGE_TIME = 0.0002;
    /**
     * Average memory size per product in bytes
     */
    const PRODUCT_AVERAGE_SIZE = 570;

    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'moofinder';
        $this->tab = 'others';
        $this->version = '1.4.0';
        $this->author = 'Waizabú';
        $this->controllers = array('FeedGenerator', 'ProductSearch');

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Moofinder');
        $this->description = $this->l('Add ultra fast search');

        $this->confirmUninstall = $this->l('Are you sure you want uninstall this module?');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->module_key = 'c52142db7ada0a4f556fa09871135c8a';
    }

    /**
     * Module installer
     * @return bool
     * @throws PrestaShopException
     */
    public function install()
    {
        $token = Tools::encrypt(Tools::getShopDomainSsl() . time());
        Configuration::updateGlobalValue(self::PREFIX_CONFIGURATION . 'EXECUTION_TOKEN', $token);
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            Configuration::updateGlobalValue(self::PREFIX_CONFIGURATION . 'SEARCH_INPUT', '.ui-autocomplete-input');
        } else {
            Configuration::updateGlobalValue(self::PREFIX_CONFIGURATION . 'SEARCH_INPUT', '.search_query');
        }
        Configuration::updateGlobalValue(self::PREFIX_CONFIGURATION . 'TEST_WORD', '');
        Configuration::updateGlobalValue(self::PREFIX_CONFIGURATION . 'IMAGE_SIZE', ImageType::getFormatedName('home'));
        Configuration::updateGlobalValue(self::PREFIX_CONFIGURATION . 'DEPTH', 3);

        return parent::install() &&
            $this->registerHook('displayFooter') &&
            $this->registerHook('header') &&
            $this->registerHook('displayHeader');
    }

    /**
     * Module uninstaller
     * @return bool
     */
    public function uninstall()
    {
        foreach (array_keys($this->getConfigKeys()) as $key) {
            Configuration::deleteByName($key);
        }

        return parent::uninstall();
    }

    /**
     *  Return the configuration keys and if these keys are configurable or not
     * @return array
     */
    protected function getConfigKeys()
    {
        return array(
            self::PREFIX_CONFIGURATION . 'TEST_WORD' => true,
            self::PREFIX_CONFIGURATION . 'EMAIL' => true,
            self::PREFIX_CONFIGURATION . 'SEARCH_INPUT' => true,
            self::PREFIX_CONFIGURATION . 'EXECUTION_TOKEN' => false,
            self::PREFIX_CONFIGURATION . 'IMAGE_SIZE' => true,
            self::PREFIX_CONFIGURATION . 'DEPTH' => true,
            self::PREFIX_CONFIGURATION . 'EXACT_MATCH' => true,
        );
    }

    /**
     * Load the configuration form
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws SmartyException
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitMoofinderModule')) == true) {
            $this->postProcess();
        }

        return $this->renderForm();
    }

    /**
     * Validate and save form data.
     * @return bool
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();
        $preg = '/^[.#]{1}[a-zA-Z0-9_-]+$/';
        if (!preg_match($preg, Tools::getValue(self::PREFIX_CONFIGURATION . 'SEARCH_INPUT'))) {
            $this->context->controller->errors[self::PREFIX_CONFIGURATION . 'SEARCH_INPUT'] =
                $this->l('Input search identification') . ': ' . $this->l('invalid syntax:') . ' ' . $preg;

            return false;
        } else {
            if (($email = Tools::getValue(self::PREFIX_CONFIGURATION . 'EMAIL'))) {
                if (!Validate::isEmail($email)) {
                    $this->context->controller->errors[self::PREFIX_CONFIGURATION . 'EMAIL'] =
                        $this->l('Email') . ': ' . $this->l('invalid email.');

                    return false;
                }
            }
            foreach (array_keys($form_values) as $key) {
                Configuration::updateValue($key, Tools::getValue($key));
            }
        }
        $this->context->smarty->clearCompiledTemplate('moofinder.tpl');
    }

    /**
     * Set values for the inputs.
     * @return array
     */
    protected function getConfigFormValues()
    {
        $configurableFormValues = array();
        foreach ($this->getConfigKeys() as $key => $configurable) {
            if ($configurable) {
                $configurableFormValues[$key] = Configuration::getGlobalValue($key);
            }
        }

        return $configurableFormValues;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws SmartyException
     */
    protected function renderForm()
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMoofinderModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     * @return array
     * @throws PrestaShopDatabaseException
     * @throws SmartyException
     */
    protected function getConfigForm()
    {
        $this->generateTplVars();
        $cronjobs = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/cronjobs.tpl');
        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/documentation.tpl');
        $imageSizes = ImageType::getImagesTypes();
        $imageSizesAvailable = array();
        foreach ($imageSizes as $imageSize) {
            if ($imageSize['products'] == 1) {
                $imageSizesAvailable[] = $imageSize;
            }
        }

        return array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                    'title' => $this->l('Moofinder'),
                    'icon' => 'icon-search',
                ),
                'tabs' => array(
                    'DOCUMENTATION' => $this->l('Documentation'),
                    'SETTINGS' => $this->l('Settings'),
                    'CRONJOBS' => $this->l('Cron Jobs'),
                    'ADVANCED_SETTINGS' => $this->l('Advanced settings'),
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-search"></i>',
                        'desc' => $this->l('Enter a search input/s identification.') . ' ' .
                            $this->l(
                                'The "id" or "class" of search input, example of id "#search_query_top"; example of class ".search_query".'
                            )
                            . ' ' .
                            $this->l('If you have the default search module installed, you do not change this field.'),
                        'name' => self::PREFIX_CONFIGURATION . 'SEARCH_INPUT',
                        'required' => true,
                        'label' => $this->l('Input search identification'),
                        'tab' => 'SETTINGS',
                    ),
                    array(
                        'type' => 'select',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a product image size.'),
                        'default_value' => (int)$this->context->country->id,
                        'options' => array(
                            'query' => $imageSizesAvailable,
                            'id' => 'name',
                            'name' => 'name'
                        ),
                        'name' => self::PREFIX_CONFIGURATION . 'IMAGE_SIZE',
                        'required' => true,
                        'label' => $this->l('Image size'),
                        'tab' => 'SETTINGS',
                    ),
                    array(
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter an email.') . ' ' .
                            $this->l('The module will send an email in this account when the feed has been generated.'),
                        'name' => self::PREFIX_CONFIGURATION . 'EMAIL',
                        'label' => $this->l('Email'),
                        'tab' => 'SETTINGS',
                    ),
                    array(
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-search"></i>',
                        'desc' => $this->l('Enter a word/s.') . ' ' .
                            $this->l('The text will be used to test.'),
                        'name' => self::PREFIX_CONFIGURATION . 'TEST_WORD',
                        'label' => $this->l('Word/ of test'),
                        'tab' => 'SETTINGS',
                    ),
                    array(
                        'type' => 'html',
                        'label' => $this->l('IMPORTANT'),
                        'col' => 12,
                        'name' => 'cronjobs_info',
                        'html_content' => $this->l('Handle with care. A wrong configuration could make search slower'),
                        'tab' => 'ADVANCED_SETTINGS',
                    ),
                    array('type' => 'select',
                        'label' => $this->l('Search minimun resolution'),
                        'name' => self::PREFIX_CONFIGURATION . 'DEPTH',
                        'options' => array(
                            'query' => array(
                                array('id' => 1, 'name' => $this->l('1 | Slow with thousands of products.')),
                                array('id' => 2, 'name' => 2),
                                array('id' => 3, 'name' => $this->l('3 | Default')),
                            ),
                            'id' => 'id',
                            'name' => 'name',
                        ),
                        'tab' => 'ADVANCED_SETTINGS',
                        'desc' => $this->l('May be necessary clear cache after updating this param.')
                    ),
                    array(
                        'type' => 'html',
                        'label' => '',
                        'col' => 12,
                        'name' => 'cronjobs_info',
                        'html_content' => $cronjobs,
                        'tab' => 'CRONJOBS',
                    ),
                    array(
                        'type' => 'html',
                        'label' => '',
                        'col' => 12,
                        'name' => 'documentation_info',
                        'html_content' => $output,
                        'tab' => 'DOCUMENTATION',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Generate urls to cron jobs and test
     */
    protected function generateTplVars()
    {
        $token = Configuration::getGlobalValue(self::PREFIX_CONFIGURATION . 'EXECUTION_TOKEN');
        $fullUrls = array();
        $link = new Link();
        $shops = Shop::getShops();
        $searchUrls = array();
        foreach ($shops as $shop) {
            $languages = Language::getLanguages(true, $shop['id_shop']);
            $fullUrls[$shop['name']] = array();

            foreach ($languages as $lang) {
                $url = $link->getModuleLink(
                    $this->name,
                    'FeedGenerator',
                    array('token' => $token),
                    null,
                    $lang['id_lang'],
                    $shop['id_shop']
                );
                $fullUrls[$shop['name']][] = array(
                    'url' => $url,
                    'lang' => Tools::strtoupper($lang['iso_code'])
                );
            }
            $searchUrls[] = array(
                'url' => 'searchcron.php?full=1&amp;token=' . Tools::substr(_COOKIE_KEY_, 34, 8) .
                    '&amp;redirect=1&id_shop=' . (int)$shop['id_shop'],
                'shop' => $shop['name']
            );
        }
        $baseDir = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ?
            Tools::getCurrentUrlProtocolPrefix() . Tools::getShopDomainSsl()
            : _PS_BASE_URL_;
        $testUrl = $baseDir . '?moofinderTest=1';

        if (!file_exists(FeedGeneratorService::FILE_LOG)) {
            touch(FeedGeneratorService::FILE_LOG);
        }
        $logUrl = fopen(FeedGeneratorService::FILE_LOG, 'r') ?
            'logs/' . FeedGeneratorService::FILENAME_LOG :
            null;
        if (!file_exists(MoofinderException::ERROR_FILE_LOG)) {
            touch(MoofinderException::ERROR_FILE_LOG);
        }
        $errorLogUrl = fopen(MoofinderException::ERROR_FILE_LOG, 'r') ?
            'logs/' . MoofinderException::FILENAME_LOG :
            null;

        $mins = $this->calculateMinServerConf();
        $currentSize = ini_get('memory_limit');
        $currentTime = ini_get('max_execution_time');

        $this->context->smarty->assign([
            'fullFeedUrls' => $fullUrls,
            'testUrl' => $testUrl,
            'searchUrls' => $searchUrls,
            'module_dir' => $this->_path,
            'logUrl' => $logUrl,
            'errorLogUrl' => $errorLogUrl,
            'currentTime' => $currentTime,
            'currentSize' => $currentSize,
            'minSize' => $mins['minSize'],
            'alertSize' => $this->unitToInt($currentSize) < $mins['minSize'],
            'alertTime' => $currentTime < $mins['minTime'],
            'minTime' => $mins['minTime'],
        ]);
    }

    /**
     * @return float[]|int[]
     */
    public function calculateMinServerConf()
    {
        $shopId = $this->context->shop->id;
        $langId = $this->context->language->id;
        $totalProducts = (int)Db::getInstance()->getValue('SELECT count(*) FROM ' . _DB_PREFIX_ . 'product p
				' . Shop::addSqlAssociation('product', 'p', true, null, true) . '
			    LEFT JOIN ' . _DB_PREFIX_ . 'product_lang pl
				    ON p.id_product = pl.id_product AND pl.id_lang = ' . (int)$langId . '
				LEFT JOIN `' . _DB_PREFIX_ . 'image` i
			        ON i.`id_product` = p.`id_product` AND i.`cover`= 1
				LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` ish
			        ON i.`id_image` = ish.`id_image`
			        AND ish.`id_shop` = ' . (int)$shopId . '
				WHERE product_shop.`indexed` = 1
				AND product_shop.`visibility` IN ("both", "search")
				AND product_shop.`active` = 1
				AND pl.`id_shop` = product_shop.`id_shop`
				AND product_shop.`id_shop` = ' . (int)$shopId . '
				ORDER BY product_shop.`id_product` ASC');

        $totalWords = (int)Db::getInstance()
            ->getValue(
                'SELECT count(*)
                FROM ' . _DB_PREFIX_ . 'search_word sw 
                WHERE sw.id_lang = ' . (int)$langId . '
                AND sw.id_shop = ' . (int)$shopId . '
                ORDER BY sw.word ASC'
            );
        if ($totalWords && $totalProducts) {
            $minTime = ($totalWords * self::WORD_AVERAGE_TIME) + ($totalProducts * self::PRODUCT_AVERAGE_TIME);
            $minSize = ($totalWords * self::WORD_AVERAGE_SIZE) + ($totalProducts * self::PRODUCT_AVERAGE_SIZE);
        } else {
            $minSize = 0;
            $minTime = 0;
        }

        return ['minSize' => $minSize, 'minTime' => $minTime];
    }

    /**
     * Converts a number with byte unit (B / K / M / G) into an integer B
     * @param $s
     * @return int
     */
    protected function unitToInt($s)
    {
        /* converts a number with byte unit (B / K / M / G) into an integer */
        return (int)preg_replace_callback('/(\-?\d+)(.?)/', function ($m) {
            return $m[1] * pow(1024, strpos('BKMG', $m[2]));
        }, Tools::strtoupper($s));
    }

    /**
     * Add the CSS & JavaScript files and JavaScript code
     * @return string
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/dist/moofinder.js');
        $this->context->controller->addCSS($this->_path . '/views/css/dist/moofinder.min.css');
    }

    /**
     * Display product template
     * @return string
     */
    public function hookDisplayFooter()
    {
        $test = Tools::getValue('moofinderTest');

        $baseDir = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ?
            Tools::getCurrentUrlProtocolPrefix() . Tools::getShopDomainSsl()
            : _PS_BASE_URL_;
        $dir = $baseDir . _MODULE_DIR_ . $this->name . DIRECTORY_SEPARATOR;
        $dataDir = 'data' . DIRECTORY_SEPARATOR;
        $shardsDir = DIRECTORY_SEPARATOR . $this->context->shop->id . DIRECTORY_SEPARATOR .
            $this->context->language->id . DIRECTORY_SEPARATOR;
        $name = Configuration::getGlobalValue(self::PREFIX_CONFIGURATION . 'IMAGE_SIZE');
        $imageType = Db::getInstance()->getRow('SELECT * FROM `' . _DB_PREFIX_ . 'image_type` 
            WHERE `name` = \'' . pSQL($name) . '\' AND `products` = 1');
        if (empty($imageType)) {
            $imageType = [
                'height' => 250,
                'width' => 250
            ];
        }
        $tplVars = [
            'moofinderTest' => false,
            'moofinderTestWord' => null,
            'moduleDir' => $dir,
            'dataDir' => $dataDir,
            'shardsDir' => $shardsDir,
            'input' => Configuration::getGlobalValue(self::PREFIX_CONFIGURATION . 'SEARCH_INPUT'),
            'imageSize' => $name,
            'imageHeight' => $imageType['height'],
            'imageWidth' => $imageType['width'],
            'imageUrl' => _THEME_PROD_DIR_,
            'imageDefault' => $this->context->language->iso_code . '-default-' . $name . '.jpg',
            'productUrl' => $this->context->link->getModuleLink($this->name, 'ProductSearch'),
            'pageSize' => Configuration::get('PS_PRODUCTS_PER_PAGE'),
            'depth' => Configuration::get(self::PREFIX_CONFIGURATION . "DEPTH"),
            'exactMatch' => Configuration::get(self::PREFIX_CONFIGURATION . self::EXACT_MATCH_NAME),
        ];

        if ($test) {
            $tplVars['moofinderTest'] = true;
            $tplVars['moofinderTestWord'] = Configuration::getGlobalValue(self::PREFIX_CONFIGURATION . 'TEST_WORD');
        }
        $this->smarty->assign($tplVars);

        return $this->display(__FILE__, 'moofinder.tpl');
    }

    /**
     * @return array
     */
    public function getEmailText()
    {
        return array(
            'subject' => $this->l('Moofinder feed generated'),
            'content' => $this->l('Feed generated successfully.'),
            'footer' => $this->l('Begin your ultra fast searches.'),
            'errors' => $this->l('Feed generated with errors:'),
        );
    }
}
