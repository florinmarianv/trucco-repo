<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{combinationsimagesmassive}prestashop>combinationsimagesmassive_25a1adcd06dcab964c5555d84937e0ca'] = 'Edita las Imágenes de Combinaciones Masivamente';
$_MODULE['<{combinationsimagesmassive}prestashop>combinationsimagesmassive_cb0b9fd54128053884712e13011a5cd8'] = 'Con sólo un click asignas y quitas las imágenes de las combinaciones de forma automática';
$_MODULE['<{combinationsimagesmassive}prestashop>combinationsimagesmassive_826c19d01b527f2701cc3460b9c889de'] = 'Manualmente';
$_MODULE['<{combinationsimagesmassive}prestashop>combinationsimagesmassive_bf6f63550c0344e1a6716f78e172c96b'] = 'Automáticamente';
$_MODULE['<{combinationsimagesmassive}prestashop>combinationsimagesmassive_6a26f548831e6a8c26bfbbd9f6ec61e0'] = 'Ayuda';
$_MODULE['<{combinationsimagesmassive}prestashop>combinationsimagesmassive_6fc1adb3189724ee155d8404b3ad27e2'] = 'Actualizado Correctamente';
$_MODULE['<{combinationsimagesmassive}prestashop>automatic_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{combinationsimagesmassive}prestashop>automatic_0833bbf1aad5d2e5fa8a1e4f86e58160'] = 'No tiene combinaciones';
$_MODULE['<{combinationsimagesmassive}prestashop>basic_6b4a2ef056b95ed14ad265de1d5b3ce6'] = 'Quitar Imagen';
$_MODULE['<{combinationsimagesmassive}prestashop>basic_396bd70bea3437ee2fbdd961dd55edcb'] = 'Asignar Imagen';
$_MODULE['<{combinationsimagesmassive}prestashop>basic_e384efd340dc6e1f28749e129373bfd1'] = 'Este producto aun no tiene imágenes';
$_MODULE['<{combinationsimagesmassive}prestashop>content_cd4006ba2d3f119e1cdb14ccc5f12800'] = 'Filtrar por ID de Producto';
$_MODULE['<{combinationsimagesmassive}prestashop>content_a1d4dd86c5b3304730e0a3eac5eec6ae'] = 'ID Producto (ej. 1300)';
$_MODULE['<{combinationsimagesmassive}prestashop>content_f456d4d7c79ffcd7d7618bac4c3f4aaf'] = 'Filtrar por Nombre';
$_MODULE['<{combinationsimagesmassive}prestashop>content_abbf8c780b36c7adcf866f5f21a9320f'] = 'Nombre de Producto (ej. Camiseta Mujer)';
$_MODULE['<{combinationsimagesmassive}prestashop>content_083d1ae6b4b5c11ea6a997b0c7afe38e'] = 'Filtrar por Fabricante';
$_MODULE['<{combinationsimagesmassive}prestashop>content_29a40ffc0aadc64793e06801e9997750'] = 'Filtrar por Categoría';
$_MODULE['<{combinationsimagesmassive}prestashop>content_88f70e744f8d528d71e682d55ce7bf62'] = 'Cambiar Tamaño de Imagen';
$_MODULE['<{combinationsimagesmassive}prestashop>content_4a9bc9e300a0e542bcd3554e000f144a'] = 'Cantidad por Página';
$_MODULE['<{combinationsimagesmassive}prestashop>content_068f80c7519d0528fb08e82137a72131'] = 'Productos';
$_MODULE['<{combinationsimagesmassive}prestashop>content_1d4d9852d98701123588ff8d61eed722'] = 'Ir a la Página';
$_MODULE['<{combinationsimagesmassive}prestashop>content_193cfc9be3b995831c6af2fea6650e60'] = 'Página';
$_MODULE['<{combinationsimagesmassive}prestashop>content_d7778d0c64b6ba21494c97f77a66885a'] = 'Filtrar';
$_MODULE['<{combinationsimagesmassive}prestashop>content_6b4a2ef056b95ed14ad265de1d5b3ce6'] = 'Quitar Imagen';
$_MODULE['<{combinationsimagesmassive}prestashop>content_396bd70bea3437ee2fbdd961dd55edcb'] = 'Asignar Imagen';
$_MODULE['<{combinationsimagesmassive}prestashop>content_e384efd340dc6e1f28749e129373bfd1'] = 'Este producto aun no tiene imágenes';
$_MODULE['<{combinationsimagesmassive}prestashop>filter_cd4006ba2d3f119e1cdb14ccc5f12800'] = 'Filtrar Producto por ID';
$_MODULE['<{combinationsimagesmassive}prestashop>filter_a1d4dd86c5b3304730e0a3eac5eec6ae'] = 'ID Producto (ej. 1300)';
$_MODULE['<{combinationsimagesmassive}prestashop>filter_f456d4d7c79ffcd7d7618bac4c3f4aaf'] = 'Filtrar Producto por Nombre';
$_MODULE['<{combinationsimagesmassive}prestashop>filter_abbf8c780b36c7adcf866f5f21a9320f'] = 'Nombre Product';
$_MODULE['<{combinationsimagesmassive}prestashop>filter_083d1ae6b4b5c11ea6a997b0c7afe38e'] = 'Filtrar por Fabricante';
$_MODULE['<{combinationsimagesmassive}prestashop>filter_29a40ffc0aadc64793e06801e9997750'] = 'Filtrar por Categoría';
$_MODULE['<{combinationsimagesmassive}prestashop>filter_88f70e744f8d528d71e682d55ce7bf62'] = 'Cambiar tamaño imagen';
$_MODULE['<{combinationsimagesmassive}prestashop>filter_4a9bc9e300a0e542bcd3554e000f144a'] = 'Cantidad por página';
$_MODULE['<{combinationsimagesmassive}prestashop>filter_068f80c7519d0528fb08e82137a72131'] = 'Productos';
$_MODULE['<{combinationsimagesmassive}prestashop>filter_1d4d9852d98701123588ff8d61eed722'] = 'Ir a la página';
$_MODULE['<{combinationsimagesmassive}prestashop>filter_193cfc9be3b995831c6af2fea6650e60'] = 'Página';
$_MODULE['<{combinationsimagesmassive}prestashop>filter_d7778d0c64b6ba21494c97f77a66885a'] = 'Filtrar';
$_MODULE['<{combinationsimagesmassive}prestashop>help_d7778d0c64b6ba21494c97f77a66885a'] = 'Filtro';
$_MODULE['<{combinationsimagesmassive}prestashop>help_836c4c455715c19f7023958ac3f51bd0'] = 'Utiliza el filtro que tienes a la izquierda para filtrar los productos con los que quieres trabajar.';
$_MODULE['<{combinationsimagesmassive}prestashop>help_a18adbbbf5b28b3b5d35ff6d27d9d3ad'] = 'El filtro se guardará hasta que lo uses de nuevo. Así que puedes ir cambiando de apartado sin que se te borre el filtro establecido.';
$_MODULE['<{combinationsimagesmassive}prestashop>help_826c19d01b527f2701cc3460b9c889de'] = 'Manualmente';
$_MODULE['<{combinationsimagesmassive}prestashop>help_ad9099e599006643ff7d4729eda677bd'] = 'Cada vez que haces click en una imagen esta combinación se guarda automáticamente.';
$_MODULE['<{combinationsimagesmassive}prestashop>help_084e9aed3fcdd1a9ac89b95a070160fa'] = 'Se cambiará a verde si la imagen se ha asignado a la combinación. Y se le quitará el verde si se desasigna.';
$_MODULE['<{combinationsimagesmassive}prestashop>help_bf6f63550c0344e1a6716f78e172c96b'] = 'Automáticamente';
$_MODULE['<{combinationsimagesmassive}prestashop>help_4ea4158337a8398c6bc49eb2561347d9'] = 'Aparecen los productos con combinaciones agrupadas por tipo de atributo (talla, color...).';
$_MODULE['<{combinationsimagesmassive}prestashop>help_2fdf06247fb9b72e8191fe44ba379ff7'] = 'Escoge un grupo de atributos y selecciona las imágenes a asignar a ese grupo de atributos.';
$_MODULE['<{combinationsimagesmassive}prestashop>help_ba5609e7351fe6e6d0fccefb63fec181'] = 'Una vez lo tengas hecho, pulsa en guardar debajo del producto y se asignarán las imágenes a todas las combinaciones que contengan ese atributo en concreto.';
$_MODULE['<{combinationsimagesmassive}prestashop>help_7a50254a83ca5e7d5d9dad46f81fc393'] = '¡Atención!';
$_MODULE['<{combinationsimagesmassive}prestashop>help_12a6cdbf64468ac92f2dd86fa06953a8'] = 'Todo lo que no esté marcado en un grupo de atributos (talla, color...) se desasignará de las combinaciones.';
$_MODULE['<{combinationsimagesmassive}prestashop>help_b647988a9de4cd841ef485583e31659c'] = 'Por ejemplo, si tienes de Color \"blanco\" y \"negro\" y sólo asignas imágenes en uno de los dos, el otro no tendrá imágenes asignadas. Así que debes asignar imágenes al \"blanco\" y al \"negro\" y luego pulsar en guardar.';
$_MODULE['<{combinationsimagesmassive}prestashop>help_ff1d979fdb59821d80c836a4258cad5d'] = '¡Cuando entro, no veo nada asignado!';
$_MODULE['<{combinationsimagesmassive}prestashop>help_8272f4cbebb27f134f512b98fe5b08a4'] = 'No te preocupes. Es normal. Como hay distintos grupos de atributos no podemos mostrar correctamente qué imagen está asignada a cada grupo de atributos. Si quieres comprobar manualmente que se haya hecho correctamente dirígete a la sección \"manual\" y podrás comprobar que esté todo correcto.';
