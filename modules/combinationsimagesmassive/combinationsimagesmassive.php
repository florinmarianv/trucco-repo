<?php
/**
 * NOTICE OF LICENSE.
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    urimarti
 *  @copyright 2015-2020 urimarti
 *  @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class CombinationsImagesMassive extends Module
{
    public function __construct()
    {
        $this->name = 'combinationsimagesmassive';
        $this->tab = 'content_management';
        $this->version = '1.00.2';
        $this->author = 'urimarti';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->module_key = '036dc37efb306e87e8f9f5f314c74a76';
        parent::__construct();
        $this->displayName = $this->l('Edit Combinations Images Massive');
        $this->description = $this->l('Edit your product combinations images by clicking on them.');
    }

    public function install()
    {
        $filters = array(
                'image_size' => ImageType::getFormatedName('small'),
                'manufacturer' => 0,
                'category' => 0,
                'prod_per_page' => 25,
                'product_name' => '',
                'id_product' => '',
                'num_page' => 0,
        );
        Configuration::updateValue('URI_COMBINATIONS_MASSIVE', serialize($filters));
        return parent::install();
    }

    public function uninstall()
    {
        Configuration::deleteByName('URI_COMBINATIONS_MASSIVE');
        return parent::uninstall();
    }

    public function getContent()
    {
        $this->context->controller->addCSS(($this->_path).'views/css/combinationsimagesmassive.css');
        $this->context->controller->addJS($this->_path.'views/js/combinationsimagesmassive.js');
        $this->filters = unserialize(Configuration::get('URI_COMBINATIONS_MASSIVE'));
        $this->ajax_token = Tools::encrypt('combinationsimagesmassive/ajax');
        $base_url = AdminController::$currentIndex.'&configure='.
        $this->name.'&token='.Tools::getAdminTokenLite('AdminModules');
        $this->urls = array(
            'basic' => array(
                'url' => $base_url.'&mod_action=basic',
                'name' => $this->l('Manually'),
                'icon' => 'icon-list',
                'menu' => 1,
            ),
            'automatic' => array(
                'url' => $base_url.'&mod_action=automatic',
                'name' => $this->l('Automatically'),
                'icon' => 'icon-list-alt',
                'menu' => 1,
            ),
            'help' => array(
                'url' => $base_url.'&mod_action=help',
                'name' => $this->l('Help'),
                'icon' => 'icon-question',
                'menu' => 1,
            ),
        );
        $a = pSQL(Tools::getValue('mod_action'));
        return $this->postProcess() . $this->renderMenu() . $this->renderFilter($a) . $this->renderContent($a);
    }

    private function postProcess()
    {
        if (Tools::isSubmit('filter')) {
            if ((int)Tools::getValue('image_size') > 0) {
                $image_size_name = Db::getInstance()->getRow('SELECT `name`
                    FROM '._DB_PREFIX_.'image_type
                    WHERE id_image_type ='.(int)Tools::getValue('image_size'));
            } else {
                $image_size_name['name'] = pSQL(Tools::getValue('image_size'));
            }
            
            $this->filters = array(
                'image_size' => pSQL($image_size_name['name']),
                'manufacturer' => (int)Tools::getValue('id_manufacturer'),
                'category' => (int)Tools::getValue('id_category'),
                'prod_per_page' => (int)Tools::getValue('prod_per_page'),
                'product_name' => pSQL(Tools::getValue('product_name')),
                'id_product' => (int)Tools::getValue('id_product') > 0 ? (int)Tools::getValue('id_product') : '',
                'num_page' => (int)Tools::getValue('num_page'),
            );
            Configuration::updateValue('URI_COMBINATIONS_MASSIVE', serialize($this->filters));
        }
    }
    
    private function renderMenu()
    {
        $this->context->smarty->assign(array(
            'menu' => $this->urls
        ));
        return $this->context->smarty->fetch($this->local_path.'views/templates/admin/menu.tpl');
    }
    
    private function renderFilter($action)
    {
        if ($action != 'help') {
            $this->getFiltersData();
            $this->smarty->assign(array(
                'filter' => $this->filters,
            ));
            return $this->context->smarty->fetch($this->local_path.'views/templates/admin/filter.tpl');
        }
    }
    
    private function getFiltersData()
    {
        $image_sizes = Db::getInstance()->executeS('
            SELECT id_image_type AS id, `name`, width, height
            FROM '._DB_PREFIX_.'image_type
            WHERE products = 1
            ORDER BY (width*height) ASC
        ');

        $categories = Db::getInstance()->executeS('
            SELECT c.id_category, l.`name`
            FROM '._DB_PREFIX_.'category c
            INNER JOIN '._DB_PREFIX_.'category_lang l ON (
                l.id_category = c.id_category
                AND l.id_lang = '.(int)$this->context->language->id.'
            )
            WHERE c.active = 1
            AND c.id_parent != 0
            ORDER BY l.`name` ASC
        ');

        $manufacturers = Db::getInstance()->executeS('
            SELECT id_manufacturer, `name`
            FROM '._DB_PREFIX_.'manufacturer
            WHERE active = 1
            ORDER BY `name` ASC
        ');
        
        $total_product = $this->getProductsInfo(false);
        $total_pages = (@count($total_product) / ($this->filters['prod_per_page']+1));
        
        $this->context->smarty->assign(array(
            'categories' => $categories,
            'manufacturers' => $manufacturers,
            'total_pages' => (int)$total_pages,
            'image_sizes' => $image_sizes,
            'id_category' => (int)$this->filters['category'],
            'id_product' => $this->filters['id_product'],
            'prod_per_page' => (int)$this->filters['prod_per_page'],
            'id_manufacturer' => (int)$this->filters['manufacturer'],
            'prod_page' => (int)$this->filters['prod_per_page'],
            'page' => (int)$this->filters['num_page'],
            'product_name' => pSQL($this->filters['product_name']),
            'image_size' => pSQL($this->filters['image_size']),
            'CIMtoken' => $this->ajax_token,
        ));
    }

    public function getCombinationsBasic($id_product)
    {
        $return = array();
        $images = Db::getInstance()->executeS('
        SELECT id_image FROM '._DB_PREFIX_.'image WHERE id_product = '.(int)$id_product);
        $combinations = Db::getInstance()->executeS('
        SELECT id_product_attribute FROM `'._DB_PREFIX_.'product_attribute`
        WHERE id_product = '.(int)$id_product);
        foreach ($combinations as $comb) {
            $attributes = Db::getInstance()->executeS('
            SELECT l.`name` FROM '._DB_PREFIX_.'product_attribute_combination c
            INNER JOIN '._DB_PREFIX_.'attribute_lang l ON l.id_attribute = c.id_attribute
            INNER JOIN '._DB_PREFIX_.'attribute a ON a.id_attribute = c.id_attribute
            WHERE c.id_product_attribute = '.(int)$comb['id_product_attribute'].'
            AND l.id_lang = '.(int)$this->context->language->id.'
            ORDER BY a.position');

            foreach ($attributes as $attr) {
                $i = 0;
                if ($images) {
                    foreach ($images as $img) {
                        $l = $this->context->link->getImageLink('img', $img['id_image'], $this->filters['image_size']);
                        $return[$comb['id_product_attribute']]['combinations']['images'][$i]['id_image'] =
                            $img['id_image'];
                        $return[$comb['id_product_attribute']]['combinations']['images'][$i]['link'] = $l;
                        $return[$comb['id_product_attribute']]['combinations']['images'][$i]['exists'] =
                            $this->checkImageCombination(
                                $comb['id_product_attribute'],
                                $img['id_image']
                            );
                        $i++;
                    }
                } else {
                    $return[$comb['id_product_attribute']]['combinations']['images'][$i]['id_image'] = '';
                }
                
                $return[$comb['id_product_attribute']]['combinations']['text'][] = $attr['name'];
                $return[$comb['id_product_attribute']]['combinations']['id'] = $comb['id_product_attribute'];
            }
        }
        return $return;
    }
    
    public function getCombinationsAutomatic($id_product)
    {
        $return = array();
        $id_lang = (int)$this->context->language->id;
        $images = Db::getInstance()->executeS('
        SELECT id_image FROM '._DB_PREFIX_.'image WHERE id_product = '.(int)$id_product);
        foreach ($images as $img) {
            $return['images'][] = array(
                'id_image' => $img['id_image'],
                'link' => $this->context->link->getImageLink('img', $img['id_image'], $this->filters['image_size']),
            );
        }
        $combinations = Db::getInstance()->executeS('SELECT pac.id_attribute,
            al.`name` AS `attribute`, ag.`name` AS `group`
            FROM '._DB_PREFIX_.'product_attribute pa
            INNER JOIN '._DB_PREFIX_.'product_attribute_combination pac ON
                pac.id_product_attribute = pa.id_product_attribute
            INNER JOIN '._DB_PREFIX_.'attribute_lang al ON
                al.id_attribute = pac.id_attribute AND al.id_lang = '.$id_lang.'
            INNER JOIN '._DB_PREFIX_.'attribute a ON
                a.id_attribute = al.id_attribute
            INNER JOIN '._DB_PREFIX_.'attribute_group_lang ag ON
                ag.id_attribute_group = a.id_attribute_group
                AND ag.id_lang = '.$id_lang.'
            WHERE pa.id_product = '.(int)$id_product.'
            GROUP BY al.id_attribute
            ORDER BY a.position ASC');
        foreach ($combinations as $c) {
            $return['groups'][$c['group']][] = $c;
        }
        return $return;
    }

    public function checkImageCombination($id_product_attribute, $id_image)
    {
        $return = Db::getInstance()->getRow('
        SELECT * FROM '._DB_PREFIX_.'product_attribute_image
        WHERE id_product_attribute = '.(int)$id_product_attribute.'
        AND id_image = '.(int)$id_image);
        if (!$return) {
            return 0;
        } else {
            return 1;
        }
    }

    public function renderContent($action)
    {
        if ($action == 'help') {
            return $this->context->smarty->fetch($this->local_path.'views/templates/admin/help.tpl');
        }
        $prod = array();
        $products = $this->getProductsInfo(true);

        $this->context->smarty->assign(array(
            'CIMtoken' => $this->ajax_token,
        ));

        foreach ($products as $product) {
            if ($action == '' || $action == 'basic') {
                $combinations = $this->getCombinationsBasic($product['id_product']);
            } else {
                $combinations = $this->getCombinationsAutomatic($product['id_product']);
            }
            if ($combinations) {
                $prod[$product['id_product']]['id'] = $product['id_product'];
                $prod[$product['id_product']]['name'] = $product['name'];
                $prod[$product['id_product']]['information']  = $combinations;
            }
        }

        $this->context->smarty->assign(array(
            'products' => $prod,
        ));
        if ($action == '' || $action == 'basic') {
            return $this->context->smarty->fetch($this->local_path.'views/templates/admin/basic.tpl');
        } else {
            return $this->context->smarty->fetch($this->local_path.'views/templates/admin/automatic.tpl');
        }
    }
    
    public function getProductsInfo($limit = true)
    {
        $manufacturer = (int)$this->filters['manufacturer'];
        $category = (int)$this->filters['category'];
        $num_page = (int)$this->filters['num_page'];
        $prod_per_page = (int)$this->filters['prod_per_page'];
        $id_product = (int)$this->filters['id_product'];
        $product_name =  pSQL(str_replace(' ', '%', $this->filters['product_name']));
        $id_lang = (int)$this->context->language->id;

        $and = '';
        $begin = (int)$num_page * (int)$prod_per_page;


        if ((int)$manufacturer > 0) {
            $and .= ' AND p.id_manufacturer = '.(int)$manufacturer;
        }

        if ((int)$category > 0) {
            $and .= ' AND c.id_category = '.(int)$category;
        }

        if ((int)$id_product > 0) {
            $and .= ' AND p.id_product = '.(int)$id_product;
        }
        
        if ($product_name != '') {
            $and .= " AND l.`name` LIKE '%".pSQL($product_name)."%'";
        }

        $sql = 'SELECT p.id_product, l.`name`
        FROM '._DB_PREFIX_.'product p
        LEFT JOIN '._DB_PREFIX_.'product_lang l ON l.id_product = p.id_product
        LEFT JOIN '._DB_PREFIX_.'category_product c ON c.id_product = p.id_product
        WHERE l.id_lang = '.(int)$id_lang.'
        '.$and.'
        GROUP BY p.id_product';

        /* to get all products, and have its total in tpl file */
        if ($limit) {
            $sql .= ' LIMIT '.(int)$begin.','.(int)$prod_per_page;
        }
        $products = Db::getInstance()->executeS($sql);

        return $products;
    }
    
    public function updateCombinationBasic($id_product_attribute, $id_image, $method = false)
    {
        if ($method == 1) {
            Db::getInstance()->delete(
                'product_attribute_image',
                'id_product_attribute = '.(int)$id_product_attribute .'
                AND id_image = '.(int)$id_image
            );
        } elseif ($method == 0) {
            $data = array(
                'id_product_attribute' => (int)$id_product_attribute,
                'id_image' => (int)$id_image,
            );
            Db::getInstance()->insert('product_attribute_image', $data);
        }
        return $this->l('Updated Correctly');
    }


    public function updateCombinationAutomatic($data, $id_product)
    {
        if (is_array($data)) {
            $attributes = Db::getInstance()->executeS('SELECT id_product_attribute
                FROM '._DB_PREFIX_.'product_attribute
                WHERE id_product = '.(int)$id_product.'
                AND id_product_attribute > 0');
            foreach ($attributes as $a) {
                $i = (int)$a['id_product_attribute'];
                Db::getInstance()->delete('product_attribute_image', 'id_product_attribute = '.(int)$i);
            }
            foreach ($data as $id_img => $attributes) {
                $k = 1;
                $inner = '';
                foreach ($attributes as $id_attribute) {
                    if ((int)$id_attribute > 0) {
                        $inner .= ' INNER JOIN '._DB_PREFIX_.'product_attribute_combination pac'.$k.'
                                    ON pac'.$k.'.id_product_attribute = pa.id_product_attribute
                                    AND pac'.$k.'.id_attribute = '.(int)$id_attribute;
                    }
                    $k++;
                }
                if ($inner != '') {
                    $sql = 'SELECT pac1.id_product_attribute
                        FROM '._DB_PREFIX_.'product_attribute pa '.$inner.'
                        WHERE pa.id_product = '.(int)$id_product.'
                        GROUP BY pac1.id_product_attribute';
                    $combs = Db::getInstance()->executeS($sql);
                    foreach ($combs as $c) {
                        $id_product_attribute = (int)$c['id_product_attribute'];
                        $check = Db::getInstance()->getRow('SELECT COUNT(*) AS `total`
                            FROM '._DB_PREFIX_.'product_attribute_image
                            WHERE id_img = '.(int)$id_img.'
                            AND id_product_attribute = '.(int)$id_product_attribute);
                        if ((int)$check['total'] == 0) {
                            $data = array(
                                'id_product_attribute' => (int)$c['id_product_attribute'],
                                'id_image' => (int)$id_img,
                            );
                            Db::getInstance()->insert('product_attribute_image', $data);
                        }
                    }
                }
            }
        }
        return $this->l('Updated Correctly');
    }
}
