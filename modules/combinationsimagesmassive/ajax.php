<?php
/**
 * NOTICE OF LICENSE.
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    urimarti
 *  @copyright 2015-2020 urimarti
 *  @license   LICENSE.txt
 */

require_once(dirname(__FILE__).'../../../config/config.inc.php');
require_once(dirname(__FILE__).'../../../init.php');

$token = Tools::encrypt('combinationsimagesmassive/ajax');
if ($token != Tools::getValue('token') || !Module::isInstalled('combinationsimagesmassive')) {
    die('Bad token');
}

$module = Module::getInstanceByName('combinationsimagesmassive');
if ($module->active) {
    $action = Tools::getValue('action');
    if ($action == 'basic') {
        $id_image = Tools::getValue('id_image');
        $id_product_attribute = Tools::getValue('id_product_attribute');
        $method = Tools::getValue('method');
        echo $module->updateCombinationBasic($id_product_attribute, $id_image, $method);
    }
    if ($action == 'automatic') {
        $id_product = Tools::getValue('id_product');
        $data = Tools::getValue('data');
        echo $module->updateCombinationAutomatic($data, $id_product);
    }
}
