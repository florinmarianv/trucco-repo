{*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    urimarti
*  @copyright 2015-2020 urimarti
*  @license   LICENSE.txt
*}




<div class="float-left productsinfo col-xs-8 col-lg-10">
{foreach from=$products item=product}
    <div class="panel col-xs-12 float-left">
    <h3 class="text-ellipsis module-name-grid" data-toggle="tooltip" data-placement="top">{$product.name|escape:'html':'UTF-8'} <span class="badge badge-success">#{$product.id|escape:'html':'UTF-8'}</span></h3>
		{if isset($product.information.groups)}
			{foreach from=$product.information.groups key=title item=combination}
				<div class="panel">
					<div class="panel-heading toggle" data-id="{$title|escape:'html':'UTF-8'}">{$title|escape:'html':'UTF-8'}</div>
					<div class="row">
						{foreach from=$combination item=c}
							<div class="panel toggle-hidden" data-toggle="{$title|escape:'html':'UTF-8'}">
								<div class="panel-heading">{$title|escape:'html':'UTF-8'}: {$c.attribute|escape:'html':'UTF-8'}</div>
								<div class="row">
									{foreach from=$product.information.images item=img}
										<img class="img img-thumbnail float-left combinationsimagesmassive" onclick="$(this).toggleClass('selected')" src="{$img.link|escape:'html':'UTF-8'}" id-img="{$img.id_image|escape:'html':'UTF-8'}" id-attribute="{$c.id_attribute|escape:'html':'UTF-8'}" id-product="{$product.id|escape:'html':'UTF-8'}">
									{/foreach}
								</div>
							</div>
						{/foreach}
					</div>
				</div>
			{/foreach}
			<div class="panel-footer">
				<span class="btn btn-success" onclick="saveProduct({$product.id|escape:'html':'UTF-8'})"><i class="icon icon-save"></i> {l s='Save' mod='combinationsimagesmassive'}</span>
			</div>
		{else}
			<div class="row">
				<p>{l s='It has no combinations' mod='combinationsimagesmassive'}</p>
			</div>
		{/if}
    </div>
{/foreach}
</div>

<script>
    const CIMtoken = "{$CIMtoken|escape:'html':'UTF-8'}";
</script>