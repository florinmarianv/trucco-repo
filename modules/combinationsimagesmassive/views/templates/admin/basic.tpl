{*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    urimarti
*  @copyright 2015-2020 urimarti
*  @license   LICENSE.txt
*}

<div class="float-left productsinfo col-xs-8 col-lg-10">
{foreach from=$products item=product}
    <div class="panel col-xs-12 float-left">
    <h3 class="text-ellipsis module-name-grid" data-toggle="tooltip" data-placement="top">{$product.name|escape:'html':'UTF-8'} <span class="badge badge-success">#{$product.id|escape:'html':'UTF-8'}</span></h3>

        {foreach from=$product.information item=info}
        <div class="col-sm-12 col-md-6 col-lg-4">

            {foreach from=$info item=comb}

                <h4 name="combination_{$comb.id|escape:'html':'UTF-8'}">{foreach from=$comb.text item=txt} {$txt|escape:'html':'UTF-8'} {/foreach}</h4>
				
				{if $comb.images|@count}
                    {foreach from=$comb.images item=img}
                        {if $img.id_image > 0}
                            <a id="productsinfo_{$img.id_image|escape:'html':'UTF-8'}_{$comb.id|escape:'html':'UTF-8'}" onclick="javascript:CIMsubmitForm({$product.id|escape:'html':'UTF-8'}, {$img.id_image|escape:'html':'UTF-8'}, {$comb.id|escape:'html':'UTF-8'}, {$img.exists|escape:'html':'UTF-8'})">
								<img class="img img-thumbnail float-left combinationsimagesmassive {if $img.exists == 1}selected{/if}"
									title="{if $img.exists == 1}{l s='Unset Image' mod='combinationsimagesmassive'}{else}{l s='Set Image' mod='combinationsimagesmassive'}{/if}"
									id="info_{$img.id_image|escape:'html':'UTF-8'}_{$comb.id|escape:'html':'UTF-8'}"
									src="{$img.link|escape:'html':'UTF-8'}">
							</a>
                        {else}
                           <p class="alert alert-warning">{l s='This Product doesn\'t have images yet' mod='combinationsimagesmassive'}</p>
                        {/if}
                    {/foreach}
                {else}
                    {l s='This Product doesn\'t have images yet' mod='combinationsimagesmassive'}
                {/if}
                <hr>
            {/foreach}

    </div>
        {/foreach}

    </div>
{/foreach}
</div>

<script>
    const CIMtoken = "{$CIMtoken|escape:'html':'UTF-8'}";
</script>