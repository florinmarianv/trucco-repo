{*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    urimarti
*  @copyright 2015-2020 urimarti
*  @license   LICENSE.txt
*}

<div class="container">
	<div class="panel col-xs-12">
		<div class="panel-heading">{l s='Filter' mod='combinationsimagesmassive'}</div>
		<div class="row">
			<p>{l s='Use the filter on the left to filter the products you want to work with.' mod='combinationsimagesmassive'}</p>
			<p>{l s='The filter will be saved until you use it again. So you can go changing sections without deleting the established filter.' mod='combinationsimagesmassive'}</p>
		</div>
	</div>
	
	<div class="panel col-xs-12">
		<div class="panel-heading">{l s='Manually' mod='combinationsimagesmassive'}</div>
		<div class="row">
			<p>{l s='Every time you click on an image this combination is automatically saved.' mod='combinationsimagesmassive'}</p>
			<p>{l s='It will be green if this image is assigned to the combination. And it won\'t be green if it\'s not assigned.' mod='combinationsimagesmassive'}</p>
		</div>
	</div>

	<div class="panel col-xs-12">
		<div class="panel-heading">{l s='Automatically' mod='combinationsimagesmassive'}</div>
		<div class="row">
			<p>{l s='Products appear with combinations grouped by attribute type (size, color ...).' mod='combinationsimagesmassive'}</p>
			<p>{l s='Choose an attribute group and select the images to assign to that attribute group.' mod='combinationsimagesmassive'}</p>
			<p>{l s='Once you have done it, click save under the product and the images will be assigned to all the combinations that contain that specific attribute.' mod='combinationsimagesmassive'}</p>
		</div>
		<div class="row">
			<p><strong>{l s='Atention!' mod='combinationsimagesmassive'}</strong></p>
			<p>{l s='Anything that is not marked in an attribute group (size, color ...) will be unassigned from the combinations.' mod='combinationsimagesmassive'}</p>
			<p>{l s='For example, if you have Color "white" and "black" and only assign images in one of the two, the other will not have images assigned. So you must assign images to "white" and "black" and then click save.' mod='combinationsimagesmassive'}</p>
		</div>
		<div class="row">
			<p><strong>{l s='Upon entering, I don\'t see what is assigned' mod='combinationsimagesmassive'}</strong></p>
			<p>{l s='Do not worry. It is normal. As there are different groups of attributes we cannot correctly show which image is assigned to each group of attributes. If you want to manually check that it was done correctly, go to the "manual" section and you can check that everything is correct.' mod='combinationsimagesmassive'}</p>
		</div>
	</div>
</div>