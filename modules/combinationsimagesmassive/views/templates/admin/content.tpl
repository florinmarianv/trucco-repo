{*

* NOTICE OF LICENSE

*

* This file is licenced under the Software License Agreement.

* With the purchase or the installation of the software in your application

* you accept the licence agreement.

*

* You must not modify, adapt or create derivative works of this source code

*

*  @author    urimarti

*  @copyright 2015-2020 urimarti

*  @license   LICENSE.txt

*}



<div id="result"></div>



<div class="loading" style="display:none">

	<div class="icon-spinner">

    </div>

</div>

    

<div class="panel menu_filter_module col-xs-4 col-lg-2 float-left">



    <form id="productsinfo" name="filter" method="post" enctype="multipart/form-data">

       

       <div class="col-xs-12">

        <input type="hidden" name="filter" value="1" />



        <div class="form-group">

            <label for="id_product">{l s='Filter Product by ID' mod='combinationsimagesmassive'}</label>

            <input class="form-control" type="text" name="id_product" placeholder="{l s='ID Product (ex. 1300)' mod='combinationsimagesmassive'}" value="{$id_product|escape:'html':'UTF-8'}">

        </div>

        

        <div class="form-group">

            <label for="name">{l s='Filter Product by Name' mod='combinationsimagesmassive'}</label>

            <input class="form-control" type="text" name="product_name" placeholder="{l s='Product Name (ex. T-shirt Women Red)' mod='combinationsimagesmassive'}" value="{$product_name|escape:'html':'UTF-8'}">

        </div>



        <div class="form-group">

            <label for="id_manufacturer">{l s='Filter by Manufacturer' mod='combinationsimagesmassive'}</label>

            <select name="id_manufacturer">

                <option {if $id_manufacturer == 0}selected="selected"{/if} value="0">--</option>

                {foreach from=$manufacturers item=manufacturer}

                    <option {if $id_manufacturer == $manufacturer.id_manufacturer}selected="selected"{/if} value="{$manufacturer.id_manufacturer|escape:'html':'UTF-8'}">{$manufacturer.name|escape:'html':'UTF-8'}</option>

                {/foreach}

            </select>

        </div>



        <div class="form-group">

            <label for="id_category">{l s='Filter by Category' mod='combinationsimagesmassive'}</label>

            <select name="id_category">

                <option {if $id_category == 0}selected="selected"{/if} value="0">--</option>

                {foreach from=$categories item=cat}

                    <option {if $id_category == $cat.id_category}selected="selected"{/if} value="{$cat.id_category|escape:'html':'UTF-8'}">{$cat.name|escape:'html':'UTF-8'}</option>

                {/foreach}

            </select>

        </div>

        

        <div class="form-group">

            <label for="image_size">{l s='Change image size' mod='combinationsimagesmassive'}</label>

            <select name="image_size">

                {foreach from=$image_sizes item=img}

                    <option {if $img.name == $image_size}selected="selected"{/if} value="{$img.id|escape:'html':'UTF-8'}">{$img.name|escape:'html':'UTF-8'} ({$img.width|escape:'html':'UTF-8'}*{$img.height|escape:'html':'UTF-8'}px)</option>

                {/foreach}

            </select>

        </div>



        <div class="form-group">

            <label for="prod_per_page">{l s='Quantity per Page' mod='combinationsimagesmassive'}</label>

            <select name="prod_per_page">

                <option {if $prod_per_page == 25}selected="selected"{/if} value="25">25 {l s='Products' mod='combinationsimagesmassive'}</option>

                <option {if $prod_per_page == 50}selected="selected"{/if} value="50">50 {l s='Products' mod='combinationsimagesmassive'}</option>

                <option {if $prod_per_page == 100}selected="selected"{/if} value="100">100 {l s='Products' mod='combinationsimagesmassive'}</option>

            </select>

        </div>

        

        {if $total_pages > 1}

        <div class="form-group">

    

       <label for="num_page">{l s='Go to page' mod='combinationsimagesmassive'}</label>

        <select name="num_page">

            <option selected="selected" value="">--</option>

            {for $i=0 to $total_pages}

                <option value="{$i|escape:'htmlall':'UTF-8'}">{l s='Page' mod='combinationsimagesmassive'} {$i+1|escape:'htmlall':'UTF-8'}</option>

            {/for}            

        </select>

        </div>

        {/if}

        

        <div class="form-group">

            <button type="submit" id="filter" class="btn btn-success col-xs-12">{l s='Filter' mod='combinationsimagesmassive'}</button>

        </div>

    </div>

    {if $total_pages > 0}

    <div class="col-xs-12">

        <div class="col-xs-12 pagination_filter_module">

            {if $page > 0}<button type="submit" value="{$page-1|escape:'htmlall':'UTF-8'}" class="btn btn-default col-xs-2 " name="num_page">&laquo;</button>{/if}

            <button type="submit" value="{$page|escape:'htmlall':'UTF-8'}" class="btn btn-default active col-xs-2 mt-3" name="num_page">{$page+1|escape:'htmlall':'UTF-8'}</button>

            {if $page < $total_pages}<button type="submit" value="{$page+1|escape:'htmlall':'UTF-8'}" class="btn btn-default col-xs-2 mt-3" name="num_page">&raquo;</button>{/if}

        </div>

    </div>

    {/if}

        <!-- END PAGINATION TOP -->

        

    </form>

</div>





<div class="float-left productsinfo col-xs-8 col-lg-10">

{foreach from=$products item=product}

    <div class="panel col-xs-12 float-left">

    <h3 class="text-ellipsis module-name-grid" data-toggle="tooltip" data-placement="top">{$product.name|escape:'html':'UTF-8'} <span class="badge badge-success">#{$product.id|escape:'html':'UTF-8'}</span></h3>



        {foreach from=$product.information item=info}

        <div class="col-sm-12 col-md-6 col-lg-4">



            {foreach from=$info item=comb}



                <h4 name="combination_{$comb.id|escape:'html':'UTF-8'}">{foreach from=$comb.text item=txt} {$txt|escape:'html':'UTF-8'} {/foreach}</h4>

				

				{if $comb.images|@count}

                    {foreach from=$comb.images item=img}

                        {if $img.id_image > 0}

                            <a id="productsinfo_{$img.id_image|escape:'html':'UTF-8'}_{$comb.id|escape:'html':'UTF-8'}" onclick="javascript:CIMsubmitForm({$product.id|escape:'html':'UTF-8'}, {$img.id_image|escape:'html':'UTF-8'}, {$comb.id|escape:'html':'UTF-8'}, {$img.exists|escape:'html':'UTF-8'})">

								<img class="img img-thumbnail float-left combinationsimagesmassive {if $img.exists == 1}selected" title="{l s='Unset Image' mod='combinationsimagesmassive'}{else}" title="{l s='Set Image' mod='combinationsimagesmassive'}{/if}" id="info_{$img.id_image|escape:'html':'UTF-8'}_{$comb.id|escape:'html':'UTF-8'}" src="{$url}/{$img.id_image|escape:'html':'UTF-8'}-{$image_size|escape:'html':'UTF-8'}/image.jpg">

							</a>

                        {else}

                           <p class="alert alert-warning">{l s='This Product doesn\'t have images yet' mod='combinationsimagesmassive'}</p>

                        {/if}

                    {/foreach}

                {else}

                    {l s='This Product doesn\'t have images yet' mod='combinationsimagesmassive'}

                {/if}

                <hr>



            {/foreach}



    </div>

        {/foreach}



    </div>

{/foreach}

</div>



{if $id_combination_edit && $id_combination_edit > 0}

    {literal}

    <script>

		goToByScroll('combination_{/literal}{$id_combination_edit|escape:'html':'UTF-8'}{literal}');   

    </script>

    {/literal}

{/if}