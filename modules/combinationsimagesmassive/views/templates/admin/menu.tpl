{*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    urimarti
*  @copyright 2015-2020 ILLUMINATED SING, SL
*  @license   LICENSE.txt
*}

{if isset($menu) && is_array($menu)}
	{literal}
	<style>
		.panel.menu_module {display: flex;flex-wrap: wrap;align-items: center;justify-content: space-evenly;font-size: 1.5eM;}
		.panel.menu_module i {font-size: 1.5eM;display: block;text-align: center;}
		.panel.menu_module li {list-style: none;}
	</style>
	{/literal}
	<div id="growl"></div>
	<div class="panel menu_module">
		{foreach from=$menu key=k item=m}
			{if $m.menu == 1}
				<li><a href="{$m.url|escape:'html':'UTF-8'}" {if $k == 'cron'}target="_blank"{/if}><i class="{$m.icon|escape:'html':'UTF-8'}"></i> {$m.name|escape:'html':'UTF-8'}</a></li>
			{/if}
		{/foreach}
	</div>
{/if}