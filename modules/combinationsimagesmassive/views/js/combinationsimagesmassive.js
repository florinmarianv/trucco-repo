/*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    Oriol Martí
*  @copyright 2015-2020 ILLUMINATED SING, SL
*  @license   LICENSE.txt
*/

jQuery(document).ready(function(){
    jQuery('form button#filter').click(function(){
        jQuery('.loading').show();
    })
	jQuery('.toggle-hidden').slideUp();
	jQuery('.toggle').click(function(){
		var id = jQuery(this).attr('data-id');
		jQuery('.toggle-hidden[data-toggle="'+id+'"]').slideToggle().toggleClass('selected');
	})
})

function CIMsubmitForm(id_product, id_image, id_product_attribute, method){
	jQuery.ajax({
		type: "POST",
		url: "../modules/combinationsimagesmassive/ajax.php",
		data: {id_product:id_product, id_image:id_image, id_product_attribute:id_product_attribute, method:method, token:CIMtoken, action:'basic'},
		dataType: "html",
		success: function(data){
			showMessage(data,'success');
			updateLink(id_product, id_image, id_product_attribute, method)
		},
		error: function(){
			showMessage("error ajax", 'warning');
		}
	});	
}

function updateLink(id_product, id_image, id_product_attribute, method, CIMtoken){
	if(method == 0) {
		method_2 = 1;
	} else if (method == 1){
		method_2 = 0;
	}
	jQuery('#productsinfo_'+id_image+'_'+id_product_attribute).attr('onclick','CIMsubmitForm('+id_product+', '+id_image+', '+id_product_attribute+','+method_2+')')
	jQuery('#productsinfo_'+id_image+'_'+id_product_attribute+' img').toggleClass('selected')
}

function showMessage(message, type)
{
	if(type == 'warning') {
		return jQuery.growl.error({
			title: "",
			size: "large",
			message: message
		});
	}
	
	if(type == 'success') {
		return jQuery.growl.notice({
			title: "",
			size: "large",
			message: message
		});
	}
}

function saveProduct(id_product)
{
	var data, e;
	data = [];
	e = jQuery('img.selected[id-product="'+id_product+'"]');
	jQuery.each(e, function(){
        var id_img = parseInt(jQuery(this).attr('id-img'));
        var id_attribute = parseInt(jQuery(this).attr('id-attribute'));
        if(typeof(data[id_img]) == 'undefined') {
            data[id_img] = [];
        }
		data[id_img].push(id_attribute);
	})
	jQuery.ajax({
		type: "POST",
		url: "../modules/combinationsimagesmassive/ajax.php",
		data: {data:data, id_product:parseInt(id_product), token:CIMtoken, action:'automatic'},
		dataType: "html",
		success: function(data){
			showMessage(data,'success');
		},
		error: function(){
			showMessage("error ajax", 'warning');
		}
	});	
}