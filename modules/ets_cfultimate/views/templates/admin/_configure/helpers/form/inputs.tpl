{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}

<h3 class="ets_cfu_title">{l s='Add input field' mod='ets_cfultimate'}</h3>
<div class="ets_cfu_panel_inputs">
    {if isset($inputs) && $inputs}
    <div class="ets_cfu_input_box tag-generator-list">
    {foreach from=$inputs item='input'}
        {assign var='enabled' value = isset($input.enabled) && !$input.enabled}
        <div class="ets_cfu_input_{$input.id|escape:'html':'utf-8'}">
            {if $enabled}<a class="ets_cfu_google_captcha" target="_blank" href="{$google_captcha|escape:'html':'utf-8'}" title="{l s='Setting' mod='ets_cfultimate'}"><i class="fa fa-cog"></i></a>{/if}
            <a class="thickbox button{if $enabled} disabled{/if}" href="#tag-generator-panel-{$input.id|escape:'html':'utf-8'}">
                <div class="ets_cfu_input-img">
                    <img alt="{$input.label|escape:'html':'utf-8'}" src="{$image_baseurl|cat:'ip_'|cat:$input.id|escape:'html':'utf-8'}.png" /><br />
                </div>
                {$input.label|escape:'html':'utf-8'}
            </a>
        {if $enabled}
            <span class="ets_cfu_tooltip">{l s='If you want to add reCaptcha type of input field, please navigate to [1]Settings > Integration[/1] subtab first. Enable reCaptcha feature then enter your site key and secret key' tags=['<strong>'] mod='ets_cfultimate'}</span>
        {/if}
        </div>{/foreach}
    </div>{/if}
</div>