{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
{include file="./btns.tpl"}
<div class="ets_cfu_row">
    {for $ik = 1 to $col}
        <div class="ets_cfu_col col{$ik|intval}" data-col="col{$ik|intval}">
            <div class="ets_cfu_col_box">
                <span class="ets_cfu_add_input"><i class="icon-plus-circle"></i>&nbsp;{l s='Add input field' mod='ets_cfultimate'}</span>
            </div>
        </div>
    {/for}
</div>