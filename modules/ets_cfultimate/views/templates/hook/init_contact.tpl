{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
{assign var="is_multi_lang" value=$languages|count > 1}
{if $type == 'rf'}
    <div class="ets_cfu_box style3" data-type="style3" data-order="0">
        <div class="ets_cfu_btn">
            <span class="ets_cfu_btn_edit"
                  title="{l s='Edit' mod='ets_cfultimate'}">{l s='Edit' mod='ets_cfultimate'}</span>
            <span class="ets_cfu_btn_copy"
                  title="{l s='Duplicate' mod='ets_cfultimate'}">{l s='Duplicate' mod='ets_cfultimate'}</span>
            <span class="ets_cfu_btn_delete"
                  title="{l s='Delete' mod='ets_cfultimate'}">{l s='Delete' mod='ets_cfultimate'}</span>
            <span class="ets_cfu_btn_drag_drop"
                  title="{l s='Drag drop' mod='ets_cfultimate'}">{l s='Drag drop' mod='ets_cfultimate'}</span>
        </div>
        <div class="ets_cfu_row">
            <div class="ets_cfu_col col1" data-col="col1">
                <div class="ets_cfu_col_box ui-sortable">
                    <span class="ets_cfu_add_input"><i class="icon-plus-circle"></i>&nbsp;{l s='Add input field' mod='ets_cfultimate'}</span>
                    <div class="ets_cfu_input_text ets_cfu_input" data-type="text" data-required="1"
                         data-name="text-652" data-placeholder="" data-id="" data-class="" data-mailtag="1"
                         data-default="user_first_name">
                        <div class="ets_cfu_input_generator">
                            <img src="{$icon_link|escape:'html':'utf-8'}ip_text.png" alt="Input text">
                            <div class="ets_cfu_form_data">
                                {if $is_multi_lang}{foreach from=$languages item='lang'}
                                    <span class="ets_cfu_label_{$lang.id_lang|intval}"{if $lang.id_lang != $id_lang_default} style="display:none;"{/if}>First Name</span>
                                    <span class="ets_cfu_short_code_{$lang.id_lang|intval}" style="display:none;">First Name*[text* text-652 default:user_full_name]</span>
                                    <span class="ets_cfu_values_{$lang.id_lang|intval}" style="display:none;"></span>
                                <span class="ets_cfu_desc_{$lang.id_lang|intval}" style="display:none;">First Name*[text* text-652 default:user_full_name]</span>{/foreach}
                                {else}
                                    <span class="ets_cfu_label_{$id_lang_default|intval}">First Name</span>
                                    <span class="ets_cfu_short_code_{$id_lang_default|intval}" style="display:none;">First Name*[text* text-652 default:user_full_name]</span>
                                    <span class="ets_cfu_values_{$id_lang_default|intval}" style="display:none;"></span>
                                <span class="ets_cfu_desc_{$id_lang_default|intval}" style="display:none;"></span>{/if}
                            </div>
                            <p class="ets_cfu_help_block">Text</p>
                        </div>
                        <div class="ets_cfu_btn_input">
                            <span class="settings_icon" title="{l s='Setting' mod='ets_cfultimate'}">
                                <i class="fa fa-cog"></i></span>
                            <div class="settings_icon_content">
                                <span class="ets_cfu_btn_edit_input" title="{l s='Edit' mod='ets_cfultimate'}"><i class="fa fa-pencil-square-o"></i>&nbsp;{l s='Edit' mod='ets_cfultimate'}</span>
                                <span class="ets_cfu_btn_copy_input" title="{l s='Duplicate' mod='ets_cfultimate'}"><i class="fa fa-files-o"></i>&nbsp;{l s='Duplicate' mod='ets_cfultimate'}</span>
                                <span class="ets_cfu_btn_delete_input" title="{l s='Delete' mod='ets_cfultimate'}"><i class="fa fa-trash-o"></i>&nbsp;{l s='Delete' mod='ets_cfultimate'}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ets_cfu_col col2" data-col="col2">
                <div class="ets_cfu_col_box ui-sortable">
                    <span class="ets_cfu_add_input"><i class="icon-plus-circle"></i>&nbsp;{l s='Add input field' mod='ets_cfultimate'}</span>
                    <div class="ets_cfu_input_text ets_cfu_input" data-type="text" data-required="1" data-name="text-85"
                         data-placeholder="" data-id="" data-class="" data-mailtag="0" data-default="user_last_name">
                        <div class="ets_cfu_input_generator">
                            <img src="{$icon_link|escape:'html':'utf-8'}ip_text.png" alt="Input text">
                            <div class="ets_cfu_form_data">
                                {if $is_multi_lang}{foreach from=$languages item='lang'}
                                    <span class="ets_cfu_label_{$lang.id_lang|intval}"{if $lang.id_lang != $id_lang_default} style="display:none;"{/if}>Last Name</span>
                                    <span class="ets_cfu_short_code_{$lang.id_lang|intval}" style="display:none;">Last Name*[text* text-85 default:user_last_name]</span>
                                    <span class="ets_cfu_values_{$lang.id_lang|intval}" style="display:none;"></span>
                                    <span class="ets_cfu_desc_{$lang.id_lang|intval}" style="display:none;"></span>
                                {/foreach}
                                {else}
                                    <span class="ets_cfu_label_{$id_lang_default|intval}">Last Name</span>
                                    <span class="ets_cfu_short_code_{$id_lang_default|intval}" style="display:none;">Last Name*[text* text-85 default:user_last_name]</span>
                                    <span class="ets_cfu_values_{$id_lang_default|intval}" style="display:none;"></span>
                                    <span class="ets_cfu_desc_{$id_lang_default|intval}" style="display:none;"></span>
                                {/if}
                            </div>
                            <p class="ets_cfu_help_block">Text</p>
                        </div>
                        <div class="ets_cfu_btn_input">
                            <span class="settings_icon" title="{l s='Setting' mod='ets_cfultimate'}"><i class="fa fa-cog"></i></span>
                            <div class="settings_icon_content">
                                <span class="ets_cfu_btn_edit_input" title="{l s='Edit' mod='ets_cfultimate'}"><i class="fa fa-pencil-square-o"></i>&nbsp;{l s='Edit' mod='ets_cfultimate'}</span>
                                <span class="ets_cfu_btn_copy_input" title="{l s='Duplicate' mod='ets_cfultimate'}"><i class="fa fa-files-o"></i>&nbsp;{l s='Duplicate' mod='ets_cfultimate'}</span>
                                <span class="ets_cfu_btn_delete_input" title="{l s='Delete' mod='ets_cfultimate'}"><i class="fa fa-trash-o"></i>&nbsp;{l s='Delete' mod='ets_cfultimate'}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ets_cfu_box style3" data-type="style3" data-order="0">
        <div class="ets_cfu_btn">
            <span class="ets_cfu_btn_edit"
                  title="{l s='Edit' mod='ets_cfultimate'}">{l s='Edit' mod='ets_cfultimate'}</span>
            <span class="ets_cfu_btn_copy"
                  title="{l s='Duplicate' mod='ets_cfultimate'}">{l s='Duplicate' mod='ets_cfultimate'}</span>
            <span class="ets_cfu_btn_delete"
                  title="{l s='Delete' mod='ets_cfultimate'}">{l s='Delete' mod='ets_cfultimate'}</span>
            <span class="ets_cfu_btn_drag_drop"
                  title="{l s='Drag drop' mod='ets_cfultimate'}">{l s='Drag drop' mod='ets_cfultimate'}</span>
        </div>
        <div class="ets_cfu_row">
            <div class="ets_cfu_col col1" data-col="col1">
                <div class="ets_cfu_col_box ui-sortable">
                    <span class="ets_cfu_add_input"><i class="icon-plus-circle"></i>&nbsp;{l s='Add input field' mod='ets_cfultimate'}</span>
                    <div class="ets_cfu_input_email ets_cfu_input" data-type="email" data-required="1"
                         data-name="email-668" data-placeholder="" data-id="" data-class="" data-mailtag="1">
                        <div class="ets_cfu_input_generator">
                            <img src="{$icon_link|escape:'html':'utf-8'}ip_email.png"
                                 alt="{l s='Input email' mod='ets_cfultimate'}">
                            <div class="ets_cfu_form_data">
                                {if $is_multi_lang}{foreach from=$languages item='lang'}
                                    <span class="ets_cfu_label_{$lang.id_lang|intval}"{if $lang.id_lang != $id_lang_default} style="display:none;"{/if}>Email</span>
                                <span class="ets_cfu_short_code_{$lang.id_lang|intval}" style="display:none;">
                                        Email*[email* email-668 default:user_email]</span>{/foreach}
                                    <span class="ets_cfu_values_{$lang.id_lang|intval}" style="display:none;"></span>
                                    <span class="ets_cfu_desc_{$lang.id_lang|intval}" style="display:none;"></span>
                                {else}
                                    <span class="ets_cfu_label_{$id_lang_default|intval}">Email</span>
                                    <span class="ets_cfu_short_code_{$id_lang_default|intval}" style="display:none;">Email*[email* email-668 default:user_email]</span>
                                    <span class="ets_cfu_values_{$id_lang_default|intval}" style="display:none;"></span>
                                    <span class="ets_cfu_desc_{$id_lang_default|intval}" style="display:none;"></span>
                                {/if}
                            </div>
                            <p class="ets_cfu_help_block">{l s='Email' mod='ets_cfultimate'}</p>
                        </div>
                        <div class="ets_cfu_btn_input">
                            <span class="settings_icon" title="{l s='Setting' mod='ets_cfultimate'}"><i class="fa fa-cog"></i></span>
                            <div class="settings_icon_content">
                                <span class="ets_cfu_btn_edit_input" title="{l s='Edit' mod='ets_cfultimate'}"><i class="fa fa-pencil-square-o"></i>&nbsp;{l s='Edit' mod='ets_cfultimate'}</span>
                                <span class="ets_cfu_btn_copy_input" title="{l s='Duplicate' mod='ets_cfultimate'}"><i class="fa fa-files-o"></i>&nbsp;{l s='Duplicate' mod='ets_cfultimate'}</span>
                                <span class="ets_cfu_btn_delete_input" title="{l s='Delete' mod='ets_cfultimate'}"><i class="fa fa-trash-o"></i>&nbsp;{l s='Delete' mod='ets_cfultimate'}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ets_cfu_col col2" data-col="col2">
                <div class="ets_cfu_col_box ui-sortable">
                    <span class="ets_cfu_add_input"><i class="icon-plus-circle"></i>&nbsp;{l s='Add input field' mod='ets_cfultimate'}</span>
                    <div class="ets_cfu_input_tel ets_cfu_input" data-type="tel" data-required="0" data-name="tel-534"
                         data-placeholder="" data-id="" data-class="">
                        <div class="ets_cfu_input_generator">
                            <img src="{$icon_link|escape:'html':'utf-8'}ip_tel.png"
                                 alt="{l s='Input telephone' mod='ets_cfultimate'}">
                            <div class="ets_cfu_form_data">
                                {if $is_multi_lang}{foreach from=$languages item='lang'}
                                    <span class="ets_cfu_label_{$lang.id_lang|intval}"{if $lang.id_lang != $id_lang_default} style="display:none;"{/if}>Phone</span>
                                    <span class="ets_cfu_short_code_{$lang.id_lang|intval}" style="display:none;">Phone[tel tel-534]</span>
                                    <span class="ets_cfu_values_{$lang.id_lang|intval}" style="display:none;"></span>
                                    <span class="ets_cfu_desc_{$lang.id_lang|intval}" style="display:none;"></span>
                                {/foreach}
                                {else}
                                    <span class="ets_cfu_label_{$id_lang_default|intval}">Phone</span>
                                    <span class="ets_cfu_short_code_{$id_lang_default|intval}" style="display:none;">Phone[tel tel-534]</span>
                                    <span class="ets_cfu_values_{$id_lang_default|intval}" style="display:none;"></span>
                                    <span class="ets_cfu_desc_{$id_lang_default|intval}" style="display:none;"></span>
                                {/if}
                            </div>
                            <p class="ets_cfu_help_block">{l s='Telephone' mod='ets_cfultimate'}</p>
                        </div>
                        <div class="ets_cfu_btn_input">
                            <span class="settings_icon" title="{l s='Setting' mod='ets_cfultimate'}"><i class="fa fa-cog"></i></span>
                            <div class="settings_icon_content">
                                <span class="ets_cfu_btn_edit_input" title="{l s='Edit' mod='ets_cfultimate'}"><i class="fa fa-pencil-square-o"></i>&nbsp;{l s='Edit' mod='ets_cfultimate'}</span>
                                <span class="ets_cfu_btn_copy_input" title="{l s='Duplicate' mod='ets_cfultimate'}"><i class="fa fa-files-o"></i>&nbsp;{l s='Duplicate' mod='ets_cfultimate'}</span>
                                <span class="ets_cfu_btn_delete_input" title="{l s='Delete' mod='ets_cfultimate'}"><i class="fa fa-trash-o"></i>&nbsp;{l s='Delete' mod='ets_cfultimate'}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ets_cfu_box style1" data-type="style1" data-order="0">
        <div class="ets_cfu_btn">
            <span class="ets_cfu_btn_edit"
                  title="{l s='Edit' mod='ets_cfultimate'}">{l s='Edit' mod='ets_cfultimate'}</span>
            <span class="ets_cfu_btn_copy"
                  title="{l s='Duplicate' mod='ets_cfultimate'}">{l s='Duplicate' mod='ets_cfultimate'}</span>
            <span class="ets_cfu_btn_delete"
                  title="{l s='Delete' mod='ets_cfultimate'}">{l s='Delete' mod='ets_cfultimate'}</span>
            <span class="ets_cfu_btn_drag_drop"
                  title="{l s='Drag drop' mod='ets_cfultimate'}">{l s='Drag drop' mod='ets_cfultimate'}</span>
        </div>
        <div class="ets_cfu_row">
            <div class="ets_cfu_col col1" data-col="col1">
                <div class="ets_cfu_col_box ui-sortable">
                    <span class="ets_cfu_add_input"><i
                                class="icon-plus-circle"></i>&nbsp;{l s='Add input field' mod='ets_cfultimate'}</span>
                    <div class="ets_cfu_input_textarea ets_cfu_input" data-type="textarea" data-required="1"
                         data-name="textarea-261" data-placeholder="" data-id="" data-class="">
                        <div class="ets_cfu_input_generator">
                            <img src="{$icon_link|escape:'html':'utf-8'}ip_textarea.png"
                                 alt="{l s='Textarea' mod='ets_cfultimate'}">
                            <div class="ets_cfu_form_data">
                                {if $is_multi_lang}{foreach from=$languages item='lang'}
                                    <span class="ets_cfu_label_{$lang.id_lang|intval}"{if $lang.id_lang != $id_lang_default} style="display:none;"{/if}>Message</span>
                                    <span class="ets_cfu_short_code_{$lang.id_lang|intval}" style="display:none;">Message*[textarea* textarea-261]</span>
                                    <span class="ets_cfu_values_{$lang.id_lang|intval}" style="display:none;"></span>
                                    <span class="ets_cfu_desc_{$lang.id_lang|intval}" style="display:none;"></span>
                                {/foreach}
                                {else}
                                    <span class="ets_cfu_label_{$id_lang_default|intval}">Message</span>
                                    <span class="ets_cfu_short_code_{$id_lang_default|intval}" style="display:none;">Message*[textarea* textarea-261]</span>
                                    <span class="ets_cfu_values_{$id_lang_default|intval}" style="display:none;"></span>
                                    <span class="ets_cfu_desc_{$id_lang_default|intval}" style="display:none;"></span>
                                {/if}
                            </div>
                            <p class="ets_cfu_help_block">{l s='Textarea' mod='ets_cfultimate'}</p>
                        </div>
                        <div class="ets_cfu_btn_input">
                            <span class="settings_icon" title="{l s='Setting' mod='ets_cfultimate'}"><i class="fa fa-cog"></i></span>
                            <div class="settings_icon_content">
                                <span class="ets_cfu_btn_edit_input" title="{l s='Edit' mod='ets_cfultimate'}"><i class="fa fa-pencil-square-o"></i>&nbsp;{l s='Edit' mod='ets_cfultimate'}</span>
                                <span class="ets_cfu_btn_copy_input" title="{l s='Duplicate' mod='ets_cfultimate'}"><i class="fa fa-files-o"></i>&nbsp;{l s='Duplicate' mod='ets_cfultimate'}</span>
                                <span class="ets_cfu_btn_delete_input" title="{l s='Delete' mod='ets_cfultimate'}"><i class="fa fa-trash-o"></i>&nbsp;{l s='Delete' mod='ets_cfultimate'}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="ets_cfu_title_box">{l s='1 column in a row' mod='ets_cfultimate'}</span>
    </div>
    <div class="ets_cfu_box style1" data-type="style1" data-order="0">
        <div class="ets_cfu_btn">
            <span class="ets_cfu_btn_edit"
                  title="{l s='Edit' mod='ets_cfultimate'}">{l s='Edit' mod='ets_cfultimate'}</span>
            <span class="ets_cfu_btn_copy"
                  title="{l s='Duplicate' mod='ets_cfultimate'}">{l s='Duplicate' mod='ets_cfultimate'}</span>
            <span class="ets_cfu_btn_delete"
                  title="{l s='Delete' mod='ets_cfultimate'}">{l s='Delete' mod='ets_cfultimate'}</span>
            <span class="ets_cfu_btn_drag_drop"
                  title="{l s='Drag drop' mod='ets_cfultimate'}">{l s='Drag drop' mod='ets_cfultimate'}</span>
        </div>
        <div class="ets_cfu_row">
            <div class="ets_cfu_col col1" data-col="col1">
                <div class="ets_cfu_col_box ui-sortable">
                    <span class="ets_cfu_add_input"><i class="icon-plus-circle"></i>&nbsp;{l s='Add input field' mod='ets_cfultimate'}</span>
                    <div class="ets_cfu_input_submit ets_cfu_input" data-type="submit" data-required="0"
                         data-name="submit-992" data-placeholder="" data-id="" data-class="">
                        <div class="ets_cfu_input_generator">
                            <img src="{$icon_link|escape:'html':'utf-8'}ip_submit.png"
                                 alt="{l s='Submit' mod='ets_cfultimate'}">
                            <div class="ets_cfu_form_data">
                                {if $is_multi_lang}{foreach from=$languages item='lang'}
                                    <span class="ets_cfu_values_{$lang.id_lang|intval}"{if $lang.id_lang != $id_lang_default} style="display:none;"{/if}>Send</span>
                                    <span class="ets_cfu_short_code_{$lang.id_lang|intval}" style="display:none;">[submit submit-992 "Send"]</span>
                                    <span class="ets_cfu_values_{$lang.id_lang|intval}" style="display:none;"></span>
                                    <span class="ets_cfu_desc_{$lang.id_lang|intval}" style="display:none;"></span>
                                {/foreach}
                                {else}
                                    <span class="ets_cfu_values_{$id_lang_default|intval}">Send</span>
                                    <span class="ets_cfu_short_code_{$id_lang_default|intval}" style="display:none;">[submit submit-992 "Send"]</span>
                                    <span class="ets_cfu_values_{$id_lang_default|intval}" style="display:none;"></span>
                                    <span class="ets_cfu_desc_{$id_lang_default|intval}" style="display:none;"></span>
                                {/if}
                            </div>
                            <p class="ets_cfu_help_block">{l s='Submit' mod='ets_cfultimate'}</p>
                        </div>
                        <div class="ets_cfu_btn_input">
                            <span class="settings_icon" title="{l s='Setting' mod='ets_cfultimate'}"><i class="fa fa-cog"></i></span>
                            <div class="settings_icon_content">
                                <span class="ets_cfu_btn_edit_input" title="{l s='Edit' mod='ets_cfultimate'}"><i class="fa fa-pencil-square-o"></i>&nbsp;{l s='Edit' mod='ets_cfultimate'}</span>
                                <span class="ets_cfu_btn_copy_input" title="{l s='Duplicate' mod='ets_cfultimate'}"><i class="fa fa-files-o"></i>&nbsp;{l s='Duplicate' mod='ets_cfultimate'}</span>
                                <span class="ets_cfu_btn_delete_input" title="{l s='Delete' mod='ets_cfultimate'}"><i class="fa fa-trash-o"></i>&nbsp;{l s='Delete' mod='ets_cfultimate'}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="ets_cfu_title_box">{l s='1 column in a row' mod='ets_cfultimate'}</span>
    </div>
{elseif $type == 'sc'}
    <div class="ets_cfu_wrapper">
        <div class="ets_cfu_box style3">
            <div class="ets_cfu_col col1">
                <div class="ets_cfu_input_text ets_cfu_input">
                    <label>First Name*[text* text-652 default:user_full_name]</label></div>
            </div>
            <div class="ets_cfu_col col2">
                <div class="ets_cfu_input_text ets_cfu_input">
                    <label>Last Name*[text* text-85 default:user_last_name]</label>
                </div>
            </div>
        </div>
        <div class="ets_cfu_box style3">
            <div class="ets_cfu_col col1">
                <div class="ets_cfu_input_email ets_cfu_input"><label>Email*[email* email-668
                        default:user_email]</label></div>
            </div>
            <div class="ets_cfu_col col2">
                <div class="ets_cfu_input_tel ets_cfu_input"><label>Phone[tel tel-534]</label></div>
            </div>
        </div>
        <div class="ets_cfu_box style1">
            <div class="ets_cfu_col col1">
                <div class="ets_cfu_input_textarea ets_cfu_input"><label>Message*[textarea* textarea-261]</label></div>
            </div>
        </div>
        <div class="ets_cfu_box style1">
            <div class="ets_cfu_col col1">
                <div class="ets_cfu_input_submit ets_cfu_input">[submit submit-992 "Send"]</div>
            </div>
        </div>
    </div>
{elseif $type == 'msg'}
    <p>Full name: [text-652] [text-85]</p>
    <p>Email: [email-668]</p>
    <p>Phone number: [tel-534]</p>
    <p>Message: [textarea-261]</p>
{/if}