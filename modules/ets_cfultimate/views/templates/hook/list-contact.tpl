{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
{if $ETS_CFU_ENABLE_TMCE}
    <script type="text/javascript">
        var ad = '';
        var iso = 'en';
        var file_not_found = '';
        var pathCSS = '{$smarty.const._THEME_CSS_DIR_ nofilter}';
    </script>
    <script src="{$_PS_JS_DIR_|escape:'html':'UTF-8'}tiny_mce/tiny_mce.js"></script>
    {if $is_ps15}
        <script src="{$_PS_JS_DIR_|escape:'html':'UTF-8'}tinymce.inc.js"></script>
    {else}
        <script src="{$_PS_JS_DIR_|escape:'html':'UTF-8'}admin/tinymce.inc.js"></script>
    {/if}
{/if}
{if isset($okimport)&& $okimport}
    <div class="bootstrap">
        <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            {l s='Contact form imported successfully.' mod='ets_cfultimate'}
        </div>
    </div>
{/if}
{hook h='contactFormUltimateTopBlock'}
<script type="text/javascript">
    var text_update_position = '{l s='Successful update' mod='ets_cfultimate'}';
    {if isset($is_ps15) && $is_ps15}
    $(document).on('click', '.dropdown-toggle', function () {
        $(this).closest('.btn-group').toggleClass('open');
    });
    {/if}
</script>
<div class="cfu-content-block">
    <form id="form-contact" class="form-horizontal clearfix products-catalog"
          action="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}" method="post">
        <input id="submitFilterContact" type="hidden" value="0" name="submitFilterContact"/>
        <input type="hidden" value="1" name="page"/>
        <input type="hidden" value="50" name="selected_pagination"/>
        <div class="panel col-lg-12">
            <div class="panel-heading">
                {l s='Contact forms' mod='ets_cfultimate'}
                <span class="badge">{count($ets_cfu_contacts)|intval}</span>
                <span class="panel-heading-action">
                    <a id="desc-contactform-new" class="list-toolbar-btn" title="{l s='Add new' mod='ets_cfultimate'}"
                       href="{$url_module|escape:'html':'UTF-8'}&etsCfuAddContact=1">
                        <span title="{l s='Add new' mod='ets_cfultimate'}" class="label-tooltip" data-placement="top"
                              data-html="true" data-original-title="{l s='Add new' mod='ets_cfultimate'}"
                              data-toggle="tooltip" title="">
                            <i class="process-icon-new"></i>
                        </span>
                    </a>
                 </span>
            </div>
            <div class="table-responsive-row clearfix">
                <table id="table-contact" class="table contact">
                    <thead>
                    <tr class="nodrag nodrop">
                        <th class="fixed-width-xs text-center ctf_id">
                                <span class="title_box">
                                    {l s='ID' mod='ets_cfultimate'}
                                    <a {if $sort=='id_contact' && $sort_type=='desc'} class="active"{/if} href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}&sort=id_contact&sort_type=desc{$filter_params nofilter}"><i
                                                class="icon-caret-down"></i></a>
                                    <a {if $sort=='id_contact' && $sort_type=='asc'} class="active"{/if} href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}&sort=id_contact&sort_type=asc{$filter_params nofilter}"><i
                                                class="icon-caret-up"></i></a>
                                </span>
                        </th>
                        <th class="ctf_title">
                                <span class="title_box">
                                    {l s='Title' mod='ets_cfultimate'}
                                    <a {if $sort=='title' && $sort_type=='desc'} class="active"{/if} href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}&sort=title&sort_type=desc{$filter_params nofilter}"><i
                                                class="icon-caret-down"></i></a>
                                    <a {if $sort=='title' && $sort_type=='asc'} class="active"{/if} href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}&sort=title&sort_type=asc{$filter_params nofilter}"><i
                                                class="icon-caret-up"></i></a>
                                </span>
                        </th>
                        {if !isset($showShortcodeHook) || (isset($showShortcodeHook)  && $showShortcodeHook)}
                            <th class="ctf_shortcode">
                                    <span class="title_box">
                                        {l s='Short code' mod='ets_cfultimate'}
                                    </span>
                            </th>
                        {/if}
                        <th class="ct_form_url">
                                <span class="title_box">
                                    {l s='Form URL' mod='ets_cfultimate'}
                                </span>
                        </th>
                        <th class="ct_form_views">
                                <span class="title_box">
                                    {l s='Views' mod='ets_cfultimate'}
                                </span>
                        </th>
                        <th class="ctf_sort">
                                <span class="title_box">
                                    {l s='Sort order' mod='ets_cfultimate'}
                                    <a {if $sort=='position' && $sort_type=='desc'} class="active"{/if} href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}&sort=position&sort_type=desc{$filter_params nofilter}"><i
                                                class="icon-caret-down"></i></a>
                                    <a {if $sort=='position' && $sort_type=='asc'} class="active"{/if} href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}&sort=position&sort_type=asc{$filter_params nofilter}"><i
                                                class="icon-caret-up"></i></a>
                                </span>
                        </th>
                        <th class="ctf_message">
                                <span class="title_box">
                                    {l s='Save message' mod='ets_cfultimate'}
                                    <a {if $sort=='save_message' && $sort_type=='asc'} class="active"{/if} href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}&sort=save_message&sort_type=desc{$filter_params nofilter}"><i
                                                class="icon-caret-down"></i></a>
                                    <a {if $sort=='save_message' && $sort_type=='desc'} class="active"{/if} href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}&sort=save_message&sort_type=asc{$filter_params nofilter}"><i
                                                class="icon-caret-up"></i></a>
                                </span>
                        </th>
                        <th class="ctf_active">
                                <span class="title_box">
                                    {l s='Active' mod='ets_cfultimate'}
                                    <a {if $sort=='active' && $sort_type=='desc'} class="active"{/if} href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}&sort=active&sort_type=desc{$filter_params nofilter}"><i
                                                class="icon-caret-down"></i></a>
                                    <a {if $sort=='active' && $sort_type=='asc'} class="active"{/if} href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}&sort=active&sort_type=asc{$filter_params nofilter}"><i
                                                class="icon-caret-up"></i></a>
                                </span>
                        </th>
                        <th class="ctf_action">
                                <span class="title_box">
                                    {l s='Action' mod='ets_cfultimate'}
                                </span>
                        </th>
                    </tr>
                    <tr class="nodrag nodrop">
                        <th class="fixed-width-xs text-center ctf_id">
                                <span class="title_box">
                                    <input class="form-control" name="id_contact" style="width:25px"
                                           value="{if isset($values_submit.id_contact)}{$values_submit.id_contact|escape:'html':'UTF-8'}{/if}"/>
                                </span>
                        </th>
                        <th class="ctf_title">
                                <span class="title_box">
                                    <input class="form-control" name="contact_title" style="width:150px"
                                           value="{if isset($values_submit.contact_title)}{$values_submit.contact_title|escape:'html':'UTF-8'}{/if}"/>
                                </span>
                        </th>
                        {if !isset($showShortcodeHook) || (isset($showShortcodeHook)  && $showShortcodeHook)}
                            <th class="">

                            </th>
                        {/if}
                        <th class="ct_form_url">
                        </th>
                        <th class="">

                        </th>
                        <th>
                        </th>
                        <th class="ctf_message">
                                <span class="title_box">
                                    <select class="form-control" name="save_message">
                                        <option value="">---</option>
                                        <option value="1" {if isset($values_submit.save_message) && $values_submit.hook==1} selected="selected"{/if}>{l s='Yes' mod='ets_cfultimate'}</option>
                                        <option value="0" {if isset($values_submit.save_message) && $values_submit.hook==0} selected="selected"{/if}>{l s='No' mod='ets_cfultimate'}</option>
                                    </select>
                                </span>
                        </th>
                        <th class="ctf_active">
                            <select class="form-control" name="active_contact">
                                <option value="">---</option>
                                <option value="1" {if isset($values_submit.active_contact) && $values_submit.active_contact==1} selected="selected"{/if}>{l s='Yes' mod='ets_cfultimate'}</option>
                                <option value="0" {if isset($values_submit.active_contact) && $values_submit.active_contact==0} selected="selected"{/if}>{l s='No' mod='ets_cfultimate'}</option>
                            </select>
                        </th>
                        <th class="ctf_action">
                                <span class="pull-right">
                                    <button id="submitFilterButtonContact" class="btn btn-default"
                                            name="submitFilterButtonContact" type="submit">
                                    <i class="icon-search"></i>
                                        {l s='Search' mod='ets_cfultimate'}
                                    </button>
                                    {if isset($filter)&& $filter}
                                        <a class="btn btn-warning"
                                           href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}">
                                            <i class="icon-eraser"></i>
                                            {l s='Reset' mod='ets_cfultimate'}
                                        </a>
                                    {/if}
                                </span>
                        </th>
                    </tr>
                    </thead>
                    <tbody id="list-contactform">

                    {if $ets_cfu_contacts}
                        {foreach from=$ets_cfu_contacts item='contact'}
                            <tr id="formcontact_{$contact.id_contact|intval}">
                                <td class="ctf_id">{$contact.id_contact|intval}</td>
                                <td class="ctf_title">{$contact.title|escape:'html':'UTF-8'}</td>
                                {if !isset($showShortcodeHook) || (isset($showShortcodeHook)  && $showShortcodeHook)}
                                    <td class="ctf_shortcode">
                                        <div class="short-code">
                                            <input title="{l s='Click to copy' mod='ets_cfultimate'}" class="ctf-short-code"
                                                   type="text" value='[contact-form-7 id="{$contact.id_contact|intval}"]'/>
                                            <span class="text-copy">{l s='Copied' mod='ets_cfultimate'}</span>
                                        </div>
                                    </td>
                                {/if}
                                <td class="ct_form_url">
                                    {if $contact.enable_form_page}
                                        <a href="{$contact.link|escape:'html':'UTF-8'}"
                                           target="_blank">{$contact.link|escape:'html':'UTF-8'}</a>
                                    {else}
                                        {l s='Form page is disabled' mod='ets_cfultimate'}
                                    {/if}
                                </td>
                                <td class="ct_view text-center">
                                    {$contact.count_views|intval}
                                    {*<a target="_blank" href="{$link->getAdminLink('AdminContactFormUltimateStatistics')|escape:'html':'UTF-8'}&id_contact={$contact.id_contact|intval}">
                                        &nbsp;<i class="icon icon-line-chart"></i>
                                    </a>*}
                                </td>
                                <td class="ctf_sort text-center {if $sort=='position' && $sort_type=='asc'}pointer dragHandle center{/if}">
                                    <div class="dragGroup">
                                        <span class="positions">{($contact.position+1)|intval}</span>
                                    </div>
                                </td>
                                <td class="text-center ctf_message">
                                    {if $contact.save_message}
                                        <a title="{l s='Click to disable' mod='ets_cfultimate'}"
                                           href="{$url_module|escape:'html':'UTF-8'}&etsCfuSaveMessageUpdate=0&id_contact={$contact.id_contact|intval}">
                                            <i class="material-icons action-enabled">check</i>
                                        </a>
                                        {if $contact.count_message}
                                            <a title="{l s='View messages' mod='ets_cfultimate'}"
                                               href="{$link->getAdminLink('AdminContactFormUltimateMessage')|escape:'html':'UTF-8'}&id_contact={$contact.id_contact|intval}"
                                               class="">
                                                ({$contact.count_message|intval})
                                            </a>
                                        {/if}
                                    {else}
                                        <a title="{l s='Click to enable' mod='ets_cfultimate'}"
                                           href="{$url_module|escape:'html':'UTF-8'}&etsCfuSaveMessageUpdate=1&id_contact={$contact.id_contact|intval}">
                                            <i class="material-icons action-disabled">{l s='clear' mod='ets_cfultimate'}</i>
                                        </a>
                                        {if $contact.count_message}
                                            <a href="{$link->getAdminLink('AdminContactFormUltimateMessage')|escape:'html':'UTF-8'}&id_contact={$contact.id_contact|intval}"
                                               class="">
                                                ({$contact.count_message|intval})
                                            </a>
                                        {/if}
                                    {/if}
                                </td>
                                <td class="text-center ctf_active">
                                    {if $contact.active}
                                        <a title="{l s='Click to disable' mod='ets_cfultimate'}"
                                        href="{$url_module|escape:'html':'UTF-8'}&etsCfuActiveUpdate=0&id_contact={$contact.id_contact|intval}">
                                            <i class="material-icons action-enabled">check</i>
                                        </a>
                                    {else}
                                        <a title="{l s='Click to enable' mod='ets_cfultimate'}"
                                        href="{$url_module|escape:'html':'UTF-8'}&etsCfuActiveUpdate=1&id_contact={$contact.id_contact|intval}">
                                            <i class="material-icons action-disabled">{l s='clear' mod='ets_cfultimate'}</i>
                                        </a>
                                    {/if}
                                </td>
                                <td class="text-center ctf_action">
                                    <div class="btn-group-action">
                                        <div class="btn-group">
                                            <a class="btn tooltip-link product-edit" title=""
                                               href="{$url_module|escape:'html':'UTF-8'}&etsCfuEditContact=1&id_contact={$contact.id_contact|intval}"
                                               title="{l s='Edit' mod='ets_cfultimate'}">
                                                <i class="material-icons">mode_edit</i>{l s='Edit' mod='ets_cfultimate'}
                                            </a>
                                            <a class="btn btn-link dropdown-toggle dropdown-toggle-split product-edit"
                                               aria-expanded="false" aria-haspopup="true" data-toggle="dropdown"> <i
                                                        class="icon-caret-down"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right"
                                                 style="position: absolute; transform: translate3d(-164px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                {if $contact.enable_form_page}
                                                    <a href="{Ets_CfUltimate::getLinkContactForm($contact.id_contact|intval)|escape:'html':'UTF-8'}"
                                                       class="dropdown-item product-edit" target="_blank"
                                                       >
                                                        <i class="material-icons">remove_red_eye</i>
                                                        {l s='View form' mod='ets_cfultimate'}
                                                    </a>
                                                {/if}
                                                <a href="{$link->getAdminLink('AdminContactFormUltimateMessage')|escape:'html':'UTF-8'}&id_contact={$contact.id_contact|intval}"
                                                   class="dropdown-item product-edit"
                                                   >
                                                    <i class="icon icon-comments fa fa-comments"></i>
                                                    {l s='Messages' mod='ets_cfultimate'}
                                                </a>
                                                <a href="{$link->getAdminLink('AdminContactFormUltimateStatistics')|escape:'html':'UTF-8'}&id_contact={$contact.id_contact|intval}"
                                                   class="dropdown-item product-edit"
                                                   >
                                                    <i class="icon icon-line-chart"></i>
                                                    {l s='Statistics' mod='ets_cfultimate'}
                                                </a>
                                                <a href="{$url_module|escape:'html':'UTF-8'}&etsCfuDuplicateContact=1&id_contact={$contact.id_contact|intval}"
                                                   class="dropdown-item message-duplidate product-edit"
                                                   >
                                                    <i class="icon icon-copy"></i>
                                                    {l s='Duplicate' mod='ets_cfultimate'}
                                                </a>
                                                <a href="{$url_module|escape:'html':'UTF-8'}&etsCfuDeleteContact=1&id_contact={$contact.id_contact|intval}"
                                                   class="dropdown-item message-delete product-edit"
                                                   >
                                                    <i class="material-icons">delete</i>
                                                    {l s='Delete form' mod='ets_cfultimate'}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        {/foreach}
                    {else}
                        <tr>
                            <td colspan="10">
                                <p class="alert alert-warning">{l s='No contact forms available' mod='ets_cfultimate'}</p>
                            </td>
                        </tr>
                    {/if}
                    </tbody>
                </table>
                {$ets_cfu_pagination_text nofilter}
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($("table .datepicker").length > 0) {
                $("table .datepicker").datepicker({
                    prevText: '',
                    nextText: '',
                    dateFormat: 'yy-mm-dd'
                });
            }
        });
    </script>
</div>
<div class="ctf-popup-wapper-admin">
    <div class="fuc"></div>
    <div class="ctf-popup-tablecell">
        <div class="ctf-popup-content">
            <div class="ctf_close_popup">{l s='close' mod='ets_cfultimate'}</div>
            <div id="form-contact-preview">

            </div>
        </div>
    </div>
</div>