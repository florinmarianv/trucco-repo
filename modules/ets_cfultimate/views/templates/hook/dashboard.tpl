{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
<script type="text/javascript">
    var ets_cfu_line_chart = '{$ets_cfu_line_chart|json_encode}';
    var ets_cfu_lc_labels = '{$ets_cfu_lc_labels|json_encode}';
    var ets_cfu_lc_title = "{l s='Statistic' mod='ets_cfultimate'}";
    var ets_cfu_y_max = {$y_max_value|intval};
    var text_add_to_black_list = "{l s='Add IP address to blacklist successful' js='1' mod='ets_cfultimate' }";
    var detele_log = "{l s='If you clear \"View log\", view chart will be reset. Do you want to do that?' js='1' mod='ets_cfultimate' }";
</script>
<script type="text/javascript" src="{$ets_cfu_js_dir_path|escape:'quotes':'UTF-8'}chart.js"></script>
<script type="text/javascript" src="{$ets_cfu_js_dir_path|escape:'quotes':'UTF-8'}common.js"></script>
<script type="text/javascript" src="{$ets_cfu_js_dir_path|escape:'quotes':'UTF-8'}dashboard.js"></script>
{hook h='contactFormUltimateTopBlock'}
<div class="cfu-content-block">
    <div class="row manage_form_items">
        <div class="col-xs-12 col-md-6 col-sm-12">
            <div class="panel ets-cfu-panel">
                <div class="panel-header">
                    <p class="panel-title"><i class="fa fa-th"></i> {l s='Contact Management' mod='ets_cfultimate'}</p>
                </div>
                <div class="panel-content mt-15">
                    <div class="row">
                        <div class="manage_item col-xs-4 col-sm-6 col-md-3">
                            <a href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}">
                                <div class="ets-cfu-box">
                                    <div class="ets-cfu-box-content">
                                        <img src="{$ets_cfu_img_dir_path|escape:'html':'utf-8'}i_contactform.png" /><br />
                                        <h4 class="title">{l s='Contact Forms' mod='ets_cfultimate'}</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="manage_item col-xs-4 col-sm-6 col-md-3">
                            <a href="{$link->getAdminLink('AdminContactFormUltimateMessage',true)|escape:'html':'UTF-8'}">
                                <div class="ets-cfu-box">
                                    <div class="ets-cfu-box-content">
                                        <img src="{$ets_cfu_img_dir_path|escape:'html':'utf-8'}i_message.png" />{if $ets_cfu_total_message}<span class="ets-cfu-messages">{$ets_cfu_total_message|intval}</span>{/if}<br />
                                        <h4 class="title">{l s='Messages' mod='ets_cfultimate'}</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="manage_item col-xs-4 col-sm-6 col-md-3">
                            <a href="{$link->getAdminLink('AdminContactFormUltimateEmail',true)|escape:'html':'UTF-8'}">
                                <div class="ets-cfu-box">
                                    <div class="ets-cfu-box-content">
                                        <img src="{$ets_cfu_img_dir_path|escape:'html':'utf-8'}i_temp.png" /><br />
                                        <h4 class="title">{l s='Email Templates' mod='ets_cfultimate'}</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="manage_item col-xs-4 col-sm-6 col-md-3">
                            <a href="{$link->getAdminLink('AdminContactFormUltimateImportExport',true)|escape:'html':'UTF-8'}">
                                <div class="ets-cfu-box">
                                    <div class="ets-cfu-box-content">
                                        <img src="{$ets_cfu_img_dir_path|escape:'html':'utf-8'}i_backup.png" /><br />
                                        <h4 class="title">{l s='Import/Export' mod='ets_cfultimate'}</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="manage_item col-xs-4 col-sm-6 col-md-3">
                            <a href="{$link->getAdminLink('AdminContactFormUltimateIntegration',true)|escape:'html':'UTF-8'}">
                                <div class="ets-cfu-box">
                                    <div class="ets-cfu-box-content">
                                        <img src="{$ets_cfu_img_dir_path|escape:'html':'utf-8'}i_integration.png" />
                                        <h4 class="title">{l s='Integration' mod='ets_cfultimate'}</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="manage_item col-xs-4 col-sm-6 col-md-3">
                            <a href="{$link->getAdminLink('AdminContactFormUltimateStatistics',true)|escape:'html':'UTF-8'}">
                                <div class="ets-cfu-box">
                                    <div class="ets-cfu-box-content">
                                        <img src="{$ets_cfu_img_dir_path|escape:'html':'utf-8'}i_statistics.png" />
                                        <h4 class="title">{l s='Statistics' mod='ets_cfultimate'}</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="manage_item col-xs-4 col-sm-6 col-md-3">
                            <a href="{$link->getAdminLink('AdminContactFormUltimateIpBlacklist',true)|escape:'html':'UTF-8'}">
                                <div class="ets-cfu-box">
                                    <div class="ets-cfu-box-content">
                                        <img src="{$ets_cfu_img_dir_path|escape:'html':'utf-8'}i_blacklist.png" />
                                        <h4 class="title">{l s='IP & Email blacklist' mod='ets_cfultimate'}</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="manage_item col-xs-4 col-sm-6 col-md-3">
                            <a href="{$link->getAdminLink('AdminContactFormUltimateHelp',true)|escape:'html':'UTF-8'}">
                                <div class="ets-cfu-box">
                                    <div class="ets-cfu-box-content">
                                        <img src="{$ets_cfu_img_dir_path|escape:'html':'utf-8'}i_help.png" />
                                        <h4 class="title">{l s='Help' mod='ets_cfultimate'}</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-sm-12">
            <div class="panel ets-cfu-panel">
                <div class="panel-header">
                    <span class="panel-title"><i class="fa fa-bar-chart"></i> {l s='Contact Traffic' mod='ets_cfultimate'}</span>
                    {if isset($filters) && $filters}<ul class="ets_cfu_filter">
                        {foreach from=$filters item = 'filter'}
                            <li class="ets_cfu_item_filter item{$filter.id|escape:'html':'utf-8'}{if $filter_active == $filter.id} active{/if}">
                                <a href="{$action|cat:'&filter='|cat:$filter.id|escape:'html':'utf-8'}" title="{$filter.label|escape:'html':'utf-8'}">{$filter.label|escape:'html':'utf-8'}</a>
                            </li>
                        {/foreach}
                    </ul>{/if}
                </div>
                <div class="panel-content dashboar_traffix">
                    <div class="row">
                        <div class="col-md-7 col-lg-8">
                            <div class="ets_cfu_admin_chart">
                                <div class="ets_cfu_line_chart">
                                    <h4 class="ets_cfu_title_chart">{l s='Statistics' mod='ets_cfultimate'}</h4>
                                    <canvas id="ets_cfu_line_chart" style="width:100%; height: 300px;"></canvas>
                                    <a class="ets_cfu_statistics_link" href="{$ets_cfu_link->getAdminLink('AdminContactFormUltimateStatistics',true)|escape:'html':'UTF-8'}">{l s='View more' mod='ets_cfultimate'}</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-lg-4">
                            <div class="ets_cfu_dashboard-logs">
                                <h5>{l s='Last visits' mod='ets_cfultimate'}</h5>
                                {if $ets_cfu_logs}
                                    <ul>{foreach from=$ets_cfu_logs item=log}
                                        <li>
                                            <span class="browser-icon {$log.class|escape:'html':'UTF-8'}"></span> {$log.browser|escape:'html':'UTF-8'}<br>
                                            <a class="log_ip" href="https://www.infobyip.com/ip-{$log.ip|escape:'html':'utf-8'}.html" title="{l s='View location' mod='ets_cfultimate'}" target="_blank">{$log.ip|escape:'html':'utf-8'}</a>
                                            <span class="log_date">{$log.datetime_added|escape:'html':'utf-8'}</span>
                                        </li>
                                    {/foreach}</ul>
                                    {if $ets_cfu_logs|count >=5}<a class="ets_cfu_statistics_link" href="{$ets_cfu_link->getAdminLink('AdminContactFormUltimateStatistics',true)|escape:'html':'UTF-8'}">{l s='View more' mod='ets_cfultimate'}</a>{/if}
                                {else}
                                    <ul>
                                        <li>{l s='No views log' mod='ets_cfultimate'}</li>
                                    </ul>
                                {/if}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row form_dashboard-card">
        <div class="col-md-12">
            {if isset($ets_cfu_stats) && $ets_cfu_stats}{foreach from=$ets_cfu_stats item='stat'}
                <div class="dashboard-card card-{$stat.color|escape:'html':'utf-8'}">
                    <div class="dashboard-card-content">
                        <div class="pull-left">
                            <h2 class="value1">{$stat.value1|intval} <span class="value2">{$stat.value2|escape:'html':'utf-8'}</span></h2>

                            <span class="color">{$stat.label|escape:'html':'utf-8'}</span>
                        </div>
                        <div class="pull-right">
                            <div class="fa fa-{$stat.icon|escape:'html':'utf-8'} fa-2x"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="percentage"><span class="percentage_content" style="width: {$stat.percent|escape:'html':'utf-8'}%"></span></div>
                        <a href="{$stat.link|escape:'html':'utf-8'}" class="dashboard-card-link">{l s='View more' mod='ets_cfultimate'}</a>
                    </div>
                </div>
            {/foreach}{/if}
        </div>
    </div>
</div>

