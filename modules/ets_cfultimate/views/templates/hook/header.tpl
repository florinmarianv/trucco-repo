{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
<script type="text/javascript">
    var url_basic_ets = '{$url_basic|escape:'html':'UTF-8'}';
    var link_contact_ets = '{$link_contact_ets  nofilter}';
    var ets_cfu_recaptcha_enabled = {if isset($rc_enabled) && $rc_enabled}1{else}0{/if};
    var iso_code = '{if isset($iso_code) && $iso_code}{$iso_code|escape:'html':'UTF-8'}{/if}';
    {if isset($rc_enabled) && $rc_enabled}
        var ets_cfu_recaptcha_v3 = {$rc_v3|intval};
        var ets_cfu_recaptcha_key = "{$rc_key|escape:'html':'utf-8'}";
    {/if}
    {if isset($hidden_referrence) && $hidden_referrence}
        var hidden_referrence = {$hidden_referrence|intval};
    {/if}
</script>
{if isset($rc_enabled) && $rc_enabled}
    <script src="https://www.google.com/recaptcha/api.js?hl={$iso_code|escape:'html':'utf-8'}{if $rc_v3}&render={$rc_key|escape:'html':'utf-8'}{/if}"></script>
{/if}