<?php
/**
 * Page Cache Ultimate, Page Cache standard and Speed pack are powered by Jpresta (jpresta . com)
 *
 * @author    Jpresta
 * @copyright Jpresta
 * @license   You are just allowed to modify this copy for your own use. You must not redistribute it. License
 *               is permitted for one Prestashop instance only but you can install it on your test instances.
 */

include_once(dirname(__FILE__) . '/../../pagecache.php');

class AdminPageCacheDatasController extends ModuleAdminController
{
    public $php_self = null;

    public function init()
    {
        parent::init();
    }

    public function initContent()
    {
        header('Access-Control-Allow-Origin: *');

        parent::initContent();

        // DB table to use
        $table = _DB_PREFIX_.PageCacheDAO::TABLE;

        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array(
                'db' => 'url',
                'dt' => 0,
                'formatter' => function ($s, $row) {
                    return self::formatURL($s, $row);
                }
            ),
            array('db' => 'id_currency', 'dt' => -1),
            array('db' => 'id_lang', 'dt' => -1),
            array('db' => 'id_country', 'dt' => -1),
            array('db' => 'id_device', 'dt' => -1),
            array('db' => 'id_fake_customer', 'dt' => -1),
            array('db' => 'id_tax_csz', 'dt' => -1),
            array('db' => 'id_specifics', 'dt' => -1),
            array('db' => 'v_css', 'dt' => -1),
            array('db' => 'v_js', 'dt' => -1),

            array('db' => 'controller', 'dt' => 1),
            array('db' => 'id_object', 'dt' => 2),
            array(
                'db' => 'last_gen',
                'dt' => 3,
                'formatter' => function ($d, $row) {
                    $lastGen = strtotime($d);
                    $cache_ttl = 60 * ((int)Configuration::get('pagecache_'.$row['controller'].'_timeout'));
                    $age = time() - $lastGen;
                    $ttl = $cache_ttl - $age;
                    if ($cache_ttl <= 0) {
                        $percent = 0;
                    }
                    elseif ($cache_ttl <= $age) {
                        $percent = 100;
                    }
                    else {
                        $percent = $age * 100 / $cache_ttl;
                    }
                    $color = '#ccc';
                    if ($percent >= 100) {
                        $color = 'red';
                    }
                    else if ($percent >= 95) {
                        $color = 'orange';
                    }
                    $html = '<span style="cursor:help" title="Age: ' . self::getNiceDuration($age) . ' - Time to live: ' . ($cache_ttl == -60 ? 'forever' : ($ttl <= 0 ? 'dead' : self::getNiceDuration($ttl))) . '">' . date('Y-m-d H:i:s', strtotime($d)) . '</span>';
                    $html .= '<div style="border-bottom: 2px solid ' . $color . ';width: ' . $percent . '%;"></div>';
                    return $html;
                }
            ),
            array('db' => 'deleted', 'dt' => 4,'formatter' => function ($val) {
                if (Tools::version_compare(_PS_VERSION_, '1.6', '>')) {
                    return $val ? '<i class="icon-check"></i>' : '';
                }
                else {
                    return $val ? 'X' : '';
                }
            }),
            array('db' => 'count_hit', 'dt' => 5, 'formatter' => function ($s, $row) {
                return self::formatHit($s, $row);
            }),
            array('db' => 'count_missed', 'dt' => -1),
        );
        $result = self::simple($_GET, Db::getInstance(), $table, $columns);
        die(json_encode($result));
    }

    private static function formatHit($count_hit, $row) {
        return '<span style="color:green">' . $count_hit . '</span>&nbsp;/&nbsp;<span style="color:red">' . $row['count_missed'] . '</span> (' . round($count_hit * 100 / max(1, ($count_hit + $row['count_missed'])),1) . '%)';
    }

    private static function formatURL($url, $row) {
        $flags = array();

        if (!empty($row['id_currency'])) {
            $currency = new Currency($row['id_currency']);
            $flags[] = '<span class="label label-default" title="Currency">' . $currency->sign . '</span>';
        }
        if (!empty($row['id_country'])) {
            $country = new Country($row['id_country']);
            $flags[] = '<span class="label label-default" title="Country">' . $country->iso_code . '</span>';
        }
        if (!empty($row['id_device'])) {
            if ($row['id_device'] == PageCache::DEVICE_COMPUTER) {
                if (Tools::version_compare(_PS_VERSION_, '1.6', '>')) {
                    $flags[] = '<span class="label label-default" title="Desktop device"><i class="icon-desktop"></i></span>';
                }
                else {
                    $flags[] = '<span class="label label-default" title="Desktop device">Desktop</span>';
                }
            }
            elseif ($row['id_device'] == PageCache::DEVICE_TABLET) {
                if (Tools::version_compare(_PS_VERSION_, '1.6', '>')) {
                    $flags[] = '<span class="label label-default" title="Tablet device"><i class="icon-tablet"></i></span>';
                }
                else {
                    $flags[] = '<span class="label label-default" title="Tablet device">Tablet</span>';
                }
            }
            elseif ($row['id_device'] == PageCache::DEVICE_MOBILE) {
                if (Tools::version_compare(_PS_VERSION_, '1.6', '>')) {
                    $flags[] = '<span class="label label-default" title="Mobile device"><i class="icon-mobile"></i></span>';
                }
                else {
                    $flags[] = '<span class="label label-default" title="Mobile device">Mobile</span>';
                }
            }
        }
        if (!empty($row['id_fake_customer'])) {
            $groupList = '';
            $groupIds = Customer::getGroupsStatic($row['id_fake_customer']);
            foreach ($groupIds as $groupId) {
                $group = new Group($groupId);
                if (!empty($groupList)) {
                    $groupList .= ', ';
                }
                if (is_array($group->name)) {
                    $groupList .= $group->name[Context::getContext()->cookie->id_lang];
                }
                else {
                    $groupList .= $group->name;
                }
            }
            if (Tools::version_compare(_PS_VERSION_, '1.6', '>')) {
                $flags[] = '<span class="label label-default" title="User groups"><i class="icon-users"></i> ' . $groupList . '</span>';
            }
            else {
                $flags[] = '<span class="label label-default" title="User groups">Groups: ' . $groupList . '</span>';
            }
        }
        if (!empty($row['id_tax_csz'])) {
            $tax_csz = PageCacheDAO::getDetailsById($row['id_tax_csz']);
            $flags[] = '<span class="label label-default" title="Country/State/Zipcode used to compute taxes">Tax: ' . $tax_csz . '</span>';
        }
        if (!empty($row['id_specifics'])) {
            $specifics = PageCacheDAO::getDetailsById($row['id_specifics']);
            $jscks = new JprestaCacheKeySpecifics($specifics);
            $specificsDiv = '<div class="pc_specifics"><b>Some specific keys added by some modules</b><br/>' . $jscks->toPrettyString() . '</div>';
            $flags[] = '<span class="label label-default specifics" title="Some specific keys added by some modules">Specifics: #' . $row['id_specifics'] . $specificsDiv . '</span>';
        }
        if (!empty($row['v_css'])) {
            $flags[] = '<span class="label label-default" title="CSS version to avoid the use of obsolete styles">CSS v' . $row['v_css'] . '</span>';
        }
        if (!empty($row['v_js'])) {
            $flags[] = '<span class="label label-default" title="JS version to avoid the use of obsolete javascript">JS v' . $row['v_js'] . '</span>';
        }
        return "<a class=\"cacheurl\" href=\"$url\" title=\"$url\" target=\"_blank\">$url</a> ".implode($flags, ' ');
    }

    private static function unparse_url($parsed_url) {
        $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
        $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
        $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
        $pass     = ($user || $pass) ? "$pass@" : '';
        $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
        $query    = isset($parsed_url['query']) && !empty($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
        return "$scheme$user$pass$host$port$path$query$fragment";
    }

    private static function getNiceDuration($durationInSeconds) {
        $duration = '';
        if ($durationInSeconds < 0) {
            $duration = '-';
        }
        else {
            $days = floor($durationInSeconds / 86400);
            $durationInSeconds -= $days * 86400;
            $hours = floor($durationInSeconds / 3600);
            $durationInSeconds -= $hours * 3600;
            $minutes = floor($durationInSeconds / 60);
            $seconds = $durationInSeconds - $minutes * 60;

            if ($days > 0) {
                $duration .= $days . ' days';
            }
            if ($hours > 0) {
                $duration .= ' ' . $hours . ' hours';
            }
            if ($minutes > 0) {
                $duration .= ' ' . $minutes . ' minutes';
            }
            if ($seconds > 0) {
                $duration .= ' ' . $seconds . ' seconds';
            }
        }
        return $duration;
    }

    /**
     * Create the data output array for the DataTables rows
     *
     * @param  array $columns Column information array
     * @param  array $data Data from the SQL get
     * @return array          Formatted data in a row based format
     */
    private static function data_output($columns, $data)
    {
        $out = array();
        for ($i = 0, $ien = count($data); $i < $ien; $i++) {
            $row = array();
            for ($j = 0, $jen = count($columns); $j < $jen; $j++) {
                $column = $columns[$j];
                // Is there a formatter?
                if (isset($column['formatter'])) {
                    $row[$column['dt']] = $column['formatter']($data[$i][$column['db']], $data[$i]);
                } else {
                    $row[$column['dt']] = $data[$i][$columns[$j]['db']];
                }
            }
            $out[] = $row;
        }
        return $out;
    }

    /**
     * Paging
     *
     * Construct the LIMIT clause for server-side processing SQL query
     *
     * @param  array $request Data sent to server by DataTables
     * @param  array $columns Column information array
     * @return string SQL limit clause
     */
    private static function limit($request)
    {
        $limit = '';
        if (isset($request['start']) && $request['length'] != -1) {
            $limit = "LIMIT " . ((int)$request['start']) . ", " . ((int)$request['length']);
        }
        return $limit;
    }

    /**
     * Ordering
     *
     * Construct the ORDER BY clause for server-side processing SQL query
     *
     * @param  array $request Data sent to server by DataTables
     * @param  array $columns Column information array
     * @return string SQL order by clause
     */
    private static function order($request, $columns)
    {
        $order = '';
        if (isset($request['order']) && count($request['order'])) {
            $orderBy = array();
            $dtColumns = self::pluck($columns, 'dt');
            for ($i = 0, $ien = count($request['order']); $i < $ien; $i++) {
                // Convert the column index into the column data property
                $columnIdx = (int)($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if ($requestColumn['orderable'] == 'true') {
                    $dir = $request['order'][$i]['dir'] === 'asc' ?
                        'ASC' :
                        'DESC';
                    $orderBy[] = '`' . $column['db'] . '` ' . $dir;
                }
            }
            if (count($orderBy)) {
                $order = 'ORDER BY ' . implode(', ', $orderBy);
            }
        }
        return $order;
    }

    /**
     * Searching / Filtering
     *
     * Construct the WHERE clause for server-side processing SQL query.
     *
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here performance on large
     * databases would be very poor
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @return string SQL where clause
     */
    private static function filter ( $request, $columns )
    {
        $globalSearch = array();
        $columnSearch = array();
        $dtColumns = self::pluck( $columns, 'dt' );

        if ( isset($request['search']) && $request['search']['value'] != '' ) {
            $str = $request['search']['value'];

            for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                $column = $columns[ $columnIdx ];

                if ( $requestColumn['searchable'] == 'true' ) {
                    if(!empty($column['db'])){
                        $globalSearch[] = "`".$column['db']."` LIKE '%".$str."%'";
                    }
                }
            }
        }

        // Individual column filtering
        if ( isset( $request['columns'] ) ) {
            for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                $column = $columns[ $columnIdx ];

                $str = $requestColumn['search']['value'];

                if ( $requestColumn['searchable'] == 'true' && $str != '' ) {
                    if(!empty($column['db'])){
                        if ($column['db'] === 'url') {
                            $columnSearch[] = "`" . $column['db'] . "` LIKE '%" . $str . "%'";
                        }
                        else {
                            $columnSearch[] = "`" . $column['db'] . "` = '" . $str . "'";
                        }
                    }
                }
            }
        }

        // Combine the filters into a single string
        $where = '';

        if ( count( $globalSearch ) ) {
            $where = '('.implode(' OR ', $globalSearch).')';
        }

        if ( count( $columnSearch ) ) {
            $where = $where === '' ?
                implode(' AND ', $columnSearch) :
                $where .' AND '. implode(' AND ', $columnSearch);
        }

        if ( $where !== '' ) {
            $where = 'WHERE '.$where;
        }

        return $where;
    }

    /**
     * Perform the SQL queries needed for an server-side processing requested,
     * utilising the helper functions of this class, limit(), order() and
     * filter() among others. The returned array is ready to be encoded as JSON
     * in response to an SSP request, or can be modified if needed before
     * sending back to the client.
     *
     * @param  array $request Data sent to server by DataTables
     * @param  Db $db Database connection
     * @param  string $table SQL table to query
     * @param  array $columns Column information array
     * @return array Server-side processing response array
     */
    private static function simple($request, $db, $table, $columns)
    {
        // Build the SQL query string from the request
        $limit = self::limit($request);
        $order = self::order($request, $columns);
        $where = self::filter( $request, $columns);

        // Main query to actually get the data
        try {
            $data = $db->executeS("SELECT `" . implode("`, `", self::pluck($columns, 'db')) . "`
			 FROM `$table`
			 $where
			 $order
			 $limit"
            );
            // Data set length after filtering
            // Total data set length
            $recordsFiltered = $recordsTotal = $db->getValue("SELECT COUNT(*) FROM `$table`");
        } catch (Exception $e) {
            die($e->getMessage());
        }
        /*
         * Output
         */
        return array(
            "draw" => isset ($request['draw']) ? (int)$request['draw'] : 0,
            "recordsTotal" => (int)$recordsTotal,
            "recordsFiltered" => (int)$recordsFiltered,
            "data" => self::data_output($columns, $data)
        );
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Internal methods
     */

    /**
     * Pull a particular property from each assoc. array in a numeric array,
     * returning and array of the property values from each item.
     *
     * @param  array $a Array to get data from
     * @param  string $prop Property to read
     * @return array        Array of property values
     */
    private static function pluck($a, $prop)
    {
        $out = array();
        for ($i = 0, $len = count($a); $i < $len; $i++) {
            $out[] = $a[$i][$prop];
        }
        return $out;
    }
}
