{*
 *  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
 *
 * @author    Línea Gráfica E.C.E. S.L.
 * @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
 * @license   https://www.lineagrafica.es/licenses/license_en.pdf
 *            https://www.lineagrafica.es/licenses/license_es.pdf
 *            https://www.lineagrafica.es/licenses/license_fr.pdf
 *}
{if !empty($redirects)}
    {foreach $redirects as $redirect}
    <tr id="{$redirect['id']|intval}">
        <td>
            <input type="checkbox" name="selected_redirects[]" value="{$redirect['id']|intval}">
        </td>
        <td>
            <span id="redid{$redirect['id']|intval}">{$redirect['id']|intval}</span>
        </td>
        <td style="direction: ltr !important;">
            <span id="oldurl{$redirect['id']|intval}">
            {if $redirect['error_startwith']}
            {* check if the old URI starts with a / *}
                <input type="hidden" name="wrongformat{$redirect['id']|intval}" class="wrongformat{$redirect['id']|intval}" value="2">
                <span class="toolTip1">
                    <img src="../modules/lgseoredirect/views/img/important.png" />
                    <p class="tooltipDesc1">{l s='Wrong format: the old URI must start with a "/"' mod='lgseoredirect'}</p>
                </span>
            {/if}
            {* check if the old URI is duplicated *}
            {if $redirect['error_checkduplicate'] > 1}
                <input type="hidden" name="duplicate{$redirect['id']|intval}" class="duplicate{$redirect['id']|intval}" value="1">
                <span class="toolTip2">
                    <img src="../modules/lgseoredirect/views/img/important2.png" />
                    <p class="tooltipDesc2">{l s='Duplicated redirects: several redirects exist for this old URI.' mod='lgseoredirect'}</p>
                </span>
            {/if}
            {* OLD URI *}
                {if $lgseoredirect_is_rtl}
                    <a href="{$lgseoredirect_shop_domain|escape:'htmlall':'UTF-8'}{$lgseoredirect_shop_uri|escape:'htmlall':'UTF-8'}{$redirect['url_old']|escape:'htmlall':'UTF-8'}" style="direction: ltr !important;" target="_blank">
                        <span style="font-weight: normal; color: #aaaaaa;">{$lgseoredirect_shop_domain|escape:'htmlall':'UTF-8'}{$lgseoredirect_shop_uri|escape:'htmlall':'UTF-8'}</span><span style="font-weight:bold;">{$redirect['url_old']|escape:'htmlall':'UTF-8'}</span>
                    </a>
                {else}
                    <a href="{$lgseoredirect_shop_domain|escape:'htmlall':'UTF-8'}{$lgseoredirect_shop_uri|escape:'htmlall':'UTF-8'}{$redirect['url_old']|escape:'htmlall':'UTF-8'}" target="_blank">
                        <span style="font-weight: normal; color: #aaaaaa;">{$lgseoredirect_shop_domain|escape:'htmlall':'UTF-8'}{$lgseoredirect_shop_uri|escape:'htmlall':'UTF-8'}</span><span style="font-weight:bold;">{$redirect['url_old']|escape:'htmlall':'UTF-8'}</span>
                    </a>
                {/if}
            {if $redirect['error_checkduplicate'] > 1}
                <span class="autofilter" data-url="{$redirect['url_old']|escape:'htmlall':'UTF-8'}" style="cursor: pointer">
                    <img src="../modules/lgseoredirect/views/img/filter.png" />
                    <p class="tooltipDesc2">{l s='Duplicated redirects: several redirects exist for this old URI.' mod='lgseoredirect'}</p>
                </span>
            {/if}
            </span>
        </td>
        <td style="font-size:x-large;">{if $lgseoredirect_is_rtl}&larr;{else}&rarr;{/if}</td>
        <td style="direction: ltr !important;">
            <span id="newurl{$redirect['id']|intval}">
            {* check if the new URL starts with a http or https *}
                {if $redirect['error_startwith2']}
                    <input type="hidden" name="wrongformat{$redirect['id']|intval}" class="wrongformat{$redirect['id']|intval}" value="2">
                    <span class="toolTip1" class="wrongformat{$redirect['id']|intval}">
                        <img src="../modules/lgseoredirect/views/img/important.png" />
                        <p class="tooltipDesc1">{l s='Wrong format: the new URL must start with a "http" or "https".' mod='lgseoredirect'}</p>
                    </span>
                {/if}
                {* NEW URL *}
                {$redirect['url_new']|escape:'htmlall':'UTF-8'}
            </span>
        </td>
        <td>
            <input type="hidden" name="type{$redirect['id']|intval}" id="type{$redirect['id']|intval}" value="{$redirect['redirect_type']|escape:'htmlall':'UTF-8'}">
            {* check if the type of redirect starts is 301, 302 or 303*}
            {if $redirect['error_wrong_redirect_type']}
                <input type="hidden" name="wrongformat{$redirect['id']|intval}" class="wrongformat{$redirect['id']|intval}" value="2">
                <span class="toolTip1" class="wrongformat{$redirect['id']|intval}">
                    <img src="../modules/lgseoredirect/views/img/important.png" />
                    <p class="tooltipDesc1">{l s='Wrong format: the type of redirect must be "301", "302" or "303".' mod='lgseoredirect'}</p>
                </span>
            {/if}
            {* TYPE - DATE -DELETE *}
            {$redirect['redirect_type']|escape:'htmlall':'UTF-8'}
        </td>
        <td>
            <span id="date{$redirect['id']|intval}">{$redirect['fecha']|escape:'htmlall':'UTF-8'}
        </td>
        <td>
            <button class="button btn btn-default" type="button" name="deleteRedirect{$redirect['id']|intval}" idredirect="{$redirect['id']|intval}">
                <i class="icon-trash"></i> {l s='Delete' mod='lgseoredirect'}
            </button>
        </td>
    </tr>
    {/foreach}
{else}
    <tr><td colspan="8" class="lgseoredirects_no_results"><i class="icon-warning-sign"></i> &nbsp;{l s='No results found' mod='lgseoredirect'}</td></tr>
{/if}
