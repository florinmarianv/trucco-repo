/**
 *  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
 *
 * @author    Línea Gráfica E.C.E. S.L.
 * @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
 * @license   https://www.lineagrafica.es/licenses/license_en.pdf
 *            https://www.lineagrafica.es/licenses/license_es.pdf
 *            https://www.lineagrafica.es/licenses/license_fr.pdf
 */

changed_filters = false;

function LGGetRedirects(target) {
    var filters = {};
    var fredid  = $("#filterid").val().trim();
    var foldurl = $("#filteroldurl").val().trim();
    var fnewurl = $("#filternewurl").val().trim();
    var ftype   = $("#filtertype").val().trim()
    var fdate   = $("#filterdate").val().trim();
    var ferror  = $("#filtererror").val().trim();

    if (fredid != '') {
        filters['id'] = fredid;
    }

    if (foldurl != '') {
        filters['url_old'] = foldurl;
    }

    if (fnewurl != '') {
        filters['url_new'] = fnewurl;
    }

    if (ftype != '' && (parseInt(ftype) == 301 || parseInt(ftype) == 302 || parseInt(ftype) == 303)) {
        filters['type'] = ftype;
    }

    if (fdate != '') {
        filters['date'] = fdate;
    }

    if (ferror > 0) {
        filters['error'] = ferror;
    }

    if (changed_filters) {
        $('input[name="lgseoredirect_page"]').val(1);
    }

    $(target).LoadingOverlay('show');
    $.ajax({
        method: 'get',
        url: 'index.php',
        data: {
            ajax: true,
            controller: 'AdminModules',
            module_name: 'AdminModules',
            configure: 'lgseoredirect',
            token: lgseoredirect_token,
            action: 'getRedirects',
            p: $('input[name="lgseoredirect_page"]').val(),
            lgseoredirect_pagination: $('input[name="lgseoredirect_pagination"]').val(),
            filters: filters,
            rand: new Date().getTime(),
            cache: false
        },
        dataType: 'json',
        timeout: 30000
    }).error(function(jqXHR, textStatus, errorThrown) {
        if (textStatus == 'timeout') {
            $(target).LoadingOverlay('hide', true);
            showErrorMessage(lgseoredirect_msg_error_timeout);
        } else {
            showErrorMessage(lgseoredirect_msg_error_unknown);
        }
    }).done(function(response) {
        $(target).LoadingOverlay('hide', true);
        if (response.status == 'ok') {
            $('#checkall').prop('checked',false);
            //$('#tableproduct tbody').('');
            $('#tableredirect tbody').html(response.rows);
            $('#lgseoredirect_pagination').html(response.pagination);
            $('#lgseoredirect_total_products').html(response.total_products);

            if (window.lgseoredirect_select_all) {
                if (window.lgseoredirect_selected_items.length > 0) {
                    var some_deselected = false;
                    $('input[name^="selected_redirects"]').each(function(){
                        if (window.lgseoredirect_selected_items.indexOf(parseInt($(this).val())) >= 0) {
                            $(this).attr('checked', false);
                            some_deselected = true;
                        } else {
                            $(this).attr('checked', true);
                        }
                    });
                    $('#lgseoredirect_checkall').attr('checked', !some_deselected);
                } else {
                    checkAll();
                }
            } else {
                if (window.lgseoredirect_selected_items.length > 0) {
                    $('input[name^="selected_redirects"]').each(function(){
                        if (window.lgseoredirect_selected_items.indexOf(parseInt($(this).val())) >= 0) {
                            $(this).attr('checked', true);
                        }
                    });
                }
                var all_selected = true && ($('input[name^="selected_redirects"]').length > 0);
                $('input[name^="selected_redirects"]').each(function(){
                    if (!$(this).is(':checked')) {
                        all_selected = false;
                    }
                });
                if (all_selected) {
                    $('#lgseoredirect_checkall').attr('checked', true);
                } else {
                    $('#lgseoredirect_checkall').attr('checked', false);
                }
            }
        }
    });
}

function uncheckAll()
{
    $('input[name^="selected_redirects"]').each(function(){
        $(this).attr('checked', false);
    });
    $('#lgseoredirect_checkall').attr('checked', false);
}

function checkAll()
{
    $('input[name^="selected_redirects"]').each(function(){
        $(this).attr('checked', true);
    });
    $('#lgseoredirect_checkall').attr('checked', true);
}

$(document).ready(function(){
    window.lgseoredirect_selected_items = [];
    window.lgseoredirect_select_all     = 0;

    $(document).on('keyup',  "#filterid",     function(){changed_filters = true;LGGetRedirects('#tableredirect tbody');});
    $(document).on('keyup',  "#filteroldurl", function(){changed_filters = true;LGGetRedirects('#tableredirect tbody');});
    $(document).on('keyup',  "#filternewurl", function(){changed_filters = true;LGGetRedirects('#tableredirect tbody');});
    $(document).on('change', "#filtertype",   function(){changed_filters = true;LGGetRedirects('#tableredirect tbody');});
    $(document).on('keyup',  "#filterdate",   function(){changed_filters = true;LGGetRedirects('#tableredirect tbody');});
    $(document).on('change', "#filtererror",  function(){changed_filters = true;LGGetRedirects('#tableredirect tbody');});

    $(document).on('click','.pagination-link', function() {
        //var selected_products = [];
        $('input[name="lgseoredirect_page"]').val($(this).attr('data-page'));
        changed_filters = false;
        LGGetRedirects('#tableredirect tbody');
    });

    $(document).on('click', '.pagination-items-page', function(e){
        e.preventDefault();
        $('#lgseoredirect-pagination-items-page').val($(this).data("items"));
        changed_filters = false;
        LGGetRedirects('#tableredirect tbody');
    });

    $(document).on('click', "#lgseoredirect_checkall", function() {
        if ($(this).is(":checked")) {
            checkAll();
        } else {
            uncheckAll();
        }
        $('input[name^="selected_redirects"]').each(function(){
            if (window.lgseoredirect_select_all) {
                if (!$(this).attr("checked")) {
                    if (window.lgseoredirect_selected_items.indexOf(parseInt($(this).val())) < 0) {
                        window.lgseoredirect_selected_items.push(parseInt($(this).val()));
                    }
                } else {
                    if (window.lgseoredirect_selected_items.indexOf(parseInt($(this).val())) >= 0) {
                        var pos = window.lgseoredirect_selected_items.indexOf(parseInt($(this).val()));
                        window.lgseoredirect_selected_items.splice(parseInt(pos), 1);
                    }
                }
            } else {
                if ($(this).attr("checked")) {
                    if (window.lgseoredirect_selected_items.indexOf(parseInt($(this).val())) < 0) {
                        window.lgseoredirect_selected_items.push(parseInt($(this).val()));
                    }
                } else {
                    if (window.lgseoredirect_selected_items.indexOf(parseInt($(this).val())) >= 0) {
                        var pos = window.lgseoredirect_selected_items.indexOf(parseInt($(this).val()));
                        window.lgseoredirect_selected_items.splice(parseInt(pos), 1);
                    }
                }
            }
        });
    });

    $(document).on('click', '#lgseoredirect_clear_selection', function() {
        window.lgseoredirect_selected_items = [];
        window.lgseoredirect_select_all     = 0;
        uncheckAll();
    });

    $(document).on('click', '#lgseoredirect_select_all_redirects', function() {
        window.lgseoredirect_selected_items = [];
        window.lgseoredirect_select_all     = 1;
        checkAll();
    });

    $(document).on('click', 'input[name^="selected_redirects"]', function () {
        if (window.lgseoredirect_selected_items.indexOf(parseInt($(this).val())) < 0) {
            window.lgseoredirect_selected_items.push(parseInt($(this).val()));
        } else {
            var pos = window.lgseoredirect_selected_items.indexOf(parseInt($(this).val()));
            window.lgseoredirect_selected_items.splice(parseInt(pos), 1);
        }

        var all_selected = true;
        $('input[name^="selected_redirects"]').each(function(){
            if (!$(this).is(':checked')) {
                all_selected = false;
            }
        });
        if (all_selected) {
            $('#lgseoredirect_checkall').attr('checked', true);
        } else {
            $('#lgseoredirect_checkall').attr('checked', false);
        }
    });

    $(document).on('click', 'button[name="lgseoredirect_deleteSelected"]', function() {
        $('#lgseoredirect_list_form').LoadingOverlay('show');
        $.ajax({
            method: 'get',
            url: 'index.php',
            data: {
                ajax: true,
                controller: 'AdminModules',
                module_name: 'AdminModules',
                configure: 'lgseoredirect',
                token: lgseoredirect_token,
                action: 'deleteRedirects',
                redirects: window.lgseoredirect_selected_items,
                allselected: window.lgseoredirect_select_all,
                rand: new Date().getTime(),
                cache: false
            },
            dataType: 'json'
        }).success(function (response) {
            $('#lgseoredirect_list_form').LoadingOverlay('hide');
            if (response.status == 'ok') {
                window.lgseoredirect_selected_items = [];
                window.lgseoredirect_select_all = 0;
                showSuccessMessage(response.message);
                LGGetRedirects('#tableredirect tbody');
            }
        }).error(function (response) {
            $('#lgseoredirect_list_form').LoadingOverlay('hide');
            if (response.status == 'ok') {
                window.lgseoredirect_selected_items = [];
                window.lgseoredirect_select_all = 0;
                showErrorMessage(response.message);
                LGGetRedirects('#tableredirect tbody');
            }
        });
    });

    $(document).on('click', 'button[name^="deleteRedirect"]', function() {
        var id_redirect = $(this).attr('idredirect');
        var redirects   = [];
        redirects.push(id_redirect);
        $('#lgseoredirect_list_form').LoadingOverlay('show');
        $.ajax({
            method: 'get',
            url: 'index.php',
            data: {
                ajax: true,
                controller: 'AdminModules',
                module_name: 'AdminModules',
                configure: 'lgseoredirect',
                token: lgseoredirect_token,
                action: 'deleteRedirects',
                redirects: redirects,
                allselected: 0,
                rand: new Date().getTime()
            },
            dataType: 'json'
        }).success(function (response) {
            $('#lgseoredirect_list_form').LoadingOverlay('hide');
            if (response.status == 'ok') {
                if (window.lgseoredirect_selected_items.indexOf(id_redirect) >= 0) {
                    var pos = window.lgseoredirect_selected_items.indexOf(id_redirect);
                    window.lgseoredirect_selected_items.splice(parseInt(pos), 1);
                    console.log(window.lgseoredirect_selected_items);
                    console.log(window.lgseoredirect_select_all);
                }
                showSuccessMessage(response.message);
            }
            LGGetRedirects('#tableredirect tbody');
        }).error(function (response) {
            $('#lgseoredirect_list_form').LoadingOverlay('hide');
            if (response.status == 'ok') {
                showErrorMessage(response.message);
            }
            LGGetRedirects('#tableredirect tbody');
        });
    });

    $(document).on('click', '.autofilter', function() {
        $('#filteroldurl').val($(this).attr('data-url'));
        $('#filteroldurl').keyup();
    });

    $("#individualredirect").show(); $("#bulkredirects").hide(); $("#listredirects").hide();
    $("#buttonindividualredirect").removeClass("btn-default").addClass("btn-primary");
    $("#buttonbulkredirects").removeClass("btn-primary").addClass("btn-default");
    $("#buttonlistredirects").removeClass("btn-primary").addClass("btn-default");
    $("#buttonindividualredirect").click(function(){
        $("#individualredirect").show(); $("#bulkredirects").hide(); $("#listredirects").hide();
        $("#buttonindividualredirect").removeClass("btn-default").addClass("btn-primary");
        $("#buttonbulkredirects").removeClass("btn-primary").addClass("btn-default");
        $("#buttonlistredirects").removeClass("btn-primary").addClass("btn-default");
    });
    $("#buttonbulkredirects").click(function(){
        $("#individualredirect").hide(); $("#bulkredirects").show(); $("#listredirects").hide();
        $("#buttonindividualredirect").removeClass("btn-primary").addClass("btn-default");
        $("#buttonbulkredirects").removeClass("btn-default").addClass("btn-primary");
        $("#buttonlistredirects").removeClass("btn-primary").addClass("btn-default");
    });
    $("#buttonlistredirects").click(function(){
        $("#individualredirect").hide(); $("#bulkredirects").hide(); $("#listredirects").show();
        $("#buttonindividualredirect").removeClass("btn-primary").addClass("btn-default");
        $("#buttonbulkredirects").removeClass("btn-primary").addClass("btn-default");
        $("#buttonlistredirects").removeClass("btn-default").addClass("btn-primary");
    });
});
