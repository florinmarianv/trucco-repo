<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

class AdminTdkblogModuleController extends ModuleAdminControllerCore
{

    public function __construct()
    {
        parent::__construct();
        
        $url = 'index.php?controller=adminmodules&configure=tdkblog&tab_module=front_office_features&module_name=tdkblog&token='.Tools::getAdminTokenLite('AdminModules');
        Tools::redirectAdmin($url);
    }
}
