{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<div id="disqus_thread"></div>
<script type="text/javascript">
var disqus_shortname = '{$config->get('item_diquis_account','tdkstudio')|escape:'html':'UTF-8'}';
var disqus_url = '{$blog_link|escape:'html':'UTF-8'}';
(function() {
	var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
	dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
	(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
</script>
<noscript>{l s='Please enable JavaScript to view the.' mod='tdkblog'} <a href="http://disqus.com/?ref_noscript">{l s='Comments powered by Disqus.' mod='tdkblog'}.</a></noscript>