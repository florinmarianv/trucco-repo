{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {   
  	return;
  }
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/{$lang_locale}/sdk.js#xfbml=1&version=v3.2&appId={$config->get('item_facebook_appid')|escape:'html':'UTF-8'}&autoLogAppEvents=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-comments" data-href="{$blog_link|escape:'html':'UTF-8'}" 
		data-num-posts="{$config->get("item_limit_comments",10)|escape:'html':'UTF-8'}" data-width="{$config->get('item_facebook_width',600)|escape:'html':'UTF-8'}">
</div>