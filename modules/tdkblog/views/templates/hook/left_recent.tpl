{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{if isset($leading_blogs) AND !empty($leading_blogs)}
    <section id="blogRecentBlog" class="block tdk-block-sidebar hidden-sm-down">
        <h4 class='title_block'><a href="">{l s='Recent Articles' mod='tdkblog'}</a></h4>
            <div class="block_content products-block">
                <ul class="lists">
                    {foreach from=$leading_blogs item="blog" name=leading_blog}
                        <li class="list-item clearfix{if $smarty.foreach.leading_blog.last} last_item{elseif $smarty.foreach.leading_blog.first} first_item{else}{/if}">
                            <div class="blog-image">
                                <a class="products-block-image" title="{$blog.title|escape:'html':'UTF-8'}" href="{$blog.link|escape:'html':'UTF-8'}">
                                    <img alt="{$blog.title|escape:'html':'UTF-8'}" src="{$blog.preview_url|escape:'html':'UTF-8'}" class="img-fluid">
                                </a>
                            </div>
                            <div class="blog-content">
                            	<h3 class="post-name"><a title="{$blog.title}" href="{$blog.link|escape:'html':'UTF-8'}">{$blog.title}</a></h3>
                            	<span class="info">{$blog.date_add|date_format:"%b %d, %Y"}</span>
                            </div>
                        </li> 
                    {/foreach}
                </ul>
            </div>
    </section>
{/if}

