{**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**}

{if isset($tdkblogtags) AND !empty($tdkblogtags)}
    <section id="tags_blog_block_left" class="block tdk-blog-tags hidden-sm-down">
        <h4 class='title_block'><a href="">{l s='Tags Post' mod='tdkblog'}</a></h4>
        <div class="block_content clearfix">
            {foreach from=$tdkblogtags item="tag"}
                <a href="{$tag.link}">{$tag.name}</a>
            {/foreach}
        </div>
    </section>
{/if}