<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

define('_TDK_BLOG_PREFIX_', 'TDKBLOG_');
require_once(_PS_MODULE_DIR_.'tdkblog/classes/config.php');

$config = TdkBlogConfig::getInstance();


define('_TDKBLOG_BLOG_IMG_DIR_', _PS_MODULE_DIR_.'tdkblog/views/img/');
define('_TDKBLOG_BLOG_IMG_URI_', __PS_BASE_URI__.'modules/tdkblog/views/img/');


define('_TDKBLOG_CATEGORY_IMG_URI_', _PS_MODULE_DIR_.'tdkblog/views/img/');
define('_TDKBLOG_CATEGORY_IMG_DIR_', __PS_BASE_URI__.'modules/tdkblog/views/img/');

define('_TDKBLOG_CACHE_IMG_DIR_', _PS_IMG_DIR_.'tdkblog/');
define('_TDKBLOG_CACHE_IMG_URI_', _PS_IMG_.'tdkblog/');

$link_rewrite = 'link_rewrite'.'_'.Context::getContext()->language->id;
define('_TDK_BLOG_REWRITE_ROUTE_', $config->get($link_rewrite, 'blog'));

if (!is_dir(_TDKBLOG_BLOG_IMG_DIR_.'c')) {
    # validate module
    mkdir(_TDKBLOG_BLOG_IMG_DIR_.'c', 0777, true);
}

if (!is_dir(_TDKBLOG_BLOG_IMG_DIR_.'b')) {
    # validate module
    mkdir(_TDKBLOG_BLOG_IMG_DIR_.'b', 0777, true);
}

if (!is_dir(_TDKBLOG_CACHE_IMG_DIR_)) {
    # validate module
    mkdir(_TDKBLOG_CACHE_IMG_DIR_, 0777, true);
}
if (!is_dir(_TDKBLOG_CACHE_IMG_DIR_.'c')) {
    # validate module
    mkdir(_TDKBLOG_CACHE_IMG_DIR_.'c', 0777, true);
}
if (!is_dir(_TDKBLOG_CACHE_IMG_DIR_.'b')) {
    # validate module
    mkdir(_TDKBLOG_CACHE_IMG_DIR_.'b', 0777, true);
}

require_once(_PS_MODULE_DIR_.'tdkblog/libs/Helper.php');
require_once(_PS_MODULE_DIR_.'tdkblog/classes/tdkblogcat.php');
require_once(_PS_MODULE_DIR_.'tdkblog/classes/blog.php');
require_once(_PS_MODULE_DIR_.'tdkblog/classes/link.php');
require_once(_PS_MODULE_DIR_.'tdkblog/classes/comment.php');
require_once(_PS_MODULE_DIR_.'tdkblog/classes/TdkblogOwlCarousel.php');
