<?php
/**
*
* PrestaShop module created by TDK Studio, A young & creative agency focus on Digital platform
*
* @author    TDK Studio
* @copyright 2018-9999 TDK Studio
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER
* Email/Skype: info.tdkstudio@gmail.com
*
**/

if (!defined('_PS_VERSION_')) {
    # module validation
    exit;
}

include_once(_PS_MODULE_DIR_.'tdkblog/loader.php');

class Tdkblog extends Module
{
    private static $leo_xml_fields = array('title', 'guid', 'description', 'author', 'comments', 'pubDate', 'source', 'link', 'content');
    public $base_config_url;
    private $_html = '';

    public function __construct()
    {
        $currentIndex = '';

        $this->name = 'tdkblog';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'TDK Studio';
        $this->controllers = array('detail', 'category', 'list');
        $this->need_instance = 0;
        $this->bootstrap = true;

        $this->secure_key = Tools::encrypt($this->name);

        parent::__construct();

        $this->base_config_url = $currentIndex.'&configure='.$this->name.'&token='.Tools::getValue('token');
        $this->displayName = $this->l('TDK Blog Management');
        $this->description = $this->l('Manage Blog Content');
        //$this->registerHook('displayBackOfficeHeader');
        // $this->registerHook('actionAdminControllerSetMedia');
        // $this->registerTDKHook();
    }

    /**
     * Uninstall
     */
    private function uninstallModuleTab($class_sfx = '')
    {
        $tab_class = 'Admin'.Tools::ucfirst($this->name).Tools::ucfirst($class_sfx);

        $id_tab = Tab::getIdFromClassName($tab_class);
        if ($id_tab != 0) {
            $tab = new Tab($id_tab);
            $tab->delete();
            return true;
        }
        return false;
    }

    /**
     * Install Module Tabs
     */
    private function installModuleTab($title, $class_sfx = '', $parent = '')
    {
        $class = 'Admin'.Tools::ucfirst($this->name).Tools::ucfirst($class_sfx);
        @copy(_PS_MODULE_DIR_.$this->name.'/logo.gif', _PS_IMG_DIR_.'t/'.$class.'.gif');
        if ($parent == '') {
            # validate module
            $position = Tab::getCurrentTabId();
        } else {
            # validate module
            $position = Tab::getIdFromClassName($parent);
        }

        $tab1 = new Tab();
        $tab1->class_name = $class;
        $tab1->module = $this->name;
        $tab1->id_parent = (int)$position;
        $langs = Language::getLanguages(false);
        foreach ($langs as $l) {
            # validate module
            $tab1->name[$l['id_lang']] = $title;
        }
//        $id_tab1 = $tab1->add(true, false);
        $tab1->add(true, false);
    }

    /**
     * @see Module::install()
     */
    public function install()
    {
        /* Adds Module */
        if (parent::install() && $this->registerTDKHook()) {
            $res = true;

            Configuration::updateValue('TDKBLOG_CATEGORY_MENU', 1);
            Configuration::updateValue('TDKBLOG_IMAGE_TYPE', 'jpg');
            
            Configuration::updateValue('TDKBLOG_DASHBOARD_DEFAULTTAB', '#fieldset_0');
            /* Creates tables */
            $res &= $this->createTables();
            
            Configuration::updateValue('TDK_INSTALLED_TDKBLOG', '1');
            //TDK: check thumb column, if not exist auto add
            if (Db::getInstance()->executeS('SHOW TABLES LIKE \'%tdkblog_blog%\'') && count(Db::getInstance()->executes('SELECT "thumb" FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "'._DB_NAME_.'" AND TABLE_NAME = "'._DB_PREFIX_.'tdkblog_blog" AND COLUMN_NAME = "thumb"'))<1) {
                Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'tdkblog_blog` ADD `thumb` varchar(255) DEFAULT NULL');
            }
            
            // $this->registerHook('header'); # remove code in 2016
            $id_parent = Tab::getIdFromClassName('IMPROVE');
            
            $class = 'Admin'.Tools::ucfirst($this->name).'Management';
            $tab1 = new Tab();
            $tab1->class_name = $class;
            $tab1->module = $this->name;
            $tab1->id_parent = $id_parent;
            $langs = Language::getLanguages(false);
            foreach ($langs as $l) {
                # validate module
                $tab1->name[$l['id_lang']] = $this->l('TDK Blog Management');
            }
//            $id_tab1 = $tab1->add(true, false);
            $tab1->add(true, false);
            
            # insert icon for tab
            Db::getInstance()->execute(' UPDATE `'._DB_PREFIX_.'tab` SET `icon` = "create" WHERE `id_tab` = "'.(int)$tab1->id.'"');
            
            $this->installModuleTab('Blog Dashboard', 'dashboard', 'AdminTdkblogManagement');
            $this->installModuleTab('Categories Management', 'categories', 'AdminTdkblogManagement');
            $this->installModuleTab('Blogs Management', 'blogs', 'AdminTdkblogManagement');
            $this->installModuleTab('Comment Management', 'comments', 'AdminTdkblogManagement');
            $this->installModuleTab('TDK Blog Configuration', 'module', 'AdminTdkblogManagement');
            
            //TDK:: move image folder from module to theme
            $this->moveImageFolder();
            
            return (bool)$res;
        }
        return false;
    }

    public function hookDisplayBackOfficeHeader()
    {
        if (file_exists(_PS_THEME_DIR_.'css/modules/tdkblog/assets/admin/blogmenu.css')) {
            $this->context->controller->addCss($this->_path.'assets/admin/blogmenu.css');
        } else {
            $this->context->controller->addCss($this->_path.'views/css/admin/blogmenu.css');
        }
    }

    public function getContent()
    {
        if (Tools::isSubmit('submitBlockCategories')) {
            # validate module
            Configuration::updateValue('TDKBLOG_CATEGORY_MENU', (int)Tools::getValue('TDKBLOG_CATEGORY_MENU'));
            Configuration::updateValue('TDKBLOG_IMAGE_TYPE', Tools::getValue('TDKBLOG_IMAGE_TYPE'));
        }
        //TDK:: correct module
        if (Tools::getValue('correctmodule')) {
            $this->correctModule();
        }
        
        if (Tools::getValue('success')) {
            switch (Tools::getValue('success')) {
                case 'correct':
                    $this->_html .= $this->displayConfirmation($this->l('Correct Module is successful'));
                    break;
            }
        }
       
        return $this->_html.$this->renderForm();
    }

    public function getTreeForTdkPlatform($selected)
    {
        $cat = new Tdkblogcat();
        return $cat->getTreeForTdkPlatform($selected);
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable Categories Tree Block'),
                        'name' => 'TDKBLOG_CATEGORY_MENU',
                        'desc' => $this->l('Activate  The Module.'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Image type'),
                        'name' => 'TDKBLOG_IMAGE_TYPE',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => 'jpg',
                                    'name' => $this->l('jpg')
                                ),
                                array(
                                    'id' => 'png',
                                    'name' => $this->l('png')
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'desc' => $this->l('For images png type. Keep png type or optimize to jpg type'),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default')
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitBlockCategories';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'TDKBLOG_CATEGORY_MENU' => Tools::getValue('TDKBLOG_CATEGORY_MENU', Configuration::get('TDKBLOG_CATEGORY_MENU')),
            'TDKBLOG_IMAGE_TYPE' => Tools::getValue('TDKBLOG_IMAGE_TYPE', Configuration::get('TDKBLOG_IMAGE_TYPE')),
            
        );
    }

    public function _prepareHook()
    {
        $helper = TdkBlogHelper::getInstance();

        $category = new Tdkblogcat(Tools::getValue('id_tdkblogcat'), $this->context->language->id);

        $tree = $category->getFrontEndTree((int)$category->id_tdkblogcat > 1 ? $category->id_tdkblogcat : 1, $helper);
        $this->smarty->assign('tree', $tree);
        if ($category->id_tdkblogcat) {
            # validate module
            $this->smarty->assign('currentCategory', $category);
        }

        return true;
    }

    public function hookDisplayHeader()
    {
        if (file_exists(_PS_THEME_DIR_.'css/modules/tdkblog/assets/tdkblog.css')) {
            $this->context->controller->addCSS(($this->_path).'assets/tdkblog.css', 'all');
        } else {
            $this->context->controller->addCSS(($this->_path).'views/css/tdkblog.css', 'all');
        }
                    
        //TDK:: update language link
        if (Tools::getValue('module') == 'tdkblog') {
            $langs = Language::getLanguages(false);
            if (count($langs) > 1) {
                $config = TdkBlogConfig::getInstance();
                $array_list_rewrite = array();
                $array_category_rewrite = array();
                $array_config_category_rewrite = array();
                $array_blog_rewrite = array();
                $array_config_blog_rewrite = array();
                $config_url_use_id = $config->get('url_use_id');
                
                $page_name = Dispatcher::getInstance()->getController();
                
                if ($page_name == 'blog') {
                    if ($config_url_use_id == 1) {
                        $id_blog = Tools::getValue('id');
                    } else {
                        $id_shop = (int)Context::getContext()->shop->id;
                        $block_rewrite = Tools::getValue('rewrite');
                        $sql = 'SELECT bl.id_tdkblog_blog FROM '._DB_PREFIX_.'tdkblog_blog_lang bl';
                        $sql .= ' INNER JOIN '._DB_PREFIX_.'tdkblog_blog_shop bs on bl.id_tdkblog_blog=bs.id_tdkblog_blog AND id_shop='.$id_shop;
                        $sql .= " AND link_rewrite = '".$block_rewrite."'";
                        if ($row = Db::getInstance()->getRow($sql)) {
                            $id_blog = $row['id_tdkblog_blog'];
                        }
                    }
                    $blog_obj = new TdkBlogBlog($id_blog);
                }
                
                if ($page_name == 'category') {
                    if ($config_url_use_id == 1) {
                        $id_category = Tools::getValue('id');
                    } else {
                        $id_shop = (int)Context::getContext()->shop->id;
                        $category_rewrite = Tools::getValue('rewrite');
                        $sql = 'SELECT cl.id_tdkblogcat FROM '._DB_PREFIX_.'tdkblogcat_lang cl';
                        $sql .= ' INNER JOIN '._DB_PREFIX_.'tdkblogcat_shop cs on cl.id_tdkblogcat=cs.id_tdkblogcat AND id_shop='.$id_shop;
                        $sql .= ' INNER JOIN '._DB_PREFIX_.'tdkblogcat cc on cl.id_tdkblogcat=cc.id_tdkblogcat AND cl.id_tdkblogcat != cc.id_parent';  # FIX : PARENT IS NOT THIS CATEGORY
                        $sql .= " AND link_rewrite = '".$category_rewrite."'";

                        if ($row = Db::getInstance()->getRow($sql)) {
                            $id_category = $row['id_tdkblogcat'];
                        }
                    }
                    $blog_category_obj = new Tdkblogcat($id_category);
                }
                
                foreach ($langs as $lang) {
                    $array_list_rewrite[$lang['iso_code']] = $config->get('link_rewrite_'.$lang['id_lang'], 'blog');
                    
                    if (isset($id_blog)) {
                        $array_blog_rewrite[$lang['iso_code']] = $blog_obj->link_rewrite[$lang['id_lang']];
                        if ($config_url_use_id == 0) {
                            $array_config_blog_rewrite[$lang['iso_code']] = $config->get('detail_rewrite_'.$lang['id_lang'], 'detail');
                        }
                    }
                    
                    if (isset($id_category)) {
                        $array_category_rewrite[$lang['iso_code']] = $blog_category_obj->link_rewrite[$lang['id_lang']];
                        if ($config_url_use_id == 0) {
                            $array_config_category_rewrite[$lang['iso_code']] = $config->get('category_rewrite_'.$lang['id_lang'], 'category');
                        }
                    }
                };
                
                Media::addJsDef(array(
                    'array_list_rewrite' => $array_list_rewrite,
                    'array_category_rewrite' => $array_category_rewrite,
                    'array_blog_rewrite' => $array_blog_rewrite,
                    'array_config_category_rewrite' => $array_config_category_rewrite,
                    'array_config_blog_rewrite' => $array_config_blog_rewrite,
                    'config_url_use_id' => $config_url_use_id
                ));
            }
        }
    }

    public function hookLeftColumn()
    {
        $html = '';
        $fc = Tools::getValue('fc');
        $module = Tools::getValue('module');
        
        if ($fc == 'module' && $module =='tdkblog') {
            $html .= $this->leftCategoryBlog();
            $html .= $this->leftPopularBlog();
            $html .= $this->leftRecentBlog();
            $html .= $this->lefTagBlog();
        }
        
        return $html;
    }
    
    public function leftCategoryBlog()
    {
        $html = '';
        
        if (Configuration::get('TDKBLOG_CATEGORY_MENU') && $this->_prepareHook()) {
            $html .= $this->display(__FILE__, 'views/templates/hook/categories_menu.tpl');
        }
        
        return $html;
    }
    
    public function leftPopularBlog()
    {
        $html = '';
        
        $config = TdkBlogConfig::getInstance();
        if ($config->get('show_popular_blog', 0)) {
            $limit = (int)$config->get('limit_popular_blog', 5);
            $helper = TdkBlogHelper::getInstance();
            $image_w = (int)$config->get('listing_leading_img_width', 690);
            $image_h = (int)$config->get('listing_leading_img_height', 300);
            $authors = array();

            $leading_blogs = array();
            if ($limit > 0) {
                $leading_blogs = TdkBlogBlog::getListBlogs(null, $this->context->language->id, 1, $limit, 'hits', 'DESC', array(), true);
            }
            foreach ($leading_blogs as $key => $blog) {
                $blog = TdkBlogHelper::buildBlog($helper, $blog, $image_w, $image_h, $config);
                if ($blog['id_employee']) {
                    if (!isset($authors[$blog['id_employee']])) {
                        # validate module
                        $authors[$blog['id_employee']] = new Employee($blog['id_employee']);
                    }

                    if ($blog['author_name'] != '') {
                        $blog['author'] = $blog['author_name'];
                        $blog['author_link'] = $helper->getBlogAuthorLink($blog['author_name']);
                    } else {
                        $blog['author'] = $authors[$blog['id_employee']]->firstname.' '.$authors[$blog['id_employee']]->lastname;
                        $blog['author_link'] = $helper->getBlogAuthorLink($authors[$blog['id_employee']]->id);
                    }
                } else {
                    $blog['author'] = '';
                    $blog['author_link'] = '';
                }

                $leading_blogs[$key] = $blog;
            }

            $this->smarty->assign('leading_blogs', $leading_blogs);
            $html .= $this->display(__FILE__, 'views/templates/hook/left_popular.tpl');
        }
        
        return $html;
    }
    
    public function leftRecentBlog()
    {
        $html = '';
        
        $config = TdkBlogConfig::getInstance();
        if ($config->get('show_recent_blog', 0)) {
            $limit = (int)$config->get('limit_recent_blog', 5);
            $config = TdkBlogConfig::getInstance();
            $helper = TdkBlogHelper::getInstance();
            $image_w = (int)$config->get('listing_leading_img_width', 690);
            $image_h = (int)$config->get('listing_leading_img_height', 300);
            $authors = array();

            $leading_blogs = array();
            if ($limit > 0) {
                $leading_blogs = TdkBlogBlog::getListBlogs(null, $this->context->language->id, 1, $limit, 'date_add', 'DESC', array(), true);
            }
            foreach ($leading_blogs as $key => $blog) {
                $blog = TdkBlogHelper::buildBlog($helper, $blog, $image_w, $image_h, $config);
                if ($blog['id_employee']) {
                    if (!isset($authors[$blog['id_employee']])) {
                        # validate module
                        $authors[$blog['id_employee']] = new Employee($blog['id_employee']);
                    }

                    if ($blog['author_name'] != '') {
                            $blog['author'] = $blog['author_name'];
                            $blog['author_link'] = $helper->getBlogAuthorLink($blog['author_name']);
                    } else {
                            $blog['author'] = $authors[$blog['id_employee']]->firstname.' '.$authors[$blog['id_employee']]->lastname;
                            $blog['author_link'] = $helper->getBlogAuthorLink($authors[$blog['id_employee']]->id);
                    }
                } else {
                    $blog['author'] = '';
                    $blog['author_link'] = '';
                }

                $leading_blogs[$key] = $blog;
            }

            $this->smarty->assign('leading_blogs', $leading_blogs);
            $html .= $this->display(__FILE__, 'views/templates/hook/left_recent.tpl');
        }
        
        return $html;
    }

    public function lefTagBlog()
    {
        $html = '';
        $helper = TdkBlogHelper::getInstance();
        
        $config = TdkBlogConfig::getInstance();
        if ($config->get('show_all_tags', 0)) {
            $leading_blogs = TdkBlogBlog::getListBlogs(null, $this->context->language->id, 1, 100000, 'date_add', 'DESC', array(), true);

            $tags_temp = array();
            foreach ($leading_blogs as $key => $value) {
                $tags_temp = array_merge($tags_temp, explode(",", $value['tags']));
            }
            // validate module
            unset($key);

            $tags_temp = array_unique($tags_temp);
            $tags = array();
            foreach ($tags_temp as $tag_temp) {
				if ($tag_temp != '') {
					$tags[] = array(
						'name' => $tag_temp,
						'link' => $helper->getBlogTagLink($tag_temp)
					);
				}
            }
			
            $this->smarty->assign('tdkblogtags', $tags);
            $html .= $this->display(__FILE__, 'views/templates/hook/left_tdkblogtags.tpl');
        }
        
        return $html;
    }
    
    /*
      function hookRSS($params)
      {
      if (!$this->isCached('tdkblogrss.tpl', $cacheId)) {
      // Getting data
      $config = TdkBlogConfig::getInstance();
      $title = strval($config->get('rss_title_item', 'RSS FEED'));
      $url = Tools::htmlentitiesutf8('http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__).'modules/tdkblog/rss.php';
      $nb = (int)$config->get('rss_limit_item', 1);
      $cacheId = $this->getCacheId($this->name.'-'.date("YmdH"));
      $rss_links = array();
      if ($url && ($contents = Tools::file_get_contents($url)))
      try
      {
      if (@$src = new XML_Feed_Parser($contents))
      for ($i = 0; $i < ($nb ? $nb : 5); $i++)
      if (@$item = $src->getEntryByOffset($i)) {
      $xmlValues = array();
      foreach (self::$leo_xml_fields as $xmlField)
      $xmlValues[$xmlField] = $item->__get($xmlField);
      $xmlValues['enclosure'] = $item->getEnclosure();
      # Compatibility
      $xmlValues['url'] = $xmlValues['link'];
      $rss_links[] = $xmlValues;
      }
      }
      catch (XML_Feed_Parser_Exception $e) {
      Tools::dieOrLog(sprintf($this->l('Error: invalid RSS feed in "tdkblogrss" module: %s'), $e->getMessage()), false);
      }

      // Display smarty
      $this->smarty->assign(array('title' => ($title ? $title : $this->l('RSS feed')), 'rss_links' => $rss_links));
      }

      return $this->display(__FILE__, 'views/templates/hook/tdkblogrss.tpl', $cacheId);
      }
     */

    protected function getCacheId($name = null)
    {
        $name = ($name ? $name.'|' : '').implode('-', Customer::getGroupsStatic($this->context->customer->id));
        return parent::getCacheId($name);
    }

    public function hookRightcolumn($params)
    {
        return $this->hookLeftColumn($params);
    }

    /**
     * @see Module::uninstall()
     */
    public function uninstall()
    {
        if (parent::uninstall()) {
            $res = true;

            $this->uninstallModuleTab('management');
            $this->uninstallModuleTab('dashboard');
            $this->uninstallModuleTab('categories');
            $this->uninstallModuleTab('blogs');
            $this->uninstallModuleTab('comments');
            $this->uninstallModuleTab('module');
            
            $res &= $this->deleteTables();
            $this->deleteConfiguration();

            return (bool)$res;
        }
        return false;
    }

    public function deleteTables()
    {
        return Db::getInstance()->execute('
            DROP TABLE IF EXISTS
            `'._DB_PREFIX_.'tdkblogcat`,
            `'._DB_PREFIX_.'tdkblogcat_lang`,
            `'._DB_PREFIX_.'tdkblogcat_shop`,
            `'._DB_PREFIX_.'tdkblog_comment`,
            `'._DB_PREFIX_.'tdkblog_blog`,
            `'._DB_PREFIX_.'tdkblog_blog_lang`,
            `'._DB_PREFIX_.'tdkblog_blog_shop`');
    }

    public function deleteConfiguration()
    {
        Configuration::deleteByName('TDKBLOG_CATEGORY_MENU');
        Configuration::deleteByName('TDKBLOG_IMAGE_TYPE');
        
        Configuration::deleteByName('TDKBLOG_DASHBOARD_DEFAULTTAB');
        Configuration::deleteByName('TDKBLOG_CFG_GLOBAL');		
        return true;
    }

    /**
     * Creates tables
     */
    protected function createTables()
    {
        if ($this->_installDataSample()) {
            return true;
        }
        $res = 1;
        include_once(dirname(__FILE__).'/install/install.php');
        return $res;
    }

    private function _installDataSample()
    {
        if (!file_exists(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php')) {
            return false;
        }
        require_once(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php');

        $sample = new TDKDatasample(1);
        if ($sample->processImport($this->name)) {
			//TDK:: fix for case can not install sample when install theme (can not get theme name to find directory folder)
			Configuration::updateValue('TDK_INSTALLED_SAMPLE_TDKBLOG', 1);
			return true;
		} else {
			return false;
		}
    }

    protected function installSample()
    {
        $res = 1;
        include_once(dirname(__FILE__).'/install/sample.php');
        return $res;
    }

//    public function hookDisplayNav($params)
//    {
//        return $this->hookDisplayTop($params);
//    }

    /**
     * Show correct re_write url on BlockLanguage module
     * http://ps_1609_test/vn/index.php?controller=blog?id=9&fc=module&module=tdkblog
     *     $default_rewrite = array(
      '1' => 'http://ps_1609_test/en/blog/lang-en-b9.html',
      '2' => 'http://ps_1609_test/vn/blog/lang-vn-b9.html',
      '3' => 'http://ps_1609_test/cb/blog/lang-cb-b9.html',
      );
     *
     */
    public function hookDisplayBanner()
    {
        if (Module::isEnabled('blocklanguages')) {
            $default_rewrite = array();
            $module = Validate::isModuleName(Tools::getValue('module')) ? Tools::getValue('module') : '';
            $controller = Tools::getValue('controller');
            if ($module == 'tdkblog' && $controller == 'blog' && ($id_blog = (int)Tools::getValue('id'))) {
                $languages = Language::getLanguages(true, $this->context->shop->id);
                if (!count($languages)) {
                    return false;
                }
                $link = new Link();

                foreach ($languages as $lang) {
                    $config = TdkBlogConfig::getInstance();
                    $config->cur_id_lang = $lang['id_lang'];

                    $cur_key = 'link_rewrite'.'_'.Context::getContext()->language->id;
                    $cur_prefix = '/'.$config->cur_prefix_rewrite = $config->get($cur_key, 'blog').'/';

                    $other_key = 'link_rewrite'.'_'.$lang['id_lang'];
                    $other_prefix = '/'.$config->cur_prefix_rewrite = $config->get($other_key, 'blog').'/';

                    $blog = new TdkBlogBlog($id_blog, $lang['id_lang']);
                    $temp_link = $link->getModuleLink($module, $controller, array('id' => $id_blog, 'rewrite' => $blog->link_rewrite), null, $lang['id_lang']);
                    $default_rewrite[$lang['id_lang']] = str_replace($cur_prefix, $other_prefix, $temp_link);
//                    $default_rewrite[$lang['id_lang']] = $link->getModuleLink($module, $controller, array('id'=>$id_blog, 'rewrite'=>$blog->link_rewrite), null, $lang['id_lang']);
                }
            } elseif ($module == 'tdkblog' && $controller == 'category' && ($id_blog = (int)Tools::getValue('id'))) {
                $languages = Language::getLanguages(true, $this->context->shop->id);
                if (!count($languages)) {
                    return false;
                }
                $link = new Link();

                foreach ($languages as $lang) {
                    $config = TdkBlogConfig::getInstance();
                    $config->cur_id_lang = $lang['id_lang'];

                    $cur_key = 'link_rewrite'.'_'.Context::getContext()->language->id;
                    $cur_prefix = '/'.$config->cur_prefix_rewrite = $config->get($cur_key, 'blog').'/';

                    $other_key = 'link_rewrite'.'_'.$lang['id_lang'];
                    $other_prefix = '/'.$config->cur_prefix_rewrite = $config->get($other_key, 'blog').'/';

                    $blog = new Tdkblogcat($id_blog, $lang['id_lang']);
                    $temp_link = $link->getModuleLink($module, $controller, array('id' => $id_blog, 'rewrite' => $blog->link_rewrite), null, $lang['id_lang']);
                    $default_rewrite[$lang['id_lang']] = str_replace($cur_prefix, $other_prefix, $temp_link);
//                    $default_rewrite[$lang['id_lang']] = $link->getModuleLink($module, $controller, array('id'=>$id_blog, 'rewrite'=>$blog->link_rewrite), null, $lang['id_lang']);
                }
            } elseif ($module == 'tdkblog' && $controller == 'list') {
                $languages = Language::getLanguages(true, $this->context->shop->id);
                if (!count($languages)) {
                    return false;
                }
                $link = new Link();

                foreach ($languages as $lang) {
                    $config = TdkBlogConfig::getInstance();
                    $config->cur_id_lang = $lang['id_lang'];

                    $cur_key = 'link_rewrite'.'_'.Context::getContext()->language->id;
                    $cur_prefix = '/'.$config->cur_prefix_rewrite = $config->get($cur_key, 'blog').'';

                    $other_key = 'link_rewrite'.'_'.$lang['id_lang'];
                    $other_prefix = '/'.$config->cur_prefix_rewrite = $config->get($other_key, 'blog').'';

                    $temp_link = $link->getModuleLink($module, $controller, array(), null, $lang['id_lang']);
                    $default_rewrite[$lang['id_lang']] = str_replace($cur_prefix, $other_prefix, $temp_link);
                }
            }

            $this->context->smarty->assign('lang_tdk_rewrite_urls', $default_rewrite);
        }
    }

    /**
     * Hook Display Top
     */
//    public function hookDisplayTop($params)
//    {
//        $params = array();
//        $link = TdkBlogHelper::getInstance()->getFontBlogLink();
//        $config = TdkBlogConfig::getInstance();
//
//        return '<div class="topbar-box"><a href="'.$link.'">'.$config->get('blog_link_title_'.$this->context->language->id, 'Blog').'</a></div>';
//    }

    /**
     * Hook ModuleRoutes
     */
    public function hookModuleRoutes($route = '', $detail = array())
    {
        $config = TdkBlogConfig::getInstance();
        $routes = array();

        $routes['module-tdkblog-list'] = array(
            'controller' => 'list',
            'rule' => _TDK_BLOG_REWRITE_ROUTE_.'.html',
            'keywords' => array(
            ),
            'params' => array(
                'fc' => 'module',
                'module' => 'tdkblog'
            )
        );
        
        if ($config->get('url_use_id', 1)) {
            // URL HAVE ID
            $routes['module-tdkblog-detail'] = array(
                'controller' => 'detail',
                'rule' => _TDK_BLOG_REWRITE_ROUTE_.'/detail/{rewrite}-b{id}.html',
                'keywords' => array(
                    'id' => array('regexp' => '[0-9]+', 'param' => 'id'),
                    'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'tdkblog',
                    
                )
            );

            $routes['module-tdkblog-category'] = array(
                'controller' => 'category',
                'rule' => _TDK_BLOG_REWRITE_ROUTE_.'/category/{rewrite}-c{id}.html',
                'keywords' => array(
                    'id' => array('regexp' => '[0-9]+', 'param' => 'id'),
                    'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'tdkblog',
                            
                )
            );
        } else {
            // REMOVE ID FROM URL
            $category_rewrite = 'category_rewrite'.'_'.Context::getContext()->language->id;
            $category_rewrite = $config->get($category_rewrite, 'category');
            $detail_rewrite = 'detail_rewrite'.'_'.Context::getContext()->language->id;
            $detail_rewrite = $config->get($detail_rewrite, 'detail');

            $routes['module-tdkblog-detail'] = array(
                'controller' => 'blog',
                'rule' => _TDK_BLOG_REWRITE_ROUTE_.'/'.$detail_rewrite.'/{rewrite}.html',
                'keywords' => array(
                    'id' => array('regexp' => '[0-9]+', 'param' => 'id'),
                    'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'tdkblog',
                )
            );

            $routes['module-tdkblog-category'] = array(
                'controller' => 'category',
                'rule' => _TDK_BLOG_REWRITE_ROUTE_.'/'.$category_rewrite.'/{rewrite}.html',
                'keywords' => array(
                    'id' => array('regexp' => '[0-9]+', 'param' => 'id'),
                    'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'tdkblog',
                )
            );
        }
		// echo '<pre>';
		// print_r($routes);die();
        return $routes;
    }

    /**
     * Get lastest blog for TdkPlatform module
     * @param type $params
     * @return type
     */
    public function getBlogsFont($params)
    {
        $config = TdkBlogConfig::getInstance();
        $id_categories = '';
        if (isset($params['chk_cat'])) {
            # validate module
            $id_categories = $params['chk_cat'];
        }
        $order_by = isset($params['order_by']) ? $params['order_by'] : 'id_tdkblog_blog';
        $order_way = isset($params['order_way']) ? $params['order_way'] : 'DESC';
        $helper = TdkBlogHelper::getInstance();
        $limit = (int)$params['nb_blogs'];
        $blogs = TdkBlogBlog::getListBlogsForTdkPlatform($id_categories, $this->context->language->id, $limit, $order_by, $order_way, array(), true);
        $authors = array();
        $image_w = (int)$config->get('listing_leading_img_width', 690);
        $image_h = (int)$config->get('listing_leading_img_height', 300);
        foreach ($blogs as $key => &$blog) {
            $blog = TdkBlogHelper::buildBlog($helper, $blog, $image_w, $image_h, $config, true);
            if ($blog['id_employee']) {
                if (!isset($authors[$blog['id_employee']])) {
                    $authors[$blog['id_employee']] = new Employee($blog['id_employee']);
                }
                $blog['author'] = $authors[$blog['id_employee']]->firstname.' '.$authors[$blog['id_employee']]->lastname;
                $blog['author_link'] = $helper->getBlogAuthorLink($authors[$blog['id_employee']]->id);
            } else {
                $blog['author'] = '';
                $blog['author_link'] = '';
            }
            unset($key); # validate module
        }
        return $blogs;
    }
    /**
     * FIX BUG 1.7.3.3 : install theme lose hook displayHome, displayTdkProfileProduct
     * because ajax not run hookActionAdminBefore();
     */
    public function autoRestoreSampleData()
    {
        if (Hook::isModuleRegisteredOnHook($this, 'actionAdminBefore', (int)Context::getContext()->shop->id)) {
            $theme_manager = new stdclass();
            $theme_manager->theme_manager = 'theme_manager';
            $this->hookActionAdminBefore(array(
                'controller' => $theme_manager,
            ));
        }
    }
    /**
     * Run only one when install/change Theme_of_TDK Studio
     */
    public function hookActionAdminBefore($params)
    {
        $this->unregisterHook('actionAdminBefore');
        if (isset($params) && isset($params['controller']) && isset($params['controller']->theme_manager)) {
            // Validate : call hook from theme_manager
        } else {
            // Other module call this hook -> duplicate data
            return;
        }
        
        # FIX : update Prestashop by 1-Click module -> NOT NEED RESTORE DATABASE
        $tdk_version = Configuration::get('TDK_CURRENT_VERSION');
        if ($tdk_version != false) {
            $ps_version = Configuration::get('PS_VERSION_DB');
            $versionCompare =  version_compare($tdk_version, $ps_version);
            if ($versionCompare != 0) {
                // Just update Prestashop
                Configuration::updateValue('TDK_CURRENT_VERSION', $ps_version);
                return;
            }
        }
        
        
        # WHENE INSTALL THEME, INSERT HOOK FROM DATASAMPLE IN THEME
        $hook_from_theme = false;
        if (file_exists(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php')) {
            require_once(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php');
            $sample = new TDKDatasample();
            if ($sample->processHook($this->name)) {
                $hook_from_theme = true;
            };
        }
        
        # INSERT HOOK FROM MODULE_DATASAMPLE
        if ($hook_from_theme == false) {
            $this->registerTDKHook();
        }
        
        # WHEN INSTALL MODULE, NOT NEED RESTORE DATABASE IN THEME
        $install_module = (int)Configuration::get('TDK_INSTALLED_TDKBLOG', 0);
        if ($install_module) {
            Configuration::updateValue('TDK_INSTALLED_TDKBLOG', '0');    // next : allow restore sample
            //TDK:: fix for case can not install sample when install theme (can not get theme name to find directory folder)
			if ((int)Configuration::get('TDK_INSTALLED_SAMPLE_TDKBLOG', 0)) {				
				return;
			}
        }
        
        # INSERT DATABASE FROM THEME_DATASAMPLE
        if (file_exists(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php')) {
            require_once(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php');
            $sample = new TDKDatasample();
            $sample->processImport($this->name);
        }
    }
    
    /**
     * Common method
     * Resgister all hook for module
     */
    public function registerTDKHook()
    {
        $res = true;
        $res &= $this->registerHook('header');
//        $res &= $this->registerHook('top');
        $res &= $this->registerHook('leftColumn');
        $res &= $this->registerHook('rightColumn');
        $res &= $this->registerHook('moduleRoutes');
        $res &= $this->registerHook('displayBackOfficeHeader');
        # Multishop create new shop
        $res &= $this->registerHook('actionAdminShopControllerSaveAfter');
        $res &= $this->registerHook('actionAdminControllerSetMedia');
        
        return $res;
    }
    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookActionAdminControllerSetMedia()
    {
        $this->autoRestoreSampleData();
    }
    
    public function correctModule()
    {
        //TDK:: check thumb column, if not exist auto add
        if (Db::getInstance()->executeS('SHOW TABLES LIKE \'%tdkblog_blog%\'') && count(Db::getInstance()->executes('SELECT "thumb" FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "'._DB_NAME_.'" AND TABLE_NAME = "'._DB_PREFIX_.'tdkblog_blog" AND COLUMN_NAME = "thumb"'))<1) {
            Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'tdkblog_blog` ADD `thumb` varchar(255) DEFAULT NULL');
        }
        
        //TDK:: check author name column, if not exist auto add
        // Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'tdkblog_blog` ADD `author_name` varchar(255) DEFAULT NULL');
        if (Db::getInstance()->executeS('SHOW TABLES LIKE \'%tdkblog_blog%\'') && count(Db::getInstance()->executes('SELECT "author_name" FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "'._DB_NAME_.'" AND TABLE_NAME = "'._DB_PREFIX_.'tdkblog_blog" AND COLUMN_NAME = "author_name"'))<1) {
            Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'tdkblog_blog` ADD `author_name` varchar(255) DEFAULT NULL');
        }
        
        if (!is_dir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img')) {
            $this->moveImageFolder();
        }
    }
    
    //TDK:: move image folder from module to theme
    public function moveImageFolder()
    {
        //TDK:: copy image from module to theme
        if (!file_exists(_PS_THEME_DIR_.'assets/img/modules/index.php')) {
            @copy(_TDKBLOG_BLOG_IMG_DIR_.'index.php', _PS_THEME_DIR_.'assets/img/modules/index.php');
        }
        if (!is_dir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img')) {
            mkdir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img', 0777, true);
        }
        
        if (!file_exists(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/index.php')) {
            @copy(_TDKBLOG_BLOG_IMG_DIR_.'index.php', _PS_THEME_DIR_.'assets/img/modules/tdkblog/img/index.php');
        }
        
        //if (!is_dir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img')) {
            //mkdir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img', 0777, true);
        //}
        
        //if (!file_exists(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/index.php')) {
            //@copy(_TDKBLOG_BLOG_IMG_DIR_.'index.php', _PS_THEME_DIR_.'assets/img/modules/tdkblog/img/index.php');
        //}
        
        //if (!is_dir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img')) {
            //mkdir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img', 0777, true);
        //}
        
        //if (!file_exists(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/index.php')) {
            //@copy(_TDKBLOG_BLOG_IMG_DIR_.'index.php', _PS_THEME_DIR_.'assets/img/modules/tdkblog/img/index.php');
        //}
        
        if (!is_dir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/sample')) {
            mkdir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/sample', 0777, true);
            
            mkdir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/sample/b', 0777, true);
            mkdir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/sample/c', 0777, true);
            
            if (is_dir(_TDKBLOG_BLOG_IMG_DIR_.'b') && is_dir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/sample/b')) {
                $objects_b = scandir(_TDKBLOG_BLOG_IMG_DIR_.'b');
                $objects_theme_b = scandir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/sample/b');
                if (count($objects_b) > 2 && count($objects_theme_b) <= 2) {
                    foreach ($objects_b as $objects_b_val) {
                        if ($objects_b_val != '.' && $objects_b_val != '..') {
                            if (filetype(_TDKBLOG_BLOG_IMG_DIR_.'b'.'/'.$objects_b_val) == 'file') {
                                @copy(_TDKBLOG_BLOG_IMG_DIR_.'b'.'/'.$objects_b_val, _PS_THEME_DIR_.'assets/img/modules/tdkblog/img/sample/b/'.$objects_b_val);
                            }
                        }
                    }
                }
            }
            
            if (is_dir(_TDKBLOG_BLOG_IMG_DIR_.'c') && is_dir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/sample/c')) {
                $objects_c = scandir(_TDKBLOG_BLOG_IMG_DIR_.'c');
                $objects_theme_c = scandir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/sample/c');
                if (count($objects_c) > 2 && count($objects_theme_c) <= 2) {
                    foreach ($objects_c as $objects_c_val) {
                        if ($objects_c_val != '.' && $objects_c_val != '..') {
                            if (filetype(_TDKBLOG_BLOG_IMG_DIR_.'c'.'/'.$objects_c_val) == 'file') {
                                @copy(_TDKBLOG_BLOG_IMG_DIR_.'c'.'/'.$objects_c_val, _PS_THEME_DIR_.'assets/img/modules/tdkblog/img/sample/c/'.$objects_c_val);
                            }
                        }
                    }
                }
            }
        }
        
        if (!file_exists(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/sample/index.php')) {
            @copy(_TDKBLOG_BLOG_IMG_DIR_.'index.php', _PS_THEME_DIR_.'assets/img/modules/tdkblog/img/sample/index.php');
        }
        
        // if (!is_dir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/b')) {
            // mkdir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/b', 0777, true);
        // }
        
        // if (!file_exists(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/b/index.php')) {
            // @copy(_TDKBLOG_BLOG_IMG_DIR_.'index.php', _PS_THEME_DIR_.'assets/img/modules/tdkblog/img/b/index.php');
        // }
        
        // if (!is_dir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/c')) {
            // mkdir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/c', 0777, true);
        // }
        
        // if (!file_exists(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/c/index.php')) {
            // @copy(_TDKBLOG_BLOG_IMG_DIR_.'index.php', _PS_THEME_DIR_.'assets/img/modules/tdkblog/img/c/index.php');
        // }
        
        //TDK:: get list id_shop from database of blog
        $list_id_shop = Db::getInstance()->executes('SELECT `id_shop` FROM `'._DB_PREFIX_.'tdkblog_blog_shop` GROUP BY `id_shop`');
            
        if (count($list_id_shop) > 0) {
            foreach ($list_id_shop as $list_id_shop_val) {
                if (!is_dir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/'.$list_id_shop_val['id_shop'])) {
                    mkdir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/'.$list_id_shop_val['id_shop'], 0777, true);
                    
                    @copy(_TDKBLOG_BLOG_IMG_DIR_.'index.php', _PS_THEME_DIR_.'assets/img/modules/tdkblog/img/'.$list_id_shop_val['id_shop'].'/index.php');
                    
                    mkdir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/'.$list_id_shop_val['id_shop'].'/b', 0777, true);
                    
                    mkdir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/'.$list_id_shop_val['id_shop'].'/c', 0777, true);
                    
                    if (is_dir(_TDKBLOG_BLOG_IMG_DIR_.'b') && is_dir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/'.$list_id_shop_val['id_shop'].'/b')) {
                        $objects_b = scandir(_TDKBLOG_BLOG_IMG_DIR_.'b');
                        $objects_theme_b = scandir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/'.$list_id_shop_val['id_shop'].'/b');
                        if (count($objects_b) > 2 && count($objects_theme_b) <= 2) {
                            foreach ($objects_b as $objects_b_val) {
                                if ($objects_b_val != '.' && $objects_b_val != '..') {
                                    if (filetype(_TDKBLOG_BLOG_IMG_DIR_.'b'.'/'.$objects_b_val) == 'file') {
                                        @copy(_TDKBLOG_BLOG_IMG_DIR_.'b'.'/'.$objects_b_val, _PS_THEME_DIR_.'assets/img/modules/tdkblog/img/'.$list_id_shop_val['id_shop'].'/b/'.$objects_b_val);
                                    }
                                }
                            }
                        }
                    }
                    
                    if (is_dir(_TDKBLOG_BLOG_IMG_DIR_.'c') && is_dir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/'.$list_id_shop_val['id_shop'].'/c')) {
                        $objects_c = scandir(_TDKBLOG_BLOG_IMG_DIR_.'c');
                        $objects_theme_c = scandir(_PS_THEME_DIR_.'assets/img/modules/tdkblog/img/'.$list_id_shop_val['id_shop'].'/c');
                        if (count($objects_c) > 2 && count($objects_theme_c) <= 2) {
                            foreach ($objects_c as $objects_c_val) {
                                if ($objects_c_val != '.' && $objects_c_val != '..') {
                                    if (filetype(_TDKBLOG_BLOG_IMG_DIR_.'c'.'/'.$objects_c_val) == 'file') {
                                        @copy(_TDKBLOG_BLOG_IMG_DIR_.'c'.'/'.$objects_c_val, _PS_THEME_DIR_.'assets/img/modules/tdkblog/img/'.$list_id_shop_val['id_shop'].'/c/'.$objects_c_val);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * @Action Create new shop, choose theme then auto restore datasample.
     */
    public function hookActionAdminShopControllerSaveAfter($param)
    {
        if (Tools::getIsset('controller') !== false && Tools::getValue('controller') == 'AdminShop'
                && Tools::getIsset('submitAddshop') !== false && Tools::getValue('submitAddshop')
                && Tools::getIsset('theme_name') !== false && Tools::getValue('theme_name')) {
            $shop = $param['return'];
            
            if (file_exists(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php')) {
                require_once(_PS_MODULE_DIR_.'tdkplatform/libs/TDKDataSample.php');
                $sample = new TDKDatasample();
                TdkBlogHelper::$id_shop = $shop->id;
                $sample->_id_shop = $shop->id;
                $sample->processImport('tdkblog');
            }
        }
    }
}
