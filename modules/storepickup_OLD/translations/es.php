<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{storepickup}prestashop>adminpickupslipcontroller_6a654eb684215acbcff90197922ca294'] = 'Fecha recogida';
$_MODULE['<{storepickup}prestashop>store-pickup-details_6a654eb684215acbcff90197922ca294'] = 'Fecha recogida';
$_MODULE['<{storepickup}prestashop>store-pickup_4211e316b9d70e66eeb3fbdb2530a074'] = 'Elige una de nuestras tiendas para recoger tu pedido';
$_MODULE['<{storepickup}prestashop>store-pickup_6a654eb684215acbcff90197922ca294'] = 'Fecha recogida';
$_MODULE['<{storepickup}prestashop>pickup-stores_4211e316b9d70e66eeb3fbdb2530a074'] = 'Elige una de nuestras tiendas para recoger tu pedido';
$_MODULE['<{storepickup}prestashop>stores-box(original)_c0812f7b2ab78e609eaa4f90c279f193'] = 'Nuestras tiendas';
$_MODULE['<{storepickup}prestashop>stores-box_c0812f7b2ab78e609eaa4f90c279f193'] = 'Nuestras tiendas';
$_MODULE['<{storepickup}prestashop>pickup-time_original_6a654eb684215acbcff90197922ca294'] = 'Fecha recogida';
$_MODULE['<{storepickup}prestashop>pickup-time_6a654eb684215acbcff90197922ca294'] = 'Fecha recogida';
