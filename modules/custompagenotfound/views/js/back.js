/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

$(function() {
    $(document).on('change', '#CUSTOMPAGENOTFOUND_CHOICE', checkCustomPageNotFoundChoice);
    $(document).ready(checkCustomPageNotFoundChoice);
    $(document).ready(function () {
        curent_option = [];
        choice = $('#CUSTOMPAGENOTFOUND_CHOICE').val();
        $('#CUSTOMPAGENOTFOUND_TITLE_' + choice).parents('.form-group').slideDown();
        $('#CUSTOMPAGENOTFOUND_CONTENT_' + choice).parents('.form-group').slideDown();
        every_lang_initials = $("[id*=CUSTOMPAGENOTFOUND_TITLE_]:visible").closest('.form-group').find('.btn.btn-default.dropdown-toggle');
        help_title = $('*[id*=CUSTOMPAGENOTFOUND_TITLE_]:visible').closest('.form-group').find('.help-block').html();
        help_content = $('*[id*=CUSTOMPAGENOTFOUND_CONTENT_]:visible').closest('.form-group').find('.help-block').html();
        language = $('*[id*=CUSTOMPAGENOTFOUND_TITLE_]:visible').closest('.form-group').find('.dropdown-menu').html();
        button = '<div class="form-group" style="display: block;"><label class="control-label col-lg-3">'+make+'</label><div class="col-lg-9"><div class="form-group" style="display: block;"><div class="col-lg-9"><div id="triggered" style="padding: 12px 24px; border-radius: 3px; background: #2EACCE; color: white; width: auto; display: inline-block; cursor: pointer;">'+add+'</div></div></div><p class="help-block">'+own_tpl+'</p></div></div>';
        del_button = '<div class="form-group"><label class="control-label col-lg-3">'+del1+'</label><div class="col-lg-9"><div class="form-group" style="display: block;"><div class="col-lg-9"><div class="del_choice" id="del_choice" style="padding: 12px 24px; border-radius: 3px; background: #E08F95; color: white; width: auto; display: inline-block; cursor: pointer;">'+del2+'</div></div></div><p class="help-block">'+del_tpl+'</p></div></div>';
        $('.form-wrapper > .form-group:nth-child(3)').after(button);
        $('#triggered').parents('.form-group').last().after(del_button);
        variable = '0';
        $(document).on('click', '#triggered', function (event) {
            if(variable == '0'){
                event.preventDefault();
                $('.hide_away').parents('.form-group').slideUp();
                input_title = [];
                textarea_context = [];
                for (i = 0; i < id_lang.length; i++) {
                    if(every_lang_initials.length > 0){
                        lang_initials = every_lang_initials[i];
                        lang_initials = lang_initials.textContent;
                        lang = '<div class="col-lg-2 open"><button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">'+lang_initials+'<i class="icon-caret-down"></i></button><ul class="dropdown-menu"><li></li>' + language + '</ul></div>'
                        col = 'col-lg-9';
                        translate = 'translatable-field lang-' + id_lang[i];
                    } else {
                        lang = '';
                        col = 'col-lg-12';
                        translate = '';
                    }
                    input_title.push('<div class="'+ translate + '"><div class='+col+'><input id="CUSTOMPAGENOTFOUND_TITLE_' + latest_option + '_' + id_lang[i] + '" name="CUSTOMPAGENOTFOUND_TITLE_' + latest_option + '_' + id_lang[i] + '" class="hide_away" onkeyup="if (isArrowKey(event)) return ;updateFriendlyURL();" required="required" placeholder="Name your template" type="text"></div>'+ lang +'</div>');
                    textarea_context.push('<div class="form-group ' + translate + '" style="display: block;"><div class='+col+'><textarea id="CUSTOMPAGENOTFOUND_CONTENT_' + latest_option + '_' + id_lang[i] + '" name="CUSTOMPAGENOTFOUND_CONTENT_' + latest_option + '_' + id_lang[i] + '" class="rte autoload_rte hide_away"></textarea></div>'+ lang +'</div>');
                }
                input_title = input_title.join('');
                textarea_context = textarea_context.join('');
                copy_title = '<div class="form-group" style="display: block;"><label class="control-label col-lg-3 required">Template Name</label><div class="col-lg-9"><div class="form-group" style="display: block;">' + input_title + '</div><p class="help-block">' + help_title + '</p></div></div>';
                copy_content = '<div class="form-group" style="display: block;"><label class="control-label col-lg-3">Content</label><div class="col-lg-9">' + textarea_context + '<p class="help-block">' + help_content + '</p></div></div>';
                $('.form-wrapper').append(copy_title);
                $('.form-wrapper').append(copy_content);
                hideOtherLanguage(1);
                $('#CUSTOMPAGENOTFOUND_CHOICE option:last-child').after('<option value="'+latest_option+'" selected="selected">----</option>');
                if(!$(document).find('#CUSTOMPAGENOTFOUND_NEWIDS').length) {
                    $(this).closest('form').append('<input name="CUSTOMPAGENOTFOUND_NEWIDS" id="CUSTOMPAGENOTFOUND_NEWIDS" value="" />');
                }
                var newIdsJson = $('#CUSTOMPAGENOTFOUND_NEWIDS').val();
                if(!newIdsJson) {
                    newIdsJson = [];
                }
                else {
                    newIdsJson = $.parseJSON(newIdsJson);
                }
                newIdsJson.push(latest_option);
                $(document).find('#CUSTOMPAGENOTFOUND_NEWIDS').val(JSON.stringify(newIdsJson)).attr('value', JSON.stringify(newIdsJson));
                $('#CUSTOMPAGENOTFOUND_NEWIDS').hide();
                curent_option = latest_option;
                variable = '1';
                latest_option++;
                checkCustomPageNotFoundChoice();
            }
        });
        $('#del_choice').click(removeTemplate);
    });
    function removeTemplate() {
        choice = $('#CUSTOMPAGENOTFOUND_CHOICE').val();
        var option = $('#CUSTOMPAGENOTFOUND_CHOICE').find('option:selected');
        if(option.next().is('option')) {
            next = option.next().val();
        }
        else if(option.prev().is('option')) {
            next = option.prev().val();
        }
        else {
            next = 0;
        }
        var answer =  confirm(custompagenotfound_confirmation_of_delete_message);
        if(answer === true) {
            $.ajax({
                data: {action: 'deleteTemplate', id_template: choice},
                type: 'post',
                success: function (raspuns) {
                    if (raspuns == 'fault') {
                        alert("We're sorry, an error has occurred, while trying to delete.");
                    } else {
                        $("#CUSTOMPAGENOTFOUND_CHOICE option[value='" + choice + "']").remove();
                        $('#CUSTOMPAGENOTFOUND_CHOICE').val(next);
                        $("input[name*='CUSTOMPAGENOTFOUND_TITLE_" + choice + "']").parents('.form-group').next('.form-group').remove();
                        $("input[name*='CUSTOMPAGENOTFOUND_TITLE_" + choice + "']").parents('.form-group').remove();

                        checkCustomPageNotFoundChoice();
                    }
                }
            });
            $('<div class="bootstrap"><div class="module_confirmation conf confirm alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>'+custompagenotfound_delete_confirmation_message+'</div></div>').insertBefore('#module_form')
        }
    }
    function checkCustomPageNotFoundChoice() {
        $('.hide_away').parents('.form-group').slideUp();
        choice = $('#CUSTOMPAGENOTFOUND_CHOICE').val();
        var intervalCheckTinyMCE = setInterval(function () {
            if(typeof tinyMCE !== 'undefined' && !version17){
                clearInterval(intervalCheckTinyMCE);
                $('#CUSTOMPAGENOTFOUND_TITLE_'+choice+'_'+id_lang[0]).parents('.form-group').slideDown();
                $('#CUSTOMPAGENOTFOUND_CONTENT_'+choice+'_'+id_lang[0]).parents('.form-group').slideDown();
                for (i = 0; i < id_lang.length; i++) {
                    if ($('#CUSTOMPAGENOTFOUND_CONTENT_' + choice + '_' + id_lang[i]).parent().find('.mce-tinymce').length == 0) {
                        tinyMCE.init({
                            selector: '#CUSTOMPAGENOTFOUND_CONTENT_' + choice + "_" + id_lang[i],
                            plugins: "code,pagebreak,style,layer,table,save,hr,image,link,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
                            mode: "textarea",
                            toolbar1: "code,colorpicker,bold,italic,underline,strikethrough,blockquote,link,align,bullist,numlist,table,image,media,formatselect",
                            toolbar2: ""
                        });
                    }
                }
            } else if (typeof tinyMCE !== 'undefined' && version17) {
                clearInterval(intervalCheckTinyMCE);
                $('#CUSTOMPAGENOTFOUND_TITLE_'+choice+'_'+id_lang[0]).parents('.form-group').slideDown();
                $('#CUSTOMPAGENOTFOUND_CONTENT_'+choice+'_'+id_lang[0]).parents('.form-group').slideDown();
                for (i = 0; i < id_lang.length; i++) {
                    if ($('#CUSTOMPAGENOTFOUND_CONTENT_' + choice + '_' + id_lang[i]).parent().find('.mce-tinymce').length == 0) {
                        tinyMCE.init({
                            selector: '#CUSTOMPAGENOTFOUND_CONTENT_' + choice + "_" + id_lang[i],
                            plugins: "align colorpicker link image filemanager table media placeholder advlist code table autoresize",
                            browser_spellcheck: true,
                            toolbar1: "code,colorpicker,bold,italic,underline,strikethrough,blockquote,link,align,bullist,numlist,table,image,media,formatselect",
                            toolbar2: "",
                            external_filemanager_path: baseAdminDir + "filemanager/",
                            filemanager_title: "File manager",
                            external_plugins: {"filemanager": baseAdminDir + "filemanager/plugin.min.js"},
                            language: iso_user,
                            skin: "prestashop",
                            menubar: false,
                            statusbar: false,
                            relative_urls: false,
                            convert_urls: false,
                            entity_encoding: "raw",
                            extended_valid_elements: "em[class|name|id],@[role|data-*|aria-*]",
                            valid_children: "+*[*]",
                            valid_elements: "*[*]",
                            init_instance_callback: "changeToMaterial",
                            rel_list:[
                                { title: 'nofollow', value: 'nofollow' }
                            ]
                        });
                    }
                }
            }
        },100);
    }
});