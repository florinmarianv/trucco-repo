{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file='page.tpl'}

{block name="page_content"}
       <div class="pagenotfound custompagenotfound template{$id_template|escape:'htmlall':'UTF-8'}">
        {$content nofilter} {* We can’t escape this because it contains HTML *}
        {if $search}
            <form action="{$link->getPageLink('search')|escape:'html':'UTF-8'}" method="post" class="std">
                <fieldset>
                    <p class="text-center">
                        <input id="search_query" name="search_query" placeholder="{l s='Search our product catalog:' mod='custompagenotfound' }" type="text" class="form-control grey" /><button type="submit" name="Submit" value="OK" class="btn btn-default button button-small"><span>{l s='Ok' mod='custompagenotfound'}</span></button>
                    </p>
                </fieldset>
            </form>

            <div class="buttons"><a class="btn btn-default button button-medium" href="{$urls.base_url|escape:'htmlall':'UTF-8'}" title="{l s='Home' mod='custompagenotfound' }"><span><i class="icon-chevron-left left"></i>{l s='Home page' mod='custompagenotfound'}</span></a></div>
        {/if}
    </div>

{/block}
