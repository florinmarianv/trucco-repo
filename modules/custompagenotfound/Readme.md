# Custom 404 Page PRO

Instalation: 
The instalation of the module is according to PrestaShop's default behavior.
Log into Prestashop Admin Panel and go to Back Office -> Modules -> Content Management Modules and install “Custom 404 Page PRO” module.


Configuration:
I. Select “YES” to enable the module or “No” in order to disable it.
II. Select “YES” to activate a search bar on the 404 Page or “No” to disable it.
III. Select one of the predefined templates.
IV. Create your own template.
V. Delete the current template.
VI. Hit “Save” and you are good to go.