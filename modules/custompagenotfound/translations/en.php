<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_41f2974f2bc2c78412d6b136b4f69190'] = 'Custom Page Not Found';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_558b49eb972c3c62a1a4e5b6d91420ae'] = 'Configuration saved.	';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_59bcb45702b6f5456b167b2f3751da94'] = 'Please fill the title of the template, in all languages!	';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_b94b88db510c5ecea6334673998d23a8'] = 'The template has been successfully deleted.	';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_62797f59056598119601b25b148eccd1'] = 'Are you certain you wish you delete current template?	';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_f4f70727dc34561dfde1a3c529b6205c'] = 'Settings';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Active';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_d01017498f1f864af81e858fcca09d50'] = 'Activate or deactivate the module	';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Enabled';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_b9f5c797ebbf55adccdd8539a65a0241'] = 'Disabled';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_fb94c35ecf5a44be4b5fab9eb7924185'] = 'Do you want a search bar in the 404 page ?	';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_c6c3cccf45d19c2a2921dcf1b63b83b5'] = 'Activate or deactivate the search bar in 404 page	';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_912f1f3558456013c6677e97ac638dc6'] = 'Method of creating a 404 Page	';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_6b4d074d6d2b845edc2d84a0da47de47'] = 'Choose a method	';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_c9cc8cce247e49bae79f15173ce97354'] = 'Save';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_0c1ec80465825af171a2cb9df19ac8d7'] = 'Template Name	';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_cae1d7a1c45b8a0dc6233e55e2580a06'] = 'Content of custom page	';
$_MODULE['<{custompagenotfound}prestashop>custompagenotfound_f15c1cae7882448b3fb0404682e17e61'] = 'Content';
