<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Custompagenotfound extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'custompagenotfound';
        $this->tab = 'front_office_features';
        $this->version = '1.0.4';
        $this->author = 'Active Design';
        $this->need_instance = 0;
        $this->module_key = 'c4918ae1a194f75a4e175bbab38b696a';
        if (Tools::getValue('action') == 'deleteTemplate') {
            $this->removeTemplate((int)Tools::getValue('id_template'));
        }
        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Custom Page Not Found');
        $this->description = $this->l('Create your own 404/Page not found with a variety of customization options, 
        and a few base pages to choose from.');
    }

    /**
    * Don't forget to create update methods if needed:
    * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
    */
    public function install()
    {
        Configuration::updateValue('CUSTOMPAGENOTFOUND_LIVE_MODE', false);

        include(dirname(__FILE__).'/sql/install.php');

        $languages = Language::getLanguages();
        foreach ($languages as $lang) {
            Db::getInstance()->insert('custompagenotfound', array(
                'content' => '<h2 class="bigblack text-center"><img src="'._PS_BASE_URL_SSL_.__PS_BASE_URI__
                    .'/modules/custompagenotfound/views/img/404.png" alt="" width="417" height="205"/></h2>
                    <h2 class="bigblack text-center">Oops...looks like you&rsquo;ve got lost.</h2>
                    <p class="midgray text-center">Get back Home by clicking on Homepage button 
                      <span class="red">or</span> Seach for something on our website
                    </p>',
                'id_shop' => (int)$this->context->shop->id,
                'id_lang' => (int)$lang['id_lang'],
                'id_template' => '1',
                'template_name' => 'Template1'
            ));
        }
        foreach ($languages as $lang) {
            Db::getInstance()->insert('custompagenotfound', array(
                'content' => '<h2 class="bigblack text-center"><img src="'._PS_BASE_URL_SSL_.__PS_BASE_URI__
                    .'/modules/custompagenotfound/views/img/404_2.png" alt="" width="417" height="205"/></h2>
                    <h2 class="bigblack text-center">Oops...looks like you&rsquo;ve got lost.</h2>
                    <p class="midgray text-center">Get back Home by clicking on Homepage button 
                      <span class="blue">or</span> Seach for something on our website
                    </p>',
                'id_shop' => (int)$this->context->shop->id,
                'id_lang' => (int)$lang['id_lang'],
                'id_template' => '2',
                'template_name' => 'Template2'
            ));
        }

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        include(dirname(__FILE__).'/sql/uninstall.php');

        Configuration::deleteByName('CUSTOMPAGENOTFOUND_LIVE_MODE');

        return parent::uninstall();
    }
    /**
    * Load the configuration form
    */
    public function getContent()
    {

        $output = "";
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitCustompagenotfoundModule')) == true) {
            if ($this->postProcess()) {
                $output = $this->displayConfirmation($this->l('Configuration saved.'));
            } else {
                $output = $this->displayError($this->l('Please fill the title of the template, in all languages!'));
            }
        }

        $this->context->smarty->assign('module_dir', $this->_path);

//        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        Media::addJsDef(array(
            'custompagenotfound_delete_confirmation_message' => $this->l('The template has been successfully deleted.'),
            'custompagenotfound_confirmation_of_delete_message' => $this->l('Are you certain you wish you delete current template?'),
        ));

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitCustompagenotfoundModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
    * Create the structure of your form.
    */

    protected function getConfigForm()
    {
        $options = Db::getInstance()->executeS('SELECT `id_template`, `template_name` FROM `' . _DB_PREFIX_
            . 'custompagenotfound` WHERE `id_lang` = "'.(int)$this->context->language->id.'"');
        $custompage = array(
            'form' => array(
                'legend' => array(
                  'title' => $this->l('Settings'),
                  'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Active'),
                        'name' => 'CUSTOMPAGENOTFOUND_ACTIVE',
                        'is_bool' => true,
                        'desc' => $this->l('Activate or deactivate the module'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Do you want a search bar in the 404 page ?'),
                        'name' => 'CUSTOMPAGENOTFOUND_SEARCH',
                        'is_bool' => true,
                        'desc' => $this->l('Activate or deactivate the search bar in 404 page'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Method of creating a 404 Page'),
                        'name' => 'CUSTOMPAGENOTFOUND_CHOICE',
                        'desc' => $this->l('Choose a method'),
                        'required' => true,
                        'options' => array(
                            'query' => $options,
                            'id' => 'id_template',
                            'name' => 'template_name'
                        )
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
        $latest_options = array();
        foreach ($options as $option) {
            $latest_options[] = $option['id_template'];
            $custompage['form']['input'][] = array(
                'type' => 'text',
                'label' => $this->l('Template Name'),
                'lang' => true,
                'class' => 'hide_away',
                'required' => true,
                'desc' => $this->l('Create and name your template'),
                'name' => 'CUSTOMPAGENOTFOUND_TITLE_'.$option['id_template'],
                'placeholder' => 'Name your template'
            );
            $custompage['form']['input'][] = array(
                'col' => 9,
                'type' => 'textarea',
                'class' => 'hide_away',
                'lang' => true,
                'desc' => $this->l('Content of custom page'),
                'name' => 'CUSTOMPAGENOTFOUND_CONTENT_'.$option['id_template'],
                'label' => $this->l('Content'),
                'autoload_rte' => (Configuration::get('CUSTOMPAGENOTFOUND_CHOICE') == $option['id_template'])
                    ? true : false,
            );
        }
        $latest_option = max($latest_options)+1;
        Media::addJSDef(array('latest_option' => $latest_option));
        Media::addJSDef(array('basePath' => $this->basePath()));
        Media::addJSDef(array('modPath'=> $this->getPath()));
        Media::addJsDef(array('baseAdminDir' => _PS_BASE_URL_SSL_.__PS_BASE_URI__.basename(_PS_ADMIN_DIR_).'/'));
        Media::addJsDef(array('version17' => Tools::version_compare(_PS_VERSION_, '1.7', '>=')));
        return $custompage;
    }
    /**
     * Set values for the inputs.
     */
    public function removeTemplate($id)
    {
        $column_to_delete = 'DELETE FROM `'._DB_PREFIX_.'custompagenotfound` WHERE `id_template` ="'. (int)$id .'" ';
        if (!Db::getInstance()->execute($column_to_delete)) {
            echo json_encode('fault');
        } else {
            echo json_encode('success');
        }
    }
    protected function getConfigFormValues()
    {
        $languages = Language::getLanguages();
        $id_lang = array();
        $CUSTOMPAGENOTFOUND_CONTENT = array();
        $options = Db::getInstance()->executeS(
            'SELECT DISTINCT `id_template` FROM `' . _DB_PREFIX_ . 'custompagenotfound` '
        );
        $allContent = array();
        if (Tools::getValue('CUSTOMPAGENOTFOUND_NEWIDS')
            && $newids = Tools::jsonDecode(Tools::getValue('CUSTOMPAGENOTFOUND_NEWIDS'))) {
            $newoptions = array();
            foreach ($newids as $newid) {
                $newoptions[] = array('id_template' => $newid);
            }
            $options = array_merge($options, $newoptions);
        }
        foreach ($languages as $lang) {
            $id_lang[] = $lang['id_lang'];
            $CUSTOMPAGENOTFOUND_CONTENT[(int)$lang['id_lang']] = Db::getInstance()->getValue('SELECT `content` FROM `'
                . _DB_PREFIX_ . 'custompagenotfound` WHERE `id_lang`="' . (int)$lang['id_lang']
                . '" AND `id_shop`="' . (int)$this->context->shop->id . '"');
            foreach ($options as $option) {
                $allContent['CUSTOMPAGENOTFOUND_CONTENT_' . (int)$option['id_template']][(int)$lang['id_lang']]
                = Db::getInstance()->getValue('SELECT `content` FROM `'
                . _DB_PREFIX_ . 'custompagenotfound` WHERE `id_lang` = "' . (int)$lang['id_lang']
                . '" AND `id_template`="' . (int)$option['id_template']
                . '" AND `id_shop` = "' . (int)$this->context->shop->id . '"');
                $allContent['CUSTOMPAGENOTFOUND_TITLE_'
                . (int)$option['id_template']][(int)$lang['id_lang']] = Db::getInstance()->getValue('SELECT 
                `template_name` FROM `' . _DB_PREFIX_
                    . 'custompagenotfound` WHERE `id_lang` = "' . (int)$lang['id_lang']
                    . '" AND `id_template`="' . (int)$option['id_template'] . '" AND `id_shop` = "'
                    . (int)$this->context->shop->id . '"');
            }
        }
        Media::addJSDef(array('id_lang' => $id_lang));
        Media::addJsDefL('testjs', "ok");
        return array_merge($allContent, array(
        'CUSTOMPAGENOTFOUND_CHOICE' => Configuration::get('CUSTOMPAGENOTFOUND_CHOICE'),
        'CUSTOMPAGENOTFOUND_ACTIVE' => Configuration::get('CUSTOMPAGENOTFOUND_ACTIVE'),
        'CUSTOMPAGENOTFOUND_SEARCH' => Configuration::get('CUSTOMPAGENOTFOUND_SEARCH'),
        'CUSTOMPAGENOTFOUND_TEST' => Configuration::get('CUSTOMPAGENOTFOUND_TEST'),
        'CUSTOMPAGENOTFOUND_CONTENT' => $CUSTOMPAGENOTFOUND_CONTENT
        ));
    }
    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $languages = Language::getLanguages();
        $form_values = $this->getConfigFormValues();
        $options = Db::getInstance()->executeS('SELECT `id_template`, `template_name` FROM `' . _DB_PREFIX_
            . 'custompagenotfound` WHERE `id_lang` = "'.(int) $this->context->language->id.'"');


        foreach ($options as $option) {
            $id = $option['id_template'];
            foreach ($languages as $lang) {
                $id_lang = $lang['id_lang'];
                if (empty(Tools::getValue('CUSTOMPAGENOTFOUND_TITLE_' . $id . '_' . $id_lang))) {
                    return false;
                }
            }
        }


        foreach (array_keys($form_values) as $key) {
            if (strpos($key, 'CUSTOMPAGENOTFOUND_CONTENT_') !== false) {
                $data_array = array();
                $id_template = str_replace('CUSTOMPAGENOTFOUND_CONTENT_', '', $key);
                foreach ($languages as $lang) {
                    $data_array[(int)$lang['id_lang']] = Tools::getValue($key . "_" . (int)$lang['id_lang']);
                    $data_array = str_replace('src="../', 'src="'
                        . _PS_BASE_URL_SSL_ . __PS_BASE_URI__, $data_array);
                    Db::getInstance()->insert('custompagenotfound', array(
                        'content' => pSQL($this->filterVar($data_array[(int)$lang['id_lang']]), true),
                        'id_shop' => (int)$this->context->shop->id,
                        'id_lang' => (int)$lang['id_lang'],
                        'id_template' => (int)$id_template,
                        'template_name' => Tools::getValue(str_replace('CONTENT_', 'TITLE_', $key) . "_"
                            . (int)$lang['id_lang']),
                    ), false, true, Db::REPLACE);
                }
            } else {
                Configuration::updateValue($key, Tools::getValue($key), true);
            }
        }
        return true;
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name || Tools::getValue('configure') == $this->name) {
            Media::addJsDef(array(
                'make' => $this->l('Make your own template'),
                'add' => $this->l('Add a blanc template'),
                'del1' => $this->l('Delete current template'),
                'del2' => $this->l('Delete template'),
                'own_tpl' => $this->l('Do you want to make your own template ?'),
                'del_tpl' => $this->l('Do you wish to delete the current template'),
                ''
            ));
            $this->context->controller->addJquery();
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }
    public function getPath()
    {
        return dirname(__FILE__);
    }
    public function basePath()
    {
        return _PS_BASE_URL_SSL_.__PS_BASE_URI__;
    }

    public function hookDisplayOverrideTemplate($args)
    {
        if ($args['controller']->php_self == 'pagenotfound') {
            if (Configuration::get('CUSTOMPAGENOTFOUND_ACTIVE')) {
                $this->context->smarty->assign('search', Configuration::get('CUSTOMPAGENOTFOUND_SEARCH'));
                $this->context->smarty->assign('id_template', Configuration::get('CUSTOMPAGENOTFOUND_CHOICE'));

                $page_content = Db::getInstance()->getValue(
                    'SELECT `content` FROM `' . _DB_PREFIX_ .
                    'custompagenotfound` WHERE `id_lang`="' . (int)$this->context->language->id.
                    '" AND `id_shop`="' . (int)$this->context->shop->id.
                    '" AND `id_template`="'.
                    Configuration::get('CUSTOMPAGENOTFOUND_CHOICE').
                    '"'
                );
                $page_content = str_replace(
                    'src="../img/cms/',
                    'src="'._PS_BASE_URL_SSL_ .__PS_BASE_URI__.'/img/cms/',
                    $page_content
                );
                $page_content = str_replace(
                    '{$form_link}',
                    $this->context->link->getPageLink('search'),
                    $page_content
                );
                $this->context->smarty->assign('content', $page_content);
                return dirname(__FILE__).'/views/templates/front/customtemplate_ps17.tpl';
            }
        }
    }
    public function filterVar($value)
    {
        if (version_compare(_PS_VERSION_, '1.6.0.7', '>=') === true) {
            return Tools::purifyHTML($value);
        } else {
            return filter_var($value, FILTER_SANITIZE_STRING);
        }
    }
}
