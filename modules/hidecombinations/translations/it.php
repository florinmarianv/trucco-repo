<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{hideproduct}prestashop>hideproduct_b879f71a3c250cc5f677700f923752d0'] = 'Nascondi prodotti avanzati (stock, prezzo, geolocalizzazione, ...)';
$_MODULE['<{hideproduct}prestashop>hideproduct_66c901fa858d4fcc6c10807c7ce08830'] = 'Nascondi i tuoi prodotti del catalogo in modo massiccio con un\'assoluta flessibilità e condizioni configurabili (per prezzo, per magazzino, geolocalizzazione, per produttore e altro ...)';
$_MODULE['<{hideproduct}prestashop>hideproduct_0ac1aeb2429db494dd42ad2dc219ca7e'] = 'Nascondi prodotti';
$_MODULE['<{hideproduct}prestashop>hideproduct_b8daa6e225eb3981416a81733ecc0902'] = 'Nascondi la gestione dei prodotti';
$_MODULE['<{hideproduct}prestashop>hideproduct_4f9f3d9d09ad4e023f44b383efc18173'] = 'Si prega di definire una percentuale corretta';
$_MODULE['<{hideproduct}prestashop>hideproduct_865d445b9b6c7328c5b084d18ca12440'] = 'È necessario disattivare l\'opzione Disattiva moduli non nativi a ADVANCED PARAMETRI - PERFORMANCE';
$_MODULE['<{hideproduct}prestashop>hideproduct_bf7a16aca06691e8cd188caf1a048043'] = 'È necessario disattivare l\'opzione Disattiva tutte le modifiche a parametri avanzati - PERFORMANCE';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d3b206d196cd6be3a2764c1fb90b200f'] = 'Elimina selezionato';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_e25f0ecd41211b01c83e5fec41df4fe7'] = 'Eliminare elementi selezionati?';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_49ee3087348e8d44e1feda1917443987'] = 'Nome';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a1fa27779242b4902f7ae3bdd5c6d508'] = 'Tipo';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_86a9463f9649c9bf8f0029aa1177d2ec'] = '301 Reindirizzamento';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_9f684976737d09ae52510ec3e1bc2c0e'] = '302 Reindirizzamento';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_2a3f0a7774a47a64ff20f30b0220bb00'] = '404 Reindirizzamento';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_ad7c0b064ee06fbb8d92192435ed00e1'] = 'Modalità di reindirizzamento';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_981ca5079156596d00cfcb96b5a291c4'] = 'Categoria di default del prodotto';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Categoria';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_deb10517653c255364175796ace3553f'] = 'Prodotto';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_c7da501f54544eba6787960200d9efdb'] = 'CMS';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Fabbricante';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_ec136b444eede3bc85639fac0dd06229'] = 'Fornitore';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_cfe6e34a4c7f24aad32aa4299562f5b1'] = 'Home Page';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7f82cf6868d89ca72d670092caedfb86'] = 'Calo dei prezzi';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_63ee7a1d92dd1005e671bcaefadd8899'] = 'Valore di reindirizzamento';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_c7fad630db68971f75d415a62b2b8343'] = 'Categoria (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4fb35c949818d51088a2f1536401ec2b'] = 'Prodotti (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_502996d9790340c5fd7b86a5b93b1c9f'] = 'Priorità';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3ac705f2acd51a4613f9188c05c91d0d'] = 'Valido';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Abilitato';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_2b3ea594ac6d9932911e48070712dbc7'] = 'È necessario selezionare un negozio, se si desidera creare una nuova configurazione.';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7575efc133b1cb9083cc3b8321001c47'] = 'Configurazione salvata correttamente.';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_0f0c014bcfe255c6749d8e665ff8bb4e'] = 'Tradurre questo modulo';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7ae33e3f3709df22542073a88e649ae9'] = 'Gestisci la configurazione di Hideproduct';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_e29e4307142375c118d3a075a20cb596'] = 'Configurazione %s';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_6ce7074e2ac875f22d6cde390aa86a26'] = 'Modifica della configurazione %s';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_243c69c47b454241f354f085ff859ab0'] = 'Nascondi la configurazione del prodotto:';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_0557fa923dcee4d0f86b1409f5c2167f'] = 'Indietro';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_03c2e7e41ffc181a4e84080b4710e81e'] = 'Nuovo';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4d1c8263ba1036754f8db14a98f9f006'] = 'Ricaricare';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_6f091458bea6be967354a13e2e378fa5'] = 'Regole di esportazione';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_deccbe4e9083c3b5f7cd2632722765bb'] = 'Tradurre';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_53103fcc4656f55c219b600ded3c7438'] = 'Gestire ganci';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_89eb0c5108e63de11f57ca56a69b1a3c'] = 'Nascondi la configurazione dei prodotti';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Attivo';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_b9f5c797ebbf55adccdd8539a65a0241'] = 'Disabilitato';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_fd64b18fa383a9ff975259a2bdfb56f7'] = 'Attivare o disattivare questa configurazione';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_6252c0f2c2ed83b7b06dfca86d4650bb'] = 'I caratteri non validi:';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_89a486a8c2cda3308fcc9f478a8d0843'] = 'Reindirizzamento di prodotti nascosti tramite URL';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7bc873cba11f035df692c3549366c722'] = '- Scegli -';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a2a0f8144f3c1f517030709150b23fb0'] = 'Seleziona come vuoi nascondere il Prodotto';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_6eb239cb4d42b0c33ec2d81ac73d44a6'] = 'Reindirizza la modalità di destinazione';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_27842fba85efadc744fd46148bb022ec'] = 'Seleziona il punto in cui desideri reindirizzare l\'URL dei prodotti nascosti: Categoria, CMS, Prodotto, Produttore, Fornitore, Pagina iniziale, Abbandono prezzi, Categoria predefinita prodotto';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3b6eac307b543c2664914699370e9c00'] = 'Reindirizza il valore di destinazione';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3496dc670f97b1db46b2490412336bc3'] = 'Seleziona il valore dell\'URL di destinazione per i prodotti nascosti';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4c81de8442f29ac782c4aefde93e8284'] = 'Impostare la priorità quando 2 o più configurazioni sovrapposizioni. Configurazione con meno numero qui avrà più la priorità';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4777bbe3c06c7939e97101b4589ba1eb'] = 'Data da';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_8ff36b39339cd715ec4af342e5793819'] = 'Data a partire dalla quale la regola è valida. È possibile utilizzare ore, minuti e secons. Esempio: 2016/10/27 è considerato 2016/10/27 00:00:00 e significa che la regola è valida dal 2016/10/27 00:00:00';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d24e4fa285c1eab355817ed6c38ec1bd'] = 'Alla data';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_dc5c81e9bbc6fc605128bf8325e35941'] = 'Data alla quale la regola è valida. È possibile utilizzare ore, minuti e secons. Esempio: 2016/10/27 è considerato 2016/10/27 00:00:00 e significa che la regola è valida fino al 2016/10/26 23:59:59';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_25f7347236b51a5eae4ba01a7862cb55'] = 'Programma';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4ab3acda0f9433cb711f2e2ae04b1cfa'] = 'Selezionare i giorni della settimana e le ore per mostrare applicare la regola (Fare clic sulla casella per abilitare o disabilitare il giorno e definire l\'intervallo di tempo)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_c9cc8cce247e49bae79f15173ce97354'] = 'Salvare';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a88db2ebec89ac6c4e4057145a439526'] = 'Filtri del prodotto (cosa sarà nascosto)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_2706aed48c704a874e49609884aff8a3'] = 'Filtra per prezzi';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_be9ed3f8929411965749d9a66b4289fa'] = 'Abilita o Disabilita il filtro in base all\'intervallo di prezzi. Ad esempio: Appy la regola per i prodotti con prezzo al dettaglio con tasse tra 10 € (minimo) e 50 € (massimo)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d2358c588726a6386139d73cd0e49960'] = 'Prezzo da calcolare il minimo e il massimo';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_9cf2ff232b692ce08ca666ad4358753d'] = 'Selezionare il prezzo che verrà calcolato la soglia. Per esempio: Appy la regola per i prodotti che hanno un prezzo di vendita al dettaglio con prezzi tasse tra i 10 € (minimo) e 50 € (massimo)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_f877b71d9aac0ec56bcac23758029d71'] = 'Prezzo minimo di soglia';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_21874dd9dbbd3bdd38b652bf2f768448'] = 'Prezzo minimo per impostare un prezzo inferiore di soglia (per valuta di default con i %s) per applicare la regola. Se questo valore è 0 non c\'è limite minimo';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a2b544e8022bd4f36d8c6c144b7278dd'] = 'Tasse incluse';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_192a7985becf71aeaaed94de744727c8'] = 'Senza tasse';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_0857b1f91e1c5d29de6c0df193f112e8'] = 'Soglia prezzo massimo';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7301b7c9cd0f581d6c56e9a349a48805'] = 'Prezzo massimo per impostare un prezzo di soglia superiore (per valuta di default con i %s) per applicare la regola. Se questo valore è 0 non c\'è limite massimo';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_66c0a916a2cdf47ece008da51a7920d0'] = 'Filtra per stock';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_695085a560cac3a066de5b2bf2390455'] = 'Abilita o disabilita il filtro per intervallo di stock. Ad esempio: Appy la regola per i prodotti con quantità di stock tra 50 e 100';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_fcb3111cb12f7a4dcc9556d9e526ddcf'] = 'Prodotti con stock da';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7e6cef22574cf9f425443849d6eed7ce'] = 'Abilita la regola per i prodotti con stock da';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a36c02f8f140869ae85fcb1df273df17'] = 'Prodotti con riserva fino a';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_75edcc9eac089d6684ec87c54a3a1090'] = 'Abilita la regola sui prodotti con magazzino fino a';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_9eb1e7ae3a4119336a910d26f2b322d7'] = 'Filtra per peso';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_59599a08317ee7826dbf64547733d3bd'] = 'Abilita o disabilita il filtro per intervallo di peso. Ad esempio: Appy la regola per i prodotti con un peso tra 0 e 6';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_c42a71989c3e82359f995fab0830456c'] = 'Prodotti con peso da';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_1003e4436c4a5a670bd99c0f42c47fb9'] = 'Abilita la regola per i prodotti con peso da';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7c68e6c1740bab9514c2f8b16328e6c4'] = 'Prodotti con peso fino al';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3243faea26b13d3d595729cd87f04b1a'] = 'Abilita la regola per i prodotti con peso fino al';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_c3a93c1aeb02a5926e93612a8914dd55'] = 'Seleziona una categoria (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7b8d35ea7e29241af92d98284cd9b8d3'] = 'Selezionare la categoria (es) in cui verrà applicata la regola. Se non si seleziona alcun valore, la regola sarà applicata a tutte le categorie';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_367dc297aa4ea170d5313a9bc9e0041e'] = 'Seleziona prodotto (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_80570b7f8596ca904fb6082f22f18216'] = 'Selezionare il prodotto (s) in cui verrà applicata la regola. Se non si seleziona alcun valore, la regola verrà applicata a tutti i prodotti';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_8cad2e3c5400b580e8f6a1bc3dcdc3a2'] = 'Seleziona marca (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_31607415ab85aa5c0e4fac73a6ce94b7'] = 'Selezionare il produttore (s) in cui verrà applicata la regola. Se non si seleziona alcun valore, la regola sarà applicata a tutti i produttori';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_f2e0e6db3f9776720cc23d5bfbd0107e'] = 'Selezionare fornitore (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_091e954d35ac504a5208ce9920cbc4bc'] = 'Selezionare il fornitore (s) in cui verrà applicata la regola. Se non si seleziona alcun valore, la regola sarà applicata a tutti i fornitori';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_e0626222614bdee31951d84c64e5e9ff'] = 'Selezionare';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3f2107fb3fd5dad1fc86a4d5c577a119'] = 'Seleziona il ';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_95cb13c86d004a619541913aa8c64d04'] = 'Applicare questa configurazione';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_b1dc590dfd992a1d960a343676a98283'] = 'Filtri di destinazione (che non vedranno i prodotti)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d61ebaead470d53666d9dcb36ebf5ae3'] = 'Selezionare Gruppo cliente (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_5f0b2256821c43eb8ad960f48f68206b'] = 'Selezionare il gruppo di clienti (s) in cui verrà applicata la regola. Se non si seleziona alcun valore, la regola sarà applicata a tutti i gruppi';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7382dca931ae452e6c9d15a299076b0b'] = 'Selezionare Valuta (es)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_252ac6556b0457b4639d841df97a5852'] = 'Valuta (es) per applicare questa configurazione';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_9bbe89b4835e6c446e6c4ce27776b5e3'] = 'Select Language (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3b4e55e8680446e790e1be39146bc4b2'] = 'Selezionare la lingua (s) in cui verrà applicata la regola. Se non si seleziona alcun valore, la regola verrà applicata a tutte le lingue';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_91d31ef84100ef2b1252538b82902e05'] = 'Selezionare Zone (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_8e878a37ccd0b7968bf98cb15574bcfa'] = 'Selezionare la zona (s) in cui verrà applicata la regola. Se non si seleziona alcun valore, la regola sarà applicata a tutte le zone';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_362b5d66b02da7c70f50639e2f55aa15'] = 'Selezionare Paese (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_ad7fc1ad073ac5f5f5de7424334a067c'] = 'Selezionare il Paese (s) in cui verrà applicata la regola. Se non si seleziona alcun valore, la regola sarà applicata a tutti i Paesi';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_579f30655ed3a8198dd518ffd050128a'] = 'Selezionare cliente (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_1abd4d3174cf10f54d09576932434a2a'] = 'Selezionare il cliente (s) in cui verrà applicata la regola. Se non si seleziona alcun valore, la regola sarà applicata a tutti i Clienti';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_108490afa8365531d5eade07c3cc38f4'] = 'Impostazioni globali';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_e69c87235eeb366627dc1d1ac3e912a7'] = 'Usa filtro prodotti';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_f3335bf4290f23fb33f4b667231c74ed'] = 'Abilita se si desidera utilizzare le funzionalità. Disabilitare se non è necessario creare regole per prodotti';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_93cba07454f06a4a960172bbd6e2a435'] = 'Sì';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_dc964a5ca71daced13c168c38ef0a58e'] = 'Usa filtro clienti';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a0af6b2abc6e29eca4aae34938e1a81f'] = 'Abilita se si desidera utilizzare gli attributi. Disabilita se non avrai bisogno di creare regole da parte dei clienti';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d3849d7eb702f01e093b50fcfa6ae432'] = 'Usa filtro funzioni';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_19bf9f650e9c481c5e85601ddd378ab2'] = 'Abilita se si desidera utilizzare le funzionalità. Disabilitare se non è necessario creare regole per caratteristiche';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d793ec2fb7a42c38f41a53f3e6929857'] = 'Usa filtro attributi';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d485c65fe23ad4976e6b0ba98a0815bb'] = 'Abilita se si desidera utilizzare gli attributi. Disabilita se non avrai bisogno di creare regole per attributi';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_cb415aca26a3bb1450d9947d55fd1772'] = 'Regola #% s -% s duplicata correttamente.';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a9969870318db4078ff95abcb8e74181'] = 'Eliminare l\'elemento selezionato? ';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_83958c6c56c5385fc05a88fe8a3c6207'] = 'Prezzo all\'ingrosso senza tasse';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_8fa7d262d97388681beb8c54940dcff4'] = 'Prezzo al dettaglio Senza Tasse';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_2217674710faeca0cbf24b064efced1c'] = 'Prezzo all\'ingrosso con le tasse';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_8de0534c2c18e1dc450015a5d527a2d2'] = 'Prezzo di vendita al dettaglio con le tasse';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a58f5096169d7b297ee787c534bd3a8d'] = 'Prezzo del fornitore senza tasse';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_56f76a7f141428b92bdd88f54a99a085'] = 'Prezzo del fornitore con tasse';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4ed266b57b00cd1e1a31d3f5262b79b6'] = 'Vantaggi margine senza tasse (Retail - Wholesale)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7e94cf61da34a94fd52419c8084e3dd5'] = 'Margine di vantaggi con le tasse (Retail - Wholesale)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'Tutti';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_cedb5d4dec286e30ec3c47203c6ad63b'] = '... E molto altro ancora';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4c70b2412fa1d5928dc95e30e006a598'] = 'Nome campo non può essere vuoto.';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_ba73cf25fe7ffe13ccb33fe5b9ea9c67'] = 'Non valido \"Data Dal\" formato';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_364768ce1863f336496e7917cd63a036'] = 'Non valido \"data su\" formato';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_1e581338bd3dc1520b3056de7f4bf7aa'] = 'Hideproduct Configs';
$_MODULE['<{hideproduct}prestashop>admin_form_254f642527b45bc260048e30704edb39'] = 'Configurazione';
$_MODULE['<{hideproduct}prestashop>admin_form_c876ba2c5b2416966ddfe9011914b807'] = 'Vi offriamo assistenza gratuita per installare e configurare il modulo. Se avete qualche problema, è possibile:';
$_MODULE['<{hideproduct}prestashop>admin_form_1189ecb65a86791a9ff18f1baf4f86d0'] = 'Contattaci attraverso o al sito web';
$_MODULE['<{hideproduct}prestashop>admin_form_f081ea2a0aa29bcd6fb059c521424172'] = 'Inviaci una e-mail a';
$_MODULE['<{hideproduct}prestashop>admin_form_de7b9dce6dc9ab364f7b21dd5315d26f'] = 'Contatto idnovate.com';
$_MODULE['<{hideproduct}prestashop>admin_form_d8d2123f39f646dd4de62a088c45c6b0'] = 'Ci sono degli errori:';
$_MODULE['<{hideproduct}prestashop>admin_form_c888438d14855d7d96a2724ee9c306bd'] = 'Impostazioni aggiornate';
$_MODULE['<{hideproduct}prestashop>admin_form_b04f2c8a90cfc485fc990dc8dc4ba293'] = 'Configurazione moduli';
$_MODULE['<{hideproduct}prestashop>admin_form_1452867ae0d604d1e0f9065c0343d301'] = 'Definire un valore positivo per impostare un incremento';
$_MODULE['<{hideproduct}prestashop>admin_form_c8e3750479ef6c05a7b8832d034c0a69'] = 'Definire un valore negativo per impostare uno sconto';
$_MODULE['<{hideproduct}prestashop>admin_form_b17f3f4dcf653a5776792498a9b44d6a'] = 'Aggiorna impostazioni';
$_MODULE['<{hideproduct}prestashop>admin_translations_4c41e0bd957698b58100a5c687d757d9'] = 'Seleziona tutto';
$_MODULE['<{hideproduct}prestashop>admin_translations_52e3ce2bc983012661c3c11fe8b0f8ce'] = 'Deselezionare tutto';
$_MODULE['<{hideproduct}prestashop>admin_translations_ef7de3f485174ff47f061ad27d83d0ee'] = 'Selezionato';
$_MODULE['<{hideproduct}prestashop>admin_translations_ca208b5e20eb22648fb1be3d902576fb'] = 'Non ci sono opzioni selezionate (regola applicata a tutti i valori)';
$_MODULE['<{hideproduct}prestashop>admin_translations_475f57eb7a864e3f2fff65966e2466cb'] = 'Opzione selezionata (regola applicata solo a questo valore)';
$_MODULE['<{hideproduct}prestashop>admin_translations_a9429c48fc6d3fb08d1fd8435f6a6c1f'] = 'Opzioni selezionate (regola applicata solo a questi valori)';
$_MODULE['<{hideproduct}prestashop>admin_translations_f453e0c33edd79653febd0b9bc8f09b3'] = 'Nessun articolo presente';
$_MODULE['<{hideproduct}prestashop>admin_translations_db2db2fdfc1b8608682670e8a0a1684e'] = 'Opzioni disponibili';
$_MODULE['<{hideproduct}prestashop>admin_translations_831c9b214fe06e0b57e63837da6c561e'] = 'Nessuna opzione trovato';
$_MODULE['<{hideproduct}prestashop>admin_translations_d3b137efc88460b21895cb20cd6ba962'] = 'Opzione trovati';
$_MODULE['<{hideproduct}prestashop>admin_translations_91b582efb3418bb4db6a46719114883f'] = 'Opzioni trovati';
$_MODULE['<{hideproduct}prestashop>admin_translations_b4805a9af05d82889758dda70003cfc6'] = 'Opzioni di ricerca';
$_MODULE['<{hideproduct}prestashop>admin_translations_2dea6d11e06eb2ee8bfa24c4280376f3'] = 'Comprimi Gruppo';
$_MODULE['<{hideproduct}prestashop>admin_translations_51974f60c60e3934dcbe987471efb341'] = 'Espandere Gruppo';
$_MODULE['<{hideproduct}prestashop>admin_translations_6a3bc7712f60d5188b38787590a219d2'] = 'Seleziona tutti i gruppi';
$_MODULE['<{hideproduct}prestashop>admin_translations_c3316be4d878e8eecabcde4836537ffe'] = 'Deseleziona tutto Group';
$_MODULE['<{hideproduct}prestashop>admin_warnings_c1c62f1fc0065109915f257b187efdf3'] = 'Clicca sul link per andare modificare la configurazione negozio:';
$_MODULE['<{hideproduct}prestashop>admin_warnings_9446a98ad14416153cc4d45ab8b531bf'] = 'Prestazione';
$_MODULE['<{hideproduct}prestashop>schedule_426ffc82ce1cd9b670e80bd1610080a9'] = '\'Lunedì\',\'Martedì\',\'Mercoledì\',\'Giovedì\',\'Venerdì\',\'Sabato\',\'Domenica\'';
