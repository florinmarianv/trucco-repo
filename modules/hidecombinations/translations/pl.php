<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{hideproduct}prestashop>hideproduct_b879f71a3c250cc5f677700f923752d0'] = 'Zaawansowane Ukryj produkty (czas, cena, geolokalizacja, ...)';
$_MODULE['<{hideproduct}prestashop>hideproduct_66c901fa858d4fcc6c10807c7ce08830'] = 'Ukryj produkty z katalogu masowo dzięki absolutnej elastyczności i konfigurowalnym warunkom (pod względem ceny, czasu, geolokalizacji, według producenta itd.)';
$_MODULE['<{hideproduct}prestashop>hideproduct_0ac1aeb2429db494dd42ad2dc219ca7e'] = 'Ukryj produkty';
$_MODULE['<{hideproduct}prestashop>hideproduct_b8daa6e225eb3981416a81733ecc0902'] = 'Ukryj zarządzanie produktami';
$_MODULE['<{hideproduct}prestashop>hideproduct_4f9f3d9d09ad4e023f44b383efc18173'] = 'Proszę określić prawidłową wartość procentową';
$_MODULE['<{hideproduct}prestashop>hideproduct_865d445b9b6c7328c5b084d18ca12440'] = 'Musisz ustawić opcję \'Wyłącz wszystkie nadpisywania\' na NIE';
$_MODULE['<{hideproduct}prestashop>hideproduct_bf7a16aca06691e8cd188caf1a048043'] = 'Musisz ustawić opcję \'Wyłącz wszystkie nadpisywania\' na NIE';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d3b206d196cd6be3a2764c1fb90b200f'] = 'Usuń wybrane';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_e25f0ecd41211b01c83e5fec41df4fe7'] = 'Usuń zaznaczone elementy?';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_49ee3087348e8d44e1feda1917443987'] = 'Nazwa';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a1fa27779242b4902f7ae3bdd5c6d508'] = 'Rodzaj';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_86a9463f9649c9bf8f0029aa1177d2ec'] = '301 Przekierowanie';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_9f684976737d09ae52510ec3e1bc2c0e'] = '302 Przekierowanie';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_2a3f0a7774a47a64ff20f30b0220bb00'] = '404 Przekierowanie';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_ad7c0b064ee06fbb8d92192435ed00e1'] = 'Tryb przekierowania';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_981ca5079156596d00cfcb96b5a291c4'] = 'Domyślna kategoria produktu';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Kategoria';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_deb10517653c255364175796ace3553f'] = 'Produkt';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_c7da501f54544eba6787960200d9efdb'] = 'CMS';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Producent';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_ec136b444eede3bc85639fac0dd06229'] = 'Dostawca';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_cfe6e34a4c7f24aad32aa4299562f5b1'] = 'Strona główna';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7f82cf6868d89ca72d670092caedfb86'] = 'Spadek cen';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_63ee7a1d92dd1005e671bcaefadd8899'] = 'Wartość przekierowania';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_c7fad630db68971f75d415a62b2b8343'] = 'Kategorii (e)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4fb35c949818d51088a2f1536401ec2b'] = 'Produkty (a)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_502996d9790340c5fd7b86a5b93b1c9f'] = 'Priorytet';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3ac705f2acd51a4613f9188c05c91d0d'] = 'Ważny';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Włączone';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_2b3ea594ac6d9932911e48070712dbc7'] = 'Musisz wybrać sklep, jeśli chcesz utworzyć nową konfigurację.';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7575efc133b1cb9083cc3b8321001c47'] = 'Konfiguracja została pomyślnie zapisana.';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_0f0c014bcfe255c6749d8e665ff8bb4e'] = 'Przetłumacz ten moduł';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7ae33e3f3709df22542073a88e649ae9'] = 'Zarządzaj konfiguracją produktu Hideproduct';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_e29e4307142375c118d3a075a20cb596'] = 'Konfiguracja %s';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_6ce7074e2ac875f22d6cde390aa86a26'] = 'Konfiguracja Montaż %s';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_243c69c47b454241f354f085ff859ab0'] = 'Ukryj konfigurację produktu:';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_0557fa923dcee4d0f86b1409f5c2167f'] = 'Plecy';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_03c2e7e41ffc181a4e84080b4710e81e'] = 'Nowy';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4d1c8263ba1036754f8db14a98f9f006'] = 'Przeładować';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_6f091458bea6be967354a13e2e378fa5'] = 'Zasady eksportu';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_deccbe4e9083c3b5f7cd2632722765bb'] = 'Tłumaczyć';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_53103fcc4656f55c219b600ded3c7438'] = 'Zarządzaj haki';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_89eb0c5108e63de11f57ca56a69b1a3c'] = 'Ukryj konfigurację produktów';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Aktywny';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_b9f5c797ebbf55adccdd8539a65a0241'] = 'Niepełnosprawny';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_fd64b18fa383a9ff975259a2bdfb56f7'] = 'Włączyć lub wyłączyć tę konfigurację';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_6252c0f2c2ed83b7b06dfca86d4650bb'] = 'Nieprawidłowe znaki:';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_89a486a8c2cda3308fcc9f478a8d0843'] = 'Przekierowanie produktu ukrytego URL';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7bc873cba11f035df692c3549366c722'] = '- Wybierz -';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a2a0f8144f3c1f517030709150b23fb0'] = 'Wybierz, w jaki sposób chcesz ukryć produkt';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_6eb239cb4d42b0c33ec2d81ac73d44a6'] = 'Przekieruj docelowy tryb';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_27842fba85efadc744fd46148bb022ec'] = 'Wybierz, gdzie chcesz przekierować adres URL produktów: kategoria, CMS, produkt, producent, dostawca, strona główna, ceny spadają, domyślna kategoria produktu';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3b6eac307b543c2664914699370e9c00'] = 'Przekieruj docelową wartość';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3496dc670f97b1db46b2490412336bc3'] = 'Wybierz wartość docelowego adresu URL do ukrytych produktów';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4c81de8442f29ac782c4aefde93e8284'] = 'Ustaw priorytet przy 2 lub więcej konfiguracje nachodzi. Konfiguracja przy mniejszej liczbie tutaj będą miały wyższy priorytet';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4777bbe3c06c7939e97101b4589ba1eb'] = 'Data, od';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_8ff36b39339cd715ec4af342e5793819'] = 'Data, od której obowiązuje reguła. Można użyć godziny, minuty i secons. Przykład: 27.10.2016 jest uważany 2016-10-27 00:00:00 a to oznacza, że ​​reguła jest ważna od 2016-10-27 00:00:00';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d24e4fa285c1eab355817ed6c38ec1bd'] = 'Data Aby';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_dc5c81e9bbc6fc605128bf8325e35941'] = 'Data, do której reguła jest ważna. Można użyć godziny, minuty i secons. Przykład: 27.10.2016 jest uważany 2016-10-27 00:00:00 a to oznacza, że ​​reguła jest ważny do 2016-10-26 23:59:59';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_25f7347236b51a5eae4ba01a7862cb55'] = 'Harmonogram';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4ab3acda0f9433cb711f2e2ae04b1cfa'] = 'Wybierz dni tygodnia i godziny do pokazania Zastosuj regułę (Kliknij pole, aby włączyć lub wyłączyć dzień i określić zakres czasu)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_c9cc8cce247e49bae79f15173ce97354'] = 'Zapisać';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a88db2ebec89ac6c4e4057145a439526'] = 'Filtry produktów (co będzie ukryte)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_2706aed48c704a874e49609884aff8a3'] = 'Filtruj według cen';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_be9ed3f8929411965749d9a66b4289fa'] = 'Włącz lub wyłącz filtr według zakresu cen. Na przykład: Zastosuj regułę do produktów o cenie detalicznej z podatkami od 10 € (minimum) do 50 € (maksymalna)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d2358c588726a6386139d73cd0e49960'] = 'Cena od obliczenia minimum i maksimum';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_9cf2ff232b692ce08ca666ad4358753d'] = 'Wybierz cenę, która zostanie obliczony próg. Na przykład: można było zastosować regułę do produktów, które mają ceny detalicznej z podatkami ceny od 10 zł (minimum) i 50 zł (maksymalna)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_f877b71d9aac0ec56bcac23758029d71'] = 'Próg cena minimalna';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_21874dd9dbbd3bdd38b652bf2f768448'] = 'Minimalna cena ustawić mniejszą cenę progową (domyślnie waluty  %s), aby zastosować regułę. Jeśli wartość ta wynosi 0 nie ma minimalna granica';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a2b544e8022bd4f36d8c6c144b7278dd'] = 'Podatki wliczone';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_192a7985becf71aeaaed94de744727c8'] = 'Bez podatków';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_0857b1f91e1c5d29de6c0df193f112e8'] = 'Próg cena maksymalna';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7301b7c9cd0f581d6c56e9a349a48805'] = 'Maksymalna cena ustawić maksymalną cenę progową (domyślnie waluty  %s), aby zastosować regułę. Jeśli wartość ta wynosi 0 nie ma maksymalny limit';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_66c0a916a2cdf47ece008da51a7920d0'] = 'Filtruj według magazynu';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_695085a560cac3a066de5b2bf2390455'] = 'Włącz lub wyłącz filtr według zakresu zapasów. Na przykład: załóż regułę do produktów o zapasu od 50 do 100';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_fcb3111cb12f7a4dcc9556d9e526ddcf'] = 'Produkty z zapasem od';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7e6cef22574cf9f425443849d6eed7ce'] = 'Włącz regułę dla produktów z zapasem z';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a36c02f8f140869ae85fcb1df273df17'] = 'Produkty z zapasem do';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_75edcc9eac089d6684ec87c54a3a1090'] = 'Włącz regułę dla produktów z zapasem do';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_9eb1e7ae3a4119336a910d26f2b322d7'] = 'Filtruj według wagi';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_59599a08317ee7826dbf64547733d3bd'] = 'Włącz lub wyłącz filtr według zakresu wag. Na przykład: przystaw regułę do produktów o wadze od 0 do 6';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_c42a71989c3e82359f995fab0830456c'] = 'Produkty o wadze od';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_1003e4436c4a5a670bd99c0f42c47fb9'] = 'Włącz regułę dla produktów o wadze od';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7c68e6c1740bab9514c2f8b16328e6c4'] = 'Produkty o wadze do';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3243faea26b13d3d595729cd87f04b1a'] = 'Włącz regułę dla produktów o wadze do';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_c3a93c1aeb02a5926e93612a8914dd55'] = 'Wybierz kategorię (e)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7b8d35ea7e29241af92d98284cd9b8d3'] = 'Wybierz kategorię (y), gdzie będzie stosowana reguła. Jeśli nie wybierzesz żadnej wartości, reguła będzie stosowana do wszystkich kategoriach';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_367dc297aa4ea170d5313a9bc9e0041e'] = 'Wybierz produkt (y)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_80570b7f8596ca904fb6082f22f18216'] = 'Wybierz produkt (y), gdzie będzie stosowana reguła. Jeśli nie wybierzesz żadnej wartości, reguła będzie stosowana do wszystkich produktów';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_8cad2e3c5400b580e8f6a1bc3dcdc3a2'] = 'Wybierz producenta (y)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_31607415ab85aa5c0e4fac73a6ce94b7'] = 'Wybierz producenta (-ów), gdzie będzie stosowana reguła. Jeśli nie wybierzesz żadnej wartości, reguła zostanie zastosowana do wszystkich producentów';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_f2e0e6db3f9776720cc23d5bfbd0107e'] = 'Wybierz Dostawca (y)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_091e954d35ac504a5208ce9920cbc4bc'] = 'Wybierz dostawcy (-ów), gdzie będzie stosowana reguła. Jeśli nie wybierzesz żadnej wartości, reguła będzie stosowana do wszystkich dostawców';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_e0626222614bdee31951d84c64e5e9ff'] = 'Wybierz';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3f2107fb3fd5dad1fc86a4d5c577a119'] = 'Wybierz ';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_95cb13c86d004a619541913aa8c64d04'] = 'Zastosowanie tej konfiguracji';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_b1dc590dfd992a1d960a343676a98283'] = 'Filtry docelowe (kto nie zobaczy produktów)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d61ebaead470d53666d9dcb36ebf5ae3'] = 'Wybierz grupę klienta (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_5f0b2256821c43eb8ad960f48f68206b'] = 'Wybierz grupę (y) Klienta, gdzie zostanie zastosowana reguła. Jeśli nie wybierzesz żadnej wartości, reguła będzie stosowana do wszystkich grup';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7382dca931ae452e6c9d15a299076b0b'] = 'Wybierz Waluta (es)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_252ac6556b0457b4639d841df97a5852'] = 'Waluta (-y) w celu zastosowania tej konfiguracji';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_9bbe89b4835e6c446e6c4ce27776b5e3'] = 'Wybierz język (e)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_3b4e55e8680446e790e1be39146bc4b2'] = 'Wybierz Język (i), gdzie zostanie zastosowana reguła. Jeśli nie wybierzesz żadnej wartości, reguła będzie stosowana do wszystkich językach';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_91d31ef84100ef2b1252538b82902e05'] = 'Wybierz Zone (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_8e878a37ccd0b7968bf98cb15574bcfa'] = 'Wybierz strefę (y), gdzie będzie stosowana reguła. Jeśli nie wybierzesz żadnej wartości, reguła będzie stosowana do wszystkich stref';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_362b5d66b02da7c70f50639e2f55aa15'] = 'Wybierz kraj (e)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_ad7fc1ad073ac5f5f5de7424334a067c'] = 'Wybierz kraj (y), gdzie będzie stosowana reguła. Jeśli nie wybierzesz żadnej wartości, reguła będzie stosowana do wszystkich krajów';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_579f30655ed3a8198dd518ffd050128a'] = 'Wybierz klienta (s)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_1abd4d3174cf10f54d09576932434a2a'] = 'Wybierz klienta (y), gdzie będzie stosowana reguła. Jeśli nie wybierzesz żadnej wartości, reguła będzie stosowana do wszystkich Klientów';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_108490afa8365531d5eade07c3cc38f4'] = 'Ustawienia ogólne';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_e69c87235eeb366627dc1d1ac3e912a7'] = 'Użyj filtra produktów';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_f3335bf4290f23fb33f4b667231c74ed'] = 'Włącz, jeśli chcesz korzystać z funkcji. Wyłącz, jeśli nie musisz tworzyć reguł według produktów';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_93cba07454f06a4a960172bbd6e2a435'] = 'Tak';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Nie';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_dc964a5ca71daced13c168c38ef0a58e'] = 'Użyj filtra klientów';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a0af6b2abc6e29eca4aae34938e1a81f'] = 'Włącz, jeśli chcesz korzystać z atrybutów. Wyłącz, jeśli nie będziesz musiał tworzyć reguł przez klientów';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d3849d7eb702f01e093b50fcfa6ae432'] = 'Użyj filtra funkcji';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_19bf9f650e9c481c5e85601ddd378ab2'] = 'Włącz, jeśli chcesz korzystać z funkcji. Wyłącz, jeśli nie musisz tworzyć reguł według funkcji';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d793ec2fb7a42c38f41a53f3e6929857'] = 'Użyj filtra atrybutów';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_d485c65fe23ad4976e6b0ba98a0815bb'] = 'Włącz, jeśli chcesz korzystać z atrybutów. Wyłącz, jeśli nie potrzebujesz tworzyć reguł według atrybutów';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_cb415aca26a3bb1450d9947d55fd1772'] = 'Reguła #% s -% s została pomyślnie zduplikowana.';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a9969870318db4078ff95abcb8e74181'] = 'Usuń wybrany element? ';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_f2a6c498fb90ee345d997f888fce3b18'] = 'Kasować';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_83958c6c56c5385fc05a88fe8a3c6207'] = 'Hurtownie Cena bez podatków';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_8fa7d262d97388681beb8c54940dcff4'] = 'Cena detaliczna bez podatków';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_2217674710faeca0cbf24b064efced1c'] = 'Cen hurtowych z podatkami';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_8de0534c2c18e1dc450015a5d527a2d2'] = 'Cena detaliczna z podatkami';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_a58f5096169d7b297ee787c534bd3a8d'] = 'Cena dostawcy bez podatków';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_56f76a7f141428b92bdd88f54a99a085'] = 'Cena dostawcy z podatkami';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4ed266b57b00cd1e1a31d3f5262b79b6'] = 'Margines Korzyści bez podatków (Detal - Hurt)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_7e94cf61da34a94fd52419c8084e3dd5'] = 'Margines Korzyści z podatków (Detal - Hurt)';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'Wszystko';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_cedb5d4dec286e30ec3c47203c6ad63b'] = '...i więcej';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_4c70b2412fa1d5928dc95e30e006a598'] = 'Nazwa Pole nie może być puste.';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_ba73cf25fe7ffe13ccb33fe5b9ea9c67'] = 'Nieprawidłowy \"Data od\" formatu';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_364768ce1863f336496e7917cd63a036'] = 'Nieprawidłowy \"Data do\" formatu';
$_MODULE['<{hideproduct}prestashop>adminhideproductcontroller_1e581338bd3dc1520b3056de7f4bf7aa'] = 'Hideproduct Configs';
$_MODULE['<{hideproduct}prestashop>admin_form_254f642527b45bc260048e30704edb39'] = 'Konfiguracja';
$_MODULE['<{hideproduct}prestashop>admin_form_c876ba2c5b2416966ddfe9011914b807'] = 'Oferujemy bezpłatną pomoc w instalacji i konfiguracji modułu. Jeśli masz jakiekolwiek problemy można:';
$_MODULE['<{hideproduct}prestashop>admin_form_1189ecb65a86791a9ff18f1baf4f86d0'] = 'Skontaktuj się z nami za pośrednictwem strony internetowej lub';
$_MODULE['<{hideproduct}prestashop>admin_form_f081ea2a0aa29bcd6fb059c521424172'] = 'Wyślij do nas maila';
$_MODULE['<{hideproduct}prestashop>admin_form_de7b9dce6dc9ab364f7b21dd5315d26f'] = 'Kontakt z idnovate.com';
$_MODULE['<{hideproduct}prestashop>admin_form_d8d2123f39f646dd4de62a088c45c6b0'] = 'Istnieją błędy:';
$_MODULE['<{hideproduct}prestashop>admin_form_c888438d14855d7d96a2724ee9c306bd'] = 'Ustawienia zaktualizowane';
$_MODULE['<{hideproduct}prestashop>admin_form_b04f2c8a90cfc485fc990dc8dc4ba293'] = 'Konfiguracja modułu';
$_MODULE['<{hideproduct}prestashop>admin_form_1452867ae0d604d1e0f9065c0343d301'] = 'Określ wartość dodatnią, aby ustawić przyrost';
$_MODULE['<{hideproduct}prestashop>admin_form_c8e3750479ef6c05a7b8832d034c0a69'] = 'Określ wartość ujemną, aby ustawić zniżkę';
$_MODULE['<{hideproduct}prestashop>admin_form_b17f3f4dcf653a5776792498a9b44d6a'] = 'Ustawienia aktualizacji';
$_MODULE['<{hideproduct}prestashop>admin_translations_4c41e0bd957698b58100a5c687d757d9'] = 'Zaznacz wszystko';
$_MODULE['<{hideproduct}prestashop>admin_translations_52e3ce2bc983012661c3c11fe8b0f8ce'] = 'Odznacz wszystkie';
$_MODULE['<{hideproduct}prestashop>admin_translations_ef7de3f485174ff47f061ad27d83d0ee'] = 'Wybrany';
$_MODULE['<{hideproduct}prestashop>admin_translations_ca208b5e20eb22648fb1be3d902576fb'] = 'Nie wybrano opcje (zasada stosuje się do wszystkich wartości)';
$_MODULE['<{hideproduct}prestashop>admin_translations_475f57eb7a864e3f2fff65966e2466cb'] = 'Wybrana opcja (zasada stosowana tylko do tej wartości)';
$_MODULE['<{hideproduct}prestashop>admin_translations_a9429c48fc6d3fb08d1fd8435f6a6c1f'] = 'Wybrane opcje (zasada stosowana tylko do tych wartości)';
$_MODULE['<{hideproduct}prestashop>admin_translations_f453e0c33edd79653febd0b9bc8f09b3'] = 'Brak dostępnych produktów';
$_MODULE['<{hideproduct}prestashop>admin_translations_db2db2fdfc1b8608682670e8a0a1684e'] = 'Opcje dostępne';
$_MODULE['<{hideproduct}prestashop>admin_translations_831c9b214fe06e0b57e63837da6c561e'] = 'Nie znaleziono opcje';
$_MODULE['<{hideproduct}prestashop>admin_translations_d3b137efc88460b21895cb20cd6ba962'] = 'Opcja znaleziono';
$_MODULE['<{hideproduct}prestashop>admin_translations_91b582efb3418bb4db6a46719114883f'] = 'Znaleziono opcje';
$_MODULE['<{hideproduct}prestashop>admin_translations_b4805a9af05d82889758dda70003cfc6'] = 'Opcje wyszukiwania';
$_MODULE['<{hideproduct}prestashop>admin_translations_2dea6d11e06eb2ee8bfa24c4280376f3'] = 'Zwinąć Grupa';
$_MODULE['<{hideproduct}prestashop>admin_translations_51974f60c60e3934dcbe987471efb341'] = 'Rozwiń grupę';
$_MODULE['<{hideproduct}prestashop>admin_translations_6a3bc7712f60d5188b38787590a219d2'] = 'Zaznacz całą grupę';
$_MODULE['<{hideproduct}prestashop>admin_translations_c3316be4d878e8eecabcde4836537ffe'] = 'Odznacz wszystkie grupy';
$_MODULE['<{hideproduct}prestashop>admin_warnings_c1c62f1fc0065109915f257b187efdf3'] = 'Kliknij na link, aby przejść zmienić konfigurację sklepu:';
$_MODULE['<{hideproduct}prestashop>admin_warnings_9446a98ad14416153cc4d45ab8b531bf'] = 'Wydajność';
$_MODULE['<{hideproduct}prestashop>schedule_426ffc82ce1cd9b670e80bd1610080a9'] = '\'Poniedziałek\',\'Wtorek\',\'środa\',\'Czwartek\',\'Piątek\',\'Sobota\',\'Niedziela\'';
