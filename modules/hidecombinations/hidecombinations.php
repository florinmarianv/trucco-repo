<?php
/**
* Hide combinations
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2021 idnovate
*  @license   See above
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}

include_once(_PS_MODULE_DIR_.'hidecombinations/classes/HidecombinationsConfiguration.php');

class Hidecombinations extends Module
{
    private $errors = array();
    private $success;

    public function __construct()
    {
        $this->name = 'hidecombinations';
        $this->tab = 'front_office_features';
        $this->version = '1.0.6';
        $this->author = 'idnovate';
        $this->module_key = '60460ab7864a2388e09aa7a9e729d218';
        $this->addons_id_product = '32668';
        $this->module_path = $this->_path;

        parent::__construct();

        $this->displayName = $this->l('Hide combinations');
        $this->description = $this->l('Hide combinations');

        $this->tabs[] = array(
            'class_name' => 'AdminHidecombinations',
            'name' => $this->l('Admin Hidecombinations'),
            'visible' => false
        );

        /* Backward compatibility */
        if (version_compare(_PS_VERSION_, '1.5', '<')) {
            require(_PS_MODULE_DIR_.$this->name.'/backward_compatibility/backward.php');
        }
    }

    public function copyOverrideFolder()
    {
        $version_override_folder = _PS_MODULE_DIR_.$this->name.'/override_'.Tools::substr(str_replace('.', '', _PS_VERSION_), 0, 2);
        $override_folder = _PS_MODULE_DIR_.$this->name.'/override';

        if (file_exists($override_folder) && is_dir($override_folder)) {
            $this->recursiveRmdir($override_folder);
        }

        if (is_dir($version_override_folder)) {
            $this->copyDir($version_override_folder, $override_folder);
        }

        return true;
    }

    protected function copyDir($src, $dst)
    {
        if (is_dir($src)) {
            $dir = opendir($src);
            @mkdir($dst);
            while (false !== ($file = readdir($dir))) {
                if (($file != '.') && ($file != '..')) {
                    if (is_dir($src.'/'.$file)) {
                        $this->copyDir($src.'/'.$file, $dst.'/'.$file);
                    } else {
                        copy($src.'/'.$file, $dst.'/'.$file);
                    }
                }
            }
            closedir($dir);
        }
    }

    protected function recursiveRmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir") {
                        $this->recursiveRmdir($dir."/".$object);
                    } else {
                        unlink($dir."/".$object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    public function reset()
    {
        if (!$this->uninstall(false)) {
            return false;
        }

        if (!$this->install(false)) {
            return false;
        }

        return true;
    }

    public function install()
    {
        $this->copyOverrideFolder();

        return parent::install()
            && $this->initSQL()
            && $this->installTabs()
            && $this->registerHook('actionGetProductPropertiesBefore')
            && $this->registerHook('displayProductButtons');
    }

    public function uninstall()
    {
        return parent::uninstall()
            && $this->uninstallTabs()
            && $this->uninstallSQL();
    }

    private function postValidation()
    {
        $replaced_array = array();
        foreach (Tools::getValue('reduction') as $key => $value) {
            $value = str_replace(',', '.', $value);
            if (is_numeric($value)) {
                $replaced_array[$key] = $value;
            } else {
                $replaced_array[$key] = 0;
                $this->errors[] = $this->l('Please define a correct percentage');
            }
        }

        if (!count($this->errors)) {
            $this->success = true;
        }

        /* clean cache to show all prices properly */
        if (class_exists(Tools::clearCache())) {
            Tools::clearCache();
        }
    }

    public function hookActionGetProductPropertiesBefore($params)
    {
        if (!Module::isEnabled('hidecombinations')) {
            return;
        }

        $context = Context::getContext();
        if (isset($context->controller) && $context->controller->controller_type == 'admin') {
            return;
        }

        if ($context->controller->php_self != 'product') {
            include_once(_PS_MODULE_DIR_.'hidecombinations/classes/HidecombinationsConfiguration.php');
            $hidecombinations = new HidecombinationsConfiguration();
            if (isset($params['product']['cache_default_attribute'])) {
                $params['product']['cache_default_attribute'] = $hidecombinations->getNewIpaDefault($params['product']['id_product'], $params['product']['cache_default_attribute']);
            }
        }
    }

    public function getContent()
    {
        $warnings_to_show = '';
        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            if (Configuration::get('PS_DISABLE_NON_NATIVE_MODULE')) {
                $warnings_to_show = $warnings_to_show . $this->displayError($this->l('You have to disable the option Disable non native modules at ADVANCED PARAMETERS - PERFORMANCE'));
            }

            if (Configuration::get('PS_DISABLE_OVERRIDES')) {
                $warnings_to_show = $warnings_to_show . $this->displayError($this->l('You have to disable the option Disable all overrides at ADVANCED PARAMETERS - PERFORMANCE'));
            }
        }

        if (!empty($warnings_to_show)) {
            $this->context->smarty->assign(array(
                'performance_link' => $this->context->link->getAdminLink('AdminPerformance'),
            ));
            return $warnings_to_show . $this->display(__FILE__, 'views/templates/admin/admin_warnings.tpl');
        }

        // check if the tab was not created in the installation
        foreach ($this->tabs as $myTab) {
            $id_tab = Tab::getIdFromClassName($myTab['class_name']);
            if (!$id_tab) {
                $this->addTab($myTab);
            }
        }

        return Tools::redirectAdmin('index.php?controller=' . $this->tabs[0]['class_name'] . '&token=' . Tools::getAdminTokenLite($this->tabs[0]['class_name']));
    }

    public function hookDisplayProductButtons($params)
    {
        if ($this->checkRulesExist()) {
            $id_product = Tools::getValue('id_product');
            if ($id_product) {
                include_once(_PS_MODULE_DIR_.'hidecombinations/classes/HidecombinationsConfiguration.php');
                $configs = HidecombinationsConfiguration::getConfigs($id_product);
                if (!empty($configs)) {
                    if (version_compare(_PS_VERSION_, '1.7', '<')) {
                        return $this->display(__FILE__, 'views/templates/front/front.tpl');
                    } else {
                        return $this->display(__FILE__, 'views/templates/front/front17.tpl');
                    }
                }
            }
        }
    }

    public function installTabs()
    {
        if (version_compare(_PS_VERSION_, '1.7.1', '>=')) {
            return true;
        }

        foreach ($this->tabs as $myTab) {
            $this->addTab($myTab);
        }
        return true;
    }

    public function addTab($myTab)
    {
        $id_tab = Tab::getIdFromClassName($myTab['class_name']);
        if (!$id_tab) {
            $tab = new Tab();
            $tab->class_name = $myTab['class_name'];
            $tab->module = $this->name;

            if (isset($myTab['parent_class_name'])) {
                $tab->id_parent = Tab::getIdFromClassName($myTab['parent_class_name']);
            } else {
                $tab->id_parent = -1;
            }

            $languages = Language::getLanguages(false);
            foreach ($languages as $lang) {
                $tab->name[$lang['id_lang']] = $myTab['name'];
            }

            $tab->add();
        }
    }

    public function uninstallTabs()
    {
        if (version_compare(_PS_VERSION_, '1.7.1', '>=')) {
            return true;
        }

        foreach ($this->tabs as $myTab) {
            $idTab = Tab::getIdFromClassName($myTab['class_name']);
            if ($idTab) {
                $tab = new Tab($idTab);
                $tab->delete();
            }
        }

        return true;
    }

    protected function initSQL()
    {
        Db::getInstance()->Execute('
            CREATE TABLE IF NOT EXISTS `'.pSQL(_DB_PREFIX_.$this->name).'_configuration` (
                `id_hidecombinations_configuration` int(10) unsigned NOT NULL auto_increment,
                `name` VARCHAR(100) NULL,
                `groups` TEXT NULL,
                `customers` TEXT NULL,
                `products` TEXT NULL,
                `countries` TEXT NULL,
                `zones` TEXT NULL,
                `categories` TEXT NULL,
                `manufacturers` TEXT NULL,
                `suppliers` TEXT NULL,
                `languages` TEXT NULL,
                `currencies` TEXT NULL,
                `features` TEXT NULL,
                `attributes` TEXT NULL,
                `active` tinyint(1) unsigned NOT NULL DEFAULT "0",
                `filter_prices` tinyint(1) unsigned DEFAULT "0",
                `threshold_min_price` decimal(10,3) NULL DEFAULT "0.000",
                `threshold_max_price` decimal(10,3) NULL DEFAULT "0.000",
                `threshold_price` int(1) unsigned NOT NULL DEFAULT "1",
                `filter_stock` tinyint(1) unsigned,
                `min_stock` int(10) NULL,
                `max_stock` int(10) NULL,
                `filter_weight` tinyint(1) unsigned,
                `min_weight` decimal(10,3) NULL DEFAULT "0.000",
                `max_weight` decimal(10,3) NULL DEFAULT "0.000",
                `priority` int(1) unsigned DEFAULT "0",
                `date_from` DATETIME,
                `date_to` DATETIME,
                `schedule` TEXT NULL DEFAULT "",
                `id_shop` tinyint(1) unsigned NOT NULL DEFAULT "0",
                `date_add` DATETIME,
                `date_upd` DATETIME,
            PRIMARY KEY (`id_hidecombinations_configuration`),
            KEY `id_hidecombinations_configuration` (`id_hidecombinations_configuration`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;');

        return true;
    }

    protected function uninstallSQL()
    {
        return Db::getInstance()->Execute('DROP TABLE IF EXISTS `'.pSQL(_DB_PREFIX_.$this->name).'_configuration`');
    }

    protected function checkRulesExist()
    {
        $id_shop = Context::getContext()->shop->id;

        $query = '
                SELECT hi.* FROM `'._DB_PREFIX_.'hidecombinations_configuration` hi WHERE hi.`id_shop` = '.(int)$id_shop.'
                AND hi.`active` = 1';
        $rules = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);

        return ($rules == false) ? false : true;
    }
}
