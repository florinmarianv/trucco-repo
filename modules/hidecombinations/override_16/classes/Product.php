<?php
/**
* Hide Combinations
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2021 idnovate
*  @license   See above
*/

class Product extends ProductCore
{
    public static function priceCalculation($id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $zipcode, $id_currency,
        $id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, &$specific_price, $use_group_reduction,
        $id_customer = 0, $use_customer_price = true, $id_cart = 0, $real_quantity = 0)
    {
        if (Module::isEnabled('hidecombinations') && Context::getContext()->controller->php_self != 'product') {
            $context = Context::getContext();
            if (isset($context->controller) && $context->controller->controller_type == 'admin') {
                return parent::priceCalculation($id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $zipcode, $id_currency,
                    $id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, $specific_price, $use_group_reduction,
                    $id_customer, $use_customer_price, $id_cart, $real_quantity);
            }

            include_once(_PS_MODULE_DIR_.'hidecombinations/classes/HidecombinationsConfiguration.php');
            $hidecombinations = new HidecombinationsConfiguration();
            $configs = $hidecombinations->getConfigs($id_product);
            $price = parent::priceCalculation($id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $zipcode, $id_currency,
                $id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, $specific_price, $use_group_reduction,
                $id_customer, $use_customer_price, $id_cart, $real_quantity);

            if ((float)$price == 0) {
                $pr = new Product($id_product);
                $price = $pr->price;
                static $address = null;
                static $context = null;
                if ($context == null) {
                    $context = Context::getContext()->cloneContext();
                }
                if ($address === null) {
                    if (is_object($context->cart) && $context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')} != null) {
                        $id_address = $context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
                        $address = new Address($id_address);
                    } else {
                        $address = new Address();
                    }
                }
                if ($id_shop !== null && $context->shop->id != (int) $id_shop) {
                    $context->shop = new Shop((int) $id_shop);
                }
                if (!$use_customer_price) {
                    $id_customer = 0;
                }
                if ($id_product_attribute === null) {
                    $id_product_attribute = Product::getDefaultAttribute($id_product);
                }
                $specific_price = SpecificPrice::getSpecificPrice(
                    (int) $id_product,
                    $id_shop,
                    $id_currency,
                    $id_country,
                    $id_group,
                    $quantity,
                    $id_product_attribute,
                    $id_customer,
                    $id_cart,
                    $real_quantity
                );

                $cache_id_2 = $id_product.'-'.$id_shop;
                if (!isset(self::$_pricesLevel2[$cache_id_2]) || !isset(self::$_pricesLevel2[$cache_id_2][(int)$id_product_attribute]['wholesale_price'])) {
                    $sql = new DbQuery();
                    if (Combination::isFeatureActive()) {
                        $sql->select('product_shop.`price`, product_shop.`wholesale_price`, product_shop.`ecotax`, pa.`wholesale_price` as attr_wholesale_price');
                    } else {
                        $sql->select('product_shop.`price`, product_shop.`wholesale_price`, product_shop.`ecotax`');
                    }
                    $sql->from('product', 'p');
                    $sql->innerJoin('product_shop', 'product_shop', '(product_shop.id_product=p.id_product AND product_shop.id_shop = '.(int)$id_shop.')');
                    $sql->where('p.`id_product` = '.(int)$id_product);
                    if (Combination::isFeatureActive()){
                        $sql->select('product_attribute_shop.id_product_attribute, product_attribute_shop.`price` AS attribute_price, product_attribute_shop.default_on');
                        $sql->leftJoin('product_attribute', 'pa', 'pa.`id_product` = p.`id_product`');
                        $sql->leftJoin('product_attribute_shop', 'product_attribute_shop', '(product_attribute_shop.id_product_attribute = pa.id_product_attribute AND product_attribute_shop.id_shop = '.(int)$id_shop.')');
                    } else {
                        $sql->select('0 as id_product_attribute');
                    }
                    $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
                    if (is_array($res) && count($res)) {
                        foreach ($res as $row) {
                            $array_tmp = array(
                                'price' => $row['price'],
                                'ecotax' => $row['ecotax'],
                                'wholesale_price' => $row['wholesale_price'],
                                'attr_wholesale_price' => $row['attr_wholesale_price'],
                                'attribute_price' => (isset($row['attribute_price']) ? $row['attribute_price'] : null)
                            );
                            self::$_pricesLevel2[$cache_id_2][(int)$row['id_product_attribute']] = $array_tmp;
                            if (isset($row['default_on']) && $row['default_on'] == 1) {
                                self::$_pricesLevel2[$cache_id_2][0] = $array_tmp;
                            }
                        }
                    }
                }
                $result = self::$_pricesLevel2[$cache_id_2][(int)$id_product_attribute];

                if (isset($specific_price['price']) && $specific_price['price'] > 0) {
                    $price = (float) $specific_price['price'];
                }
                // convert only if the specific price is in the default currency (id_currency = 0)
                if (!$specific_price || !($specific_price['price'] >= 0 && $specific_price['id_currency'])) {
                    $price = Tools::convertPrice($price, $id_currency);

                    if (isset($specific_price['price']) && $specific_price['price'] >= 0) {
                        $specific_price['price'] = $price;
                    }
                }

                // Attribute price
                if (is_array($result) && (!$specific_price || !$specific_price['id_product_attribute'] || $specific_price['price'] < 0)) {
                    $attribute_price = Tools::convertPrice($result['attribute_price'] !== null ? (float) $result['attribute_price'] : 0, $id_currency);
                    // If you want the default combination, please use NULL value instead
                    if ($id_product_attribute !== false) {
                        $price += $attribute_price;
                    }
                }

                if (!$specific_price || !($specific_price['price'] >= 0 && $specific_price['id_currency'])) {
                    $price = Tools::convertPrice($price, $id_currency);
                    if (isset($specific_price['price']) && $specific_price['price'] >= 0) {
                        $specific_price['price'] = $price;
                    }
                }

                $address->id_country = $id_country;
                $address->id_state = $id_state;
                $address->postcode = $zipcode;
                $tax_manager = TaxManagerFactory::getManager($address, Product::getIdTaxRulesGroupByIdProduct((int) $id_product, $context));
                $product_tax_calculator = $tax_manager->getTaxCalculator();
                if ($use_tax) {
                    $price = $product_tax_calculator->addTaxes($price);
                }
                $specific_price_reduction = 0;
                if (($only_reduc || $use_reduc) && $specific_price) {
                    if ($specific_price['reduction_type'] == 'amount') {
                        $reduction_amount = $specific_price['reduction'];
                        if (!$specific_price['id_currency']) {
                            $reduction_amount = Tools::convertPrice($reduction_amount, $id_currency);
                        }
                        $specific_price_reduction = $reduction_amount;
                        if (!$use_tax && $specific_price['reduction_tax']) {
                            $specific_price_reduction = $product_tax_calculator->removeTaxes($specific_price_reduction);
                        }
                        if ($use_tax && !$specific_price['reduction_tax']) {
                            $specific_price_reduction = $product_tax_calculator->addTaxes($specific_price_reduction);
                        }
                    } else {
                        $specific_price_reduction = $price * $specific_price['reduction'];
                    }
                }
                if ($use_reduc) {
                    $price -= $specific_price_reduction;
                }
                if ($use_group_reduction) {
                    $reduction_from_category = GroupReduction::getValueForProduct($id_product, $id_group);
                    if ($reduction_from_category !== false) {
                        $group_reduction = $price * (float) $reduction_from_category;
                    } else { // apply group reduction if there is no group reduction for this category
                        $group_reduction = (($reduc = Group::getReductionByIdGroup($id_group)) != 0) ? ($price * $reduc / 100) : 0;
                    }
                    $price -= $group_reduction;
                }
                if ($only_reduc) {
                    return Tools::ps_round($specific_price_reduction, $decimals);
                }
                $price = Tools::ps_round($price, $decimals);
                if ($price < 0) {
                    $price = 0;
                }
                return $price;
            } else {
                return parent::priceCalculation($id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $zipcode, $id_currency,
                $id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, $specific_price, $use_group_reduction,
                $id_customer, $use_customer_price, $id_cart, $real_quantity);
            }
        }
        return parent::priceCalculation($id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $zipcode, $id_currency,
                    $id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, $specific_price, $use_group_reduction,
                    $id_customer, $use_customer_price, $id_cart, $real_quantity);
    }
}
