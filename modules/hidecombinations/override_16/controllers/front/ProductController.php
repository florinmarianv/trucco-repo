<?php
/**
* Hide Products (by stock, by price, by geolocation, ...)
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2021 idnovate
*  @license   See above
*/

class ProductController extends ProductControllerCore
{
    protected function getMinimalProductOrDeclinationQuantity($product)
    {
        if (!Module::isEnabled('hidecombinations')) {
            return parent::getMinimalProductOrDeclinationQuantity($product);
        }

        $productAttributeId = $product['id_product_attribute'];
        $minimalProductQuantity = 1;

        if ($this->combinations && isset($this->combinations[$productAttributeId])) {
            return parent::getMinimalProductOrDeclinationQuantity($product);
        }
    }
}
