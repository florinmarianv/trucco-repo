{**
* Hide combinations
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2021 idnovate
*  @license   See above
*}

{if (isset($groups))}
    {foreach $groups as $key => $group}
        {append var='group' value=$key index='group_id'}
        {append var='arrayGroups' value=$group}
    {/foreach}
{/if}

<script type="text/javascript">
    function checkJQuery() {
        if (window.jQuery) {
            {if isset($combinations)}
                combinations = {$combinations|json_encode nofilter};
                arraySize = 0;
                if (Array.isArray(combinations)) {
                    arraySize = combinations.length;
                } else {
                    arraySize = Object.keys(combinations).length;
                }

                if (arraySize > 0) {
                    {if isset($arrayGroups)}
                        attributesGroup = {$arrayGroups|json_encode nofilter};

                        findCombinationHC();
                    {/if}
                }
            {/if}

            if (Number($('.current-price span').attr('content')) == 0) {
                $('.product-prices').html('');
                $('.control-label').html('');
                $('.product-information .product-minimal-quantity').html('');
                $('.product-information .product-quantity').html('');
            }
            prestashop.on('updatedProduct', function (event) {
                {if isset($attributesGroup)}
                    findCombinationHC();
                {/if}
            });

        } else {
            setTimeout(function() { checkJQuery() }, 50);
        }
    }


    checkJQuery();

    function findCombinationHC()
    {
        arrayAttrValues = new Array();
        found = false;
        follow = true;
        attributesGroup.forEach( function(attr, indexAttr, array) {
            var obj = attributesGroup[indexAttr];
            group_id = obj['group_id'];

            if (obj['group_type'] == 'select') {
                $('.product-variants select[name="group['+group_id+']"] option').each(function() {
                    if ($(this).length > 0) {
                        follow = searchCombination(arrayAttrValues, Number($(this).attr('value')), indexAttr, group_id, obj['group_type']);
                    }
                    if (!follow) {
                        return false;
                    }
                });
                arrayAttrValues[indexAttr] = Number($('.product-variants select[name="group['+group_id+']"]').val());
                if (!follow) {
                    return false;
                }
            }

            if (obj['group_type'] == 'radio' || obj['group_type'] == 'color') {
                $('.product-variants input[name="group['+group_id+']"][type=radio]').each(function() {
                    follow = searchCombination(arrayAttrValues, Number($(this).attr('value')), indexAttr, group_id, obj['group_type']);
                    if (!follow) {
                        return false;
                    }
                });
                arrayAttrValues[indexAttr] = Number($('.product-variants input[name="group['+group_id+']"][type=radio]:checked').val());
                if (!follow) {
                    return false;
                }
            }
        });
    }

    function searchCombination(arrayAttrValues, value, indexAttr, group_id, type)
    {
        if (type == 'select') {
            $('.product-variants select[name="group['+group_id+']"] option[value=' + Number($(this).attr('value')) + ']').show();
        }

        if (type == 'radio' || type == 'color') {
            $('.product-variants input[name="group['+group_id+']"][type=radio][value='+value+']').closest('li').show();
        }

        arrayAttrValues[indexAttr] = value;
        found = false;
        follow = true;

        for (var key in combinations ) {
            if (combinations.hasOwnProperty(key)) {
                if (compareArrays(combinations[key].attributes, arrayAttrValues)) {
                    found = true;
                    break;
                }
            }
        }

        if (!found) {
            if (type == 'select') {
                if ($('.product-variants select[name="group['+group_id+']"]').val() == value) {
                    changeOption = value;
                    $(".product-variants select[name='group_"+group_id+"'] option").each(function() {
                        if ($(this).length > 0 && $(this).attr('value') != changeOption) {
                            $('.product-variants select[name="group['+group_id+']"]').val(Number($(this).attr('value'))).trigger('change');
                            $(".product-variants select[name='group_"+group_id+"'] option[value=" + value + "]").hide();
                        }
                    });
                }
            }

            if (type == 'radio' || type == 'color') {
                if ($('.product-variants input[name="group['+group_id+']"][type=radio]:checked').val() == value) {
                    changeOption = value;
                    $('.product-variants input[name="group['+group_id+']"][type=radio]').each(function() {
                        if ($(this).length > 0 && Number($(this).attr('value')) != changeOption) {
                            $('.product-variants input[name="group['+group_id+']"][value='+$(this).attr('value')+']').attr('checked', true).trigger('change');
                            $('.product-variants input[name="group['+group_id+']"][type=radio][value='+value+']').closest('li').hide();
                            follow = false;
                            return false;
                        }
                    });
                    if (!follow) {
                        return false;
                    }
                } else {
                    $('.product-variants input[name="group['+group_id+']"][type=radio][value='+value+']').closest('li').hide();
                }
            }
        } else {
            {* if find combination, not hide, click if the value of the checked option is undefined *}
            if (type == 'select') {
                if ($('.product-variants select[name="group['+group_id+']"]').val() == undefined) {
                    valueSelect = $('.product-variants input[name="group['+group_id+']"][type=radio]').val()
                    $('.product-variants select[name="group['+group_id+']"]').val(Number(valueSelect)).trigger('change');
                }
            }

            if (type == 'radio' || type == 'color') {
                if ($('.product-variants input[name="group['+group_id+']"][type=radio]:checked').val() == undefined) {
                    valueRadio = $('.product-variants input[name="group['+group_id+']"][type=radio]').val()
                    $('.product-variants input[name="group['+group_id+']"][value='+valueRadio+']').attr('checked', true).trigger('change');
                    $('.product-variants input[name="group['+group_id+']"][value='+valueRadio+']').click();
                }
            }
        }
        return true;
    }

    function compareArrays(a, b)
    {
        var match = 0;
        for (var i = 0; i < a.length; i++) {
            for (var j = 0; j < b.length; j++) {
                if (a[i] === b[j]) {
                    match++;
                }
            }
        }
        if (match >= b.length) {
            return true;
        }
        return false;
    }

</script>