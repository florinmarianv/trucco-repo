{if (isset($groups))}
    {foreach $groups as $key => $group}
        {append var='group' value=$key index='group_id'}
        {append var='arrayGroups' value=$group}
    {/foreach}
{/if}

<script type="text/javascript">

    if (typeof findCombination !== "undefined") {
        attributesGroup = {$arrayGroups|json_encode nofilter};
        findCombination = (function() {
            var findCombinationCached = findCombination;

            return function(json) {
                findCombinationHC();
                findCombinationCached.apply(this, arguments);
            }
        })();
    }

    function findCombinationHC()

        arrayAttrValues = new Array();
        found = false;
        attributesGroup.forEach( function(attr, indexAttr, array) {
            var obj = attributesGroup[indexAttr];
            group_id = obj['group_id'];
            if (obj['group_type'] == 'select') {
                $(".attribute_list select[name='group_"+group_id+"'] option").each(function() {
                    if ($(this).length > 0) {
                        follow = searchCombination(arrayAttrValues, Number($(this).attr('value')), indexAttr, group_id, obj['group_type']);
                    }
                    if (!follow) {
                        return false;
                    }
                });
                arrayAttrValues[indexAttr] = Number($(".attribute_list select[name='group_"+group_id+"']").val());
                if (!follow) {
                    return false;
                }
            }

            if (obj['group_type'] == 'radio') {
                $(".attribute_list input[name='group_"+group_id+"'][type=radio]").each(function() {
                    follow = searchCombination(arrayAttrValues, Number($(this).attr('value')), indexAttr, group_id, obj['group_type']);
                    if (!follow) {
                        return false;
                    }
                });
                arrayAttrValues[indexAttr] = Number($(".attribute_list input[name='group_"+group_id+"']:checked").val());
                if (!follow) {
                    return false;
                }
            }

            if (obj['group_type'] == 'color') {
                arrayColors = new Array();
                $(".attribute_list #color_to_pick_list a").each(function() {
                    valueColor = getValueColor($(this).attr('id'));
                    follow = searchCombination(arrayAttrValues, valueColor, indexAttr, group_id, obj['group_type']);
                    if (!follow) {
                        return false;
                    }
                });
                arrayAttrValues[indexAttr] = getValueColor($(".attribute_list #color_to_pick_list a.selected").attr('id'));
                if (!follow) {
                    return false;
                }
            }
        });
    }

    function searchCombination(arrayAttrValues, value, indexAttr, group_id, type)
    {
        if (type == 'select') {
            $(".attribute_list select[name='group_"+group_id+"'] option[value=" + Number($(this).attr('value')) + "]").show();
        }

        if (type == 'radio') {
            $(".attribute_list input[name='group_"+group_id+"'][type=radio][value="+value+"]").closest('li').show();
        }

        if (type == 'color') {
            $(".attribute_list #color_to_pick_list a[id='color_"+value+"']").closest('li').show();
        }

        arrayAttrValues[indexAttr] = value;
        found = false;
        follow = true;

        for (x = 0; x < combinations.length; x++) {
            if (compareArrays(combinations[x]['idsAttributes'], arrayAttrValues)) {
                found = true;
                break;
            }
        }

        if (!found) {
            if (type == 'select') {
                if ($(".attribute_list select[name='group_"+group_id+"']").val() == value) {
                    changeOption = value;
                    $(".attribute_list select[name='group_"+group_id+"'] option").each(function() {
                        if ($(this).length > 0 && $(this).attr('value') != changeOption) {
                            $(".attribute_list select[name='group_"+group_id+"']").val(Number($(this).attr('value'))).trigger('change');
                            $(".attribute_list select[name='group_"+group_id+"'] option[value=" + value + "]").hide();
                            follow = false;
                            return false;
                        }
                    });
                    if (!follow) {
                        return false;
                    }
                } else {
                    $(".attribute_list select[name='group_"+group_id+"'] option[value=" + value + "]").hide();
                }
            }

            if (type == 'radio') {
                if ($(".attribute_list input[name='group_"+group_id+"'][type=radio]").val() == value) {
                    changeOption = value;
                    $(".attribute_list input[name='group_"+group_id+"'][type=radio]").each(function() {
                        if ($(this).length > 0 && $(this).attr('value') != changeOption) {
                            $(".attribute_list input[name='group_"+group_id+"'][value="+$(this).attr('value')+"]").prop('checked', true).trigger('click');
                            $(".attribute_list input[name='group_"+group_id+"'][type=radio][value="+value+"]").closest('li').hide();
                            follow = false;
                            return false;
                        }
                    });
                    if (!follow) {
                        return false;
                    }
                } else {
                    $(".attribute_list input[name='group_"+group_id+"'][type=radio][value="+value+"]").closest('li').hide();
                }
            }

            if (type == 'color') {
                valueSelected = getValueColor($(".attribute_list #color_to_pick_list a.selected").attr('id'));
                if (valueSelected == value) {
                    changeOption = value;
                    $(".attribute_list #color_to_pick_list a").each(function() {
                        valueCompare = getValueColor($(this).attr('id'));
                        if ($(this).length > 0 && valueCompare != changeOption) {
                            $(".attribute_list #color_to_pick_list a[id='color_"+valueCompare+"']").click();
                            $(".attribute_list #color_to_pick_list a[id='color_"+value+"']").closest('li').hide();
                            follow = false;
                            return false;
                        }
                    });
                    if (!follow) {
                        return false;
                    }
                } else {
                    $(".attribute_list #color_to_pick_list a[id='color_"+value+"']").closest('li').hide();
                }
            }
        } else {
            /* if find combination, not hide, click if the value of the checked option is undefined */
            if (type == 'select') {
                $(".attribute_list select[name='group_"+group_id+"'] option[value=" + value + "]").show();
                if ($(".attribute_list select[name='group_"+group_id+"']").val() == undefined) {
                    valueSelect = $(".attribute_list input[name='group_"+group_id+"'][type=radio]").val()
                    $(".attribute_list select[name='group_"+group_id+"']").val(Number(valueSelect)).trigger('change');
                }
            }

            if (type == 'radio') {
                $(".attribute_list input[name='group_"+group_id+"'][value="+value+"]").show();
                if ($(".attribute_list input[name='group_"+group_id+"'][type=radio]:checked").val() == undefined) {
                    valueRadio = $(".attribute_list input[name='group_"+group_id+"'][type=radio]").val()
                    $(".attribute_list input[name='group_"+group_id+"'][value="+valueRadio+"]").prop('checked', true).trigger('click');
                }
            }

            if (type == 'color') {
                //if ($(".color_pick_hidden input[name='group_"+group_id+"]'").val() == 0) {
                    //$(".color_pick_hidden input:hidden[name=group_3]'").val()
                if ($("input:hidden[name=group_"+group_id+"]").val() == 0) {
                    $(".attribute_list #color_"+value+"").trigger('click');
                    $(".attribute_list #color_"+value+"").closest('li').addClass('selected');
                }
                //$(".attribute_list input[name='group_"+group_id+"'][value="+value+"]").show();
                /*if ($(".attribute_list input[name='group_"+group_id+"'][type=radio]:checked").val() == undefined) {
                    valueRadio = $(".attribute_list input[name='group_"+group_id+"'][type=radio]").val()
                    $(".attribute_list input[name='group_"+group_id+"'][value="+valueRadio+"]").prop('checked', true).trigger('click');
                }*/
            }
        }
        return true;
    }

    function getValueColor(id)
    {
        value = 0;
        if (typeof id !== "undefined") {
            value = '';
            if (id != '') {
                pos = id.indexOf('_');
                if (pos > 0) {
                    value = Number(id.substring(pos + 1));
                }
            }
        }
        return value;
    }

    function compareArrays(a, b)
    {
        var match = 0;
        for (var i = 0; i < a.length; i++) {
            for (var j = 0; j < b.length; j++) {
                if (a[i] == b[j]) {
                    match++;
                }
            }
        }
        if (match >= b.length) {
            return true;
        }
        return false;
    }

</script>