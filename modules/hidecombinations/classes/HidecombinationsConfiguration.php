<?php
/**
* Hide Combinations (by stock, by price, by geolocation, ...)
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2021 idnovate
*  @license   See above
*/

class HidecombinationsConfiguration extends ObjectModel
{
    public $id_hidecombinations_configuration;
    public $name;
    public $groups;
    public $customers;
    public $countries;
    public $zones;
    public $categories;
    public $products;
    public $manufacturers;
    public $suppliers;
    public $currencies;
    public $languages;
    public $features;
    public $attributes;
    public $active = true;
    public $filter_prices;
    public $threshold_min_price;
    public $threshold_max_price;
    public $threshold_price;
    public $filter_stock;
    public $min_stock;
    public $max_stock;
    public $filter_weight;
    public $min_weight;
    public $max_weight;
    public $date_from;
    public $date_to;
    public $schedule;
    public $priority;
    public $id_shop;
    public $date_add;
    public $date_upd;

    protected static $cache;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'hidecombinations_configuration',
        'primary' => 'id_hidecombinations_configuration',
        'multilang' => false,
        'fields' => array(
            'name' =>                   array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 100),
            'groups' =>                 array('type' => self::TYPE_STRING),
            'countries' =>              array('type' => self::TYPE_STRING),
            'products' =>               array('type' => self::TYPE_STRING),
            'customers' =>              array('type' => self::TYPE_STRING),
            'zones' =>                  array('type' => self::TYPE_STRING),
            'categories' =>             array('type' => self::TYPE_STRING),
            'manufacturers' =>          array('type' => self::TYPE_STRING),
            'currencies' =>             array('type' => self::TYPE_STRING),
            'languages' =>              array('type' => self::TYPE_STRING),
            'suppliers' =>              array('type' => self::TYPE_STRING),
            'features' =>               array('type' => self::TYPE_STRING),
            'attributes' =>             array('type' => self::TYPE_STRING),
            'active' =>                 array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'filter_prices' =>          array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'threshold_min_price' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'threshold_max_price' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'threshold_price' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'filter_stock' =>           array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'min_stock' =>              array('type' => self::TYPE_INT, 'copy_post' => false),
            'max_stock' =>              array('type' => self::TYPE_INT, 'copy_post' => false),
            'min_weight' =>             array('type' => self::TYPE_FLOAT, 'copy_post' => false),
            'max_weight' =>             array('type' => self::TYPE_FLOAT, 'copy_post' => false),
            'filter_weight' =>          array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'date_from' =>              array('type' => self::TYPE_DATE, 'copy_post' => false),
            'date_to' =>                array('type' => self::TYPE_DATE, 'copy_post' => false),
            'schedule' =>               array('type' => self::TYPE_STRING),
            'priority' =>               array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'id_shop' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'date_add' =>               array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_upd' =>               array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
        ),
    );

    public function __construct($id = null, $id_lang = null)
    {
        parent::__construct($id, $id_lang);
    }

    public function add($autodate = true, $null_values = true)
    {
        $this->id_shop = ($this->id_shop) ? $this->id_shop : Context::getContext()->shop->id;
        $success = parent::add($autodate, $null_values);
        return $success;
    }

    public function toggleStatus()
    {
        parent::toggleStatus();
        return Db::getInstance()->execute('
        UPDATE `'._DB_PREFIX_.bqSQL($this->def['table']).'`
        SET `date_upd` = NOW()
        WHERE `'.bqSQL($this->def['primary']).'` = '.(int)$this->id);
    }

    public function delete()
    {
        if (parent::delete()) {
            return $this->deleteImage();
        }
    }

    public static function getConfigs($id_product = 0)
    {
        $context = Context::getContext();

        $id_lang = $context->language->id;
        $id_shop = $context->shop->id;

        if (isset($context->currency) && !empty($context->currency)) {
            $id_currency = $context->currency->id;
        }

        $id_customer = 0;
        if (isset($context->customer) && !empty($context->customer)) {
            $id_customer = $context->customer->id;
        }

        $id_country = 0;
        $id_state = 0;

        if (isset($context->cart) && !empty($context->cart)) {
            $id_address_delivery = $context->cart->id_address_delivery;
            $address = new Address($id_address_delivery);
            $id_country = $address->id_country;
            $id_state = $address->id_state;
        }

        if ($id_country == 0) {
            $id_country = $context->country->id;
        }
        $query = '';
        $today = date("Y-m-d H:i:s");

        $query = '
                SELECT h.* FROM `'._DB_PREFIX_.'hidecombinations_configuration` h WHERE h.`id_shop` = '.(int)$id_shop.'
                    AND h.`active` = 1 AND (date_from <= "'.pSQL($today). '" OR date_from = "0000-00-00 00:00:00") AND (date_to >= "'.pSQL($today).'" OR date_to = "0000-00-00 00:00:00")';

        $configs = Db::getInstance(_PS_USE_SQL_SLAVE_)->executesmodule($query);
        if (empty($configs)) {
            return false;
        }

        $configs_priority = array();
        foreach ($configs as $key => $row) {
            $configs_priority[$key] = $row['priority'];
        }
        array_multisort($configs_priority, SORT_ASC, $configs);

        $customer = new Customer($id_customer);
        $customer_groups = $customer->getGroupsStatic($customer->id);

        $country = new Country($id_country);
        $zone = 0;
        if ($id_state > 0) {
            $zone = State::getIdZone($id_state);
        } else if ($id_country != null and $id_country > 0) {
            $zone = $country->getIdZone($id_country);
        }

        $array_confs_result = array();
        $product = new Product($id_product);
        $array_configurations_result = array();
        $categories = Product::getProductCategories($id_product);
        $product = new Product($id_product);
        $id_manufacturer = $product->id_manufacturer;
        $product_suppliers_array = ProductSupplier::getSupplierCollection($id_product);

        foreach ($configs as $conf) {
            if (!HidecombinationsConfiguration::isShowableBySchedule($conf)) {
                continue;
            }

            if ($conf['attributes'] == '' && $conf['features'] == '' && $conf['currencies'] == '' && $conf['languages'] == '' && $conf['groups'] == '' && $conf['products'] == '' && $conf['customers'] == '' && $conf['countries'] == '' && $conf['zones'] == '' && $conf['categories'] == '' && $conf['manufacturers'] == '' && $conf['suppliers'] == '') {
                $array_configurations_result[] = $conf;
                    continue;

            }

            $filter_features = false;
            $array_features_selected = Tools::jsonDecode($conf['features'], true);
            $product_features = Product::getFeaturesStatic((int)$id_product);

            $flag_features = 0;
            if (!empty($array_features_selected) && count($array_features_selected) > 0) {
                foreach ($product_features as $pf) {
                    if (isset($array_features_selected[$pf['id_feature']])) {
                        $array_f = explode(";", $array_features_selected[$pf['id_feature']]);
                        if (in_array($pf['id_feature_value'], $array_f)) {
                            $flag_features++;
                            continue;
                        }
                    }
                }
            } else {
                $filter_features = true;
            }

            if ($flag_features > 0) {
                $filter_features = true;
            }

            $filter_currencies = true;
            if ($conf['currencies'] !== '') {
                $currencies_array = explode(';', $conf['currencies']);
                if (!in_array($id_currency, $currencies_array)) {
                    $filter_currencies = false;
                }
            }
            $filter_languages = true;
            if ($conf['languages'] !== '') {
                $languages_array = explode(';', $conf['languages']);
                if (!in_array($id_lang, $languages_array)) {
                    $filter_languages = false;
                }
            }

            $filter_groups = true;
            $filter_customers = true;
            if ($conf['groups'] !== '' && $conf['customers'] == '') {
                $groups_array = explode(';', $conf['groups']);
                foreach ($customer_groups as $group) {
                    if (!in_array($group, $groups_array)) {
                        $filter_groups = false;
                    } else {
                        $filter_groups = true;
                        break;
                    }
                }
                if (!$filter_groups) {
                    $filter_customers = false;
                }
            } else if ($conf['groups'] == '' && $conf['customers'] !== '') {
                $customers_array = explode(';', $conf['customers']);
                if (!in_array($id_customer, $customers_array)) {
                    $filter_customers = false;
                }
            } else if ($conf['groups'] !== '' && $conf['customers'] !== '') {
                $groups_array = explode(';', $conf['groups']);
                foreach ($customer_groups as $group) {
                    if (!in_array($group, $groups_array)) {
                        $filter_groups = false;
                    } else {
                        $filter_groups = true;
                    }
                }
                if (!$filter_groups) {
                    $customers_array = explode(';', $conf['customers']);
                    if (!in_array($id_customer, $customers_array)) {
                        $filter_customers = false;
                    } else {
                        $filter_customers = true;
                    }
                } else {
                    $customers_array = explode(';', $conf['customers']);
                    if (!in_array($id_customer, $customers_array)) {
                        $filter_customers = false;
                    }
                }
            }
            $filter_countries = true;
            if ($conf['countries'] !== '') {
                $countries_array = explode(';', $conf['countries']);

                if (!in_array($country->id, $countries_array)) {
                    $filter_countries = false;
                }
            }

            $filter_zones = true;
            if ($conf['zones'] !== '') {
                $zones_array = explode(';', $conf['zones']);
                if (!in_array($zone, $zones_array)) {
                    $filter_zones = false;
                }
            }
            $filter_categories = true;
            $filter_products = true;
            $filter_manufacturers = true;
            $filter_suppliers = true;

            if ($id_product > 0) {
                if (@unserialize($conf['categories']) !== false) {
                    $categories_array = unserialize($conf['categories']);
                } else {
                    $categories_array = explode(';', $conf['categories']);
                }

                if ($conf['categories'] !== '' && $conf['products'] == '') {
                    foreach ($categories as $category) {
                        if (in_array($category, $categories_array)) {
                            $filter_categories = true;
                            $filter_products = true;
                            break;
                        } else {
                            $filter_categories = false;
                        }
                    }
                    if (!$filter_categories) {
                        $filter_products = false;
                    }
                } else if ($conf['categories'] == '' && $conf['products'] !== '') {

                    $products_array = explode(';', $conf['products']);
                    if (!in_array($id_product, $products_array)) {
                        $filter_products = false;
                        $filter_categories = true;
                    }
                } else if ($conf['categories'] !== '' && $conf['products'] !== '') {
                    foreach ($categories as $category) {
                        if (!in_array($category, $categories_array)) {
                            $filter_categories = false;
                        } else {
                            $filter_categories = true;
                            break;
                        }
                    }
                    if (!$filter_categories) {
                        $products_array = explode(';', $conf['products']);
                        if (!in_array($id_product, $products_array)) {
                            $filter_products = false;
                        } else {
                            $filter_products = true;
                        }
                    } else {
                        $products_array = explode(';', $conf['products']);
                        if (!in_array($id_product, $products_array)) {
                            $filter_products = false;
                        }
                    }
                }

                $filter_manufacturers = true;
                if ($conf['manufacturers'] !== '') {
                    $manufacturers_array = explode(';', $conf['manufacturers']);
                    if (!in_array($id_manufacturer, $manufacturers_array)) {
                        $filter_manufacturers = false;
                    }
                }
                $filter_suppliers = true;
                if ($conf['suppliers'] !== '') {
                    $filter_suppliers = false;
                    $suppliers_array = explode(';', $conf['suppliers']);
                    if (!empty($product_suppliers_array)) {
                        foreach ($product_suppliers_array as $ps) {
                            if (in_array($ps->id_supplier, $suppliers_array)) {
                                $filter_suppliers = true;
                                break;
                            }
                        }
                    }
                }
            }


/*
$logger->logDebug("filter_groups: ".print_r($filter_groups, true));
$logger->logDebug("filter_customers: ".print_r($filter_customers, true));
$logger->logDebug("filter_countries: ".print_r($filter_countries, true));
$logger->logDebug("filter_zones: ".print_r($filter_zones, true));
$logger->logDebug("filter_categories: ".print_r($filter_categories, true));
$logger->logDebug("filter_products: ".print_r($filter_products, true));
$logger->logDebug("filter_manufacturers: ".print_r($filter_manufacturers, true));
$logger->logDebug("filter_suppliers: ".print_r($filter_suppliers, true));
$logger->logDebug("filter_attributes: ".print_r($filter_attributes, true));
$logger->logDebug("filter_features: ".print_r($filter_features, true));
$logger->logDebug("filter_currencies: ".print_r($filter_currencies, true));
$logger->logDebug("filter_languages: ".print_r($filter_languages, true));
*/
            if ($filter_currencies && $filter_languages && $filter_features && $filter_groups && $filter_customers && $filter_countries
                && $filter_zones && $filter_categories && $filter_products && $filter_manufacturers && $filter_suppliers) {
                    $array_configurations_result[] = $conf;
                    continue;
            }
        }

        if (count($array_configurations_result) > 0) {
            return $array_configurations_result;
        }
        return false;
    }

    public static function returnSQLModified($sql)
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            $url_name = $_SERVER['HTTP_REFERER'];
        } else {
            $url_name = "";
        }

        if (strpos($url_name, 'fs2ps') !== false || strpos($url_name, 'simpleimportproduct') !== false) {
            return $sql;
        }


        /*if (Context::getContext()->controller) {
            $cname = Context::getContext()->controller->php_self;
            if (strpos($cname, 'category') !== false) {
                return $sql;
            }
        }*/

        if ($sql instanceof DbQuery) {
            $sql = $sql->build();
        }

        /*$tableNames = array('/\b'._DB_PREFIX_.'product_attribute\b/i',
                            '/\b`'._DB_PREFIX_.'product_attribute`\b/i',
                            '/\b'._DB_PREFIX_.'product_attribute\)/i',
                            '/\b'._DB_PREFIX_.'product_attribute_shop\b/i',
                            '/\b`'._DB_PREFIX_.'product_attribute_shop`\b/i',
                            '/\b'._DB_PREFIX_.'product_attribute_shop\)/i'
                        );

        $tableNamesDest = array(_DB_PREFIX_.'product_attribute_hide',
                                _DB_PREFIX_.'product_attribute_hide`',
                                _DB_PREFIX_.'product_attribute_hide)',
                                _DB_PREFIX_.'product_attribute_shop_hide',
                                _DB_PREFIX_.'product_attribute_shop_hide`',
                                _DB_PREFIX_.'product_attribute_shop_hide)',
                        );*/

        $tableNames = array('/\b'._DB_PREFIX_.'product_attribute\b/i',
                            '/\b`'._DB_PREFIX_.'product_attribute`\b/i',
                            '/\b'._DB_PREFIX_.'product_attribute\)/i',
                            '/\b'._DB_PREFIX_.'pm_advancedpack_products\b/i',
                            '/\b`'._DB_PREFIX_.'pm_advancedpack_products`\b/i',
                            '/\b'._DB_PREFIX_.'pm_advancedpack_products\)/i',
                            '/\b'._DB_PREFIX_.'product_attribute_shop\b/i',
                            '/\b`'._DB_PREFIX_.'product_attribute_shop`\b/i',
                            '/\b'._DB_PREFIX_.'product_attribute_shop\)/i'
                        );

        $tableNamesDest = array(_DB_PREFIX_.'product_attribute_hide',
                                _DB_PREFIX_.'product_attribute_hide`',
                                _DB_PREFIX_.'product_attribute_hide)',
                                _DB_PREFIX_.'pm_advancedpack_products_hide',
                                _DB_PREFIX_.'pm_advancedpack_products_attributes_hide`',
                                _DB_PREFIX_.'pm_advancedpack_products_attributes_hide)',
                                _DB_PREFIX_.'product_attribute_shop_hide',
                                _DB_PREFIX_.'product_attribute_shop_hide`',
                                _DB_PREFIX_.'product_attribute_shop_hide)',
                        );



        $cont = 0;
        $sqlReplace = preg_replace($tableNames, $tableNamesDest, $sql, -1, $cont);

        //$sqlReplace = preg_replace($tableNames, $tableNamesDest, $sql);

        $ret = false;

        if (strcmp($sql, $sqlReplace) !== 0) {
            $sqlReplace_result = $sql;
            $numTables = $cont;
            if ($cont > 1) {
                do {
                    $sqlReplace_result = preg_replace_callback($tableNames, function($match) use($cont) { return (($match[0].'_hide_'.$cont)); }, $sqlReplace_result, 1);
                    $cont--;
                } while ($cont > 0);
                $sqlReplace = $sqlReplace_result;
            }
            if (Tools::isSubmit('id_product')) {
                $id_product = Tools::getValue('id_product');
            } else {
                $id_product = 0;
            }
            $configs = HidecombinationsConfiguration::getConfigs($id_product);
            if (!empty($configs)) {
                $iPas = array();
                $iPas = HidecombinationsConfiguration::getIPAsToUnset($configs, $id_product);

                if ($iPas && $iPas != '') {
                    HidecombinationsConfiguration::createAndReplace($iPas, $numTables);
                    $ret = true;
                }
                if ($ret) {
                    self::$cache = true;
                    return $sqlReplace;
                }
            }
        }
        return $sql;
    }

    public static function createAndReplace($idIpas, $numTables)
    {
        if ($numTables > 1) {

            if ($idIpas != -1) {
                do {
                    $sql_create = ' CREATE TEMPORARY TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_attribute_shop_hide_'.$numTables.'` (PRIMARY KEY (`id_product_attribute`, `id_shop`), KEY `id_product_id_product_attribute` (`id_product_attribute`,`id_product`)) ENGINE=Memory SELECT * FROM `'._DB_PREFIX_.'product_attribute_shop` where id_shop = '.Context::getContext()->shop->id.' AND id_product_attribute NOT IN ('.pSQL($idIpas).'); ';

                    $sql_create .= 'CREATE TEMPORARY TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_attribute_hide_'.$numTables.'` (PRIMARY KEY (`id_product_attribute`),
                                    KEY `product_attribute_product` (`id_product`),
                                    KEY `reference` (`reference`),
                                    KEY `supplier_reference` (`supplier_reference`),
                                    KEY `id_product_id_product_attribute` (`id_product_attribute`,`id_product`)) ENGINE=Memory SELECT * FROM `'._DB_PREFIX_.'product_attribute` where id_product_attribute NOT IN ('.pSQL($idIpas).');';

                    Db::getInstance()->executemodule($sql_create);
                    $numTables--;
                } while ($numTables > 0);
            }
        } else {
            if ($idIpas != -1) {
                $sql_create = ' CREATE TEMPORARY TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_attribute_shop_hide` (PRIMARY KEY (`id_product_attribute`, `id_shop`), KEY `id_product_id_product_attribute` (`id_product_attribute`,`id_product`)) ENGINE=Memory SELECT * FROM `'._DB_PREFIX_.'product_attribute_shop` where id_shop = '.Context::getContext()->shop->id.' AND id_product_attribute NOT IN ('.pSQL($idIpas).'); ';

                $sql_create .= 'CREATE TEMPORARY TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_attribute_hide` (PRIMARY KEY (`id_product_attribute`),
                                    KEY `product_attribute_product` (`id_product`),
                                    KEY `reference` (`reference`),
                                    KEY `supplier_reference` (`supplier_reference`),
                                    KEY `id_product_id_product_attribute` (`id_product_attribute`,`id_product`)) ENGINE=Memory SELECT * FROM `'._DB_PREFIX_.'product_attribute` where id_product_attribute NOT IN ('.pSQL($idIpas).');';

                if (Module::isEnabled('pm_advancedpack')) {
                    $sql_create .= ' CREATE TEMPORARY TABLE IF NOT EXISTS `'._DB_PREFIX_.'pm_advancedpack_products_hide` ENGINE=Memory SELECT * FROM `'._DB_PREFIX_.'pm_advancedpack_products` where 1=1;';
                }
            } else {
                $sql_create = ' CREATE TEMPORARY TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_attribute_shop_hide` (PRIMARY KEY (`id_product_attribute`, `id_shop`), KEY `id_product_id_product_attribute` (`id_product_attribute`,`id_product`)) ENGINE=Memory SELECT * FROM `'._DB_PREFIX_.'product_attribute_shop` where id_shop = '.Context::getContext()->shop->id.' and 1 = 0);';

                $sql_create .= 'CREATE TEMPORARY TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_attribute_hide` (PRIMARY KEY (`id_product_attribute`),
                                    KEY `product_attribute_product` (`id_product`),
                                    KEY `reference` (`reference`),
                                    KEY `supplier_reference` (`supplier_reference`),
                                    KEY `id_product_id_product_attribute` (`id_product_attribute`,`id_product`)) ENGINE=Memory SELECT * FROM `'._DB_PREFIX_.'product_attribute` where 1 = 0;';

                if (Module::isEnabled('pm_advancedpack')) {
                    $sql_create .= ' CREATE TEMPORARY TABLE IF NOT EXISTS `'._DB_PREFIX_.'pm_advancedpack_products_attributes_hide` (PRIMARY KEY (`id_product_pack`,`id_product_attribute`) ENGINE=Memory SELECT * FROM `'._DB_PREFIX_.'pm_advancedpack_products_attributes` where 1 = 0;';
                }
            }
        }

        $ret = Db::getInstance()->executemodule($sql_create);
        return true;
    }

    public static function getNewIpaDefault($id_product = 0, $id_product_attribute = 0)
    {
        if ($id_product_attribute) {
            $sql = 'SELECT sa.`quantity`
                FROM `'._DB_PREFIX_.'stock_available` sa
                WHERE sa.`id_product_attribute` = '.(int)$id_product_attribute;
            $quantity = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);

            if ($quantity == 0) {
                $newIPA = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
                    SELECT sa.`id_product_attribute`
                        FROM `'._DB_PREFIX_.'stock_available` sa
                        WHERE sa.`id_product` = '.(int)$id_product.' && sa.`quantity` > 0');
                return $newIPA;
            }
        }

        return $id_product_attribute;
    }



    public static function getIPAsToUnset($configs, $id_product = 0)
    {
        $hidecomb = new HidecombinationsConfiguration();
        $ret = "";

        $ipasArray = array();
        foreach ($configs as $conf) {

            $array_attributes = array();
            $array_attr = array();

            if (isset($conf['attributes']) && $conf['attributes']) {
                $array_attr = Tools::jsonDecode($conf['attributes'], true);
            }

            foreach ($array_attr as $key => $a) {
                $array_attr[$key] = explode(',', $a);
            }

            foreach ($array_attr as $k=>$v) {
                foreach ($v as $item) {
                    $array_attributes[] = str_replace(";", ",", $item);
                }
            }


            $sql = '
                SELECT pa.id_product_attribute ';

            if (isset($conf['filter_stock']) && $conf['filter_stock']) {
                $sql .= ', sa.quantity ';
            }

            $sql .= 'FROM `'._DB_PREFIX_.'product_attribute` pa LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac on pa.id_product_attribute = pac.id_product_attribute';

            if (isset($conf['filter_stock']) && $conf['filter_stock']) {
                $sql .= ' LEFT JOIN `'._DB_PREFIX_.'stock_available` sa ON (pa.id_product_attribute = sa.id_product_attribute ';
                //$sql .= ' AND sa.id_shop_group = 1 ) ';

                $context = Context::getContext();

                if (Shop::getContext() == Shop::CONTEXT_GROUP) {
                    $shop_group = Shop::getContextShopGroup();
                } else {
                    $shop_group = $context->shop->getGroup();
                }

                $shop = $context->shop;
                if (is_object($shop)) {
                    $shop_group = $shop->getGroup();
                } else {
                    $shop = new Shop($shop);
                    $shop_group = $shop->getGroup();
                }

                if ($shop_group->share_stock) {
                    $sql .= ' AND sa.id_shop_group = '.(int)$shop_group->id.' ) ';
                } else {
                    $sql .= ' AND sa.id_shop = '.Context::getContext()->shop->id.' ) ';
                }
            }

            if (!empty($array_attributes)) {
                $sql .= ' WHERE ';
            }

            $sqlAttributes = '';
            if (!empty($array_attributes)) {
                 $sqlAttributes = ' (';
                $countAttr = count($array_attributes) - 1;
            } else {
                $countAttr = 0;
            }
            $i = 0;

            foreach ($array_attributes as $key => $item) {
                $sqlAttributes .= ' `id_attribute` in ('.$item.')';
                if ($i < $countAttr) {
                    $sqlAttributes .= ' or ';
                }
                $i++;
            }
            if (!empty($array_attributes)) {
                $sqlAttributes .= ') ';
            }

            if ($id_product > 0) {
                $idProducts = array($id_product);
            } else {
                $idProducts = HidecombinationsConfiguration::getIdProducts($conf);
            }

            $sqlProducts = '';
            if (!empty($idProducts)) {
                if (is_array($idProducts)) {
                    $idProducts = implode(',', $idProducts);
                }
                if (empty($array_attributes)) {
                    $sqlProducts = ' WHERE pa.id_product in ('.$idProducts.') ';
                } else {
                    $sqlProducts = ' AND pa.id_product in ('.$idProducts.') ';
                }
            }

            $sql .= $sqlAttributes . $sqlProducts;

            if (isset($conf['filter_stock']) && $conf['filter_stock']) {
                if ($conf['max_stock'] > 0 && $conf['min_stock'] > 0) {
                    $sql .= ' AND ( sa.quantity >= '.$conf['min_stock'].' AND sa.quantity <= '.$conf['max_stock'].' )';
                } else if ($conf['max_stock'] >= 0 && $conf['min_stock'] <= 0) {
                    $sql .= ' AND ( ( sa.quantity >= '.$conf['min_stock'].' AND sa.quantity <= '.$conf['max_stock'].') OR sa.quantity is NULL )';
                }
                else if ($conf['max_stock'] == 0) {
                    $sql .= ' AND sa.quantity >= '.$conf['min_stock'];
                } else {
                    $sql .= ' AND sa.quantity <= '.$conf['max_stock'];
                }
            }

            $sql .= ' GROUP BY pa.id_product_attribute ';
            /*if ($countAttr > 0) {
                $sql .= ' HAVING count(*) > '.$countAttr;
            }*/


            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executesmodule($sql, true, true);
            if (!empty($result)) {
                $ipasArray[] = implode(',', array_column(Db::getInstance(_PS_USE_SQL_SLAVE_)->executesmodule($sql, true, true), 'id_product_attribute'));
            }

        }

        if (!empty($ipasArray)) {
            return implode(',', $ipasArray);
        }
        return false;
    }

    public static function getIdProducts($conf)
    {
        $productsReturn = array();
        $prods = array();

        if ($conf['products'] == '' && $conf['manufacturers'] == '' && $conf['categories'] == '' && $conf['suppliers'] == '' && $conf['attributes'] == '' && $conf['features'] == '' && !$conf['filter_stock'] && !$conf['filter_weight'] && !$conf['filter_prices']) {
            return -1;
        }

        $categories = false;
        $manufacturers = false;
        $suppliers = false;
        $products = false;

        if ($conf['products']) {
            $products = implode(',', explode(";", $conf['products']));
        }

        if ($conf['categories'] != '') {
            if (@unserialize($conf['categories']) !== false) {
                $categories = implode(',', unserialize($conf['categories']));
            } else {
                $categories = implode(',', explode(';', $conf['categories']));
            }
        }

        if ($conf['manufacturers'] != '') {
            $manufacturers = implode(',', explode(";", $conf['manufacturers']));
        }

        if ($conf['suppliers']) {
            $suppliers = implode(',', explode(";", $conf['suppliers']));
        }

        $prods = HidecombinationsConfiguration::getProductsLite(Context::getContext()->language->id, $categories, $manufacturers, $suppliers, $products, $conf);

        if (!empty($prods)) {
            return implode(',', array_column($prods, 'id_product'));
        }
        return '';
    }

    public static function getProductsLite($id_lang, $categories = false, $manufacturers = false, $suppliers = false, $products = false, $conf = false)
    {
        $context = Context::getContext();

        $sql = 'SELECT p.`id_product`, CONCAT(p.`reference`, " - ", pl.`name`) as name ';

        if (isset($conf['filter_prices']) && $conf['filter_prices']) {
            $sql .= ', p.`wholesale_price` * (1 + (t.`rate` / 100)) as whole_with_taxes, p.`price` * (1 + (t.`rate` / 100)) as price_with_taxes ';
        }

        if (isset($conf['filter_weight']) && $conf['filter_weight']) {
            $sql .= ', weight ';
        }

        $sql .= 'FROM `'._DB_PREFIX_.'product` p '.Shop::addSqlAssociation('product', 'p').'
                    LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')'.
                    ($categories ? ' LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product` ' : '');

        if (isset($conf['filter_prices']) && $conf['filter_prices']) {
            $sql .= ' LEFT JOIN `'._DB_PREFIX_.'tax_rules_group` trg ON p.`id_tax_rules_group` = trg.`id_tax_rules_group`
                    LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON trg.`id_tax_rules_group` = tr.`id_tax_rules_group`
                    LEFT JOIN `'._DB_PREFIX_.'tax` t ON tr.`id_tax` = tr.`id_tax` ';
        }



        $sql .= ' WHERE pl.`id_lang` = '.(int)$id_lang.' AND product_shop.`visibility` IN ("both", "catalog") AND product_shop.`active` = 1'.
                    ($manufacturers ? ' AND p.`id_manufacturer` IN ('.$manufacturers.')' : '').
                    ($suppliers ? ' AND p.`id_supplier` IN ('.$suppliers.')' : '').
                    ($categories ? ' AND cp.`id_category` IN ('.$categories.')' : '').
                    ($products ? ' AND p.`id_product` IN ('.$products.')' : '');

        /* no discount or discount and no image or image filters */
        if (isset($conf['hide_no_discount']) && $conf['hide_no_discount']) {
            if (isset($conf['hide_no_image']) && $conf['hide_no_image']) {
                $sql .= ' AND p.id_product NOT IN (SELECT sp.id_product FROM `'._DB_PREFIX_.'specific_price` sp, `'._DB_PREFIX_.'image_shop` i WHERE i.id_product = sp.id_product)';
            } else if (isset($conf['hide_image']) && $conf['hide_image']) {
                $sql .= ' AND p.id_product NOT IN (SELECT sp.id_product FROM `'._DB_PREFIX_.'specific_price` sp WHERE sp.id_product NOT IN (SELECT i.id_product FROM `'._DB_PREFIX_.'image_shop` i) )';
            } else {
                $sql .= ' AND p.id_product NOT IN (SELECT sp.id_product FROM `'._DB_PREFIX_.'specific_price` sp)';
            }
        } else if (isset($conf['hide_discount']) && $conf['hide_discount']) {
            if (isset($conf['hide_image']) && $conf['hide_image']) {
                $sql .= '  AND (p.id_product IN (SELECT sp.id_product FROM `'._DB_PREFIX_.'specific_price` sp) OR p.id_product IN (SELECT i.id_product FROM `'._DB_PREFIX_.'image_shop` i) )';
            } else if (isset($conf['hide_no_image']) && $conf['hide_no_image']) {
                $sql .= ' AND p.id_product IN (SELECT sp.id_product FROM `'._DB_PREFIX_.'specific_price` sp WHERE sp.id_product NOT IN (SELECT i.id_product FROM `'._DB_PREFIX_.'image_shop` i) )';
            } else {
                $sql .= ' AND p.id_product IN (SELECT sp.id_product FROM `'._DB_PREFIX_.'specific_price` sp)';
            }
        } else if (isset($conf['hide_image']) && $conf['hide_image']) {
            $sql .= ' AND p.id_product IN (SELECT i.id_product FROM `'._DB_PREFIX_.'image_shop` i)';
        } else if (isset($conf['hide_no_image']) && $conf['hide_no_image']) {
            $sql .= ' AND p.id_product NOT IN (SELECT i.id_product FROM `'._DB_PREFIX_.'image_shop` i)';
        }


        if (isset($conf['filter_prices']) && $conf['filter_prices']) {
            if ($conf['threshold_price'] == 0) {
                if (($conf['threshold_max_price'] == 0 && $conf['threshold_min_price'] == 0) || ($conf['threshold_max_price'] > 0 && $conf['threshold_min_price'] > 0)) {
                    $sql .= ' AND ( p.`wholesale_price` >= '.$conf['threshold_min_price'].' AND p.`wholesale_price` <= '.$conf['threshold_max_price'].' )';
                } else if ($conf['threshold_max_price'] == 0) {
                    $sql .= ' AND p.`wholesale_price` >= '.$conf['threshold_min_price'];
                } else {
                    $sql .= ' AND p.`wholesale_price` <= '.$conf['threshold_max_price'];
                }
            }

            if ($conf['threshold_price'] == 1) {
                if (($conf['threshold_max_price'] == 0 && $conf['threshold_min_price'] == 0) || ($conf['threshold_max_price'] > 0 && $conf['threshold_min_price'] > 0)) {
                    $sql .= ' AND ( p.`price` >= '.$conf['threshold_min_price'].' AND p.`price` <= '.$conf['threshold_max_price'].' )';
                } else if ($conf['threshold_max_price'] == 0) {
                    $sql .= ' AND p.`price` >= '.$conf['threshold_min_price'];
                } else {
                    $sql .= ' AND p.`price` <= '.$conf['threshold_max_price'];
                }
            }

            if ($conf['threshold_price'] == 2) {
                if (($conf['threshold_max_price'] == 0 && $conf['threshold_min_price'] == 0) || ($conf['threshold_max_price'] > 0 && $conf['threshold_min_price'] > 0)) {
                    $sql .= ' AND ( p.`whole_with_taxes` >= '.$conf['threshold_min_price'].' AND p.`whole_with_taxes` <= '.$conf['threshold_max_price'].' )';
                } else if ($conf['threshold_max_price'] == 0) {
                    $sql .= ' AND p.`whole_with_taxes` >= '.$conf['threshold_min_price'];
                } else {
                    $sql .= ' AND p.`whole_with_taxes` <= '.$conf['threshold_max_price'];
                }
            }

            if ($conf['threshold_price'] == 3) {
                if (($conf['threshold_max_price'] == 0 && $conf['threshold_min_price'] == 0) || ($conf['threshold_max_price'] > 0 && $conf['threshold_min_price'] > 0)) {
                    $sql .= ' AND ( p.`price_with_taxes` >= '.$conf['threshold_min_price'].' AND p.`price_with_taxes` <= '.$conf['threshold_max_price'].' )';
                } else if ($conf['threshold_max_price'] == 0) {
                    $sql .= ' AND p.`price_with_taxes` >= '.$conf['threshold_min_price'];
                } else {
                    $sql .= ' AND p.`price_with_taxes` <= '.$conf['threshold_max_price'];
                }
            }
        }

        if (isset($conf['filter_weight']) && $conf['filter_weight']) {
            $sql .= ' AND ( p.`weight` >= '.$conf['min_weight'].' AND p.`weight` <= '.$conf['max_weight'].' )';
        }

        if (isset($conf['features']) && $conf['features']) {
            $array_features = Tools::jsonDecode($conf['features'], true);
            //$features = implode(',', array_keys($array_features));
            //$feature_values = implode(',', $array_features);
            //$sql .= ' AND (fp.id_feature_value IN ('.$feature_values.') AND fp.id_feature IN ('.$features.')) ';
            foreach ($array_features as $key => $fv) {
                $sql .= ' AND p.id_product in
                            (SELECT fp.id_product
                            FROM `'._DB_PREFIX_.'feature_product` fp
                            LEFT JOIN `'._DB_PREFIX_.'feature_value` fv ON fv.id_feature_value = fp.id_feature_value
                            WHERE fv.id_feature_value = '.pSQL($fv).' AND fv.id_feature = '.pSQL($key).') ';
            }
        }

        if (isset($conf['attributes']) && $conf['attributes']) {
            $array_attributes = Tools::jsonDecode($conf['attributes'], true);
            //$attributes = implode(',', array_keys($array_attributes));
            //$attribute_values = implode(',', $array_attributes);
            //$sql .= ' AND (fp.id_feature_value IN ('.$feature_values.') AND fp.id_feature IN ('.$features.')) ';
            foreach ($array_attributes as $key => $av) {
                $av = str_replace(';', ',', $av);
                $sql .= ' AND p.id_product in
                            (SELECT pa.id_product
                            FROM `'._DB_PREFIX_.'attribute_group` ag
                            LEFT JOIN `'._DB_PREFIX_.'attribute` a ON ag.id_attribute_group = a.id_attribute_group
                            LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac ON pac.id_attribute = a.id_attribute
                            LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON pac.id_product_attribute = pa.id_product_attribute
                            WHERE a.id_attribute in ('.pSQL($av).') AND a.id_attribute_group = '.pSQL($key).') ';
            }
        }

        $sql .= ' GROUP BY p.id_product';

        $rq = Db::getInstance(_PS_USE_SQL_SLAVE_)->executesmodule($sql, true, true);
        return ($rq);
    }

    public function recursiveArraySearch($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            if ($key === $needle) {
                return $value;
            } elseif (is_array($value)) {
                $check = $this->recursiveArraySearch($needle, $value);
                if ($check)
                   return $check;
            }
        }
        return false;
    }

    public function returnUnsetCombinations($arrayCombinations = false, $id_product = 0)
    {
        return $arrayCombinations;

        $configs = $this->getConfigs($id_product);
        if (!$configs) {
            return $arrayCombinations;
        }

        foreach ($configs as $conf) {
            $array_attributes = array();
            $array_attr = array();

            if (isset($conf['attributes']) && $conf['attributes']) {
                $array_attr = Tools::jsonDecode($conf['attributes'], true);
            }

            foreach ($array_attr as $key => $a) {
                $array_attr[$key] = explode(';', $a);
            }

            foreach ($array_attr as $k=>$v) {
                foreach ($v as $item) {
                    $array_attributes[] = $item;
                }
            }

            $arrayIdProdAttr = array();
            foreach ($arrayCombinations as $combKey => $comb) {
                foreach ($array_attributes as $key => $attr) {
                    if ($attr == $comb['id_attribute']) {
                        $arrayIdProdAttr[$comb['id_product_attribute']] = isset($arrayIdProdAttr[$comb['id_product_attribute']]) ? $arrayIdProdAttr[$comb['id_product_attribute']] = $arrayIdProdAttr[$comb['id_product_attribute']] + 1 : $arrayIdProdAttr[$comb['id_product_attribute']] = 1;
                    }
                }

                if (isset($conf['filter_stock']) && $conf['filter_stock']) {
                    if ($comb['quantity'] < $conf['min_stock'] || ($conf['max_stock'] > 0 && $comb['quantity'] > $conf['max_stock'])) {
                        continue;
                    }
                }
                /* control precio combinacion, añadir precio combinacion y impacto en el precio */
            }

            foreach ($arrayIdProdAttr as $key => $item) {
                if ($item == count($array_attr)) {
                    foreach ($arrayCombinations as $combKey => $arrayCombination) {
                        if ($arrayCombination['id_product_attribute'] == $key) {
                            unset($arrayCombinations[$combKey]);
                        }
                    }
                }
            }
        }
        return $arrayCombinations;
    }

    public static function getAttributeCombinationsById($id_product, $id_product_attribute, $id_lang)
    {
        if (!Combination::isFeatureActive()) {
            return array();
        }
        $sql = 'SELECT pa.*, product_attribute_shop.*, ag.`id_attribute_group`, ag.`is_color_group`, agl.`name` AS group_name, al.`name` AS attribute_name,
                    a.`id_attribute`
                FROM `'._DB_PREFIX_.'product_attribute` pa
                '.Shop::addSqlAssociation('product_attribute', 'pa').'
                LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac ON pac.`id_product_attribute` = pa.`id_product_attribute`
                LEFT JOIN `'._DB_PREFIX_.'attribute` a ON a.`id_attribute` = pac.`id_attribute`
                LEFT JOIN `'._DB_PREFIX_.'attribute_group` ag ON ag.`id_attribute_group` = a.`id_attribute_group`
                LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = '.(int)$id_lang.')
                LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl ON (ag.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = '.(int)$id_lang.')
                WHERE pa.`id_product` = '.(int)$id_product.'
                AND pa.`id_product_attribute` = '.(int)$id_product_attribute.'
                GROUP BY pa.`id_product_attribute`, ag.`id_attribute_group`
                ORDER BY pa.`id_product_attribute`';
        $res = Db::getInstance()->executeS($sql);

        //Get quantity of each variations
        foreach ($res as $key => $row) {
            $cache_key = $row['id_product'].'_'.$row['id_product_attribute'].'_quantity';

            if (!Cache::isStored($cache_key)) {
                $result = StockAvailable::getQuantityAvailableByProduct($row['id_product'], $row['id_product_attribute']);
                Cache::store(
                    $cache_key,
                    $result
                );
                $res[$key]['quantity'] = $result;
            } else {
                $res[$key]['quantity'] = Cache::retrieve($cache_key);
            }
        }

        return $res;
    }

    public static function isShowableBySchedule($configuration)
    {
        $schedule = Tools::jsonDecode($configuration['schedule']);
        $dayOfWeek = date('w') - 1;
        if ($dayOfWeek < 0) {
            $dayOfWeek = 6;
        }
        if (is_array($schedule)) {
            if (is_object($schedule[$dayOfWeek]) && $schedule[$dayOfWeek]->isActive === true) {
                if ($schedule[$dayOfWeek]->timeFrom <= date('H:i') && $schedule[$dayOfWeek]->timeTill > date('H:i')) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}