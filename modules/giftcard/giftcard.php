<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once dirname(__FILE__) . '/models/Gift.php';

class GiftCard extends Module
{
    public $msg = 0;

    public $translations = array();

    public $field_labels = array();

    public $tab_class = 'GiftCard';

    public $tab_module = 'giftcard';

    public $tpl_version = 'v1_6';

    public function __construct()
    {
        $this->name = 'giftcard';
        $this->tab = 'front_office_features';
        $this->version = '1.7.1';
        $this->author = 'FMM Modules';
        $this->bootstrap = true;
        $this->module_key = '26c0ea03bb9df50375ba49227d63e4d7';
        $this->author_address = '0xcC5e76A6182fa47eD831E43d80Cd0985a14BB095';

        parent::__construct();

        if (true === Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $this->tpl_version = 'v1_7';
        }

        $this->displayName = $this->l('Gift Cards');
        $this->description = $this->l('This module allows you to create gift cards in your shop. Customers can order them and send as a gift to anyone.');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        $this->translations = array(
            'home' => $this->l('To Myself'),
            'sendsomeone' => $this->l('Send to a Friend'),
            'invalid_name' => $this->l('Inavlid recipient name'),
            'required_email' => $this->l('E-mail address required'),
            'invalid_email' => $this->l('Invalid e-mail address'),
            'invalid_message' => $this->l('Invalid Message content'),
            'gift_sending_failed' => $this->l('Gift card sending failed'),
            'gift_card_failure' => $this->l('Cannot sent gift card at this moment, something went wrong'),
        );
        $this->field_labels = array(
            'type' => $this->l('Gift Type'),
            'reciptent' => $this->l('Name'),
            'email' => $this->l('Email'),
            'message' => $this->l('Message'),
        );
    }

    public function install()
    {
        if (!$this->existsTab($this->tab_class)) {
            if (!$this->addTab()) {
                return false;
            }
        }

        include dirname(__FILE__) . '/sql/install.php';

        if (parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('newOrder') &&
            $this->registerHook('ModuleRoutes') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('actionCartSave') &&
            $this->registerHook('actionProductDelete') &&
            $this->registerHook('ActionObjectDeleteAfter') &&
            $this->registerHook('actionProductUpdate') &&
            $this->registerHook('displayProductButtons') &&
            $this->registerHook('displayMyAccountBlock') &&
            $this->registerHook('displayCustomerAccount') &&
            $this->registerHook('displayOrderConfirmation') &&
            $this->registerHook('actionOrderStatusPostUpdate') &&
            $this->registerHook('ActionOrderStatusUpdate') &&
            $this->registerHook('ActionGiftCardDeleteFromCart') &&
            $this->registerHook('displayProductListReviews') &&
            $this->createGiftCategory() &&
            Configuration::updateValue('GIFTCARD_VOUCHER_PREFIX', 'GiftCard_') &&
            Configuration::updateValue('GIFT_APPROVAL_STATUS', 2) &&
            Configuration::updateValue('GIFTCARD_CRON_HOURS', 24) &&
            Configuration::updateValue('GIFTCARD_CRON_KEY', Tools::passwdGen(32))) {
            if (true === Tools::version_compare(_PS_VERSION_, '1.6.0.0', '<')) {
                @copy(_PS_MODULE_DIR_ . 'giftcard/views/img/GiftCard.gif',_PS_MODULE_DIR_ . 'giftcard/GiftCard.gif');
            }
            return true;
        }
        return false;
    }

    public function uninstall()
    {
        if (!$this->removeTab()) {
            return false;
        }

        include dirname(__FILE__) . '/sql/uninstall.php';

        if (parent::uninstall() &&
            $this->unregisterHook('header') &&
            $this->unregisterHook('newOrder') &&
            $this->unregisterHook('backOfficeHeader') &&
            $this->unregisterHook('actionCartSave') &&
            $this->unregisterHook('actionProductDelete') &&
            $this->unregisterHook('ActionObjectDeleteAfter') &&
            $this->unregisterHook('actionProductUpdate') &&
            $this->unregisterHook('displayMyAccountBlock') &&
            $this->unregisterHook('displayCustomerAccount') &&
            $this->unregisterHook('displayProductButtons') &&
            $this->unregisterHook('displayOrderConfirmation') &&
            $this->unregisterHook('actionOrderStatusPostUpdate') &&
            $this->unregisterHook('ActionOrderStatusUpdate') &&
            $this->unregisterHook('ActionGiftCardDeleteFromCart') &&
            $this->unregisterHook('displayProductListReviews') &&
            $this->removeGiftCategory() &&
            Configuration::deleteByName('GIFTCARD_VOUCHER_PREFIX') &&
            Configuration::deleteByName('GIFT_APPROVAL_STATUS') &&
            Configuration::deleteByName('GIFTCARD_CRON_HOURS') &&
            Configuration::deleteByName('GIFTCARD_CRON_KEY')) {
            if (true === Tools::version_compare(_PS_VERSION_, '1.6.0.0', '<') &&
                is_file(_PS_MODULE_DIR_ . 'giftcard/GiftCard.gif')) {
                @unlink(_PS_MODULE_DIR_ . 'giftcard/GiftCard.gif');
            }
            return true;
        }
        return false;
    }

    private function addTab()
    {
        //** @function to add tab in admin backend
        $return = true;
        $tab = new Tab();
        $tab->id_parent = 0;
        $tab->module = $this->name;
        $tab->class_name = $this->tab_class;
        $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->displayName;
        if (true === Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $tab->icon = 'card_giftcard';
        }

        if ($tab->add()) {
            $subtab1 = new Tab();
            $subtab1->id_parent = $tab->id;
            $subtab1->module = $this->name;
            $subtab1->class_name = 'AdminGiftCards';
            $subtab1->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->l('Add Gift Card');
            if (true === (bool) Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $tab->icon = 'redeem';
            }
            $return &= $subtab1->add();

            $subtab2 = new Tab();
            $subtab2->id_parent = $tab->id;
            $subtab2->module = $this->name;
            $subtab2->class_name = 'AdminGiftSettings';
            $subtab2->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->l('Settings');
            if (true === (bool) Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $tab->icon = 'settings';
            }
            $return &= $subtab2->add();
        }
        return $return;
    }

    private function removeTab()
    {
        //** @function to remove the tab
        $id_tab = Tab::getIdFromClassName($this->tab_class);
        if ($id_tab != 0) {
            $tab = new Tab($id_tab);
            $tab->delete();
        }
        $id_tab1 = Tab::getIdFromClassName('AdminGiftCards');
        if ($id_tab1 != 0) {
            $tab = new Tab($id_tab1);
            $tab->delete();
        }
        $id_tab2 = Tab::getIdFromClassName('AdminGiftSettings');
        if ($id_tab2 != 0) {
            $tab = new Tab($id_tab2);
            $tab->delete();
        }

        return true;
    }

    private function existsTab($tab_class)
    {
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT id_tab AS id
            FROM `' . _DB_PREFIX_ . 'tab` WHERE LOWER(`class_name`) = \'' . pSQL((string) $tab_class) . '\'');
        if (count($result) == 0) {
            return false;
        }
        return true;
    }

    public function getContent()
    {
        if (true === (bool)Configuration::get('PS_DISABLE_OVERRIDES')) {
            $this->context->controller->warnings[] = $this->l('Your overrides are disabled. Please enable overrides.');
        }

        $this->html = $this->display(__FILE__, 'views/templates/hook/info.tpl');
        $current_index = $this->context->link->getAdminLink('AdminModules', false);
        $current_token = Tools::getAdminTokenLite('AdminModules');
        $action_url = $current_index . '&configure=' . $this->name . '&token=' . $current_token . '&tab_module=' . $this->tab . '&module_name=' . $this->name;

        if (Tools::isSubmit('updateConfiguration')) {
            if (Tools::getValue('GIFTCARD_VOUCHER_PREFIX') && !Validate::isTablePrefix(Tools::getValue('GIFTCARD_VOUCHER_PREFIX'))) {
                $this->context->controller->errors[] = $this->l('Invalid gift voucher code prefix.');
            } elseif (!Tools::getValue('GIFTCARD_CRON_HOURS') || !Validate::isUnsignedInt(Tools::getValue('GIFTCARD_CRON_HOURS'))) {
                $this->context->controller->errors[] = $this->l('Invalid hour(s) value.');
            } else {
                $approval_states = (Tools::getValue('approval_states')) ? implode(',', Tools::getValue('approval_states')) : '';
                Configuration::updateValue('GIFT_APPROVAL_STATUS', $approval_states);
                Configuration::updateValue('GIFTCARD_VOUCHER_PREFIX', pSQL(Tools::getValue('GIFTCARD_VOUCHER_PREFIX')));
                Configuration::updateValue('GIFTCARD_CRON_HOURS', (int) Tools::getValue('GIFTCARD_CRON_HOURS'));
                if (!Configuration::get('GIFTCARD_CRON_KEY')) {
                    Configuration::updateValue('GIFTCARD_CRON_KEY', Tools::passwdGen(32));
                }
                $this->context->controller->confirmations[] = $this->l('Settings successfully updated');
            }
        }

        $approval_states = (Configuration::get('GIFT_APPROVAL_STATUS') ? explode(',', Configuration::get('GIFT_APPROVAL_STATUS')) : '');
        $this->context->smarty->assign(array(
            'states' => OrderState::getOrderStates($this->context->employee->id_lang),
            'ps_version' => _PS_VERSION_,
            'approval_states' => $approval_states,
            'action_url' => $action_url,
            'GIFTCARD_VOUCHER_PREFIX' => Configuration::get('GIFTCARD_VOUCHER_PREFIX'),
            'GIFTCARD_CRON_HOURS' => Configuration::get('GIFTCARD_CRON_HOURS'),
            'gifts_controller' => $this->context->link->getModuleLink($this->name, 'mygiftcards', array(), true),
            'giftcard_cron' => $this->context->link->getModuleLink(
                $this->name,
                'cron',
                array('giftcard_cron_key' => Configuration::get('GIFTCARD_CRON_KEY')),
                true
            ),
        ));
        return $this->html . $this->display($this->_path, 'views/templates/admin/config.tpl');
    }

    public function hookDisplayBackOfficeHeader()
    {
        $this->context->controller->addCSS($this->_path . 'views/css/admin.css');
    }

    public function hookModuleRoutes()
    {
        return array(
            'module-giftcard-mygiftcards' => array(
                'controller' => 'mygiftcards',
                'rule' => 'gift-cards',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'giftcard',
                ),
            ),
            'module-giftcard-ajax' => array(
                'controller' => 'ajax',
                'rule' => 'process-giftcards',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'giftcard',
                ),
            ),
            'module-giftcard-cron' => array(
                'controller' => 'cron',
                'rule' => 'giftcard-cron',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'giftcard',
                ),
            ),
        );
    }

    public function hookHeader()
    {
        $id_product = (int) Tools::getValue('id_product');
        $controller = Dispatcher::getInstance()->getController();
        $force_ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
        $this->context->smarty->assign(array(
            'base_dir' => _PS_BASE_URL_ . __PS_BASE_URI__,
            'base_dir_ssl' => _PS_BASE_URL_SSL_ . __PS_BASE_URI__,
            'force_ssl' => $force_ssl,
        ));

        if ($controller === 'category' && Tools::getValue('id_category') && Tools::getValue('id_category') == Configuration::get('GIFT_CARD_CATEGORY')) {
            Tools::redirect($this->context->link->getModuleLink(
                $this->name,
                'mygiftcards',
                array(),
                true,
                $this->context->cookie->id_lang,
                $this->context->shop->id
            ));
        }

        if ($controller === 'product' && $id_product && Gift::isExists($id_product)) {
            $this->context->smarty->assign(array('ps_version' => _PS_VERSION_));
            $this->context->controller->addCss($this->_path . 'views/css/goft_card.css');
            if (true === (bool) Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->context->controller->registerJavascript(
                    'modules-giftcard',
                    'modules/' . $this->name . '/views/js/gift_script.js',
                    array('position' => 'bottom', 'priority' => 300)
                );
                return $this->display(__FILE__, $this->tpl_version . '/gift_variables.tpl');
            } else {
                $this->context->smarty->assign(array(
                    'customizationFields' => array(),
                ));
            }
        }
    }

    public function hookDisplayCustomerAccount()
    {
        $this->context->smarty->assign('ps_version', _PS_VERSION_);
        return $this->display(__FILE__, $this->tpl_version . '/my-account.tpl');
    }

    public function hookDisplayMyAccountBlock($params)
    {
        return $this->hookDisplayCustomerAccount($params);
    }

    public function hookDisplayProductListReviews($params)
    {
        if (isset($params) && isset($params['product']) && isset($params['product']['id_product']) && $params['product']['id_product']) {
            if (true === Tools::getValue(_PS_VERSION_, '1.7.0.0', '<') && Gift::isExists($params['product']['id_product'])) {
                $this->context->smarty->assign('id_gift_product', (int) $params['product']['id_product']);
                return $this->display(__FILE__, 'views/templates/hook/v1_6/giftcard_script.tpl');
            }
        }
    }

    public function hookdisplayProductButtons()
    {
        $action = '';
        $card_type = '';
        $preselected_price = '';
        if ($id_product = (int) Tools::getValue('id_product')) {
            $product = new Product($id_product, true, $this->context->language->id, $this->context->shop->id);
        }
        if (Tools::getIsset('action') && Tools::getValue('action') == 'getGiftPrice') {
            $action = 'getGiftPrice';
            $card_type = (string) Tools::getValue('card_type');
            $preselected_price = (float) Tools::getValue('current_price');
        }

        $vals = Gift::getCardValue($product->id);
        if (Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $force_ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
            $this->context->smarty->assign(array(
                'base_dir' => _PS_BASE_URL_ . __PS_BASE_URI__,
                'base_dir_ssl' => _PS_BASE_URL_SSL_ . __PS_BASE_URI__,
                'force_ssl' => $force_ssl,
                )
            );
        }

        if (!empty($vals) && Gift::isExists($id_product)) {
            $price_display = (int) Product::getTaxCalculationMethod((int) $this->context->cookie->id_customer);
            $priceses = explode(',', $vals['card_value']);
            $prices_tax_incl = array();
            $prices_tax_excl = array();

            foreach ($priceses as $price) {
                if ((!$price_display || $price_display == 2) && (int) $product->id_tax_rules_group > 0) {
                    $prices_tax_incl[] = Tools::convertPriceFull($this->calculateGiftPrice($price, $id_product, true), null, $this->context->currency);
                } else {
                    $prices_tax_excl[] = Tools::convertPriceFull($this->calculateGiftPrice($price, $id_product, false), null, $this->context->currency);
                }
            }
            $_value = ((!Configuration::get('PS_TAX') && $product->id_tax_rules_group > 0) ? $prices_tax_incl : $prices_tax_excl);
            if (empty($_value)) {
                $_value = $prices_tax_incl;
            }
            $_prices_tax = ((!$price_display && $product->id_tax_rules_group > 0) ? $prices_tax_incl : $prices_tax_excl);
            if (empty($_prices_tax)) {
                $_prices_tax = $prices_tax_incl;
            }

            $this->context->smarty->assign(array(
                'values' => $_value,
                'prices_tax' => $_prices_tax,
                'type' => $vals['value_type'],
                'pid' => $product->id,
                'ps_version' => _PS_VERSION_,
                'tax_enabled' => Configuration::get('PS_TAX'),
                'product_tax' => (int) $product->id_tax_rules_group,
                'display_tax_label' => 1,
                'priceDisplay' => (int) $price_display,
                'id_module' => $this->id,
                'preselected_price' => $preselected_price,
            ));

            if (!empty($action) && $action == 'getGiftPrice' && !empty($card_type) && $card_type != 'fixed') {
                return $this->display(__FILE__, 'views/templates/hook/' . $this->tpl_version . '/' . $card_type . '.tpl');
            } else {
                return $this->display(__FILE__, 'views/templates/hook/' . $this->tpl_version . '/gift_card.tpl');
            }
        }
    }

    public function hookActionGiftCardDeleteFromCart($params)
    {
        if (isset($params['id_product']) && $params['id_product']) {
            if (Gift::hasBaseProduct($params['id_product']) && in_array(Gift::getGiftCardType($params['id_product']), array('', 'dropdown', 'range'))) {
                Gift::removeCartGP($params['id_cart'], $params['id_product']);
            }
        }
    }

    public function hookActionProductDelete()
    {
        $id_product = (int) Tools::getValue('id_product');
        if ($id_product && Gift::isExists($id_product)) {
            //$product = new Product($id_product);
            //if ($product->delete()) {
            Gift::deleteByProduct($id_product);
            //}
        }
    }

    public function hookActionObjectDeleteAfter($params)
    {
        $giftProduct = (isset($params) && isset($params['object']) ? $params['object'] : null);
        if (isset($giftProduct) && $giftProduct instanceof Product) {
            if (Gift::isExists($giftProduct->id)) {
                Gift::deleteByProduct($giftProduct->id);
            }
        }
    }

    public function hookActionProductUpdate($params)
    {
        $id_product = $id_product = (int) (isset($params) && isset($params['object']) && isset($params['object']->id)) ? $params['object']->id : Tools::getValue('id_product');
        if (Validate::isLoadedObject($product = new Product($id_product)) && Gift::isExists($id_product)) {
            Gift::updateGiftCardField('qty', $id_product, $product->quantity);
            Gift::updateGiftCardField('status', $id_product, $product->active);
            if (in_array(Gift::getGiftCardType($product->id), array('dropdown', 'range'))) {
                Gift::hideGiftProductPrice($product->id);
            }
        }
    }

    public function hookNewOrder($params)
    {
        $cart = $params['cart'];
        $id_cart = $cart->id;
        $id_order = $params['order']->id;
        $id_customer = $cart->id_customer;
        $products = $cart->getProducts();

        if (!empty($products)) {
            foreach ($products as $product) {
                if ((int) $product['id_product'] && Validate::isLoadedObject($giftProduct = new Product((int) $product['id_product']))) {
                    $reference_product = Gift::hasBaseProduct($giftProduct->id, true);
                    if ($reference_product) {
                        Gift::updateCartGC($id_cart, $id_order, $giftProduct->id, $cart->id_customer);
                        //disable child product
                        if (Gift::getGiftCardType($giftProduct->id) != 'fixed') {
                            StockAvailable::setQuantity($giftProduct->id, 0, $product['cart_quantity']);
                        }
                        $giftProduct->active = (Gift::isExists($giftProduct->id) && Gift::getGiftCardType($giftProduct->id) == 'fixed') ? true : false;
                        $giftProduct->update();

                        // decrease parent qty as per ordered qty
                        $deltaQty = (int) StockAvailable::getQuantityAvailableByProduct($reference_product) - (int) $product['cart_quantity'];
                        if (Gift::getGiftCardType($giftProduct->id) != 'fixed') {
                            StockAvailable::setQuantity($reference_product, 0, $deltaQty);
                        }
                    }
                }
            }
        }
    }

    public function hookActionOrderStatusUpdate($params)
    {
        $id_order = (int) $params['id_order'];
        $id_cart = (int) Order::getCartIdStatic($id_order);
        $id_order_state = $params['newOrderStatus']->id;

        $giftOrdered = Gift::getOrderedGiftProducts($id_cart, ' AND has_voucher = 0');
        $approval_states = (Configuration::get('GIFT_APPROVAL_STATUS') ? explode(',', Configuration::get('GIFT_APPROVAL_STATUS')) : array());
        if (!empty($approval_states) && in_array($id_order_state, $approval_states) && count($giftOrdered) > 0) {
            //geberate vouchers for self
            $this->generateVoucher($id_cart, 'home');
            //generate and send voucher directly to friends
            $this->generateVoucher($id_cart, 'sendsomeone');
        }
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        $id_cart = $params['cart']->id;
        $id_order_state = (int) Tools::getValue('id_order_state');

        $giftOrdered = Gift::getOrderedGiftProducts($id_cart, ' AND has_voucher = 0');
        $approval_states = (Configuration::get('GIFT_APPROVAL_STATUS') ? explode(',', Configuration::get('GIFT_APPROVAL_STATUS')) : array());
        if (!empty($approval_states) && in_array($id_order_state, $approval_states) && count($giftOrdered) > 0) {
            //geberate vouchers for self
            $this->generateVoucher($id_cart, 'home');
            //generate and send voucher directly to friends
            $this->generateVoucher($id_cart, 'sendsomeone');
        }
    }

    public function generateVoucher($id_cart, $type = 'home')
    {
        $id_order = Gift::getOrderIdsByCartId($id_cart);
        $order = new Order((int) $id_order);
        $customer = new Customer((int) $order->id_customer);
        $cart_rules = array();
        $id_lang = Context::getContext()->language->id;

        $all_products = Gift::getGiftVoucherProducts($id_cart, $order->id, $type);
        $languages = Language::getLanguages(true);

        if (isset($all_products) && $all_products) {
            foreach ($all_products as $product) {
                if (Gift::hasBaseProduct((int) $product['id_product']) && Validate::isLoadedObject($giftProduct = new Product((int) $product['id_product'], true))) {
                    $prod_detail = Gift::getProductDetail($giftProduct->id, $id_cart, $id_order, $customer->id);
                    $voucher = new CartRule();
                    $vcode = (Configuration::get('GIFTCARD_VOUCHER_PREFIX') ? Configuration::get('GIFTCARD_VOUCHER_PREFIX') : '') . Tools::passwdGen($prod_detail['length'], $prod_detail['vcode_type']);

                    //** Initializing Voucher
                    foreach ($languages as $lang) {
                        $voucher->name[$lang['id_lang']] = $giftProduct->name[$lang['id_lang']];
                    }

                    $voucher->date_from = date('Y-m-d H:i:s', strtotime($prod_detail['from']));
                    $voucher->date_to = date('Y-m-d H:i:s', strtotime($prod_detail['to']));
                    $voucher->quantity = $product['cart_quantity'];
                    $voucher->quantity_per_user = $product['cart_quantity'];
                    $voucher->free_shipping = $prod_detail['free_shipping'];
                    $voucher->reduction_currency = $prod_detail['reduction_currency'];
                    $voucher->active = $prod_detail['status'];
                    //$voucher->date_add              = date('Y-m-d H:i:s');
                    $voucher->reduction_product = $prod_detail['id_discount_product'];
                    $voucher->code = $vcode;
                    $voucher->minimum_amount_currency = $prod_detail['reduction_currency'];

                    $prod_detail['price'] = Product::getPriceStatic((int) $giftProduct->id, false, 0, 2);
                    if ($prod_detail['reduction_type'] == 'amount') {
                        $voucher->reduction_amount = (float) $prod_detail['price'];
                        $voucher->reduction_tax = (int) $prod_detail['reduction_tax'];
                        $voucher->reduction_percent = 0;
                    } elseif ($prod_detail['reduction_type'] == 'percent') {
                        if ($prod_detail['value_type'] == 'range') {
                            $val = explode(',', $prod_detail['card_value']); // range values
                            $pri = explode(',', $prod_detail['reduction_amount']); // percentage values
                            $cal = (float) ((float) $pri[0] / (float) $val[0]) * (float) $prod_detail['price'];
                        } elseif ($prod_detail['value_type'] == 'dropdown') {
                            $val = explode(',', $prod_detail['card_value']); // range values
                            $pri = explode(',', $prod_detail['reduction_amount']); // percentage values

                            foreach ($val as $k => $v) {
                                $value = (float) $v;
                                if ($value == $prod_detail['price']) {
                                    $cal = (float) $pri[$k];
                                    break;
                                }
                            }
                        } else {
                            $cal = (float) $prod_detail['reduction_amount'];
                        }

                        $voucher->reduction_percent = $cal;
                        $voucher->reduction_amount = 0;
                        $voucher->reduction_tax = 0;

                        $voucher->shop_restriction = (Shop::isFeatureActive()) ? 1 : 0;
                    }

                    if ($voucher->add()) {
                        $id_cart_rule = $voucher->id;
                        if (Shop::isFeatureActive()) {
                            Db::getInstance()->delete('cart_rule_shop', '`id_cart_rule` = ' . (int) $id_cart_rule);
                            $product_shops = Gift::getShopsByProduct($giftProduct->id);
                            foreach ($product_shops as $id_shop) {
                                Gift::restrictVoucherToShop($id_cart_rule, $id_shop);
                            }
                        }
                        $id_image = Gift::getId_image($giftProduct->id);
                        Gift::insertCustomer($id_cart_rule, $id_cart, $id_order, $giftProduct->id, $customer->id, $giftProduct->link_rewrite[$id_lang], $id_image);
                        array_push($cart_rules, $id_cart_rule);

                        //set has_vocuher to true
                        Gift::setVoucherFlag($id_cart, $id_order, $giftProduct->id);
                        if ($type == 'sendsomeone') {
                            $newDate = date_diff(date_create($voucher->date_from), date_create($voucher->date_to));
                            $currency = new Currency((int) $voucher->reduction_currency);
                            $value = (isset($voucher->reduction_percent) && $voucher->reduction_percent != '0.00') ? $voucher->reduction_percent . ' %' : Tools::displayPrice($voucher->reduction_amount, $currency);
                            $this->sendGiftCard(
                                $customer->firstname . ' ' . $customer->lastname,
                                $product['friend_name'],
                                $product['friend_email'],
                                $voucher->name[$order->id_lang],
                                $vcode,
                                $newDate->format('%R%a days'),
                                date('Y-m-d H:i:s'),
                                $value,
                                $giftProduct->id,
                                $product['gift_message'],
                                $voucher->quantity
                            );
                        }
                    }
                } else {
                    continue;
                }
            }
            if (isset($cart_rules) && $cart_rules && $type == 'home') {
                Gift::sendAlert($cart_rules, $customer->id);
            }
        }
    }

    protected function calculateGiftPrice($price, $id_product, $use_tax = true, $use_group_reduction = false, $use_reduc = false, $decimals = 2)
    {
        $product = new Product((int) $id_product, true, (int) $this->context->language->id);
        $id_customer = (int) $this->context->customer->id;

        // Initializations
        $id_group = null;
        if ($id_customer) {
            $id_group = Customer::getDefaultGroupId((int) $this->context->customer->id);
        }

        if (!$id_group) {
            $id_group = (int) Group::getCurrent()->id;
        }

        // Tax
        $id_address = $this->context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
        $address = new Address($id_address);

        $id_shop = (int) $this->context->shop->id;
        $id_currency = (int) $this->context->currency->id;
        $id_country = (int) $address->id_country;

        $tax_manager = TaxManagerFactory::getManager($address, Product::getIdTaxRulesGroupByIdProduct((int) $id_product, $this->context));
        $product_tax_calculator = $tax_manager->getTaxCalculator();

        // Add Tax
        if ($use_tax) {
            $price = $product_tax_calculator->addTaxes($price);
        }

        // Eco Tax
        if (!isset($product->ecotax) && $product->ecotax) {
            if ($id_currency) {
                $ecotax = Tools::convertPrice($product->ecotax, $id_currency);
            }
            if ($use_tax) {
                // reinit the tax manager for ecotax handling
                $tax_manager = TaxManagerFactory::getManager($address, (int) Configuration::get('PS_ECOTAX_TAX_RULES_GROUP_ID'));
            }
            $ecotax_tax_calculator = $tax_manager->getTaxCalculator();
            $price += $ecotax_tax_calculator->addTaxes($ecotax);
        }

        // Reduction
        $specific_price = SpecificPrice::getSpecificPrice(
            (int) $id_product,
            $id_shop,
            $id_currency,
            $id_country,
            $id_group,
            1
        );

        $specific_price_reduction = 0;
        if ($specific_price && $use_reduc) {
            if ($specific_price['reduction_type'] == 'amount') {
                $reduction_amount = $specific_price['reduction'];
                if (!$specific_price['id_currency']) {
                    $reduction_amount = Tools::convertPrice($reduction_amount, $id_currency);
                }

                $specific_price_reduction = $reduction_amount;

                // Adjust taxes if required
                if (!$use_tax && $specific_price['reduction_tax']) {
                    $specific_price_reduction = $product_tax_calculator->removeTaxes($specific_price_reduction);
                }
                if ($use_tax && !$specific_price['reduction_tax']) {
                    $specific_price_reduction = $product_tax_calculator->addTaxes($specific_price_reduction);
                }
            } else {
                $specific_price_reduction = $price * $specific_price['reduction'];
            }
        }

        if ($use_reduc) {
            $price -= $specific_price_reduction;
        }

        // Group reduction
        if ($use_group_reduction) {
            $reduction_from_category = GroupReduction::getValueForProduct($id_product, $id_group);
            if ($reduction_from_category !== false) {
                $group_reduction = $price * (float) $reduction_from_category;
            } else {
                $group_reduction = (($reduc = Group::getReductionByIdGroup($id_group)) != 0) ? ($price * $reduc / 100) : 0;
            }
            $price -= $group_reduction;
        }

        $price = Tools::ps_round($price, $decimals);
        if ($price < 0) {
            $price = 0;
        }
        return $price;
    }

    private function setPrice($id_product, $new_price)
    {
        if (!empty($id_product) && !empty($new_price) && $new_price != 0) {
            $product = new Product($id_product);
            $product->price = $new_price;
            $product->update(true);
        }
    }

    protected function createGiftCategory()
    {
        if (!Configuration::get('GIFT_CARD_CATEGORY')) {
            $languages = Language::getLanguages(false);
            $gift_category = new Category();
            $gift_category->active = true;
            $gift_category->id_parent = Configuration::get('PS_HOME_CATEGORY');
            foreach ($languages as $lang) {
                $gift_category->name[$lang['id_lang']] = $this->l('Gift Cards');
                $gift_category->link_rewrite[$lang['id_lang']] = Tools::str2url($gift_category->name[$lang['id_lang']]);
            }
            if ($gift_category->add()) {
                Configuration::updateValue('GIFT_CARD_CATEGORY', $gift_category->id);
                return true;
            } else {
                return false;
            }
        }
    }

    protected function removeGiftCategory()
    {
        if (Configuration::get('GIFT_CARD_CATEGORY')) {
            $gift_category = new Category(Configuration::get('GIFT_CARD_CATEGORY'));
            if ($gift_category->delete()) {
                Configuration::deleteByName('GIFT_CARD_CATEGORY');
                return true;
            } else {
                return false;
            }
        }
    }

    public function processDuplicate($old_id_product, $price = 0.0)
    {
        if (Validate::isLoadedObject($product = new Product((int) $old_id_product))) {
            $id_product_old = $product->id;
            if (empty($product->price) && Shop::getContext() == Shop::CONTEXT_GROUP) {
                $shops = ShopGroup::getShopsFromGroup(Shop::getContextShopGroupID());
                foreach ($shops as $shop) {
                    if ($product->isAssociatedToShop($shop['id_shop'])) {
                        $product_price = new Product($id_product_old, false, null, $shop['id_shop']);
                        $product->price = $product_price->price;
                    }
                }
            }

            // exclude tax price if tax is enabled - tax incl/ecxl option compatibility
            $priceDisplay = (int) Product::getTaxCalculationMethod((int) Context::getContext()->cart->id_customer);
            $tax = (!$priceDisplay || $priceDisplay == 2) ? true : false;
            $taxRate = Gift::parseFloatValue(Tax::getProductTaxRate($old_id_product));
            if ($tax && $taxRate) {
                $price = Tools::ps_round($price / (1 + ((float)$taxRate / 100)), 3);
            }

            unset($product->id);
            unset($product->id_product);
            $product->indexed = 0;
            $product->active = 1;
            $product->customizable = 1;
            $product->visibility = 'none';
            $product->reference = 'GIFT_PRODUCT_' . $product->reference;
            $product->price = (float) $price;
            if ($product->add()
                && Category::duplicateProductCategories($id_product_old, $product->id)
                && Product::duplicateSuppliers($id_product_old, $product->id)
                && ($combination_images = Product::duplicateAttributes($id_product_old, $product->id)) !== false
                && GroupReduction::duplicateReduction($id_product_old, $product->id)
                && Product::duplicateAccessories($id_product_old, $product->id)
                && Product::duplicateFeatures($id_product_old, $product->id)
                && Pack::duplicate($id_product_old, $product->id)
                && Product::duplicateCustomizationFields($id_product_old, $product->id)
                && Product::duplicateTags($id_product_old, $product->id)
                && Product::duplicateDownload($id_product_old, $product->id)) {
                if ($product->hasAttributes()) {
                    Product::updateDefaultAttribute($product->id);
                } else {
                    Product::duplicateSpecificPrices($id_product_old, $product->id);
                }

                if (!Tools::getValue('noimage') && !Image::duplicateProductImages($id_product_old, $product->id, $combination_images)) {
                    $this->context->controller->errors[] = $this->l('An error occurred while copying images.');
                } else {
                    Hook::exec('actionProductAdd', array('id_product' => (int) $product->id, 'product' => $product));
                    if (in_array($product->visibility, array('both', 'search')) && Configuration::get('PS_SEARCH_INDEXATION')) {
                        Search::indexation(false, $product->id);
                    }
                    //set stock equals to parent
                    StockAvailable::setQuantity($product->id, 0, StockAvailable::getStockAvailableIdByProductId($old_id_product));
                    return $product->id;
                }
            } else {
                $this->context->controller->errors[] = $this->l('An error occurred while creating an object.');
            }
        }
    }

    public function sendGiftCard($seder, $receiverName, $receiverEmail, $giftcard_name, $vcode, $expire_date, $date, $value, $id_product, $message = '', $qty = 1)
    {
        $image = '';
        $id_lang = (int) Context::getContext()->language->id;
        if (Validate::isLoadedObject($product = new Product((int) $id_product, false, $id_lang))) {
            $id_image = Gift::getId_image($product->id);
            $type = (true === Tools::version_compare(_PS_VERSION_, '1.7.0.0', '<')) ? ImageType::getFormatedName('large') : ImageType::getFormattedName('large');
            $image = '<img src="' . $this->context->link->getImageLink($product->link_rewrite, $id_image, $type) . '">';
        }
        $templateVars = array(
            '{gift_image}' => $image,
            '{sender}' => $seder,
            '{rec_name}' => $receiverName,
            '{giftcard_name}' => $giftcard_name,
            '{vcode}' => $vcode,
            '{expire_date}' => $expire_date,
            '{date}' => $date,
            '{value}' => $value,
            '{message}' => $message,
            '{quantity}' => $qty,
            '{shop_link}' => _PS_BASE_URL_ . __PS_BASE_URI__,
        );

        if (!empty($receiverEmail)) {
            $res = Mail::Send(
                (int) $id_lang,
                'gift',
                Mail::l('You received a Gift Card', (int) $id_lang),
                $templateVars,
                $receiverEmail,
                null,
                null,
                null,
                null,
                null,
                _PS_MODULE_DIR_ . 'giftcard/mails/',
                false
            );
            return $res;
        }
        return false;
    }
}
