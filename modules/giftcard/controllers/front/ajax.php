<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*/

header('Content-type: text/javascript');
class GiftCardAjaxModuleFrontController extends ModuleFrontController
{
    public $ajax = false;

    public function __construct()
    {
        parent::__construct();
        $this->context = Context::getContext();
        $this->ajax = Tools::getValue('ajax', 0);
    }

    public function displayAjaxProductExists()
    {
        $id_product = Tools::jsonDecode(Tools::getValue('id_product'));
        if (Gift::isExists($id_product)) {
            $html = $id_product;
        } else {
            $html = 0;
        }
        echo $html;
    }

    public function displayAjaxGetGiftPrice()
    {
        
        header('Content-Type: application/json');
        if (true == Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $this->ajaxDie(json_encode(array(
                'gift_prices' => $this->module->hookdisplayProductButtons(),
            )));
        }
    }

    public function displayAjaxGetGiftType()
    {
        $gift_type = '';
        if (true == Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $id_product = (int)Tools::getValue('id_product');
            if ($id_product) {
                $gift_type = Gift::getCardValue($id_product)? Gift::getCardValue($id_product)['value_type'] : '';
            }
        }
        $this->ajaxDie(json_encode(array('gift_type' => $gift_type)));
    }
}
