<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*/

class GiftcardCronModuleFrontController extends ModuleFrontController
{
    public $useSSL = true;
    public function __construct()
    {
        parent::__construct();
        $this->context = Context::getContext();
    }

    public function initContent()
    {
        parent::initContent();
        $result = array('deleted' => 0, 'skip' => 0);

        if (!Tools::getIsset('giftcard_cron_key')) {
            $this->context->controller->errors[] = $this->module->l('cron key not set.', 'cron');
        } elseif (Configuration::get('GIFTCARD_CRON_KEY') !== Tools::getValue('giftcard_cron_key')) {
            $this->context->controller->errors[] = $this->module->l('cron key not set.', 'cron');
        } else {
            $hours = (int)Configuration::get('GIFTCARD_CRON_HOURS');
            $abandonedGifts = Gift::getAbandonedGifts($hours);

            if (isset($abandonedGifts) && $abandonedGifts) {
                foreach ($abandonedGifts as $gifts) {
                    if ($gifts['id_cart'] && $gifts['id_product']) {
                        if (Gift::getGiftCardType($gifts['id_product']) != 'fixed' &&
                            Validate::isLoadedObject($product = new Product($gifts['id_product']))) {
                            if ($product->delete()) {
                                //Gift::removeFriendDetails($gifts['id_cart']);
                                $result['deleted']++;
                            } else {
                                $result['skip']++;
                            }
                        }
                    }
                }
            }
        }
        $this->context->smarty->assign('result', $result);
        if (Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $this->setTemplate('module:'.$this->module->name.'/views/templates/front/v1_7/cron.tpl');
        } else {
            return $this->setTemplate('v1_6/cron.tpl');
        }
    }
}
