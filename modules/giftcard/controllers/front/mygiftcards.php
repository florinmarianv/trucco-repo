<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*/

/**
 * @since 1.5.0
 */

class GiftCardMyGiftCardsModuleFrontController extends ModuleFrontController
{
    public $html;

    public $errors = '';

    protected $my_giftcards = false;

    protected $tpl = 'v1_6/giftcards.tpl';

    public function __construct()
    {
        parent::__construct();
        $this->display_column_left = false;
        $this->display_column_right = false;
        $this->context = Context::getContext();
    }

    public function init()
    {
        parent::init();

        $this->my_giftcards = (Tools::getIsset('my_gifts') || Tools::getIsset('pending_gifts'))? true : false;

        $this->setGiftcardTemplate();
    }

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
        if ($this->my_giftcards) {
            if ($this->context->customer->isLogged()) {
                return $this->displayMyGiftCards();
            } else {
                Tools::redirect('index.php?controller=authentication&back='.urlencode($this->context->link->getModuleLink('giftcard', 'mygiftcards')));
            }
        }
        return $this->displayGiftCards();
    }

    public function postProcess()
    {
        parent::postprocess();
        $this->html = 0;
        $id_lang = $this->context->cookie->id_lang;
        if (Tools::isSubmit('send_giftcard')) {
            $id_cart_rule   = (int)Tools::getValue('id_coupen');
            $id_gift_product = (int)Tools::getValue('id_gift_product');
            $friend_name    = (string)Tools::getValue('friend_name');
            $friend_email   = (string)Tools::getValue('friend_email');
            $friend_message = (string)Tools::getValue('friend_message');

            $now = (string)date('Y-m-d H:i:s');

            $mycoupon = Gift::getCartsRuleById($id_cart_rule, $id_lang);
            $old_voucher = new CartRule((int)$id_cart_rule);
            $gift_coupon = new CartRule();

            $date_from = date_create($mycoupon['date_from']);
            $date_to = date_create($mycoupon['date_to']);
            $newDate = date_diff($date_from, $date_to);

            $gift_coupon->name[$id_lang]            = $mycoupon['name'];
            $gift_coupon->date_from                 = $now;
            $gift_coupon->date_to                   = date('Y-m-d H:i:s', strtotime($now.' '.$newDate->format('%R%a days')));
            $gift_coupon->quantity                  = 1;
            $gift_coupon->quantity_per_user         = 1;
            $gift_coupon->free_shipping             = $mycoupon['free_shipping'];
            $gift_coupon->reduction_currency        = $mycoupon['reduction_currency'];
            $gift_coupon->active                    = $mycoupon['active'];
            $gift_coupon->date_add                  = $mycoupon['date_add'];
            $gift_coupon->reduction_product         = $mycoupon['reduction_product'];
            $gift_coupon->code                      = (Configuration::get('GIFTCARD_VOUCHER_PREFIX')? Configuration::get('GIFTCARD_VOUCHER_PREFIX') :'').Tools::passwdGen(mt_rand(4, 14));
            $gift_coupon->minimum_amount_currency   = $mycoupon['minimum_amount_currency'];
            $gift_coupon->reduction_amount          = $mycoupon['reduction_amount'];
            $gift_coupon->reduction_tax             = $mycoupon['reduction_tax'];
            $gift_coupon->reduction_percent         = $mycoupon['reduction_percent'];
            $gift_coupon->shop_restriction          = $old_voucher->shop_restriction;

            if ($gift_coupon->add()) {
                CartRule::copyConditions($id_cart_rule, $gift_coupon->id);
                $email = trim($friend_email);
                if (!validate::isName($friend_name)) {
                    $this->context->controller->errors[] = $this->module->translations['invalid_name'];
                }
                if (empty($email)) {
                    $this->context->controller->errors[] = $this->module->translations['required_email'];
                } elseif (!Validate::isEmail($email)) {
                    $this->context->controller->errors[] = $this->module->translations['invalid_email'];
                } elseif (!Validate::isMessage($friend_message)) {
                    $this->context->controller->errors[] = $this->module->translations['invalid_message'];
                } elseif (!$this->context->controller->errors) {
                    $currency = new Currency((int)$gift_coupon->reduction_currency);
                    $sender_name = $this->context->cookie->customer_firstname.' '.$this->context->cookie->customer_lastname;
                    $value = (isset($mycoupon['reduction_percent']) && $mycoupon['reduction_percent'] != '0.00')? $mycoupon['reduction_percent'].' %' : Tools::displayPrice($mycoupon['reduction_amount'], $currency);

                    if (!$this->module->sendGiftCard(
                        $sender_name,
                        $friend_name,
                        $friend_email,
                        $gift_coupon->name[$id_lang],
                        $gift_coupon->code,
                        $newDate->format('%R%a days'),
                        $now,
                        $value,
                        $id_gift_product,
                        $friend_message
                    )) {
                        $this->context->controller->errors[] = $this->module->translations['gift_sending_failed'];
                    } else {
                        $qty = $mycoupon['quantity'];
                        if (!empty($mycoupon['quantity'])) {
                            $qty = $qty - 1;
                        }

                        Db::getInstance()->update('cart_rule', array('quantity' => (int)$qty, 'quantity_per_user' => (int)$qty), 'id_cart_rule ='.(int)$mycoupon['id_cart_rule']);
                        Tools::redirect($this->context->link->getModuleLink(
                            $this->module->name,
                            'mygiftcards',
                            array('my_gifts' => 'show', 'msg' => (int)$this->html),
                            true
                        ));
                    }
                } else {
                    //remove created voucher
                    $gift_coupon->delete();
                }
            } else {
                $this->context->controller->errors[] = $this->module->translations['gift_card_failure'];
            }
        }
    }

    protected function setGiftcardTemplate()
    {
        if ($this->my_giftcards) {
            if (true == Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                if (Tools::getIsset('my_gifts')) {
                    $this->tpl = 'module:'.$this->module->name.'/views/templates/front/v1_7/mygiftcards.tpl';
                } else {
                    $this->tpl = 'module:'.$this->module->name.'/views/templates/front/v1_7/pending_giftcards.tpl';
                }
            } else {
                if (Tools::getIsset('my_gifts')) {
                    $this->tpl = 'v1_6/mygiftcards.tpl';
                } else {
                    $this->tpl = 'v1_6/pending_giftcards.tpl';
                }
            }
        } else {
            if (true == Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->tpl = 'module:'.$this->module->name.'/views/templates/front/v1_7/giftcards.tpl';
            } else {
                $this->tpl = 'v1_6/giftcards.tpl';
            }
        }
    }

    public function displayMyGiftCards()
    {
        $model = new Gift();
        $id_lang = $this->context->cookie->id_lang;

        $id_customer = (int)$this->context->cookie->id_customer;
        $id_lang = (int)$this->context->cookie->id_lang;
        $coupen = $model->getVoucherByCustomerId($id_customer, true, $id_lang);

        //$pending_gc = $model->getOrderedGiftCards($id_customer);
        $this->context->smarty->assign(array(
            'id_customer'   => $id_customer,
            'coupens'       => $coupen,
            'errors'        => $this->context->controller->errors,
            'msg'           => $this->html,
            'ps_version'    => _PS_VERSION_,
            'pending_cards' => $model->getPendingGiftCards($id_customer, $id_lang),
        ));
        $this->setTemplate($this->tpl);
    }

    public function displayGiftCards()
    {
        $productTags = array();
        $giftProducts = array();
        $idLang = $this->context->cookie->id_lang;
        $giftCategory = new Category(Configuration::get('GIFT_CARD_CATEGORY'));
        $products = $giftCategory->getProducts($idLang, 1, 100);
        if (isset($products) && $products) {
            foreach ($products as $key => $product) {
                $_in_gift_table = (int)$this->lookForGiftTable($product['id_product']);
                if (Gift::isInOrderCart($product['id_product']) && $_in_gift_table <= 0) {// && in_array(Gift::getGiftCardType($product['id_product']), array('dropdown', 'range'))) {
                    unset($products[$key]);
                } else {
                    $giftProducts[$key] = $product;
                    $tags = Tag::getProductTags((int)$product['id_product']);
                    $giftProducts[$key]['id_image'] = Gift::getId_image($product['id_product']);
                    $giftProducts[$key]['tags'] = ($tags && count($tags) > 0)? array_shift($tags) : array();
                    $productTags[] = $giftProducts[$key]['tags'];
                    $giftProducts[$key]['tags'] = (count($giftProducts[$key]['tags']) > 0)? implode(',', $giftProducts[$key]['tags']) : '';
                }
            }
        }

        $filterTags = array();
        if (isset($productTags) && $productTags) {
            foreach ($productTags as $ptags) {
                if (isset($ptags) && $ptags) {
                    foreach ($ptags as $tag) {
                        $filterTags[$tag] = $tag;
                    }
                }
            }
        }

        $this->context->smarty->assign(array(
            'filterTags' => $filterTags,
            'giftProducts' => $giftProducts
        ));
        $this->setTemplate($this->tpl);
    }

    public function setMedia($newTheme = false)
    {
        parent::setMedia($newTheme);
        Media::addJsDef(array('isGiftListingPage' => !$this->my_giftcards));
        $this->addCSS(_PS_MODULE_DIR_.$this->module->name.'/views/css/gift_card.css');
        if (false === $this->my_giftcards) {
            $this->addCSS(_PS_MODULE_DIR_.$this->module->name.'/views/css/gift_products.css');
            $this->addJS(_PS_MODULE_DIR_.$this->module->name.'/views/js/jquery.filterizr.min.js');
        }
        $this->addJS(_PS_MODULE_DIR_.$this->module->name.'/views/js/my_gifts.js');
    }

    private function lookForGiftTable($id)
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT `id_gift_card`
		FROM '._DB_PREFIX_.'gift_card
		WHERE `id_product` = '.(int)$id);
    }
}
