<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
 */

class AdminGiftCardsController extends ModuleAdminController
{
    public $msg = 0;
    public $gift = array();
    public function __construct()
    {
        $this->table = 'gift_card';
        $this->className = 'Gift';
        $this->identifier = 'id_gift_card';
        $this->context = Context::getContext();
        $this->deleted = false;
        $this->bootstrap = true;

        $this->submit_action = 'SaveGift' . $this->table;

        $this->addRowAction('edit');
        $this->addRowAction('view');
        $this->addRowAction('delete');

        parent::__construct();

        $this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));

        $this->_group = 'GROUP BY a.' . $this->identifier;
        $this->_select = 'pl.name';
        $this->_join = '
        LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (a.`id_product` = pl.`id_product` AND pl.id_lang = ' . (int) $this->context->employee->id_lang . ')';

        $this->fields_list = array(
            'id_gift_card' => array(
                'title' => '#',
                'width' => 25,
            ),
            'id_product' => array(
                'title' => $this->l('Photo'),
                'width' => 'auto',
                'callback' => 'getProductPhoto',
                'orderby' => false,
                'filter' => false,
                'search' => false,
            ),
            'name' => array(
                'title' => $this->l('Name'),
                'width' => 'auto',
                'callback' => 'getProductName',
                'filter_key' => 'pl!name',
                'havingFilter' => true,
            ),
            'qty' => array(
                'title' => $this->l('Quantity'),
                'type' => 'int',
                'callback' => 'getProductQuantity',
            ),
            'value_type' => array(
                'title' => $this->l('Value Type'),
            ),
            'reduction_type' => array(
                'title' => $this->l('Discount Type'),
            ),
            'reduction_currency' => array(
                'title' => $this->l('Discount Currency'),
                'callback' => 'getCurrencyCode',
                'search' => false,
                'orderby' => false,
            ),
            'status' => array(
                'title' => $this->l('Status'),
                'width' => 70,
                'active' => 'status',
                'type' => 'bool',
                'align' => 'center',
                'orderby' => false,
            ),
            'to' => array(
                'title' => $this->l('Expiry Date'),
                'width' => 40,
                'align' => 'center',
                'type' => 'datetime',
            ),
        );
    }

//    public function init()
    //    {
    //        parent::init();
    //        Shop::addTableAssociation($this->table, array('type' => 'shop'));
    //        if (Shop::getContext() == Shop::CONTEXT_SHOP) {
    //            $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'gift_card_shop` sa ON (a.`id_gift_card` = sa.`id_gift_card` AND sa.id_shop = '.(int)$this->context->shop->id.') ';
    //        }
    //        if (Shop::getContext() == Shop::CONTEXT_SHOP && Shop::isFeatureActive()) {
    //            $this->_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
    //        }
    //    }

    public function getProductName($echo, $row)
    {
        if (isset($row) && isset($row['id_product'])) {
            $product = new Product($row['id_product'], false, $this->context->language->id);
            return $product->name;
        }
    }

    public function getProductQuantity($qty, $row)
    {
        if (isset($row) && isset($row['id_product'])) {
            return (int) StockAvailable::getQuantityAvailableByProduct($row['id_product']);
        }
        return (int) $qty;
    }

    public function getProductPhoto($id_product)
    {
        if ($id_product) {
            $product = new Product($id_product, false, $this->context->language->id);
            $cover = Product::getCover($product->id);
            if (isset($cover) && $cover) {
                $path_to_image = 'p/' . Image::getImgFolderStatic($cover['id_image']) . (int) $cover['id_image'] . '.jpg';
                if (is_file(_PS_IMG_DIR_ . $path_to_image)) {
                    return '<img class="img img-thumbnail" src="' . __PS_BASE_URI__ . 'img/' . $path_to_image . '" width="80px">';
                }
            }
        }
        return '<img class="img img-thumbnail" src="' . __PS_BASE_URI__ . 'modules/' . $this->module->name . '/views/img/no_image.png" width="80px">';
    }

    public function getCurrencyCode($id_currecny)
    {
        if ($id_currecny) {
            $currency = new Currency($id_currecny);
            return $currency->iso_code;
        }
    }

    public function initPageHeaderToolbar()
    {
        if (Tools::version_compare(_PS_VERSION_, '1.6.0.0', '>=')) {
            if (empty($this->display)) {
                $this->page_header_toolbar_btn['new'] = array(
                    'href' => self::$currentIndex . '&add' . $this->table . '&token=' . $this->token,
                    'desc' => $this->l('Add Gift Card'),
                    'icon' => 'process-icon-new',
                );
            }
            parent::initPageHeaderToolbar();
        }
    }

    public function postProcess()
    {
        $id_lang = $this->context->cookie->id_lang;
        if (Tools::isSubmit('SaveGift' . $this->table)) {
            return $this->addGiftCard();
        } elseif (Tools::isSubmit('status' . $this->table)) {
            $id_gift_card = (int) Tools::getValue('id_gift_card');
            if (!Validate::isLoadedObject($giftCard = new Gift($id_gift_card))) {
                $this->errors[] = $this->l('Gift card not found.');
            } else {
                if (!isset($giftCard->id_product) || !Validate::isLoadedObject($giftProduct = new Product($giftCard->id_product))) {
                    $this->errors[] = $this->l('Gift card product not found.');
                } else {
                    $giftCard->status = !$giftCard->status;
                    $giftProduct->active = $giftCard->status;
                    if ($giftCard->update()) {
                        $giftProduct->update();
                        return $this->confirmations[] = $this->l('Status updated successfully.');
                    }
                    return $this->errors[] = $this->l('Gift card product not found.');
                }
            }
        }
        parent::postProcess();
    }

    public function renderView()
    {
        parent::renderView();
        $id_product = Gift::getIdProductFromCard((int) Tools::getValue('id_gift_card'));
        if ($id_product && Validate::isLoadedObject($product = new Product((int) $id_product))) {
            if (true === Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $productLink = $this->context->link->getAdminLink('AdminProducts', true, array('id_product' => $id_product));
            } else {
                $productLink = $this->context->link->getAdminLink('AdminProducts') . '&updateproduct&id_product=' . (int) $id_product;
            }
            Tools::redirectAdmin($productLink);
        } else {
            $this->errors[] = $this->l('Gift Product not found in catalog.');
        }
    }

    public function renderList()
    {
        return parent::renderList();
    }

    public function renderForm()
    {
        $obj = $this->loadObject(true);
        $defaultCurrencyObject = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));
        $back = Tools::safeOutput(Tools::getValue('back', ''));
        if (empty($back)) {
            $back = self::$currentIndex . '&token=' . $this->token;
        }

        if (Tools::getValue('id_gift_card')) {
            $gift = new Gift(Tools::getValue('id_gift_card'));
            $this->gift = Gift::getGiftCard($gift->id_product, $gift->id, $this->context->language->id);
        }

        $radio = (Tools::version_compare(_PS_VERSION_, '1.6.0.0', '>=')) ? 'switch' : 'radio';
        $currencies = array();
        foreach (Currency::getCurrenciesByIdShop($this->context->shop->id) as $currency) {
            $currencies[] = array(
                'id_option' => $currency['id_currency'],
                'name' => $currency['name'] . ((isset($currency['sign']) && $currency['sign']) ? sprintf(' (%s)', $currency['sign']) : sprintf(' (%s)', $currency['iso_code'])),
            );
        }
        $this->fields_form = array(
            'tabs' => array(
                'product' => $this->l('Gift Product'),
                'voucher' => $this->l('Gift Voucher'),
            ),
            'legend' => array(
                'title' => (Tools::getValue('id_gift_card')) ? $this->l('Edit Gift Product') : $this->l('Add Gift Product'),
                'icon' => 'icon-conf',
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'id_gift_card',
                    'tab' => 'product',
                ),
                array(
                    'type' => 'hidden',
                    'name' => 'id_product',
                    'tab' => 'product',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Gift Card Name'),
                    'desc' => $this->l('Your Product name will be considered as your Gift card name.'),
                    'name' => 'card_name',
                    'lang' => true,
                    'tab' => 'product',
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Gift Card Description'),
                    'name' => 'product_description',
                    'lang' => true,
                    'class' => 'rte autoload_rte',
                    'tab' => 'product',
                ),
                array(
                    'type' => 'tags',
                    'label' => $this->l('Gift Card Tags'),
                    'desc' => $this->l('Use a comma to create separate tags. E.g.: birthday, christmas, friendship..'),
                    'name' => 'tags',
                    'lang' => true,
                    'id' => 'gift_card_tags',
                    'tab' => 'product',
                ),
                array(
                    'type' => 'giftimage',
                    'name' => 'giftimage',
                    'label' => $this->l('Gift Card Image'),
                    'tab' => 'product',
                ),
                array(
                    'type' => 'radio',
                    'name' => 'value_type',
                    'label' => $this->l('Card Type'),
                    'col' => 4,
                    'values' => array(
                        array('id' => 'dropdown', 'value' => 'dropdown', 'label' => $this->l('Drop Down')),
                        array('id' => 'fixed', 'value' => 'fixed', 'label' => $this->l('Fixed Price')),
                        array('id' => 'range', 'value' => 'range', 'label' => $this->l('Range Type')),
                    ),
                    'tab' => 'product',
                ),
                array(
                    'type' => 'card_value',
                    'name' => 'card_value',
                    'label' => $this->l('Card Value'),
                    'prefix' => $defaultCurrencyObject->iso_code,
                    'tab' => 'product',
                ),
                array(
                    'type' => 'text',
                    'name' => 'qty',
                    'label' => $this->l('Quantity'),
                    'col' => 2,
                    'tab' => 'product',
                ),
                array(
                    'type' => 'tax_rules_group',
                    'name' => 'id_tax_rules_group',
                    'label' => $this->l('Tax Rule'),
                    'tab' => 'product',
                ),
                array(
                    'type' => $radio,
                    'class' => 't',
                    'is_bool' => true,
                    'label' => $this->l('Status:'),
                    'name' => 'status',
                    'values' => array(
                        array(
                            'id' => 'status_on',
                            'value' => 1,
                            'label' => $this->l('Enabled'),
                        ),
                        array(
                            'id' => 'status_off',
                            'value' => 0,
                            'label' => $this->l('Disabled'),
                        ),
                    ),
                    'tab' => 'product',
                ),
                //voucher tab
                array(
                    'type' => 'radio',
                    'name' => 'apply_discount',
                    'label' => $this->l('Discount Type'),
                    'col' => 4,
                    'values' => array(
                        array('id' => 'apply_discount_percent', 'value' => 'percent', 'label' => $this->l('Percent (%)')),
                        array('id' => 'apply_discount_amount', 'value' => 'amount', 'label' => $this->l('Amount') . '<b> ' . $this->l('(card values will bes used)') . '</b>'),
                    ),
                    'tab' => 'voucher',
                ),
                array(
                    'type' => 'discount_value',
                    'name' => 'discount_value',
                    'label' => $this->l('Value'),
                    'tab' => 'voucher',
                ),
                array(
                    'type' => 'select',
                    'col' => 4,
                    'label' => $this->l('Voucher Tax'),
                    'name' => 'reduction_tax',
                    'id' => 'voucher_reduction_tax',
                    'options' => array(
                        'query' => array(
                            array('id_option' => 0, 'name' => $this->l('Tax excluded')),
                            array('id_option' => 1, 'name' => $this->l('Tax included')),
                        ),
                        'id' => 'id_option',
                        'name' => 'name',
                    ),
                    'tab' => 'voucher',
                ),
                array(
                    'type' => 'radio',
                    'name' => 'apply_discount_to',
                    'label' => $this->l('Apply Discount To'),
                    'col' => 4,
                    'values' => array(
                        array('id' => 'apply_discount_to_order', 'value' => 'order', 'label' => $this->l('Order (without shipping)')),
                        array('id' => 'apply_discount_to_product', 'value' => 'specific', 'label' => $this->l('Specific Product')),
                    ),
                    'tab' => 'voucher',
                ),
                array(
                    'type' => 'product_search',
                    'name' => 'reductionProductFilter',
                    'hint' => $this->l('Discount will be applied to specific product only.'),
                    'label' => $this->l('Product'),
                    'cols' => 6,
                    'id' => 'reductionProductFilter',
                    'tab' => 'voucher',
                ),
                array(
                    'type' => 'select',
                    'col' => 3,
                    'label' => $this->l('Discount Currency'),
                    'name' => 'reduction_currency',
                    'options' => array(
                        'query' => $currencies,
                        'id' => 'id_option',
                        'name' => 'name',
                    ),
                    'tab' => 'voucher',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Coupon code Length'),
                    'desc' => $this->l('Minimum length is 4 and maximum length is 20.'),
                    'hint' => $this->l('Specified length of code will be generated, default length is 14.'),
                    'name' => 'length',
                    'col' => 2,
                    'tab' => 'voucher',
                ),
                array(
                    'type' => 'radio',
                    'name' => 'vcode_type',
                    'label' => $this->l('Coupon code type'),
                    'hint' => $this->l('Coupon code type(i.e Numeric or Alphanumeric)'),
                    'col' => 4,
                    'values' => array(
                        array('id' => 'vcode_type_num', 'value' => 'NUMERIC', 'label' => $this->l('Numeric (i.e 12345)')),
                        array('id' => 'vcode_type_alphanum', 'value' => 'ALPHANUMERIC', 'label' => $this->l('Alphanumeric  (i.e ABC123)')),
                    ),
                    'tab' => 'voucher',
                ),
                array(
                    'type' => 'date',
                    'name' => 'from',
                    'label' => $this->l('From'),
                    'hint' => $this->l('Validity period starts from selected date.'),
                    'prefix' => '<i class="icon-calendar-empty"></i>',
                    'tab' => 'voucher',
                ),
                array(
                    'type' => 'date',
                    'name' => 'to',
                    'label' => $this->l('To'),
                    'hint' => $this->l('Validity period ends on selected date.'),
                    'prefix' => '<i class="icon-calendar-empty"></i>',
                    'tab' => 'voucher',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn button btn-default pull-right',
            ),
        );

        $this->toolbar_title = (Tools::getValue('id_gift_card')) ? $this->l('Edit Card') : $this->l('Add Gift Card');
        $product = null;
        $languages = Language::getLanguages();

        if (isset($this->gift) && $this->gift) {
            $product = new Product($this->gift['id_product'], true);

            //setting empty tags for all langs
            $tags = array();
            foreach ($languages as $lang) {
                $tags[$lang['id_lang']] = '';
            }

            if ($product) {
                $cover = Product::getCover($product->id);

                $this->fields_value['card_name'] = $product->name;
                $this->fields_value['product_description'] = $product->description;
                $this->fields_value['qty'] = StockAvailable::getQuantityAvailableByProduct($product->id);

                $product = (array) $product;
                $product['id_cover'] = $cover['id_image'];

                if (Tag::getProductTags($this->gift['id_product'])) {
                    foreach (Tag::getProductTags($this->gift['id_product']) as $id_lang => $tag) {
                        $tags[$id_lang] = (isset($tag) && $tag) ? implode(',', $tag) : '';
                    }
                }
            }

            $this->fields_value['tags'] = $tags;
            $this->fields_value['status'] = (bool) $this->gift['status'];
            $this->fields_value['value_type'] = $this->gift['value_type'];
            $this->fields_value['apply_discount'] = $this->gift['reduction_type'];
            $this->fields_value['vcode_type'] = $this->gift['vcode_type'];
            $this->fields_value['apply_discount_to'] = ($this->gift['id_discount_product'] > 0) ? 'specific' : 'order';
        } else {
            $this->fields_value['status'] = true;
            $this->fields_value['value_type'] = 'fixed';
            $this->fields_value['apply_discount'] = 'amount';
            $this->fields_value['vcode_type'] = 'ALPHANUMERIC';
            $this->fields_value['apply_discount_to'] = 'order';
        }

        $this->context->smarty->assign('current_lang', $this->context->language->id);
        $this->context->smarty->assign('languages', $languages);
        $this->context->smarty->assign('module', $this->module);

        $iso_tiny_mce = $this->context->language->iso_code;
        $iso_tiny_mce = (file_exists(_PS_JS_DIR_ . 'tiny_mce/langs/' . $iso_tiny_mce . '.js') ? $iso_tiny_mce : 'en');

        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'name' => 'checkBoxShopAsso',
                'tab' => 'product',
            );
        }
        $link = ($this->context->link) ? $this->context->link : new Link();
        //$this->context->smarty->assign(array('shops' => $shops, 'selected_shops' => $selected_shops));
        $this->context->smarty->assign(array(
            'iso_tiny_mce' => $iso_tiny_mce,
            'card' => $this->gift,
            'currentIndex' => self::$currentIndex,
            'currentToken' => $this->token,
            'id_lang' => $this->context->language->id,
            'id_currency' => $this->context->cookie->id_currency,
            'msg' => $this->msg,
            'link' => $link,
            'token' => $this->token,
            'current_id_tab' => (int) $this->context->controller->id,
            'version' => _PS_VERSION_,
            'product' => $product,
            'tax_exclude_taxe_option' => Tax::excludeTaxeOption(),
            'currencies' => Currency::getCurrencies(false, true, true),
            'tax_rules_groups' => TaxRulesGroup::getTaxRulesGroups(true),
            'ad' => __PS_BASE_URI__ . basename(_PS_ADMIN_DIR_),
            'default_currency' => Configuration::get('PS_CURRENCY_DEFAULT'),
            'default_currency_object' => $defaultCurrencyObject,
        ));

        return parent::renderForm();
    }

    public function addGiftCard()
    {
        $action = 'add';
        $id_gift_card = (int) Tools::getValue('id_gift_card');
        $id_product = (int) Tools::getValue('id_product');

        if ($id_gift_card) {
            $action = 'update';
            $giftCard = new Gift($id_gift_card);
            $product = new Product($id_product, true);
        } else {
            $giftCard = new Gift();
            $product = new Product();
        }

        $now = date('Y-m-d H:i:s');
        $id_lang = $this->context->cookie->id_lang;

        //** fetching form values
        $tags = array();
        $card_name = Tools::getValue('card_name');
        $length = (int) Tools::getValue('length');
        $vcode_type = (string) Tools::getValue('vcode_type');
        $card_qty = (int) Tools::getValue('qty');
        $status = (int) Tools::getValue('status');
        $free_ship = (int) Tools::getValue('free_shipping');
        $from = (string) Tools::getValue('from');
        $to = (string) Tools::getValue('to');
        $value_type = (string) Tools::getValue('value_type');
        $card_value = Tools::getValue('card_value');
        $red_type = (string) Tools::getValue('apply_discount');
        $reduction_product = (int) Tools::getValue('reduction_product');
        $apply_discount_to = (string) Tools::getValue('apply_discount_to');
        $min = (int) Tools::getValue('min');
        $max = (int) Tools::getValue('max');
        $min_percent = (float) Tools::getValue('min_percent');
        $max_percent = (float) Tools::getValue('max_percent');
        $id_attribute = 0;
        $img_name = (string) $_FILES['giftimage']['name'];
        $reduction_currency = (int) Tools::getValue('reduction_currency');
        $price = 0.0;
        $id_tax_rules_group = (int) Tools::getValue('id_tax_rules_group');

        //** setting value of gift card product
        if ($value_type == 'fixed') {
            $card_value = (float) $card_value;
            $price = (float) $card_value;
            $product->show_price = true;
        } else {
            $product->show_price = false;
        }
        if ($value_type == 'range') {
            $card_value = $min . ',' . $max;
        }

        //** Initializing gift card product
        $languages = Language::getLanguages(false);
        if (empty(Tools::getValue('card_name_' . Configuration::get('PS_LANG_DEFAULT')))) {
            return $this->errors[] = $this->l('You must enter gift card name for default language.');
        }if (Tools::getValue('card_name_' . Configuration::get('PS_LANG_DEFAULT')) && !Validate::isCatalogName(Tools::getValue('card_name_' . Configuration::get('PS_LANG_DEFAULT')))) {
            return $this->errors[] = $this->l('Gift card name is invalid for default language.');
        }
        foreach ($languages as $language) {
            if (Tools::getValue('card_name_' . $language['id_lang']) && !Validate::isCatalogName(Tools::getValue('card_name_' . $language['id_lang']))) {
                $this->errors[] = $this->l('Invalid card name in ') . $language['name'];
            } else {
                $product->name[$language['id_lang']] = (string) Tools::getValue('card_name_' . $language['id_lang']);
                $product->link_rewrite[$language['id_lang']] = Tools::str2url((string) Tools::getValue('card_name_' . $language['id_lang']));
            }

            if (!Validate::isCleanHtml(Tools::getValue('product_description_' . $language['id_lang']))) {
                $this->errors[] = $this->l('Invalid description in ') . $language['name'];
            } else {
                $product->description[$language['id_lang']] = (string) Tools::getValue('product_description_' . $language['id_lang']);
            }

            if ($value = Tools::getValue('tags_' . $language['id_lang'])) {
                if (!Validate::isTagsList($value)) {
                    $this->errors[] = sprintf(
                        $this->l('The tags list (%s) is invalid.'),
                        $language['name']
                    );
                } else {
                    $tags[(int) $language['id_lang']] = $value;
                }
            }
        }

        $product->quantity = $card_qty;
        $product->active = $status;
        $product->available_date = $from;
        $product->date_add = $now;
        $product->is_virtual = true;
        $product->price = $price;
        $product->show_price = ($value_type == 'fixed') ? true : false;
        $product->id_category_default = (Configuration::get('GIFT_CARD_CATEGORY')) ? (int) Configuration::get('GIFT_CARD_CATEGORY') : (int) Configuration::get('PS_HOME_CATEGORY');
        $product->redirect_type = '404';
        $product->id_tax_rules_group = $id_tax_rules_group;

        $categories = new Category($product->id_category_default, $id_lang);
        $product->category = $categories->link_rewrite;
        //$product->tags = $tags;

        $reduction_amount = $card_value;
        if ($red_type == 'amount') {
            $reduction_tax = (int) Tools::getValue('reduction_tax');
        } elseif ($red_type == 'percent') {
            if ($value_type == 'range') {
                $reduction_amount = $min_percent . ',' . $max_percent;
            } elseif ($value_type == 'dropdown') {
                $val1 = explode(',', $card_value);
                $val2 = explode(',', Tools::getValue('reduction_percent_dropdown'));
                $val1 = count($val1);
                $val2 = count($val2);
                if ($val1 != $val2) {
                    $this->errors[] = $this->l('No.of values of card price does not match No of values discount type.');
                } else {
                    $reduction_amount = Tools::getValue('reduction_percent_dropdown');
                }
            } elseif ($value_type == 'fixed') {
                $reduction_amount = (int) Tools::getValue('reduction_percent_fixed');
                $product->show_price = true;
            }

            $reduction_tax = 0;
        }

        //** setting default voucher code length
        if (empty($length)) {
            $length = 14;
        }

        // assigning gift card values
        $giftCard->id_product = $id_product;
        $giftCard->id_discount_product = $reduction_product;
        $giftCard->id_attribute = $id_attribute;
        $giftCard->card_name = $card_name;
        $giftCard->qty = $card_qty;
        $giftCard->status = $status;
        $giftCard->length = $length;
        $giftCard->free_shipping = $free_ship;
        $giftCard->from = $from;
        $giftCard->to = $to;
        $giftCard->value_type = $value_type;
        $giftCard->card_value = $card_value;
        $giftCard->vcode_type = $vcode_type;
        $giftCard->reduction_type = $red_type;
        $giftCard->reduction_amount = $reduction_amount;
        $giftCard->reduction_currency = $reduction_currency;
        $giftCard->reduction_tax = $reduction_tax;

        //** checking field values and displaying error messages respectively
        if (empty($card_qty) || $card_qty < 1 || !Validate::isInt($card_qty)) {
            $this->errors[] = $this->l('Invalid Card quantity');
        }
        if ($length < 4 || $length > 30 || !Validate::isInt($length)) {
            $this->errors[] = $this->l('Invalid Code length');
        }
        if (empty($from) || empty($to) || $from > $now || $to < $now || $from == $to) {
            $this->errors[] = $this->l('Invalid Validation date');
        }
        if (($red_type == 'amount' || $red_type == 'percent') && empty($reduction_amount)) {
            $this->errors[] = $this->l('Invalid discount amount/percentage');
        }
        if (($red_type == 'amount' || $red_type == 'percent') && ($apply_discount_to == 'specific' && empty($reduction_product))) {
            $this->errors[] = $this->l('Please specificy a discount product');
        }
        if ((($value_type == 'dropdown' || $value_type == 'fixed' || $value_type == 'fixed') && empty($card_value))) {
            $this->errors[] = $this->l('Invalid Gift card price');
        }
        if (($value_type == 'range') && (empty($min) || empty($max) || $min < 1 || $max < 1 || $min >= $max)) {
            $this->errors[] = $this->l('Invalid Gift card price');
        }
        if (($value_type == 'range' && $red_type == 'percent') && (empty($min_percent) || empty($max_percent) || $min_percent < 1 || $max_percent < 1 || $min_percent >= $max_percent)) {
            $this->errors[] = $this->l('Invalid range discount percent');
        }

        if (!count($this->errors)) {
            if ($action == 'update') {
                if (!empty($img_name) && $img_name != null) {
                    $this->setImage($id_product, $product->link_rewrite);
                }

                Db::getInstance()->delete('product_shop', 'id_product = 0');
                if (isset($tags) && $tags) {
                    if (Tag::deleteTagsForProduct((int) $product->id)) {
                        $this->updateTags($tags, $languages, $product->id);
                    }
                }
                if ($product->update() && $giftCard->update()) {
                    if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_ALL) {
                        $shops = Tools::getValue('checkBoxShopAsso_gift_card');
                        if (!empty($shops)) {
                            foreach ($shops as $shop) {
                                StockAvailable::setQuantity($product->id, $id_attribute, $card_qty, (int) $shop, false);
                            }
                        }
                    } else {
                        StockAvailable::setQuantity($product->id, $id_attribute, $card_qty);
                    }
                    $this->confirmations[] = $this->l('Gift card updated successfully.');
                }
            } else {
                if ($product->add()) {
                    $giftCard->id_product = (int) $product->id;
                    $giftCard->save();
                    //$product->addToCategories(array(Configuration::get('PS_HOME_CATEGORY')));
                    $product->updateCategories(array(Configuration::get('PS_HOME_CATEGORY'), Configuration::get('GIFT_CARD_CATEGORY')));
                    $this->setImage($product->id, $product->link_rewrite);
                    if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_ALL) {
                        $shops = Tools::getValue('checkBoxShopAsso_gift_card');
                        if (!empty($shops)) {
                            foreach ($shops as $shop) {
                                StockAvailable::setQuantity($product->id, $id_attribute, $card_qty, (int) $shop, false);
                            }
                        }
                    } else {
                        StockAvailable::setQuantity($product->id, $id_attribute, $card_qty);
                    }
                    $this->updateTags($tags, $languages, $product->id);
                    //Hook::exec('actionProductAdd', array('id_product' => (int)$product->id, 'product' => $product));
                    Hook::exec('actionProductUpdate', array('id_product' => (int) $product->id, 'product' => $product));
                    $this->confirmations[] = $this->l('Gift card added successfully.');
                }
            }

            //multi-shop
            if (Shop::isFeatureActive()) {
                Gift::removeAssocShops($product->id);
                if ($giftCard->id) {
                    Db::getInstance()->delete('gift_card_shop', 'id_gift_card = ' . (int) $giftCard->id);
                }
                if ($shops = Tools::getValue('checkBoxShopAsso_gift_card')) {
                    foreach ($shops as $shop) {
                        Gift::updateGiftShops(
                            $product->id,
                            (int) $shop,
                            $product->id_category_default,
                            $product->id_tax_rules_group,
                            $product->active,
                            $price
                        );
                        if ($giftCard->id) {
                            Db::getInstance()->insert(
                                'gift_card_shop',
                                array(
                                    'id_gift_card' => (int) $giftCard->id,
                                    'id_shop' => (int) $shop
                                )
                            );
                        }
                    }
                }
            }
        } else {
            return $this->errors;
        }
    }

    public function setImage($id_product, $legend)
    {
        $image = new Image();
        $image->id_product = (int) $id_product;
        $image->position = Image::getHighestPosition($id_product) + 1;
        Image::deleteCover((int) $id_product);
        $image->cover = true;

        $languages = Language::getLanguages();
        foreach ($languages as $language) {
            $image->legend[$language['id_lang']] = $legend[$language['id_lang']];
        }

        $image->id_image = $image->id;
        $image->add();
        $tmp_name = tempnam(_PS_PROD_IMG_DIR_, 'PS');
        move_uploaded_file($_FILES['giftimage']['tmp_name'], $tmp_name);

        $new_path = $image->getPathForCreation();
        ImageManager::resize($tmp_name, $new_path . '.' . $image->image_format);
        $images_types = ImageType::getImagesTypes('products');
        foreach ($images_types as $imageType) {
            ImageManager::resize($tmp_name, $new_path . '-' . Tools::stripslashes($imageType['name']) . '.' . $image->image_format, $imageType['width'], $imageType['height'], $image->image_format);
        }
    }

    public function ajaxProcess()
    {
        if (Tools::isSubmit('reductionProductFilter')) {
            $products = Product::searchByName($this->context->language->id, trim(Tools::getValue('q')));
            die(Tools::jsonEncode($products));
        }
    }

    protected function updateTags($tags, $languages, $id_product)
    {
        $success = true;
        if (isset($tags) && $tags) {
            foreach ($languages as $language) {
                $tags = (isset($tags[$language['id_lang']]) && $tags[$language['id_lang']]) ? explode(',', $tags[$language['id_lang']]) : array();
                if (isset($tags) && $tags) {
                    foreach ($tags as $tag) {
                        if ($tag) {
                            $success &= Tag::addTags($language['id_lang'], (int) $id_product, $tag);
                        }
                    }
                }
            }
            return $success;
        }
        return false;
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->addjQueryPlugin(array(
            'date',
            'tagify',
        ));
        $this->addJqueryUI(array(
            'ui.slider',
            'ui.datepicker',
        ));

        $this->addJS(array(
            _PS_JS_DIR_ . 'tiny_mce/tiny_mce.js',
            _PS_JS_DIR_ . 'admin/tinymce.inc.js',
            _PS_JS_DIR_ . 'admin/product.js',
        ));

        $this->addJS(array(_PS_JS_DIR_ . 'jquery/plugins/timepicker/jquery-ui-timepicker-addon.js'));
        $this->addCSS(array(_PS_JS_DIR_ . 'jquery/plugins/timepicker/jquery-ui-timepicker-addon.css'));
        $this->addCSS(array(_PS_JS_DIR_ . 'jquery/plugins/autocomplete/jquery.autocomplete.css'));
        $this->addJS(array(_PS_JS_DIR_ . 'jquery/plugins/autocomplete/jquery.autocomplete.js'));
    }
}
