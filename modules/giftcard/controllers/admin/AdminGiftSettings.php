<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*/

class AdminGiftSettingsController extends ModuleAdminController
{
    public function __construct()
    {
        $this->lang = false;
        $this->deleted = false;
        $this->bootstrap = true;
        parent::__construct();
        $this->context = Context::getContext();
    }

    public function initProcess()
    {
        parent::initProcess();
        $url = Context::getContext()->link->getAdminLink('AdminModules').'&configure='.$this->module->name.'&token='.Tools::getAdminTokenLite('AdminModules');
        Tools::redirectAdmin($url);
    }
}
