<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*/

$sql = array();

$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'gift_card`';

$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'gift_card_customer`';

$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'ordered_gift_cards`';

$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'gift_card_shop`';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
