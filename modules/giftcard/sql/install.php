<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*/

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'gift_card`(
        `id_gift_card`          int(10) unsigned NOT NULL auto_increment,
        `id_product`            int(10) unsigned NOT NULL,
        `id_discount_product`   int(10) unsigned NOT NULL,
        `id_attribute`          int(10)  unsigned NOT NULL,
        `card_name`             TEXT,
        `qty`                   int(10),
        `status`                int(2),
        `length`                int(10),
        `free_shipping`         int(2),
        `from`                  DATE,
        `to`                    DATE,
        `value_type`            TEXT,
        `card_value`            TEXT,
        `vcode_type`            TEXT,
        `reduction_type`        TEXT,
        `reduction_amount`      TEXT,
        `reduction_currency`    int(10),
        `reduction_tax`         int(2),
        PRIMARY KEY             (`id_gift_card`))
        ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'gift_card_customer`(
        `id_cart_rule`          int(10) unsigned NOT NULL,
        `id_customer`           int(10) unsigned NOT NULL,
        `id_cart`               int(10) unsigned,
        `id_order`              int(10) unsigned,
        `id_product`            int(10) unsigned,
        `link_rewrite`          TEXT,
        `id_image`              int(10) unsigned,
        PRIMARY KEY             (`id_cart_rule`, `id_customer`))
        ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ordered_gift_cards`(
        `id_cart`               int(10) unsigned NOT NULL,
        `id_order`              int(10) unsigned NOT NULL,
        `id_product`            int(10),
        `id_customer`           int(10) DEFAULT 0,
        `reference_product`     int(10),
        `has_voucher`           tinyint(2) DEFAULT 0,
        `gift_type`             VARCHAR(20) NOT NULL DEFAULT \'home\',
        `friend_name`           varchar(100),
        `friend_email`          varchar(100),
        `gift_message`          TEXT,
        PRIMARY KEY             (`id_cart`, `id_product`))
        ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'gift_card_shop`(
        `id_gift_card`          int(10) unsigned NOT NULL,
        `id_shop`               int(10) unsigned NOT NULL,
        PRIMARY KEY             (`id_gift_card`, `id_shop`))
        ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
