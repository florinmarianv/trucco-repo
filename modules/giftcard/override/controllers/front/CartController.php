<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*/

class CartController extends CartControllerCore
{
    protected $new_price;

    protected $gift_order_type = 'home';

    protected $is_gift_product = false;

    protected $add_gift_data = false;

    protected $reference_product = 0;

    protected $giftcard_type = false;

    protected $friend_name = '';

    protected $friend_email = '';

    protected $gift_message = '';
    /**
     * Initialize cart controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();
        if (Module::isInstalled('giftcard')) {
            $default_currency = (int)Configuration::get('PS_CURRENCY_DEFAULT');
            $active_currency = (int)$this->context->currency->id;
            $currency_current = new Currency($active_currency);
            $this->reference_product = $this->id_product;
            include_once(_PS_MODULE_DIR_.'giftcard/models/Gift.php');
            if (Gift::isExists($this->id_product) && Tools::getValue('add')) {
                $this->gift_order_type = Tools::getValue('gift_order_type', 'home');
                $this->giftcard_type = Tools::getValue('giftcard_type');
                $priceDisplay = (int) Product::getTaxCalculationMethod((int) Context::getContext()->cart->id_customer);
                $tax = (!$priceDisplay || $priceDisplay == 2) ? true : false;
                $this->new_price = (float)(($this->giftcard_type && $this->giftcard_type == 'fixed')? Product::getPriceStatic($this->id_product, $tax) : Tools::getValue('giftcard_price'));
                $this->is_gift_product = true;
                
                if ($this->gift_order_type === 'sendsomeone' || ($this->giftcard_type && in_array($this->giftcard_type, array('dropdown', 'range')))) {
                    if ($default_currency != $active_currency) {
                        $store_currency = new Currency($default_currency);
                        $this->new_price = Tools::convertPriceFull($this->new_price, $currency_current, $store_currency);
                    }
                    $this->id_product = Module::getInstanceByName('giftcard')->processDuplicate($this->id_product, $this->new_price);
                }
                
                if ($this->gift_order_type === 'sendsomeone') {
                    $giftDetails = Tools::getValue('gift_vars');
                    if (!Validate::isName(trim($giftDetails['reciptent']))) {
                        $this->errors[] = Module::getInstanceByName('giftcard')->l('Invalid reciptent name.');
                        $this->ajaxDie(Tools::jsonEncode(array(
                            'hasError' => true,
                            'success' => false,
                            'errors' => $this->errors,
                        )));
                    } else if (!Validate::isEmail(trim($giftDetails['email']))) {
                        $this->errors[] = Module::getInstanceByName('giftcard')->l('Invalid reciptent email.');
                        $this->ajaxDie(Tools::jsonEncode(array(
                            'hasError' => true,
                            'success' => false,
                            'errors' => $this->errors,
                        )));
                    } else {
                        $this->setContextCart();
                        $this->customization_id = (int)$this->setGiftCardData();
                        $this->friend_name = pSQL($giftDetails['reciptent']);
                        $this->friend_email = pSQL($giftDetails['email']);
                        $this->gift_message = pSQL($giftDetails['message']);
                    }
                }
            }
        }
    }

    protected function processChangeProductInCart()
    {
        parent::processChangeProductInCart();
        if (!count($this->errors) && $this->is_gift_product) {
            Gift::orderGC(array(
                'id_cart' => $this->context->cart->id,
                'id_product' => $this->id_product,
                'reference_product' => $this->reference_product,
                'gift_type' => $this->gift_order_type,
                'friend_name' => $this->friend_name,
                'friend_email' => $this->friend_email,
                'gift_message' => $this->gift_message,
            ));
        }
    }

    protected function processDeleteProductInCart()
    {
        parent::processDeleteProductInCart();
        Hook::exec(
            'actionGiftCardDeleteFromCart',
            array(
                'id_product' => $this->id_product,
                'id_cart' => $this->context->cart->id
            )
        );
    }

    protected function setGiftCardData()
    {
        $giftDetails = Tools::getValue('gift_vars');
        if (!empty(trim($giftDetails['reciptent'])) && !empty(trim($giftDetails['email']))) {
            $i = 0;
            $id_customization = 0;
            $giftDetails['type'] = Module::getInstanceByName('giftcard')->translations[$this->gift_order_type];
            foreach (Module::getInstanceByName('giftcard')->field_labels as $key => $label) {
                if (isset($giftDetails[$key]) && $giftDetails[$key]) {
                    $index = Gift::addGiftMetaLabel($this->id_product, $label);
                    $id_customization = Gift::addGiftCustomization($this->context->cart->id, $this->id_product, $index, $giftDetails[$key]);
                }
            }
            return $id_customization;
        }
    }
    
    protected function setContextCart()
    {
        if (!$this->errors) {
            if (!$this->context->cart->id) {
                if (Context::getContext()->cookie->id_guest) {
                    $guest = new Guest(Context::getContext()->cookie->id_guest);
                    $this->context->cart->mobile_theme = $guest->mobile_theme;
                }
                $this->context->cart->add();
                if ($this->context->cart->id) {
                    $this->context->cookie->id_cart = (int)$this->context->cart->id;
                }
            }
        }
    }
}
