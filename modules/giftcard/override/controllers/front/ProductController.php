<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*/

class ProductController extends ProductControllerCore
{
    protected function addProductCustomizationData(array $product_full)
    {
        $product_full = parent::addProductCustomizationData($product_full);
        if (true === Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            if (Module::isInstalled('giftcard')) {
                include_once(_PS_MODULE_DIR_.'giftcard/models/Gift.php');
                if (Gift::isExists($this->product->id)) {
                    $product_full['customizations'] = array(
                        'fields' => array(),
                    );
                }
            }
        }
        return $product_full;
    }
}
