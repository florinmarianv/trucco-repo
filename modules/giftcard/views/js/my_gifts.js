/*
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*/

$(function() {
    if (isGiftListingPage) {
        var f = $('.filtr-container').filterizr({ controlsSelector: '.fltr-controls' });
        window.filterizr = f;

        $('.gift-filter li').click(function() {
            $('.gift-filter li').removeClass('active');
            $(this).addClass('active');
        });
        //Shuffle control
        $('.shuffle-btn').click(function() {
        $('.sort-btn').removeClass('active');
        });
        //Sort controls
        $('.sort-btn').click(function() {
        $('.sort-btn').removeClass('active');
        $(this).addClass('active');
        });
    }
});

$(document).on('click', '.show-form', function(e){
	$(this).closest('.my-gift-voucher').next('.form-row').toggleClass('invisible_row');
});