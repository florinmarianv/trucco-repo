/*
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*/

var price_elem = $('#gift-card-wrapper');
$('document').ready( function() {
    if (typeof giftType !== 'undefined' && giftType) {
        initGiftCard(giftType)
        prestashop.on('updateProduct', function(e) {
            _getGiftPrice(giftType, parseFloat($('#gift_card_price').val()));
        });
    }

    // ************** find gift products on home and category pages.
    var subURL = base_url + "?fc=module&module=giftcard&controller=ajax";
    $('.product-miniature').map(function() {
        var thisActiveBlock = $(this);
        pid = $(this).data('id-product');
        var request = {
            url: subURL,
            type: "POST",
            cache: false,
            dataType: "json",
            data: {
                action: 'ProductExists',
                id_product: pid
            },
            success: function(data) {
                var pid = parseInt(data);
                if(pid > 0) {
                    thisActiveBlock.find('.price').hide();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        };
        $.ajax(request);
    });

});

$(document).on('click', '.quick-view', function(e) {
    var pid = $(this).closest('.js-product-miniature').data('id-product');
    var _ctype = _getCtype(pid);
    if (typeof _ctype !== 'undefined' && _ctype) {
        _getGiftPrice(_ctype, parseFloat($('#gift_card_price').val()))
    }
});

$(document).on('click', 'input[name=gift_order_type]', function(event) {
    checkGiftCardOrderMethod($(this).val());
});

$(document).on('keydown', '.gc_required_fields', checkGcRequiredVals);

function initGiftCard(giftType) {
    //***************** check gift card type on product page.
    $('.add-to-cart').hide();
    if ($('#gift_product').length <= 0) {
        $('#add-to-cart-or-refresh').prepend('<div id="gift_product"></div>');
    }
    var price_elem = $('#gift-card-wrapper');
    if (price_elem.length) {
        if (giftType == 'dropdown') {
            price_elem.append(
                $('#dropdown_price').show()
            );
        } else if (giftType == 'fixed') {
            $('.product-prices').show();
        } else if (giftType == 'range') {
            price_elem.append(
                $('#range_price').show()
            );
            //$('#range_price').show().appendTo(price_elem);
        }
        $('#gift_card_price').keydown(function(e) {
            var key = e.charCode || e.keyCode || 0;
            return (key == 8 || 
                    key == 9 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
        });
    }

    if (giftType == 'fixed') {
        $('.product-prices').show();
    } else {
        $('.product-prices').hide();
    }
    $('#gift_product').html($(price_elem));
    checkGiftCardOrderMethod($('input[name=gift_order_type]:checked').val());
    _validatePrice(parseFloat($('#gift_card_price').val()), giftType);
}

function checkGiftCardOrderMethod(method) {

    if (typeof method === 'undefined') {
        $('#print-home').click();
    }

    $('.add-to-cart').hide();
    if (method == 'sendsomeone') {
        $('#giftcard_send_to_friend').show();
    } else {
        $('#giftcard_send_to_friend').hide();
    }

    checkGcRequiredVals();
}

function _validatePrice(val, giftType) {

    checkGcRequiredVals();

    if (typeof val !== 'undefine' && val) {
        val = parseFloat(val);
        $('.add-to-cart').show();
        $('#price_error').hide();
        $('#gift_card_price').attr('value', val);
        if (isNaN(val) && giftType != 'fixed'){
            $('#gift_card_price').val('');
            $('#price_error').show();
            $('.add-to-cart').hide();
            $('#gift_card_price').focus();
        }

        if (giftType == 'range'){
            $('.add-to-cart').show();
            var min = parseFloat($('#range_min').val());
            var max = parseFloat($('#range_max').val());
            if (!val || (val < min) || (val > max))
            {
                $('#gift_card_price').focus();
                $('#price_error').show();
                $('.add-to-cart').hide();
            }
        }
    }
}

function _getGiftPrice(_ctype, _sprice) {
    var update_display = ((_ctype == 'fixed' && $('#gift-card-wrapper').length <= 0) || $.inArray(_ctype, ['dropdown','range']) !== -1)? true : false;
    setTimeout(function() {
        $.post(ajax_URL , { ajax: '1', action: 'getGiftPrice', id_product: pid, card_type: _ctype, current_price: _sprice }, null, 'json')
        .then(function (resp) {
            $('.gift_card').remove();
            $('.product-prices').hide();
            if ($.inArray(_ctype, ['dropdown','range']) !== -1) {
                $('#' + _ctype + '_price').remove();
            }
            $('#gift_product').html($(price_elem));
            if (update_display) {
                $('#gift-card-wrapper').append(resp.gift_prices);
            }
            $('.product-additional-info').find('#gift-card-wrapper').remove();
            initGiftCard(_ctype);
        });
    }, 1000);
}

function _getCtype(pid) {
    var cType = '';
    var options = {
        type        : "GET",
        cache       : false,
        async       : false,
        dataType    : "json",
        url         : base_url + "?fc=module&module=giftcard&controller=ajax",
        data        : {
            ajax : 1,
            action : 'getGiftType',
            id_product : pid,
        },
        success : function(resp) {
            if(resp && resp.gift_type) {
                cType = resp.gift_type;
            }
        },
    };
    $.ajax(options);
    return cType;
}

function checkGcRequiredVals() {
    var show_add_to_cart = false;
    $('.add-to-cart').hide();
    var selected_method = $('input[name=gift_order_type]:checked').val();
    $('#add_to_cart').hide();
    if (selected_method == 'sendsomeone') {
        $('.gc_required_fields').each(function() {
            if ($.trim($(this).val())) {
                show_add_to_cart = true;
            } else {
                show_add_to_cart = false;
                $(this).attr({
                    placeholder: $(this).prev('.gc_required_label').text() + ` ${required_label}`,
                })
            }
        });
    } else {
        show_add_to_cart = true;
    }

    if (show_add_to_cart) {
        $('.add-to-cart').show();
    }
}