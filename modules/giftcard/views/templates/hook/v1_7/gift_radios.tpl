{*
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*}
<p>
    <div class="print_home">
        <input id="print-home" type="radio" name="gift_order_type" value="home">
        <label for="print-home">{l s='Print at Home' mod='giftcard'}</label>
    </div>
    <div class="send_to_friend">
        <input id="send_someone" type="radio" name="gift_order_type" value="sendsomeone">
        <label for="send_someone">{l s='Send to Friend' mod='giftcard'}</label>
    </div>

    <div id="giftcard_send_to_friend" style="display: none;">
        <hr>
        <div class="form-group">
            <label class="label gc_required_label">{l s='Reciptent Full Name' mod='giftcard'}</label>
            <input class="form-control gc_required_fields" type="text" name="gift_vars[reciptent]">
            <p class="small gc_required">* {l s='required' mod='giftcard'}</p>
        </div>
        <div class="form-group">
            <label class="label gc_required_label">{l s='Reciptent Email' mod='giftcard'}</label>
            <input class="form-control gc_required_fields" type="text" name="gift_vars[email]">
            <p class="small gc_required">* {l s='required' mod='giftcard'}</p>
        </div>
        <div class="form-group">
            <label class="label">{l s='Message (optional)' mod='giftcard'}</label>
            <textarea class="form-control" name="gift_vars[message]"></textarea>
        </div>
    </div>
</p>
