{*
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*}
<script type="text/javascript">
var base_url 	= "{if $force_ssl == 1}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{else}{$base_dir|escape:'htmlall':'UTF-8'}{/if}";
var ajax_URL 	= "{$link->getModuleLink('giftcard','ajax', [], true)|escape:'htmlall':'UTF-8'}";
var required_label = "{l s='is required' mod='giftcard' js=1}";
</script>