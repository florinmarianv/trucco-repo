{*
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*}
{extends file=$layout}

{block name='content'}

<div class="block card card-block">
{if isset($smarty.get.msg) AND $smarty.get.msg == 1}
	<div class="success conf alert alert-success">
		<p>{l s='Your gift card has been sent successfully.' mod='giftcard'}</p>
	</div>
{/if}


<h4 class="block title_block giftcards_title">{l s='My Gift Cards' mod='giftcard'}</h4>
	<div id="block-giftcard" class="block-center table-responsive">
		{if count($coupens) > 0}
		<table class="table table-bordered {if $ps_version < 1.6}std{/if}" id="order-list">
				<thead>
					<tr>
						<th class="first_item">{l s='Card' mod='giftcard'}</th>
						<th class="item">{l s='Code' mod='giftcard'}</th>
						<th class="item">{l s='Qty' mod='giftcard'}</th>
						<th class="item">{l s='Value' mod='giftcard'}</th>
						<th class="item">{l s='Expire Date' mod='giftcard'}</th>
						<th class="item">&nbsp;</th>
					</tr>
				</thead>
			<tbody>
				{foreach from=$coupens item=card}
						<tr class="my-gift-voucher" style="text-align:center">
							<td>
								<center><span><img style="height:50px;" alt="" src="{$link->getImageLink($card.link_rewrite, $card.id_image, 'small_default')|escape:'htmlall':'UTF-8'}"></span>
								<p>{$card.name|escape:'htmlall':'UTF-8'}<p></center>
							</td>
							<td>
								{$card.code|escape:'htmlall':'UTF-8'}
							</td>
							<td>
								{$card.quantity|escape:'htmlall':'UTF-8'}
							</td>
							<td>
								{if isset($card.reduction_percent) AND $card.reduction_percent != 0}
									{$card.reduction_percent|escape:'htmlall':'UTF-8'}{l s='%' mod='giftcard'}
								{elseif isset($card.reduction_amount) AND $card.reduction_amount != 0}
									{Tools::displayPrice($card.reduction_amount)|escape:'htmlall':'UTF-8'}
								{else}
									{l s='0' mod='giftcard'}
								{/if}
							</td>
							<td>
								{$card.date_to|escape:'htmlall':'UTF-8'}
							</td>
							<td class="send_someone">
								<a href="javascript:;" class="show-form btn btn-primary button">
									<i class="material-icons">keyboard_arrow_down</i>{l s='Send to someone' mod='giftcard'}
								</a>
							</td>
						</tr>
						<tr class="form-row invisible_row">
							<td class="send_someone" colspan="6">
								<div class="send_someone_form">
									<form action="{$link->getModuleLink('giftcard','mygiftcards')|escape:'htmlall':'UTF-8'}" method="post" name="giftcard_send_to_friend" id="from_giftcard">
										<div class="form-group col-lg-12">
											<label class="control-label col-lg-3">{l s='To' mod='giftcard'}</label>
											<div class="col-lg-7">
												<input class="form-control" type="text" name="friend_name" value="" required placeholder="John Doe" />
											</div>
										</div>
										<div class="form-group col-lg-12">
											<label class="control-label col-lg-3">{l s='Email' mod='giftcard'}</label>
											<div class="col-lg-7">
												<input class="form-control" type="text" name="friend_email" value="" required placeholder="demo@demo.com" />
											</div>
										</div>
										<div class="form-group col-lg-12">
											<label class="control-label col-lg-3">{l s='Message' mod='giftcard'}</label>
											<div class="col-lg-7">
												<textarea class="form-control" name="friend_message"/></textarea>
											</div>
										</div>
										<div class="form-group col-lg-10">
											<input class="button btn btn-primary  float-xs-right" type="submit" name="send_giftcard" value="{l s='Send' mod='giftcard'}"/>
										</div>
										
										<input type="hidden" name="id_gift_product" value="{$card.id_product|escape:'htmlall':'UTF-8'}"/>
										<input type="hidden" name="giftcard_name" value="{$card.name|escape:'htmlall':'UTF-8'}"/>
										<input type="hidden" name="vcode" value="{$card.code|escape:'htmlall':'UTF-8'}"/>
										<input type="hidden" name="id_coupen" value="{$card.id_cart_rule|escape:'htmlall':'UTF-8'}"/>
										<input type="hidden" name="expire_date" value="{$card.date_to|escape:'htmlall':'UTF-8'}"/>
									</form>
								</div>
							</td>
						</tr>
				{/foreach}
			</tbody>
		</table>
		{else}
			<div class="alert alert-warning warning">
				<center>{l s='You did not purchased any Gift card yet.' mod='giftcard'}</center>
			</div>
		{/if}
	</div>

	<ul class="footer_links">
		<li class="col-lg-3">
			<a href="{$link->getPageLink('my-account', true)|escape:'htmlall':'UTF-8'}" class="btn btn-primary">
				<span><i class="icon-chevron-left"></i> {l s='Back to Your Account' mod='giftcard'}</span>
			</a>
		</li>
		<li class="f_right">
			<a href="{if $force_ssl == 1}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{else}{$base_dir|escape:'htmlall':'UTF-8'}{/if}" class="btn btn-primary">
				<span><i class="icon-home"></i> {l s='Home' mod='giftcard'}</span>
			</a>
		</li>
	</ul>
</div>

{/block}