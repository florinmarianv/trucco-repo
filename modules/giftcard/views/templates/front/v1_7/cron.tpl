{*
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    FMM Modules
 *  @copyright 2019 FMM Modules
 *  @license   FMM Modules
 *  @version   1.7.0
*}

{extends file='index.tpl'}

{block name="page_content"}
    {block name="content_wrapper"}
        <div class="card card-block">
            {if isset($errors) && $errors}
            	<h3>{l s='An error occurred' mod='giftcard'}:</h3>
            	<ul class="alert alert-danger">
            		{foreach from=$errors item='error'}
            			<li>{$error|escape:'htmlall':'UTF-8'}.</li>
            		{/foreach}
            	</ul>
            {/if}
            <hr>
            <p class="alert alert-info info">
                {l s='Flushed Giftcards' mod='giftcard'}:{$result.deleted|escape:'htmlall':'UTF-8'}
                <br>
                {l s='Skipped' mod='giftcard'}:{$result.skip|escape:'htmlall':'UTF-8'}
            </p>
        </div>
    {/block}
{/block}
