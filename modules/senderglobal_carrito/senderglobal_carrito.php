<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Senderglobal_carrito extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'senderglobal_carrito';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'SenderGlobal';
        $this->need_instance = 0;
		
		$this->usernameAPI = "trucco_API";
		$this->passwordAPI = "Trc542370123";
		$this->basecodeAPI = "056126";

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('SenderGlobal Carrito');
        $this->description = $this->l('Plugin para sincronizar carrito con SenderGlobal');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('SENDERGLOBAL_CARRITO_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('actionValidateOrder') &&
			$this->registerHook('actionAfterDeleteProductInCart') &&			
			$this->registerHook('actionDeleteProductInCartAfter') &&
            $this->registerHook('actionObjectProductInCartDeleteBefore') &&
            $this->registerHook('actionCartSave');
    }

    public function uninstall()
    {
        Configuration::deleteByName('SENDERGLOBAL_CARRITO_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitSenderglobal_carritoModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitSenderglobal_carritoModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'SENDERGLOBAL_CARRITO_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'SENDERGLOBAL_CARRITO_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'SENDERGLOBAL_CARRITO_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'SENDERGLOBAL_CARRITO_LIVE_MODE' => Configuration::get('SENDERGLOBAL_CARRITO_LIVE_MODE', true),
            'SENDERGLOBAL_CARRITO_ACCOUNT_EMAIL' => Configuration::get('SENDERGLOBAL_CARRITO_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'SENDERGLOBAL_CARRITO_ACCOUNT_PASSWORD' => Configuration::get('SENDERGLOBAL_CARRITO_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookActionValidateOrder($params)
    {
        $usernameAPI = $this->usernameAPI;
		$passwordAPI = $this->passwordAPI;
		$basecodeAPI = $this->basecodeAPI;

        $customerEmail = $params['customer']->email;

		if(!empty($customerEmail))
		{
			$urlAPI = "http://webapp.senderglobal.com/app/APIS/carrito_abandonado/pasarela_magento.php?user_api=$usernameAPI&pwd_api=$passwordAPI&base_code=$basecodeAPI&to=$customerEmail&actiondelete=true";

			file_get_contents($urlAPI);
		}
    }

    public function hookActionCartSave($params)
    {        
        $usernameAPI = $this->usernameAPI;
		$passwordAPI = $this->passwordAPI;
		$basecodeAPI = $this->basecodeAPI;
		$plantilla = "1";
		$source = strtoupper(Language::getIsoById($params['cookie']->id_lang));

		$customerEmail = $params['cookie']->email;
		$nombreUsuario = $params['cookie']->customer_firstname;

		if(!empty($customerEmail))
		{
            if(is_object($params['cart']))
            {
                $productos = $params['cart']->getProducts(true);

                if(count($productos) > 0)
                {
                    foreach($productos as $producto)
                    {
                        $nombreProducto = str_replace('"', '\"', $producto['name']);
                        $descripcionProducto = strip_tags($producto['description_short']);
                        $precioProducto = number_format($producto['price_without_reduction'], 2);
                        $precioFinalProducto = number_format($producto['price_with_reduction'], 2);

                        $urlProducto = $this->context->link->getProductLink($producto, null, null, null, null, null, $producto['id_product_attribute']);

						$imgs = Image::getImages($this->context->language->id, $producto['id_product'], $producto['id_product_attribute']);
						
						if(count($imgs) > 0)
						{
							$imagenProducto = $this->context->link->getImageLink($producto['link_rewrite'], $producto['id_product'].'-'.$imgs[0]['id_image'], ImageType::getFormatedName('home'));
						}
						else
						{
							$images = Product::getCover($producto['id_product']);
							$imagenProducto = $this->context->link->getImageLink($producto['link_rewrite'], $images['id_image'], ImageType::getFormatedName('home'));
						}
                    }

                    $dataCarrito = base64_encode(json_encode(array("parametros" => array("nombre" => $nombreUsuario), "productos" => array(array("name" => $nombreProducto, "url" => $urlProducto, "price" => $precioProducto, "finalprice" => $precioFinalProducto, "img" => $imagenProducto, "desc" => $descripcionProducto))), JSON_UNESCAPED_UNICODE));

                    $urlAPI = "http://webapp.senderglobal.com/app/APIS/carrito_abandonado/pasarela_magento.php?user_api=$usernameAPI&pwd_api=$passwordAPI&base_code=$basecodeAPI&to=$customerEmail&plantilla=$plantilla&source=$source&data=$dataCarrito";
    
                    file_get_contents($urlAPI);
                }
            }
		}
    }
			
	public function hookActionAfterDeleteProductInCart($params)
	{
		$usernameAPI = $this->usernameAPI;
		$passwordAPI = $this->passwordAPI;
		$basecodeAPI = $this->basecodeAPI;
		
		$customerEmail = $params['cookie']->email;
		
		if(!empty($customerEmail))
		{
			$url_product = base64_encode($this->context->link->getProductLink($params, null, null, null, null, null, $params['id_product_attribute']));
			
			$urlAPI = "http://webapp.senderglobal.com/app/APIS/carrito_abandonado/pasarela_magento.php?user_api=$usernameAPI&pwd_api=$passwordAPI&base_code=$basecodeAPI&to=$customerEmail&data=$url_product&actiondeleteproduct=true";

			file_get_contents($urlAPI);
		}
	}
	
	public function hookActionDeleteProductInCartAfter($params)
	{
		$usernameAPI = $this->usernameAPI;
		$passwordAPI = $this->passwordAPI;
		$basecodeAPI = $this->basecodeAPI;
		
		$customerEmail = $params['cookie']->email;
		
		if(!empty($customerEmail))
		{
			$url_product = base64_encode($this->context->link->getProductLink($params, null, null, null, null, null, $params['id_product_attribute']));
			
			$urlAPI = "http://webapp.senderglobal.com/app/APIS/carrito_abandonado/pasarela_magento.php?user_api=$usernameAPI&pwd_api=$passwordAPI&base_code=$basecodeAPI&to=$customerEmail&data=$url_product&actiondeleteproduct=true";

			file_get_contents($urlAPI);
		}
	}
	
	public function hookActionObjectProductInCartDeleteBefore($params)
	{
		$usernameAPI = $this->usernameAPI;
		$passwordAPI = $this->passwordAPI;
		$basecodeAPI = $this->basecodeAPI;
		
		$customerEmail = $params['cookie']->email;
		
		if(!empty($customerEmail))
		{
			$url_product = base64_encode($this->context->link->getProductLink($params, null, null, null, null, null, $params['id_product_attribute']));
			
			$urlAPI = "http://webapp.senderglobal.com/app/APIS/carrito_abandonado/pasarela_magento.php?user_api=$usernameAPI&pwd_api=$passwordAPI&base_code=$basecodeAPI&to=$customerEmail&data=$url_product&actiondeleteproduct=true";

			file_get_contents($urlAPI);
		}
	}
}
