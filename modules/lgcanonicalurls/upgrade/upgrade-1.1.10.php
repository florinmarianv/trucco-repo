<?php
/**
 *  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
 *
 * @author    Línea Gráfica E.C.E. S.L.
 * @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
 * @license   https://www.lineagrafica.es/licenses/license_en.pdf
 *            https://www.lineagrafica.es/licenses/license_es.pdf
 *            https://www.lineagrafica.es/licenses/license_fr.pdf
 * User: desar10
 * Date: 1/07/16
 * Time: 9:54
 */

function upgrade_module_1_1_10($module)
{
    // Obtenemos los duplicados generados por error
    $sql    = 'SELECT * FROM `'._DB_PREFIX_.'lgcanonicalurls` GROUP BY `id_object`, `type_object`';
    $result = Db::getInstance()->executeS($sql);

    // Si hay duplicados eliminamos antes de cambiar la tabla (si no no podemos alterar la tabla)
    if (!empty($result)) {
        foreach ($result as $row) {
            // Eliminamos todos
            $sql_del = 'DELETE FROM `'._DB_PREFIX_.'lgcanonicalurls` '
                .'WHERE `id_object` = '.$row['id_object']
                .'  AND `type_object` = "' . $row['type_object'].'"';
            Db::getInstance()->execute($sql_del);

            // insertamos una única fila
            $sql_ins = 'INSERT INTO `'._DB_PREFIX_.'lgcanonicalurls`(`id_object`, `type_object`, `type`, `parameters`)'
                .' VALUES ('
                .$row['id_object'].', "'
                .$row['type_object'].'", "'
                .$row['type'].'", "'
                .$row['parameters']
                .'")';
            Db::getInstance()->execute($sql_ins);
        }
    }

    $queries = array(
        "ALTER TABLE `"._DB_PREFIX_."lgcanonicalurls` ADD PRIMARY KEY (`id_object`,`type_object`)",
        "ALTER TABLE `"._DB_PREFIX_."lgcanonicalurls_lang` ADD PRIMARY KEY(`id_object`,`type_object`, `id_lang`)",
    );
    return $module->proccessQueries($queries);
}
