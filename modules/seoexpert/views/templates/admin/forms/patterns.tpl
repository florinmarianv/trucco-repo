{*
* 2007-2017 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2017 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*}

<ul class="tags_select" {if $social}style="padding-top:10px"{/if}>
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}product_name{literal}}{/literal}" title="{l s='Product name' mod='seoexpert'}">
			{l s='Product name' mod='seoexpert'}
		</a>
	</li>
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}product_description{literal}}{/literal}" title="{l s='Product description' mod='seoexpert'}">
			{l s='Product description' mod='seoexpert'}
		</a>
	</li>
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}product_description_short{literal}}{/literal}" title="{l s='Product short description' mod='seoexpert'}">
			{l s='Product short description' mod='seoexpert'}
		</a>
	</li>
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}product_reference{literal}}{/literal}" title="{l s='Product reference' mod='seoexpert'}">
			{l s='Product reference' mod='seoexpert'}
		</a>
	</li>
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}manufacturer_name{literal}}{/literal}" title="{l s='Product manufacturer' mod='seoexpert'}">
			{l s='Product manufacturer' mod='seoexpert'}
		</a>
	</li>
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}product_features{literal}}{/literal}" title="{l s='Product features' mod='seoexpert'}">
			{l s='Product features' mod='seoexpert'}
		</a>
	</li>
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}default_cat_name{literal}}{/literal}" title="{l s='Product category name' mod='seoexpert'}">
			{l s='Product category name' mod='seoexpert'}
		</a>
	</li>
{*
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}parent_cat_name{literal}}{/literal}" title="{l s='Product parent category name' mod='seoexpert'}">
			{l s='Product parent category name' mod='seoexpert'}
		</a>
	</li>
*}
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}product_price{literal}}{/literal}" title="{l s='Product retail price with tax' mod='seoexpert'}">
			{l s='Product retail price with tax' mod='seoexpert'}
		</a>
	</li>
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}product_reduce_price{literal}}{/literal}" title="{l s='Product specific prices with tax' mod='seoexpert'}">
			{l s='Product specific prices with tax' mod='seoexpert'}
		</a>
	</li>
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}product_price_wt{literal}}{/literal}" title="{l s='Product pre-tax retail price' mod='seoexpert'}">
			{l s='Product pre-tax retail price' mod='seoexpert'}
		</a>
	</li>
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}product_reduce_price_wt{literal}}{/literal}" title="{l s='Product pre-tax specific prices' mod='seoexpert'}">
			{l s='Product pre-tax specific prices' mod='seoexpert'}
		</a>
	</li>
	<li>
		<a class="pattern-btn" data-ref="{literal}{{/literal}product_reduction_percent{literal}}{/literal}" title="{l s='Product reduction' mod='seoexpert'}">
			{l s='Product reduction' mod='seoexpert'}
		</a>
	</li>
</ul>