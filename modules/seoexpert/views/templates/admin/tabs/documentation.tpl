{*
* 2007-2017 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2017 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*}

<h3><i class="icon-book"></i> {l s='Documentation' mod='seoexpert'} <small>{$module_display|escape:'htmlall':'UTF-8'}</small></h3>
<p>{l s='The SEO Expert module helps you to quickly ensure search engine optimization on your shop.' mod='seoexpert'}</p>
<p>{l s='This module allows you to automatically create:' mod='seoexpert'}</p>
<ul>
	<li><b>{l s='Your Friendly URL as well as Meta tags of your product pages.' mod='seoexpert'}</b></li>
	<li><b>{l s='Your Social media Meta tags and share links for Facebook and Twitter.' mod='seoexpert'}</b></li>
</ul>
<p>&nbsp;</p>
<div class="media">
	<a class="pull-left" target="_blank" href="{$module_dir|escape:'htmlall':'UTF-8'}{$guide_link|escape:'htmlall':'UTF-8'}">
		<img heigh="64" width="64" class="media-object" src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/pdf.png" alt="" title=""/>
	</a>
	<div class="media-body">
		<h4 class="media-heading"><p>{l s='Attached you will find the documentation for your module. Please consult it to properly configure the module.' mod='seoexpert'}</p></h4>
		<p><a href="{$module_dir|escape:'htmlall':'UTF-8'}{$guide_link|escape:'htmlall':'UTF-8'}" target="_blank">{l s='SEO Expert User Guide' mod='seoexpert'}</a></p>
		{*<p><a href="http://www.prestashop.com/{$white_seo|escape:'htmlall':'UTF-8'}" target="_blank">{l s='SEO White Paper' mod='seoexpert'}</a></p>*}
		<p>{l s='Access to Prestashop free documentation: ' mod='seoexpert'} <a href="http://doc.prestashop.com/" target="_blank">{l s='Click here' mod='seoexpert'}</a></p>
	</div>
</div>

<p>&nbsp;</p>
<p>{l s='Need help? You will find it on' mod='seoexpert'} <a href="#contacts" class="contactus" data-toggle="tab" data-original-title="" title="{l s='Need help?' mod='seoexpert'}">{l s='Contact tab.' mod='seoexpert'}</a></p>