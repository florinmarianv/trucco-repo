{*
* 2007-2017 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2017 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*}

<div class="clearfix"></div>
<div class="tab-pane panel" id="contacts">
	<h3>
		Prestashop Addons
	</h3>
	<div class="form-group">
		<b>{l s='Thank you for choosing a module developed by the Addons Team of PrestaShop.' mod='seoexpert'}</b><br /><br />
		<a target="_blank" href="{$cross_link|escape:'htmlall':'UTF-8'}">{l s='See more facebook features' mod='seoexpert'}</a><br /><br />
		{l s='If you encounter a problem using the module, our team is at your service via the ' mod='seoexpert'} <a target="_blank" href="http://addons.prestashop.com/contact-form.php">{l s='contact form' mod='seoexpert'}.</a><br /><br />

		{l s='To save you time, before you contact us:' mod='seoexpert'}<br />
		- {l s='make sure you have read the documentation well. ' mod='seoexpert'}<br />
		- {l s='in the event you would be contacting us via the form, do not hesitate to give us your first message, maximum of details on the problem and its origins (screenshots, reproduce actions to find the bug, etc. ..) ' mod='seoexpert'}<br /><br />
		{l s='This module has been developped by PrestaShop and can only be sold through' mod='seoexpert'} <a target="_blank" href="http://addons.prestashop.com">addons.prestashop.com</a>.<br /><br />

		The PrestaShop Addons Team<br />
	</div>
</div>