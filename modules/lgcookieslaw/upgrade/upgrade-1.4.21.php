<?php
/**
 *  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
 *
 * @author    Línea Gráfica E.C.E. S.L.
 * @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
 * @license   https://www.lineagrafica.es/licenses/license_en.pdf
 *            https://www.lineagrafica.es/licenses/license_es.pdf
 *            https://www.lineagrafica.es/licenses/license_fr.pdf
 */

function upgrade_module_1_4_21($module)
{
    $sql = array(
        'ALTER TABLE `'._DB_PREFIX_.'lgcookieslaw_lang` ADD `required` TEXT NOT NULL',
        'ALTER TABLE `'._DB_PREFIX_.'lgcookieslaw_lang` ADD `additional` TEXT NOT NULL'
    );
    foreach ($sql as $q) {
        Db::getInstance()->execute($q);
    }
    $messages = array(
        'en' => array(
            'accept'   => 'I accept',
            'moreinfo' => 'More information',
            'message'  =>
                'This website uses its own and third-party cookies to improve our services and show you '.
                'advertising related to your preferences by analyzing your browsing habits. '.
                'To give your consent to its use, press the Accept button.',
            'required' => '<ul>
                 <li>Necessary to navigate this site and use its functions.</li>
                 <li>Identify you as a user and store your preferences such as language and currency.</li>
                 <li>Customize your experience based on your browsing.</li>
             </ul>',
            'additional' => '<ul>
                 <li>Third-party cookies for analytical purposes.</li>
                 <li>Show personalized recommendations based on your browsing on other sites.</li>
                 <li>Show custom campaigns on other websites.</li>
             </ul>',
        ),
        'es' => array(
            'accept'   => 'Acepto',
            'moreinfo' => 'Más información',
            'message'  =>
                'Este sitio web utiliza cookies propias y de terceros para mejorar nuestros servicios '.
                'y mostrarle publicidad relacionada con sus preferencias mediante el análisis de sus hábitos '.
                'de navegación. Para dar su consentimiento sobre su uso pulse el botón Aceptar.',
            'required' => '<ul>
                <li>Necesarias para navegar en este sitio y utilizar sus funciones.</li>
                <li>Identificarle como usuario y almacenar sus preferencias como idioma y moneda.</li>
                <li>Personalizar su experiencia en base con su navegación.</li>
            </ul>',
            'additional' => '<ul>
                <li>Cookies de terceros con propósitos analíticos.</li>
                <li>Mostrar recomendaciones personalizadas basadas en su navegación en otros sitios.</li>
                <li>Mostrar campañas personalizadas en otras sitios web.</li>
            </ul>',
        ),
        'fr' => array(
            'accept'   => 'J\'accepte',
            'moreinfo' => 'Plus d\'informations',
            'message'  =>
                'Ce site Web utilise ses propres cookies et ceux de tiers pour '.
                'améliorer nos services et vous montrer des publicités liées à vos '.
                'préférences en analysant vos habitudes de navigation. '.
                'Pour donner votre consentement à son utilisation, appuyez sur le bouton Accepter.',
            'required' => '<ul>
                 <li>Nécessaire pour naviguer sur ce site et utiliser ses fonctions.</li>
                 <li>Vous identifier en tant qu\'utilisateur et enregistrer vos préférences telles que '.
                 'la langue et la devise.</li>
                 <li>Personnalisez votre expérience en fonction de votre navigation.</li>
             </ul> ',
             'additional' => '<ul>
                 <li>Cookies tiers à des fins d\'analyse.</li>
                 <li>Afficher des recommandations personnalisées en fonction de votre navigation '.
                 'sur d\'autres sites</li>
                 <li>Afficher des campagnes personnalisées sur d\'autres sites Web</li>
             </ul> ',
        ),
        'it' => array(
            'accept'   => 'Accetto',
            'moreinfo' => 'Piú info',
            'message'  =>
                'Questo sito web utilizza cookie propri e di terze parti per migliorare i'.
                ' nostri servizi e mostrarti pubblicità relativa alle tue preferenze analizzando'.
                ' le tue abitudini di navigazione. Per dare il tuo consenso al suo utilizzo, '.
                'premi il pulsante Accetta.',
            'required' => '<ul>
                 <li>Necessario per navigare in questo sito e utilizzare le sue funzioni.</li>
                 <li>Identificarti come utente e memorizzare le tue preferenze come lingua e valuta.</li>
                 <li>Personalizza la tua esperienza in base alla tua navigazione.</li>
             </ul>',
             'additional' => '<ul>
                 <li>Cookie di terze parti per scopi analitici.</li>
                 <li>Mostra consigli personalizzati basati sulla tua navigazione su altri siti.</li>
                 <li>Mostra campagne personalizzate su altri siti web.</li>
             </ul>',
        ),
    );
    $languages = Language::getLanguages();
    foreach ($languages as $language) {
        if (isset($messages[$language['iso_code']])) {
            $message = $messages[$language['iso_code']];
        } else {
            $message = $messages['en'];
        }

        Db::getInstance()->Execute(
            'UPDATE `'._DB_PREFIX_.'lgcookieslaw_lang` SET 
            `required` = "'.pSQL($message['required'], 'html').'",
            `additional` = "'.pSQL($message['additional'], 'html').'" 
            WHERE `id_lang` = '. (int)$language['id_lang']
        );
    }
    return true;
}
