<?PHP
$expire=time()+60*60*24*30*12;

function vaciar_cache_prestashop()
{
    $service_url="https://www.armonias.com/nubeado/clear_cache_prestashopppp.php?token=vBnmmP3asdasdas121218";

    $curl = curl_init($service_url);
    
    $header = array();    
    curl_setopt($curl, CURLOPT_HTTPHEADER,$header);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);    
    $curl_response = curl_exec($curl);
    if ($curl_response === false) {
	$info = curl_getinfo($curl);
	curl_close($curl);
	die('Error durante la ejecucion de CURL. ' . var_export($info));
    }
    curl_close($curl);
    $decoded = json_decode($curl_response);
    if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
	die('Error: ' . $decoded->response->errormessage);
    }    
}//fin del function vaciar_cache_prestashop()


function generar_tpl_mensaje_arriba($llamada_desde_contrab="No")
{    
    $conexion_prestashop = new conexion_prestashop();
    
    if(strcmp($llamada_desde_contrab,"Si")==0)
	$prestashop_ruta_plantilla=$conexion_prestashop->prestashop_ruta_plantilla_crontab;
    else
	$prestashop_ruta_plantilla=$conexion_prestashop->prestashop_ruta_plantilla;
        
    $cadena_archivo="";
    
    $sqlMensajeInicio="SELECT * FROM mensaje_arriba_web
		WHERE mensaje_Estado='Activo'
		AND mensaje_HoraFechaFin>'" . date("Y-m-d") . "'
		AND '" . date("Y-m-d") . "' BETWEEN mensaje_Aparece_FechaInicio AND mensaje_Aparece_FechaFin
		ORDER BY mensaje_Orden DESC";
    //print($sqlMensajeInicio);
    $resMensajeInicio=$conexion_prestashop->BD_Consulta($sqlMensajeInicio);
    $tuplaMensajeInicio=$conexion_prestashop->BD_GetTupla($resMensajeInicio);
    
    if($tuplaMensajeInicio!=NULL)
    {
	$cadena_archivo=$cadena_archivo . "<div id=\"rotate\" class=\"top-envios text-center\" style=\"background: " . $tuplaMensajeInicio['mensaje_ColorFondo'] . ";\">";
	
	//$mensaje_FechaInicio = date('d/m/Y',strtotime($tuplaMensajeInicio['mensaje_HoraFechaFin']));
	$mensaje_FechaInicio = date('M d, Y',strtotime($tuplaMensajeInicio['mensaje_HoraFechaFin']));
	$hora_Inicio = date('H',strtotime($tuplaMensajeInicio['mensaje_HoraFechaFin']));
	$minuto_Inicio = date('i',strtotime($tuplaMensajeInicio['mensaje_HoraFechaFin']));
	
	
	//TEXTO 1
	if(trim($tuplaMensajeInicio['mensaje_Texto'])!="")
	{
	    $cadena_archivo=$cadena_archivo . "<span class=\"quotes\">" . $tuplaMensajeInicio['mensaje_Texto'] . "</span>";
	}//fin del if(trim($tuplaMensajeInicio['mensaje_Texto'])!="")
	    
	//TEXTO 2
	if(trim($tuplaMensajeInicio['mensaje_Texto1'])!="")
	{
	    $cadena_archivo=$cadena_archivo . "<span class=\"quotes\" style=\"display:none;\">" . $tuplaMensajeInicio['mensaje_Texto1'] . "</span>";
	}//fin del if(trim($tuplaMensajeInicio['mensaje_Texto1'])!="")
	    
	//TEXTO 3	    
	if(trim($tuplaMensajeInicio['mensaje_Texto2'])!="")
	{
	    $cadena_archivo=$cadena_archivo . "<span class=\"quotes\" style=\"display:none;\">" . $tuplaMensajeInicio['mensaje_Texto2'] . "</span>";
	}//fin del if(trim($tuplaMensajeInicio['mensaje_Texto2'])!="")
	    
	if(strcmp($tuplaMensajeInicio['mensaje_ApareceHoraFin'],"Si")==0)
	{
	    $cadena_archivo=$cadena_archivo . "<ul class=\"ul-count-down\">
		    <li id=\"li_dias\"><span id=\"days\"></span> D</li>
		    <li><span id=\"hours\"></span> H</li>
		    <li><span id=\"minutes\"></span> M</li>
		    <li><span id=\"seconds\"></span> S</li>
		</ul>
		
		</div>
		
		
		<script type=\"text/javascript\">
						
			const second = 1000,
			      minute = second * 60,
			      hour = minute * 60,
			      day = hour * 24;
		
			let countDown = new Date('" . $mensaje_FechaInicio . " " . $hora_Inicio . ":" . $minuto_Inicio . ":00').getTime(),
			    x = setInterval(function() {
		
				let now = new Date().getTime(),
				distance = countDown - now;
				
				var days=Math.floor(distance / (day));
				var hours=Math.floor((distance % (day)) / (hour));
				var minutes=Math.floor((distance % (hour)) / (minute));
				var seconds=Math.floor((distance % (minute)) / second);
				
				if(days>0)
				{
				    document.getElementById('days').innerText = days;
				}//fin del if(days>0)
				else
				{
				    document.getElementById('li_dias').style.display = 'none';
				}//fin del else del if(days>0)
		
				document.getElementById('hours').innerText = hours;
				document.getElementById('minutes').innerText = minutes;
				document.getElementById('seconds').innerText = seconds;
			      
			      
		
			    }, second)
		
		</script>";
	}//fin del if(strcmp($tuplaMensajeInicio['mensaje_ApareceHoraFin'],"Si")==0)
	else
	{
	    $cadena_archivo=$cadena_archivo . "</div>";
	}//fin del else del if(strcmp($tuplaMensajeInicio['mensaje_ApareceHoraFin'],"Si")==0)
    }//fin del if($tuplaMensajeInicio!=NULL)
    
    $cadena_archivo=$cadena_archivo . "	
		<script>	    	
		(function() {
		    var quotes = $('.quotes');
		    var quoteIndex = -1;
		    
		    function showNextQuote() {
			++quoteIndex;
			quotes.eq(quoteIndex % quotes.length)
			    .fadeIn(500)
			    .delay(4000)
			    .fadeOut(500, showNextQuote);
		    }
		    
		    showNextQuote();
					
		})();
		</script>";
    
    $ruta_archivo=$prestashop_ruta_plantilla . "_partials/header/aviso_nubeado.tpl";
    //$ruta_archivo="../../../categorias_nubeado.tpl";
    
    $file = fopen($ruta_archivo, "w");
    fwrite($file, $cadena_archivo);  
    fclose($file);
}//fin del function generar_tpl_mensaje_arriba()


function generar_tpl_mensaje_arriba_yesitex($llamada_desde_contrab="No")
{    
    $conexion_prestashop = new conexion_prestashop();
    
    if(strcmp($llamada_desde_contrab,"Si")==0)
	$prestashop_ruta_plantilla=$conexion_prestashop->prestashop_ruta_plantilla_yesitex_crontab;
    else
	$prestashop_ruta_plantilla=$conexion_prestashop->prestashop_ruta_plantilla_yesitex;
        
    $cadena_archivo="";
    
    $sqlMensajeInicio="SELECT * FROM mensaje_arriba_web_yesitex
		WHERE mensaje_Estado='Activo'
		AND mensaje_HoraFechaFin>'" . date("Y-m-d") . "'
		AND '" . date("Y-m-d H:i:s") . "' BETWEEN mensaje_Aparece_FechaInicio AND mensaje_Aparece_FechaFin
		ORDER BY mensaje_Orden DESC";
    //print($sqlMensajeInicio);
    $resMensajeInicio=$conexion_prestashop->BD_Consulta($sqlMensajeInicio);
    $tuplaMensajeInicio=$conexion_prestashop->BD_GetTupla($resMensajeInicio);
    
    if($tuplaMensajeInicio!=NULL)
    {
	$cadena_archivo=$cadena_archivo . "<div id=\"rotate\" class=\"top-envios text-center\" style=\"background: " . $tuplaMensajeInicio['mensaje_ColorFondo'] . ";\">";
	
	//$mensaje_FechaInicio = date('d/m/Y',strtotime($tuplaMensajeInicio['mensaje_HoraFechaFin']));
	$mensaje_FechaInicio = date('M d, Y',strtotime($tuplaMensajeInicio['mensaje_HoraFechaFin']));
	$hora_Inicio = date('H',strtotime($tuplaMensajeInicio['mensaje_HoraFechaFin']));
	$minuto_Inicio = date('i',strtotime($tuplaMensajeInicio['mensaje_HoraFechaFin']));
	
	//TEXTO 1
	if(trim($tuplaMensajeInicio['mensaje_Texto'])!="")
	{
	    $cadena_archivo=$cadena_archivo . "<span class=\"quotes\">" . $tuplaMensajeInicio['mensaje_Texto'] . "</span>";
	}//fin del if(trim($tuplaMensajeInicio['mensaje_Texto'])!="")
	    
	//TEXTO 2
	if(trim($tuplaMensajeInicio['mensaje_Texto1'])!="")
	{
	    $cadena_archivo=$cadena_archivo . "<span class=\"quotes\" style=\"display:none;\">" . $tuplaMensajeInicio['mensaje_Texto1'] . "</span>";
	}//fin del if(trim($tuplaMensajeInicio['mensaje_Texto1'])!="")
	    
	//TEXTO 3	    
	if(trim($tuplaMensajeInicio['mensaje_Texto2'])!="")
	{
	    $cadena_archivo=$cadena_archivo . "<span class=\"quotes\" style=\"display:none;\">" . $tuplaMensajeInicio['mensaje_Texto2'] . "</span>";
	}//fin del if(trim($tuplaMensajeInicio['mensaje_Texto2'])!="")
	    
	if(strcmp($tuplaMensajeInicio['mensaje_ApareceHoraFin'],"Si")==0)
	{
	    $cadena_archivo=$cadena_archivo . "<ul class=\"ul-count-down\">
		    <li id=\"li_dias\"><span id=\"days\"></span> D</li>
		    <li><span id=\"hours\"></span> H</li>
		    <li><span id=\"minutes\"></span> M</li>
		    <li><span id=\"seconds\"></span> S</li>
		</ul>
		
		</div>
		
		
		<script type=\"text/javascript\">
						
			const second = 1000,
			      minute = second * 60,
			      hour = minute * 60,
			      day = hour * 24;
		
			let countDown = new Date('" . $mensaje_FechaInicio . " " . $hora_Inicio . ":" . $minuto_Inicio . ":00').getTime(),
			    x = setInterval(function() {
		
				let now = new Date().getTime(),
				distance = countDown - now;
				
				var days=Math.floor(distance / (day));
				var hours=Math.floor((distance % (day)) / (hour));
				var minutes=Math.floor((distance % (hour)) / (minute));
				var seconds=Math.floor((distance % (minute)) / second);
				
				if(days>0)
				{
				    document.getElementById('days').innerText = days;
				}//fin del if(days>0)
				else
				{
				    document.getElementById('li_dias').style.display = 'none';
				}//fin del else del if(days>0)
		
				document.getElementById('hours').innerText = hours;
				document.getElementById('minutes').innerText = minutes;
				document.getElementById('seconds').innerText = seconds;
			      
			      
		
			    }, second)
		
		</script>";
	}//fin del if(strcmp($tuplaMensajeInicio['mensaje_ApareceHoraFin'],"Si")==0)
	else
	{
	    $cadena_archivo=$cadena_archivo . "</div>";
	}//fin del else del if(strcmp($tuplaMensajeInicio['mensaje_ApareceHoraFin'],"Si")==0)
    }//fin del if($tuplaMensajeInicio!=NULL)
    
    $cadena_archivo=$cadena_archivo . "	
		<script>	    	
		(function() {
		    var quotes = $('.quotes');
		    var quoteIndex = -1;
		    
		    function showNextQuote() {
			++quoteIndex;
			quotes.eq(quoteIndex % quotes.length)
			    .fadeIn(500)
			    .delay(4000)
			    .fadeOut(500, showNextQuote);
		    }
		    
		    showNextQuote();
					
		})();
		</script>";
		
    
    
    $ruta_archivo=$prestashop_ruta_plantilla . "_partials/header/aviso_nubeado.tpl";
    //$ruta_archivo="../../../categorias_nubeado.tpl";
    
    $file = fopen($ruta_archivo, "w");
    fwrite($file, $cadena_archivo);  
    fclose($file);
}//fin del function generar_tpl_mensaje_arriba_yesitex()

function generar_tpl_categorias_seccion($seccion_SeccionPrestashopFK)
{
    $conexion_prestashop = new conexion_prestashop();
    
    $prestashop_ruta_plantilla=$conexion_prestashop->prestashop_ruta_plantilla;
    $prestashop_ruta_imagen_seccion=$conexion_prestashop->prestashop_ruta_imagen_seccion;
    
    $cadena_archivo="";
    
    $sqlBannerSeccion="SELECT * FROM banner_seccion
	WHERE seccion_Imagen!=''
	AND seccion_SeccionPrestashopFK=" . $seccion_SeccionPrestashopFK . "
	ORDER BY seccion_Orden ASC";
    $resBannerSeccion=$conexion_prestashop->BD_Consulta($sqlBannerSeccion);
    $tuplaBannerSeccion=$conexion_prestashop->BD_GetTupla($resBannerSeccion);
    
    while($tuplaBannerSeccion!=NULL)
    {
	$cadena_archivo=$cadena_archivo . "<div class=\"item\">
		    	<a href=\"" . $tuplaBannerSeccion['seccion_URL'] . "\"><img src=\"" . $prestashop_ruta_imagen_seccion . $tuplaBannerSeccion['seccion_Imagen'] . "\" alt=\"Ir a " . $tuplaBannerSeccion['seccion_Titulo'] . "\"></a>
		    	<h4>" . $tuplaBannerSeccion['seccion_Titulo'] . "</h4>
		    	</div>";
	
	$tuplaBannerSeccion=$conexion_prestashop->BD_GetTupla($resBannerSeccion);
    }//fin del while($tuplaBannerSeccion!=NULL)
    
    if(trim($cadena_archivo)!="")
    {
	$cadena_archivo="<section class=\"por-categoria\">
		    <div class=\"container clearfix text-align cont-cat-carousel\">
			<div class=\"carousel-wrap\">
			    <h3 class=\"block-title\">Compra por categoria</h3>
			    <div class=\"owl-categorias\">
				" . $cadena_archivo . "            
			  </div>
			</div>            
		    </div>        
		</section>";
    }//fin del if(trim($cadena_archivo)!="")
    
    $ruta_archivo=$prestashop_ruta_plantilla . "_partials/content/panel_nube/categorias_nubeado_" . $seccion_SeccionPrestashopFK . ".tpl";
    //$ruta_archivo="../../../categorias_nubeado.tpl";
    
    $file = fopen($ruta_archivo, "w");
    fwrite($file, $cadena_archivo);  
    fclose($file);
}//fin del function generar_tpl_categorias_seccion($seccion_SeccionPrestashopFK)

function generar_tpl_categorias_inicio()
{    
    $conexion_prestashop = new conexion_prestashop();
    
    $prestashop_ruta_plantilla=$conexion_prestashop->prestashop_ruta_plantilla;
    $prestashop_ruta_imagen=$conexion_prestashop->prestashop_ruta_imagen;
    
    $cadena_archivo="";
    
    $sqlBannerIni="SELECT * FROM banner_inicio
	WHERE inicio_Imagen!=''
	ORDER BY inicio_Orden ASC";
    $resBannerIni=$conexion_prestashop->BD_Consulta($sqlBannerIni);
    $tuplaBannerIni=$conexion_prestashop->BD_GetTupla($resBannerIni);
    
    while($tuplaBannerIni!=NULL)
    {
	$cadena_archivo=$cadena_archivo . "<div class=\"item\">
		    	<a href=\"" . $tuplaBannerIni['inicio_URL'] . "\"><img src=\"" . $prestashop_ruta_imagen . $tuplaBannerIni['inicio_Imagen'] . "\" alt=\"Ir a " . $tuplaBannerIni['inicio_Titulo'] . "\"></a>
		    	<h4>" . $tuplaBannerIni['inicio_Titulo'] . "</h4>
		    	</div>";
	
	$tuplaBannerIni=$conexion_prestashop->BD_GetTupla($resBannerIni);
    }//fin del while($tuplaBannerIni!=NULL)
    
    $ruta_archivo=$prestashop_ruta_plantilla . "_partials/content/categorias_nubeado.tpl";
    //$ruta_archivo="../../../categorias_nubeado.tpl";
    
    $file = fopen($ruta_archivo, "w");
    fwrite($file, $cadena_archivo);  
    fclose($file);
}//fin del function generar_tpl_categorias_inicio()

function generar_tpl_categorias_inicio_yesitex()
{    
    $conexion_prestashop = new conexion_prestashop();
    
    $prestashop_ruta_plantilla_yesitex=$conexion_prestashop->prestashop_ruta_plantilla_yesitex;
    $prestashop_ruta_imagen_yesitex=$conexion_prestashop->prestashop_ruta_imagen_yesitex;
    
    $cadena_archivo="";
    
    $sqlBannerIni="SELECT * FROM banner_inicio_yesitex
	WHERE inicio_Imagen!=''
	ORDER BY inicio_Orden ASC";
    $resBannerIni=$conexion_prestashop->BD_Consulta($sqlBannerIni);
    $tuplaBannerIni=$conexion_prestashop->BD_GetTupla($resBannerIni);
    
    while($tuplaBannerIni!=NULL)
    {
	$cadena_archivo=$cadena_archivo . "<div class=\"item\">
		    	<a href=\"" . $tuplaBannerIni['inicio_URL'] . "\"><img src=\"" . $prestashop_ruta_imagen_yesitex . $tuplaBannerIni['inicio_Imagen'] . "\" alt=\"Ir a " . $tuplaBannerIni['inicio_Titulo'] . "\"></a>
		    	<h4>" . $tuplaBannerIni['inicio_Titulo'] . "</h4>
		    	</div>";
	
	$tuplaBannerIni=$conexion_prestashop->BD_GetTupla($resBannerIni);
    }//fin del while($tuplaBannerIni!=NULL)
    
    $ruta_archivo=$prestashop_ruta_plantilla_yesitex . "_partials/content/categorias_nubeado.tpl";
    //$ruta_archivo="../../../categorias_nubeado.tpl";
    
    $file = fopen($ruta_archivo, "w");
    fwrite($file, $cadena_archivo);  
    fclose($file);
}//fin del function generar_tpl_categorias_inicio_yesitex()

function imprime_cabecera()
{    
    print("<!--HEADER SLIDE-->
    <header id=\"header\" class=\"l-header-slide l-header-slide-3 t-header-slide-3\">
      <div class=\"widget-weather-forecast l-row\">
        <div class=\"l-span-xl-10 l-span-lg-9 l-span-md-9 l-span-sm-12\">
          <div id=\"weatherForecast\">&amp;nbsp;
          </div>
        </div>
        <div class=\"l-span-xl-2 l-span-lg-3 l-span-md-3 hidden-sm hidden-xs\">
          <div class=\"weather-forecast-settings\">
            <h3>Forecast Settings</h3>
            <div class=\"form-group\">
              <div class=\"input-group input-group-sm\">
                <div class=\"input-group-addon\"><i class=\"fa fa-map-marker\"></i></div>
                <input type=\"text\" placeholder=\"Search location\" value=\"New York, NY\" class=\"forecast-location form-control\"><span class=\"input-group-btn\">
                  <button type=\"button\" class=\"btn btn-dark forecast-location-btn\"><i class=\"fa fa-search\"></i></button></span>
              </div>
            </div>
            <div class=\"forecastcheckbo\">
              <div>
                <label class=\"forecast-unit cb-radio cb-radio-primary-1\">
                  <input type=\"radio\" name=\"temperature\" value=\"f\" checked=\"checked\">Fahrenheit
                </label>
              </div>
              <div>
                <label class=\"forecast-unit cb-radio cb-radio-primary-1\">
                  <input type=\"radio\" name=\"temperature\" value=\"c\">Celsius
                </label>
              </div>
            </div><a href=\"#\" class=\"forecast-geo-location\">Use Your Location</a>
          </div>
        </div>
      </div>
    </header>");		    
}//fin del function imprime_cabecera()

function imprime_header()
{
    include_once "fechas.php";
    
    $conexion_prestashop = new conexion_prestashop();
    $vectorUsuario=Seguridad();
    
    $nombre_empresa=$conexion_prestashop->nombre_empresa_panel_control;
    
    print("<!--HEADER-->
        <header class=\"l-header l-header-1 t-header-1\">
          <div class=\"navbar navbar-ason\">
            <div class=\"container-fluid\">
              <div class=\"navbar-header\">
                <button type=\"button\" data-toggle=\"collapse\" data-target=\"#ason-navbar-collapse\" class=\"navbar-toggle collapsed\"><span class=\"sr-only\">Toggle navigation</span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span></button><a href=\"index.php\" class=\"navbar-brand widget-logo\"><span class=\"logo-default-header\"><img src=\"../img/logo_dark.png\" alt=\"Proteus\"></span></a>
              </div>
              <div id=\"ason-navbar-collapse\" class=\"collapse navbar-collapse\">
                <div class=\"col-md-4\">
                  <h2 class=\"page-title\">Bienvenido <strong>" . $nombre_empresa . " - " . utf8_encode($vectorUsuario['firstname']) . " " . utf8_encode($vectorUsuario['lastname']) . "</strong></h2>
                </div>
                <ul class=\"nav navbar-nav navbar-right\">");
    
	
                /*<li>
                    <!-- Slide Header Switcher--><a href=\"#\" title=\"Show Weather Forecast\" data-ason-type=\"header\" data-ason-header=\"l-header-slide-3\" data-ason-header-push=\"l-header-slide-push-3\" data-ason-anim-all='[\"velocity\",\"transition.expandIn\",\"transition.fadeOut\"]' data-ason-is-push=\"true\" data-ason-target=\"#header\" class=\"sidebar-switcher switcher t-switcher-header ason-widget\"><i class=\"fa fa-cloud\"></i></a>
                  </li>*/
        print("<li class=\"hidden-sm\">
                    <!-- Full Screen Toggle--><a href=\"#\" class=\"full-screen-page sidebar-switcher switcher t-switcher-header\"><i class=\"fa fa-expand\"></i></a>
                  </li>
                  <li>
		    <!-- Close --><a href=\"../desconectar.php\"><i class=\"fa fa-power-off\"></i> Cerrar sesión</a>");
    
                    /*<!-- Profile Widget-->
                    <div class=\"widget-profile profile-in-header\">
                      <button type=\"button\" data-toggle=\"dropdown\" class=\"btn dropdown-toggle\"><span class=\"name\">Pepito Pérez</span><img src=\"" . $imagen_user . "\"></button>
                      <ul role=\"menu\" class=\"dropdown-menu\">
                        <!--li><a href=\"page-profile.html\"><i class=\"fa fa-user\"></i>Profile</a></li>
                        <li><a href=\"app-mail.html\"><i class=\"fa fa-envelope\"></i>Inbox</a></li>
                        <li><a href=\"#\"><i class=\"fa fa-cog\"></i>Settings</a></li>
                        <li class=\"lock\"><a href=\"page-lock-screen.html\"><i class=\"fa fa-lock\"></i>Log Out</a></li-->
                        <li class=\"power\"><a href=\"#\"><i class=\"fa fa-power-off\"></i>Cerrar sesión</a></li>
                      </ul>
		      <div class=\"power\"><a href=\"#\"><i class=\"fa fa-power-off\"></i>Cerrar sesión</a></div>
                    </div>*/
                print("</li>
                </ul>
              </div>
            </div>
          </div>
        </header>");	    
}//fin del function imprime_header()


function imprime_sidebar($section)
{
    include_once "../../clases/seguridad_intranet.php";
    include_once "../../clases/conexion_prestashop.php";
    
    $vectorUsuario=Seguridad();
    $conexion_prestashop=new conexion_prestashop();
    
    $active_informacion_articulos="";
    
    if(strcmp($section,"active_informacion_articulos")==0)
	$active_informacion_articulos="class=\"active\"";
	
	
    
    
    
    print("<aside id=\"sb-left\" class=\"l-sidebar t-sidebar-1 l-sidebar-left l-initial l-sidebar-1\">
        <!--Switcher-->
        <div class=\"l-side-box\"><a href=\"#\" data-ason-type=\"sidebar\" data-ason-to-sm=\"sidebar\" data-ason-target=\"#sb-left\" class=\"sidebar-switcher switcher t-switcher-side ason-widget\"><i class=\"fa fa-bars\"></i></a></div>
        <!-- Profile in sidebar-->
        <!--div class=\"widget-profile-2 profile-2-in-side-2 t-profile-2-3\"><a href=\"#\" title=\"Toggle Profile\" class=\"tt-left profile-2-toggle\"><i class=\"fa fa-angle-down\"></i></a>
          <div class=\"profile-2-wrapper\">
            <div class=\"profile-2-details\">
              <div class=\"profile-2-img\"><a href=\"page-profile.html\"><img src=\"img/profile/profile.jpg\"></a></div>
              <ul class=\"profile-2-info\">
                <li>
                  <h3>William Jones</h3>
                </li>
                <li>Designer</li>
                <li>
                  <div class=\"btn-group btn-group-sm btn-group-justified\"><a role=\"button\" title=\"Social Stats\" class=\"toggle-stats btn btn-dark tt-top\"><i class=\"fa fa-rss\"></i></a><a role=\"button\" title=\"Visitor Stats\" class=\"toggle-visitors btn btn-dark tt-top\"><i class=\"fa fa-area-chart\"></i></a><a href=\"page-profile.html\" title=\"Edit Profile\" class=\"btn btn-dark tt-top\"><i class=\"fa fa-cogs\"></i></a></div>
                </li>
              </ul>
            </div>
            <div class=\"profile-2-social-stats\">
              <div class=\"l-span-xs-4\">
                <div class=\"profile-2-status-nr text-danger\">527</div>Likes
              </div>
              <div class=\"l-span-xs-4\">
                <div class=\"profile-2-status-nr text-info\">232</div>Comments
              </div>
              <div class=\"l-span-xs-4\">
                <div class=\"profile-2-status-nr text-success\">15</div>Messages
              </div>
            </div>
            <div class=\"profile-2-chart\">
              <div class=\"hide rickshaw-visitors\"></div>
              <div id=\"rickshawVisitors\"></div>
              <div id=\"rickshawVisitorsLegend\" class=\"visitors_rickshaw_legend\"></div>
            </div>
          </div>
        </div-->
        <!-- Logo in Sidebar-->
        <div class=\"l-side-box\">
          <!--Logo-->
          <div class=\"widget-logo logo-in-side\">
            <h1><a href=\"../inicio/index.php\">
		<span class=\"logo-default visible-default-inline-block\"><img src=\"../img/logo_dark.png\" alt=\"Logo\" /></span>
		<span class=\"logo-medium visible-compact-inline-block\"><img src=\"../img/logo_medium.png\" alt=\"Logo\" title=\"Logo\"></span>
                <span class=\"logo-small visible-collapsed-inline-block\"><img src=\"../img/logo_small.png\" alt=\"Logo\" title=\"Logo\" /></spanl>
		</a></h1>
          </div>
        </div>
        <!--Search-->
        <!--div class=\"l-side-box mt-5 mb-10\">
          <div class=\"widget-search search-in-side is-search-left t-search-4\">
            <div class=\"search-toggle\"><i class=\"fa fa-search\"></i></div>
            <div class=\"search-content\">
              <form role=\"form\" action=\"page-search.html\">
                <input type=\"text\" placeholder=\"Search...\" class=\"form-control\">
                <button type=\"submit\" class=\"btn\"><i class=\"fa fa-search\"></i></button>
              </form>
            </div>
          </div>
        </div-->
        <!--Main Menu-->
        <div class=\"l-side-box\">
          <!--MAIN NAVIGATION MENU-->
          <nav class=\"navigation\">
            <ul data-ason-type=\"menu\" class=\"ason-widget\">");
	
		    print("<li " . $active_informacion_articulos . "><a href=\"#\"><i class=\"icon fa fa-thumb-tack\"></i><span class=\"title\">FICHEROS MAESTROS</span><span class=\"arrow\"><i class=\"fa fa-angle-left\"></i></span></a>
			    <ul>				
				<li " . $active_informacion_articulos . "><a href=\"../informacion_articulo/index.php\"><i class=\"icon fa fa-database\"></i><span class=\"title\">Información Articulos</span><span class=\"\"></span></a></li>
			    </ul>
			</li>");	
		
            print("</ul>
          </nav>
        </div>
      </aside>");
}//fin del function imprime_sidebar($section)


function imprime_pie()
{
    print("<footer class=\"l-footer l-footer-1 t-footer-1 clearfix\">
          <br />
          <div class=\"group pt-10 pb-10 ph\">
            <div class=\"copyright pull-left\">              
              © Copyright 2020
            </div>
            <div class=\"version pull-right\">v 1.0</div>
          </div>
        </footer>");
}//fin del function imprime_pie()

function imprime_script_impresion()
{
    print("<script src=\"../jQuery.print.js\"></script>
    <script type='text/javascript'>
    //<![CDATA[
    jQuery(function($) { 'use strict';
	$(\"#ele2\").find('.print-link').on('click', function() {
	    //Print ele2 with default options
	    $.print(\"#ele2\");
	});
	$(\"#ele4\").find('button').on('click', function() {
	    //Print ele4 with custom options
	    $(\"#ele4\").print({
		//Use Global styles
		globalStyles : false,
		//Add link with attrbute media=print
		mediaPrint : false,
		//Custom stylesheet
		stylesheet : \"http://fonts.googleapis.com/css?family=Inconsolata\",
		stylesheet : \"../css/basic.css\",
		stylesheet : \"../css/general.css\",
		stylesheet : \"../css/theme.css\",
		//Print in a hidden iframe
		iframe : false,
		//Don't print this
		noPrintSelector : \".avoid-this\",
		//Add this at top
		prepend : \"Hello World!!!<br/>\",
		//Add this on bottom
		append : \"<br/>Buh Bye!\",
		//Log to console when printing is done via a deffered callback
		deferred: $.Deferred().done(function() { console.log('Printing done', arguments); })
	    });
	});
	// Fork https://github.com/sathvikp/jQuery.print for the full list of options
    });
    //]]>
    </script>");
}//fin del function imprime_script_impresion()
?>