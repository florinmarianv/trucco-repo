<?PHP
error_reporting(E_ALL);
ini_set('display_errors', '1');

session_start();
           
include_once "../../clases/fun_aux_menu_intranet.php";
include_once "../../clases/fechas.php";
include_once "../../clases/conexion_prestashop.php";
include_once "../../clases/seguridad_intranet.php";

$conexion_prestashop = new conexion_prestashop();    
$vectorUsuario=Seguridad();

$sqlSolucionarTransportista="SELECT * FROM prstshp_carrier WHERE active=1 AND deleted=0;";
$resSolucionarTransportista=$conexion_prestashop->BD_Consulta($sqlSolucionarTransportista);
$tuplaSolucionarTransportista=$conexion_prestashop->BD_GetTupla($resSolucionarTransportista);

while($tuplaSolucionarTransportista!=NULL)
{
    //SQL ACTUALIZACION
    $sqlActualizacion="SELECT * FROM prstshp_carrier
        WHERE id_reference=" . $tuplaSolucionarTransportista['id_reference'] . "
        AND id_carrier!=" . $tuplaSolucionarTransportista['id_carrier'];
    $resActualizacion=$conexion_prestashop->BD_Consulta($sqlActualizacion);
    $tuplaActualizacion=$conexion_prestashop->BD_GetTupla($resActualizacion);
    
    $cadena_in="";
    while($tuplaActualizacion!=NULL)
    {
        if(trim($cadena_in)!="")
            $cadena_in=$cadena_in . ", ";
            
        $cadena_in=$cadena_in . $tuplaActualizacion['id_carrier'];
        
        $tuplaActualizacion=$conexion_prestashop->BD_GetTupla($resActualizacion);
    }//fin del while($tuplaActualizacion!=NULL)
    
    $updateTransporte="UPDATE prstshp_orders SET id_carrier=" . $tuplaSolucionarTransportista['id_carrier'] . " WHERE id_carrier IN(" . $cadena_in . ")";
    $resTransporte=$conexion_prestashop->BD_Consulta($updateTransporte);
    //print($updateTransporte . "<br /><br />");
    
    
    $tuplaSolucionarTransportista=$conexion_prestashop->BD_GetTupla($resSolucionarTransportista);
}//fin del while($tuplaSolucionarTransportista!=NULL)

print("<strong>Modificados los transportistas para los pedidos.</strong>");
?>