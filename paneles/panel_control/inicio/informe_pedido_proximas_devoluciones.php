<?PHP
error_reporting(E_ALL);
ini_set('display_errors', '1');

session_start();
           
include_once "../../clases/fun_aux_menu_intranet.php";
include_once "../../clases/fechas.php";
include_once "../../clases/conexion_prestashop.php";
include_once "../../clases/seguridad_intranet.php";

$conexion_prestashop = new conexion_prestashop();    
$vectorUsuario=Seguridad();

header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=devolucion_" . date("HisYmd") . ".xls");



$sqlPedidoDetalleAnulado="SELECT por.id_order AS Num_Pedido, CONCAT(c.firstname, ' ', c.lastname) AS Nombre, c.email AS Email, a.phone AS Telefono, 
            a.city AS Poblacion, a.address1 AS Direccion, a.postcode AS CP, s.name AS Provincia
        FROM prstshp_order_return por
        INNER JOIN prstshp_customer c ON c.id_customer=por.id_customer
        INNER JOIN prstshp_orders o ON o.id_order=por.id_order
        INNER JOIN prstshp_address a ON a.id_address=o.id_address_delivery
        INNER JOIN prstshp_state s ON s.id_state=a.id_state;";
$resPedidoDetalleAnulado=$conexion_prestashop->BD_Consulta($sqlPedidoDetalleAnulado);
$tuplaPedidoDetalleAnulado=$conexion_prestashop->BD_GetTupla($resPedidoDetalleAnulado);

$cadena="<table>
  <tr>
    <th style='background:#CCC; color:#000'>Num Pedido</th>
    <th style='background:#CCC; color:#000'>Nombre</th>
    <th style='background:#CCC; color:#000'>Provincia</th>
    <th style='background:#CCC; color:#000'>Poblacion</th>
    <th style='background:#CCC; color:#000'>Direccion</th>
    <th style='background:#CCC; color:#000'>CP</th>
    <th style='background:#CCC; color:#000'>Telefono</th>
    <th style='background:#CCC; color:#000'>Email</th>
  </tr>";

while($tuplaPedidoDetalleAnulado!=NULL)
{       
    $cadena=$cadena . "<tr>
        <td>" . $tuplaPedidoDetalleAnulado['Num_Pedido'] . "</td>
        <td>" . utf8_encode($tuplaPedidoDetalleAnulado['Nombre']) . "</td>
        <td>" . utf8_encode($tuplaPedidoDetalleAnulado['Provincia']) . "</td>
        <td>" . utf8_encode($tuplaPedidoDetalleAnulado['Poblacion']) . "</td>
        <td>" . utf8_encode($tuplaPedidoDetalleAnulado['Direccion']) . "</td>
        <td>" . utf8_encode($tuplaPedidoDetalleAnulado['CP']) . "</td>
        <td>" . utf8_encode($tuplaPedidoDetalleAnulado['Telefono']) . "</td>
        <td>" . $tuplaPedidoDetalleAnulado['Email'] . "</td>
      </tr>";
    
    
    
    
    $tuplaPedidoDetalleAnulado=$conexion_prestashop->BD_GetTupla($resPedidoDetalleAnulado);    
}//fin del while($tuplaPedidoDetalleAnulado!=NULL)


$cadena=$cadena . "</table>";

$cadena=utf8_decode($cadena);

print($cadena);
?>