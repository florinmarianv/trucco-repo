<?PHP
    //error_reporting(E_ALL & ~E_NOTICE);
    //ini_set("display_errors", 1);
    ini_set('memory_limit', '-1');
	
    ini_set("session.cookie_lifetime","72000");
    ini_set("session.gc_maxlifetime","72000");
    
    session_start();
    
    include_once "../../clases/fun_aux_menu_intranet.php";
    include_once "../../clases/conexion_prestashop.php";
    include_once "../../clases/seguridad_intranet.php";	
    include_once "../../clases/ui.notification.php";
    include_once "../../clases/fechas.php";
    include_once "../../clases/PHPExcel.php";
    
        
    $conexion_prestashop = new conexion_prestashop();    
    $nombre_empresa=$conexion_prestashop->nombre_empresa_panel_control;
    $vectorUsuario=Seguridad();
    
    $mensaje = "";
    $errores="";    
    
    if(isset($_GET['mensaje']))
    {
	$mensaje=$_GET['mensaje'];
	if ($mensaje != "")
	    EjecutarCorrecto();	
    }
    
    if(isset($_GET['mensajeError']))
    {
	$mensaje=$_GET['mensajeError'];
	if ($mensaje != "")
	    EjecutarError();	
    }
    
    function str2url($string, $slug = '-', $extra = null)
    {
	return strtolower(trim(preg_replace('~[^0-9a-z' . preg_quote($extra, '~') . ']+~i', $slug, unaccent($string)), $slug));
    }//fin del function str2url($string, $slug = '-', $extra = null)
    
    function unaccent($string) // normalizes (romanization) accented chars
    {
	if (strpos($string = htmlentities($string, ENT_QUOTES, 'UTF-8'), '&') !== false) {
	    $string = html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|tilde|uml);~i', '$1', $string), ENT_QUOTES, 'UTF-8');
	}
    
	return $string;
    }//fin del function unaccent($string)
    
    function quitarAcentos($String)
    {
	$String = str_replace(array('á','à','â','ã','ª','ä'),"&aacute;",$String);
	$String = str_replace(array('Á','À','Â','Ã','Ä'),"&Aacute;",$String);
	$String = str_replace(array('Í','Ì','Î','Ï'),"&Iacute;",$String);
	$String = str_replace(array('í','ì','î','ï'),"&iacute;",$String);
	$String = str_replace(array('é','è','ê','ë'),"&eacute;",$String);
	$String = str_replace(array('É','È','Ê','Ë'),"&Eacute;",$String);
	$String = str_replace(array('ó','ò','ô','õ','ö'),"&oacute;",$String);
	$String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"&Oacute;",$String);
	$String = str_replace(array('ú','ù','û','ü'),"&uacute;",$String);
	$String = str_replace(array('Ú','Ù','Û','Ü'),"&Uacute;",$String);
	$String = str_replace(array('Ñ'),"&Ntilde;",$String);
	$String = str_replace(array('ñ'),"&ntilde;",$String);
	$String = str_replace("Ý","Y",$String);
	$String = str_replace("ý","y",$String);
	
	return $String;
    }//fin del function quitarAcentos($String)
    
    if(isset($_POST['aux_envio_estudio']))
    {	
	if (trim($_FILES['fichero']['name'])!="")
	{		
	    //subimos el fichero al directorio especifico
	    $separado = explode(".",$_FILES['fichero']['name']); 
	    $ext_nueva = strtolower($separado[count($separado)-1]); //cogemos la extension (ya en minusculas) 
	    $nombreDirectorio = "../../archivo_articulo/";
	    $nombreFichero = date("Y_m_d_H_i_s") . "." . $ext_nueva;
	    
	    //guardamos el fichero en una carpeta especifica
	    $subido_archivo=move_uploaded_file ($_FILES['fichero']['tmp_name'],$nombreDirectorio . $nombreFichero);
	    
	    if($subido_archivo==false)
	    {
		$mensaje="Fallo al subir el archivo";
		EjecutarError();
	    }//fin del if($subido_archivo==false)
	    else
	    {
		//tratamos el fichero en excel
		$objPHPExcel = PHPExcel_IOFactory::load($nombreDirectorio . $nombreFichero);
		$estudio_fila_datos=1;

		/////////////////////////////////////////////////////////////
		//OBTENEMOS LA HOJA ACTIVA
		$objHoja=$objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
		$i=1;
		$contador_actualizado=0;
		$id_shop=1;
		$id_shop2=2;
		$date_add=date("Y-m-d") . " 00:00:00";
		$date_upd=date("Y-m-d") . " 00:00:00";
		
		//RECORREMOS LAS FILAS OBTENIDAS
		foreach($objHoja as $indice => $objCelda)
		{
		    if($i>$estudio_fila_datos)
		    {
			//var_dump($objCelda);
			//EAN 13 => A
			//es - name => B
			//es - meta_description	 => C
			//es - meta_title => D
			//es - descripcion-ficha => E
			//es - composicion-lavado => F
			//es - devoluciones => G


			
			$referencia=str_replace("'","",$objCelda['A']);
			$es_name=str_replace("'","",$objCelda['B']);
			$es_meta_description=str_replace("'","",$objCelda['C']);
			$es_meta_title=str_replace("'","",$objCelda['D']);
			$es_descripcion_ficha=str_replace("'","",$objCelda['E']);
			$es_composicion_ficha=str_replace("'","",$objCelda['F']);
			$es_devoluciones=str_replace("'","",$objCelda['G']);
			
			$eng_name=str_replace("'","",$objCelda['H']);
			$eng_meta_description=str_replace("'","",$objCelda['I']);
			$eng_meta_title=str_replace("'","",$objCelda['J']);
			$eng_descripcion_ficha=str_replace("'","",$objCelda['K']);
			$eng_composicion_ficha=str_replace("'","",$objCelda['L']);
			$eng_devoluciones=str_replace("'","",$objCelda['M']);
			
			$es_name=quitarAcentos($es_name);
			$es_meta_description=quitarAcentos($es_meta_description);
			$es_meta_title=quitarAcentos($es_meta_title);
			$es_descripcion_ficha=quitarAcentos($es_descripcion_ficha);
			$es_composicion_ficha=quitarAcentos($es_composicion_ficha);
			$es_devoluciones=quitarAcentos($es_devoluciones);
			$eng_name=quitarAcentos($eng_name);
			$eng_meta_description=quitarAcentos($eng_meta_description);
			$eng_meta_title=quitarAcentos($eng_meta_title);
			$eng_descripcion_ficha=quitarAcentos($eng_descripcion_ficha);
			$eng_composicion_ficha=quitarAcentos($eng_composicion_ficha);
			$eng_devoluciones=quitarAcentos($eng_devoluciones);
			
			
			$id_type_redirected=str_replace("'","",$objCelda['N']);
			    
			//para tratar el fichero, referencia, color, stock y venta debe ser distinto de vacio
			if(trim($referencia)!="")
			{
			    //TIENDA 1 => TRUCCO
				//print($referencia . "<br />");
				//comprobamos que no existe la referencia y el color y el detalle
				$sqlDetalle="SELECT * FROM psnb_product WHERE reference='" . $referencia . "'";
				$resDetalle=$conexion_prestashop->BD_Consulta($sqlDetalle);
				$tuplaDetalle=$conexion_prestashop->BD_GetTupla($resDetalle);
				
				if($tuplaDetalle!=NULL)
				{
				    $es_link_rewrite=str2url($es_name);
				    $eng_link_rewrite=str2url($eng_name);
				    
				    $sqlUpdate="UPDATE psnb_product_lang SET name='" . ($es_name) . "', meta_description='" . ($es_meta_description) . "', meta_title='" . ($es_meta_title) . "', link_rewrite='" . $es_link_rewrite . "'
					WHERE id_lang=1 AND id_shop=" . $id_shop . " AND id_product='" . $tuplaDetalle['id_product'] . "'";				
				    $resUpdate=$conexion_prestashop->BD_Consulta($sqlUpdate);
				    //print("<br />" . $sqlUpdate);
				    
				    $sqlUpdate="UPDATE psnb_product_lang SET name='" . ($eng_name) . "', meta_description='" . ($eng_meta_description) . "', meta_title='" . ($eng_meta_title) . "', link_rewrite='" . $eng_link_rewrite . "'
					WHERE id_lang=2 AND id_shop=" . $id_shop . " AND id_product='" . $tuplaDetalle['id_product'] . "'";				
				    $resUpdate=$conexion_prestashop->BD_Consulta($sqlUpdate);
				    //print("<br />" . $sqlUpdate);
				    
				    if(trim($id_type_redirected)!="")
				    {
					$sqlUpdateTypeRedirected="UPDATE psnb_product_shop SET id_type_redirected='" . $id_type_redirected . "' WHERE id_shop=" . $id_shop . " AND id_product='" . $tuplaDetalle['id_product'] . "'";				
					$resUpdateTypeRedirected=$conexion_prestashop->BD_Consulta($sqlUpdateTypeRedirected);
					//print("<br />" . $sqlUpdateTypeRedirected);
				    }//actualizamos el id destino
				    
				    
				    //vemos si existe el psnb_product_extra_field del product
				    $sqlProductExtraField="SELECT * FROM psnb_product_extra_field WHERE id_product=" . $tuplaDetalle['id_product'];
				    $resProductExtraField=$conexion_prestashop->BD_Consulta($sqlProductExtraField);
				    $tuplaProductExtraField=$conexion_prestashop->BD_GetTupla($resProductExtraField);
				    
				    //sino existe lo insertamos
				    if($tuplaProductExtraField==NULL)
				    {
					$sqlUltimoExtraField="SELECT id_product_extra_field FROM psnb_product_extra_field ORDER BY id_product_extra_field DESC LIMIT 1";
					$resUltimoExtraField=$conexion_prestashop->BD_Consulta($sqlUltimoExtraField);
					$tuplaUltimoExtraField=$conexion_prestashop->BD_GetTupla($resUltimoExtraField);
					
					$id_product_extra_field=1;
					if($tuplaUltimoExtraField!=NULL)
					    $id_product_extra_field=$tuplaUltimoExtraField['id_product_extra_field'] + 1;
					
					$sqlInsertArticuloExtraField="INSERT INTO `psnb_product_extra_field` (`id_product_extra_field`, `id_shop_default`, `id_product`, `date_add`, `date_upd`) VALUES
							    ('" . $id_product_extra_field . "', 0, '" . $tuplaDetalle['id_product'] . "', '" . $date_add . "', '" . $date_upd . "');";
					$resInsertArticuloExtraField=$conexion_prestashop->BD_Consulta($sqlInsertArticuloExtraField);
					//print("<br />" . $sqlInsertArticuloExtraField);
				    }//fin del if($tuplaProductExtraField==NULL)
				    else
				    {
					$id_product_extra_field=$tuplaProductExtraField['id_product_extra_field'];
				    }//fin del else del if($tuplaProductExtraField==NULL)
					
				    //VEMOS SI EXISTE EL psnb_product_extra_field_shop PARA LA TIENDA
				    $sqlProductExtraFieldShop="SELECT * FROM psnb_product_extra_field_shop WHERE id_product_extra_field=" . $id_product_extra_field . " AND id_shop=" . $id_shop;
				    $resProductExtraFieldShop=$conexion_prestashop->BD_Consulta($sqlProductExtraFieldShop);
				    $tuplaProductExtraFieldShop=$conexion_prestashop->BD_GetTupla($resProductExtraFieldShop);
				    
				    //sino existe lo insertamos
				    if($tuplaProductExtraFieldShop==NULL)
				    {
					$sqlInsertArticuloExtraFieldShop="INSERT INTO `psnb_product_extra_field_shop` (`id_product_extra_field`, `id_shop`) VALUES
						('" . $id_product_extra_field . "', '" . $id_shop . "');";
					$resInsertArticuloExtraFieldShop=$conexion_prestashop->BD_Consulta($sqlInsertArticuloExtraFieldShop);
					//print("<br />" . $sqlInsertArticuloExtraFieldShop);
				    }//fin del if($tuplaProductExtraFieldShop==NULL)
				
				
				    //VEMOS SI EXISTE EL psnb_product_extra_field_lang PARA LA TIENDA
				    $sqlProductExtraFieldLang="SELECT * FROM psnb_product_extra_field_lang WHERE id_product_extra_field=" . $id_product_extra_field . " AND id_shop=" . $id_shop;
				    $resProductExtraFieldLang=$conexion_prestashop->BD_Consulta($sqlProductExtraFieldLang);
				    $tuplaProductExtraFieldLang=$conexion_prestashop->BD_GetTupla($resProductExtraFieldLang);
				    
				    //sino existe lo insertamos
				    if($tuplaProductExtraFieldShop==NULL)
				    {
					//hacemos la insercion
					$sqlInsertArticuloExtraFieldLang="INSERT INTO `psnb_product_extra_field_lang` (`id_product_extra_field`, `id_shop`, `id_lang`, `descripcion`, `composicion_y_lavado`, `devoluciones`) VALUES
						('" . $id_product_extra_field . "', '" . $id_shop . "', '1',
						'" . ($es_descripcion_ficha) . "', '" . ($es_composicion_ficha) . "', '" . ($es_devoluciones) . "');";
					$resInsertArticuloExtraFieldLang=$conexion_prestashop->BD_Consulta($sqlInsertArticuloExtraFieldLang);
					//print("<br />" . $sqlInsertArticuloExtraFieldLang);
					
					
					$sqlInsertArticuloExtraFieldLang="INSERT INTO `psnb_product_extra_field_lang` (`id_product_extra_field`, `id_shop`, `id_lang`, `descripcion`, `composicion_y_lavado`, `devoluciones`) VALUES
						('" . $id_product_extra_field . "', '" . $id_shop . "', '2',
						'" . ($eng_descripcion_ficha) . "', '" . ($eng_composicion_ficha) . "', '" . ($eng_devoluciones) . "');";
					$resInsertArticuloExtraFieldLang=$conexion_prestashop->BD_Consulta($sqlInsertArticuloExtraFieldLang);					
					//print("<br />" . $sqlInsertArticuloExtraFieldLang);
				    }//fin del if($tuplaProductExtraFieldShop==NULL)
				    else
				    {
					$sqlUpdate="UPDATE psnb_product_extra_field_lang SET descripcion='" . ($es_descripcion_ficha) . "', composicion_y_lavado='" . ($es_composicion_ficha) . "', devoluciones='" . ($es_devoluciones) . "'
					    WHERE id_lang=1 AND id_shop=" . $id_shop . " AND id_product_extra_field='" . $id_product_extra_field . "'";				    
					$resUpdate=$conexion_prestashop->BD_Consulta($sqlUpdate);
					//print("<br />" . $sqlUpdate);
					
					
					$sqlUpdate="UPDATE psnb_product_extra_field_lang SET descripcion='" . ($eng_descripcion_ficha) . "', composicion_y_lavado='" . ($eng_composicion_ficha) . "', devoluciones='" . ($eng_devoluciones) . "'
					    WHERE id_lang=2 AND id_shop=" . $id_shop . " AND id_product_extra_field='" . $id_product_extra_field . "'";				    
					$resUpdate=$conexion_prestashop->BD_Consulta($sqlUpdate);
					//print("<br />" . $sqlUpdate);
				    }//fin del else del if($tuplaProductExtraFieldShop==NULL)
				    
			    
			    //TIENDA 2 => B2B				
				    $es_link_rewrite=str2url($es_name);
				    $eng_link_rewrite=str2url($eng_name);
				    
				    $sqlUpdate="UPDATE psnb_product_lang SET name='" . ($es_name) . "', meta_description='" . ($es_meta_description) . "', meta_title='" . ($es_meta_title) . "', link_rewrite='" . $es_link_rewrite . "'
					WHERE id_lang=1 AND id_shop=" . $id_shop2 . " AND id_product='" . $tuplaDetalle['id_product'] . "'";				
				    $resUpdate=$conexion_prestashop->BD_Consulta($sqlUpdate);
				    
				    $sqlUpdate="UPDATE psnb_product_lang SET name='" . ($eng_name) . "', meta_description='" . ($eng_meta_description) . "', meta_title='" . ($eng_meta_title) . "', link_rewrite='" . $eng_link_rewrite . "'
					WHERE id_lang=2 AND id_shop=" . $id_shop2 . " AND id_product='" . $tuplaDetalle['id_product'] . "'";				
				    $resUpdate=$conexion_prestashop->BD_Consulta($sqlUpdate);
				    //print("<br />" . $sqlUpdate);
				    
				    if(trim($id_type_redirected)!="")
				    {
					$sqlUpdateTypeRedirected="UPDATE psnb_product_shop SET id_type_redirected='" . $id_type_redirected . "' WHERE id_shop=" . $id_shop2 . " AND id_product='" . $tuplaDetalle['id_product'] . "'";				
					$resUpdateTypeRedirected=$conexion_prestashop->BD_Consulta($sqlUpdateTypeRedirected);
				    }//actualizamos el id destino
				    
				    
				    //vemos si existe el psnb_product_extra_field del product
				    $sqlProductExtraField="SELECT * FROM psnb_product_extra_field WHERE id_product=" . $tuplaDetalle['id_product'];
				    $resProductExtraField=$conexion_prestashop->BD_Consulta($sqlProductExtraField);
				    $tuplaProductExtraField=$conexion_prestashop->BD_GetTupla($resProductExtraField);
				    
				    //sino existe lo insertamos
				    if($tuplaProductExtraField==NULL)
				    {
					$sqlUltimoExtraField="SELECT id_product_extra_field FROM psnb_product_extra_field ORDER BY id_product_extra_field DESC LIMIT 1";
					$resUltimoExtraField=$conexion_prestashop->BD_Consulta($sqlUltimoExtraField);
					$tuplaUltimoExtraField=$conexion_prestashop->BD_GetTupla($resUltimoExtraField);
					
					$id_product_extra_field=1;
					if($tuplaUltimoExtraField!=NULL)
					    $id_product_extra_field=$tuplaUltimoExtraField['id_product_extra_field'] + 1;
					
					$sqlInsertArticuloExtraField="INSERT INTO `psnb_product_extra_field` (`id_product_extra_field`, `id_shop_default`, `id_product`, `date_add`, `date_upd`) VALUES
							    ('" . $id_product_extra_field . "', 0, '" . $tuplaDetalle['id_product'] . "', '" . $date_add . "', '" . $date_upd . "');";
					$resInsertArticuloExtraField=$conexion_prestashop->BD_Consulta($sqlInsertArticuloExtraField);
					//print("<br />" . $sqlInsertArticuloExtraField);
				    }//fin del if($tuplaProductExtraField==NULL)
				    else
				    {
					$id_product_extra_field=$tuplaProductExtraField['id_product_extra_field'];
				    }//fin del else del if($tuplaProductExtraField==NULL)
					
				    //VEMOS SI EXISTE EL psnb_product_extra_field_shop PARA LA TIENDA
				    $sqlProductExtraFieldShop="SELECT * FROM psnb_product_extra_field_shop WHERE id_product_extra_field=" . $id_product_extra_field . " AND id_shop=" . $id_shop2;
				    $resProductExtraFieldShop=$conexion_prestashop->BD_Consulta($sqlProductExtraFieldShop);
				    $tuplaProductExtraFieldShop=$conexion_prestashop->BD_GetTupla($resProductExtraFieldShop);
				    
				    //sino existe lo insertamos
				    if($tuplaProductExtraFieldShop==NULL)
				    {
					$sqlInsertArticuloExtraFieldShop="INSERT INTO `psnb_product_extra_field_shop` (`id_product_extra_field`, `id_shop`) VALUES
						('" . $id_product_extra_field . "', '" . $id_shop2 . "');";
					$resInsertArticuloExtraFieldShop=$conexion_prestashop->BD_Consulta($sqlInsertArticuloExtraFieldShop);
					//print("<br />" . $sqlInsertArticuloExtraFieldShop);
				    }//fin del if($tuplaProductExtraFieldShop==NULL)
				
				
				    //VEMOS SI EXISTE EL psnb_product_extra_field_lang PARA LA TIENDA
				    $sqlProductExtraFieldLang="SELECT * FROM psnb_product_extra_field_lang WHERE id_product_extra_field=" . $id_product_extra_field . " AND id_shop=" . $id_shop2;
				    $resProductExtraFieldLang=$conexion_prestashop->BD_Consulta($sqlProductExtraFieldLang);
				    $tuplaProductExtraFieldLang=$conexion_prestashop->BD_GetTupla($resProductExtraFieldLang);
				    
				    //sino existe lo insertamos
				    if($tuplaProductExtraFieldShop==NULL)
				    {
					//hacemos la insercion
					$sqlInsertArticuloExtraFieldLang="INSERT INTO `psnb_product_extra_field_lang` (`id_product_extra_field`, `id_shop`, `id_lang`, `descripcion`, `composicion_y_lavado`, `devoluciones`) VALUES
						('" . $id_product_extra_field . "', '" . $id_shop2 . "', '1',
						'" . ($es_descripcion_ficha) . "', '" . ($es_composicion_ficha) . "', '" . ($es_devoluciones) . "');";
					$resInsertArticuloExtraFieldLang=$conexion_prestashop->BD_Consulta($sqlInsertArticuloExtraFieldLang);
					
					
					$sqlInsertArticuloExtraFieldLang="INSERT INTO `psnb_product_extra_field_lang` (`id_product_extra_field`, `id_shop`, `id_lang`, `descripcion`, `composicion_y_lavado`, `devoluciones`) VALUES
						('" . $id_product_extra_field . "', '" . $id_shop2 . "', '2',
						'" . ($eng_descripcion_ficha) . "', '" . ($eng_composicion_ficha) . "', '" . ($eng_devoluciones) . "');";
					$resInsertArticuloExtraFieldLang=$conexion_prestashop->BD_Consulta($sqlInsertArticuloExtraFieldLang);					
					//print("<br />" . $sqlInsertArticuloExtraFieldLang);
				    }//fin del if($tuplaProductExtraFieldShop==NULL)
				    else
				    {
					$sqlUpdate="UPDATE psnb_product_extra_field_lang SET descripcion='" . ($es_descripcion_ficha) . "', composicion_y_lavado='" . ($es_composicion_ficha) . "', devoluciones='" . ($es_devoluciones) . "'
					    WHERE id_lang=1 AND id_shop=" . $id_shop2 . " AND id_product_extra_field='" . $id_product_extra_field . "'";				    
					$resUpdate=$conexion_prestashop->BD_Consulta($sqlUpdate);
					
					
					$sqlUpdate="UPDATE psnb_product_extra_field_lang SET descripcion='" . ($eng_descripcion_ficha) . "', composicion_y_lavado='" . ($eng_composicion_ficha) . "', devoluciones='" . ($eng_devoluciones) . "'
					    WHERE id_lang=2 AND id_shop=" . $id_shop2 . " AND id_product_extra_field='" . $id_product_extra_field . "'";				    
					$resUpdate=$conexion_prestashop->BD_Consulta($sqlUpdate);
					//print("<br />" . $sqlUpdate);
				    }//fin del else del if($tuplaProductExtraFieldShop==NULL)
			    
				
				
				$contador_actualizado++;
			    }//fin del else del if($tuplaDetalle!=NULL)
			}//fin del if(trim($referencia)!="")
				
				
		    }//fin del if($i>$estudio_fila_datos)
			
		    $i++;
		}//fin del foreach($objHoja as $indice => $objCelda)
		
		$mensaje="Fichero subido Correctamente. Se han actualizado un total de: " . $contador_actualizado . " registros.";
		EjecutarCorrecto();
	    }//fin del else del if($subido_archivo==false)
	}//fin del if (trim($_FILES['fichero']['name'])!="")
    }//fin del if(isset($_POST['aux_envio_estudio']))
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Gestion de Información Articulos</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="../favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    
    <!-- Specific-->
    <link rel="stylesheet" href="../css/addons/fonts/artill-clean-icons.css"/>
    <link rel="stylesheet" href="../css/addons/theme/select2.css" class="style-theme-addon"/>
    <!--[if lt IE 9]>
    <script src="../js/basic/respond.min.js"></script>
    <script src="../js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body class="l-dashboard">
    <!--[if lt IE 9]>
    <p class="browsehappy">Su versión de navegador está <strong>absoleta</strong> por favor <a href="http://browsehappy.com/">Actualiza tu navegador</a> y mejorarás tu experiencia.</p>
    <![endif]-->
    <?PHP imprime_cabecera(); ?>
    
    <!--SECTION-->
    <section class="l-main-container">
    <!--Left Sidebar Content-->
    <?PHP imprime_sidebar("active_informacion_articulos");?>
    <!--Main Content-->
	<section class="l-container">
	    <?PHP imprime_header("FICHEROS MAESTROS - <strong>Gestión de Información - Articulos</strong>"); ?>
	    <div class="resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
	        <div class="l-row l-spaced-bottom">
	            <div class="l-box">
	                <div class="l-box-header">
			    <h2 class="l-box-title">Gestión de Información de Articulos</h2>
			    <ul class="l-box-options">
				<li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"><i class="fa fa-expand"></i></a></li>
				<li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
				<li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"><i class="fa fa-chevron-down"></i></a></li>
			    </ul>
			</div>			
			<div class="l-box-body">
			    <br/><br/><br/><br/>
			    <form class="form-horizontal" name="form_buscador2" id="form_buscador2" method="post" action="index.php" enctype="multipart/form-data">
				<input type="hidden" name="aux_envio_estudio" id="aux_envio_estudio" />
				
				<div class="form-group">				   
				    <label for="textoBuscador" class="col-sm-1 control-label">Fichero Excel</label>
				    <div class="col-sm-2">
					<input autofocus autocomplete="off" name="fichero" id="fichero" class="form-control" value="" type="file" accept="application/vnd.ms-excel" required />					
				    </div>			
				    <div class="col-sm-1" style="text-align: right">
					<button type="submit" class="btn btn-dark">Importar</button>
				    </div>
				    <div class="col-sm-2">
					<a target="_blank" href="ejemplo.xls">[Ejemplo archivo Importación]</a>
					<p>Categoria Destino 301 => es el ID de la categoria destino, por ejemplo New In es 3.</p>
				    </div>
				</div>
			    </form>
			    <br/>			    
			    <div class="clearfix"></div>
			</div>
		    </div>
		</div>
	    </div>
	
        <!--FOOTER-->
        <?PHP imprime_pie(); ?>
	
	
    </section>
    <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
    
    <!-- Semi general-->
    <script type="text/javascript">
      var paceSemiGeneral = { restartOnPushState: false };
      if (typeof paceSpecific != 'undefined'){
      	var paceOptions = $.extend( {}, paceSemiGeneral, paceSpecific );
      	paceOptions = paceOptions;
      }else{
      	paceOptions = paceSemiGeneral;
      }
    </script>
    
    <!-- Calendario-->
    <script src="../js/plugins/datetime/bootstrap-datepicker.min.js"></script>
    <script src="../js/shared/moment.min.js"></script>
    <script src="../js/plugins/datetime/daterangepicker.js"></script>
    <script src="../js/calls/ui.datetime.js"></script>
    
    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Mensajes emergentes-->
    <script src="../js/plugins/notifications/jquery.toastr.min.js"></script>
    <!-- MENSAJES -->
    <?php notification($mensaje) ?>
    
    
    
    
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/jquery.cookie.min.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/accordions/jquery.collapsible.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkradios.min.js"></script>
    <script src="../js/plugins/forms/upload/jquery.ui.widget.js"></script>
    <script src="../js/plugins/forms/upload/jquery.tmpl.min.js"></script>
    <script src="../js/plugins/forms/upload/jquery.load-image.all.min.js"></script>
    <script src="../js/plugins/forms/upload/jquery.canvas-to-blob.min.js"></script>
    <script src="../js/plugins/forms/upload/jquery.blueimp-gallery.min.js"></script>
    <script src="../js/plugins/forms/upload/jquery.iframe-transport.js"></script>
    <script src="../js/plugins/forms/upload/jquery.fileupload.js"></script>
    <script src="../js/plugins/forms/upload/jquery.fileupload-process.js"></script>
    <script src="../js/plugins/forms/upload/jquery.fileupload-image.js"></script>
    <script src="../js/plugins/forms/upload/jquery.fileupload-audio.js"></script>
    <script src="../js/plugins/forms/upload/jquery.fileupload-video.js"></script>
    <script src="../js/plugins/forms/upload/jquery.fileupload-validate.js"></script>
    <script src="../js/plugins/forms/upload/jquery.fileupload-ui.js"></script>
    <script src="../js/plugins/forms/upload/jquery.summernote.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/form.upload.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
    
    
    
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/jquery.cookie.min.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/accordions/jquery.collapsible.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/table/jquery.dataTables.min.js"></script>
    <script src="../js/plugins/tabs/jquery.easyResponsiveTabs.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
    <script src="../js/calls/table.data.js"></script>
    <script src="../js/plugins/table/dataTables.autoFill.min.js"></script>
    <script src="../js/plugins/table/dataTables.colReorder.min.js"></script>
    <script src="../js/plugins/table/dataTables.colVis.min.js"></script>
    <script src="../js/plugins/table/dataTables.responsive.min.js"></script>
    
    <!--select con buscador-->
    <script src="../js/plugins/forms/elements/jquery.select2.min.js"></script>
    <script src="../js/calls/form.elements.js"></script>
  </body>
</html>